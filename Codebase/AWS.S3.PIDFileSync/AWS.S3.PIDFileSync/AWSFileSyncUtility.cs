﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using log4net;
using log4net.Config;
using Amazon;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;
namespace Anritsu.AWS.S3.PIDFileSync
{
    internal class AWSFileSyncUtility : IDisposable
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AWSFileSyncUtility));
        private RegionEndpoint _AWS_S3_Endpoint;
        private TransferUtility _AWS_S3_TransferUtil;
        private AmazonS3Client _AWS_S3_Client;
        private AmazonCloudFrontClient _AWS_CF_Client;
        private String _AWS_AccessKey;
        private String _AWS_SecretKey;
        private Int32 _AWS_S3_Timeout;
        private String _AWS_S3_EndpointName;
        private String _AWS_S3_BucketName;
        private String _AWS_S3_Prefix;
        private Boolean _AWS_CF_Enabled = false;
        private String _AWS_CF_DistributionID;
        private Int32 _AWS_CF_MaxAgeSeconds;
        private List<String> _ContentDeposition_InlineExtentions;
        private Int32 _AWS_CF_WaitTimeInSeconds = 600000;

        public AWSFileSyncUtility()
        {
            _AWS_AccessKey = ConfigUtility.AppSettingGetValue("AWS.AWSAccessKey");
            _AWS_SecretKey = ConfigUtility.AppSettingGetValue("AWS.AWSSecretKey"); 
            _AWS_S3_Timeout = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("AWS.S3.Timeout"), 5);
            _AWS_S3_EndpointName = ConfigUtility.AppSettingGetValue("AWS.S3.EndpointName");
            _AWS_S3_BucketName = ConfigUtility.AppSettingGetValue("AWS.S3.BucketName");
            _AWS_S3_Prefix = ConfigUtility.AppSettingGetValue("AWS.S3.Prefix");
            _AWS_CF_Enabled = ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("AWS.CF.Enable"), false);
            _AWS_CF_DistributionID = ConfigUtility.AppSettingGetValue("AWS.CF.DistributionID");
            _AWS_CF_MaxAgeSeconds = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("AWS.CF.MaxAgeSeconds"), 7776000);
            _ContentDeposition_InlineExtentions = ConfigUtility.AppSettingGetValue("PIDFileSync.ContentDeposition.InlineExts").Split(',').ToList<String>();

            _AWS_S3_Endpoint = RegionEndpoint.GetBySystemName(_AWS_S3_EndpointName);
            _AWS_S3_TransferUtil = new TransferUtility(_AWS_AccessKey, _AWS_SecretKey, _AWS_S3_Endpoint);
            _AWS_S3_Client = new AmazonS3Client(_AWS_AccessKey, _AWS_SecretKey, _AWS_S3_Endpoint);
            _AWS_CF_Client = new AmazonCloudFrontClient(_AWS_AccessKey, _AWS_SecretKey);
            _AWS_CF_WaitTimeInSeconds = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("AWS.CF.WaitTimeInSeconds"), 600000);
            if (_AWS_CF_WaitTimeInSeconds < 1000) _AWS_CF_WaitTimeInSeconds = 600000;
        }

        public String AWS_S3_UploadFile(Byte[] fileData, String fileName, String friendlyFileName, String fileType)
        {
            String awsS3ObjectName = String.Empty;
            if (fileData == null || fileData.Length < 1) return awsS3ObjectName;
            using (MemoryStream ms = new MemoryStream(fileData))
            {
                awsS3ObjectName = AWS_S3_UploadFile(ms, fileName, friendlyFileName, fileType);
            }
            return awsS3ObjectName;
        }

        public String AWS_S3_UploadFile(Stream inputStream, String fileName, String friendlyFileName, String fileType)
        {
            try
            {
                if (inputStream == null || friendlyFileName.IsNullOrEmptyString() || fileType.IsNullOrEmptyString()) return String.Empty;

                String prefix = _AWS_S3_Prefix;
                String bucket = _AWS_S3_BucketName;
                String awsS3ObjectName = AWS_S3_GetAwsS3ObjectName(fileName, prefix);

                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
                       .WithBucketName(bucket)
                       .WithTimeout(_AWS_S3_Timeout)
                       .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)
                       .WithKey(awsS3ObjectName.ToLowerInvariant())
                       .WithContentType(fileType)
                       ;
                inputStream.Seek(0, SeekOrigin.Begin);
                request.InputStream = inputStream;
                
                List<String> inlineContentDepoExts = _ContentDeposition_InlineExtentions;
                string dlmode = "attachment";
                String fileExt = Path.GetExtension(friendlyFileName);
                if (inlineContentDepoExts.Contains(fileExt.Replace(".", String.Empty), StringComparer.InvariantCultureIgnoreCase))
                {
                    dlmode = "inline";
                }

                String contentDispoFileName = friendlyFileName.ToLower();
                if (contentDispoFileName.Contains("/")) contentDispoFileName = contentDispoFileName.Substring(contentDispoFileName.IndexOf("/") + 1);
                try
                {
                    request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Content-Disposition", String.Format("{0};filename={1};", dlmode,
                                contentDispoFileName)));
                }
                catch { } //some file names are having CRLF issues.  bad data from SIS.

                request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Cache-Control", String.Format("max-age={0},must-revalidate", _AWS_CF_MaxAgeSeconds)));
                _AWS_S3_TransferUtil.Upload(request);

                _logger.DebugFormat("{0} uploaded to S3", awsS3ObjectName);
                return awsS3ObjectName;
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("ERROR In UploadFileToS3:{0}:{1}", friendlyFileName, ex.ToString());
                throw ex;
            }

        }

        public String AWS_S3_DeleteFile(String fileName)
        {
            try
            {
                String prefix = _AWS_S3_Prefix;
                String bucket = _AWS_S3_BucketName;

                String awsS3ObjectName = AWS_S3_GetAwsS3ObjectName(fileName, prefix);
                DeleteObjectRequest deleteReq = new DeleteObjectRequest
                {
                    BucketName = bucket,
                    Key = awsS3ObjectName
                };
                _AWS_S3_Client.DeleteObject(deleteReq);
                _logger.DebugFormat("{0} deleted from S3.", awsS3ObjectName);
                return awsS3ObjectName;
                

            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("ERROR In DeleteFileFromS3:{0}:{1}", fileName, ex.ToString());
                throw ex;
            }
        }

        private string AWS_S3_GetAwsS3ObjectName(String targetFileName, String s3FolderPrefix)
        {
            String awsS3ObjectName = targetFileName.Replace("\\", "/");
            if (awsS3ObjectName.StartsWith("/")) awsS3ObjectName = awsS3ObjectName.Substring(1);

            if (!String.IsNullOrEmpty(s3FolderPrefix))
            {
                awsS3ObjectName = String.Format("{0}{1}", s3FolderPrefix, awsS3ObjectName);
            }
            return awsS3ObjectName.ToLowerInvariant();
        }

        public void AWS_CloudFront_InvalidateCache(List<String> listToInvalidate)
        {
            //http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html#InvalidationLimits
            /*
             * Invalidation Limits
                You can make any number of invalidation requests, but you can have only three invalidation requests per distribution in progress at one time. 
             * Each request can contain up to 1000 objects to invalidate. If you exceed these limits, CloudFront returns an error message.
                Note: It usually takes 10 to 15 minutes for CloudFront to complete your invalidation request, depending on the size of the request.
             * */
            if (!_AWS_CF_Enabled)
            {
                _logger.InfoFormat("CloudFront disabled.");
                return;
            }
            if(listToInvalidate == null)
            {
                _logger.InfoFormat("List is empty.");
                return;
            }
            Int32 processedArray = 0;
            try
            {
                Int32 maxLength = 1000;
                List<String[]> splittedList = new List<String[]>();

                for(int i=0; i<listToInvalidate.Count; i+= maxLength)
                {
                    if(listToInvalidate.Count < i + maxLength)
                        maxLength = listToInvalidate.Count - i;
                    String[] item = new String[maxLength];
                    Array.Copy(listToInvalidate.ToArray(), i, item, 0, maxLength);
                    splittedList.Add(item);
                }

                
                Int32 totalArrayToProcess = splittedList.Count;
                foreach (String[] objectList in splittedList)
                {
                    if (objectList == null || objectList.Length < 1) continue;
                    InvalidationBatch ib = new InvalidationBatch();
                    Amazon.CloudFront.Model.Paths path = new Amazon.CloudFront.Model.Paths();                    

                    while (true)
                    {
                        try
                        {
                            _logger.InfoFormat("==================== Processing array #{0} of {1} =========================", processedArray + 1, totalArrayToProcess);

                            CreateInvalidationRequest req = new CreateInvalidationRequest();
                            req.WithDistributionId(_AWS_CF_DistributionID);
                            req.WithInvalidationBatch(new InvalidationBatch
                            {
                                Paths = new Paths
                                {
                                    Quantity = objectList.Length,
                                    Items = objectList.ToList()
                                },
                                CallerReference = DateTime.UtcNow.ToString("R")
                            });

                            CreateInvalidationResponse resp = _AWS_CF_Client.CreateInvalidation(req);
                            String invalidationID = resp.CreateInvalidationResult.Invalidation.Id;
                            processedArray++;
                            _logger.InfoFormat("\n{0} items invalidated from CloudFront. {1} of {2} tasks done. InvalidationID={3} \n=========================================================\n"
                                , objectList.Length, processedArray, totalArrayToProcess, invalidationID);
                            break;
                        }
                        catch (AmazonCloudFrontException awsCFEx)
                        {
                            if (awsCFEx.ErrorCode == "TooManyInvalidationsInProgress")
                            {
                                _logger.InfoFormat("\n\n ====== TooManyInvalidationsInProgress.  Waiting for {0} seconds to try again. ====", _AWS_CF_WaitTimeInSeconds);
                                System.Threading.Thread.Sleep(_AWS_CF_WaitTimeInSeconds);//3 request limit wait

                            }
                            else
                            {
                                _logger.ErrorFormat("\n\n" + awsCFEx.ErrorCode);
                                break;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("ERROR in InvalidateCloudFrontCache, Processed Array#{0}:ERRORMSG:{1}", processedArray, ex.ToString());
            }
        }

        public void Dispose()
        {
            if (_AWS_CF_Client != null) _AWS_CF_Client.Dispose();
            if (_AWS_S3_TransferUtil != null) _AWS_S3_TransferUtil.Dispose();
        }
    }
}
