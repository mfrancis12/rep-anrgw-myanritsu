﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using log4net;
using log4net.Config;
using EM = Anritsu.AnrCommon.EmailSDK;

namespace Anritsu.AWS.S3.PIDFileSync
{
    internal class PIDFileSyncUtility
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(PIDFileSyncUtility));
        private String _PIDConnStr = ConfigUtility.ConnStrGetValue("PIDDB.ConnStr");
        private Boolean _EnableDelete = false;
        private String _LimitRecords = String.Empty;
        private Boolean _SendEmail = false;
        private List<String> _CloudFrontObjectsToInvalidate = new List<String>();

        public PIDFileSyncUtility()
        {
            _LimitRecords = ConfigUtility.AppSettingGetValue("PIDFileSync.LimitRecords");
            _EnableDelete = ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("PIDFileSync.EnableDelete"), false);
            _SendEmail = ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("PIDFileSync.SendEmail"), false);
        }

        public void DoSyncUpload(List<Int32> sisAccessTypesToInclude, DateTime lastSync, Int32 sisDocIDMin, Int32 sisDocIDMax)
        {
            if (sisAccessTypesToInclude == null || sisAccessTypesToInclude.Count < 1) return;
            Int32 totalFileToBeUploaded = 0;
            Int32 totalFileUploaded = 0;
            Int32 totalFileDeleted = 0;
            String sqlSelect = InlineSQL_SelectDocInfo(sisAccessTypesToInclude, lastSync, sisDocIDMin, sisDocIDMax);
            String sqlSelectToDelete = InlineSQL_SelectDocInfoToDelete(sisAccessTypesToInclude, lastSync, sisDocIDMin, sisDocIDMax);
            String sqlSelectDocData = InlineSQL_SelectDocData();
            AWSFileSyncUtility awsutil = new AWSFileSyncUtility();
            using (SqlConnection conn = SQLUtility.ConnOpen(_PIDConnStr))
            {
                #region " Upload "
                SqlCommand cmd1 = SQLUtility.MakeInlineCmd(conn, null, sqlSelect, 0);
                DataTable tb = SQLUtility.FillInDataTable(cmd1);
                if (tb == null || tb.Rows.Count < 1) return;
                totalFileToBeUploaded = tb.Rows.Count;
                _logger.InfoFormat("{0} records found.", totalFileToBeUploaded);

                cmd1 = SQLUtility.MakeInlineCmd(conn, null, sqlSelectDocData, 0);
                SQLUtility.AddParam(ref cmd1, "@DocumentID", 0);
                foreach (DataRow r in tb.Rows)
                {
                    Int32 docID = ConvertUtility.ConvertToInt32(r["DocumentID"], 0);
                    try
                    {

                        Int32 accessType = ConvertUtility.ConvertToInt32(r["AccessType"], 0);

                        cmd1.Parameters["@DocumentID"].Value = docID;
                        DataRow fileDataRow = SQLUtility.FillInDataRow(cmd1);
                        if (fileDataRow == null) continue;

                        String sisFileName = ConvertUtility.ConvertNullToEmptyString(r["FileName"].ToString());
                        String fileExt = Path.GetExtension(sisFileName);
                        String fileName = GenerateAWSFilePath(String.Format("sisdoc_{0}{1}", docID, fileExt), accessType);
                        String fileType = ConvertUtility.ConvertNullToEmptyString(fileDataRow["FileType"]);


                        Byte[] fileData = fileDataRow["FileImage"] as Byte[];
                        if (fileData == null || fileData.Length < 1) continue;
                        String awsS3ObjectName = awsutil.AWS_S3_UploadFile(fileData, fileName, sisFileName, fileType);
                        if (!awsS3ObjectName.IsNullOrEmptyString())
                        {
                            totalFileUploaded++;
                            this.AddToCloudFrontInvalidateCacheList(ref awsutil, awsS3ObjectName);
                            _logger.InfoFormat("{0} uploaded.  {1} of {2}", fileName, totalFileUploaded, totalFileToBeUploaded);
                        }
                    }
                    catch (Exception exFileUpload)
                    {
                        _logger.ErrorFormat("FileUpload Error: {0} - {1}", docID, exFileUpload.Message);
                    }

                }
                #endregion

                if (_EnableDelete)
                {
                    #region " delete "
                    // delete expired files from CloudFront and S3
                    cmd1 = SQLUtility.MakeInlineCmd(conn, null, sqlSelectToDelete, 0);
                    DataTable tbToDelete = SQLUtility.FillInDataTable(cmd1);
                    if (tbToDelete != null)
                    {
                        _logger.InfoFormat("{0} records found to delete from AWS.", tbToDelete.Rows.Count);
                        foreach (DataRow rToDel in tbToDelete.Rows)
                        {
                            Int32 docID = ConvertUtility.ConvertToInt32(rToDel["DocumentID"], 0);
                            try
                            {

                                Int32 accessType = ConvertUtility.ConvertToInt32(rToDel["AccessType"], 0);
                                String sisFileName = ConvertUtility.ConvertNullToEmptyString(rToDel["FileName"].ToString());
                                String fileExt = Path.GetExtension(sisFileName);

                                String fileName = GenerateAWSFilePath(String.Format("sisdoc_{0}{1}", docID, fileExt), accessType);
                                String awsS3ObjectName = awsutil.AWS_S3_DeleteFile(fileName);
                                if (!awsS3ObjectName.IsNullOrEmptyString())
                                {
                                    totalFileDeleted++;
                                    this.AddToCloudFrontInvalidateCacheList(ref awsutil, awsS3ObjectName);
                                    _logger.InfoFormat("{0} deleted.", fileName);
                                }
                            }
                            catch (Exception exFileDelete)
                            {
                                _logger.ErrorFormat("FileDelete Error: {0} - {1}", docID, exFileDelete.Message);
                                throw exFileDelete;
                            }
                        }
                    } 
                    #endregion
                }
            }

            if (_CloudFrontObjectsToInvalidate.Count > 0)
            {
                _logger.InfoFormat("Invalidating CloudFront Cache.");
                awsutil.AWS_CloudFront_InvalidateCache(_CloudFrontObjectsToInvalidate);
            }

            _logger.InfoFormat("Task completed. {0} of {1} files uploaded and {2} deleted.", totalFileUploaded, totalFileToBeUploaded, totalFileDeleted);
            if (_SendEmail)
            {
                SendEmail(totalFileUploaded, totalFileToBeUploaded, totalFileDeleted);
                _logger.Info("Email sent.");
            }
            
        }

        private void SendEmail(int totalFileUploaded, int totalFileToBeUploaded, int totalFileDeleted)
        {
            EM.EmailWebRef.SendEmailCallRequest emailCallReq = new EM.EmailWebRef.SendEmailCallRequest();
            emailCallReq.FromEmailAddress = "webmaster@anritsu.com";
            emailCallReq.ToEmailAddresses = new String[] { ConfigUtility.AppSettingGetValue("PIDFileSync.AdminEmail") };
            emailCallReq.EmailSubject = String.Format("Connect-PID Sync- {0} of {1} files uploaded and {2} deleted.", totalFileUploaded, totalFileToBeUploaded, totalFileDeleted);
            emailCallReq.EmailBody = DateTime.UtcNow.ToString("MMM-dd-yyy hh:mm tt");
            EM.EmailServiceManager.SendEmail(emailCallReq);
        }

        private String GenerateAWSFilePath(String fileName, Int32 accessType)
        {
            if (fileName.IsNullOrEmptyString()) return String.Empty;
            switch (accessType)
            {
                case 1:
                    fileName = String.Format("specific/{0}", fileName);
                    break;
                case 2:
                    fileName = String.Format("public/{0}", fileName);
                    break;
                case 3:
                    fileName = String.Format("private/{0}", fileName);
                    break;
                case 4:
                    fileName = String.Format("private-anritsu-only/{0}", fileName);
                    break;
                case 5:
                    fileName = String.Format("npi/{0}", fileName);
                    break;
            }
            return fileName;
        }

        private void AddToCloudFrontInvalidateCacheList(ref AWSFileSyncUtility awsutil, String s3ObjectKey)
        {
            if (s3ObjectKey.IsNullOrEmptyString()) return;

            String objectKey = s3ObjectKey;
            if (!s3ObjectKey.StartsWith("/")) 
                objectKey = "/" + s3ObjectKey;

            if (!_CloudFrontObjectsToInvalidate.Contains(objectKey)) 
                _CloudFrontObjectsToInvalidate.Add(objectKey);

            if (_CloudFrontObjectsToInvalidate.Count % 200 == 0)
            {
                try
                {
                    _logger.InfoFormat("Invalidating CloudFront Cache.");
                    awsutil.AWS_CloudFront_InvalidateCache(_CloudFrontObjectsToInvalidate);
                    _CloudFrontObjectsToInvalidate = new List<string>();
                }
                catch { }
                
            }
        }

        private String InlineSQL_SelectDocInfo(List<Int32> sisAccessTypesToInclude, DateTime lastSyncUTC, Int32 sisDocIDMin, Int32 sisDocIDMax)
        {
            if (sisAccessTypesToInclude == null || sisAccessTypesToInclude.Count < 1) return String.Empty;

            String accessTypeParm = "(";
            foreach (Int32 at in sisAccessTypesToInclude) accessTypeParm += String.Format("{0},", at);
            accessTypeParm = accessTypeParm.Remove(accessTypeParm.Length - 1) + ")";

            StringBuilder sb = new System.Text.StringBuilder("SELECT ");
            if (!_LimitRecords.IsNullOrEmptyString()) sb.AppendFormat("{0} ", _LimitRecords);
            sb.Append("[DocumentID], [AccessType], [FileName], [FileType] ");
            sb.Append("FROM [dbo].[AR_Documents] (NOLOCK) WHERE [FileName] IS NOT NULL ");
            sb.Append("AND [StatusID] = 8 ");
            sb.Append("AND [AccessType] IN " + accessTypeParm);
            if (lastSyncUTC != DateTime.MinValue)
            {
                sb.AppendFormat("AND ([CreatedDate] >= '{0}' OR [ModifiedDate] >= '{0}') ", lastSyncUTC.ToString());
            }

            if (sisDocIDMin > 0 || sisDocIDMax > 0)
            {
                if (sisDocIDMin < 1) sisDocIDMin = 0;
                if (sisDocIDMax < 1) sisDocIDMax = Int32.MaxValue - 1;
                sb.AppendFormat("AND [DocumentID] BETWEEN {0} AND {1} ", sisDocIDMin, sisDocIDMax);
            }

            sb.Append("ORDER BY [DocumentID] DESC ");
            return sb.ToString();
        }

        private String InlineSQL_SelectDocInfoToDelete(List<Int32> sisAccessTypesToInclude, DateTime lastSyncUTC, Int32 sisDocIDMin, Int32 sisDocIDMax)
        {
            if (sisAccessTypesToInclude == null || sisAccessTypesToInclude.Count < 1) return String.Empty;

            String accessTypeParm = "(";
            foreach (Int32 at in sisAccessTypesToInclude) accessTypeParm += String.Format("{0},", at);
            accessTypeParm = accessTypeParm.Remove(accessTypeParm.Length - 1) + ")";

            StringBuilder sb = new System.Text.StringBuilder("SELECT ");
            if (!_LimitRecords.IsNullOrEmptyString()) sb.AppendFormat("{0} ", _LimitRecords);
            sb.Append("[DocumentID], [AccessType], [FileName], [FileType] ");
            sb.Append("FROM [dbo].[AR_Documents] (NOLOCK) WHERE [FileName] IS NOT NULL ");
            sb.Append("AND [StatusID] IN (7,9) ");
            sb.Append("AND [AccessType] IN " + accessTypeParm);
            if (lastSyncUTC != DateTime.MinValue)
            {
                sb.AppendFormat("AND ([CreatedDate] >= '{0}' OR [ModifiedDate] >= '{0}') ", lastSyncUTC.ToString());
            }
            if (sisDocIDMin > 0 || sisDocIDMax > 0)
            {
                if (sisDocIDMin < 1) sisDocIDMin = 0;
                if (sisDocIDMax < 1) sisDocIDMax = Int32.MaxValue - 1;
                sb.AppendFormat("AND [DocumentID] BETWEEN {0} AND {1} ", sisDocIDMin, sisDocIDMax);
            }
            sb.Append("ORDER BY [DocumentID] DESC ");
            return sb.ToString();
        }

        private String InlineSQL_SelectDocData()
        {
            StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("SELECT [DocumentID], [FileName], [FileType], [FileSize], [FileImage] ");
            sb.Append("FROM [dbo].[AR_Documents] (NOLOCK) WHERE [DocumentID] = @DocumentID ");
            return sb.ToString();
        }
    }
}
