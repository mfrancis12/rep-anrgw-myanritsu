﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.AnrCommon.CoreLib;
using log4net;
using log4net.Config;
namespace Anritsu.AWS.S3.PIDFileSync
{
    class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Anritsu.AWS.S3.PIDFileSync.Program));
        static void Main(string[] args)
        {
            Int32 sisDocIDMin = 0;
            Int32 sisDocIDMax = 0;

            if (args != null)
            {
                foreach (string arg in args)
                {
                    if (arg.StartsWith("MinSisDocID:", StringComparison.InvariantCultureIgnoreCase))
                        sisDocIDMin = ConvertUtility.ConvertToInt32(arg.Replace("MinSisDocID:", String.Empty), 0);
                    else if (arg.StartsWith("MaxSisDocID:", StringComparison.InvariantCultureIgnoreCase))
                        sisDocIDMax = ConvertUtility.ConvertToInt32(arg.Replace("MaxSisDocID:", String.Empty), 0);
                }
            }

            XmlConfigurator.Configure();
            List<Int32> sisAccessTypes = new List<int>();
            String[] accessTypes = ConfigUtility.AppSettingGetValue("PIDFileSync.SISAccessTypesToInclude").Split(',');
            foreach (String at in accessTypes) sisAccessTypes.Add(ConvertUtility.ConvertToInt32(at, 0));

            _logger.InfoFormat(String.Format("SIS Sync started for DocID between {0} and {1}", sisDocIDMin, sisDocIDMax));

            Int32 syncMinutes = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("PIDFileSync.SyncMinutes"), 0);
            DateTime lastSync = DateTime.MinValue;
            if (syncMinutes > 0) lastSync = DateTime.UtcNow.AddMinutes(-1 * syncMinutes);

            

            PIDFileSyncUtility pidFileSync = new PIDFileSyncUtility();
            pidFileSync.DoSyncUpload(sisAccessTypes, lastSync, sisDocIDMin, sisDocIDMax);

            _logger.InfoFormat(String.Format("SIS Sync completed for DocID between {0} and {1}", sisDocIDMin, sisDocIDMax));

           // Console.WriteLine("COMPLETED");
           // Console.Read();
        }
    }
}
