﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.AnrCommon.AnrGWResourceProvider
{
    public static class AnrGWResourceAdmin
    {
        public static void ClearResCache()
        {
            var cach = new AnrGwResourceCacheFactory();
            cach.TouchCacheFile();
        }

        public static DataTable SelectFiltered(String classKey, String resKey, String locale, String contentKeyword)
        {
            return Lib.ResourceData.SelectFiltered(classKey, resKey, locale, contentKeyword);
        }

        public static DataRow Select(Int32 contentId)
        {
            return Lib.ResourceData.Select(contentId);
        }

        public static DataRow Select(String classKey, String resourceKey, String locale, Boolean? exactMatch)
        {
            return Lib.ResourceData.Select(classKey, resourceKey, locale, exactMatch);
        }

        public static DataRow Select(String classKey, String resourceKey, String locale)
        {
            return Select(classKey, resourceKey, locale, true);
        }

        public static void Delete(Int32 contentId)
        {
            DataRow deletedRow = Lib.ResourceData.Delete(contentId);
            if (deletedRow != null)
            {
                var resDataCache = new AnrGwResourceCacheFactory();
                resDataCache.RemoveFromCache(ConvertUtility.ConvertNullToEmptyString(deletedRow["ClassKey"]),
                    ConvertUtility.ConvertNullToEmptyString(deletedRow["ResourceKey"]),
                    ConvertUtility.ConvertNullToEmptyString(deletedRow["Locale"]));
            }
        }

        public static Int32 Insert(String classKey, String resKey, String locale, String contentStr, Boolean encodeHtml)
        {
            return Lib.ResourceData.Insert(classKey, resKey, locale, contentStr, encodeHtml);
        }

        public static void Update(Int32 contentId, String contentStr, Boolean allowAutoTranslate, Boolean encodeHtml)
        {
            DataRow contentRow = Lib.ResourceData.Update(contentId, contentStr, allowAutoTranslate, encodeHtml);
            if(contentRow != null)
            {
                var resDataCache = new AnrGwResourceCacheFactory();
                resDataCache.RemoveFromCache(ConvertUtility.ConvertNullToEmptyString(contentRow["ClassKey"]),
                    ConvertUtility.ConvertNullToEmptyString(contentRow["ResourceKey"]),
                    ConvertUtility.ConvertNullToEmptyString(contentRow["Locale"]));
            }
        }
    }
}
