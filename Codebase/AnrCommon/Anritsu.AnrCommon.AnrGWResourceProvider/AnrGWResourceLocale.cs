﻿using System.Data;

namespace Anritsu.AnrCommon.AnrGWResourceProvider
{
    public static class AnrGWResourceLocale
    {
        public static DataTable SelectAll()
        {
            return Data.DAResourceLocale.SelectAll();
        }
    }
}
