﻿using System;
using System.Web.Compilation;
using System.Resources;
using System.Globalization;
using System.Collections.Generic;
using System.Web;

namespace Anritsu.AnrCommon.AnrGWResourceProvider
{
    public class AnrGWResourceProvider : IResourceProvider, IDisposable
    {
        private bool _disposed;
        private readonly string _classKey = string.Empty;
        internal static List<AnrGWResourceProvider> _LoadedProviders = new List<AnrGWResourceProvider>();

        protected bool Disposed
        {
            get
            {
                lock (this)
                {
                    return _disposed;
                }
            }
        }

        public object GetObject(string resourceKey, CultureInfo culture)
        {
            if (Disposed) throw new ObjectDisposedException("AnrGWResourceProvider object is already disposed.");
            if (string.IsNullOrEmpty(resourceKey)) throw new ArgumentNullException("resourceKey");
            if (string.IsNullOrEmpty(_classKey)) throw new ArgumentNullException("classKey");
            if (culture == null || String.IsNullOrEmpty(culture.Name)) culture = CultureInfo.CurrentUICulture;
            if (String.IsNullOrEmpty(culture.Name)) culture = new CultureInfo("en");
            string classKey = _classKey;
            if (_classKey.Equals("ThisPage", StringComparison.InvariantCultureIgnoreCase))
            {
                classKey = GetCurrentPageUrl();
            }

            var resDataCache = new AnrGwResourceCacheFactory();
            return resDataCache.GetResource(classKey, resourceKey.Trim(), culture.Name);
        }

        public IResourceReader ResourceReader
        {
            get { throw new NotImplementedException(); }
        }

        public void Dispose()
        {
            lock (this)
            {
                if (_disposed == false)
                {
                    _disposed = true;
                    GC.SuppressFinalize(this);
                }
            }
        }

        public AnrGWResourceProvider(String classKey)
        {
            _classKey = classKey;
            lock (_LoadedProviders)
            {
                _LoadedProviders.Add(this);
            }
        }

        private static string GetCurrentPageUrl()
        {
            if (HttpContext.Current == null) return null;
            string relUrl = "~" + HttpContext.Current.Request.Path;
            return relUrl.ToLowerInvariant();
        }
    }
}
