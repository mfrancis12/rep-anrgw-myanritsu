﻿using System.Web.Compilation;

namespace Anritsu.AnrCommon.AnrGWResourceProvider
{
    public class AnrGWResourceProviderFactory : ResourceProviderFactory
    {

        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            return new AnrGWResourceProvider(classKey);
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            string classKey = virtualPath;
            if (!string.IsNullOrEmpty(virtualPath))
            {
                virtualPath = virtualPath.Remove(0, 1);
                classKey = virtualPath.Remove(0, virtualPath.IndexOf('/') + 1);
            }
            return new AnrGWResourceProvider(classKey);
        }
    }
}
