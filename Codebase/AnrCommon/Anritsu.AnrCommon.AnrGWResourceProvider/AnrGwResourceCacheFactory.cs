﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Globalization;

namespace Anritsu.AnrCommon.AnrGWResourceProvider
{
    internal class AnrGwResourceCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;
        public string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                {
                    _cacheFileDependencyPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["AnrGWResourceProvider.CacheFileDependencyPath.ResourceCache"]);
                }
                return _cacheFileDependencyPath;
            }
        }

        public string CacheKey
        {
            get { return "anrgwResCache"; }
        }

        public void TouchCacheFile()
        {
            using (var sw = new StreamWriter(CacheFileDependencyPath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                sw.Close();
            }
        }

        public String GetResource(String resClass, String resKey, String locale)
        {
            if (String.IsNullOrEmpty(resClass) || String.IsNullOrEmpty(resKey)) return String.Empty;
            var context = HttpContext.Current;
            String contentStr;
            Boolean contentFound;
            var ci = new CultureInfo(locale);
            if (String.IsNullOrEmpty(ci.Name)) ci = new CultureInfo("en");
            if (context == null || context.Cache == null)
            {
                contentFound = Lib.ResourceData.GetContentStr(resClass, resKey, ci.Name, true, out contentStr);
            }
            else
            {
                String cacheKeyForRes = GetCacheKey(resClass.Trim(), resKey.Trim(), ci.Name);
                contentStr = context.Cache.Get(cacheKeyForRes) as String;
                if (String.IsNullOrEmpty(contentStr))
                {
                    contentFound = Lib.ResourceData.GetContentStr(resClass, resKey, ci.Name, true, out contentStr);
                    if (contentFound)
                    {
                        context.Cache.Insert(cacheKeyForRes, contentStr,
                            new CacheDependency(CacheFileDependencyPath), DateTime.UtcNow.AddDays(30), Cache.NoSlidingExpiration);
                    }
                }
                else
                {
                    contentFound = true;
                }
            }

            return contentFound ? contentStr : resKey;
        }

        public void RemoveFromCache(String resClass, String resKey, String culture)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;
            String cacheKeyForRes = GetCacheKey(resClass.Trim(), resKey.Trim(), culture);
            context.Cache.Remove(cacheKeyForRes);
        }

        private String GetCacheKey(String resClass, String resKey, String culture)
        {
            return String.Format("{0}_{1}_{2}_{3}", CacheKey, culture.Trim(), resClass.Trim(), resKey.Trim());
        }
    }
}
