﻿using System;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.AnrCommon.AnrGWResourceProvider.Data
{
    internal static class DABase
    {
        public static class DB
        {
            public class AnrGWResourceProviderDB
            {
                private static String Schema
                {
                    get { return ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.Schema"); }
                }

                private static String ConnStr
                {
                    get { return ConfigUtility.ConnStrGetValue("AnrGWResourceProvider.ConnStr"); }
                }

                private static Int32 CommandTimeout
                {
                    get
                    {
                        int timeout;
                        int.TryParse(ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.CmdTimeout"), out timeout);
                        return timeout;
                    }
                }

                public static SqlConnection GetOpenedConn()
                {
                    return SQLUtility.ConnOpen(ConnStr);
                }

                public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
                {
                    return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
                }
            }

        }

        
                
    }
}
