﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.AnrCommon.AnrGWResourceProvider.Data
{
    internal class DAResourceData
    {
        public static DataRow Select(String classKey, String resourceKey, String locale, Boolean? exactMatch, SqlConnection conn, SqlTransaction tran)
        {
            if (locale.IsNullOrEmptyString()) locale = "en";
            SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn, tran, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.Select"));
            cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
            cmd.Parameters.AddWithValue("@ResourceKey", resourceKey.Trim());
            cmd.Parameters.AddWithValue("@Locale", locale);
            if (exactMatch.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ExactMatch", (Boolean)exactMatch);
            else cmd.Parameters.AddWithValue("@ExactMatch", DBNull.Value);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataRow Select(String classKey, String resourceKey, String locale)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                return Select(classKey, resourceKey, locale, false, conn, null);
            }
        }

        public static DataRow Select(String classKey, String resourceKey, String locale, Boolean? exactMatch)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                return Select(classKey, resourceKey, locale, exactMatch, conn, null);
            }
        }

        public static DataRow Select(Int32 contentId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn, tran, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.SelectByContentID"));
            cmd.Parameters.AddWithValue("@ContentID", contentId);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataRow Select(Int32 contentId)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                return Select(contentId, conn, null);
            }
        }

        public static DataTable SelectFiltered(String classKey, String resKey, String locale, String contentKeyword)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn
                    , null, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.SelectFiltered"));
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ClassKey", classKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ResourceKey", resKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@Locale", locale.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ContentKeyword", contentKeyword.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Delete(Int32 contentId)
        {
            DataRow rowToDelete;
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                rowToDelete = Select(contentId, conn, null);
                SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn, null, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.DeleteByContentID"));
                cmd.Parameters.AddWithValue("@ContentID", contentId);
                cmd.ExecuteNonQuery();
            }
            return rowToDelete;
        }

        public static DataRow Insert(String classKey, String resKey, String locale, String contentStr, Boolean encodeHtml)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn
                    , null, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.Insert"));
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ClassKey", classKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ResourceKey", resKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@Locale", locale.Trim());
                if (encodeHtml) SQLUtility.SqlParam_AddEncodedHtml(ref cmd, "@ContentStr", contentStr);
                else cmd.Parameters.AddWithValue("@ContentStr", contentStr);     
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow Update(Int32 contentId, String contentStr, Boolean allowAutoTranslate, Boolean encodeHtml)
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn
                    , null, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.Update"));
                cmd.Parameters.AddWithValue("@ContentID", contentId);
                if (encodeHtml) SQLUtility.SqlParam_AddEncodedHtml(ref cmd, "@ContentStr", contentStr);
                else cmd.Parameters.AddWithValue("@ContentStr", contentStr);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AllowAutoTranslate", allowAutoTranslate);
                cmd.ExecuteNonQuery();

                return Select(contentId, conn, null);
            }
        }
    }
}
