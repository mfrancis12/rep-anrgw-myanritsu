﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.AnrGWResourceProvider.Data
{
    internal class DAResourceLocale
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.DB.AnrGWResourceProviderDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.DB.AnrGWResourceProviderDB.MakeCmdSP(conn, null, ConfigUtility.AppSettingGetValue("AnrGWResourceProvider.DB.SP.ResLocale.Select"));
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
