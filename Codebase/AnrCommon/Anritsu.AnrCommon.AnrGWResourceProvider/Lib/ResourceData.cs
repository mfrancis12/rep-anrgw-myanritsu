﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.AnrCommon.AnrGWResourceProvider.Lib
{
    internal class ResourceData
    {
        public static Boolean GetContentStr(String classKey, String resourceKey, String locale, Boolean decodeHtml, out String contentStr)
        {
            contentStr = string.Empty;
            DataRow r = Select(classKey, resourceKey, locale);
            if (r == null) return false;
            contentStr = ConvertUtility.ConvertNullToEmptyString(r["ContentStr"]);
            if (decodeHtml) contentStr = System.Web.HttpUtility.HtmlDecode(contentStr);
            return true;
        }

        public static DataRow Select(String classKey, String resourceKey, String locale)
        {
            return Data.DAResourceData.Select(classKey, resourceKey, locale);
        }

        public static DataRow Select(String classKey, String resourceKey, String locale, Boolean? exactMatch)
        {
            return Data.DAResourceData.Select(classKey, resourceKey, locale, exactMatch);
        }

        public static DataRow Select(Int32 contentId)
        {
            return Data.DAResourceData.Select(contentId);
        }

        public static DataTable SelectFiltered(String classKey, String resKey, String locale, String contentKeyword)
        {
            return Data.DAResourceData.SelectFiltered(classKey, resKey, locale, contentKeyword);
        }

        public static DataRow Delete(Int32 contentId)
        {
            return Data.DAResourceData.Delete(contentId);
        }

        public static Int32 Insert(String classKey, String resKey, String locale, String contentStr, Boolean encodeHtml)
        {
            DataRow row = Data.DAResourceData.Insert(classKey, resKey, locale, contentStr, encodeHtml);
            if (row == null) return 0;
            return ConvertUtility.ConvertToInt32(row["ContentID"], 0);

        }

        public static DataRow Update(Int32 contentId, String contentStr, Boolean allowAutoTranslate, Boolean encodeHtml)
        {
            return Data.DAResourceData.Update(contentId, contentStr, allowAutoTranslate, encodeHtml);
        }
    }
}
