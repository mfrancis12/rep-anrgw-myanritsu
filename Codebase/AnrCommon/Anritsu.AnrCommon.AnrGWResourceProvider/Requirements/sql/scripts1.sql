/***** Change the Schedule and Table names as needed for your application*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GW_SSO_AnrGWResLocale](
	[Locale] [varchar](5) NOT NULL,
	[LocaleName] [varchar](50) NOT NULL,
	[Lang] [varchar](5) NOT NULL,
	[LangName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--- 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GW_SSO_AnrGWResResourceData](
	[ContentID] [int] IDENTITY(1,1) NOT NULL,
	[ClassKey] [varchar](300) NOT NULL,
	[ResourceKey] [varchar](100) NOT NULL,
	[Locale] [varchar](5) NOT NULL,
	[AllowAutoTranslate] [bit] NOT NULL,
	[ContentStr] [nvarchar](max) NULL,
	[ModifiedOnUTC] [datetime] NULL,
	[CreatedOnUTC] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



ALTER TABLE [dbo].[GW_SSO_AnrGWResResourceData]  WITH CHECK ADD  CONSTRAINT [FK_GW_SSO_AnrGWResResourceData_GW_SSO_AnrGWResLocale] FOREIGN KEY([Locale])
REFERENCES [dbo].[GW_SSO_AnrGWResLocale] ([Locale])
GO

ALTER TABLE [dbo].[GW_SSO_AnrGWResResourceData] CHECK CONSTRAINT [FK_GW_SSO_AnrGWResResourceData_GW_SSO_AnrGWResLocale]
GO

ALTER TABLE [dbo].[GW_SSO_AnrGWResResourceData] ADD  DEFAULT ((1)) FOR [AllowAutoTranslate]
GO

ALTER TABLE [dbo].[GW_SSO_AnrGWResResourceData] ADD  DEFAULT (getutcdate()) FOR [CreatedOnUTC]
GO

/* stored procedures */

CREATE PROCEDURE [dbo].[uSP_GW_SSO_AnrGWResResourceData_Select]
( 
   @ClassKey VARCHAR(300), 
   @ResourceKey VARCHAR(500), 
   @Locale VARCHAR(10), 
   @ExactMatch BIT = NULL
)
AS
SET NOCOUNT ON;

	DECLARE @LocalizationCode varchar(10)
	SET @LocalizationCode = @Locale
	
	IF (@ExactMatch IS NOT NULL AND @ExactMatch = 0)
	BEGIN
	
		IF NOT EXISTS (SELECT * FROM [dbo].[GW_SSO_AnrGWResResourceData] (NOLOCK) 
		WHERE [ClassKey] = @ClassKey AND [ResourceKey] = @ResourceKey AND [Locale] = @LocalizationCode)
		BEGIN
			DECLARE @CountryCodeIndex int
			SET @CountryCodeIndex = CHARINDEX('-', @Locale)
			IF(@CountryCodeIndex > 0)
			BEGIN
				--fall back
				SET @LocalizationCode = SUBSTRING(@Locale, 0, @CountryCodeIndex)	
			END
		END
		
		--fall back to english if fall back lang was not avaiable
		IF NOT EXISTS (SELECT [ContentID] FROM [dbo].[GW_SSO_AnrGWResResourceData] (NOLOCK) 
		WHERE [ClassKey] = @ClassKey AND [ResourceKey] = @ResourceKey AND [Locale] = @LocalizationCode)
		BEGIN
			SET @LocalizationCode = 'en'
		END
	
	END
	
	SELECT c.*
	, LocaleDisplayText = [LangName] + ' (' + [LocaleName] + ')'
	FROM  [dbo].[GW_SSO_AnrGWResResourceData] (NOLOCK) c
	INNER JOIN [dbo].[GW_SSO_AnrGWResLocale] (NOLOCK) l ON c.[Locale] = l.[Locale]
	WHERE c.[ClassKey] = @ClassKey AND c.[ResourceKey] = @ResourceKey AND c.[Locale] = @LocalizationCode

GO


CREATE PROCEDURE [dbo].[uSP_GW_SSO_AnrGWResResourceData_SelectByContentID]
( @ContentID INT )
AS 
BEGIN
SET NOCOUNT ON;
	SELECT c.*
	, LocaleDisplayText = [LangName] + ' (' + [LocaleName] + ')'
	FROM  [dbo].[GW_SSO_AnrGWResResourceData] (NOLOCK) c
	INNER JOIN [dbo].[GW_SSO_AnrGWResLocale] (NOLOCK) l ON c.[Locale] = l.[Locale]
	WHERE c.[ContentID] = @ContentID
END

GO

/*** Init data */
INSERT INTO [dbo].[GW_SSO_AnrGWResLocale] ([Locale],[LocaleName],[Lang],[LangName])
SELECT 'en', 'Default', 'en', 'English' UNION
SELECT 'en-AU', 'en-AU', 'en', 'English' UNION
SELECT 'en-GB', 'en-GB', 'en', 'English' UNION
SELECT 'en-US', 'en-US', 'en', 'English' UNION
SELECT 'ja', 'ja-JP', 'ja', 'Japanese' UNION
SELECT 'ko', 'ko-KR', 'ko', 'Korean' UNION
SELECT 'ru', 'ru-RU', 'ru', 'Russian' UNION
SELECT 'zh', 'zh-CN', 'zh-CN', 'Chinese Simplified' UNION
SELECT 'zh-TW', 'zh-TW', 'zh-TW', 'Chinese Traditional'

GO

INSERT INTO [dbo].[GW_SSO_AnrGWResResourceData] ([ClassKey],[ResourceKey],[Locale],[AllowAutoTranslate],[ContentStr])
SELECT '~/signin', 'BrowserPageTitle', 'en', 1, N'Sign In' UNION
SELECT '~/signin', 'PageTitle', 'en', 1, N'Sign In' UNION
SELECT 'IdpSignInRPCustom', '8bfd570c_PageTitle', 'en', 1, N'to MyAnritsu' UNION
SELECT 'IdpSignInRPCustom', '8bfd570c_AboutApp', 'en', 1, N'About MyAnritsu....' UNION


SELECT 'IdpSignIn', 'lcalSignInLegend.Text', 'en', 1, N'Sign In' UNION
SELECT 'IdpSignIn', 'lcalSignInLabel.Text', 'en', 1, N'' UNION
SELECT 'IdpSignIn', 'lcalSignInLegend.Text', 'en', 1, N'Sign In' UNION
SELECT 'IdpSignIn', 'lcalSignInLegend.Text', 'en', 1, N'Sign In' UNION

/*SELECT 'Common', 'CopyRight', 'en', 1, N'Copyrightę ANRITSU' UNION
SELECT 'Common', 'TOU', 'en', 1, N'Terms' UNION
SELECT 'Common', 'PrivacyAndCookies', 'en', 1, N'Privacy &amp; Cookies' UNION
SELECT 'Common', 'Anritsu', 'en', 1, N'Anritsu' UNION
SELECT 'Common', 'DefaultAppBrowserTitle', 'en', 1, N'Anritsu'*/
     
GO

/* for sso IDP messages*/
INSERT INTO [dbo].[GW_SSO_AnrGWResResourceData] ([ClassKey],[ResourceKey],[Locale],[AllowAutoTranslate],[ContentStr])
SELECT 'GwIdpMembershipProvider', 'AuthStatus_Authorized', 'en', 1, N'Logged In Successfully.' UNION
SELECT 'GwIdpMembershipProvider', 'AuthStatus_InvalidUserOrPassword', 'en', 1, N'Invalid Email/Login name or Password.' UNION
SELECT 'GwIdpMembershipProvider', 'AuthStatus_MaxFailedLoginAttempts', 'en', 1, N'The max failed login attempts have been reached.  Your account has been locked, please try again in 5 minutes.' UNION
SELECT 'GwIdpMembershipProvider', 'AuthStatus_AccountLocked', 'en', 1, N'Account is locked.' UNION
SELECT 'GwIdpMembershipProvider', 'AuthStatus_SystemError', 'en', 1, N'System Error'

SELECT * FROM  [dbo].[GW_SSO_AnrGWResResourceData]