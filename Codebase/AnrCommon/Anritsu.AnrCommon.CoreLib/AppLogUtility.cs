﻿using System;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Data.SqlClient;

namespace Anritsu.AnrCommon.CoreLib
{
    public class AppLogUtility
    {
        private static bool IsLogEnabled
        {
            get
            {
                return ConfigUtility.GetBoolPropertyFromConfig("Idp.Web.EnableLogs", false);
            }
        }

        public static void LogInfo(String appDesc,
            String environment,
            String pageUrl,
            String requestedUrl,
            String requestParams,
            String requestMethod,
            String requestUniqueID,
            String requestStartTime,
            String requestEndTime,
            String responseTimeInMilliSeconds,
            String message)
        {
            try
            {
                LogInfoInDB(appDesc, environment, pageUrl, requestedUrl, requestParams, requestMethod, requestUniqueID, requestStartTime,
                    requestEndTime, responseTimeInMilliSeconds, message, WebUtility.GetUserIP());
            }
            catch (Exception localException)
            {
                #region "  cannot log info, send email to webmasters "

                //var sbBody = new StringBuilder();
                //sbBody.Append("ExceptionUtility.LogError method cannot log the following error to database.");
                //sbBody.Append(String.Format("\nApplication : {0}", appDesc));
                //sbBody.Append("\n");
                //sbBody.Append("Exception ...\n\n");
                //sbBody.Append(string.Format("Short Message : {0}\n\n", message));
                //sbBody.Append(string.Format("Error: {0}\n\n", localException));
                //sbBody.Append(
                //    "\n\n ===================== unable to log error due to the following =======================\n\n");
                //sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                //sbBody.Append("Class Name : ExceptionUtility\n\n");
                //sbBody.Append(
                //    "Method Name : LogError(AnritsuAppsEnum enumApp, ExceptionApplicationType applicationType, Exception exception, bool emailNotification)\n");
                //sbBody.Append(string.Format("Stack Trace {0}\n\n", localException.StackTrace));
                //sbBody.AppendFormat("\n\nServer IP : {0}\n\n", Environment.MachineName);

                //var email = new MailMessage { From = new MailAddress("WebMasterNoReply@anritsu.com") };
                //var toAddr = new[] { ConfigUtility.AppSettingGetValue("CoreLib.ErrorLogUtility.EmailTo") };
                //foreach (var to in toAddr)
                //{
                //    email.To.Add(new MailAddress(to));
                //}
                //email.Subject = string.Format("Unable to log error on {0}-", appDesc);
                //email.Body = sbBody.ToString();
                //email.IsBodyHtml = false;
                //var client = new SmtpClient();
                //client.Send(email);

                #endregion
            }
        }

        private static void LogInfoInDB(String appDesc,
            String environment,
            String pageUrl,
            String requestedUrl,
            String requestParams,
            String requestMethod,
            String requestUniqueID,
            String requestStartTime,
            String requestEndTime,
            String responseTimeInMilliSeconds,
            String message,
            String clientIP
            )
        {
            var errorLogConnStr = ConfigUtility.ConnStrGetValue("CoreLib.ErrorLogUtility");
            using (var conn = SQLUtility.ConnOpen(errorLogConnStr))
            {
                var cmd = SQLUtility.MakeSPCmd(conn, null, "[dbo]", "[uSP_AppLog_Add]", 0);
                cmd.Parameters.AddWithValue("@AppDescription", appDesc);
                cmd.Parameters.AddWithValue("@Environment", environment);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@RequestedUrl", requestedUrl);
                cmd.Parameters.AddWithValue("@RequestParams", requestParams);
                cmd.Parameters.AddWithValue("@RequestMethod", requestMethod);
                cmd.Parameters.AddWithValue("@RequestUniqueID", requestUniqueID);
                cmd.Parameters.AddWithValue("@RequestStartTime", requestStartTime);
                cmd.Parameters.AddWithValue("@RequestEndTime", requestEndTime);
                cmd.Parameters.AddWithValue("@ResponseTimeInMilliSeconds", responseTimeInMilliSeconds);
                cmd.Parameters.AddWithValue("@Message", message);
                cmd.Parameters.AddWithValue("@ClientIP", clientIP);
                cmd.ExecuteNonQuery();
            }
        }

        public static void InitRequestLog(String appDesc,
            String environment,
            String pageUrl,
            String requestedUrl,
            String requestParams,
            String requestMethod,
            String responseTimeInMilliSeconds,
            String message)
        {
            //check if logs are enabled
            if (!IsLogEnabled) return;
            HttpContext.Current.Items["RequestId"] = Guid.NewGuid();
            LogInfo(appDesc, environment, pageUrl, requestedUrl, requestParams, requestMethod, HttpContext.Current.Items["RequestId"].ToString(), DateTime.UtcNow.ToString(),
                null, responseTimeInMilliSeconds, message);
        }
        public static void EndRequestLog(String appDesc,
           String environment,
           String pageUrl,
           String requestedUrl,
           String requestParams,
           String requestMethod,
           String responseTimeInMilliSeconds,
           String message)
        {
            if (IsLogEnabled)
            {
                LogInfo(appDesc, environment, pageUrl, requestedUrl, requestParams, requestMethod, HttpContext.Current.Items["RequestId"].ToString(), null,
                    DateTime.UtcNow.ToString(), responseTimeInMilliSeconds, message);
            }
        }
    }
}
