﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class ConfigUtility
    {
        public static string AppSettingGetValue(string configKey)
        {
            try
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings[configKey])
                    ? string.Empty
                    : ConfigurationManager.AppSettings[configKey];
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ConnStrGetValue(string configKey)
        {
            try
            {
                return string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[configKey].ConnectionString)
                    ? string.Empty
                    : ConfigurationManager.ConnectionStrings[configKey].ConnectionString;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static bool GetBoolProperty(string key, bool defaultValue)
        {
            return GetBoolSettingFromContext(key, defaultValue);
        }

        public static bool GetBoolProperty(string key, bool defaultValue, bool byPassContext)
        {
            return byPassContext
                ? GetBoolPropertyFromConfig(key, defaultValue)
                : GetBoolSettingFromContext(key, defaultValue);
        }

        private static bool GetBoolSettingFromContext(string key, bool defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return GetBoolPropertyFromConfig(key, defaultValue);
            }

            bool setting;
            if (HttpContext.Current.Items[key] == null)
            {
                setting = GetBoolPropertyFromConfig(key, defaultValue);
                HttpContext.Current.Items[key] = setting;
            }
            else
            {
                setting = (bool) HttpContext.Current.Items[key];
            }
            return setting;


        }

        public static bool GetBoolPropertyFromConfig(string key, bool defaultValue)
        {
            if (ConfigurationManager.AppSettings[key] == null) return defaultValue;

            if (string.Equals(ConfigurationManager.AppSettings[key], "true", StringComparison.InvariantCultureIgnoreCase))
                return true;

            return
                !string.Equals(ConfigurationManager.AppSettings[key], "false",
                    StringComparison.InvariantCultureIgnoreCase) && defaultValue;
        }

        public static string GetStringProperty(string key, string defaultValue)
        {
            return GetStringSettingFromContext(key, defaultValue);
        }

        private static string GetStringPropertyFromConfig(string key, string defaultValue)
        {
            return ConfigurationManager.AppSettings[key] ?? defaultValue;
        }

        private static string GetStringSettingFromContext(string key, string defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return GetStringPropertyFromConfig(key, defaultValue);
            }

            string setting;
            if (HttpContext.Current.Items[key] == null)
            {
                setting = GetStringPropertyFromConfig(key, defaultValue);
                HttpContext.Current.Items[key] = setting;
            }
            else
            {
                setting = HttpContext.Current.Items[key].ToString();
            }
            return setting;
        }


        public static int GetIntProperty(string key, int defaultValue)
        {
            int setting;
            return int.TryParse(ConfigurationManager.AppSettings[key], out setting) ? setting : defaultValue;
        }

        public static long GetLongProperty(string key, long defaultValue)
        {
            long setting;
            return long.TryParse(ConfigurationManager.AppSettings[key], out setting) ? setting : defaultValue;
        }

        public static Unit GetUnit(string key, Unit defaultValue)
        {
            return ConfigurationManager.AppSettings[key] != null
                ? Unit.Parse(ConfigurationManager.AppSettings[key], CultureInfo.InvariantCulture)
                : defaultValue;
        }
    }
}
