﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class ConvertUtility
    {
        public static String ConvertToString(object input, String defaultValue)
        {
            if (input == null || input == DBNull.Value || input.ToString().IsNullOrEmptyString()) return defaultValue;
            try
            {
                return input.ToString();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string ConvertNullToEmptyString(object input)
        {
            if (input == null || input == DBNull.Value || string.IsNullOrEmpty(input.ToString()))
                return string.Empty;
            return input.ToString();
        }

        public static string ConvertEmptyGuidToEmptyString(object input)
        {
            if (input == null || input == DBNull.Value || string.IsNullOrEmpty(input.ToString()) ||
                (Guid) input == Guid.Empty)
                return string.Empty;
            return input.ToString();
        }

        public static bool ConvertToBoolean(object input, bool defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return bool.Parse(input.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Int32 ConvertToInt32(object input, Int32 defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Convert.ToInt32(input);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Int64 ConvertToInt64(object input, Int64 defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Convert.ToInt64(input);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Guid ConvertToGuid(object input, Guid defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return new Guid(input.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        public static DateTime ConvertToDateTime(object input, DateTime defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Convert.ToDateTime(input.ToString());
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Byte[] ConvertToBlob(object input, Byte[] defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return (Byte[]) input;
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string ConvertToHtmlEncodedString(object input)
        {
            if (input == null || input == DBNull.Value) return string.Empty;
            return System.Web.HttpUtility.HtmlEncode(input);
        }

        public static object ConvertNullToDBNull(object input)
        {
            return input ?? DBNull.Value;
        }

        public static String ConvertToQueryString(NameValueCollection nvc)
        {
            if (nvc == null || nvc.Count < 1) return string.Empty;
            var items =
                (from string key in nvc select String.Concat(key, "=", System.Web.HttpUtility.UrlEncode(nvc[key])))
                    .ToList();
            if (items.Count < 1) return string.Empty;
            return "?" + String.Join("&", items.ToArray());
        }

        public static String ConvertToDelimitedString(List<String> lst, string delimiter)
        {
            if (lst == null || !lst.Any() || string.IsNullOrEmpty(delimiter)) return string.Empty;
            var sb = new StringBuilder();
            foreach (var s in lst) sb.AppendFormat("{0}{1}", s, delimiter);
            return sb.Remove(sb.Length - 1, 1).ToString();
        }

        public static String ConvertToFileExtention(object input, String defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Path.GetExtension(input.ToString()).ToLower();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static String ConvertToFileName(object input, String defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Path.GetFileName(input.ToString()).ToLower();
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Double ConvertToDouble(object input, Double defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Convert.ToDouble(input);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static Single ConvertToSingle(object input, Single defaultValue)
        {
            if (input == null || input == DBNull.Value) return defaultValue;
            try
            {
                return Convert.ToSingle(input);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static DataSet ConvertCsvToDataSet(string csvFilePath)
        {
            if (csvFilePath.IsNullOrEmptyString()) return null;
            var dirName = Path.GetDirectoryName(csvFilePath);
            var ext = Path.GetExtension(csvFilePath);
            //http://www.microsoft.com/en-us/download/details.aspx?id=13255

            var sbConnStr = new StringBuilder();
            sbConnStr.Append(@"Provider=Microsoft.ACE.OLEDB.12.0;");
                //Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};            

            var cmd = "SELECT * FROM [Sheet1$]";
            var isExcel = false;

            if (ext != null && ext.EndsWith("xls"))
            {
                sbConnStr.AppendFormat(@"Data Source=""{0}"";", csvFilePath);
                sbConnStr.Append(@"Extended Properties=""Excel 8.0;HDR=No;IMEX=1""");
                isExcel = true;
            }
            else if (ext != null && ext.EndsWith("xlsx"))
            {
                sbConnStr.AppendFormat(@"Data Source=""{0}"";", csvFilePath);
                sbConnStr.Append(@"Extended Properties=""Excel 12.0 Xml;HDR=No;IMEX=1""");
                isExcel = true;
            }
            else if (ext != null && ext.EndsWith("xlsm"))
            {
                sbConnStr.AppendFormat(@"Data Source=""{0}"";", csvFilePath);
                sbConnStr.Append(@"Extended Properties=""Excel 12.0 Macro;HDR=No;IMEX=1""");
                isExcel = true;
            }
            else if (ext != null && ext.EndsWith("xlsb"))
            {
                sbConnStr.AppendFormat(@"Data Source=""{0}"";", csvFilePath);
                sbConnStr.Append(@"Extended Properties=""Excel 12.0;HDR=No;IMEX=1""");
                isExcel = true;
            }
            else if (ext != null && ext.EndsWith("csv"))
            {
                sbConnStr.AppendFormat(@"Data Source=""{0}"";", dirName);
                sbConnStr.Append(@"Extended Properties=""Text;HDR=No;FMT=Delimited""");
                cmd = String.Format("SELECT * FROM [{0}]", Path.GetFileName(csvFilePath));
            }

            var connectionString = sbConnStr.ToString();

            #region " backup "

            //string connectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=\"{0}\"; Extended Properties=\"Text;HDR=No;FMT=Delimited\"", dirName);
            //String cmd = String.Format("SELECT * FROM [{0}]", Path.GetFileName(csvFilePath));
            //Boolean isExcel = false;
            //if (ext.EndsWith("xls"))
            //{
            //    connectionString = String.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=No;IMEX=1""", csvFilePath);
            //    isExcel = true;
            //    cmd = "SELECT * FROM [Sheet1$]";
            //}
            //else if (ext.EndsWith("xlsx"))
            //{
            //    connectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;HDR=No;IMEX=1""", csvFilePath);
            //    isExcel = true;
            //    cmd = "SELECT * FROM [Sheet1$]";
            //} 

            #endregion

            using (var cn = new OleDbConnection(connectionString))
            {
                cn.Open();
                if (isExcel)
                {
                    var dbSchema = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dbSchema == null || dbSchema.Rows.Count < 1)
                        throw new Exception("Error: Could not determine the name of the first worksheet.");
                    cmd = String.Format("SELECT * FROM [{0}]", dbSchema.Rows[0]["TABLE_NAME"].ToString());
                }

                var ds = new DataSet();
                var da = new OleDbDataAdapter(cmd, cn);
                da.Fill(ds);

                return ds;
            }
        }

        public static T Parse<T>(object value)
        {
            //convert value to string to allow conversion from types like float to int
            //converter.IsValid only works since .NET4 but still returns invalid values for a few cases like NULL for Unit
            try
            {
                return (T) System.ComponentModel.TypeDescriptor.GetConverter(typeof (T)).ConvertFrom(value.ToString());
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static string ConvertEmptyToNull(object input)
        {
            return string.IsNullOrEmpty(input.ToString()) ? null : input.ToString();
        }
    }
}
