﻿namespace Anritsu.AnrCommon.CoreLib.CryptoUtilities
{
    public class ClientCacheEncUtility
    {
        private const string Enckey = "4038C253-85C4-436A-8775-C413DDBBE05C";//don't change this
        public static string EncryptString(string input)
        {
            var enc = new AnritsuEncrypter(Enckey);
            return enc.EncryptData(input);
        }
        public static string DecryptString(string encryptedValue)
        {
            var enc = new AnritsuEncrypter(Enckey);
            return enc.DecryptData(encryptedValue);
        }
    }
}
