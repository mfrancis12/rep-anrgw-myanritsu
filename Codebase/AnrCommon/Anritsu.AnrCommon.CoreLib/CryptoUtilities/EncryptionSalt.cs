﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
// Article: http://www.4guysfromrolla.com/webtech/090501-1.shtml

namespace Anritsu.AnrCommon.CoreLib.CryptoUtilities
{
    public class EncryptionSalt
    {
        private const int MinKeyLength = 9;
        private String _strKey = "__Key_IntelCoreUtility";

        private static readonly Byte[] MKey = new Byte[8];
        private static readonly Byte[] MIv = new Byte[8];

        public void SetEncryptionKeyByIntId(int intId)
        {
            var segment = "_" + intId;
            var key = segment;
            while (key.Length < MinKeyLength)
            {
                key += segment;
            }
            _strKey = key;
        }

        //----------------------------------------------------------------
        //Function to encrypt data
        //----------------------------------------------------------------
        public string EncryptData(String strData)
        {
            string strResult; //Return Result

            //1. String Length cannot exceed 90Kb. Otherwise, buffer will overflow. See point 3 for reasons
            if (strData.Length > 92160)
            {
                strResult = "Error. Data String too large. Keep within 90Kb.";
                return strResult;
            }

            //2. Generate the Keys
            if (!InitKey(_strKey))
            {
                strResult = "Error. Fail to generate key for encryption";
                return strResult;
            }

            //3. Prepare the String
            //	The first 5 character of the string is formatted to store the actual length of the data.
            //	This is the simplest way to remember to original length of the data, without resorting to complicated computations.
            //	If anyone figure a good way to 'remember' the original length to facilite the decryption without having to use additional function parameters, pls let me know.
            strData = String.Format("{0,5:00000}" + strData.Replace("{", "{{").Replace("}", "}}"), strData.Length);


            //4. Encrypt the Data
            var rbData = new byte[strData.Length];
            var aEnc = new ASCIIEncoding();
            aEnc.GetBytes(strData, 0, strData.Length, rbData, 0);

            var descsp = new DESCryptoServiceProvider();

            var desEncrypt = descsp.CreateEncryptor(MKey, MIv);

            //5. Perpare the streams:
            //	mOut is the output stream. 
            //	mStream is the input stream.
            //	cs is the transformation stream.
            var mOut = new MemoryStream();
            var cs = new CryptoStream(mOut, desEncrypt, CryptoStreamMode.Write);

            long lRead = 0;
            long lTotal = strData.Length;

            try
            {
                //6. Start performing the encryption
                while (!(lTotal < lRead))
                {
                    cs.Write(rbData, 0, rbData.Length);
                    //descsp.BlockSize=64
                    lRead = mOut.Length + Convert.ToUInt32(((rbData.Length/descsp.BlockSize)*descsp.BlockSize));
                }
                // Handle last fragment
                cs.Write(rbData, 0, rbData.Length);
            }
            catch (Exception ex)
            {
                strResult = ex + "\n" + "Error. Encryption Failed. Possibly due to incorrect Key or corrputed data";
                return strResult;
            }

            //7. Returns the encrypted result after it is base64 encoded
            //	In this case, the actual result is converted to base64 so that it can be transported over the HTTP protocol without deformation.
            strResult = mOut.Length == 0 ? "" : Convert.ToBase64String(mOut.GetBuffer(), 0, (int) mOut.Length);

            return strResult;
        }

        //----------------------------------------------------------------
        //Function to decrypt data
        //----------------------------------------------------------------
        public string DecryptData(String strData)
        {
            string strResult;

            //1. Generate the Key used for decrypting
            if (!InitKey(_strKey))
            {
                strResult = "Error. Fail to generate key for decryption";
                return strResult;
            }

            //2. Initialize the service provider
            var descsp = new DESCryptoServiceProvider();
            var desDecrypt = descsp.CreateDecryptor(MKey, MIv);

            //3. Prepare the streams:
            //	mOut is the output stream. 
            //	cs is the transformation stream.
            var mOut = new MemoryStream();
            var cs = new CryptoStream(mOut, desDecrypt, CryptoStreamMode.Write);

            //4. Remember to revert the base64 encoding into a byte array to restore the original encrypted data stream
            byte[] bPlain;
            try
            {
                bPlain = Convert.FromBase64CharArray(strData.ToCharArray(), 0, strData.Length);
            }
            catch (Exception)
            {
                strResult = "Error. Input Data is not base64 encoded.";
                return strResult;
            }

            long lRead = 0;
            long lTotal = strData.Length;

            try
            {
                //5. Perform the actual decryption
                while (!(lTotal < lRead))
                {
                    cs.Write(bPlain, 0, bPlain.Length);
                    //descsp.BlockSize=64
                    lRead = mOut.Length + Convert.ToUInt32(((bPlain.Length/descsp.BlockSize)*descsp.BlockSize));
                }
                // Handle last fragment
                cs.Write(bPlain, 0, bPlain.Length);

                var aEnc = new ASCIIEncoding();
                strResult = aEnc.GetString(mOut.GetBuffer(), 0, (int) mOut.Length);

                //6. Trim the string to return only the meaningful data
                //	Remember that in the encrypt function, the first 5 character holds the length of the actual data
                //	This is the simplest way to remember to original length of the data, without resorting to complicated computations.
                var strLen = strResult.Substring(0, 5);
                var nLen = Convert.ToInt32(strLen);
                strResult = strResult.Substring(5, nLen);
            }
            catch (Exception ex)
            {
                strResult = ex + "\n" + "Error. Decryption Failed. Possibly due to incorrect Key or corrputed data";
                return strResult;
            }

            return strResult;
        }

        //----------------------------------------------------------------
        //Private function to generate the keys into member variables
        //----------------------------------------------------------------
        private static bool InitKey(String strKey)
        {
            try
            {
                // Convert Key to byte array
                var bp = new byte[strKey.Length];
                var aEnc = new ASCIIEncoding();
                aEnc.GetBytes(strKey, 0, strKey.Length, bp, 0);

                //Hash the key using SHA1
                var sha = new SHA1CryptoServiceProvider();
                var bpHash = sha.ComputeHash(bp);

                int i;
                // use the low 64-bits for the key value
                for (i = 0; i < 8; i++)
                    MKey[i] = bpHash[i];

                for (i = 8; i < 16; i++)
                    MIv[i - 8] = bpHash[i];

                return true;
            }
            catch (Exception)
            {
                //Error Performing Operations
                return false;
            }
        }
    }
}
