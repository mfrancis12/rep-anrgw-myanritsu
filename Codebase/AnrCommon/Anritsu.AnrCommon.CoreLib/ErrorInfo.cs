﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.AnrCommon.CoreLib
{
   public class ErrorInfo
   {
       public int StatusCode = 404;
       public string ErrorMessage = "No records found";
   }
}
