﻿using System;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Data.SqlClient;

namespace Anritsu.AnrCommon.CoreLib
{
    public class ErrorLogUtility
    {
        public static void LogError(String appDesc, String environment, Exception ex, String sourceHostName)
        {
            try
            {
                var shortMessage = ex.Message;
                var stackTrace = ex.StackTrace;
                var methodName = ex.TargetSite != null ? ex.TargetSite.Name : null;
                var className = ex.TargetSite != null
                    ? (ex.TargetSite.ReflectedType != null ? ex.TargetSite.ReflectedType.ToString() : null)
                    : null;

                LogErrorInDB(appDesc, environment, "n/a",
                    "n/a",
                    0,
                    "n/a",
                    shortMessage,
                    stackTrace,
                    className + "." + methodName,
                    "n/a",
                    "n/a",
                    "n/a",
                    "n/a",
                    sourceHostName);

                var innerException = ex.InnerException;
                while (innerException != null)
                {
                    stackTrace += "\r\n======== INNER EXCEPTION ==================\r\n";
                    stackTrace += String.Format("Inner Exception Message: {0}\r\n", ex.InnerException.Message);
                    stackTrace += String.Format("Inner Exception Source: {0}\r\n", ex.InnerException.Source);
                    stackTrace += String.Format("Inner Exception TargetSite: {0}\r\n", ex.InnerException.TargetSite);
                    stackTrace += String.Format("Inner Exception StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                    stackTrace += "\r\n======== INNER EXCEPTION END ==================\r\n";

                    shortMessage += String.Format(" | {0}", innerException.Message);
                    innerException = innerException.InnerException;
                }

            }
            catch (Exception localException)
            {
                #region "  cannot log error, send email to webmasters "

                var sbBody = new StringBuilder();
                sbBody.Append("ExceptionUtility.LogError method cannot log the following error to database.");
                sbBody.Append(String.Format("\nApplication : {0}", appDesc));
                sbBody.Append("\n");
                sbBody.Append("Exception ...\n\n");
                sbBody.Append(string.Format("Short Message : {0}\n\n", ex.Message));
                sbBody.Append(string.Format("Error: {0}\n\n", localException));
                sbBody.Append(
                    "\n\n ===================== unable to log error due to the following =======================\n\n");
                sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                sbBody.Append("Class Name : ExceptionUtility\n\n");
                sbBody.Append(
                    "Method Name : LogError(AnritsuAppsEnum enumApp, ExceptionApplicationType applicationType, Exception exception, bool emailNotification)\n");
                sbBody.Append(string.Format("Stack Trace {0}\n\n", localException.StackTrace));
                sbBody.AppendFormat("\n\nServer IP : {0}\n\n", Environment.MachineName);

                var message = new MailMessage {From = new MailAddress("WebMasterNoReply@anritsu.com")};
                var toAddr = new[] {ConfigUtility.AppSettingGetValue("CoreLib.ErrorLogUtility.EmailTo")};
                foreach (var to in toAddr)
                {
                    message.To.Add(new MailAddress(to));
                }
                message.Subject = string.Format("Unable to log error on {0}-", appDesc);
                message.Body = sbBody.ToString();
                message.IsBodyHtml = false;
                var client = new SmtpClient();
                client.Send(message);

                #endregion
            }
        }

        public static void LogError(String appDesc, String environment, Exception exception, HttpContext context)
        {
            if (exception == null) return;

            string link = null;
            var httpCode = 0;
            if (exception is HttpException)
            {
                var httpEx = (HttpException) exception;
                httpCode = httpEx.GetHttpCode();
            }

            string clientip = null;
            string referer = null;
            string stackTrace = null;
            string userAgent = null;
            string hostName = null;
            string userName = null;
            string clientHostName = null;
            string serializedRequest = null;
            HttpRequest request = null;
            var ex = exception.GetBaseException();

            var shortMessage = HttpUtility.HtmlEncode(ex.Message);

            if (!string.IsNullOrEmpty(ex.StackTrace))
                stackTrace = HttpUtility.HtmlEncode(ex.StackTrace);

            if (ex.InnerException != null)
            {
                stackTrace += "\r\n======== INNER EXCEPTION ==================\r\n";
                stackTrace += String.Format("Inner Exception Message: {0}\r\n", ex.InnerException.Message);
                stackTrace += String.Format("Inner Exception Source: {0}\r\n", ex.InnerException.Source);
                stackTrace += String.Format("Inner Exception TargetSite: {0}\r\n", ex.InnerException.TargetSite);
                stackTrace += String.Format("Inner Exception StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                stackTrace += "\r\n======== INNER EXCEPTION END ==================\r\n";
            }

            var methodName = ex.TargetSite != null ? ex.TargetSite.Name : null;
            var className = ex.TargetSite != null
                ? (ex.TargetSite.ReflectedType != null ? ex.TargetSite.ReflectedType.ToString() : null)
                : null;

            try
            {
                if (context != null)
                {
                    request = context.Request;
                    link = HttpUtility.HtmlEncode(request.Url.ToString());

                    clientip = WebUtility.GetUserIP();
                    clientHostName = request.UserHostName;

                    if (request.ServerVariables["HTTP_REFERER"] != null)
                        referer = request.ServerVariables["HTTP_REFERER"];

                    userAgent = request.UserAgent;
                    if (request.LogonUserIdentity != null) userName = request.LogonUserIdentity.Name;
                    hostName = request.ServerVariables["SERVER_NAME"];

                    /*System.IO.StringWriter sWriter = new System.IO.StringWriter();
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(request.GetType());
                    xmlSerializer.Serialize(sWriter, request);
                    serializedRequest = sWriter.ToString();*/
                    serializedRequest = request.Params.ToString();
                    //environment = request.RawUrl;
                }

                LogErrorInDB(appDesc, environment, context.Request.RawUrl,
                    referer,
                    httpCode,
                    clientip, shortMessage, stackTrace,
                    className + "." + methodName,
                    serializedRequest,
                    userName,
                    clientHostName,
                    userAgent,
                    hostName);
            }
            catch (Exception localException)
            {
                #region "  cannot log error, send email to webmasters "

                if (HttpContext.Current != null)
                {
                    var sbBody = new StringBuilder();
                    sbBody.Append("ExceptionUtility.LogError method cannot log the following error to database.");
                    sbBody.Append(String.Format("\nApplication : {0}", appDesc));
                    sbBody.Append("\n");
                    sbBody.Append("Exception ...\n\n");
                    sbBody.Append(string.Format("Short Message : {0}\n\n", shortMessage));
                    sbBody.Append(string.Format("Class Name : {0}\n\n", className));
                    sbBody.Append(string.Format("Method Name : {0}\n\n", methodName));
                    sbBody.Append(string.Format("Stack Trace : {0}\n\n", stackTrace));
                    sbBody.Append(string.Format("Current Url : {0}\n", link));
                    sbBody.Append(string.Format("Referring Url: {0}\n", referer));
                    sbBody.Append(string.Format("Client IP : {0}\n", clientip));
                    sbBody.Append(string.Format("Client HostName : {0}\n", clientHostName));
                    sbBody.Append(string.Format("Host : {0}\n", hostName));
                    sbBody.Append(string.Format("User Agent : {0}\n", userAgent));
                    sbBody.Append(string.Format("HttpCode : {0}\n\n", httpCode));
                    sbBody.Append(string.Format("RawURL : {0}\n\n", context.Request.RawUrl));
                    sbBody.Append(
                        "\n\n ===================== unable to log error due to the following =======================\n\n");
                    sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                    sbBody.Append("Class Name : ExceptionUtility\n\n");
                    sbBody.Append(
                        "Method Name : LogError(AnritsuAppsEnum enumApp, ExceptionApplicationType applicationType, Exception exception, bool emailNotification)\n");
                    sbBody.Append(string.Format("Stack Trace {0}\n\n", localException.StackTrace));
                    sbBody.AppendFormat("\n\nServer IP : {0}\n\n"
                        , HttpContext.Current.Request.ServerVariables["REMOTE_HOST"]);

                    var message = new MailMessage {From = new MailAddress("WebMasterNoReply@anritsu.com")};
                    var toAddr = new[] {ConfigUtility.AppSettingGetValue("CoreLib.ErrorLogUtility.EmailTo")} ??
                                 new[] {"webmaster@anritsu.com"};
                    foreach (var to in toAddr)
                    {
                     //   message.To.Add(new MailAddress(to));
                    }
                    message.Subject = string.Format("Unable to log error on {0}", request.RawUrl);
                    message.Body = sbBody.ToString();
                    message.IsBodyHtml = false;
                    var client = new SmtpClient();
                    client.Send(message);
                }

                #endregion
            }
        }

        private static void LogErrorInDB(String appDesc,
            String environment,
            String pageUrl,
            String httpReferer,
            Int32 httpCode,
            String clientIp,
            String shortMessage,
            String stackTrace,
            String method,
            String httpRequest,
            String userName,
            String clientHostName,
            String userAgent,
            String hostName
            )
        {
            var errorLogConnStr = ConfigUtility.ConnStrGetValue("CoreLib.ErrorLogUtility");
            using (var conn = SQLUtility.ConnOpen(errorLogConnStr))
            {
                var cmd = SQLUtility.MakeSPCmd(conn, null, "[dbo]", "[uSP_ExceptionLog_Add]", 0);
                cmd.Parameters.AddWithValue("@AppDescription", appDesc);
                cmd.Parameters.AddWithValue("@Environment", environment);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@HttpReferer", httpReferer);
                cmd.Parameters.AddWithValue("@HttpCode", httpCode);
                cmd.Parameters.AddWithValue("@ClientIP", clientIp);
                cmd.Parameters.AddWithValue("@ClientHostName", clientHostName);
                cmd.Parameters.AddWithValue("@ShortMessage", shortMessage);
                cmd.Parameters.AddWithValue("@StackTrace", stackTrace);
                cmd.Parameters.AddWithValue("@Method", method);
                cmd.Parameters.AddWithValue("@HttpRequest", httpRequest);
                cmd.Parameters.AddWithValue("@UserName", userName);

                cmd.Parameters.AddWithValue("@Host", hostName);
                cmd.Parameters.AddWithValue("@UserAgent", userAgent);
                cmd.Parameters.AddWithValue("@ExceptionLogApplicationTypeID", 1);

                cmd.ExecuteNonQuery();
            }
        }
    }
}
