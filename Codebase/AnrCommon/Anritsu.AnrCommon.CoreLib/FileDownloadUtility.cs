﻿using System;
using System.Text;
using System.Web;
using System.IO;
using System.Threading;
using System.Security.Cryptography;
using System.Reflection;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class FileDownloadUtility
    {
        /// <summary>
        /// this is not very good download method., but keeping it for reference
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="filePath"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public static bool DownloadFileMethod(HttpContext httpContext, string filePath, long speed)
        {
            var ret = true;
            try
            {
                switch (httpContext.Request.HttpMethod.ToUpper())
                {
                        //support Get and head method
                    case "GET":
                    case "HEAD":
                        break;
                    default:
                        httpContext.Response.StatusCode = 501;
                        return false;
                }
                if (!File.Exists(filePath))
                {
                    httpContext.Response.StatusCode = 404;
                    return false;
                }
                //#endregion

                var fileInfo = new FileInfo(filePath);

                long startBytes = 0;
                const int packSize = 1024*10; //read in block，every block 10K bytes
                var fileName = Path.GetFileName(filePath);
                var myFile = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                var br = new BinaryReader(myFile);
                var fileLength = myFile.Length;

                var sleep = (int) Math.Ceiling(1000.0*packSize/speed); //the number of millisecond
                var lastUpdateTiemStr = File.GetLastWriteTimeUtc(filePath).ToString("r");
                var eTag = HttpUtility.UrlEncode(fileName, Encoding.UTF8) + lastUpdateTiemStr;

                //validate whether the file is too large
                if (myFile.Length > Int32.MaxValue)
                {
                    httpContext.Response.StatusCode = 413;
                    return false;
                }

                if (httpContext.Request.Headers["If-Range"] != null)
                {
                    if (httpContext.Request.Headers["If-Range"].Replace("\"", "") != eTag)
                    {
                        httpContext.Response.StatusCode = 412;
                        return false;
                    }
                }
                //#endregion

                try
                {
                    httpContext.Response.Clear();
                    httpContext.Response.Buffer = false;
                    httpContext.Response.AddHeader("Content-MD5", GetMD5Hash(fileInfo));
                    httpContext.Response.AddHeader("Accept-Ranges", "bytes");
                    httpContext.Response.AppendHeader("ETag", "\"" + eTag + "\"");
                    httpContext.Response.AppendHeader("Last-Modified", lastUpdateTiemStr);
                    httpContext.Response.ContentType = "application/octet-stream";
                    httpContext.Response.AddHeader("Content-Disposition", "attachment;filename=" +

                                                                          HttpUtility.UrlEncode(fileName, Encoding.UTF8)
                                                                              .Replace("+", "%20"));
                    httpContext.Response.AddHeader("Content-Length", (fileLength - startBytes).ToString());
                    httpContext.Response.AddHeader("Connection", "Keep-Alive");
                    httpContext.Response.ContentEncoding = Encoding.UTF8;
                    if (httpContext.Request.Headers["Range"] != null)
                    {
                        httpContext.Response.StatusCode = 206;
                        var range = httpContext.Request.Headers["Range"].Split(new char[] {'=', '-'});
                        startBytes = Convert.ToInt64(range[1]);
                        if (startBytes < 0 || startBytes >= fileLength)
                        {
                            return false;
                        }
                    }
                    if (startBytes > 0)
                    {
                        httpContext.Response.AddHeader("Content-Range",
                            string.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength));
                    }
                    //#endregion

                    //send data
                    br.BaseStream.Seek(startBytes, SeekOrigin.Begin);
                    var maxCount = (int) Math.Ceiling((fileLength - startBytes + 0.0)/packSize); //download in block
                    for (var i = 0; i < maxCount && httpContext.Response.IsClientConnected; i++)
                    {
                        httpContext.Response.BinaryWrite(br.ReadBytes(packSize));
                        httpContext.Response.Flush();
                        if (sleep > 1) Thread.Sleep(sleep);
                    }
                    //#endregion
                }
                catch
                {
                    ret = false;
                }
                finally
                {
                    br.Close();
                    myFile.Close();
                }
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        private static string GetMD5Hash(FileInfo file)
        {
            var stream = file.OpenRead();
            MD5 md5 = new MD5CryptoServiceProvider();
            var retVal = md5.ComputeHash(stream);
            stream.Close();

            var sb = new StringBuilder();
            foreach (var t in retVal)
            {
                sb.Append(t.ToString("x2"));
            }
            return sb.ToString();
        }

        public static string GetMimeType(string fileName)
        {
            //note use version 2.0.0.0 if .NET 4 is not installed, in .NET 4.5 this method has now been made public, this method apparently stores a list of mime types which would be more complete then using registry
            return
                (string) Assembly.Load("System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
                    .GetType("System.Web.MimeMapping")
                    .GetMethod("GetMimeMapping", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)
                    .Invoke(null, new object[] {fileName});
        }

        public static string GenerateETag(HttpContext context, DateTime lastModTime)
        {
            //http://reflector.webtropy.com/default.aspx/DotNET/DotNET/8@0/untmp/whidbey/REDBITS/ndp/fx/src/xsp/System/Web/StaticFileHandler@cs/2/StaticFileHandler@cs
            var strETag = new StringBuilder();

            //
            // For now, we will produce an ETag which
            // will be invalidated when:
            // a) The static file has changes (that's ok)
            // b) When an apppool starts
            //
            var appDomainFileTime = DateTime.Now.ToFileTime();

            //
            // Get 64-bit FILETIME stamps
            //

            var lastModFileTime = lastModTime.ToFileTime();

            //
            // ETag is "<hexified last="" mod="">:<hexified create="" app="">"
            //

            strETag.Append("\"");
            strETag.Append((lastModFileTime).ToString("X8", CultureInfo.InvariantCulture));
            strETag.Append(":");
            strETag.Append((appDomainFileTime).ToString("X8", CultureInfo.InvariantCulture));
            strETag.Append("\"");

            //
            // Is this a strong ETag.  Do what IIS does to determine this.
            // Compare the last modified time to now and if it earlier by
            // more than 3 seconds, then it is strong.Equals( strIfRange ) )
            //

            if (!((DateTime.Now.ToFileTime() - lastModFileTime) > 30000000))
            {
                //
                // Weak ETag
                //

                return "W/" + strETag;
            }

            //
            // Stron ETag.  Leave as is
            //

            return strETag.ToString();
        }

        public static string GenerateFileETag(HttpContext context, String fileName, DateTime modifyDate)
        {
            //use file name and modify date as the unique identifier
            var fileString = fileName + modifyDate.ToString("d", CultureInfo.InvariantCulture);
            //get string bytes
            var stringEncoder = Encoding.UTF8.GetEncoder();
            var stringBytes = new byte[stringEncoder.GetByteCount(fileString.ToCharArray(), 0, fileString.Length, true)];
            stringEncoder.GetBytes(fileString.ToCharArray(), 0, fileString.Length, stringBytes, 0, true);
            //hash string using MD5 and return the hex-encoded hash
            var md5Enc = new MD5CryptoServiceProvider();
            return "\"" + BitConverter.ToString(md5Enc.ComputeHash(stringBytes)).Replace("-", string.Empty) + "\"";
        }

        public static bool IsFileModified(HttpContext context, string filename, DateTime modifyDate, string eTag)
        {
            //http://hartzer.wordpress.com/tag/c/
            DateTime modifiedSince;

            //assume file has been modified unless we can determine otherwise
            var fileDateModified = true;

            //Check If-Modified-Since request header, if it exists 
            if (!string.IsNullOrEmpty(context.Request.Headers["If-Modified-Since"])
                && DateTime.TryParse(context.Request.Headers["If-Modified-Since"], out modifiedSince))
            {
                fileDateModified = false;
                if (modifyDate > modifiedSince)
                {
                    var modifyDiff = modifyDate - modifiedSince;
                    //ignore time difference of up to one seconds to compensate for date encoding
                    fileDateModified = modifyDiff > TimeSpan.FromSeconds(1);
                }
            }

            //check the If-None-Match header, if it exists, this header is used by FireFox to validate entities based on the etag response header 
            var eTagChanged = false;
            if (!string.IsNullOrEmpty(context.Request.Headers["If-None-Match"]))
            {
                eTagChanged = context.Request.Headers["If-None-Match"] != eTag;
            }
            return (eTagChanged || fileDateModified);
        }

        public static void TransmitResumableFile(HttpContext context
            , FileInfo fileInfo
            , String contentType
            , String customFileNameForUser
            , Boolean useInlineContentDisposition)
        {
            var request = context.Request;
            var response = context.Response;

            if (fileInfo == null || !fileInfo.Exists || String.IsNullOrEmpty(fileInfo.Extension))
            {
                response.StatusCode = (int) HttpStatusCode.NotFound;
                response.End();
                return;
            }

            var startIndex = 0;
            var responseLength = fileInfo.Exists ? fileInfo.Length : 0;
            var lastFileModDate = new DateTime(fileInfo.LastWriteTime.Year,
                fileInfo.LastWriteTime.Month,
                fileInfo.LastWriteTime.Day,
                fileInfo.LastWriteTime.Hour,
                fileInfo.LastWriteTime.Minute,
                fileInfo.LastWriteTime.Second,
                0);
            if (lastFileModDate > DateTime.Now) lastFileModDate = DateTime.Now;
            var etag = request.Headers["If-None-Match"];
            if (etag.IsNullOrEmptyString()) etag = GenerateFileETag(context, fileInfo.Name, lastFileModDate);

            //if the "If-Match" exists and is different to etag (or is equal to any "*" with no resource) then return 412 precondition failed
            if (request.Headers["If-Match"] == "*" && !fileInfo.Exists ||
                request.Headers["If-Match"] != null && request.Headers["If-Match"] != "*" &&
                request.Headers["If-Match"] != etag)
            {
                context.Response.StatusCode = (int) HttpStatusCode.PreconditionFailed;
                context.Response.End();
            }

            if (request.Headers["Range"] != null &&
                (request.Headers["If-Range"] == null || request.Headers["IF-Range"] == etag))
            {
                var match = Regex.Match(request.Headers["Range"], @"bytes=(\d*)-(\d*)");
                startIndex = ConvertUtility.Parse<int>(match.Groups[1].Value);
                responseLength = (ConvertUtility.Parse<int?>(match.Groups[2].Value) + 1 ?? fileInfo.Length) - startIndex;
                response.StatusCode = (int) HttpStatusCode.PartialContent;
                response.AddHeader("Content-Range",
                    "bytes " + startIndex + "-" + (startIndex + responseLength - 1) + "/" + fileInfo.Length);
            }
            else
            {
                if (!IsFileModified(context, fileInfo.Name, lastFileModDate, etag))
                {
                    //no change, return 304
                    response.StatusCode = (int) HttpStatusCode.NotModified;
                    response.StatusDescription = "Not Modified";
                    response.AddHeader("Content-Length", "0"); //set to 0 to prevent client waiting for data
                    response.Cache.SetCacheability(HttpCacheability.Public); //has to be not Private
                    response.Cache.SetLastModified(lastFileModDate);
                    response.Cache.SetETag(etag);
                    response.End();
                    return;
                }

            }
            response.AddHeader("Accept-Ranges", "bytes");
            response.AddHeader("Content-Length", responseLength.ToString());
            response.Cache.SetAllowResponseInBrowserHistory(true);
            response.Cache.SetCacheability(HttpCacheability.Public);
            response.Cache.SetLastModified(lastFileModDate);
            response.Cache.SetETag(etag);
            response.BufferOutput = false;
            if (contentType.IsNullOrEmptyString()) contentType = GetMimeType(fileInfo.Name);
            context.Response.ContentType = contentType;

            if (customFileNameForUser.IsNullOrEmptyString()) customFileNameForUser = fileInfo.Name;
            if (useInlineContentDisposition)
                context.Response.AddHeader("Content-Disposition", "inline; filename=" + customFileNameForUser);
            else context.Response.AddHeader("Content-Disposition", "attachment; filename=" + customFileNameForUser);

            //context.Response.AddHeader("X-Robots-Tag", "noarchive");//Tell search engine not to show cache link
            response.TransmitFile(fileInfo.FullName, startIndex, responseLength);

        }
    }
}
