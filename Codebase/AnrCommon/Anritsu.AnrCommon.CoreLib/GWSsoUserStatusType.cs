﻿namespace Anritsu.AnrCommon.CoreLib
{
    public class GWSsoUserStatusType
    {
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }
        public string UserStatusTitle { get; set; }
    }
}
