﻿namespace Anritsu.AnrCommon.CoreLib
{
    public class GWSsoUserType
    {
        public int UserId { get; set; }
        public string SecToken { get; set; }
        public string EmailAddress { get; set; }
        public string LoginName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string FirstNameInRoman { get; set; }
        public string LastNameInRoman { get; set; }
        public string CompanyInRoman { get; set; }

        public GWSsoUserStatusType UserStatus { get; set; }
        public GWSsoUserTypeType UserType { get; set; }
        public string PrimaryCultureCode { get; set; }
        public string JobTitle { get; set; }
        public string CompanyName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string RegisteredFromURL { get; set; }
        public bool IsTempPassword { get; set; }
        public string EncryptedTempPassword { get; set; }
        public string CreatedUTCDate { get; set; }
        public string MotifiedUTCDate { get; set; }

        public string CountryName { get; set; }
        public bool AcceptsEmail { get; set; }
        public string ReturnURL { get; set; }
        public string SpToken { get; set; }
        public string Browser { get; set; }
        public string UserAgent { get; set; }
        public string ClientIP { get; set; }
        public bool IsIndividualEmailAddress { get; set; }
        public bool IsNewUser { get; set; }
        //public List<GWSsoUserRoleType> UserRoleCollection { get; set; }
        //public List<GWSsoLoginHistoryType> LoginHistoryCollection { get; set; }

        public bool IsUSCustomer
        {
            get
            {
                return !string.IsNullOrEmpty(CountryCode) && CountryCode == "US";
            }
        }
    }
}
