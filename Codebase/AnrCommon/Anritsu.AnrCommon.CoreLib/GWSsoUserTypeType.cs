﻿namespace Anritsu.AnrCommon.CoreLib
{
    public class GWSsoUserTypeType
    {
        public int UserTypeId { get; set; }
        public string UserType { get; set; }
        public bool IsUserProfile { get; set; }
        public string Description { get; set; }
    }
}
