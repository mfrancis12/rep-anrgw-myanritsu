﻿using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class JsonUtil
    {
        /// <summary>
        /// Serilaize table data to Json string 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="clientStatus"></param>
        /// <returns></returns>
        public static string ConvertTableToJsonString(DataTable dt, ref HttpStatusCode clientStatus,ErrorInfo errorInfo=null)
        {
            if (dt == null || dt.Rows.Count == 0)
            {
                //send not found status code if no rows found
                clientStatus = HttpStatusCode.NotFound;
                if(errorInfo==null) errorInfo=new ErrorInfo();
                return JsonConvert.SerializeObject(errorInfo);
            }
            var jsonString = JsonConvert.SerializeObject(dt);
            return jsonString;
        }
        
    }
}
