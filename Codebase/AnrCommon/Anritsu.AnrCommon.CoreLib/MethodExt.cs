﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web;
using System.Data;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class MethodExt
    {
        public static bool IsInt32(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            try
            {
                var i = Convert.ToInt32(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsGuid(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;
            try
            {
                var test = new Guid(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDateTime(this string input)
        {
            try
            {
                Convert.ToDateTime(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsSingle(this object input)
        {
            if (input == null)
                return false;
            try
            {
                var test = Convert.ToSingle(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsValidEmailFormat(this string stringToValidate)
        {
            if (string.IsNullOrEmpty(stringToValidate))
                return false;

            var mRegEx =
                new Regex(
                    @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return mRegEx.IsMatch(stringToValidate);
        }

        public static string StripHtml(this string input)
        {
            var regex = new Regex("<[^>]*>|<.*$", RegexOptions.IgnoreCase);
            return regex.Replace(input, string.Empty);
        }

        public static bool IsNullOrEmptyGuid(this Guid input)
        {
            return input == Guid.Empty;
        }

        public static bool IsCultureInfo(this string input)
        {
            if (string.IsNullOrEmpty(input)) return false;
            try
            {
                var ci = new CultureInfo(input);
                return !string.IsNullOrEmpty(ci.Name);
            }
            catch
            {
                return false;
            }
        }

        public static bool IsNullOrEmptyString(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        public static string ConvertNullToEmptyString(this string input)
        {
            return String.IsNullOrWhiteSpace(input) ? String.Empty : input;
        }

        public static string MakeAlphaNumericOnly(this string input, string replacement)
        {
            var cleanUp = new Regex("[^a-zA-Z0-9]+");
            return cleanUp.Replace(input, replacement);
        }

        public static String ToStringFormatted(this DateTime input)
        {
            if (input == DateTime.MinValue) return String.Empty;
            var ciFormat = System.Threading.Thread.CurrentThread.CurrentCulture;
            return input.ToString(ciFormat.DateTimeFormat);
        }

        public static string HTMLEncode(this string inputString)
        {
            return HttpUtility.HtmlEncode(inputString);
        }

        public static string HTMLDecode(this string inputString)
        {
            return HttpUtility.HtmlDecode(inputString);
        }

        public static bool IsValidLoginNameOrEmailFormat(this string stringToValidate)
        {
            if (string.IsNullOrEmpty(stringToValidate))
            {
                return false;
            }
            var regex =
                new Regex(
                    "\\s*\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\s*|^[a-zA-Z0-9][a-zA-Z0-9-_.]{4,6}[a-zA-Z0-9]$");
            return regex.IsMatch(stringToValidate);
        }

        public static Boolean Contains(this String source, String keyword, RegexOptions option)
        {
            return Regex.IsMatch(source, keyword, option);
        }

        public static string FormatPhoneNumber(this string phone)
        {
            var text = RemovePhoneFormatting(phone);
            if (text.Trim().Length == 10)
            {
                phone = string.Format("{0}-{1}-{2}", text.Substring(0, 3), text.Substring(3, 3), text.Substring(6, 4));
            }
            return phone;
        }

        private static string RemovePhoneFormatting(string phone)
        {
            if (phone == null)
            {
                phone = "";
            }
            var array = phone.ToCharArray();
            return array.Where(c => c >= '0' && c <= '9').Aggregate("", (current, c) => current + c);
        }

        public static Boolean IsNullOrEmpty(this DataTable tblToCheck)
        {
            return (tblToCheck == null || tblToCheck.Rows.Count == 0);
        }

        public static void SetUsCulture(this System.Threading.Thread curretnThread)
        {
            curretnThread.CurrentCulture = new CultureInfo("en-US");
        }
        public static void SetCurretnCulture(this System.Threading.Thread curretnThread,CultureInfo curretnCulture)
        {
            curretnThread.CurrentCulture = curretnCulture;
        }
    }
}
