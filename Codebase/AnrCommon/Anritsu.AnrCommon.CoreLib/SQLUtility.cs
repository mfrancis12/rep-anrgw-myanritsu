﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class SQLUtility
    {
        public static SqlConnection ConnOpen(string connStr)
        {
            var conn = new SqlConnection(connStr);
            conn.Open();
            return conn;
        }

        public static void ConnClose(SqlConnection conn)
        {
            if (conn != null && conn.State != ConnectionState.Closed)
                conn.Close();
        }

        public static SqlCommand MakeSPCmd(SqlConnection conn
            , SqlTransaction tran
            , string spSchema
            , string spName
            , int timeout)
        {
            if (string.IsNullOrEmpty(spSchema)) spSchema = "[dbo]";
            if (string.IsNullOrEmpty(spName)) throw new ArgumentNullException("spName");

            var sp = spSchema + "." + spName;
            return MakeSqlCommand(conn, tran, CommandType.StoredProcedure, sp, timeout);
        }

        public static SqlCommand MakeInlineCmd(SqlConnection conn
            , SqlTransaction tran
            , string sqlStatement
            , int timeout)
        {
            return MakeSqlCommand(conn, tran, CommandType.Text, sqlStatement, timeout);
        }

        public static SqlCommand MakeSqlCommand(SqlConnection conn
            , SqlTransaction tran
            , CommandType cmdType
            , string cmdText
            , int timeout)
        {

            if (conn == null) throw new ArgumentNullException("conn");
            if (string.IsNullOrEmpty(cmdText)) throw new ArgumentNullException("cmdText");

            var cmd = new SqlCommand(cmdText, conn);
            if (tran != null) cmd.Transaction = tran;
            cmd.CommandType = cmdType;

            if (timeout > 0) cmd.CommandTimeout = timeout;
            return cmd;
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd)
        {
            return FillInDataSet(sqCmd, string.Empty);
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd, string tbName)
        {
            var da = new SqlDataAdapter(sqCmd);
            var ds = new DataSet();
            if (string.IsNullOrEmpty(tbName)) da.Fill(ds);
            else da.Fill(ds, tbName);
            return ds;
        }

        public static DataSet FillInDataSet(SqlDataAdapter sqDa)
        {
            if (sqDa == null || sqDa.SelectCommand == null)
                return null;

            var ds = new DataSet();
            sqDa.Fill(ds);
            return ds;

        }

        public static DataTable FillInDataTable(SqlCommand sqCmd)
        {
            return FillInDataTable(sqCmd, string.Empty);
        }

        public static DataTable FillInDataTable(SqlCommand sqCmd, string tbName)
        {
            var ds = FillInDataSet(sqCmd, tbName);
            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;

        }

        public static DataRow FillInDataRow(SqlCommand sqCmd)
        {
            var tb = FillInDataTable(sqCmd);
            if (tb == null || tb.Rows.Count < 1) return null;
            return tb.Rows[0];
        }

        public static SqlDataReader FillInDataReader(SqlCommand sqCmd)
        {
            if (sqCmd == null) throw new ArgumentNullException("sqCmd");
            return sqCmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public static SqlDataReader FillInDataReader(SqlCommand sqCmd, CommandBehavior cmdBehavior)
        {
            if (sqCmd == null) throw new ArgumentNullException("sqCmd");
            return sqCmd.ExecuteReader(cmdBehavior);
        }

        public static void SqlParam_AddAsVarBinary(ref SqlCommand sqCmd, string parmName, Byte[] inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            var parm = new SqlParameter(parmName, SqlDbType.VarBinary);

            if (inputValue == null)
            {
                parm.Value = DBNull.Value;
            }
            else
            {
                parm.Value = inputValue;
            }
            sqCmd.Parameters.Add(parm);
        }

        public static void SqlParam_AddEncodedHtml(ref SqlCommand sqCmd, string parmName, string inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;

            var parm = new SqlParameter(parmName, SqlDbType.NVarChar);

            if (inputValue == null)
            {
                parm.Value = DBNull.Value;
            }
            else
            {
                parm.Value = System.Web.HttpUtility.HtmlEncode(inputValue);
            }
            sqCmd.Parameters.Add(parm);
        }

        public static void SqlParam_AddAsGuid(ref SqlCommand sqCmd, string parmName, Guid inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            var parm = new SqlParameter(parmName, SqlDbType.UniqueIdentifier);

            if (inputValue.IsNullOrEmptyGuid())
            {
                parm.Value = DBNull.Value;
            }
            else
            {
                parm.Value = inputValue;
            }
            sqCmd.Parameters.Add(parm);
        }

        public static void SqlParam_AddAsDateTime(ref SqlCommand sqCmd, string parmName, DateTime inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            var parm = new SqlParameter(parmName, SqlDbType.DateTime);

            if (inputValue == DateTime.MinValue)
            {
                parm.Value = DBNull.Value;
            }
            else
            {
                parm.Value = inputValue;
            }
            sqCmd.Parameters.Add(parm);
        }

        public static void SqlParam_AddAsString(ref SqlCommand sqCmd, string parmName, String inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            if (String.IsNullOrEmpty(inputValue)) sqCmd.Parameters.AddWithValue(parmName, DBNull.Value);
            else sqCmd.Parameters.AddWithValue(parmName, inputValue);
        }

        public static void SqlParam_AddAsInt(ref SqlCommand sqCmd, string parmName, int inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            if (inputValue <= 0) sqCmd.Parameters.AddWithValue(parmName, DBNull.Value);
            else sqCmd.Parameters.AddWithValue(parmName, inputValue);
        }

        public static void SqlParam_AddAsLongInt(ref SqlCommand sqCmd, string parmName, long inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            if (inputValue <= 0) sqCmd.Parameters.AddWithValue(parmName, DBNull.Value);
            else sqCmd.Parameters.AddWithValue(parmName, inputValue);
        }

        public static void SqlParam_AddAsBoolean(ref SqlCommand sqCmd, string parmName, Boolean inputValue)
        {
            if (sqCmd == null || string.IsNullOrEmpty(parmName)) return;
            sqCmd.Parameters.AddWithValue(parmName, inputValue ? 1 : 0);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, String input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            cmd.Parameters.AddWithValue(paramName, input.IsNullOrEmptyString() ? String.Empty : input.Trim());
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, Guid input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            if (input.IsNullOrEmptyGuid()) cmd.Parameters.AddWithValue(paramName, DBNull.Value);
            else cmd.Parameters.AddWithValue(paramName, input);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, Int32 input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            cmd.Parameters.AddWithValue(paramName, input);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, Int32 input, Int32 dbNullValue)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            if (input == dbNullValue)
                cmd.Parameters.AddWithValue(paramName, DBNull.Value);
            else
                cmd.Parameters.AddWithValue(paramName, input);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, DateTime input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            if (input == DateTime.MinValue) cmd.Parameters.AddWithValue(paramName, DBNull.Value);
            else cmd.Parameters.AddWithValue(paramName, input);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, Boolean input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            cmd.Parameters.AddWithValue(paramName, input ? 1 : 0);
        }

        public static void AddParam(ref SqlCommand cmd, String paramName, DataTable input)
        {
            if (cmd == null) return;
            if (paramName.IsNullOrEmptyString()) return;
            cmd.Parameters.AddWithValue(paramName, input);
        }
    }
}
