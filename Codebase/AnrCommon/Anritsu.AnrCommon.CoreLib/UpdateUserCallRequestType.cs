﻿namespace Anritsu.AnrCommon.CoreLib
{
    public class UpdateUserCallRequestType
    {
        public string NewEncryptedPassword { get; set; }
        public bool NewPasswordSpecified { get; set; }
        public GWSsoUserType GWSsoUser { get; set; }
    }
}
