﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.IO;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Xml.Linq;

namespace Anritsu.AnrCommon.CoreLib
{
    public static class WebUtility
    {
        private const string Whitelistcachekey = "WhiteList";

        public static void HttpRedirectWithUpdatedQueryStrings(Control control, string newQueryStrings)
        {
            var pageName = HttpContext.Current.Request.Path;
            var newUrl = string.Format("{0}{1}{2}", pageName, (newQueryStrings.StartsWith("?") ? "" : "?"),
                newQueryStrings);
            HttpRedirect(control, newUrl);
        }

        public static void HttpRedirect(Control control, string redirectUrl)
        {
            // the reason for using this overload of Response.Redirect:
            //  Response.Redirect("somepage.aspx", false);
            // as opposed to this
            //  Response.Redirect("somepage.aspx");
            // is that by default true is used which causes Response.End to be invoked automatically
            // this can increase the number of ThreadWasAboutToAbortExceptions and impact performance
            // in some scenarios
            // however using the overlaod with 2 params also allows the current thread to continue processing
            // after redirection which is not neccessarily desireable and can have some security implications.
            // If using redirection to enforce security, like redirecting to the  AccessDenied page, it is best to always
            // use plain old Response.Redirect("somepage.aspx");
            // so that the response is ended immediately.
            // When redirecting after an update maximum performance can be achieved using
            // Response.Redirect("somepage.aspx", false);
            // You should call SiteUtils.UseGracefulThreadEnding after your redirect using
            // Response.Redirect("somepage.aspx", false);
            // in order to gracefully end processing in the most secure fashion possible

            // for more info about the above see
            // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnaspp/html/monitor_perf.asp

            if (redirectUrl == null ||
                (!redirectUrl.StartsWith("/") && !redirectUrl.StartsWith("~/") &&
                 !IsWhiteListHost(new Uri(redirectUrl).Host))) return;
            HttpContext.Current.Response.Redirect(redirectUrl, false);

            if (control != null)
            {
                control.EnableViewState = false;
                control.Visible = false;
            }

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.End();
            return;
        }

        public static string RemoveQueryString(string key)
        {
            if (HttpContext.Current == null) return string.Empty;
            var qs = new NameValueCollection {HttpContext.Current.Request.QueryString};
            if (qs.AllKeys.Contains(key))
                qs.Remove(key);
            return ConvertUtility.ConvertToQueryString(qs);
        }

        public static string UpdateQueryString(string key, string newValue)
        {
            if (HttpContext.Current == null) return string.Empty;
            var qs = new NameValueCollection {HttpContext.Current.Request.QueryString};
            var updated = false;
            if (qs.AllKeys.Contains(key))
            {
                qs[key] = newValue;
                updated = true;
            }
            if (!updated) qs.Add(key, newValue);
            return ConvertUtility.ConvertToQueryString(qs);
        }

        public static bool IsPhysicalFileExists(IRouteHandler rh)
        {
            var prh = rh as PageRouteHandler;
            return prh != null && File.Exists(HttpContext.Current.Server.MapPath(prh.VirtualPath));
        }

        public static void AddToRoute(RouteCollection routes, string pageUrl, string physicalFile)
        {
            if (string.IsNullOrEmpty(pageUrl) || string.IsNullOrEmpty(physicalFile)) return;
            pageUrl = pageUrl.ToLower();

            var routeUrl = pageUrl.Replace("~/", string.Empty);
            if (routeUrl.StartsWith("/")) routeUrl = routeUrl.Remove(0, 1);

            var routeName = "DBROUTE_" + routeUrl;
            routes.MapPageRoute(routeName, routeUrl, physicalFile, false);
        }

        public static void RemoveRoute(RouteCollection routes, string oldPageUrl)
        {
            if (string.IsNullOrEmpty(oldPageUrl)) return;
            var routesToRemove = (from Route rt in routes
                let pageUrl = "~/" + rt.Url
                where oldPageUrl.Equals(pageUrl, StringComparison.InvariantCultureIgnoreCase)
                select rt).ToList();

            foreach (var rm in routesToRemove)
                routes.Remove(rm);
        }

        public static HttpWebResponse MakeWebRequest(Uri url, string httpMethod, string dataToPost, string putFilePath,
            string proxy, string username, string pwd, string userAgent)
        {
            var cookieJar = new CookieContainer();

            // this function will try to make a connection and then fill out the 
            // different controls with their correct values based on the request
            // it will return a false if it fails.

            // declare a WebProxy class object to use.

            //try catch block to catch any errors
            try
            {
                //check to see if we need to set a proxy
                if (!string.IsNullOrEmpty(proxy))
                {
                    if (!proxy.Contains(":")) proxy = proxy + ":80";
                    var proxyObject = new WebProxy(proxy, true); // the true is to bypass local address
                    GlobalProxySelection.Select = proxyObject;
                }
                //Create a new request
                var httpWRequest = (HttpWebRequest) WebRequest.Create(url);
                // set the HttpWebRequest objects cookie container
                // if you have any cookies that you want to go with the request you can add them 
                // to the cookiecontainer. If you had made a previous request that returned any cookies
                // that needed to be sent on subsequent request this will make sure that they are sent. 
                httpWRequest.CookieContainer = cookieJar;
                // check to see if the user added user name and password for Basic authentication.
                // you can also use digest and Kerbeors authentication
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(pwd))
                    // we will add the user and password for basic auth.
                {
                    var myCred = new NetworkCredential(username, pwd);
                    var myCrendentialCache = new CredentialCache {{url, "Basic", myCred}};
                    httpWRequest.Credentials = myCrendentialCache;
                }
                else
                    //Set the default Credentials. This will allow NTLM or Kerbeors authentication with out prompting the user
                {
                    // the default credentials are usually the Windows credentials (user name, password, and domain) of the user running the application
                    httpWRequest.Credentials = CredentialCache.DefaultCredentials;
                }
                // set the name of the user agent. This is the client name that is passed to IIS
                if (string.IsNullOrEmpty(userAgent))
                    userAgent = "Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20100101 Firefox/6.0";
                httpWRequest.UserAgent = userAgent;
                // set the connection keep-alive
                httpWRequest.KeepAlive = true; //this is the default
                //we don't want caching to take place so we need
                // to set the pragma header to say we don't want caching
                httpWRequest.Headers.Set("Pragma", "no-cache");
                //set the request timeout to 5 min.
                httpWRequest.Timeout = 300000;
                // set the request method
                httpWRequest.Method = httpMethod;
                // See what the Method is a POST 
                if ("POST" == httpMethod)
                {
                    // add the content type so we can handle form data
                    httpWRequest.ContentType = "application/x-www-form-urlencoded";
                    if (string.IsNullOrEmpty(dataToPost)) // we don't have any data to post
                    {
                        throw new ArgumentNullException("dataToPost");
                    }
                    // we need to store the data into a byte array
                    var postData = Encoding.ASCII.GetBytes(dataToPost);
                    httpWRequest.ContentLength = postData.Length;
                    var tempStream = httpWRequest.GetRequestStream();
                    // write the data to be posted to the Request Stream
                    tempStream.Write(postData, 0, postData.Length);
                    tempStream.Close();
                }
                if ("PUT" == httpMethod) //we want to put a file
                {
                    if (string.IsNullOrEmpty(putFilePath))
                    {
                        throw new ArgumentNullException("putFilePath");
                    }
                    //open the file to put
                    var readIn = new FileStream(putFilePath, FileMode.Open, FileAccess.Read);
                    readIn.Seek(0, SeekOrigin.Begin); // move to the start of the file
                    var fileData = new byte[readIn.Length];
                    readIn.Read(fileData, 0, (int) readIn.Length); // read the file data
                    httpWRequest.ContentLength = readIn.Length;
                    var tempStream = httpWRequest.GetRequestStream();
                    // place the file data in the Request Stream to send to the server
                    // for ease of use we just read the whole file into on large byte array to send.
                    tempStream.Write(fileData, 0, (int) readIn.Length);
                    tempStream.Close();
                    readIn.Close();
                }

                //get the response. This is where we make the connection to the server
                return (HttpWebResponse) httpWRequest.GetResponse();
                // Fill out the data on the Response Header Tab
                // check for headers. We don't have the CRLF vs non CRLF headers
                // we get a headers collection. However the following code
                // will output all the Headers in the collection.
                // note they next line of code may be oblolete check when we release. 
                //txtReturnHeaders.Text = HttpWResponse.Headers.ToString();
                ////Get the content Type 
                //txtContentType.Text = HttpWResponse.ContentType.ToString();
                ////Get the Content Length 
                //txtContentLen.Text = HttpWResponse.ContentLength.ToString();
                ////Get the Request Method
                //txtRespMethod.Text = HttpWResponse.Method.ToString();
                //// Get the Status code
                //int iStatCode = (int)HttpWResponse.StatusCode;
                //txtStatusCode.Text = iStatCode.ToString();
                //// Get last modified
                //txtLastMod.Text = HttpWResponse.LastModified.ToLongDateString();
                //// Get HTTP version
                //txtVersion.Text = HttpWResponse.ProtocolVersion.ToString();
                //// Get the status text
                //txtText.Text = HttpWResponse.StatusCode.ToString();
                //// Get what the server is
                //txtServer.Text = HttpWResponse.Server.ToString();
                //// Get the Content Encoding if any
                //txtEncoding.Text = HttpWResponse.ContentEncoding.ToString();
                //// write the request info to the controls on the
                //// Request Header tab
                //// Get the request headers
                //txtReqHeaders.Text = HttpWRequest.Headers.ToString();
                //// Get the request method
                //txtReqMethod.Text = HttpWRequest.Method.ToString();
                //// Get the user agent name
                //txtUserAgent.Text = HttpWRequest.UserAgent.ToString();
                //// Get any request cookies
                //txtReqCookies.Text = HttpWRequest.CookieContainer.GetCookieHeader(URL).ToString();
                // you can check to see if any cookies were returned using a cookiecollection. It is the developer's job
                // to persist any cookies that need to be persisted see Serialization in online help
                // example to get the return cookies. 
                /*
                 CookieCollection cookies = Response.Cookies;
                Debug.WriteLine("cookie count: " + cookies.Count.ToString());
                // you could use a for loop to loop through the cookies
                Debug.WriteLine("cookie name: " + cookies[0].Name.ToString());
                Debug.WriteLine("cookie expires: " + cookies[0].Expires.ToString());
                Debug.WriteLine("cookie path: " + cookies[0].Path.ToString());
                Debug.WriteLine("cookie domain: " + cookies[0].Domain.ToString());
                Debug.WriteLine("cookie value: " + cookies[0].Value.ToString());
                 */

            }
            catch (WebException webExcp)
            {
                //MessageBox.Show(WebExcp.Message.ToString());
                //ICertificatePolicy CertPolicy = ServicePointManager.CertificatePolicy;
                throw webExcp;
            }
            catch (Exception e) // get any other error
            {
                //MessageBox.Show(e.Message.ToString());
                //return false;
                throw e;
            }

        }

        public static string ReadWebResponseString(HttpWebResponse resp, Encoding enc)
        {
            if (enc == null) enc = Encoding.UTF8;
            var str = "";
            using (var loResponseStream = new StreamReader(resp.GetResponseStream(), enc))
            {
                str = loResponseStream.ReadToEnd();
                loResponseStream.Close();
                resp.Close();
            }
            return str;
        }

        public static string GetHostName()
        {
            if (HttpContext.Current == null) return String.Empty;
            var hostname = HttpContext.Current.Items["hostname"] as string;
            if (hostname != null) return hostname;
            hostname = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            if (hostname != null)
                HttpContext.Current.Items["hostname"] = hostname;
            return hostname;
        }

        public static string GetVirtualRoot()
        {
            var serverName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            return "/" + serverName + GetApplicationRoot();
        }

        public static string GetApplicationRoot()
        {
            if (HttpContext.Current == null) return String.Empty;

            var applicationRoot = HttpContext.Current.Items["applicationRoot"] as string;
            if (applicationRoot != null) return applicationRoot;
            applicationRoot = CalculateApplicationRoot();
            if (applicationRoot != null)
                HttpContext.Current.Items["applicationRoot"] = applicationRoot;
            return applicationRoot;
        }

        public static string GetAbsoluteUrl(string relativeUrl)
        {
            if (HttpContext.Current == null) return String.Empty;
            return relativeUrl.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)
                ? relativeUrl
                : new Uri(HttpContext.Current.Request.Url, VirtualPathUtility.ToAbsolute(relativeUrl)).ToString();
            //return VirtualPathUtility.ToAbsolute(relativeUrl);
        }

        public static String GetUserIP()
        {
            var cxt = HttpContext.Current;
            if (cxt == null) return String.Empty;

            var ip = cxt.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            // If there is no proxy, get the standard remote address
            if (String.IsNullOrEmpty(ip) || (ip.Equals("unknown", StringComparison.InvariantCultureIgnoreCase)))
                ip = cxt.Request.ServerVariables["REMOTE_ADDR"];

            if (String.IsNullOrEmpty(ip)) ip = cxt.Request.UserHostAddress;

            if (!String.IsNullOrEmpty(cxt.Request["ipaddress"]) &&
                cxt.Request["iptest_token"] == ConfigUtility.AppSettingGetValue("GeoLib.iptest_token"))
            {
                ip = cxt.Request["ipaddress"];
            }
            if (ip == "::1") ip = "127.0.0.1";
            return ip;
        }

        private static string CalculateApplicationRoot()
        {
            return HttpContext.Current.Request.ApplicationPath.Length == 1
                ? string.Empty
                : HttpContext.Current.Request.ApplicationPath;
        }

        public static void DisableBrowserCache()
        {
            if (HttpContext.Current == null) return;
            HttpContext.Current.Response.Cache.SetExpires(new DateTime(1995, 5, 6, 12, 0, 0, DateTimeKind.Utc));
            HttpContext.Current.Response.Cache.SetNoStore();
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.AppendCacheExtension("post-check=0,pre-check=0");
        }

        public static void DisableDownloadCache()
        {
            HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));

            // no-store makes firefox reload page
            // no-cache makes firefox reload page only over SSL
            // IE will fail when downloading a file over SSL if no-store or no-cache is set
            var oBrowser
                = HttpContext.Current.Request.Browser;

            if (!oBrowser.Browser.ToLower().Contains("ie"))
            {
                HttpContext.Current.Response.Cache.SetNoStore();
            }

            //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // private cache allows IE to download over SSL with no-store set. 
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.AppendCacheExtension("post-check=0,pre-check=0");

        }

        public static void SetClientCaching(HttpContext context, DateTime lastModified, TimeSpan maxAge)
        {
            if (context == null) return;

            context.Response.Cache.SetETag(lastModified.Ticks.ToString(CultureInfo.InvariantCulture));
            context.Response.Cache.SetLastModified(lastModified.ToLocalTime());
            context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            //context.Response.Cache.SetMaxAge(new TimeSpan(7, 0, 0, 0));
            context.Response.Cache.SetMaxAge(maxAge);
            context.Response.Cache.SetSlidingExpiration(true);
        }

        public static void SetFileCaching(HttpContext context, string fileName, TimeSpan maxAge)
        {
            if (fileName == null) return;
            if (context == null) return;
            if (fileName.Length == 0) return;

            context.Response.AddFileDependency(fileName);
            context.Response.Cache.SetETagFromFileDependencies();
            context.Response.Cache.SetLastModifiedFromFileDependencies();
            context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            //context.Response.Cache.SetMaxAge(new TimeSpan(7, 0, 0, 0));
            context.Response.Cache.SetMaxAge(maxAge);
            context.Response.Cache.SetSlidingExpiration(true);
        }

        public static String ExtractCountryCodeFromRequest()
        {
            var countryCode = string.Empty;
            var cxt = HttpContext.Current;
            if (cxt == null) return String.Empty;

            if (cxt.Request.UserLanguages == null || cxt.Request.UserLanguages.Length <= 0) return countryCode;

            #region " first browser lang [ll-CC] "

            foreach (var browseLang in cxt.Request.UserLanguages)
            {
                if (browseLang.Contains("-") && !browseLang.EndsWith("-"))
                {
                    countryCode = browseLang.Substring(browseLang.IndexOf("-", StringComparison.Ordinal) + 1);
                    if (countryCode.Contains(";"))
                        countryCode = countryCode.Substring(0, countryCode.IndexOf(";", StringComparison.Ordinal));
                }
                if (!string.IsNullOrEmpty(countryCode)) break;
            }

            #endregion

            return countryCode;
        }

        public static void DoPost(String formName, String method, String url, NameValueCollection inputValues)
        {
            HttpContext.Current.Response.Clear();
            var formToPost = new StringBuilder();
            formToPost.Append("<html><head>");
            formToPost.AppendFormat("</head><body onload=\"document.{0}.submit()\">", formName);
            formToPost.AppendFormat("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >",
                formName, method, url);
            if (inputValues != null)
            {
                for (int i = 0; i < inputValues.Keys.Count; i++)
                {
                    formToPost.AppendFormat("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">"
                        , inputValues.Keys[i], inputValues[inputValues.Keys[i]]);
                }
            }
            formToPost.Append("</form>");
            formToPost.Append("</body></html>");
            HttpContext.Current.Response.Write(formToPost.ToString());
            HttpContext.Current.Response.End();
        }

        public static void GenerateProcessingScript(WebControl webCtrl, string clientEvent, string processingText)
        {
            if (processingText.IsNullOrEmptyString()) processingText = "Processing..";
            webCtrl.Attributes[clientEvent] = string.Format("{0}.disabled=true;{0}.value='{1}';{2};"
                , webCtrl.ClientID
                , processingText
                , webCtrl.Page.ClientScript.GetPostBackEventReference(webCtrl, string.Empty));
        }

        public static void GenerateProcessingScript(WebControl webCtrl, string clientEvent, string processingText,
            List<string> clientIdsToDisable)
        {
            if (processingText.IsNullOrEmptyString()) processingText = "Processing..";
            var script = new StringBuilder();
            script.AppendFormat("{0}.disabled=true;{0}.value='{1}';", webCtrl.ClientID, processingText);
            if (clientIdsToDisable != null)
            {
                foreach (string cid in clientIdsToDisable)
                {
                    script.AppendFormat("{0}.disabled=true;", cid);
                }
            }

            webCtrl.Attributes[clientEvent] = string.Format("{0}{1};"
                , script
                , webCtrl.Page.ClientScript.GetPostBackEventReference(webCtrl, string.Empty));
        }

        // for resetpasssword page
        public static T ReadRequest<T>(string requestName, bool isQueryStringOnly) where T : IConvertible
        {
            var reqValue = string.Empty;
            var context = HttpContext.Current;

            if (isQueryStringOnly)
            {
                if (!string.IsNullOrEmpty(context.Request.QueryString[requestName]))
                    reqValue = context.Request.QueryString[requestName];
            }
            else
            {
                if (!string.IsNullOrEmpty(context.Request[requestName]))
                    reqValue = context.Request[requestName];
            }

            if (reqValue.Trim() != string.Empty)
                return (T) Convert.ChangeType(reqValue.Trim(), typeof (T));
            return default(T);
        }

        public static string ReadRequest(string requestName, bool isQueryStringOnly)
        {
            return ReadRequest<string>(requestName, isQueryStringOnly);
        }

        public static bool IsWhiteListHost(string hostName)
        {
            var isWhiteListUrl = false;
            try
            {
                var hostlist = GetWhiteListfromCache();
                if (hostlist != null && hostlist.Count > 0)
                {
                    isWhiteListUrl = hostlist.Contains(hostName);
                }
            }
            catch (Exception)
            {
            }
            return isWhiteListUrl;
        }

        private static List<string> GetWhiteListfromCache()
        {
            const string cachefile = "~/App_Data/WhitelistUrl.config";
            var hostlist = new List<string>();
            var cache = HttpContext.Current.Cache;

            try
            {
                if (cache[Whitelistcachekey] == null)
                {
                    var elements = XElement.Load(HttpContext.Current.Server.MapPath(cachefile));
                    hostlist = (from c in elements.Elements("add")
                        where (string) c.Attribute("Isactive") == "true"
                        select c.Attribute("url").Value).ToList<string>();
                    cache.Insert(Whitelistcachekey, hostlist,
                        new CacheDependency(HttpContext.Current.Server.MapPath(cachefile)), Cache.NoAbsoluteExpiration,
                        Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                }
                else
                {
                    hostlist = (List<string>) cache[Whitelistcachekey];
                }
            }
            catch (Exception)
            {
            }
            return hostlist;
        }
    }
}
