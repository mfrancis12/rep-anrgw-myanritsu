﻿using System;
using System.Configuration;

namespace Anritsu.AnrCommon.EmailSDK
{
    internal static class EmailServiceFactory
    {
        private static string _emailServiceToken = string.Empty;
        private static string _emailServiceEndpointUrl = string.Empty;

        public static string EmailServiceToken
        {
            get
            {
                if (!string.IsNullOrEmpty(_emailServiceToken)) return _emailServiceToken;
                _emailServiceToken = ConfigurationManager.AppSettings["EmailSDK_ServiceToken"];
                if (string.IsNullOrEmpty(_emailServiceToken))
                    throw new ArgumentException("Invalid Email service token.");
                return _emailServiceToken;
            }
        }

        public static string EmailServiceEndpointURL
        {
            get
            {
                if (!string.IsNullOrEmpty(_emailServiceEndpointUrl)) return _emailServiceEndpointUrl;
                _emailServiceEndpointUrl = ConfigurationManager.AppSettings["EmailSDK_ServiceEndpointURL"];
                if (string.IsNullOrEmpty(_emailServiceEndpointUrl))
                    throw new ArgumentException("Invalid Email service endpoint url.");
                return _emailServiceEndpointUrl;
            }
        }

        static EmailServiceFactory()
        {
            _emailServiceToken = ConfigurationManager.AppSettings["EmailSDK_ServiceToken"];
        }

        public static EmailWebRef.AnritsuEmailwebservice GetService()
        {
            var ws = new EmailWebRef.AnritsuEmailwebservice
            {
                Url = EmailServiceEndpointURL,
                GWApiHeaderTypeValue = new EmailWebRef.GWApiHeaderType {SToken = EmailServiceToken}
            };
            return ws;
        }
    }
}
