﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Caching;

namespace ApiSdk
{
    public abstract class BaseAppCacheFactory
    {

        public virtual Object TryRetrieveDataFromCache(String cacheKey)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return null;
            return context.Cache.Get(cacheKey);
        }

        protected virtual void SetCacheData(String cacheKey, Object data)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;

            if (data == null) context.Cache.Remove(cacheKey);
            else
            {
                int expTime = ConfigurationManager.AppSettings["CacheExpireTime"] == null
                    ? 10
                    : Convert.ToInt32(ConfigurationManager.AppSettings["CacheExpireTime"]);
                context.Cache.Add(cacheKey, data, null, DateTime.Now.AddMinutes(expTime), Cache.NoSlidingExpiration, CacheItemPriority.High, null);
                //   context.Cache.Insert(cacheKey, data, null, DateTime.Now.AddMinutes(ConfigurationManager.AppSettings["CacheExpireTime"]==null?10:Convert.ToDouble(ConfigurationManager.AppSettings["CacheTime"])), Cache.NoSlidingExpiration);
            }
        }
    }
}