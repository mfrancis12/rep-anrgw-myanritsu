﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace ApiSdk
{
    public class ResponseClass
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public object Content { get; set; }

        public ResponseClass CallMethod<T>(Uri apiUri, Dictionary<string, string> param, bool cache = true, Object logger = null)
        {
            var response = CallApi(apiUri, param, "Get", cache, logger);
            StatusCode = response.StatusCode;
            var result = response.Content.ReadAsStringAsync().Result;
            if (typeof(T) == typeof(string))
            {
                Content = result;
            }
            else
            {
                var ob = JsonConvert.DeserializeObject(result);
                var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects };
                var json = JsonConvert.SerializeObject(ob, Formatting.Indented, settings);
                var tb = JsonConvert.DeserializeObject<T>(json);
                Content = tb;
            }
            return this;
        }

        public HttpResponseMessage CallApi(Uri apiUri, Dictionary<string, string> param, string type, bool cache = true, Object logger = null)
        {
            var cacheFactory = new ApiCacheFactory();
            var urlparams = new StringBuilder();
            var uri = string.Empty;
            if (param.Count > 0)
            {
                if (!urlparams.ToString().Contains("?"))
                    urlparams.Append("?");

                foreach (var key in param)
                {
                    urlparams.Append(key.Key);
                    urlparams.Append("=");
                    urlparams.Append(key.Value);
                    urlparams.Append("&");
                }

                uri = urlparams.ToString().Substring(0, urlparams.ToString().LastIndexOf("&", StringComparison.Ordinal));

            }
            var cacheKey = String.Format("{0}{1}", apiUri.ToString(), uri.ToString(CultureInfo.InvariantCulture));
            if (cache)
            {
                var cacheData = cacheFactory.GetCacheData(cacheKey) as HttpContent;
                if (cacheData != null)
                {
                    HttpResponseMessage resmsg = new HttpResponseMessage(HttpStatusCode.OK);
                    resmsg.Content = cacheData;
                    return resmsg;
                }
            }

            var client = new HttpClient { BaseAddress = apiUri };

            try
            {

                if (type.Equals("POST"))
                {
                    client.DefaultRequestHeaders.Accept.Add(
                  new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    var postData = param.Select(key => new KeyValuePair<string, string>(key.Key, key.Value)).ToList();

                    HttpContent content = new FormUrlEncodedContent(postData);
                    if (logger != null)
                        AppLogUtility.InitRequestLog(logger, HttpContext.Current.Request.RawUrl, apiUri.ToString(), urlparams.ToString(), logger.GetType().GetProperty("RequestMethod").GetValue(logger).ToString(), null, "Request Initiated");
                    var time = new TimeSpan(0, 0, 10, 0);
                    client.Timeout = time;
                    var response = client.PostAsync(apiUri, content).Result;
                    if (logger != null)
                        AppLogUtility.EndRequestLog(logger, HttpContext.Current.Request.RawUrl, apiUri.ToString(), urlparams.ToString(), logger.GetType().GetProperty("RequestMethod").GetValue(logger).ToString(), null, "Request End");
                    return response;
                }
                else
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (logger != null)
                        AppLogUtility.InitRequestLog(logger, HttpContext.Current.Request.RawUrl, apiUri.ToString(), urlparams.ToString(), logger.GetType().GetProperty("RequestMethod").GetValue(logger).ToString(), null, "Request Initiated");
                    var response = client.GetAsync(uri).Result;
                    if (logger != null)
                        AppLogUtility.EndRequestLog(logger, HttpContext.Current.Request.RawUrl, apiUri.ToString(), urlparams.ToString(), logger.GetType().GetProperty("RequestMethod").GetValue(logger).ToString(), null, "Request End");
                    if (cache)
                    {
                        cacheFactory.SetCachedData(cacheKey, response.Content);
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                if (logger != null)
                    AppLogUtility.EndRequestLog(logger, HttpContext.Current.Request.RawUrl, apiUri.ToString(), urlparams.ToString(), logger.GetType().GetProperty("RequestMethod").GetValue(logger).ToString(), null, "Request terminated due to Exception:"+ex.Message+"Stacktrace:"+ex.StackTrace);
                throw ex;
            }
        }

        public ResponseClass CallMethodList<T>(Uri apiUri, Dictionary<string, string> param)
        {
            var response = CallApi(apiUri, param, "Get");
            StatusCode = response.StatusCode;
            var ob = JsonConvert.DeserializeObject(response.ToString());
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Arrays };
            var json = JsonConvert.SerializeObject(ob, Formatting.Indented, settings);
            var tb = JsonConvert.DeserializeObject<List<T>>(json);
            Content = tb;
            return this;
        }
        public HttpResponseMessage PostMethodList<T>(Uri apiUri, Dictionary<string, string> param)
        {
            var response = CallApi(apiUri, param, "POST", false);
            StatusCode = response.StatusCode;
            return response;
        }
    }


}
