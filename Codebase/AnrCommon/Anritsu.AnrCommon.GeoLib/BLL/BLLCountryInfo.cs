﻿using System;
using System.Data;

namespace Anritsu.AnrCommon.GeoLib.BLL
{
    public static class BLLCountryInfo
    {
        public static DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId)
        {
            var da = new DAL.DACountryInfo();
            return da.Country_SelectByCultureGroupId(cultureGroupId);
        }

        public static DataTable Country_SelectByCountryCode(String countryCode)
        {
            var da = new DAL.DACountryInfo();
            return da.Country_SelectByCultureGroupId(countryCode);
        }

        public static DataRow Country_ISOInfo_SelectByCountryCode(String countryCode)
        {
            var da = new DAL.DACountryInfo();
            return da.Country_ISOInfo_SelectByCountryCode(countryCode);
        }

        public static DataTable State_SelectByCountryCode(String countryCode)
        {
            var da = new DAL.DACountryInfo();
            return da.State_SelectByCountryCode(countryCode);
        }
    }
}
