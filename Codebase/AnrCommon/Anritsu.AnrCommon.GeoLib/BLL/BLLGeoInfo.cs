﻿using System;
using System.Data;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.GeoLib.BLL
{
    public static class BLLGeoInfo
    {
        private static GeoInfo InitFromData(DataRow r, String ipv4)
        {
            if (r == null) return null;
            var obj = new GeoInfo
            {
                IPAddress = ipv4,
                CountryCode = ConvertUtility.ConvertNullToEmptyString(r["ISOCountryCode"]),
                CountryName = ConvertUtility.ConvertNullToEmptyString(r["CountryName"])
            };

            var dac = new DAL.DACountryInfo();
            var rc = dac.Country_ISOInfo_SelectByCountryCode(obj.CountryCode);
            if (rc == null)
            {
                obj.Region_CultureGroupId = 1;
                obj.Region_CultureCode = "en-US";
            }
            else
            {
                obj.Region_CultureGroupId = ConvertUtility.ConvertToInt32(rc["DefaultCultureGroupId"], 1);
                obj.Region_CultureCode = ConvertUtility.ConvertNullToEmptyString(rc["CultureCode"]);
                if (String.IsNullOrEmpty(obj.Region_CultureCode)) obj.Region_CultureCode = "en-US";
            }
            return obj;
        }

        public static GeoInfo GetCountryInfoFromIP4(String ipv4)
        {
            var da = new DAL.DAGeoInfo();
            return InitFromData(da.GetCountryFromIPv4(ipv4), ipv4);
        }
        public static string GetRegionByCountryCode(string countryCode)
        {
            var da = new DAL.DAGeoInfo();
            DataRow dr = da.GetRegionByCountryCode(countryCode);
            return (null != dr) ? dr["Region"].ToString() : string.Empty;

        }
        public static GeoInfo GetCountryInfoFromIP4()
        {
            var cxt = HttpContext.Current;
            if (cxt == null) return null;
            var ipAddr = WebUtility.GetUserIP(); // cxt.Request.UserHostAddress;

            return GetCountryInfoFromIP4(ipAddr);
        }
    }
}
