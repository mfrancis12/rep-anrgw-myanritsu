﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.GeoLib.DAL
{
    internal sealed class DACountryInfo : DBGlobalWeb
    {
        public DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId)
        {
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                return Country_SelectByCultureGroupId(cultureGroupId, conn);
            }
        }

        public DataTable Country_SelectByCultureGroupId(String countryCode)
        {
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                var r = Country_ISOInfo_SelectByCountryCode(countryCode);
                if (r == null) return null;
                var cultureGroupId = ConvertUtility.ConvertToInt32(r["DefaultCultureGroupId"], 1);
                return Country_SelectByCultureGroupId(cultureGroupId, conn);
            }
        }

        public DataRow Country_ISOInfo_SelectByCountryCode(String countryCode)
        {
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                return Country_ISOInfo_SelectByCountryCode(countryCode, conn);
            }
        }

        private DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId, SqlConnection conn)
        {
            var cmd = SQLUtility.MakeSPCmd(conn, null
                , SP_Schema
                , StrDef.SP.Country_SelectByCultureGroupId
                , CommandTimeout);
            cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
            return SQLUtility.FillInDataTable(cmd);
        }

        private DataRow Country_ISOInfo_SelectByCountryCode(String countryCode, SqlConnection conn)
        {
            var cmd = SQLUtility.MakeSPCmd(conn, null
                , SP_Schema
                , StrDef.SP.Country_SelectByCountryAlpha2Code
                , CommandTimeout);
            cmd.Parameters.AddWithValue("@Alpha2Code", countryCode);
            return SQLUtility.FillInDataRow(cmd);
        }

        public DataTable State_SelectByCountryCode(String countryCode)
        {
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                var cmd = SQLUtility.MakeSPCmd(conn, null
                    , SP_Schema
                    , StrDef.SP.State_SelectByCountryAlpha2Code
                    , CommandTimeout);
                cmd.Parameters.AddWithValue("@CountryValue", countryCode);

                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
