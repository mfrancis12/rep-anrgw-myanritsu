﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.GeoLib.DAL
{
    internal sealed class DAGeoInfo : DBGeoIP
    {
        public  DataRow GetRegionByCountryCode(string countryCode)
        {
            if (countryCode.IsNullOrEmptyString()) return null;
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                //SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GeoIPDB.GeoIP.IPCountryRegion_GetByCountryCode);
                //cmd.Parameters.AddWithValue("@CountryCode", countryCode);

                var cmd = SQLUtility.MakeSPCmd(conn, null
                   , SP_Schema
                   , StrDef.SP.IPCountryRegion_GetByCountryCode
                   , CommandTimeout);
                cmd.Parameters.AddWithValue("@CountryCode", countryCode);

                return SQLUtility.FillInDataRow(cmd);
            }
        }
        public DataRow GetCountryFromIPv4(String ipv4)
        {
            using (var conn = SQLUtility.ConnOpen(ConnStr))
            {
                var cmd = SQLUtility.MakeSPCmd(conn, null
                    , SP_Schema
                    , StrDef.SP.IPCountryLocation_GetByIP
                    , CommandTimeout);
                cmd.Parameters.AddWithValue("@IPAddress", ConvertUtility.ConvertNullToEmptyString(ipv4).Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }
    }
}
