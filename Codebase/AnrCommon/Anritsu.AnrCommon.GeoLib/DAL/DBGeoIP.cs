﻿using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.GeoLib.DAL
{
    internal abstract class DBGeoIP
    {
        internal class StrDef
        {
            public const string DB_CONNSTR = "GeoLib.DB.GeoIP.ConnStr";
            public const string DB_SCHEMA = "GeoLib.DB.GeoIP.Schema";
            public const string DB_CMDTIMEOUT = "GeoLib.DB.GeoIP.CmdTimeout";

            public class SP
            {
                public const string IPCountryLocation_GetByIP = "uSP_IPCountryLocation_GetByIP";
                public const string IPCountryRegion_GetByCountryCode = "uSP_IPCountryRegion_GetByCountryCode";

            }
        }

        protected virtual string SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue(StrDef.DB_SCHEMA); }
        }

        protected virtual string ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue(StrDef.DB_CONNSTR); }
        }

        protected virtual int CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue(StrDef.DB_CMDTIMEOUT), out timeout);
                return timeout;
            }
        }

    }
}
