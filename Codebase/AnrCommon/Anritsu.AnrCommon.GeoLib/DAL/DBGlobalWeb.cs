﻿using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.AnrCommon.GeoLib.DAL
{
    internal abstract class DBGlobalWeb
    {
        internal class StrDef
        {
            public const string DB_CONNSTR = "GeoLib.DB.GlobalWeb.ConnStr";
            public const string DB_SCHEMA = "GeoLib.DB.GlobalWeb.Schema";
            public const string DB_CMDTIMEOUT = "GeoLib.DB.GlobalWeb.CmdTimeout";

            public class SP
            {
                public const string Country_SelectByCultureGroupId = "uSP_ContactUs_Countries_Get";
                public const string Country_SelectByCountryAlpha2Code = "usp_ar_iso_country_select_byalpha2code";
                public const string State_SelectByCountryAlpha2Code = "uSP_ContactUs_States_Get";
            }
        }

        protected virtual string SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue(StrDef.DB_SCHEMA); }
        }

        protected virtual string ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue(StrDef.DB_CONNSTR); }
        }

        protected virtual int CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue(StrDef.DB_CMDTIMEOUT), out timeout);
                return timeout;
            }
        }

    }
}
