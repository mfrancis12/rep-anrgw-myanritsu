﻿using System;

namespace Anritsu.AnrCommon.GeoLib
{
    public class GeoInfo
    {
        public String IPAddress { get; set; }
        public String CountryCode { get; set; }
        public String CountryName { get; set; }
        public Int32 Region_CultureGroupId { get; set; }
        public String Region_CultureCode { get; set; }
    }
}
