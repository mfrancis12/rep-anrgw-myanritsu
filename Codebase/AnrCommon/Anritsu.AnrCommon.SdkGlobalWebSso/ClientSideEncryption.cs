﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Security;
using System.Security.Cryptography;
namespace Anritsu.AnrCommon.SdkGlobalWebSso
{
    //class ClientSideEncryption
    
    public class ClientSideEncryption
    {
        private const int MIN_KEY_LENGTH = 9;
        static private byte[] _Key = new Byte[8];
        static private byte[] _IV = new Byte[8];
        static private string _Keystring = string.Empty;//enter key //"E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C";

        public ClientSideEncryption(string sKEY)
        {
            _Keystring = sKEY;
        }
        public void SetEncryptionKeyByIntId(int intId)
        {
            string segment = "_" + intId.ToString();
            string key = segment;
            while (key.Length < MIN_KEY_LENGTH)
            {
                key += segment;
            }
            _Keystring = key;
        }
        /// <summary>
        /// Function to encrypt data
        /// string Length cannot exceed 92160 bytes.
        /// </summary>
        /// <param name="sStringToEncrypt"></param>
        /// <returns></returns>
        public string EncryptData(string sStringToEncrypt)
        {
            string mResultString;//Return Result
            //1. string Length cannot exceed 92160 bytes.
            //Otherwise, buffer will overflow.
            //See point 3 for reasons
            if (sStringToEncrypt.Length > 92160)
            {
                mResultString = "Error. Data string too large. Keep within 90Kb.";
                return mResultString;
            }
            //2. Generate the Keys
            if (!InitKey())
            {
                mResultString = "Error. Fail to generate key for encryption";
                return mResultString;
            }
            //3. Prepare the string
            //The first 5 character of the string is formatted to store
            //the actual length of the data.
            //This is the simplest way to remember to original length of the data,
            //without resorting to complicated computations.
            sStringToEncrypt = string.Format("{0,5:00000}" + sStringToEncrypt,
                     sStringToEncrypt.Length);
            //4. Encrypt the Data
            byte[] mDataBytes = new byte[sStringToEncrypt.Length];
            ASCIIEncoding mASCIIEnc = new ASCIIEncoding();
            //UTF8Encoding aEnc = new UTF8Encoding();
            mASCIIEnc.GetBytes(sStringToEncrypt, 0, sStringToEncrypt.Length, mDataBytes, 0);
            DESCryptoServiceProvider mDESProvider = new DESCryptoServiceProvider();
            ICryptoTransform mDESEncrypt = mDESProvider.CreateEncryptor(_Key, _IV);
            //5. Perpare the streams
            MemoryStream mInputStream = new MemoryStream(mDataBytes);
            CryptoStream mTransformStream = new CryptoStream(mInputStream,
                mDESEncrypt, CryptoStreamMode.Read);
            MemoryStream mOutputStream = new MemoryStream();
            //6. Start performing the encryption
            int mBytesRead;
            byte[] mOutputBytes = new byte[1024];
            do
            {
                mBytesRead = mTransformStream.Read(mOutputBytes, 0, 1024);
                if (mBytesRead != 0)
                    mOutputStream.Write(mOutputBytes, 0, mBytesRead);
            }
            while (mBytesRead > 0);
            //7. Returns the encrypted result after it is base64 encoded
            //In this case, the actual result is converted to base64 so that
            //it can be transported over the HTTP protocol without deformation.
            if (mOutputStream.Length == 0)
                mResultString = "";
            else
                mResultString = Convert.ToBase64String(mOutputStream.GetBuffer(),
                    0, (int)mOutputStream.Length);
            return mResultString;
        }
        /// <summary>
        /// Function to decrypt data
        /// </summary>
        /// <param name="sStringToEncrypt"></param>
        /// <returns></returns>
        public string DecryptData(string sStringToEncrypt)
        {
            string mResultString;
            //1. Generate the Key used for decrypting
            if (!InitKey())
            {
                mResultString = "Error. Fail to generate key for decryption.";
                return mResultString;
            }
            //2. Initialize the service provider
            int mReturn = 0;
            DESCryptoServiceProvider mDESProvider = new DESCryptoServiceProvider();
            ICryptoTransform mDESDecrypt = mDESProvider.CreateDecryptor(_Key, _IV);
            //3. Prepare the streams
            MemoryStream mOutputStream = new MemoryStream();
            CryptoStream mTransformStream = new CryptoStream(mOutputStream, mDESDecrypt,
                CryptoStreamMode.Write);
            //4. Remember to revert the base64 encoding into a byte array
            //to restore the original encrypted data stream
            byte[] mPlainBytes = new byte[sStringToEncrypt.Length];
            try
            {
                mPlainBytes = Convert.FromBase64CharArray(sStringToEncrypt.ToCharArray(),
                    0, sStringToEncrypt.Length);
            }
            catch (Exception)
            {
                mResultString = "Error. Input Data is not base64 encoded.";
                return mResultString;
            }
            long mRead = 0;
            long mTotal = sStringToEncrypt.Length;
            try
            {
                //5. Perform the actual decryption
                while (mTotal >= mRead)
                {
                    mTransformStream.Write(mPlainBytes, 0, (int)mPlainBytes.Length);
                    //descsp.BlockSize=64
                    mRead = mOutputStream.Length + Convert.ToUInt32(
                        ((mPlainBytes.Length / mDESProvider.BlockSize) *
                        mDESProvider.BlockSize));
                }
                ASCIIEncoding mASCIIEnc = new ASCIIEncoding();
                mResultString = mASCIIEnc.GetString(mOutputStream.GetBuffer(),
                    0, (int)mOutputStream.Length);
                //6. Trim the string to return only the meaningful data
                //Remember that in the encrypt function, the first 5 character
                //holds the length of the actual data. This is the simplest way
                //to remember to original length of the data, without resorting
                //to complicated computations.
                string mStringLength = mResultString.Substring(0, 5);
                int mLen = Convert.ToInt32(mStringLength);
                mResultString = mResultString.Substring(5, mLen);
                //strResult = strResult.Remove(0,5);
                mReturn = (int)mOutputStream.Length;
                return mResultString;
            }
            catch
            {
                mResultString = "Error. Decryption Failed. Possibly due to incorrect Key"
                    + " or corrputed data";
                return mResultString;
            }
        }
        /// <summary>
        /// Private function to generate the keys into member variables
        /// </summary>
        /// <returns></returns>
        static private bool InitKey()
        {
            try
            {
                // Convert Key to byte array
                byte[] mKeyBytes = new byte[_Keystring.Length];
                ASCIIEncoding mASCIIEnc = new ASCIIEncoding();
                mASCIIEnc.GetBytes(_Keystring, 0, _Keystring.Length, mKeyBytes, 0);
                //Hash the key using SHA1
                SHA1CryptoServiceProvider mSHAProvider = new SHA1CryptoServiceProvider();
                byte[] mHashBytes = mSHAProvider.ComputeHash(mKeyBytes);
                int i;
                // use the low 64-bits for the key value
                for (i = 0; i < 8; i++)
                    _Key[i] = mHashBytes[i];
                for (i = 8; i < 16; i++)
                    _IV[i - 8] = mHashBytes[i];
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
