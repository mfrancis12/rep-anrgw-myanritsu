﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using API = Anritsu.AnrCommon.SdkGlobalWebSso.ApiGlobalWebSso;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.AnrCommon.SdkGlobalWebSso
{
    public class SsoClient
    {
        private Guid _ServiceToken = Guid.Empty;
        private string _EncKey = string.Empty;

        private API.AnritsuSSOwebservice _Service;
        internal API.AnritsuSSOwebservice Service
        {
            get
            {
                if (_Service == null)
                {
                    _Service = new API.AnritsuSSOwebservice();
                    _Service.GWApiHeaderTypeValue = new API.GWApiHeaderType();
                    _Service.GWApiHeaderTypeValue.SToken = _ServiceToken.ToString();
                    _Service.GWApiHeaderTypeValue.CultureInfo = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
                    _Service.Timeout = 100000;
                    _Service.Proxy = null;
                    _Service.Url = ConfigUtility.AppSettingGetValue("SdkGlobalWebSso.EndpointUrl");
                }
                return _Service;
            }
        }
        
        public SsoClient()
        {
            _ServiceToken = new Guid(ConfigUtility.AppSettingGetValue("SdkGlobalWebSso.ServiceToken"));
            _EncKey = ConfigUtility.AppSettingGetValue("SdkGlobalWebSso.ServiceEncryptionKey");
        }

        public API.GWSsoUserType AuthenticateUserOLD(string emailAddress, string pwd, string clientIP)
        {
            API.AuthenticateUserCallRequestType req = new API.AuthenticateUserCallRequestType();
            req.ClientIP = clientIP;
            req.EmailAddress = emailAddress;

            ClientSideEncryption enc = new ClientSideEncryption(_EncKey);
            req.EncryptedPassword = enc.EncryptData(pwd);

            API.AuthenticateUserCallResponseType res = Service.AuthenticateUserCall(req);

            if (res == null)
                throw new ArgumentException("Please try again later.  system is down.");
            else if (res.StatusCode == 1) //user not found, log in failed.
                return null;
            else if (res.StatusCode == 2)
                throw new ArgumentException("ERROR: " + res.ResponseMessage);

            return res.GWSsoUser;
        }

        public API.GWSsoUserType AuthenticateUser(string emailAddress, string pwd, string clientIP, out int statusCode, out string resMsg)
        {
            statusCode = 2;
            resMsg = "";

            API.AuthenticateUserCallRequestType req = new API.AuthenticateUserCallRequestType();
            req.ClientIP = clientIP;
            req.EmailAddress = emailAddress;
            
            ClientSideEncryption enc = new ClientSideEncryption(_EncKey);
            req.EncryptedPassword = enc.EncryptData(pwd);

            API.AuthenticateUserCallResponseType res = Service.AuthenticateUserCall(req);
            
            if (res == null)
                throw new ArgumentException("Please try again later.  system is down.");
            else
            {
                statusCode = res.StatusCode;
                resMsg = res.ResponseMessage;
            }
            //else if (res.StatusCode == 1) //user not found, log in failed.
            //    return null;
            //else if (res.StatusCode == 2)
            //    throw new ArgumentException("ERROR: " + res.ResponseMessage);

            return res.GWSsoUser;
        }

        public API.GWSsoUserType GetUserByGWUserId(int gwUserId, string secretKey)
        {
            API.GetUserCallRequestType req = new API.GetUserCallRequestType();
            req.SecretKey = secretKey;
            req.UserId = gwUserId;
            API.GetUserCallResponseType res = Service.GetUserCall(req);

            CheckResponse(res);
            
            return res.GWSsoUser;
        }

        public API.GWSsoUserType GetUser(int userID)
        {
            API.SearchUsersCallRequestType req = new API.SearchUsersCallRequestType();
            req.UserID = userID;
            API.SearchUsersCallResponseType res = Service.SearchUsersCall(req);

            CheckResponse(res);

            if (res.Results == null || res.Results.Count<API.GWSsoUserType>() != 1)
                return null;
            else
                return res.Results.First<API.GWSsoUserType>();
        }

        public API.GWSsoUserType GetUserByEmail(String emailAddress)
        {
            API.SearchUsersCallRequestType req = new API.SearchUsersCallRequestType();
            req.Email = emailAddress.Trim();
            API.SearchUsersCallResponseType res = Service.SearchUsersCall(req);

            if (res == null || res.Results == null || res.Results.Length != 1) return null;
            return res.Results[0];
        }

        private void CheckResponse(API.BaseResponseType res)
        {
            if (res == null)
                throw new ArgumentException("Please try again later.  system is down.");
            else if (res.StatusCode != 0)
                throw new ArgumentException(res.ResponseMessage);
        }
    }
}
