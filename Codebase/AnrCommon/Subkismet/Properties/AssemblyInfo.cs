﻿#region Disclaimer/Info
///////////////////////////////////////////////////////////////////////////////////////////////////
// Subkismet.Captcha WebLog
// 
// Subkismet.Captcha is an open source weblog system that is a fork of the .TEXT
// weblog system.
//
// For updated news and information please visit http://subtextproject.com/
// Subkismet.Captcha is hosted at SourceForge at http://sourceforge.net/projects/subtext
// The development mailing list is at subtext-devs@lists.sourceforge.net 
//
// This project is licensed under the BSD license.  See the License.txt file for more information.
///////////////////////////////////////////////////////////////////////////////////////////////////
#endregion

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web.UI;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Subkismet Spam Library")]
[assembly: AssemblyDescription("A library of spam filtering and blocking techniques for .NET. Includes an Akismet implementation, InvisibleCaptcha and Google Safe Browsing API.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyCompany("Subkismet")]
[assembly: AssemblyProduct("Subkismet Spam Library")]
[assembly: AssemblyCopyright("Copyright © 2007-2008 Phil Haack - New BSD License")]
[assembly: AssemblyInformationalVersion("0.2.0.1")]
[assembly: AssemblyVersionAttribute("1.0.3.0")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0D237A09-FC6F-4831-8ED6-C04E0BB4C94D")]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum, Execution = true)]

// Required for embedded resources exposed as a script
[assembly: WebResource("Subkismet.Resources.InvisibleCaptcha.js", "text/javascript")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("UnitTests.GoogleSafeBrowsing")]