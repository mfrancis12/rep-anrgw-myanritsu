namespace Subkismet.Services.GoogleSafeBrowsing
{
    /// <summary>
    /// Enumerator for Google backlist type.
    /// </summary>
    public enum BlackListType
    {
        /// <summary>
        /// Phishing blacklist.
        /// </summary>
        Phishing = 0,
        /// <summary>
        /// Malware backlist.
        /// </summary>
        Malware = 1
    }
}