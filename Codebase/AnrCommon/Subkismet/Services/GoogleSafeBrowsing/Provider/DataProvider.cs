using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Provider;

namespace Subkismet.Services.GoogleSafeBrowsing.Provider
{
    /// <summary>
    /// Base provider class for storage system.
    /// </summary>
    public abstract class DataProvider : ProviderBase
    {
        /// <summary>
        /// Adds a single key to a backlist.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="listType">The blacklist type.</param>
        public abstract void AddKey(string key, BlackListType listType);

        /// <summary>
        /// Removes a single key from a blacklist.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="listType">The blacklist type.</param>
        public abstract void RemoveKey(string key, BlackListType listType);

        /// <summary>
        /// Adds a list of keys to a blacklist.
        /// </summary>
        /// <param name="keys">A list of keys.</param>
        /// <param name="listType">The blacklist type.</param>
        public abstract void AddKeys(List<Key> keys, BlackListType listType);

        /// <summary>
        /// Returns a list of string values for all keys in a blacklist.
        /// </summary>
        /// <param name="listType">The blacklist type.</param>
        /// <returns>A list of string values representing key values.</returns>
        public abstract List<string> GetKeys(BlackListType listType);

        /// <summary>
        /// Returns the latest list version for a blacklist.
        /// </summary>
        /// <param name="listType">The blacklist type.</param>
        /// <returns>String value of the latest version.</returns>
        public abstract string GetLatestVersion(BlackListType listType);

        /// <summary>
        /// Updates the version of the list for a blacklist.
        /// </summary>
        /// <param name="updateVersion">The new version.</param>
        /// <param name="listType">The blacklist type.</param>
        public abstract void UpdateVersion(string updateVersion, BlackListType listType);

        /// <summary>
        /// Clears a blacklist from all items.
        /// </summary>
        /// <param name="listType">The blacklist type.</param>
        public abstract void Clear(BlackListType listType);
    }
}
