﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;

namespace Anritsu.AnrConnectFW
{
    public class AnrConnectFW// : System.IDisposable
    {
        private FileSystemWatcher _FSWConnect;
        private String _ServerName = String.Empty;
        private String _MonitorType = String.Empty;
        private String _FolderPathToWatch = String.Empty;
        private DirectoryInfo _FWDirectoryInfo;
        private AnrConnectFWData _DAL;
        private Regex _ExcludePattern_FileExt;
        private Regex _ExcludePattern_Folder;
        private Boolean _IsConsoleMode = false;
 
        public AnrConnectFW()
        {
            InitConfig();
        }

        public void StartWatch()
        {
            // Begin watching.
            _FSWConnect.EnableRaisingEvents = true;
            WriteConsoleOutput("Started..");
        }

        public void StopWatch()
        {
            _FSWConnect.EnableRaisingEvents = false;
            WriteConsoleOutput("Stopped.");
        }

        private void InitConfig()
        {
            bool.TryParse(ConfigurationManager.AppSettings["FW.IsConsoleMode"], out _IsConsoleMode);

            _MonitorType = ConfigurationManager.AppSettings["FW.MonitorType"];
            _FolderPathToWatch = ConfigurationManager.AppSettings["FW.FolderPath"];
            _FWDirectoryInfo = new DirectoryInfo(_FolderPathToWatch);
            if (_FWDirectoryInfo == null || !_FWDirectoryInfo.Exists) throw new ArgumentNullException("FW.FolderPath:" + _FolderPathToWatch);
            _DAL = new AnrConnectFWData();

            String fileExtPattern = ConfigurationManager.AppSettings["FW.ExcludePattern.FileExt"];
            if (String.IsNullOrEmpty(fileExtPattern)) fileExtPattern = Guid.NewGuid().ToString();
            _ExcludePattern_FileExt = new Regex(fileExtPattern, RegexOptions.IgnoreCase);

            String folderPattern = ConfigurationManager.AppSettings["FW.ExcludePattern.Folder"];
            if (String.IsNullOrEmpty(folderPattern)) folderPattern = Guid.NewGuid().ToString();
            _ExcludePattern_Folder = new Regex(folderPattern, RegexOptions.IgnoreCase);

            _ServerName = System.Environment.MachineName;

            _FSWConnect = new System.IO.FileSystemWatcher();
            _FSWConnect.IncludeSubdirectories = true;

            _FSWConnect.Path = _FolderPathToWatch;
            _FSWConnect.IncludeSubdirectories = true;
            //fswConnect.NotifyFilter = NotifyFilters.LastWrite; //NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
            _FSWConnect.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            // Add event handlers.
            _FSWConnect.Changed += new FileSystemEventHandler(fswAnrConnect_Changed);
            _FSWConnect.Created += new FileSystemEventHandler(fswAnrConnect_Changed);
            _FSWConnect.Deleted += new FileSystemEventHandler(fswAnrConnect_Changed);
            _FSWConnect.Renamed += new RenamedEventHandler(fswAnrConnect_Renamed);

        }

        private void fswAnrConnect_Changed(object sender, FileSystemEventArgs e)
        {
           // WriteConsoleOutput(e.ChangeType.ToString());
            try
            {
                if (!CheckExclude_DirPath(e.FullPath)) return;
                String ext = Path.GetExtension(e.FullPath);
                if (!CheckExclude_FileExt(ext)) return;

                Boolean isDir = false;
                FileInfo fileInfo = new FileInfo(e.FullPath);
                if (((fileInfo.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                    && String.IsNullOrEmpty(fileInfo.Extension))
                {
                    isDir = true;
                    if (e.ChangeType != WatcherChangeTypes.Deleted)
                        return;
                }

                if (e.ChangeType == WatcherChangeTypes.Changed)
                {
                    WriteConsoleOutput("File size: " + fileInfo.Length);
                   // if (fileInfo.Length < 102400 || !IsFileLocked(fileInfo)) won't work on srever
                    {
                        ProcessEvent(e.FullPath, String.Empty, isDir, e.ChangeType);
                    }
                    
                }
                else if (e.ChangeType == WatcherChangeTypes.Created)
                {
                   // ProcessEvent(e.FullPath, String.Empty, isDir, e.ChangeType);
                }
                else if (e.ChangeType == WatcherChangeTypes.Deleted)
                {
                    ProcessEvent(e.FullPath, String.Empty, isDir, e.ChangeType);
                }
            }
            catch (Exception ex)
            {
                WriteConsoleOutput(ex.ToString());
                AnrConnectFWUtility.LogError(_DAL.MonitorType, "ConnectFW", ex, _ServerName);
            }
        }

        private void fswAnrConnect_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (!CheckExclude_DirPath(e.FullPath)) return;
                String ext = Path.GetExtension(e.FullPath);
                if (!CheckExclude_FileExt(ext)) return;

                Boolean isDir = false;
                FileInfo fileInfo = new FileInfo(e.FullPath);
                if (((fileInfo.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                    && String.IsNullOrEmpty(fileInfo.Extension))
                {
                    isDir = true;
                    if (e.ChangeType != WatcherChangeTypes.Deleted)
                        return;
                }

                ProcessEvent(e.FullPath, e.OldFullPath, isDir, WatcherChangeTypes.Renamed);
                
            }
            catch (Exception ex)
            {
                WriteConsoleOutput(ex.ToString());
                AnrConnectFWUtility.LogError(_DAL.MonitorType, "ConnectFW", ex, _ServerName);
            }
        }

        protected bool IsFileLocked(FileInfo file)
        {
            
            try
            {
                using (FileStream fs = File.Open(file.FullName, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    fs.Close();
                    return false;
                }
            }
            catch (IOException ioException)
            {
                WriteConsoleOutput(ioException.Message);
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                var errorCode = Marshal.GetHRForException(ioException) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;
            }

        }

        private Boolean CheckExclude_FileExt(string ext)
        {
            return !_ExcludePattern_FileExt.IsMatch(ext);
        }

        private Boolean CheckExclude_DirPath(string fullPath)
        {
            String pathToMatch = fullPath.Replace(_FolderPathToWatch, String.Empty).Substring(1);
            return !_ExcludePattern_Folder.IsMatch(pathToMatch);
        }

        private void ProcessEvent(String fullPath, String oldPath, Boolean isDir, WatcherChangeTypes changeType)
        {
            try
            {
                String errorMsg = String.Empty;
                Boolean status = _DAL.LogFWEventToProcess(fullPath, oldPath, isDir, changeType.ToString().ToUpperInvariant(), out errorMsg);
            }
            catch (Exception ex)
            {
                WriteConsoleOutput(ex.ToString());
                AnrConnectFWUtility.LogError(_DAL.MonitorType, "ConnectFW", ex, _ServerName);
            }
        }

       
        //public void Dispose()
        //{
        //    WriteConsoleOutput("Disposing..");
        //    StopWatch();
        //    _FSWConnect = null;
        //}

        private void WriteConsoleOutput(String msg)
        {
            if (_IsConsoleMode) Console.WriteLine(msg);
        }
    }
}
