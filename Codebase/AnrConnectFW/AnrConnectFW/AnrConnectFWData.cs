﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Anritsu.AnrConnectFW
{
    public class AnrConnectFWData
    {
        private String _ConnStr = String.Empty;
        public String MonitorType { get; set; }
        public AnrConnectFWData()
        {
            _ConnStr = ConfigurationManager.ConnectionStrings["FWDB.FileWatcherEvents"].ConnectionString;
            MonitorType = ConfigurationManager.AppSettings["FW.MonitorType"];
        }

        public Boolean LogFWEventToProcess(String fullPath, String oldFullPath, Boolean isDir, String changeType, out String errorMsg)
        {
            
            errorMsg = String.Empty;
           // Console.WriteLine("{0}-{1}", changeType, fullPath);
            //return true;
            try
            {
                using (SqlConnection conn = AnrConnectFWUtility.ConnOpen(_ConnStr))
                {
                    SqlCommand cmd = AnrConnectFWUtility.MakeSPCmd(conn, null, "[dbo]", "[CNT_JapanDownloadsQueue_Insert]", 0);
                    cmd.Parameters.AddWithValue("@MonitorType", MonitorType.ToUpperInvariant());
                    cmd.Parameters.AddWithValue("@FullPath", fullPath);
                    cmd.Parameters.AddWithValue("@OldFullPath", oldFullPath);
                    cmd.Parameters.AddWithValue("@EventType", changeType.ToUpperInvariant());
                    cmd.Parameters.AddWithValue("@IsDir", isDir ? 1 : 0);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.ToString();
                return false;
            }
        }
    }
}
