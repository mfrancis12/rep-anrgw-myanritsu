﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;

namespace Anritsu.AnrConnectFW
{
    partial class AnrConnectFWService : ServiceBase
    {
        private AnrConnectFW _ConnectFW;

        public AnrConnectFWService()
        {
            InitializeComponent();
            _ConnectFW = new AnrConnectFW();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            _ConnectFW.StartWatch();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            _ConnectFW.StopWatch();
        }

       
    }
}
