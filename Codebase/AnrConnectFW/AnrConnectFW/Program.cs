﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.ServiceProcess;
using System.Security.Permissions;

namespace Anritsu.AnrConnectFW
{
    public class Program
    {


        public static void Main(string[] args)
        {
            if (ConfigurationManager.AppSettings["FW.IsConsoleMode"] == "true")
            {
                AnrConnectFW fw = new AnrConnectFW();
                    fw.StartWatch();
                    Console.Read();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new AnrConnectFWService()
                };
                ServiceBase.Run(ServicesToRun);
            }

        }





    }
}
