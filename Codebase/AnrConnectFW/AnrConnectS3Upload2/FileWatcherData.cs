﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace Anritsu.AnrConnectS3Upload2
{
    public class FileWatcherData
    {
        private String _ConnStr = String.Empty;
        private String _ConnStrJPDB = String.Empty;

        public FileWatcherData()
        {
            _ConnStr = ConfigurationManager.ConnectionStrings["FWDB.FileWatcherEvents"].ConnectionString;
            _ConnStrJPDB = ConfigurationManager.ConnectionStrings["Connect.DB.ConnStr.JPDL"].ConnectionString;
            
        }

        public DataTable FileWatcherEvents_SelectPending()
        {
           // Console.WriteLine("{0}-{1}", changeType, fullPath);
            //return true;
            using (SqlConnection conn = Util.ConnOpen(_ConnStr))
            {
                SqlCommand cmd = Util.MakeSPCmd(conn, null, "[dbo]", "[CNT_JapanDownloadsQueue_SelectPending]", 0);
                return Util.FillInDataTable(cmd);
            }
        }

        public void FileWatcherEvents_InsertError(Int32 queueID, String errorMsg)
        {
            using (SqlConnection conn = Util.ConnOpen(_ConnStr))
            {
                SqlCommand cmd = Util.MakeSPCmd(conn, null, "[dbo]", "[CNT_JapanDownloadsQueue_InsertError]", 0);
                cmd.Parameters.AddWithValue("@QueueID", queueID);
                cmd.Parameters.AddWithValue("@Errors", errorMsg.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public void FileWatcherEvents_UpdateStatus(Int32 queueID, String newStatus)
        {
            using (SqlConnection conn = Util.ConnOpen(_ConnStr))
            {
                SqlCommand cmd = Util.MakeSPCmd(conn, null, "[dbo]", "[CNT_JapanDownloadsQueue_UpdateStatus]", 0);
                cmd.Parameters.AddWithValue("@QueueID", queueID);
                cmd.Parameters.AddWithValue("@ProcessStatus", newStatus.ToUpperInvariant());
                cmd.ExecuteNonQuery();
            }
        }

        public void FileWatcherEvents_InsertAWSDetail(Int32 queueID, String awsType, String awsRegionEndpoint, String awsBucketName
            , String awsResponse, String awsStatus)
        {
            using (SqlConnection conn = Util.ConnOpen(_ConnStr))
            {
                SqlCommand cmd = Util.MakeSPCmd(conn, null, "[dbo]", "[CNT_JapanDownloadsQueue_AwsDetails_Insert]", 0);
                cmd.Parameters.AddWithValue("@QueueID", queueID);
                cmd.Parameters.AddWithValue("@AwsType", awsType.ToUpperInvariant());
                cmd.Parameters.AddWithValue("@AwsRegionEndpoint", awsRegionEndpoint);
                cmd.Parameters.AddWithValue("@AwsBucketName", awsBucketName);
                cmd.Parameters.AddWithValue("@AwsResponse", awsResponse);
                cmd.Parameters.AddWithValue("@AwsStatus", awsStatus);
                cmd.ExecuteNonQuery();
            }
        }

        public String JapanDB_SelectFriendlyName(FileInfo fi)
        {
            if (fi == null) return String.Empty;

            String friendlyFileNameForUser = string.Empty;
            String modelName = String.Empty;
            DirectoryInfo diParent = fi.Directory;
            if (diParent != null)
            {
                modelName = diParent.Name;
            }
            if (String.IsNullOrEmpty(modelName)) return string.Empty;

            using (SqlConnection conn = Util.ConnOpen(_ConnStrJPDB))
            {
                SqlCommand cmd = new SqlCommand("", conn);
                cmd.CommandText = "SELECT * FROM [dbo].[W2_Object] (NOLOCK) WHERE [ModelName] = @ModelName AND [FileName] = @FileName";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ModelName", modelName);
                cmd.Parameters.AddWithValue("@FileName", fi.Name.Replace(".bf", ""));
                String softName = String.Empty;
                DataRow r = Util.FillInDataRow(cmd);
                if (r == null) return String.Empty;
                softName = r["SoftName"].ToString();

                if (!String.IsNullOrEmpty(softName))
                {
                    String ext = Path.GetExtension(fi.FullName.Replace(".bf", ""));
                    friendlyFileNameForUser = String.Format("{0}{1}", softName, ext);
                }
                
            }

            return friendlyFileNameForUser;
        }
    }
}
