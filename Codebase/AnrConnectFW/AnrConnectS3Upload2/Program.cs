﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.AnrConnectS3Upload2
{
    class Program
    {
        static void Main(string[] args)
        {

            foreach (string arg in args)
            {
                if (arg.StartsWith("-help", StringComparison.InvariantCultureIgnoreCase))
                {
                    PrintMenu();
                    return;
                }
                else if (arg.StartsWith("-copybucket:", StringComparison.InvariantCultureIgnoreCase))
                {
                    //-copybucket:srckey=DL1:targetkey=DL1_US
                    String[] copyParms = arg.Replace("-copybucket:", String.Empty).Split(':');
                    String sourceKey = String.Empty;
                    String targetKey = String.Empty;

                    foreach (String copyParm in copyParms)
                    {
                        if (copyParm.StartsWith("srckey", StringComparison.InvariantCultureIgnoreCase))
                            sourceKey = copyParm.Replace("srckey=", String.Empty);
                        else if (copyParm.StartsWith("targetkey", StringComparison.InvariantCultureIgnoreCase))
                            targetKey = copyParm.Replace("targetkey=", String.Empty);
                    }
                    S3Util util = new S3Util();
                    util.CopyBucket(sourceKey, targetKey);
                }
                else if (arg.StartsWith("-fwchange:", StringComparison.InvariantCultureIgnoreCase))
                {
                    //-fwchange
                    //String[] cmdParms = arg.Replace("-fwchange:", String.Empty).Split(':');
                    S3Util util = new S3Util();
                    util.HandleFileWatcherEvents();

                }
                else
                {
                    PrintMenu();
                    return;
                }
            }
            //Console.Read();
        }

        private static void PrintMenu()
        {
            Console.WriteLine("=========== Optional parameters ==============");
            Console.WriteLine("Help ==> -help");
            Console.WriteLine("Copy S3 Bucket ==> -copybucket:srckey=DL1:targetkey=DL1_US");
            Console.WriteLine("Process pending file watcher events ==> -fwchange:");
            //Console.WriteLine("Number of Threads ==> -threads:<10>");
            //Console.WriteLine("RunMode ==> -runmode:<uploadfromjpdb|normal>");
        }
    }
}
