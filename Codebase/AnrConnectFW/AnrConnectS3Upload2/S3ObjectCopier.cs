﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;


namespace Anritsu.AnrConnectS3Upload2
{
    public class S3ObjectCopier : IDisposable
    {
        private Int32 _ID { get; set; }
        private AmazonS3Client _S3Client { get; set; }
        private String _SrcBucketName { get; set; }
        private String _DestBucketName { get; set; }
        public List<String> S3ObjectKeys { get; set; }

        public S3ObjectCopier(Int32 id, AmazonS3Client client, String src_BucketName, String dest_BucketName)
        {
            _ID = id;
            _S3Client = client;
            _SrcBucketName = src_BucketName;
            _DestBucketName = dest_BucketName;
        }

        public void CopyObject()
        {
            if (S3ObjectKeys == null) return;
            Console.WriteLine("S3Copier:{0} Started.", _ID);
            foreach (String so in S3ObjectKeys)
            {
                try
                {
                    CopyObjectRequest copyReq = new CopyObjectRequest()
                        .WithSourceBucket(_SrcBucketName)
                        .WithSourceKey(so)
                        .WithDestinationBucket(_DestBucketName)
                        .WithDestinationKey(so)
                        .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)

                    ;
                    CopyObjectResponse copyResp = _S3Client.CopyObject(copyReq);
                    //Console.WriteLine(so + " copied to " + _DestBucketName);
                }
                catch (Exception copyEx)
                {
                    Console.WriteLine("ERROR:{0}:{1}", so, copyEx.Message);
                }
            }
            Console.WriteLine("S3Copier:{0} completed.", _ID);
        }

        public void Dispose()
        {
            try
            {
                _S3Client.Dispose();
            }
            catch { }
        }
    }
}
