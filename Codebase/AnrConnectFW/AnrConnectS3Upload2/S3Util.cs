﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Threading;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;

namespace Anritsu.AnrConnectS3Upload2
{
    public class S3Util
    {
        private String _AWS_AccessKey = String.Empty;
        private String _AWS_SecretKey = String.Empty;
        private String _BlowFishTempFolder = String.Empty;
        private FileWatcherData _DAL;
        private Boolean _EnableCloudFront_Invalidate = false;

        public S3Util()
        {
            _AWS_AccessKey = ConfigurationManager.AppSettings["AWS.AWSAccessKey"];
            _AWS_SecretKey = ConfigurationManager.AppSettings["AWS.AWSSecretKey"];

            _BlowFishTempFolder = ReadFWEventConfig("EncryptedType", "BFEncryptTempDir");
            DirectoryInfo diBF = new DirectoryInfo(_BlowFishTempFolder);
            if (!diBF.Exists) diBF.Create();
            _BlowFishTempFolder = diBF.FullName;

            _DAL = new FileWatcherData();

            _EnableCloudFront_Invalidate = Boolean.Parse(ReadFWEventConfig("CdnType", "EnableInvalidate"));
        }

        public AmazonS3Client GetS3Client(String bucketKey)
        {
            
            AmazonS3Client s3Client;
            if (String.IsNullOrEmpty(bucketKey))
            {
                s3Client = new AmazonS3Client(_AWS_AccessKey, _AWS_SecretKey);
            }
            else
            {
                RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ReadS3BucketConfig(bucketKey, "LocationConstraint"));
                s3Client = new AmazonS3Client(_AWS_AccessKey, _AWS_SecretKey, regionEndpoint);
            }
            return s3Client;
        }

        public TransferUtility GetS3TransferUtility(String bucketKey)
        {
            RegionEndpoint regionEndpoint = RegionEndpoint.GetBySystemName(ReadS3BucketConfig(bucketKey, "LocationConstraint"));
            return new TransferUtility(_AWS_AccessKey, _AWS_SecretKey, regionEndpoint);
            
        }

        public void CopyBucket(String sourceBucketKey, String targetBucketKey)
        {
            if (String.IsNullOrEmpty(sourceBucketKey) || String.IsNullOrEmpty(targetBucketKey)) return;
            
            String src_BucketName = ReadS3BucketConfig(sourceBucketKey, "BucketName");
            String target_BucketName = ReadS3BucketConfig(targetBucketKey, "BucketName");
            if (String.IsNullOrEmpty(src_BucketName) || String.IsNullOrEmpty(target_BucketName)) return;

            String src_LocationConstraint = ReadS3BucketConfig(sourceBucketKey, "LocationConstraint");
            String src_Endpoint = ReadS3BucketConfig(sourceBucketKey, "Endpoint");

            String target_LocationConstraint = ReadS3BucketConfig(targetBucketKey, "LocationConstraint");
            String target_Endpoint = ReadS3BucketConfig(targetBucketKey, "Endpoint");

            try
            {
                
                using (AmazonS3Client s3Client = GetS3Client(sourceBucketKey))
                {
                    ListObjectsRequest listReq = new ListObjectsRequest()
                        .WithBucketName(src_BucketName)
                        ;
                    Int32 totalFound = 0;
                    #region " get all objects "
                    List<String> s3ObjectList = new List<String>();
                    do
                    {
                        ListObjectsResponse listResp = s3Client.ListObjects(listReq);
                        List<S3Object> objs = listResp.S3Objects;
                        if (objs == null)
                        {
                            Console.WriteLine("Not found.");
                            break;
                        }

                        totalFound += objs.Count;
                        Console.WriteLine("Total {0} found.  HasMore:{1}", totalFound, listResp.IsTruncated.ToString());

                        foreach (S3Object so in objs)
                        {
                            s3ObjectList.Add(so.Key);
                        }

                        if (listResp.IsTruncated)
                            listReq.Marker = listResp.NextMarker;
                        else
                            break;


                    }
                    while (true); 
                    #endregion

                    Int32 numberOfThreads = (totalFound / 30) + 1;
                    List<String>[] list = Partition<String>(s3ObjectList, numberOfThreads);
                    Console.WriteLine("{0} threads to run.", list.Length);
                    int copierCount = 0;
                    foreach (List<String> lst in list)
                    {
                        S3ObjectCopier cp = new S3ObjectCopier(copierCount, GetS3Client(targetBucketKey), src_BucketName, target_BucketName);
                        cp.S3ObjectKeys = lst;
                        ThreadStart job = new ThreadStart(cp.CopyObject);
                        Thread tr = new Thread(job);
                        tr.Start();
                        Console.WriteLine("ThreadID: {0} started.", copierCount);
                        copierCount++;
                    }
                    Console.WriteLine("==== Total copier {0} for {1} items.", copierCount, totalFound);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: {0}", ex.Message);
            }
            Console.WriteLine("CopyBucket task completed.");
        }

        public static List<T>[] Partition<T>(List<T> list, int totalPartitions)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            if (totalPartitions < 1)
                throw new ArgumentOutOfRangeException("totalPartitions");

            List<T>[] partitions = new List<T>[totalPartitions];

            int maxSize = (int)Math.Ceiling(list.Count / (double)totalPartitions);
            int k = 0;

            for (int i = 0; i < partitions.Length; i++)
            {
                partitions[i] = new List<T>();
                for (int j = k; j < k + maxSize; j++)
                {
                    if (j >= list.Count)
                        break;
                    partitions[i].Add(list[j]);
                }
                k += maxSize;
            }

            return partitions;
        }

        //public void CopyObject(String )
        //{
        //    using (AmazonS3Client s3ClientForCopy = GetS3Client(targetBucketKey))
        //    {
        //        foreach (S3Object so in objs)
        //        {
        //            //Console.WriteLine(so.Key + " processing..");
        //            //if (so.Key.EndsWith("/")) continue;
        //            try
        //            {
        //                CopyObjectRequest copyReq = new CopyObjectRequest()
        //                    .WithSourceBucket(src_BucketName)
        //                    .WithSourceKey(so.Key)
        //                    .WithDestinationBucket(target_BucketName)
        //                    .WithDestinationKey(so.Key)
        //                    .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)

        //                ;
        //                CopyObjectResponse copyResp = s3ClientForCopy.CopyObject(copyReq);
        //                completeCount++;
        //                Console.WriteLine(String.Format("{0} of {1}:{2} copied.", completeCount, totalFound, so.Key));
        //            }
        //            catch (Exception copyEx)
        //            {
        //                Console.WriteLine("ERROR:{0}:{1}", so.Key, copyEx.Message);
        //            }
        //        }
        //    }
        //}

        private String ReadS3BucketConfig(String bucketKey, String parmKey)
        {
            return ConfigurationManager.AppSettings[String.Format("AWS.S3.{0}.{1}", bucketKey, parmKey)];
        }

        private String ReadFWEventConfig(String fileType, String parmKey)
        {
            return ConfigurationManager.AppSettings[String.Format("FWEvent.{0}.{1}", fileType, parmKey)];
        }

        public void HandleFileWatcherEvents()
        {
            FileWatcherData data = new FileWatcherData();
            DataTable tb = data.FileWatcherEvents_SelectPending();
            if (tb == null || tb.Rows.Count < 1) return;

            foreach (DataRow r in tb.Rows)
            {                
                Int32 queueID = int.Parse(r["QueueID"].ToString());
                String monitorType = r["MonitorType"].ToString();
                String fullPath = r["FullPath"].ToString();
                Boolean isDir = Boolean.Parse(r["IsDir"].ToString());
                String eventType = r["EventType"].ToString();
                DateTime eventTimeUTC = DateTime.Parse(r["EventTimeUTC"].ToString());
                String processStatus = r["ProcessStatus"].ToString();
                Boolean isEncryptedFile = fullPath.EndsWith(".bf", StringComparison.InvariantCultureIgnoreCase);

                Console.WriteLine(System.Environment.NewLine + "Processing==> {0}:{1}:{2}", eventType, monitorType, fullPath);

                if (eventType == "DELETED")
                {
                    ProcessDeleteEvent(queueID, fullPath.Replace(".bf", String.Empty), isDir, isEncryptedFile, monitorType);
                }
                else
                {
                    String filePathToUpload = fullPath;
                    String tempDecryptedFilePath = String.Empty;

                    if (!isDir && Util.IsFileLocked(fullPath))
                    {
                        _DAL.FileWatcherEvents_UpdateStatus(queueID, "PENDING");
                        Console.WriteLine("File is locked.  Skipping..");
                        continue;
                    }

                    #region " Handle encryption "
                    if (isEncryptedFile)
                    {
                        try
                        {
                            //encrypted file
                            FileInfo fi = new FileInfo(fullPath);
                            String tempEncryptedFilePath = Path.Combine(_BlowFishTempFolder, String.Format("{0}.bft", Guid.NewGuid()));
                            fi.CopyTo(tempEncryptedFilePath, true);

                            tempDecryptedFilePath = DecryptBlowfishEncryptedFile(tempEncryptedFilePath);
                            File.Delete(tempEncryptedFilePath);
                            filePathToUpload = tempDecryptedFilePath;
                        }
                        catch (Exception encEx)
                        {
                            String errorMsg = String.Format("ERROR:DecryptFileEvent:{0}", encEx.Message);
                            _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                            Console.WriteLine(errorMsg);

                            _DAL.FileWatcherEvents_UpdateStatus(queueID, "FAILED");
                            continue;
                        }
                    } 
                    #endregion
                    
                    if (!isDir)
                    {
                        ProcessChangeEvent(queueID, fullPath, isDir, isEncryptedFile, monitorType, filePathToUpload);
                        if (isEncryptedFile) File.Delete(tempDecryptedFilePath);
                    }
                }
                
            }
        }

        private void ProcessDeleteEvent(int queueID, String fullPath, Boolean isDir, Boolean isEncryptedFile, String monitorType)
        {
            try
            {
                String type = isEncryptedFile ? "EncryptedType" : "CdnType";
                String originBucketKey = ReadFWEventConfig(type, "S3OriginBucketKey");
                String originBucketName = ReadS3BucketConfig(originBucketKey, "BucketName");
                String originBucketEndpoint = ReadS3BucketConfig(originBucketKey, "Endpoint");
                String prefix = "dl/";
                String baseFolderPath = ConfigurationManager.AppSettings["FWEvent.DownloadFiles.BaseFolder"];
                if (monitorType.Equals("CONNECT-FWDB-VERTBL"))
                {
                    baseFolderPath = ConfigurationManager.AppSettings["FWEvent.VersionTables.BaseFolder"];
                    prefix = "vertbl/";

                }

                String targetFileName = fullPath.Replace(baseFolderPath, String.Empty);
                String s3ObjectName = GetS3ObjectName(targetFileName, prefix);
                String[] copyToBucketKeys = ReadFWEventConfig(type, "CopyToBucketKeys").Split(',');                

                _DAL.FileWatcherEvents_UpdateStatus(queueID, String.Format("PROCESSING-{0}", originBucketKey.ToUpperInvariant()));

                Boolean deletedFromOrigin = false;
                if (isDir) deletedFromOrigin = DeleteFolderFromS3(queueID, s3ObjectName, originBucketKey, originBucketName);
                else deletedFromOrigin = DeleteFromS3(queueID, s3ObjectName, originBucketKey, originBucketName);
                _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "S3Delete", originBucketEndpoint, originBucketName, s3ObjectName, deletedFromOrigin ? "SUCCESS" : "FAILED");

                if (copyToBucketKeys != null)
                {
                    foreach (String cpBK in copyToBucketKeys)
                    {
                        String targetBucketName = ReadS3BucketConfig(cpBK, "BucketName");
                        String targetBucketEndpoint = ReadS3BucketConfig(cpBK, "Endpoint");
                        _DAL.FileWatcherEvents_UpdateStatus(queueID, String.Format("PROCESSING-{0}", cpBK));
                        Boolean deletedFromBucket = false;
                        if(isDir) deletedFromBucket = DeleteFolderFromS3(queueID, s3ObjectName, cpBK, targetBucketName);
                        else deletedFromBucket = DeleteFromS3(queueID, s3ObjectName, cpBK, targetBucketName);

                        _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "S3Delete", targetBucketEndpoint, targetBucketName, s3ObjectName, deletedFromBucket ? "SUCCESS" : "FAILED");
                    }
                }

                if (!isEncryptedFile)
                {
                    Boolean invalidateCF = InvalidateCloudFrontCache(s3ObjectName);
                    _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "InvalidateCloudFront", "-", originBucketName, s3ObjectName, invalidateCF ? "SUCCESS" : "FAILED");
                }

                _DAL.FileWatcherEvents_UpdateStatus(queueID, "COMPLETED");
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ERROR:ProcessDeleteEvent:{0}:{1}", queueID, ex.Message);
                String errorMsg = String.Format("ERROR:ProcessDeleteEvent:{0}", ex.Message);
                _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                Console.WriteLine(errorMsg);
            }
        }

        private void ProcessChangeEvent(int queueID, String fullPath, Boolean isDir, Boolean isEncryptedFile, String monitorType, String filePathToUpload)
        {
            try
            {
                String type = isEncryptedFile ? "EncryptedType" : "CdnType";
                String originBucketKey = ReadFWEventConfig(type, "S3OriginBucketKey");
                String originBucketName = ReadS3BucketConfig(originBucketKey, "BucketName");
                String originBucketEndpoint = ReadS3BucketConfig(originBucketKey, "Endpoint");

                String prefix = "dl/";
                String baseFolderPath = ConfigurationManager.AppSettings["FWEvent.DownloadFiles.BaseFolder"];
                FileInfo file = new FileInfo(fullPath);
                String friendlyFileName = String.Empty;
                if (monitorType.Equals("CONNECT-FWDB-VERTBL"))
                {
                    baseFolderPath = ConfigurationManager.AppSettings["FWEvent.VersionTables.BaseFolder"];
                    prefix = "vertbl/";

                }
                else
                {
                    friendlyFileName = _DAL.JapanDB_SelectFriendlyName(file);
                }
                String targetFileName = fullPath.Replace(baseFolderPath, String.Empty);
                String s3ObjectName = GetS3ObjectName(targetFileName, prefix);
                String[] copyToBucketKeys = ReadFWEventConfig(type, "CopyToBucketKeys").Split(','); 
                
                _DAL.FileWatcherEvents_UpdateStatus(queueID, String.Format("PROCESSING-{0}", originBucketKey.ToUpperInvariant()));

                FileInfo fiToUpload = new FileInfo(filePathToUpload);

                Boolean uploadedToOrigin = UploadToS3(queueID, originBucketKey, s3ObjectName, fiToUpload, friendlyFileName);
                if (!uploadedToOrigin) throw new ArgumentException("Unable to upload to origin for " + s3ObjectName);
                _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "S3Upload", originBucketEndpoint, originBucketName, s3ObjectName, "SUCCESS");

                int failCount = 0;
                if (copyToBucketKeys != null)
                {
                    foreach (String cpBK in copyToBucketKeys)
                    {
                        _DAL.FileWatcherEvents_UpdateStatus(queueID, String.Format("PROCESSING-{0}", cpBK));
                        String targetBucketName = ReadS3BucketConfig(cpBK, "BucketName");
                        String targetEndpoint = ReadS3BucketConfig(cpBK, "Endpoint");
                        Boolean copiedToS3Bucket = CopyS3Object(originBucketName, targetBucketName, s3ObjectName, cpBK);
                        _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "S3Copy", targetEndpoint, targetBucketName, s3ObjectName, copiedToS3Bucket ? "SUCCESS" : "FAILED");
                        if (!copiedToS3Bucket) failCount++;
                    }
                }

                if (!isEncryptedFile)
                {
                    Boolean invalidateCF = InvalidateCloudFrontCache(s3ObjectName);
                    _DAL.FileWatcherEvents_InsertAWSDetail(queueID, "InvalidateCloudFront", "-", originBucketName, s3ObjectName, invalidateCF ? "SUCCESS" : "FAILED");
                    if (!invalidateCF) failCount++;
                }
                if (failCount > 0)
                    throw new ArgumentException(String.Format("Total failed: {0}", failCount));
                else
                    _DAL.FileWatcherEvents_UpdateStatus(queueID, "COMPLETED");
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ERROR:ProcessChangeEvent:{0}:{1}", queueID, ex.Message);

                String errorMsg = String.Format("ERROR:ProcessChangeEvent:{0}", ex.Message);
                _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                Console.WriteLine(errorMsg);

                _DAL.FileWatcherEvents_UpdateStatus(queueID, "FAILED");
            }
        }

        public Boolean DeleteFolderFromS3(Int32 queueID, String awsS3ObjectName, String bucketKey, String bucketName)
        {
            String s3ObjectNameToDelete = awsS3ObjectName;
            if (!s3ObjectNameToDelete.EndsWith("/")) s3ObjectNameToDelete += "/";

            try
            {
                using (AmazonS3Client s3Client = GetS3Client(bucketKey))
                {
                    ListObjectsRequest listReq = new ListObjectsRequest();
                    listReq.BucketName = bucketName;
                    listReq.WithPrefix(s3ObjectNameToDelete);

                    Int64 totalFound = 0;
                    do
                    {
                        ListObjectsResponse listResp = s3Client.ListObjects(listReq);
                        List<S3Object> objs = listResp.S3Objects;
                        if (objs == null)
                        {
                            Console.WriteLine("Empty folder.");
                            DeleteFromS3(queueID, s3ObjectNameToDelete, bucketKey, bucketName);
                            break;
                        }

                        totalFound += objs.Count;
                        Console.WriteLine("Total {0} found.  HasMore:{1}", totalFound, listResp.IsTruncated.ToString());
                        foreach (S3Object so in objs)
                        {
                            Boolean deleted = DeleteFromS3(queueID, so.Key, bucketKey, bucketName);
                        }
                        if (listResp.IsTruncated)
                            listReq.Marker = listResp.NextMarker;
                        else
                            break;

                    }
                    while (true);

                }

                return true;
            }
            catch (Exception ex)
            {
                String errorMsg = String.Format("ERROR:DeleteFolderFromS3:{0}:{1}", bucketKey, ex.Message);
                _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                Console.WriteLine(errorMsg);
                return false;
            }

            
        }

        public Boolean DeleteFromS3(Int32 queueID, String awsS3ObjectName, String bucketKey, String bucketName)
        {
            try
            {
                using (AmazonS3Client client = GetS3Client(bucketKey))
                {
                    DeleteObjectRequest deleteReq = new DeleteObjectRequest
                    {
                        BucketName = bucketName,
                        Key = awsS3ObjectName
                    };
                    client.DeleteObject(deleteReq);
                }
                Console.WriteLine(String.Format("{0} deleted from {1}.", awsS3ObjectName, bucketName));
                return true;

            }
            catch (Exception ex)
            {
                //Console.WriteLine(String.Format("ERROR In DeleteFileFromS3:{0}:{1}", awsS3ObjectName, ex.ToString()));
                String errorMsg = String.Format("ERROR:DeleteFileFromS3:{0}:{1}", bucketKey, ex.Message);
                _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                Console.WriteLine(errorMsg);
                return false;
            }
        }

        private String GetS3ObjectName(String path, String prefix)
        {
            String awsS3ObjectName = path.Replace("\\", "/");
            if (awsS3ObjectName.StartsWith("/")) awsS3ObjectName = awsS3ObjectName.Substring(1);

            if (!String.IsNullOrEmpty(prefix))
            {
                awsS3ObjectName = String.Format("{0}{1}", prefix, awsS3ObjectName);
            }
            return awsS3ObjectName.ToLowerInvariant().Replace(".bf", String.Empty);
        }

        private Boolean UploadToS3(Int32 queueID, String bucketKey, String awsS3ObjectName, FileInfo fi, String friendlyFileName)
        {
            try
            {
                String bucketName = ReadS3BucketConfig(bucketKey, "BucketName");
                Int32 timeout = Int32.Parse(ConfigurationManager.AppSettings["AWS.S3.Timeout"]);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
                        .WithBucketName(bucketName)
                        .WithFilePath(fi.FullName)
                        .WithTimeout(timeout)
                        .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)
                        .WithKey(awsS3ObjectName.ToLowerInvariant())
                        ;
                if (!String.IsNullOrEmpty(friendlyFileName))
                {
                    string dlmode = "attachment";
                    if (fi.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                    {
                        dlmode = "inline";
                    }
                    request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Content-Disposition", String.Format("{0};filename={1}", dlmode,
                        friendlyFileName.ToLowerInvariant())));
                }
                String cacheControl = ReadFWEventConfig("CdnType", "CacheControl");
                request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Cache-Control", cacheControl));
                TransferUtility transferUtil = GetS3TransferUtility(bucketKey);
                transferUtil.Upload(request);
                
                return true;
            }
            catch (Exception ex)
            {
                //Util.LogError(ConfigurationManager.AppSettings["AppName"], "UploadToS3", ex, System.Environment.MachineName);
                String errorMsg = String.Format("ERROR:UploadToS3:{0}:{1}", bucketKey, ex.Message);
                _DAL.FileWatcherEvents_InsertError(queueID, errorMsg);
                Console.WriteLine(errorMsg);
                return false;
            }

        }

        private Boolean CopyS3Object(String srcBucketName, String targetBucketName, String awsS3ObjectName, String targetBucketKey)
        {
            try
            {
                using (AmazonS3Client s3ClientForCopy = GetS3Client(targetBucketKey))
                {
                    CopyObjectRequest copyReq = new CopyObjectRequest()
                        .WithSourceBucket(srcBucketName)
                        .WithSourceKey(awsS3ObjectName)
                        .WithDestinationBucket(targetBucketName)
                        .WithDestinationKey(awsS3ObjectName)
                        .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)

                    ;
                    CopyObjectResponse copyResp = s3ClientForCopy.CopyObject(copyReq);
                    Console.WriteLine("{0} copied to {1}", awsS3ObjectName, targetBucketName);
                    return copyResp != null;
                }
            }
            catch (Exception copyEx)
            {
                Console.WriteLine("ERROR:{0}:{1}", awsS3ObjectName, copyEx.Message);
                return false;
            }
        }

        public Boolean InvalidateCloudFrontCache(String s3ObjectKey)
        {
            if (!_EnableCloudFront_Invalidate) return true;
            List<String> objectsToInvalidate = new List<string>();
            if (s3ObjectKey.StartsWith("/")) objectsToInvalidate.Add(s3ObjectKey);
            else objectsToInvalidate.Add("/" + s3ObjectKey);
            try
            {
                using (AmazonCloudFrontClient client = new AmazonCloudFrontClient(_AWS_AccessKey, _AWS_SecretKey))
                {

                    InvalidationBatch ib = new InvalidationBatch();
                    Amazon.CloudFront.Model.Paths path = new Amazon.CloudFront.Model.Paths();

                    CreateInvalidationRequest req = new CreateInvalidationRequest();
                    req.WithDistributionId( ReadFWEventConfig("CdnType", "DistributionID"));
                    req.WithInvalidationBatch(new InvalidationBatch
                    {
                        Paths = new Paths
                        {
                            Quantity = 1,
                            Items = objectsToInvalidate
                        },
                        CallerReference = DateTime.UtcNow.ToString("R")
                    });

                    CreateInvalidationResponse resp = client.CreateInvalidation(req);
                    
                    return resp != null;
                    //Trace.Write(String.Format("{0} invalidated from CloudFront.", _EventPath));
                }
            }
            catch (Exception ex)
            {
                Console.Write(String.Format("ERROR in InvalidateCloudFrontCache: {0}", ex.ToString()));
                return false;
            }
        }

        private String DecryptBlowfishEncryptedFile(String encryptedFilePath)
        {
            String encKey = "Lgt3tJQTnLNai42Q8KtE";

            #region " COM REF "
            COMBLOWFISHLib.Crypt cr = new COMBLOWFISHLib.Crypt();
            cr.SetKey(encKey);
            cr.FileDeCrypt(encryptedFilePath);
            return encryptedFilePath + ".bf";

            #endregion



        }
    }
}
