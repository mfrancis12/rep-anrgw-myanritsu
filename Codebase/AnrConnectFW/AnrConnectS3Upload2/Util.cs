﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.IO;
using System.Runtime.InteropServices;

namespace Anritsu.AnrConnectS3Upload2
{
    public class Util
    {
        public static bool IsFileLocked(String fullPath)
        {

            try
            {
                using (FileStream fs = File.Open(fullPath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    fs.Close();
                    return false;
                }
            }
            catch (IOException ioException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                var errorCode = Marshal.GetHRForException(ioException) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;
            }

        }

        public static SqlConnection ConnOpen(string connStr)
        {
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            return conn;
        }

        public static SqlCommand MakeSPCmd(SqlConnection conn
            , SqlTransaction tran
            , string spSchema
            , string spName
            , int timeout)
        {
            if (string.IsNullOrEmpty(spSchema)) spSchema = "[dbo]";
            if (string.IsNullOrEmpty(spName)) throw new ArgumentNullException("spName");

            string sp = spSchema + "." + spName;
            SqlCommand cmd = new SqlCommand(sp, conn);
            if (tran != null) cmd.Transaction = tran;
            cmd.CommandType = CommandType.StoredProcedure;

            if (timeout > 0) cmd.CommandTimeout = timeout;
            return cmd;
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd)
        {
            return FillInDataSet(sqCmd, string.Empty);
        }

        public static DataSet FillInDataSet(SqlCommand sqCmd, string tbName)
        {
            DataSet ds = null;
            SqlDataAdapter da = new SqlDataAdapter(sqCmd);
            ds = new DataSet();
            if (string.IsNullOrEmpty(tbName)) da.Fill(ds);
            else da.Fill(ds, tbName);
            return ds;
        }

        public static DataSet FillInDataSet(SqlDataAdapter sqDA)
        {
            if (sqDA == null || sqDA.SelectCommand == null)
                return null;

            DataSet ds = new DataSet();
            sqDA.Fill(ds);
            return ds;

        }

        public static DataTable FillInDataTable(SqlCommand sqCmd)
        {
            return FillInDataTable(sqCmd, string.Empty);
        }

        public static DataTable FillInDataTable(SqlCommand sqCmd, string tbName)
        {
            DataSet ds = FillInDataSet(sqCmd, tbName);
            if (ds != null && ds.Tables.Count > 0)
                return ds.Tables[0];
            return null;

        }

        public static DataRow FillInDataRow(SqlCommand sqCmd)
        {
            DataTable tb = FillInDataTable(sqCmd);
            if (tb == null || tb.Rows.Count < 1) return null;
            return tb.Rows[0];
        }

        public static void LogError(String appDesc, String environment, Exception ex, String sourceHostName)
        {
            try
            {
                String shortMessage = ex.Message;
                String stackTrace = ex.StackTrace;
                String methodName = ex.TargetSite != null ? ex.TargetSite.Name : null;
                String className = ex.TargetSite != null ? ex.TargetSite.ReflectedType.ToString() : null;

                LogErrorInDB(appDesc, environment, "n/a",
                    "n/a",
                    0,
                    "n/a",
                    shortMessage,
                    stackTrace,
                    className + "." + methodName,
                    "n/a",
                    "n/a",
                    "n/a",
                    "n/a",
                    sourceHostName);

                Exception innerException = ex.InnerException;
                while (innerException != null)
                {
                    stackTrace += "\r\n======== INNER EXCEPTION ==================\r\n";
                    stackTrace += String.Format("Inner Exception Message: {0}\r\n", ex.InnerException.Message);
                    stackTrace += String.Format("Inner Exception Source: {0}\r\n", ex.InnerException.Source);
                    stackTrace += String.Format("Inner Exception TargetSite: {0}\r\n", ex.InnerException.TargetSite);
                    stackTrace += String.Format("Inner Exception StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                    stackTrace += "\r\n======== INNER EXCEPTION END ==================\r\n";

                    shortMessage += String.Format(" | {0}", innerException.Message);
                    innerException = innerException.InnerException;
                }

            }
            catch (Exception localException)
            {
                #region "  cannot log error, send email to webmasters "
                StringBuilder sbBody = new StringBuilder();
                sbBody.Append("ExceptionUtility.LogError method cannot log the following error to database.");
                sbBody.Append(String.Format("\nApplication : {0}", appDesc));
                sbBody.Append("\n");
                sbBody.Append("Exception ...\n\n");
                sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                sbBody.Append(string.Format("Error: {0}\n\n", localException.ToString()));
                sbBody.Append("\n\n ===================== unable to log error due to the following =======================\n\n");
                sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                sbBody.Append("Class Name : ExceptionUtility\n\n");
                sbBody.Append("Method Name : LogError(AnritsuAppsEnum enumApp, ExceptionApplicationType applicationType, Exception exception, bool emailNotification)\n");
                sbBody.Append(string.Format("Stack Trace {0}\n\n", localException.StackTrace));
                sbBody.AppendFormat("\n\nServer IP : {0}\n\n", System.Environment.MachineName);

                MailMessage message = new MailMessage();
                message.From = new MailAddress("WebMasterNoReply@anritsu.com");
                String[] toAddr = new String[] { ConfigurationManager.AppSettings["CoreLib.ErrorLogUtility.EmailTo"] };
                if (toAddr == null) toAddr = new String[] { "webmaster@anritsu.com" };
                foreach (String to in toAddr)
                {
                    message.To.Add(new MailAddress(to));
                }
                message.Subject = string.Format("Unable to log error on {0}-", appDesc, environment);
                message.Body = sbBody.ToString();
                message.IsBodyHtml = false;
                SmtpClient client = new SmtpClient();
                client.Send(message);
                #endregion
            }
        }

        private static void LogErrorInDB(String appDesc,
            String environment,
            String pageUrl,
            String httpReferer,
            Int32 httpCode,
            String clientIP,
            String shortMessage,
            String stackTrace,
            String method,
            String httpRequest,
            String userName,
            String clientHostName,
            String userAgent,
            String hostName
            )
        {
            String errorLogConnStr = ConfigurationManager.ConnectionStrings["CoreLib.ErrorLogUtility"].ConnectionString;
            using (SqlConnection conn = ConnOpen(errorLogConnStr))
            {
                SqlCommand cmd = MakeSPCmd(conn, null, "[dbo]", "[uSP_ExceptionLog_Add]", 0);
                cmd.Parameters.AddWithValue("@AppDescription", appDesc);
                cmd.Parameters.AddWithValue("@Environment", environment);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@HttpReferer", httpReferer);
                cmd.Parameters.AddWithValue("@HttpCode", httpCode);
                cmd.Parameters.AddWithValue("@ClientIP", clientIP);
                cmd.Parameters.AddWithValue("@ClientHostName", clientHostName);
                cmd.Parameters.AddWithValue("@ShortMessage", shortMessage);
                cmd.Parameters.AddWithValue("@StackTrace", stackTrace);
                cmd.Parameters.AddWithValue("@Method", method);
                cmd.Parameters.AddWithValue("@HttpRequest", httpRequest);
                cmd.Parameters.AddWithValue("@UserName", userName);

                cmd.Parameters.AddWithValue("@Host", hostName);
                cmd.Parameters.AddWithValue("@UserAgent", userAgent);
                cmd.Parameters.AddWithValue("@ExceptionLogApplicationTypeID", 1);

                cmd.ExecuteNonQuery();
            }
        }
    }
}
