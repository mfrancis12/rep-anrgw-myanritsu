﻿using System;
using Anritsu.AnrCommon.CoreLib;
using Scheduler.Lib;
using System.Data;
using SDK = Anritsu.AnrCommon.EmailSDK;
using System.Collections.Generic;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;

namespace ChangeEmailAddressScheduler
{
    class Program
    {
        static void Main()
        {
            //process all the change emails requests that are 72 hours old
            //get the request
            var changeRequest = UpdateEmailJobBll.Get72HoursOldEmailChangeReuqest();
            if (changeRequest == null || changeRequest.Rows.Count == 0) return;
            foreach (DataRow request in changeRequest.Rows)
            {
                //update email id's of all the users and send emails to admins and user's new email addresses.
                if (!String.IsNullOrEmpty(Convert.ToString(request["UserOldEmailAddress"])) &&
                    !String.IsNullOrEmpty(Convert.ToString(request["UserNewEmailAddress"])))
                {
                    try
                    {
                        var rowsEffected = UpdateEmailJobBll.UpdatUserEmailAddress(Convert.ToInt32(Convert.ToString(request["GWUserId"])),
                        Convert.ToString(request["UserOldEmailAddress"]),
                        Convert.ToString(request["UserNewEmailAddress"]),
                        Convert.ToInt32(request["RequestID"]));
                        if (rowsEffected > 0)
                        {
                            UpdateEmailJobBll.UpdateConnectUserEmailAddress(
                                Convert.ToInt32(Convert.ToString(request["GWUserId"])),
                                Convert.ToString(request["UserOldEmailAddress"]),
                                Convert.ToString(request["UserNewEmailAddress"]));
                      //      send emails 
                            SendEmailChangeComplete(Convert.ToString(request["UserNewEmailAddress"]),
                                Convert.ToString(request["FirstName"]),
                                Convert.ToString(request["LastName"]),
                                Convert.ToInt32(request["CultureGroupId"]));
                            SendEmailChangeCompleteAdminEmail(Convert.ToString(request["UserOldEmailAddress"]),
                                Convert.ToString(request["UserNewEmailAddress"]));
                        }
                    }
                    catch (Exception ex)
                    {
                        AppLogBll.LogError(ex);
                    }
                }
            }
        }
        private static void SendEmailChangeComplete(string email, string firstname, string lastname, int cultureGropuId)
        {
            #region "Send email using EmailAPI "

            var req = new SendTemplatedEmailCallRequest
            {
                CultureGroupId = cultureGropuId,
                EmailTemplateKey = ConfigUtility.AppSettingGetValue("EmailChangeRequestComplete"),
                ToEmailAddresses = new[] {email.Trim()}
            };

           

            #region " body replacement values "
            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]"},
                new ReplaceKeyValueType {ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]"}
            };

            req.BodyReplacementKeyValues = bodyR.ToArray();
            #endregion

            // Send an email with Email service
            SDK.EmailServiceManager.SendTemplatedEmail(req);

            #endregion
        }

        private static void SendEmailChangeCompleteAdminEmail(string oldUserEmail, string newUserEmail)
        {
            #region " send email(s) to web master/Account Admins"

            var email =
               new SendTemplatedDistributedEmailCallRequest
               {
                   EmailTemplateKey = ConfigUtility.AppSettingGetValue("EmailChangeRequestCompleteAdmin"),
                   DistributionListKey = ConfigUtility.AppSettingGetValue("EmailScheduler.ChangeEmailRequestComplete.DistributionKey"),
                   CultureGroupId = 1
               };

            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = oldUserEmail, TemplateKey = "[[OLDUSEREMAIL]]"},
                new ReplaceKeyValueType {ReplacementValue = newUserEmail, TemplateKey = "[[NEWUSEREMAIL]]"}
            };

            email.BodyReplacementKeyValues = bodyR.ToArray();
            SDK.EmailServiceManager.SendDistributedTemplatedEmail(email);

            #endregion
        }
    }
}
