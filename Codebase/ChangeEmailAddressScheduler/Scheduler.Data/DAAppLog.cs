﻿using System;
using System.Data.SqlClient;

namespace Scheduler.Data
{
    public static class DaAppLog
    {
        public static void LogError(
            String environment,
            String pageUrl,
            String httpReferer,
            Int32 httpCode,
            String clientIp,
            String shortMessage,
            String stackTrace,
            String method,
            String httpRequest,
            String userName,
            String clientHostName,
            String userAgent,
            String hostName
            )
        {
            using (var conn = DaBase.ExceptionDb.GetOpenedConn())
            {
                SqlCommand cmd = DaBase.ExceptionDb.MakeCmdSp(conn, null, DaBase.ExceptionDb.StoredProcs.ExceptionLogAdd);
                cmd.Parameters.AddWithValue("@AppDescription", "CONNECT");
                cmd.Parameters.AddWithValue("@Environment", environment);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@HttpReferer", httpReferer);
                cmd.Parameters.AddWithValue("@HttpCode", httpCode);
                cmd.Parameters.AddWithValue("@ClientIP", clientIp);
                cmd.Parameters.AddWithValue("@ClientHostName", clientHostName);
                cmd.Parameters.AddWithValue("@ShortMessage", shortMessage);
                cmd.Parameters.AddWithValue("@StackTrace", stackTrace);
                cmd.Parameters.AddWithValue("@Method", method);
                cmd.Parameters.AddWithValue("@HttpRequest", httpRequest);
                cmd.Parameters.AddWithValue("@UserName", userName);
                
                cmd.Parameters.AddWithValue("@Host", hostName);
                cmd.Parameters.AddWithValue("@UserAgent", userAgent);
                cmd.Parameters.AddWithValue("@ExceptionLogApplicationTypeID", 1);

                cmd.ExecuteNonQuery();
            }
        }
    }
}
