﻿using Anritsu.AnrCommon.CoreLib;
using System;
using System.Data.SqlClient;

namespace Scheduler.Data
{
    internal class DaBase
    {
        public class GlobalWebSsoDb
        {
            private static String Schema
            {
                get { return ConfigUtility.AppSettingGetValue("EmailScheduler.GlobalWebSsoDB.Schema"); }
            }

            private static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("EmailScheduler.GlobalWebSsoDB.ConnStr"); }
            }

            private static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("EmailScheduler.GlobalWebSsoDB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

            public static SqlConnection GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ConnStr);
            }

            public static SqlCommand MakeCmdSp(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
            }

            public class StoredProcs
            {
                public const string ChangeEmailLogGet = "uSP_GW_SSO_Get72HoursOld_EmailValidationLog";
                public const string UpdateUserEmail = "uSP_GW_SSO_User_Email_Update";
            }
        }

        public class ExceptionDb
        {
            private static String Schema
            {
                get { return ConfigUtility.AppSettingGetValue("EmailScheduler.ExceptionDb.Schema"); }
            }

            private static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("EmailScheduler.ExceptionDb.ConnStr"); }
            }

            private static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("EmailScheduler.ExceptionDb.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

            public static SqlConnection GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ConnStr);
            }

            public static SqlCommand MakeCmdSp(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
            }

            public class StoredProcs
            {
                public const String ExceptionLogAdd = "[uSP_ExceptionLog_Add]";
            }
        }

        public class ConnectDB
        {
            private static String ConnectConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
            }
            public static SqlConnection Connect_GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ConnectConnStr);
            }
            public static String SP_Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Connect.DB.SPSchema"); }
            }

            public static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
            }
            public class StoredProcs
            {

                public const string UpdateUserEmail = "uSP_Sec_UserMembership_SSO_Update";
            }
            public static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }
            public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, ConnectDB.SP_Schema, spName, ConnectDB.CommandTimeout);
            }
        }
    }
}
