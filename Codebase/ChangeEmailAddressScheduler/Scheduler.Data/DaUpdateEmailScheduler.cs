﻿using Anritsu.AnrCommon.CoreLib;
using System.Data;

namespace Scheduler.Data
{
    public static class DaUpdateEmailScheduler
    {
        public static DataTable Get72HoursOldEmailChangeReuqest()
        {
            using (var conn = DaBase.GlobalWebSsoDb.GetOpenedConn())
            {
                var cmd = DaBase.GlobalWebSsoDb.MakeCmdSp(conn, null, DaBase.GlobalWebSsoDb.StoredProcs.ChangeEmailLogGet);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
        public static int UpdatUserEmailAddress(int gwUserId, string userOldEmail, string userNewEmail, int requestId)
        {
            using (var conn = DaBase.GlobalWebSsoDb.GetOpenedConn())
            {
                var cmd = DaBase.GlobalWebSsoDb.MakeCmdSp(conn, null, DaBase.GlobalWebSsoDb.StoredProcs.UpdateUserEmail);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userOldEmail);
                cmd.Parameters.AddWithValue("@UserNewEmailAddress", userNewEmail);
                cmd.Parameters.AddWithValue("@RequestID", requestId);
                return cmd.ExecuteNonQuery();
            }
        }
        public static int UpdateConnectUserEmailAddress(int gwUserId, string userOldEmail, string userNewEmail)
        {
            using (var conn = DaBase.ConnectDB.Connect_GetOpenedConn())
            {
                var cmd = DaBase.ConnectDB.MakeCmdSP(conn, null, DaBase.ConnectDB.StoredProcs.UpdateUserEmail);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userOldEmail);
                cmd.Parameters.AddWithValue("@UserNewEmailAddress", userNewEmail);
                cmd.Parameters.AddWithValue("@IsEmailChange", 1);
                return cmd.ExecuteNonQuery();
            }
        }

    }
}
