﻿using Scheduler.Data;
using System;
using System.Web;
using EM = Anritsu.AnrCommon.EmailSDK;

namespace Scheduler.Lib
{
    public static class AppLogBll
    {
        public static void LogError(Exception exception)
        {
            if (exception == null) return;

            if (exception is HttpException)
            {
                var httpEx = (HttpException)exception;
                httpEx.GetHttpCode();
            }

            string shortMessage;
            string stackTrace = null;
            string methodName;
            string className = null;
            var ex = exception.GetBaseException();

            shortMessage = HttpUtility.HtmlEncode(ex.Message);

            if (ex.StackTrace != null && ex.StackTrace.Length > 0)
                stackTrace = HttpUtility.HtmlEncode(ex.StackTrace);

            if (ex.InnerException != null)
            {
                stackTrace += "\r\n======== INNER EXCEPTION ==================\r\n";
                stackTrace += String.Format("Inner Exception Message: {0}\r\n", ex.InnerException.Message);
                stackTrace += String.Format("Inner Exception Source: {0}\r\n", ex.InnerException.Source);
                stackTrace += String.Format("Inner Exception TargetSite: {0}\r\n", ex.InnerException.TargetSite);
                stackTrace += String.Format("Inner Exception StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                stackTrace += "\r\n======== INNER EXCEPTION END ==================\r\n";
            }

            methodName = ex.TargetSite != null ? ex.TargetSite.Name : null;
            if (ex.TargetSite != null && ex.TargetSite.ReflectedType != null)
                className = ex.TargetSite != null ? ex.TargetSite.ReflectedType.ToString() : null;

            try
            {
                DaAppLog.LogError("EmailUpdateScheduler", "",
                    "",
                    0,
                    "", shortMessage, stackTrace,
                    className + "." + methodName,
                    "",
                    "",
                    "",
                    "",
                    "");
            }
            catch
            {
               
            }
        }
    }
}
