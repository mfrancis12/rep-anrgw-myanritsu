﻿using System.Data;
using Scheduler.Data;

namespace Scheduler.Lib
{
    public static class UpdateEmailJobBll
    {
        public static DataTable Get72HoursOldEmailChangeReuqest()
        {
            return DaUpdateEmailScheduler.Get72HoursOldEmailChangeReuqest();
        }
        public static int UpdatUserEmailAddress(int gwUserId, string userOldEmail, string userNewEmail, int requestId)
        {
            return DaUpdateEmailScheduler.UpdatUserEmailAddress(gwUserId,userOldEmail,userNewEmail, requestId);
        }
        public static int UpdateConnectUserEmailAddress(int gwUserId, string userOldEmail, string userNewEmail)
        {
            return DaUpdateEmailScheduler.UpdateConnectUserEmailAddress(gwUserId, userOldEmail, userNewEmail);
        }
    }
}
