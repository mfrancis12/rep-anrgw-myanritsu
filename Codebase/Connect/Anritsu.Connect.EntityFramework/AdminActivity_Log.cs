//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdminActivity_Log
    {
        public long ActivityID { get; set; }
        public string EmailID { get; set; }
        public string AreaModified { get; set; }
        public string TaskPerformed { get; set; }
        public string ActionPerformed { get; set; }
        public string IPAddress { get; set; }
        public System.DateTime CreatedOnUTC { get; set; }
        public string ModelConfigType { get; set; }
    
        public virtual Cfg_ModelConfigType Cfg_ModelConfigType { get; set; }
    }
}
