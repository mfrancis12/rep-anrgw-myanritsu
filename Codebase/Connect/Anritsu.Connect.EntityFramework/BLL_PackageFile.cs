﻿namespace Anritsu.Connect.EntityFramework
{
   public static class BLL_PackageFile
    {
        private static PackageFile pkgFile = new PackageFile();

        /// <summary>
        /// Returns the Packagefile object according to id
        /// </summary>
        /// <param name="id">file id</param>
        /// <returns></returns>
        public static object GetPackageFileById(int id)
        {
            return pkgFile.GetPackageFileById(id);
        }


        /// <summary>
        /// Update the package file values to database
        /// </summary>
        /// <param name="packageFile"></param>
        /// <returns></returns>
        public static object UpdatePackageFileById(dynamic packageFile)
        {
            return pkgFile.UpdatePackageFileById(packageFile);
        }

        /// <summary>
        /// Lists all the package group names.
        /// </summary>
        /// <param name="packageFile"></param>
        /// <returns></returns>
        public static object GetPackageGrpNames(string pkGrp)
        {
            return pkgFile.PackageGroupNames(pkGrp);
        }

        /// <summary>
        /// Lists all the package names.
        /// </summary>
        /// <param name="packageFile"></param>
        /// <returns></returns>
        public static object GetPackageName(string pkName)
        {
            return pkgFile.PackageName(pkName);
        }
    }
}
