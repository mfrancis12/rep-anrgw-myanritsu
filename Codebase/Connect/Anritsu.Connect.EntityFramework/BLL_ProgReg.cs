﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.EntityFramework
{
   public class BLL_ProgReg
    {
        ProdReg prodReg=new ProdReg();

        public object GetSearchList(Dictionary<string, string> options)
        {
            return prodReg.GetSearchList(options);
        }

        public object BatchApprove(DataTable dtEntries, string statusCode,string updatedBy)
       {
           return prodReg.BatchApprove(dtEntries,statusCode,updatedBy);
       }
    }
}
