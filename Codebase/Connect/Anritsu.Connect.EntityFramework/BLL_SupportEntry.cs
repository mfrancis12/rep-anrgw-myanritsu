﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Anritsu.Connect.EntityFramework
{
    public static class BLL_SupportEntry
    {

        static ProductSupportEntity prodSupport = new ProductSupportEntity();
        //App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
        /// <summary>
        /// Returns list of model numbers with given modelconfigType and param
        /// </summary>
        /// <param name="param">substring of modelnumer 3 chars minimum</param>
        /// <param name="modelConfigType">ModelConfigType</param>
        /// <returns></returns>
        public static string ModelNumber(string param, string modelConfigType)
        {
            try
            {
                string str = "";//usr.FirstName;   
                var models = prodSupport.ModelNumber(param, modelConfigType);
                return JsonConvert.SerializeObject(models);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject("Error");
            }

        }

        /// <summary>
        /// Returns list of user emailaddress starting with param
        /// </summary>
        /// <param name="param">substring of email address</param>
        /// <returns></returns>
        public static string EmailAddresses(string param)
        {
            try
            {
                var models = prodSupport.EmailAddresses(param);
                return JsonConvert.SerializeObject(models);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject("Error");
            }

        }

        /// <summary>
        /// Returns list of organizations containing param
        /// </summary>
        /// <param name="param">substring of organization</param>
        /// <returns></returns>
        public static string Organizations(string param)
        {
            try
            {
                var orgs = prodSupport.Organizations(param);
                return JsonConvert.SerializeObject(orgs);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject("Error");
            }

        }

        /// <summary>
        /// Creates a new Product Support Entry in the database
        /// </summary>
        /// <param name="formValues"></param>
        /// <returns></returns>
        public static object AddProductSupportEntry(dynamic formValues)
        {
           return (int)prodSupport.AddProductSupportEntry(formValues);
        }
        /// <summary>
        /// Returns the list of Product Support types.
        /// </summary>
        /// <returns></returns>
        public static object GetProductSupportList()
        {
            return prodSupport.GetProductSupportList();
        }

        /// <summary>
        /// Returns the list of Product Support types. (Searching by Model/Serial/Email by Partial Match Search)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="serial"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public static object GetProductSupportList(String model, String serial, String email)
        {
            return prodSupport.GetProductSupportList(model, serial, email);
        }

        /// <summary>
        /// Returns the list of Product Support types. (Searching by Model/Serial/Email by Perfect Match Search)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="serial"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public static object GetProductSupportListAsPerfectMatching(String model, String serial, String email)
        {
            return prodSupport.GetProductSupportListAsPerfectMatching(model, serial, email);
        }

        public static IEnumerable<dynamic> GetProductSupportList(String supportTypeCode, Guid? accId, Int64 packageId, String modelNumber,out int totSelected)
        {
            return prodSupport.GetProductSupportList(supportTypeCode, accId, packageId, modelNumber,out totSelected);
        }

        public static List<ProductSupportEntity.ProductSupportListType> GetProductSupportList(String supportTypeCode, Guid? accId, Int64 packageId, String modelNumber, out int totSelected, String modelBySearchForm, String serialBySearchForm, String emailBySearchForm)
        {
            return prodSupport.GetProductSupportList(supportTypeCode, accId, packageId, modelNumber, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm);
        }

        /// <summary>
        /// Updates the product support entry in database
        /// </summary>
        /// <param name="formValues"></param>
        /// <returns></returns>
        public static object UpdateProductSupportEntry(dynamic formValues)
        {
            return prodSupport.UpdateProductSupportEntry(formValues);
        }

        /// <summary>
        /// Deletes the product support entry from database
        /// </summary>
        /// <param name="id">Id of product support entry</param>
        /// <returns></returns>
        public static object DeleteProductSupportEntry(string id)
        {
            return prodSupport.DeleteProductSupportEntry(id);
           
        }

        /// <summary>
        /// Gets mappings according to id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<SupportEntryMapping> GetMappings(int id)
        {
            return prodSupport.GetMappings(id);
        }

        public static List<SupportEntryMapping> GetMappings(int id, String supportTypeCode)
        {
            return prodSupport.GetMappings(id, supportTypeCode);
        }

        /// <summary>
        /// Returns membershipId related to emailaddress
        /// </summary>
        /// <param name="emailAddress">emailaddress</param>
        /// <returns></returns>
        public static string MemberShipId(string emailAddress)
        {
            return prodSupport.MemberShipId(emailAddress);
        }

        /// <summary>
        /// Returns support types
        /// </summary>
        /// <returns></returns>
        public static object GetSupportTypes()
        {
            return prodSupport.GetSupportTypes();
        }

        public static bool UserLevelPackageAccess(Int64 packageId, String supportTypeCode)
        {
            return prodSupport.UserLevelPackageAccess(packageId, supportTypeCode);
        }
        public static bool DeleteUserPackageAccess(Int64 packageId)
        {
            return prodSupport.DeleteUserPackageAccess(packageId);
        }

        public static object GetSerialNumbers(string model,string serial)
        {
            return prodSupport.GetSerialNumbers(model,serial);
        }

        public static object GetSupportTypeEntry(dynamic prodSupportType)
        {
            return prodSupport.GetSupportTypeEntry(prodSupportType);
        }

        public static object GetSupportTypesByModelSerial(string model, string serial, string email,Guid webtoken)
        {
            return prodSupport.GetSupportTypesByModelSerial(model, serial, email, webtoken);
        }

        public static object GetProductSupportEntry(Int32 supportTypeEntryId)
        {
            return prodSupport.GetProductSupportEntry(supportTypeEntryId);
        }
    }
}