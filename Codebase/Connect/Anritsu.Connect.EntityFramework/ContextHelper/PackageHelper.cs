﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.EntityFramework.ContextHelper
{
    public class PackageHelper
    {
        public static List<Package_Files> GetPackageFiles(long packaegId)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                return context.Package_Files.Where(f => f.PackageID == packaegId).ToList();
            }
        }
        public static void UpdatePackageFiles(Package_Files obj)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                var one = context.Package_Files.First(b => b.FileID == obj.FileID);
                one.ShowUpdateIcon = obj.ShowUpdateIcon;
                context.Entry(one).State=EntityState.Modified;
                int t=context.SaveChanges();
            }
        }
    }
}
