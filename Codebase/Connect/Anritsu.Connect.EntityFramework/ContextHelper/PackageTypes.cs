﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.EntityFramework.ContextHelper
{
    public static class PackageTypes
    {
        public static IList<Package_System> GetPackageTypes(String modelconfigtype)
        {
            using (var context=new  MyAnritsuDBEntities())
            {
                return context.Package_System.Where(ps => ps.ModelConfigType == modelconfigtype).ToList();
            }
        }
    }
}
