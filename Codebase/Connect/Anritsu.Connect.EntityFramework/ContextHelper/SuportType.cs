﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.Connect.EntityFramework.ContextHelper
{
    public static class SuportType
    {
        public static IList<SupportType> GetSupportTypes(String modelconfigtype)
        {
            using (var dbContext=new MyAnritsuDBEntities())
            {
                return dbContext.SupportTypes.Where(st => st.ModelConfigType == modelconfigtype).ToList();
            }
        }
    }
}
