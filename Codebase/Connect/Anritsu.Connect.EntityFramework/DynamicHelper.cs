﻿using System;

namespace Anritsu.Connect.EntityFramework
{
    public class DynamicHelper 
    {
        ProductSupportEntity prodSupport = new ProductSupportEntity();

        public object DynamicObject(dynamic formValues, string objectType,bool AddRecord)
        {
            if (objectType.Equals("SupportTypeEntry"))
            {
                var ste = new SupportTypeEntry();

                ste.AccountId = string.IsNullOrEmpty(Convert.ToString(formValues["OrgId"])) ? ste.AccountId : Guid.ParseExact(Convert.ToString(formValues["OrgId"]), "D");
                ste.MembershipId = string.IsNullOrEmpty(Convert.ToString(formValues["MembershipId"])) ? ste.MembershipId : Guid.ParseExact(Convert.ToString(formValues["MembershipId"]), "D");
                ste.ModelNumber = formValues["ModelNumber"];
                ste.SerialNumber = formValues["SerialNumber"];
                ste.ModelConfigType = formValues["ModelConfigType"];
                try
                {
                    ste.SupportTypeEntryId = string.IsNullOrEmpty(Convert.ToString(formValues["Id"])) ? ste.SupportTypeEntryId : Convert.ToInt32(formValues["Id"]);
                }
                catch 
                {
                    
                }
                
                if (AddRecord)
                {
                    ste.CreatedBy = formValues["CreatedBy"];
                    ste.CreatedOnUTC = DateTime.UtcNow;
                    ste.ModifiedOnUTC = DateTime.UtcNow;
                    ste.ModifiedBy = formValues["CreatedBy"];
                }
                else
                {
                    ste.ModifiedBy = formValues["CreatedBy"];
                    ste.ModifiedOnUTC = DateTime.UtcNow;
                }
                return ste;
            }
            return null;
        }

     
    }
}
