﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.EntityFramework
{
    class PackageFile
    {
        private readonly MyAnritsuDBEntities dbEntities = new MyAnritsuDBEntities();
        /// <summary>
        /// Returns the Packagefile object according to id
        /// </summary>
        /// <param name="id">file id</param>
        /// <returns></returns>
        public object GetPackageFileById(int id)
        {
            return dbEntities.Package_Files.Where(i => i.FileID.Equals(id)).FirstOrDefault();
        }

        /// <summary>
        /// Update the package file values to database
        /// </summary>
        /// <param name="packageFile"></param>
        /// <returns></returns>
        public object UpdatePackageFileById(dynamic packageFile)
        {
            var pkgFile = (Package_Files)GetPackageFileById(Convert.ToInt32(packageFile.FileID));
            pkgFile.ModifiedOnUTC = packageFile.ModifiedOnUTC;
            pkgFile.ShowNewIcon = packageFile.ShowNewIcon;
            pkgFile.ShowUpdateIcon = packageFile.ShowUpdateIcon;
            dbEntities.Entry(pkgFile).State = EntityState.Modified;
            int i = dbEntities.SaveChanges();
            return i;
        }

        public List<string> PackageGroupNames(string pkgGrp)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                return
                    context.Package_Master.Where(
                        k =>
                            k.PackageGroupName != null && k.PackageGroupName != string.Empty &&
                            k.PackageGroupName.StartsWith(pkgGrp)).Select(k =>
                                k.PackageGroupName
                        ).OrderByDescending(i => i).Distinct().ToList();
            }
        }

        public object PackageName(string packageName)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                return context.Package_Master.Where(k => k.PackageName != null && k.PackageName != string.Empty && k.PackageName.ToLower().Equals(packageName.ToLower())).Select(k =>
                    k.PackageName
                ).Any();
            }
        }
    }
}
