//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class Package_Models
    {
        public long PackageID { get; set; }
        public string ModelNumber { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOnUTC { get; set; }
    
        public virtual Package_Master Package_Master { get; set; }
    }
}
