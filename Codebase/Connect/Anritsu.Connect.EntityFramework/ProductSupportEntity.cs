﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using Newtonsoft.Json;
//using System.Data.Entity.Migrations;

namespace Anritsu.Connect.EntityFramework
{
    public class ProductSupportEntity : SupportTypeEntry
    {

        public class ProductSupportListType
        {
            public String ModelNumber { get; set; }
            public String SerialNumber { get; set; }
            public String EmailAdd_Org { get; set; }
            public int Id { get; set; }
            public Guid? MemberShipId { get; set; }
            public Guid? AccountId { get; set; }
            public DateTime? SuppDate { get; set; }
            public String SupportTypeCode { get; set; }
            public Boolean Selected { get; set; }
        }

        private readonly MyAnritsuDBEntities dbEntities = new MyAnritsuDBEntities();
        public string EmailOrg { get; set; }
        public string ModelNum { get; set; }
        public string SerialNum { get; set; }
        /// <summary>
        /// Returns list of model numbers with given modelconfigType and param
        /// </summary>
        /// <param name="param">substring of modelnumer 3 chars minimum</param>
        /// <param name="modelConfigType">ModelConfigType</param>
        /// <returns></returns>
        public List<string> ModelNumber(string param, string modelConfigType)
        {
            var models = new List<string>();

            models =
                dbEntities.Cfg_ModelConfig.Where(
                    i =>
                        i.ModelConfigType.ToLower().StartsWith(modelConfigType.ToLower()) &&
                        i.ModelNumber.Contains(param.ToLower())).Select(x => x.ModelNumber).ToList();

            if (models.Count > 0)
            {
                return models;
            }
            else
            {
                return models;
            }
        }

        /// <summary>
        /// Returns list of user emailaddress starting with param
        /// </summary>
        /// <param name="param">substring of email address</param>
        /// <returns></returns>
        public object EmailAddresses(string param)
        {


            dynamic emailAddresses =
                dbEntities.Sec_UserMembership.Where(i => i.EmailAddress.ToLower().StartsWith(param.ToLower())).Select(x => new { EmailAddress = x.EmailAddress, Id = x.MembershipId }).ToList();
            if (emailAddresses.Count > 0)
            {
                return emailAddresses;
            }
            else
            {
                return emailAddresses;
            }

        }

        /// <summary>
        /// Returns list of organizations containing param
        /// </summary>
        /// <param name="param">substring of organization</param>
        /// <returns></returns>
        public object Organizations(string param)
        {

            dynamic entity =
                dbEntities.Acc_AccountMaster.Where(
                    i =>
                        i.AccountName.ToLower().StartsWith(param.ToLower()) ||
                        i.CompanyName.ToLower().StartsWith(param.ToLower()))
                    .Select(
                        x =>
                            new
                            {
                                OrganizationId = x.AccountID,
                                OrganizationName = x.CompanyName + "-" + x.AccountName
                            })
                    .ToList();
            if (entity.Count > 0)
            {
                return entity;
            }
            else
            {
                HttpContext.Current.Response.StatusCode = 400;
                return HttpContext.Current.Response.Status;
            }


        }

        /// <summary>
        /// Adds Product support entry to database
        /// </summary>
        /// <param name="formValues">Json sent from client.</param>
        /// <returns></returns>
        public object AddProductSupportEntry(dynamic formValues)
        {
            var dynHelper = new DynamicHelper();

            var supTypeEntry = (SupportTypeEntry)dynHelper.DynamicObject(formValues, "SupportTypeEntry", true);
            var mapping = new List<SupportEntryMapping>();
            dynamic serialized = formValues["SupportType"];

            foreach (var obj in serialized)
            {
                var map = new SupportEntryMapping();

                map.ExpiryDate = !string.IsNullOrEmpty(Convert.ToString(obj.Value)) ? Convert.ToDateTime(obj.Value) : null;
                map.SupportTypeCode = obj.Key;
                map.SupportTypeEntry = supTypeEntry;
                supTypeEntry.SupportEntryMappings.Add(map);
            }
            dbEntities.SupportTypeEntries.Add(supTypeEntry);
            int saved = dbEntities.SaveChanges();
            return saved;


        }

        /// <summary>
        /// Updates the Product Support Entry fields.
        /// </summary>
        /// <param name="formValues"></param>
        /// <returns></returns>
        public object UpdateProductSupportEntry(dynamic formValues)
        {
            var dynHelper = new DynamicHelper();

            var parent = (SupportTypeEntry)dynHelper.DynamicObject(formValues, "SupportTypeEntry", false);
            using (var context = new MyAnritsuDBEntities())
            {
                parent.SupportTypeEntryId = Convert.ToInt32(formValues["Id"]);
                var parentRecord = context.SupportTypeEntries.Find(parent.SupportTypeEntryId);
                parentRecord.ModelNumber = parent.ModelNumber;
                parentRecord.SerialNumber = parent.SerialNumber;
                parentRecord.MembershipId = parent.MembershipId;
                parentRecord.AccountId = parent.AccountId;
                parentRecord.ModelConfigType = parent.ModelConfigType;
                parentRecord.ModifiedOnUTC = parent.ModifiedOnUTC;
                parentRecord.ModifiedBy = formValues["CreatedBy"];
                parentRecord.SupportEntryMappings.Clear();
                var mapping = new List<SupportEntryMapping>();
                dynamic serialized = formValues["SupportType"];
                foreach (dynamic obj in serialized)
                {

                    var map = new SupportEntryMapping();

                    map.ExpiryDate = !string.IsNullOrEmpty(Convert.ToString(obj.Value)) ? Convert.ToDateTime(obj.Value) : null;
                    map.SupportTypeCode = obj.Key;
                    map.SupportTypeEntry = parentRecord;

                    parentRecord.SupportEntryMappings.Add(map);
                }
                context.Entry(parentRecord).State = EntityState.Modified;
                int saved = context.SaveChanges();
                return saved;
            }

        }

        /// <summary>
        /// Deletes the product suppport entry from database.
        /// </summary>
        /// <param name="supportTypeEntryId"></param>
        /// <returns></returns>
        public object DeleteProductSupportEntry(string supportTypeEntryId)
        {
            var dynHelper = new DynamicHelper();

            var item = new SupportTypeEntry() { SupportTypeEntryId = Convert.ToInt32(supportTypeEntryId) };
            using (var context = new MyAnritsuDBEntities())
            {
                //var delRecord = (SupportTypeEntry)context.SupportTypeEntries.Find(item.SupportTypeEntryId);
                //if (delRecord != null)
                //{
                //    delRecord.SupportEntryMappings.Clear();
                //    delRecord.UserPackageAccesses.Clear();
                //}

                //context.SupportTypeEntries.Remove(delRecord);
                //int saved = context.SaveChanges();
                //return saved;
                String sql = String.Format("DELETE FROM cnt.SupportEntryMapping WHERE SupportTypeEntryId={0};DELETE FROM cnt.UserPackageAccess WHERE SupportTypeEntryId={0};DELETE FROM cnt.SupportTypeEntry WHERE SupportTypeEntryId={0}", supportTypeEntryId);
                return context.Database.ExecuteSqlCommand(sql);
            }

            return 0;
        }


        /// <summary>
        /// Gets the product suppport entry from database.
        /// </summary>
        /// <param name="supportTypeEntryId"></param>
        /// <returns></returns>
        public object GetProductSupportEntry(Int32 supportTypeEntryId)
        {
            var dynHelper = new DynamicHelper();

            var dynItem = new ProductSupportEntity();
            var item = new SupportTypeEntry() { SupportTypeEntryId = Convert.ToInt32(supportTypeEntryId) };
            using (var context = new MyAnritsuDBEntities())
            {
                var record = (SupportTypeEntry)context.SupportTypeEntries.Find(item.SupportTypeEntryId);

                dynItem.ModelNum = record.ModelNumber;
                dynItem.SerialNum = record.SerialNumber;
                if (record.MembershipId != null)
                {
                    dynItem.EmailOrg = record.Sec_UserMembership.EmailAddress;
                }
                if (record.AccountId != null)
                {
                    dynItem.EmailOrg = record.Acc_AccountMaster.AccountName;
                }

            }

            return dynItem;
        }

        /// <summary>
        /// Returns membershipid of the corresponding emailaddress requested
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public string MemberShipId(string emailAddress)
        {
            string memberShip = string.Empty;

            memberShip =
                dbEntities.Sec_UserMembership.Where(x => x.EmailAddress.ToLower().Equals(emailAddress.ToLower()))
                    .FirstOrDefault()
                    .MembershipId.ToString();


            return memberShip;
        }


        /// <summary>
        /// Returns list of Product support entries
        /// </summary>
        /// <returns></returns>
        public object GetProductSupportList()
        {

            return dbEntities.SupportTypeEntries.Select(spt => new
            {
                ModelNumber = spt.ModelNumber,
                SerialNumber = spt.SerialNumber,
                EmailAdd_Org = spt.Sec_UserMembership.EmailAddress ?? spt.Acc_AccountMaster.AccountName,
                Id = spt.SupportTypeEntryId,
                MemberShipId = spt.MembershipId,
                AccountId = spt.AccountId,
                CreatedDate = spt.CreatedOnUTC
            }).GroupJoin(
                      dbEntities.SupportEntryMappings.Select(
                          se =>
                              new
                              {
                                  SupportTypeEntryId = se.SupportTypeEntryId,
                                  Expiry = se.ExpiryDate,
                                  SupportTypeCode = se.SupportTypeCode,
                              }), e => e.Id, s => s.SupportTypeEntryId,
                      (e, s) => new
                      {
                          ModelNumber = e.ModelNumber,
                          SerialNumber = e.SerialNumber,
                          EmailAdd_Org = e.EmailAdd_Org,
                          MemberShipId = e.MemberShipId,
                          AccountId = e.AccountId,
                          CreatedDate = e.CreatedDate,
                          Id = e.Id,
                          SupportType = s
                      }).ToList().OrderByDescending(i => i.CreatedDate);


        }

        /// <summary>
        /// Returns list of Product support entries by searching Model,Serial,Email (Partial match search)
        /// </summary>
        /// <param name="modelNumber"></param>
        /// <param name="serialNumber"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public object GetProductSupportList(String modelNumber, String serialNumber, String emailAddress)
        {
            String upperModel = String.IsNullOrEmpty(modelNumber) ? String.Empty : modelNumber.ToUpper();
            String upperSerial = String.IsNullOrEmpty(serialNumber) ? String.Empty : serialNumber.ToUpper();
            String upperEMail = String.IsNullOrEmpty(emailAddress) ? String.Empty : emailAddress.ToUpper();

            var supportTypeEntries = dbEntities.SupportTypeEntries
                .Select(spt => new
            {
                ModelNumber = spt.ModelNumber,
                SerialNumber = spt.SerialNumber,
                EmailAdd_Org = spt.Sec_UserMembership.EmailAddress ?? spt.Acc_AccountMaster.AccountName,
                Id = spt.SupportTypeEntryId,
                MemberShipId = spt.MembershipId,
                AccountId = spt.AccountId,
                CreatedDate = spt.CreatedOnUTC
            })
                .Where(w => (String.IsNullOrEmpty(modelNumber) == false && w.ModelNumber.ToUpper().Contains(upperModel)) ||
                    String.IsNullOrEmpty(modelNumber))
                .Where(w => (String.IsNullOrEmpty(serialNumber) == false && w.SerialNumber.ToUpper().Contains(upperSerial)) ||
                    String.IsNullOrEmpty(serialNumber))
                .Where(w => (String.IsNullOrEmpty(emailAddress) == false && w.EmailAdd_Org.ToUpper().Contains(upperEMail)) ||
                    String.IsNullOrEmpty(emailAddress))
            .GroupJoin(
                      dbEntities.SupportEntryMappings.Select(
                          se =>
                              new SupportTypeDataForJPAdmin
                              {
                                  SupportTypeEntryId = se.SupportTypeEntryId,
                                  Expiry = se.ExpiryDate,
                                  SupportTypeCode = se.SupportTypeCode,
                              }), e => e.Id, s => s.SupportTypeEntryId,
                      (e, s) => new SupportTypeEntriesForJPAdmin
                      {
                          ModelNumber = e.ModelNumber,
                          SerialNumber = e.SerialNumber,
                          EmailAdd_Org = e.EmailAdd_Org,
                          MemberShipId = e.MemberShipId,
                          AccountId = e.AccountId,
                          CreatedDate = e.CreatedDate,
                          Id = e.Id,
                          SupportType = s
                      })
                      .ToList().OrderByDescending(i => i.CreatedDate);

            return supportTypeEntries;

        }

        /// <summary>
        /// Returns list of Product support entries by searching Model,Serial,Email (Partial match search)
        /// </summary>
        /// <param name="modelNumber"></param>
        /// <param name="serialNumber"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public object GetProductSupportListAsPerfectMatching(String modelNumber, String serialNumber, String emailAddress)
        {
            String upperModel = String.IsNullOrEmpty(modelNumber) ? String.Empty : modelNumber.ToUpper();
            String upperSerial = String.IsNullOrEmpty(serialNumber) ? String.Empty : serialNumber.ToUpper();
            String upperEMail = String.IsNullOrEmpty(emailAddress) ? String.Empty : emailAddress.ToUpper();

            var supportTypeEntries = dbEntities.SupportTypeEntries
                .Select(spt => new
                {
                    ModelNumber = spt.ModelNumber,
                    SerialNumber = spt.SerialNumber,
                    EmailAdd_Org = spt.Sec_UserMembership.EmailAddress ?? spt.Acc_AccountMaster.AccountName,
                    Id = spt.SupportTypeEntryId,
                    MemberShipId = spt.MembershipId,
                    AccountId = spt.AccountId,
                    CreatedDate = spt.CreatedOnUTC
                })
                .Where(w => (String.IsNullOrEmpty(modelNumber) == false && w.ModelNumber.ToUpper() == upperModel) ||
                    String.IsNullOrEmpty(modelNumber))
                .Where(w => (String.IsNullOrEmpty(serialNumber) == false && w.SerialNumber.ToUpper() == upperSerial) ||
                    String.IsNullOrEmpty(serialNumber))
                .Where(w => (String.IsNullOrEmpty(emailAddress) == false && w.EmailAdd_Org.ToUpper() == upperEMail) ||
                    String.IsNullOrEmpty(emailAddress))
            .GroupJoin(
                      dbEntities.SupportEntryMappings.Select(
                          se =>
                              new SupportTypeDataForJPAdmin
                              {
                                  SupportTypeEntryId = se.SupportTypeEntryId,
                                  Expiry = se.ExpiryDate,
                                  SupportTypeCode = se.SupportTypeCode,
                              }), e => e.Id, s => s.SupportTypeEntryId,
                      (e, s) => new SupportTypeEntriesForJPAdmin
                      {
                          ModelNumber = e.ModelNumber,
                          SerialNumber = e.SerialNumber,
                          EmailAdd_Org = e.EmailAdd_Org,
                          MemberShipId = e.MemberShipId,
                          AccountId = e.AccountId,
                          CreatedDate = e.CreatedDate,
                          Id = e.Id,
                          SupportType = s
                      })
                      .ToList().OrderByDescending(i => i.CreatedDate);

            return supportTypeEntries;

        }


        public IEnumerable<dynamic> GetProductSupportList(String supportTypeCode, Guid? accId, Int64 packageId, String modelNumber, out int totSelected)
        {
            var mdls = modelNumber.Split(';').ToList();
            var lst = dbEntities.SupportTypeEntries.Join(dbEntities.SupportEntryMappings, ste => ste.SupportTypeEntryId,
                 sm => sm.SupportTypeEntryId, (ste, sm) => new
                 {
                     ste.ModelNumber,
                     ste.SerialNumber,
                     EmailAdd_Org = ste.Sec_UserMembership.EmailAddress ?? ste.Acc_AccountMaster.AccountName,
                     Id = ste.SupportTypeEntryId,
                     MemberShipId = ste.MembershipId,
                     ste.AccountId,
                     SuppDate = sm.ExpiryDate,
                     sm.SupportTypeCode,
                     Selected = dbEntities.UserPackageAccesses.Any(i => i.SupportTypeEntryId.Equals(ste.SupportTypeEntryId) && i.PackageId.Equals(packageId))
                 }).Where(s => s.SupportTypeCode == supportTypeCode && s.MemberShipId.HasValue && mdls.Contains(s.ModelNumber)).ToList();

            totSelected = lst.Count(i => i.Selected);
            return lst;
        }

        public List<ProductSupportListType> GetProductSupportList(String supportTypeCode, Guid? accId, Int64 packageId, String modelNumber, out int totSelected, String modelBySearchForm, String serialBySearchForm, String emailBySearchForm)
        {
            String upperModel = String.IsNullOrEmpty(modelBySearchForm) ? String.Empty : modelBySearchForm.ToUpper();
            String upperSerial = String.IsNullOrEmpty(serialBySearchForm) ? String.Empty : serialBySearchForm.ToUpper();
            String upperEMail = String.IsNullOrEmpty(emailBySearchForm) ? String.Empty : emailBySearchForm.ToUpper();

            var mdls = modelNumber.Split(';').ToList();
            var lst = dbEntities.SupportTypeEntries.Join(dbEntities.SupportEntryMappings, ste => ste.SupportTypeEntryId,
                 sm => sm.SupportTypeEntryId, (ste, sm) => new ProductSupportListType()
                 {
                     ModelNumber = ste.ModelNumber,
                     SerialNumber = ste.SerialNumber,
                     EmailAdd_Org = ste.Sec_UserMembership.EmailAddress ?? ste.Acc_AccountMaster.AccountName,
                     Id = ste.SupportTypeEntryId,
                     MemberShipId = ste.MembershipId,
                     AccountId=ste.AccountId,
                     SuppDate = sm.ExpiryDate,
                     SupportTypeCode=sm.SupportTypeCode,
                     Selected = dbEntities.UserPackageAccesses.Any(i => i.SupportTypeEntryId.Equals(ste.SupportTypeEntryId) && i.PackageId.Equals(packageId))
                 }).Where(s => s.SupportTypeCode == supportTypeCode && s.MemberShipId.HasValue && mdls.Contains(s.ModelNumber))
                .Where(w => (String.IsNullOrEmpty(modelBySearchForm) == false && w.ModelNumber.ToUpper().Contains(upperModel)) ||
                    String.IsNullOrEmpty(modelBySearchForm))
                .Where(w => (String.IsNullOrEmpty(serialBySearchForm) == false && w.SerialNumber.ToUpper().Contains(upperSerial)) ||
                    String.IsNullOrEmpty(serialBySearchForm))
                .Where(w => (String.IsNullOrEmpty(emailBySearchForm) == false && w.EmailAdd_Org.ToUpper().Contains(upperEMail)) ||
                    String.IsNullOrEmpty(emailBySearchForm))
                 .ToList();

            totSelected = lst.Count(i => i.Selected);
            return lst;
        }



        /// <summary>
        /// Gets mappings according to id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<SupportEntryMapping> GetMappings(int id)
        {
            using (var entities = new MyAnritsuDBEntities())
            {
                return entities.SupportEntryMappings.Where(i => i.SupportTypeEntryId.Equals(id)).ToList();
            }
        }

        public List<SupportEntryMapping> GetMappings(int id, String supportTypeCode)
        {
            return dbEntities.SupportEntryMappings.Where(i => i.SupportTypeEntryId.Equals(id) && i.SupportTypeCode.Equals(supportTypeCode)).ToList();
        }

        /// <summary>
        /// Returns all support types.
        /// </summary>
        /// <returns></returns>
        public List<SupportType> GetSupportTypes()
        {
            return dbEntities.SupportTypes.ToList();
        }
        public bool UserLevelPackageAccess(Int64 packageId, String supportTypeCode)
        {
            return dbEntities.UserPackageAccesses.Any(upa => upa.PackageId.Equals(packageId) && upa.Package_Master.SupportTypeCode.Equals(supportTypeCode));
        }
        public bool DeleteUserPackageAccess(Int64 packageId)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                context.Database.ExecuteSqlCommand("Delete from [CNT].UserPackageAccess where PackageId=" + packageId);
                return context.SaveChanges() > 0;
            }
        }

        public object GetSerialNumbers(string model, string serial)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                var serials = context.Acc_ProductRegistered_Master.Where(m => m.MasterModel.Equals(model) && m.MasterSerial.ToLower().StartsWith(serial.ToLower())).Select(k => k.MasterSerial).ToList();
                return serials;
            }
        }
        public object GetSupportTypesByModelSerial(string model, string serial, string email, Guid webToken)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                string supTypes = string.Empty;
                var fltModelSer = context.SupportTypeEntries.Where(e => e.ModelNumber == model && e.SerialNumber == serial);
                var fltEmail = fltModelSer.Join(
                    context.Sec_UserMembership.Where(k => k.EmailAddress.Equals(email)), (s => s.MembershipId),
                    (u => u.MembershipId), (s, u) => new
                    {
                        MemberShipId = u.MembershipId,
                        SupportEntryId = s.SupportTypeEntryId
                    });
                if (fltEmail.Any())
                {
                    fltEmail.Join(context.SupportEntryMappings, (j => j.SupportEntryId), (m => m.SupportTypeEntryId),
                        (j, m) => new
                        {
                            SupportTypes = m.SupportTypeCode,
                            Expiry = m.ExpiryDate
                        }).ToList().ForEach(j =>
                        {
                            string exp = !string.IsNullOrEmpty(Convert.ToString(j.Expiry))
                                ? j.Expiry.Value.ToString("MM/dd/yyyy")
                                : "Not Set";
                            supTypes += j.SupportTypes + " - " + exp + ", ";
                        });
                }
                else
                {
                    var orgGuid = context.Acc_ProductRegistered_Master.First(k => k.WebToken == webToken).AccountID;
                    var fltOrg = fltModelSer.Where(i => i.AccountId == orgGuid);
                    fltOrg.Join(context.SupportEntryMappings, (j => j.SupportTypeEntryId), (m => m.SupportTypeEntryId),
                        (j, m) => new
                        {
                            SupportTypes = m.SupportTypeCode,
                            Expiry = m.ExpiryDate
                        }).ToList().ForEach(j =>
                        {
                            string exp = !string.IsNullOrEmpty(Convert.ToString(j.Expiry))
                                ? j.Expiry.Value.ToString("MM/dd/yyyy")
                                : "Not Set";
                            supTypes += j.SupportTypes + " - " + exp + ", ";
                        });
                }
                return supTypes.TrimEnd(new[] { ',', ' ' });
            }
        }
        public object GetSupportTypeEntry(dynamic prodSupport)
        {
            var dynHelper = new DynamicHelper();

            var obj = (SupportTypeEntry)dynHelper.DynamicObject(prodSupport, "SupportTypeEntry", false);
            if (obj.AccountId != null)
            {
                var list =
                    dbEntities.SupportTypeEntries.Where(i => i.ModelNumber == obj.ModelNumber && i.SerialNumber == obj.SerialNumber && i.AccountId == obj.AccountId).ToList();
                if (obj.SupportTypeEntryId != null)
                {
                    list = list.Where(i => i.SupportTypeEntryId != obj.SupportTypeEntryId).ToList();
                }
                return list.Count() > 0 ? true : false;
            }
            else
            {
                var list =
                    dbEntities.SupportTypeEntries.Where(i => i.ModelNumber == obj.ModelNumber && i.SerialNumber == obj.SerialNumber && i.MembershipId == obj.MembershipId).ToList();
                if (obj.SupportTypeEntryId != null)
                {
                    list = list.Where(i => i.SupportTypeEntryId != obj.SupportTypeEntryId).ToList();
                }
                return list.Count() > 0 ? true : false;
            }


        }

        //public object GetSupportEntryIdList(string modelNumber, string supportType)
        //{
        //    var list =
        //          dbEntities.SupportTypeEntries.Where(i => i.ModelNumber == modelNumber &).ToList();
        //}
    }
    public class Sup
    {
        public String ModelNumber;
        public String SerialNumber;
        public String EmailAdd_Org;
        public int Id;
        public Guid? MemberShipId;
        public Guid? AccountId;
        public DateTime? SuppDate;
        public string SupportTypeCode;
        public bool Selected;
    }

    public class SupportTypeEntriesForJPAdmin
    {
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public String EmailAdd_Org { get; set; }
        public Guid? MemberShipId { get; set; }
        public Guid? AccountId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int Id { get; set; }
        public IEnumerable<SupportTypeDataForJPAdmin> SupportType { get; set; }

    }

    public class SupportTypeDataForJPAdmin
    {
        public int SupportTypeEntryId { get; set; }
        public DateTime? Expiry { get; set; }
        public String SupportTypeCode { get; set; }
    }

}