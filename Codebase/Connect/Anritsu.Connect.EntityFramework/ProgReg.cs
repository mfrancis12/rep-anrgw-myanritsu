﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Anritsu.Connect.EntityFramework
{
    public class ProdReg
    {
        public object GetSearchList(Dictionary<string, string> options)
        {
            dynamic ob = null;
            string model = options["ModelNumber"].ToLower();
            string orgname = options["Orgname"].ToLower();
            string companyname = options["Companyname"].ToLower();
            string emailAddress = options["EmailAddress"].ToLower();
            using (var entities = new MyAnritsuDBEntities())
            {

                ob = entities.Acc_ProductRegistered_Master.Where(
                    p => p.MasterModel.ToLower().Equals(model))
                    .Join(entities.Acc_ProductRegistered_Item.Where(pi => pi.StatusCode.Equals("submitted")), (k => k.ProdRegID), (r => r.ProdRegID), (k, r) => new
                    {
                        ProdRegID = k.ProdRegID,
                        MasterModel = k.MasterModel,
                        MasterSerial = k.MasterSerial,
                        AccountID = k.AccountID
                    }).ToList().Join(
                        entities.Acc_AccountMaster.Where(
                            a =>
                                a.CompanyName.ToLower()
                                    .Equals(companyname) &&
                                a.AccountName.ToLower()
                                    .Equals(orgname)), (i => i.AccountID),
                        (j => j.AccountID), (i, j) => new
                        {
                            ProdRegId = i.ProdRegID,
                            ModelNumber = i.MasterModel,
                            Email = emailAddress,
                            SerialNumber = i.MasterSerial,
                            AccountId = i.AccountID
                        }).ToList()
                    .Join(
                        entities.Sec_UserAccountRole.Join(
                            entities.Sec_UserMembership.Where(
                                uem => uem.EmailAddress.ToLower().Equals(emailAddress)),
                            (ua => ua.MembershipId), (um => um.MembershipId), (ua, um) => new
                            {
                                AccountID = ua.AccountID
                            }), (k => k.AccountId), (u => u.AccountID), (k, u) => new
                            {
                                Id = k.ProdRegId,
                                ModelNumber = k.ModelNumber,
                                Email = k.Email,
                                SerialNumber = k.SerialNumber,
                                AccountId = k.AccountId,
                                Selected = false
                            }).ToList().Distinct();

            }
            return ob;
        }

        public object BatchApprove(DataTable dtEntries,string statusCode,string updatedby)
        {
            using (var context = new MyAnritsuDBEntities())
            {
                using (var conn = context.Database.Connection as SqlConnection)
                {
                    if (conn.State.Equals(ConnectionState.Closed))
                    {
                        conn.Open();
                    }
                    var cmd = new SqlCommand("[CNT].uSP_PRODUCTSTATUS_BULKAPPROVE",conn);
                    cmd.CommandType=CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StatusCode", statusCode);
                    cmd.Parameters.AddWithValue("@UpdatedBy", updatedby);
                    cmd.Parameters.AddWithValue("@ProductRegisteredList", dtEntries);
                    int i= cmd.ExecuteNonQuery();
                    if (conn.State.Equals(ConnectionState.Open))
                    {
                        conn.Close();
                    }
                    return i;
                }
            }
        }
    }
}
