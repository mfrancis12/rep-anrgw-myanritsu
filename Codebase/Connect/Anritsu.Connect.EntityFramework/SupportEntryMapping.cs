//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class SupportEntryMapping
    {
        public int SupportTypeEntryId { get; set; }
        public string SupportTypeCode { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
    
        public virtual SupportTypeEntry SupportTypeEntry { get; set; }
        public virtual SupportType SupportType { get; set; }
    }
}
