﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using Amazon.CloudWatch.Model;
using Amazon.S3.Model;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Data;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Lib.ProductRegistration;


namespace Anritsu.Connect.MigrationScript
{
    class Program
    {
        public static string SerialNumber
        {
            get { return ConfigurationManager.AppSettings["Connect.Web.MigrationScript.SerialNumber"]; }

        }

        public static string BasePath
        {
            get { return ConfigurationManager.AppSettings["JPDL.AWS.S3.JP.BasePath"].Replace("/",""); }
        }

        public static string ScriptCreated
        {
            get
            {
                return ConfigurationManager.AppSettings["Connect.Web.MigrationScript.Created"];
            }
        }

        static void Main(string[] args)
        {

            try
            {
                string filename = ConfigurationManager.AppSettings["MigrationXL"];

                //execcute this SP for JP user migrations [CNT].[JPUsers_Migration]

                SelectOptions(filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                LogWriter.LogError(string.Format("{0} stacktrace:{1}", ex.Message, ex.StackTrace));
            }
            Console.ReadLine();
        }

        private static void SelectOptions(string filename)
        {
            Console.WriteLine("Select options:");
            Console.WriteLine("Option 1 for migrating package downloads DL_forUser.");
            Console.WriteLine("Option 2 for migrating package downloads DLforSpecificUser.");
            Console.WriteLine("Option 3 for migrating package version matching.");
            Console.WriteLine("Option 4 to exit.");
            var option = Console.ReadLine();
            DataTable dtFileInfo = null;
            switch (Convert.ToInt32(option))
            {
               
                case 1:
                    Console.WriteLine("Started package downloads DL_forUser");
                    dtFileInfo = makeDataTableFromSheetName(filename, "◎FileComment");
                    MigratePackages(filename, "◎PackageInfo(DL_forUser)",4,dtFileInfo); SelectOptions(filename); //1dBLL_SupportEntry call
                    //LogWriter.LogError("Completed downloads");
                    Console.WriteLine("Completed package downloads");
                    break;
                case 2:
                    Console.WriteLine("Started package downloads DLforSpecificUser");
                       dtFileInfo = makeDataTableFromSheetName(filename, "◎FileComment");
                      MigratePackages(filename, "◎PackageInfo(DLforSpecificUser)", 4, dtFileInfo); 
                    
                    SelectOptions(filename); //1dBLL_SupportEntry call
                    //LogWriter.LogError("Completed downloads");
                    Console.WriteLine("Completed package downloads");
                    break;
                case 3:
                    Console.WriteLine("Started Version matching");
                    //LogWriter.LogError("Started Version matching");
                    dtFileInfo = makeDataTableFromSheetName(filename, "◎FileComment(VMTF)");
                    MigratePackages(filename, "◎Package Information（VMTF)",6,dtFileInfo); SelectOptions(filename);//1dBLL_SupportEntry call
                    //LogWriter.LogError("Completed Version matching");
                    Console.WriteLine("Completed Version matching");
                    break;
                default: Environment.Exit(0);
                    break;

            }
        }

        private static void MigratePackages(string filename, string sheetname, int sysId, DataTable dtFileInfo)
        {
            DataTable dt = makeDataTableFromSheetName(filename, sheetname);
          
            Console.WriteLine("Excel file reading completed.Started to create packages.");
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    //Console.WriteLine(i.ToString() + " package creating for path " + Convert.ToString(dr["Package Path"]));
                    PackageMigrationScript(i.ToString(), Convert.ToString(dr["Package Group"]),
                        Convert.ToString(dr["Package Name"]), BasePath + Convert.ToString(dr["Package Path"]), sysId,
                        Convert.ToString(dr["Assign Model"]), Convert.ToString(dr["SupportType"]), dtFileInfo);
                    Console.WriteLine(i.ToString() + " package created for path " + Convert.ToString(dr["Package Path"]));
                    i++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    LogWriter.LogError("Package Migration");
                    LogWriter.LogError(string.Format("{0} stacktrace:{1} , values:package {2},packagepath:{3} Model :{4}", ex.Message, ex.StackTrace, Convert.ToString(dr["Package Name"]), Convert.ToString(dr["Package Path"]), Convert.ToString(dr["Assign Model"])));
                }
            }
        }

        
        private static void MigrateSpecificUsers(string filename)
        {
            DataTable dt = makeDataTableFromSheetName(filename, "◎SpecificUser");
            Console.WriteLine("Excel file reading completed");
            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    Console.WriteLine(i.ToString() + " Assigning packages ");
                    UserPackageAccess(Convert.ToString(dr["ModelName"]), Convert.ToString(dr["PackageName"]), Convert.ToString(dr["Customer_EmailAddress"]), Convert.ToString(dr["SupportType"]), Convert.ToInt32(dr["Restrict Package Access to Specific Users"]));
                    Console.WriteLine(i.ToString() + " Assigning packages done ");
                    i++;
                }
                catch (Exception ex)
                {
                    LogWriter.LogError("Specific Users Migration");
                    LogWriter.LogError(ex.Message + string.Format(" customer email:{0} and package name {1} ", Convert.ToString(dr["Customer_EmailAddress"]), Convert.ToString(dr["PackageName"])));
                    Console.WriteLine(ex.Message + string.Format(" customer email:{0} and package name {1} ", Convert.ToString(dr["Customer_EmailAddress"]), Convert.ToString(dr["PackageName"])));
                }
            }
        }

        
        private static DataTable makeDataTableFromSheetName(string filename, string sheetName)
        {
            DataTable dtImport = new DataTable();
            using (System.Data.OleDb.OleDbConnection myConnection = new System.Data.OleDb.OleDbConnection(
                        "Provider=Microsoft.ACE.OLEDB.12.0; " +
                         "data source='" + filename + "';" +
                            "Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\" "))
            {


                using (System.Data.OleDb.OleDbDataAdapter myImportCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [" + sheetName + "$]", myConnection))
                    myImportCommand.Fill(dtImport);
            } return dtImport;
        }

        private static DataTable makeDataTableFromSheetNameByGroup(string filename, string sheetName)
        {
            DataTable dtImport = new DataTable();
            using (System.Data.OleDb.OleDbConnection myConnection = new System.Data.OleDb.OleDbConnection(
                        "Provider=Microsoft.ACE.OLEDB.12.0; " +
                         "data source='" + filename + "';" +
                            "Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\" "))
            {


                using (System.Data.OleDb.OleDbDataAdapter myImportCommand = new System.Data.OleDb.OleDbDataAdapter("select ModelName,UserEmailAddress,RegistDate from [" + sheetName + "$] group by ModelName,UserEmailAddress,RegistDate", myConnection))
                    myImportCommand.Fill(dtImport);
            } return dtImport;
        }

        static void PackageMigrationScript(string slno, string packageGroup, string packageName, string packagePath, int packageType, string assignedModel, string supportType, DataTable dtFilesInfo)
        {
            //pull the files under the package
            List<S3Object> s3Objects = PackageBLL.GetPackaeFileList(packagePath.Substring(0, packagePath.Length - 1), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
            var filtered = from item in s3Objects
                           select new { item.Key, item.Size, LastModified = Convert.ToDateTime(item.LastModified) };

            if (s3Objects.Count == 0)
            {
                //Raise no Files under selected package error message
                Console.WriteLine("No Files under selected package for " + slno.ToString());
                return;
            }

            //create Files list
            DataTable dtPackageFiles = ToFileListDataTable(filtered, dtFilesInfo);

            //create Package models
            DataTable dtPackageModel = GetModelTable(assignedModel);
            //construct Package Object
            var pkgpath = packagePath.Substring(0, packagePath.Length - 1);
            var objPackage = new Package(0, packageName,
                  pkgpath,
                  ConvertUtility.ConvertToInt32(packageType, 0), true,
                  ModelConfigTypeInfo.CONFIGTYPE_JP, ConvertUtility.ConvertNullToEmptyString(supportType), packageGroup);
            JpS3Settings.InsertPackageWithFiles(objPackage, dtPackageFiles, ScriptCreated, dtPackageModel,
                    "PackageFileDescription");
        }

        
        static void UserPackageAccess(string modelNumber, string packageName, string email, string supportType, int restrictAccess)
        {
            if (!restrictAccess.Equals(1))
            {
                return;
            }
            JpS3Settings.GetSupportEntryId(email, supportType, modelNumber, packageName);
        }

        static bool IsValidEmail(string email)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool GetSupportType(dynamic formValues)
        {
            return BLL_SupportEntry.GetSupportTypeEntry(formValues);
        }
      

        public static DataTable ToFileListDataTable(dynamic s3Objects, DataTable dtfiles)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Key");
            dataTable.Columns.Add("LastModified");
            dataTable.Columns.Add("Size");
            dataTable.Columns.Add("ShowNewIcon");
            dataTable.Columns.Add("ShowUpdateIcon");
            dataTable.Columns.Add("ModifiedOnUTC");
            dataTable.Columns.Add("JPComments");
            dataTable.Columns.Add("ENCommetns");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            foreach (var n in s3Objects)
            {
                try
                {
                    var dr = dataTable.NewRow();
                    dr["Key"] = n.Key;
                    var fileinfo =
                        dtfiles.AsEnumerable()
                            .Where(
                                i =>
                                    ((Convert.ToString(i["File Path"]).EndsWith("/") ? BasePath + Convert.ToString(i["File Path"]) : BasePath + Convert.ToString(i["File Path"]) + "/") + Convert.ToString(i["Filename"])).Equals(
                                        n.Key)).Select(i => new
                                        {
                                            shownew = i["Show \"New\" Icon"],
                                            showUpt = i["Show \"Update\" Icon"],
                                            modified = i["Last Modified"],
                                            updated = i["Updated"],
                                            jpCommetns = i["Comment(Jp)"],
                                            enCommetns = i["Comment(en)"]
                                        }).OrderByDescending(i => i.updated).FirstOrDefault();
                    //if (fileinfo == null) continue;

                    dr["ShowNewIcon"] = (fileinfo != null)?fileinfo.shownew:false;
                    dr["ShowUpdateIcon"] = (fileinfo != null)?fileinfo.showUpt:false;
                    dr["ModifiedOnUTC"] = (fileinfo != null)?fileinfo.modified:null;
                    dr["LastModified"] = n.LastModified.ToUniversalTime();
                    dr["Size"] = n.Size;
                    dr["JPComments"] = (fileinfo != null)?fileinfo.jpCommetns:String.Empty;
                    dr["ENCommetns"] = (fileinfo != null)?fileinfo.enCommetns:String.Empty;
                    dataTable.Rows.Add(dr);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            Thread.CurrentThread.CurrentCulture = currentCulture;
            return dataTable;
        }

        public static String GetFileList(DataTable dtPackageFiles)
        {
            var sbFilelist = new StringBuilder();
            if (dtPackageFiles == null || dtPackageFiles.Rows.Count == 0) return String.Empty;
            for (var i = 0; i < dtPackageFiles.Rows.Count; i++)
            {
                if (i != dtPackageFiles.Rows.Count - 1)
                    sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], "") + ",");
                else
                    sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], ""));
            }
            return sbFilelist.ToString();
        }

        public static DataTable GetModelTable(string modelList)
        {
            if (String.IsNullOrEmpty(modelList)) return null;
            String[] models = modelList.Split(';');
            if (models.Length == 0) return null;
            var dtModels = new DataTable();
            dtModels.Columns.Add("ModelNumber");
            foreach (String model in models)
            {
                DataRow drModel = dtModels.NewRow();
                drModel["ModelNumber"] = model;
                dtModels.Rows.Add(drModel);
            }
            return dtModels;
        }


    }

    public static class JpS3Settings
    {
        public static String ActiveRegion
        {
            get { return "JP"; }
        }

        public static String AwsConfigType
        {
            get { return "JPDL"; }
        }

        public static String BasePath
        {
            get { return ConfigUtility.AppSettingGetValue(AwsConfigType + ".AWS.S3." + ActiveRegion + ".BasePath"); }
        }

        public static long InsertPackageWithFiles(Package package, DataTable packageFiles, String createdBy, DataTable dtPackageModels, string classKey)
        {
            long packageID = 0;
            using (SqlConnection connection = GetOpenedConn())
            {
                SqlTransaction transaction;
                //Start transaction.
                // transaction = connection.BeginTransaction("InsertPackageWithFiles");
                SqlCommand cmd = new SqlCommand("cnt.uSP_Package_Insert_Migrate");
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Transaction = transaction;
                try
                {
                    cmd.Parameters.AddWithValue("@PackageName", package.PackageName);
                    if (!string.IsNullOrEmpty(package.PackageGroupName))
                        cmd.Parameters.AddWithValue("@PackageGroupName", package.PackageGroupName);
                    cmd.Parameters.AddWithValue("@PackagePath", package.PackagePath);
                    cmd.Parameters.AddWithValue("@SystemID", package.SystemID);
                    cmd.Parameters.AddWithValue("@IsEnabled", package.Enabled);
                    cmd.Parameters.AddWithValue("@ModalConfigType", package.ModelConfigType);

                    SqlParameter param = new SqlParameter();
                    param.TypeName = "[CNT].FileListTable1";
                    param.ParameterName = "@FileTable";
                    param.SqlDbType = SqlDbType.Structured;
                    param.Value = packageFiles;
                    //   param.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(param);

                    SqlParameter param1 = new SqlParameter();
                    param1.TypeName = "[CNT].PackageModelTable";
                    param1.ParameterName = "@PackageModels";
                    param1.SqlDbType = SqlDbType.Structured;
                    param1.Value = dtPackageModels;
                    // param.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(param1);


                    if (!String.IsNullOrEmpty(package.SupportTypeCode))
                        cmd.Parameters.AddWithValue("@SupportTypeCode", package.SupportTypeCode);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    cmd.Parameters.AddWithValue("@ResourceClass", classKey);
                    //cmd.Parameters.AddWithValue("@JPContentStr", newContentJp);
                    //cmd.Parameters.AddWithValue("@enContentStr", newContentEn);
                    //cmd.Parameters.AddWithValue("@FilePath", filePath);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    //int effected = cmd.ExecuteNonQuery();
                    //transaction.Commit();
                }
                catch (Exception ex)
                {
                    // transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }
                return packageID;
            }
        }

        public static DataRow GetSupportEntryId(string email, string supporttype, string model, String packageName)
        {

            using (SqlConnection connection = GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("InsertUserPackageAccess");
                SqlCommand cmd = MakeCmdSP(connection, transaction, "uSP_GetSupportEntry_EmailModel");
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                try
                {
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@Model", model);
                    cmd.Parameters.AddWithValue("@SupportType", supporttype);
                    cmd.Parameters.AddWithValue("@PackageName", packageName);
                    DataRow r = SQLUtility.FillInDataRow(cmd);
                    if (r == null) throw new ApplicationException();

                    transaction.Commit();
                    return r;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }

            }
        }


        public static long GetPackageId(string packageName, Int32 supportEntryId)
        {
            long PackageId;
            using (SqlConnection connection = GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("GetPackageId");
                SqlCommand cmd = MakeCmdSP(connection, transaction, "[uSP_GetPackageId_PackageName_Migration]");
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                try
                {
                    cmd.Parameters.AddWithValue("@PackageName", packageName);
                    cmd.Parameters.AddWithValue("@SupportEntryId", supportEntryId);
                    DataRow r = SQLUtility.FillInDataRow(cmd);
                    if (r == null) throw new ApplicationException();
                    PackageId = ConvertUtility.ConvertToInt64(r["packageid"], 0);


                    if (PackageId > 0)
                        transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }
                return PackageId;
            }
        }

        public static Int32 Insert(Guid accountID,
            String mn,
            String sn,
            String createdBy,
            Dictionary<String, String> additionalData,
            Boolean validateSN,
            out Guid webToken, out Int32 regActiveCount,
            DateTime regDateTime, String regSNPattern = null)
        {
            webToken = Guid.Empty;
            regActiveCount = 0;

            if (validateSN)
            {
                Boolean isInGlobalSerialNumberSystem = Data.DAProductInfo.IsValidMnSn(mn.Trim(), sn.Trim(), !validateSN);
                Regex reg = new Regex(regSNPattern);
                Match m = reg.Match(sn);
                if (!isInGlobalSerialNumberSystem && !m.Success)
                {
                    return -2;//invalid sn
                }
            }

            DataRow r = Insert(accountID, mn, sn, createdBy, additionalData, regDateTime);
            if (r == null) return 0;
            Int32 prodRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
            webToken = ConvertUtility.ConvertToGuid(r["WebToken"], Guid.Empty);
            regActiveCount = ConvertUtility.ConvertToInt32(r["RegActiveCount"], 0);
            return prodRegID;
        }

        public static Guid CreateAccount(Guid managerUserMembershipId
           , String accountName
          , String companyName
           , String companyNameInRuby
           , String addressNameCAddr
           , String address1CAddr
           , String address2CAddr
           , String cityTownCAddr
           , String stateProvinceCAddr
           , String zipPostalCodeCAddr
           , String countryCodeCAddr
           , String phoneNumberCAddr
           , String faxNumberCAddr
           , String addedFromIP
           , String addedByEmail
           , String addedByName
          )
        {
            Guid accountID = Guid.Empty;
            SqlTransaction tran;
            using (SqlConnection conn = GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    DataRow accRow = Insert(accountName, managerUserMembershipId,
                        companyName, companyNameInRuby, addedFromIP, addedByEmail, addedByName,
                        conn, tran, addressNameCAddr, address1CAddr, address2CAddr
                            , cityTownCAddr, stateProvinceCAddr, zipPostalCodeCAddr, countryCodeCAddr, phoneNumberCAddr, faxNumberCAddr);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            return accountID;
        }


        public static Int32 UpdateUserPackageAcces(long packId, Int32 supportEntryId)
        {
            SqlConnection conn = GetOpenedConn();
            SqlTransaction tran = conn.BeginTransaction("Updateuserpackage");
            SqlCommand cmd = MakeCmdSP(conn, tran, "[uSP_Package_User_Migration]");
            cmd.Parameters.AddWithValue("@PackageId", packId);
            cmd.Parameters.AddWithValue("@SupportEntryId", supportEntryId);
            return cmd.ExecuteNonQuery();
        }


        private static DataRow Insert(String accountName,
           Guid managerMembershipID,
           String companyName,
           String companyNameInRuby,
           String addedFromIP,
           String addedByEmail,
           String addedByName,
           SqlConnection conn, SqlTransaction tran, String addressName
            , String address1
            , String address2
            , String cityTown
            , String stateProvince
            , String zipPostalCode
            , String countryCode
            , String phoneNumber
            , String faxNumber)
        {
            SqlCommand cmd = MakeCmdSP(conn, tran, "[uSP_Acc_AccountMaster_Insert_Migration]");
            cmd.Parameters.AddWithValue("@AccountName", accountName);
            cmd.Parameters.AddWithValue("@CompanyName", companyName.Trim());
            cmd.Parameters.AddWithValue("@CompanyNameInRuby", companyNameInRuby.Trim());
            cmd.Parameters.AddWithValue("@MembershipId", managerMembershipID);
            cmd.Parameters.AddWithValue("@AddressName", addressName.Trim());
            cmd.Parameters.AddWithValue("@Address1", address1.Trim());
            cmd.Parameters.AddWithValue("@Address2", address2.Trim());
            cmd.Parameters.AddWithValue("@CityTown", cityTown.Trim());
            cmd.Parameters.AddWithValue("@StateProvince", stateProvince.Trim());
            cmd.Parameters.AddWithValue("@ZipPostalCode", zipPostalCode.Trim());
            cmd.Parameters.AddWithValue("@CountryCode", countryCode.Trim());
            cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber.Trim());
            cmd.Parameters.AddWithValue("@FaxNumber", faxNumber.Trim());
            DataRow r = SQLUtility.FillInDataRow(cmd);
            if (r == null) return null;
            Guid accountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            return r;
        }
       

        public static DataRow Insert(Guid accountID, String mn, String sn, String createdBy, Dictionary<String, String> additionalData, DateTime regDateTime)
        {
            if (accountID.IsNullOrEmptyGuid() || mn.IsNullOrEmptyString() || sn.IsNullOrEmptyString()) return null;

            SqlTransaction tran = null;
            DataRow regData = null;
            using (SqlConnection conn = GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {

                    SqlCommand cmd = MakeCmdSP(conn, tran, "[uSP_Acc_ProductRegistered_Master_Insert_Migration]");
                    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                    //cmd.Parameters.AddWithValue("@Description", desc.Trim());
                    cmd.Parameters.AddWithValue("@MasterModel", mn.Trim());
                    cmd.Parameters.AddWithValue("@MasterSerial", sn.Trim());
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    cmd.Parameters.AddWithValue("@CreatedonUTC", regDateTime);
                    regData = SQLUtility.FillInDataRow(cmd);
                    Int32 prodRegID = ConvertUtility.ConvertToInt32(regData["ProdRegID"], 0);
                    if (prodRegID > 0 && additionalData != null && additionalData.Count > 0)
                    {
                        foreach (String datakey in additionalData.Keys)
                        {
                            DAAcc_ProductRegistered_Data.Insert(prodRegID, datakey,
                                ConvertUtility.ConvertNullToEmptyString(additionalData[datakey]).Trim(),
                                conn, tran
                                );
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                return regData;
            }
        }

        public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
        {
            return SQLUtility.MakeSPCmd(conn, tran, DB.ConnectDB.SP_Schema, spName, DB.ConnectDB.CommandTimeout);
        }
        public static SqlConnection GetOpenedConn()
        {
            return SQLUtility.ConnOpen(ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"));
        }
    }
    static class DB
    {
        public class ConnectDB
        {
            public static String SP_Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Connect.DB.SPSchema"); }
            }

            public static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
            }

            public static Int32 CommandTimeout
            {
                get
                {
                    int timeout = 0;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }
        }

        public class GlobalWebDB
        {
            public static String SP_Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Connect.DB.GlobalWeb.SPSchema"); }
            }

            public static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.GlobalWeb.ConnStr"); }
            }

            public static Int32 CommandTimeout
            {
                get
                {
                    int timeout = 0;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.GlobalWeb.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

        }
    }

    public class LogWriter
    {
        private static string m_exePath = string.Empty;
        public static void LogError(string logMessage)
        {
            LogWrite(logMessage);
        }
        public static void LogWrite(string logMessage)
        {
            m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + "Log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("Error Test  :{0}", logMessage);
                txtWriter.WriteLine("--------------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }
}
