﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_AccountMaster
    {
        public static DataRow SelectAccountHistoryByID(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.SelectAccountHistoryByID);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        
        public static DataTable SelectAllFiltered(Guid accountId, 
            String companyName, 
            String orgName,
            String companyCountryCode,
            String accountStatusCode, 
            Boolean? isVerifiedAndApproved)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.AccountMaster_SelectAllFiltered);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", companyName.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@OrgName", orgName.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyCountryCode", companyCountryCode.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@AccountStatusCode", accountStatusCode.Trim());
                if (isVerifiedAndApproved.HasValue)
                    SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsVerifiedAndApproved", (Boolean)isVerifiedAndApproved);
                else
                    cmd.Parameters.AddWithValue("@IsVerifiedAndApproved", DBNull.Value);
                return SQLUtility.FillInDataTable(cmd);
            }
        }


        public static DataTable SearchFilteredResult(Guid accountId,
            String companyName,
            String orgName,
            String companyCountryCode,
            String accountStatusCode,
            Boolean? isVerifiedAndApproved)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.AccountMaster_SearchFilteredResult);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", companyName.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@OrgName", orgName.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyCountryCode", companyCountryCode.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@AccountStatusCode", accountStatusCode.Trim());
                if (isVerifiedAndApproved.HasValue)
                    SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsVerifiedAndApproved", (Boolean)isVerifiedAndApproved);
                else
                    cmd.Parameters.AddWithValue("@IsVerifiedAndApproved", DBNull.Value);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByMembershipId(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.AccountMaster_SelectByMembershipId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByAccountID(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.SelectByAccountID);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectByCompany(String companyName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.SelectByCompany);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", companyName);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static Guid CreateAccount(Guid managerUserMembershipId
            , String accountName
           , String companyName
            , String companyNameInRuby
            , String addedFromIp
            , String addedByEmail
            , String addedByName
           )
        {
            var accountId = Guid.Empty;
            using (var conn = DABase.GetOpenedConn())
            {
               
                var accRow = Insert(accountName, managerUserMembershipId,
                        companyName, companyNameInRuby, addedFromIp, addedByEmail, addedByName, 
                        conn);
                if (accRow != null) accountId = ConvertUtility.ConvertToGuid(accRow["AccountID"], Guid.Empty);
            }
            return accountId;
        }

        private static DataRow Insert(String accountName, 
            Guid managerMembershipId, 
            String companyName,
            String companyNameInRuby,
            String addedFromIp, 
            String addedByEmail, 
            String addedByName
            )
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return Insert(accountName, 
                    managerMembershipId, 
                    companyName,
                    companyNameInRuby,
                    addedFromIp,
                    addedByEmail,
                    addedByName,
                    conn);
            }
        }

        private static DataRow Insert(String accountName, 
            Guid managerMembershipId, 
            String companyName,
            String companyNameInRuby, 
            String addedFromIp, 
            String addedByEmail, 
            String addedByName, 
            SqlConnection conn)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn,null,DABase.SP.Acc.Insert);
            cmd.Parameters.AddWithValue("@AccountName", accountName);
            cmd.Parameters.AddWithValue("@CompanyName", companyName.Trim());
            cmd.Parameters.AddWithValue("@CompanyNameInRuby", companyNameInRuby.Trim());
            cmd.Parameters.AddWithValue("@MemberShipID", managerMembershipId);

            DataRow r = SQLUtility.FillInDataRow(cmd);
            if (r == null) return null;
            Guid accountId = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            if (!accountId.IsNullOrEmptyGuid())
            {
                DASec_UserAccountRole.Insert(managerMembershipId, accountId, "accmgr"
                    , addedByEmail
                    , addedByName
                    , addedFromIp
                    , conn, null);
                DASec_UserAccountRole.Insert(managerMembershipId, accountId, "accuser"
                   , addedByEmail
                   , addedByName
                   , addedFromIp
                   , conn, null);
            }
            return r;
        }

        public static Int32 Update(Guid accountId,
            String accountName,            
            String companyName,
            String companyNameInRuby,
            Boolean isCreateHistory)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.Update);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.Parameters.AddWithValue("@AccountName", accountName);
                cmd.Parameters.AddWithValue("@CompanyName", companyName.Trim());
                cmd.Parameters.AddWithValue("@CompanyNameInRuby", companyNameInRuby.Trim());
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@CreateHistory", isCreateHistory);
                return cmd.ExecuteNonQuery();
            }
           
        }

        public static void VerifyAccount(Guid accountId, String verifiedBy, String internalComment)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.UpdateApprove);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.Parameters.AddWithValue("@ModifiedBy", verifiedBy.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@InternalComment", internalComment.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }

        }

        public static void UnVerifyAccount(Guid accountId, String unVerifiedBy, String internalComment)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.UpdateDisApprove);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.Parameters.AddWithValue("@ModifiedBy", unVerifiedBy.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@InternalComment", internalComment.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }

        }

        

        public static void AddUserToExistingAccount(Guid accountId, Guid membershipId, String role
            , String addedFromIpAddress, String addedByEmail, String addedByName)
        {
            DASec_UserAccountRole.Insert(membershipId, accountId, role, addedByEmail, addedByName, addedFromIpAddress);
        }

        public static Int32 Delete(Guid accountId)
        {
            SqlTransaction tran = null;
            try
            {
                using (SqlConnection conn = DABase.GetOpenedConn())
                {
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Acc.Delete);
                    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                    Int32 affectedRows = cmd.ExecuteNonQuery();
                    tran.Commit();
                    return affectedRows;
                }

            }
            catch (Exception ex)
            {
                if (tran != null) tran.Rollback();
                throw ex;
            }

            
        }

        //public static void UpdateCompanyAddressID(Guid accountId, Int32 companyAddressId, SqlConnection conn, SqlTransaction tran)
        //{
        //    SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Acc.UpdateCompanyAddressID);
        //    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
        //    cmd.Parameters.AddWithValue("@CompanyAddressID", companyAddressId);
        //    cmd.ExecuteNonQuery();
        //}

        public static DataTable UserMembership_SelectByAccountID_ForUser(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectByAccountID_ForUser);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void UpdateAgreeTerms(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.UpdateAgreeTerms);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateNotes(Guid accountId, String additionalNotes)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.UpdateNotes);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.Parameters.AddWithValue("@AdditionalNotes", additionalNotes.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable AccountVerifications_Select(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.AccountVerifications_Select);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAccountsByPackageID(long? packageId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Acc.SelectAccountsByPackageID);
                if(packageId.HasValue)
                SQLUtility.SqlParam_AddAsLongInt(ref cmd, "@PackageID", packageId.Value);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

    }
}
