﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_Account_Config_DistPortal
    {
        public static DataRow Select(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_Account_Config_DistPortal.Select);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Insert(Guid accountId, String statusCode,
            Boolean sisAccessTypePublic, Boolean sisAccessTypePrivate, Int32 sisProdFilter, Int32 sisDocTypeFilter, String createdBy)
        {
            if (sisProdFilter < 1) sisProdFilter = 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_Account_Config_DistPortal.Insert);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@StatusCode", statusCode);
                SQLUtility.AddParam(ref cmd, "@SIS_AccessType_Public", sisAccessTypePublic);
                SQLUtility.AddParam(ref cmd, "@SIS_AccessType_Private", sisAccessTypePrivate);
                SQLUtility.AddParam(ref cmd, "@SIS_ProdFilter", sisProdFilter, 0);
                SQLUtility.AddParam(ref cmd, "@SIS_DocTypeFilter", sisDocTypeFilter, 0);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Update(Guid accountId, String statusCode,
            Boolean sisAccessTypePublic, Boolean sisAccessTypePrivate, Int32 sisProdFilter, Int32 sisDocTypeFilter, String modifiedBy)
        {
            if (sisProdFilter < 1) sisProdFilter = 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_Account_Config_DistPortal.Update);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@StatusCode", statusCode);
                SQLUtility.AddParam(ref cmd, "@SIS_AccessType_Public", sisAccessTypePublic);
                SQLUtility.AddParam(ref cmd, "@SIS_AccessType_Private", sisAccessTypePrivate);
                SQLUtility.AddParam(ref cmd, "@SIS_ProdFilter", sisProdFilter, 0);
                SQLUtility.AddParam(ref cmd, "@SIS_DocTypeFilter", sisDocTypeFilter, 0);
                SQLUtility.AddParam(ref cmd, "@ModifiedBy", modifiedBy);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
