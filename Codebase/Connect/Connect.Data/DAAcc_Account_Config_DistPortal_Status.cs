﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_Account_Config_DistPortal_Status
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_Account_Config_DistPortal_Status.SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
