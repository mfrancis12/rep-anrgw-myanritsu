﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegCart
    {
        public static DataRow SelectByWebAccessKey(Guid webAccessKey, out Dictionary<DataRow, DataTable> items)
        {
            items = null;
            if (webAccessKey.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByWebAccessKey(webAccessKey, out items, conn, null);
            }
        }

        public static DataRow SelectByWebAccessKey(Guid webAccessKey, out Dictionary<DataRow, DataTable> items, SqlConnection conn, SqlTransaction tran)
        {
            items = null;
            if (webAccessKey.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCart_SelectByWebAccessKey);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", webAccessKey);
            DataRow r = db.FillInDataRow(cmd);

            if (r == null) return null;
            Int32 prodRegCartId = ConvertUtility.ConvertToInt32(r["ProdRegCartID"], 0);
            DataTable tbItems = DAAcc_ProductRegCartItem.SelectByProdRegCartID(conn, tran, prodRegCartId);
            if (tbItems == null) return null;
            items = new Dictionary<DataRow, DataTable>();
            foreach (DataRow rItem in tbItems.Rows)
            {
                Int32 cartItemId = ConvertUtility.ConvertToInt32(rItem["CartItemID"], 0);
                DataTable tbItemData = DAAcc_ProductRegCartItemData.SelectByCartItemID(conn, tran, cartItemId);
                if (tbItemData != null)
                {
                    items.Add(rItem, tbItemData);
                }
            }

            return r;
        }

        public static DataTable SelectByAccountID(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCart_SelectByAccountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Insert(Guid accountId, String latestUserEmail, String modelNumber)
        {

            if (accountId.IsNullOrEmptyGuid() || latestUserEmail.IsNullOrEmptyString() || modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCart_Insert);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@LatestUserEmail", latestUserEmail.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Update(Guid cartWebAccessKey, String latestUserEmail)
        {
            if (cartWebAccessKey.IsNullOrEmptyGuid() || latestUserEmail.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCart_Update);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", cartWebAccessKey);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@LatestUserEmail", latestUserEmail.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Delete(Guid webAccessKey, SqlConnection conn, SqlTransaction tran)
        {
            if (webAccessKey.IsNullOrEmptyGuid()) return 0;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCart_Delete);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", webAccessKey);
            return cmd.ExecuteNonQuery();
        }

        public static Int32 Delete(Guid webAccessKey)
        {

            if (webAccessKey.IsNullOrEmptyGuid()) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return Delete(webAccessKey, conn, null);
            }
        }
    }
}
