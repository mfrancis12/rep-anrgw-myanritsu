﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegCartItem
    {
        public static DataTable SelectByProdRegCartID(SqlConnection conn, SqlTransaction tran, Int32 prodRegCartId)
        {
            if (prodRegCartId < 1) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCartItem_SelectByProdRegCartID);
            cmd.Parameters.AddWithValue("@ProdRegCartID", prodRegCartId);
            return db.FillInDataTable(cmd);
        }

        //public static DataRow Insert(Int32 prodRegCartID, String modelNumber, String serialNumber)
        //{
        //    if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
        //    DBConnect db = new DBConnect();
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        return Insert(prodRegCartID, modelNumber, serialNumber);
        //    }
        //}

        //public static void Insert(Int32 prodRegCartID, Dictionary<String,String> modelSerialNumbers)
        //{
        //    if (modelSerialNumbers == null) return;
        //    DBConnect db = new DBConnect();
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        foreach (String mn in modelSerialNumbers.Keys)
        //        {
        //            if (mn.IsNullOrEmptyString()) continue;
        //            String sn = modelSerialNumbers[mn];
        //            if (sn.IsNullOrEmptyString()) continue;
        //           Insert(prodRegCartID, mn.Trim(), sn.Trim());
        //        }
        //    }
        //}

        //public static DataRow Insert(Guid cartWebAccessKey, String modelNumber, String serialNumber)
        //{
        //    if (cartWebAccessKey.IsNullOrEmptyGuid() || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
        //    DBConnect db = new DBConnect();
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCart_SelectByWebAccessKey);
        //        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", cartWebAccessKey);
        //        DataRow r = db.FillInDataRow(cmd);

        //        if (r == null) return null;
        //        Int32 prodRegCartID = ConvertUtility.ConvertToInt32(r["ProdRegCartID"], 0);

        //        return Insert(conn, null, prodRegCartID, modelNumber, serialNumber);
        //    }
        //}

        public static Int32 Insert(Guid cartWebAccessKey, String modelNumber, List<String> serialNumbers, Boolean snValidationNotRequired)
        {
            Int32 totalImported = 0;
            if (cartWebAccessKey.IsNullOrEmptyGuid() || modelNumber.IsNullOrEmptyString()
                || serialNumbers == null || serialNumbers.Count < 1) return 0;
            DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCart_SelectByWebAccessKey);
                    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", cartWebAccessKey);
                    DataRow r = db.FillInDataRow(cmd);

                    if (r == null) return 0;
                    Int32 prodRegCartId = ConvertUtility.ConvertToInt32(r["ProdRegCartID"], 0);
                    foreach (String sn in serialNumbers)
                    {
                        if (sn.IsNullOrEmptyString()) continue;
                        Boolean isInGlobalSerialNumberSystem = DAProductInfo.IsValidMnSn(modelNumber.Trim(), sn.Trim(), snValidationNotRequired);
                        DataRow snRow = Insert(conn, tran, prodRegCartId, modelNumber, sn.Trim(), isInGlobalSerialNumberSystem);
                        if (snRow != null) totalImported++;
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            return totalImported;
        }

        internal static DataRow Insert(SqlConnection conn, SqlTransaction tran, Int32 prodRegCartId, String modelNumber, String serialNumber
            , Boolean isInGlobalSerialNumberSystem)
        {
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            Int32 serialCheckStatus = isInGlobalSerialNumberSystem ? 1 : 0;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCartItem_Insert);
            cmd.Parameters.AddWithValue("@ProdRegCartID", prodRegCartId);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
            SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", serialNumber.Trim());
            cmd.Parameters.AddWithValue("@SerialCheckStatus", serialCheckStatus);
            return db.FillInDataRow(cmd);
        }

        public static void Delete(Int32 cartItemId)
        {
            if (cartItemId < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCartItem_Delete);
                cmd.Parameters.AddWithValue("@CartItemID", cartItemId);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteAllByCartWebAccessKey(Guid cartWebAccessKey)
        {
            if (cartWebAccessKey.IsNullOrEmptyGuid()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegCartItem_DeleteAllByWebAccessKey);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@CartWebAccessKey", cartWebAccessKey);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateAddOnFormStatus(Int32 cartItemId, Boolean isCompleted)
        {
            if (cartItemId < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                UpdateAddOnFormStatus(cartItemId, isCompleted, conn, null);
            }
        }

        public static void UpdateAddOnFormStatus(Int32 cartItemId, Boolean isCompleted, SqlConnection conn, SqlTransaction tran)
        {
            if (cartItemId < 1) return;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCartItem_UpdateAddOnFormStatus);
            cmd.Parameters.AddWithValue("@CartItemID", cartItemId);
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsAddOnFormCompleted", isCompleted);
            cmd.ExecuteNonQuery();
        }

        public static void UpdateAddOnFormStatus(List<Int32> cartItemIDs, Boolean isCompleted)
        {
            if (cartItemIDs == null || cartItemIDs.Count < 1) return;
            DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    foreach (Int32 cartItemId in cartItemIDs)
                    {
                        UpdateAddOnFormStatus(cartItemId, isCompleted, conn, tran);
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }
    }
}
