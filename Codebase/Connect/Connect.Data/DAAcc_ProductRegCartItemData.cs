﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegCartItemData
    {
        public static DataTable SelectByCartItemID(SqlConnection conn, SqlTransaction tran, Int32 cartItemId)
        {
            if (cartItemId < 1) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCartItemData_SelectByCartItemID);
            cmd.Parameters.AddWithValue("@CartItemID", cartItemId);
            return db.FillInDataTable(cmd);
        }

        public static DataRow InsertUpdate(Int32 cartItemId, String dataKey, String dataValue)
        {
            if (cartItemId < 1 || dataKey.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return InsertUpdate(conn, null, cartItemId, dataKey, dataValue);
            }
        }

        public static void InsertUpdate(Int32 cartItemId, Dictionary<String,String> data)
        {
            if (cartItemId < 1 || data == null) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                foreach (String dataKey in data.Keys)
                {
                    if (dataKey.IsNullOrEmptyString()) continue;
                    InsertUpdate(conn, null, cartItemId, dataKey.Trim(), data[dataKey]);
                }
            }
        }

        internal static DataRow InsertUpdate(SqlConnection conn, SqlTransaction tran, Int32 cartItemId, String dataKey, String dataValue)
        {
            if (cartItemId < 1 || dataKey.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegCartItemData_InsertUpdate);
            cmd.Parameters.AddWithValue("@CartItemID", cartItemId);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@DataKey", dataKey.Trim());
            SQLUtility.SqlParam_AddAsString(ref cmd, "@DataValue", dataValue.Trim());
            return SQLUtility.FillInDataRow(cmd);
        }
    }
}
