﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegWorkflow
    {
        ////public static DataRow WFMaster_Select(Guid wfInstID)
        ////{
        ////    if (wfInstID.IsNullOrEmptyGuid()) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Master_Select);
        ////        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WFInstID", wfInstID);
        ////        return db.FillInDataRow(cmd);
        ////    }
        ////}

        ////public static DataTable WFMaster_SelectStatus(Guid accountID)
        ////{
        ////    if (accountID.IsNullOrEmptyGuid()) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Master_SelectStatus);
        ////        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
        ////        return db.FillInDataTable(cmd);
        ////    }
        ////}

        ////public static DataRow WFMaster_Insert(Guid accountID, String modelNumber, List<String> serialNumbers, String userEmailAddress, String userIPAddress)
        ////{
        ////    if (accountID.IsNullOrEmptyGuid()) return null;
        ////    if (modelNumber.IsNullOrEmptyString()) return null;
        ////    if (serialNumbers == null || serialNumbers.Count < 1 || serialNumbers[0].IsNullOrEmptyString()) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        return WFMaster_Insert(accountID, modelNumber, serialNumbers, userEmailAddress, userIPAddress, conn, null);
        ////    }
        ////}

        ////public static DataRow WFMaster_Insert(Guid accountID, String modelNumber, List<String> serialNumbers
        ////    , String userEmailAddress, String userIPAddress, SqlConnection conn, SqlTransaction tran)
        ////{
        ////    if (accountID.IsNullOrEmptyGuid()) return null;
        ////    DBConnect db = new DBConnect();
        ////    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegWorkflow_Master_Insert);
        ////    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
        ////    SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        ////    SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumbers", ConvertUtility.ConvertToDelimitedString(serialNumbers, ","));
        ////    SQLUtility.SqlParam_AddAsString(ref cmd, "@UserEmailAddress", userEmailAddress);
        ////    SQLUtility.SqlParam_AddAsString(ref cmd, "@UserIPAddress", userIPAddress);

        ////    return db.FillInDataRow(cmd);
        ////}

        ////public static DataRow WFTask_Select(Int32 wfTaskID)
        ////{
        ////    if (wfTaskID < 1) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Task_Select);
        ////        cmd.Parameters.AddWithValue( "@WFTaskID", wfTaskID);
        ////        return db.FillInDataRow(cmd);
        ////    }
        ////}

        ////public static DataTable WFTask_SelectByWF(Guid wfInstID)
        ////{
        ////    if (wfInstID.IsNullOrEmptyGuid()) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Task_SelectByWF);
        ////        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WFInstID", wfInstID);
        ////        return db.FillInDataTable(cmd);
        ////    }
        ////}

        ////public static Int32 WFTask_Insert(Guid wfInstID, String taskType, Dictionary<String,String> taskData)
        ////{
        ////    if (wfInstID.IsNullOrEmptyGuid() || String.IsNullOrEmpty(taskType) || taskData == null || taskData.Count < 1) return 0;
        ////    DBConnect db = new DBConnect();
        ////    SqlTransaction tran = null;
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        tran = conn.BeginTransaction();
        ////        try
        ////        {
        ////            Int32 taskID = WFTask_Insert(wfInstID, taskType, taskData, conn, tran);
        ////            tran.Commit();
        ////            return taskID;
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            tran.Rollback();
        ////            throw ex;
        ////        }


        ////    }
        ////}

        ////public static Int32 WFTask_Insert(Guid wfInstID, String taskType, Dictionary<String, String> taskData, SqlConnection conn, SqlTransaction tran)
        ////{
        ////    if (wfInstID.IsNullOrEmptyGuid() || String.IsNullOrEmpty(taskType) || taskData == null || taskData.Count < 1) return 0;
        ////    DBConnect db = new DBConnect();
        ////    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegWorkflow_Task_Insert);
        ////    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WFInstID", wfInstID);
        ////    SQLUtility.SqlParam_AddAsString(ref cmd, "@TaskType", taskType);
        ////    DataRow rTask = db.FillInDataRow(cmd);

        ////    if (rTask == null) throw new ArgumentException("Unable to add the task");
        ////    Int32 taskID = ConvertUtility.ConvertToInt32(rTask["WFTaskID"], 0);
        ////    if (taskID < 1) throw new ArgumentException("Unable to add the task");

        ////    cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegWorkflow_TaskData_Insert);
        ////    cmd.Parameters.AddWithValue("@WFTaskID", taskID);
        ////    cmd.Parameters.AddWithValue("@DataKey", String.Empty);
        ////    cmd.Parameters.AddWithValue("@DataValue", String.Empty);
        ////    foreach (String key in taskData.Keys)
        ////    {
        ////        cmd.Parameters["@DataKey"].Value = key.Trim();
        ////        cmd.Parameters["@DataValue"].Value = taskData[key].Trim();
        ////        cmd.ExecuteNonQuery();
        ////    }
        ////    return taskID;
        ////}

        ////public static void WFTask_UpdateAsCompleted(Int32 wfTaskID)
        ////{
        ////    if (wfTaskID < 1) return;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Task_UpdateAsCompleted);
        ////        cmd.Parameters.AddWithValue("@WFTaskID", wfTaskID);
        ////        cmd.ExecuteNonQuery();
        ////    }
        ////}

        ////public static Boolean WF_Insert(Guid accountID
        ////    , String userEmailAddress
        ////    , String userIPAddress
        ////    , String companyInfo
        ////    , String modelNumber
        ////    , List<String> serialNumbers
        ////    , out Guid wfInstID
        ////    , out Boolean verifyCompany
        ////    , out Boolean verifyProductSN
        ////    , out String productOwnerRegion
        ////    )
        ////{
        ////    if (accountID.IsNullOrEmptyGuid()) throw new ArgumentException("accountID");
        ////    if (userEmailAddress.IsNullOrEmptyString()) throw new ArgumentException("userEmailAddress");
        ////    if (companyInfo.IsNullOrEmptyString()) throw new ArgumentException("companyInfo");
        ////    if (userIPAddress.IsNullOrEmptyString()) throw new ArgumentException("userIPAddress");
        ////    if (serialNumbers == null || serialNumbers.Count < 1) throw new ArgumentException("serialNumbers");
        ////    wfInstID = Guid.Empty;
        ////    verifyCompany = false;
        ////    verifyProductSN = false;
        ////    productOwnerRegion = "";
        ////    DBConnect db = new DBConnect();
        ////    SqlTransaction tran = null;
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        tran = conn.BeginTransaction();
        ////        try
        ////        {
        ////            #region " account info "
        ////            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Acc.SelectByAccountID);
        ////            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
        ////            DataRow accountRow = SQLUtility.FillInDataRow(cmd);
        ////            if (accountRow == null) throw new ArgumentException("Invalid accountID.");
        ////            Boolean isVerifiedAndApproved = ConvertUtility.ConvertToBoolean(accountRow["IsVerifiedAndApproved"], false); 
        ////            #endregion

        ////            #region " product config "
        ////            cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Erp_ProductConfig_SelectByModelNumber);
        ////            SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        ////            DataRow mnRow = db.FillInDataRow(cmd);
        ////            if (mnRow == null) throw new ArgumentException("Unsupported model number.");
        ////            verifyCompany = ConvertUtility.ConvertToBoolean(mnRow["VerifyCompany"], false);
        ////            verifyProductSN = ConvertUtility.ConvertToBoolean(mnRow["VerifyProductSN"], false);
        ////            productOwnerRegion = ConvertUtility.ConvertNullToEmptyString(mnRow["ProductOwnerRegion"]);
        ////            #endregion

        ////            if (!verifyCompany && !verifyProductSN)// US products
        ////            {
        ////                bool allReigstered = DAAcc_ProductRegistered.Insert(accountID, modelNumber, serialNumbers, "active", conn, tran);
        ////                if (!allReigstered) throw new ArgumentException("Unable to register products.");
        ////            }
        ////            else
        ////            {
        ////                #region " workflow master "
        ////                DataRow wfMasterRow = WFMaster_Insert(accountID, modelNumber, serialNumbers
        ////                    , userEmailAddress, userIPAddress, conn, tran);
        ////                if (wfMasterRow == null) throw new ArgumentException("Unable to create new workflow.");
        ////                wfInstID = ConvertUtility.ConvertToGuid(wfMasterRow["WFInstID"], Guid.Empty);
        ////                if (wfInstID.IsNullOrEmptyGuid()) throw new ArgumentException("Unable to create new workflow.");
        ////                #endregion

        ////                if (verifyCompany && !isVerifiedAndApproved)
        ////                {
        ////                    #region " company info verification task "
        ////                    Dictionary<String, String> companyInfoTaskData = new Dictionary<string, string>();
        ////                    companyInfoTaskData.Add("MN", modelNumber.Trim());
        ////                    companyInfoTaskData.Add("SNList", ConvertUtility.ConvertToDelimitedString(serialNumbers, ","));
        ////                    companyInfoTaskData.Add("CompanyInfo", companyInfo.Trim());
        ////                    WFTask_Insert(wfInstID, "VerifyCompanyTask", companyInfoTaskData, conn, tran);
        ////                    #endregion
        ////                }

        ////                if (verifyProductSN)
        ////                {
        ////                    #region " prod reg "
        ////                    foreach (String sn in serialNumbers)
        ////                    {
        ////                        #region " product registration verification task "
        ////                        if (sn.IsNullOrEmptyString()) continue;
        ////                        Dictionary<String, String> productSNTaskData = new Dictionary<string, string>();
        ////                        productSNTaskData.Add("MN", modelNumber.Trim());
        ////                        productSNTaskData.Add("SN", sn.Trim());
        ////                        //add more here 
        ////                        WFTask_Insert(wfInstID, "VerifyProductTask", productSNTaskData, conn, tran);                                
        ////                        #endregion

        ////                    }
        ////                    #endregion
        ////                }
        ////            }
        ////            tran.Commit();
        ////            return true;
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            tran.Rollback();
        ////            //throw ex;
        ////            return false;
        ////        }


        ////    }
        ////}

        ////public static DataTable WFTaskData_SelectByTaskID(Int32 taskID)
        ////{
        ////    if (taskID < 1) return null;
        ////    DBConnect db = new DBConnect();
        ////    using (SqlConnection conn = db.GetOpenedConn())
        ////    {
        ////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_TaskData_SelectByWFTaskID);
        ////        cmd.Parameters.AddWithValue("@WFTaskID", taskID);
        ////        return db.FillInDataTable(cmd);
        ////    }
        ////}

        //////public static Int32 WFTask_Delete(Int32 wfTaskID)
        //////{
        //////    if (wfTaskID < 1) return 0;
        //////    DBConnect db = new DBConnect();
        //////    using (SqlConnection conn = db.GetOpenedConn())
        //////    {
        //////        SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegWorkflow_Task_);
        //////        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WFInstID", wfInstID);
        //////        return db.FillInDataTable(cmd);
        //////    }
        //////}
    }
}
