﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered
    {
        public static DataTable SelectStatus()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredStatus_SelectAll);
                return db.FillInDataTable(cmd);
            }
        }

        //public static DataTable SelectWithPaging(Guid accountID
        //    , Int32 startRowNum
        //    , Int32 pageSize
        //    , String sortExpression)
        //{
        //    //if (accountID.IsNullOrEmptyGuid()) return null;
        //    //DBConnect db = new DBConnect();
        //    //using (SqlConnection conn = db.GetOpenedConn())
        //    //{
        //    //    SqlCommand cmd = db.MakeCmdSP(conn, null, db);
        //    //    cmd.Parameters.AddWithValue("@ProfileID", profileID);
        //    //    return db.FillInDataTable(cmd);
        //    //}
        //    return null;
        //}

        public static DataTable SelectSerialNumbers(Guid accountID, String modelNumber)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectSerialNumbers);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectActiveModels(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectActiveModels);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectActiveAll(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectActive);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                return db.FillInDataTable(cmd);
            }
        }


        public static DataRow SelectRegisteredJPProductByMembershipId(Guid membershipId, out int productCount)
        {
            productCount = 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_JP_SelectByMembershipID);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);

                SqlParameter ModNum = new SqlParameter();
                ModNum.ParameterName = "@RegisteredProductJPCount";
                ModNum.SqlDbType = SqlDbType.Int;
                ModNum.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ModNum);
                cmd.ExecuteScalar();
                productCount = Convert.ToInt32(ModNum.Value);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectAllByAccountID(Guid accountID, Guid currentUserMembershipId)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectByAccountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", currentUserMembershipId);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectWithFilter(Guid accountID
            , String statusCode
            , String modelNumber
            , String serialNumber)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectWithFilter);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", serialNumber);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectWithFilterForAdmin(Guid accountID
            , Guid prodRegWebAccessKey
            , String statusCode
            , String modelNumber
            , String serialNumber
            , String submittedUserEmail
            , String orgName
            , String companyName)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Admin_SelectWithFilter);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@ProdRegWebAccessKey", prodRegWebAccessKey);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", ConvertUtility.ConvertNullToEmptyString(statusCode).Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", ConvertUtility.ConvertNullToEmptyString(modelNumber).Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", ConvertUtility.ConvertNullToEmptyString(serialNumber).Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SubmittedUserEmail", ConvertUtility.ConvertNullToEmptyString(submittedUserEmail).Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@OrgName", ConvertUtility.ConvertNullToEmptyString(orgName).Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", ConvertUtility.ConvertNullToEmptyString(companyName).Trim());
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectNonActive(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectNonActive);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectActiveByWebAccessKey(Guid accountID, Guid webAccessKey)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectActiveByWebAccessKey);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebAccessKey", webAccessKey);
                return db.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByWebAccessKeyForAdmin(Guid webAccessKey)
        {
            if (webAccessKey.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectByWebAccessKeyForAdmin);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebAccessKey", webAccessKey);
                return db.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByProdRegID(Int32 prodRegID)
        {
            if (prodRegID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectByProdRegID);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                return db.FillInDataRow(cmd);
            }
        }

        //public static Boolean Insert_OLDToBeDeleted(Guid accountID
        //    , String modelNumber
        //    , List<String> serialNumbers
        //    , String statusCode)
        //{
        //    if (accountID.IsNullOrEmptyGuid()) return false;
        //    if (serialNumbers == null || serialNumbers.Count < 1) return false;
        //    Boolean allSuccess = false;
        //    DBConnect db = new DBConnect();
        //    SqlTransaction tran = null;

        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        tran = conn.BeginTransaction();

        //        try
        //        {
        //            allSuccess = Insert(accountID, modelNumber, serialNumbers, statusCode, conn, tran);
        //        }
        //        catch (Exception ex)
        //        {
        //            if (tran != null) tran.Rollback();
        //            //throw ex;
        //        }                
        //        tran.Commit();
        //    }
        //    return allSuccess;
        //}

        public static Boolean Insert_OLDToBeDeleted(Guid accountID
            , String modelNumber
            , List<String> serialNumbers
            , String statusCode
            , String submittedBy
            , SqlConnection conn
            , SqlTransaction tran
            )
        {
            if (accountID.IsNullOrEmptyGuid()) return false;
            if (serialNumbers == null || serialNumbers.Count < 1) return false;
            Boolean allSuccess = false;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Insert);

            foreach (String sn in serialNumbers)
            {
                cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Insert);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", sn.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@Description", String.Empty);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode.ToLower().Trim());
                
                DataRow r = db.FillInDataRow(cmd);
                if (r == null) throw new ArgumentException("Unable to register " + sn.Trim());
                Boolean isExisting = ConvertUtility.ConvertToBoolean(r["IsExist"], false);
                if (isExisting) throw new ArgumentException(sn.Trim() + " already registered.");

                Int32 prodRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
                if (prodRegID > 0)
                {
                    cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegisteredData_Insert);
                    cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                    SQLUtility.SqlParam_AddAsString(ref cmd, "@DataKey", "SubmittedBy");
                    SQLUtility.SqlParam_AddAsString(ref cmd, "@DataValue", submittedBy.Trim());
                    cmd.ExecuteNonQuery();
                }
            }


            allSuccess = true;


            return allSuccess;
        }

        public static Boolean Insert(Guid accountID
            , Guid requesterMembershipId
            , String modelNumber
            , String serialNumber
            , String statusCode
            , Dictionary<String, String> additionalData
            , SqlConnection conn
            , SqlTransaction tran
            , out Guid webAccessKey
            )
        {
            webAccessKey = Guid.Empty;
            if (accountID.IsNullOrEmptyGuid()) return false;
            if (serialNumber.IsNullOrEmptyString()) return false;
            Boolean allSuccess = false;            
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Insert);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RequesterMembershipId", requesterMembershipId);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
            SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", serialNumber.Trim());
            SQLUtility.SqlParam_AddAsString(ref cmd, "@Description", String.Empty);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode.ToLower().Trim());

            DataRow r = db.FillInDataRow(cmd);
            if (r == null) throw new ArgumentException("Unable to register " + serialNumber.Trim());
            Boolean isExisting = ConvertUtility.ConvertToBoolean(r["IsExist"], false);
            webAccessKey = ConvertUtility.ConvertToGuid(r["WebAccessKey"], Guid.Empty);
            if (isExisting) throw new ArgumentException(serialNumber.Trim() + " already registered.");

            if (additionalData != null && additionalData.Count > 0)
            {
                Int32 prodRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
                if (prodRegID > 0)
                {
                    cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegisteredData_Insert);
                    cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                    SQLUtility.SqlParam_AddAsString(ref cmd, "@DataKey", String.Empty);
                    SQLUtility.SqlParam_AddAsString(ref cmd, "@DataValue", String.Empty);

                    foreach (String key in additionalData.Keys)
                    {
                        if (key.IsNullOrEmptyString()) continue;
                        cmd.Parameters["@DataKey"].Value = key.Trim();
                        cmd.Parameters["@DataValue"].Value = ConvertUtility.ConvertNullToDBNull( additionalData[key] );
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            allSuccess = true;

            return allSuccess;
        }

        public static void UpdateStatusCode(Guid webAccessKey, String statusCode)
        {
            if (webAccessKey.IsNullOrEmptyGuid() || statusCode.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();

            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_UpdateStatusCode);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebAccessKey", webAccessKey);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode.ToLower().Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateStatusCode(Int32 prodRegID, String statusCode, SqlConnection conn, SqlTransaction tran)
        {
            if (prodRegID < 1 || statusCode.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_UpdateStatusCodeByProdRegID);
            cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode.ToLower().Trim());
            cmd.ExecuteNonQuery();
        }

        public static void Update(Guid webAccessKey, String statusCode, DateTime dtJPSupportExp,
            Boolean includeDLSrcGW, Boolean includeDLSrcJP, 
            Boolean includeModelPrimary, Boolean includeModelTags, Boolean includeModelRegSpecific,
            String modelTarget)
        {
            if (webAccessKey.IsNullOrEmptyGuid() || statusCode.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();

            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Update);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebAccessKey", webAccessKey);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@StatusCode", statusCode.ToLower().Trim());
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@JPSupportExpiredOn", dtJPSupportExp);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IncludeDLSrcGW", includeDLSrcGW);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IncludeDLSrcJP", includeDLSrcJP);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IncludeModelPrimary", includeModelPrimary);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IncludeModelTags", includeModelTags);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IncludeModelRegSpecific", includeModelRegSpecific);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelTarget", modelTarget.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Int32 prodRegID)
        {
            if (prodRegID < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Delete);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow UpdateJapanUSB(Guid webAccessKey, String usbNo, Guid usbOwnerMembershipId)
        {
            if (webAccessKey.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_UpdateUSB);
                cmd.Parameters.AddWithValue("@WebAccessKey", webAccessKey);
                cmd.Parameters.AddWithValue("@USBNo", usbNo.Trim());
                cmd.Parameters.AddWithValue("@UserMembershipId", usbOwnerMembershipId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectByUSBNo(String usbNo)
        {
            if (usbNo.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_SelectByUSBKey);
                cmd.Parameters.AddWithValue("@USBNo", usbNo.ConvertNullToEmptyString().Trim());
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAccountIDsByUSBNo(String usbNo, Guid userMembershipId)
        {
            if (userMembershipId.IsNullOrEmptyGuid() || usbNo.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_FindAccountsByUSBNo);
                cmd.Parameters.AddWithValue("@USBNo", usbNo);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", userMembershipId);
                return db.FillInDataTable(cmd);
            }
        }
    }
}
