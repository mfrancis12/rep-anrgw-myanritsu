﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegisteredData
    {
        public static DataRow Select(Int32 prodRegDataID)
        {
            if (prodRegDataID < 1 ) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredData_Select);
                cmd.Parameters.AddWithValue("@ProdRegDataID", prodRegDataID);
                return db.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectByProdRegID(Int32 prodRegID)
        {
            if (prodRegID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredData_SelectByProdRegID);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                return db.FillInDataTable(cmd);
            }
        }

        public static void Insert(Int32 prodRegID, String dataKey, String dataValue)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                Insert(prodRegID, dataKey, dataValue, conn, null);
            }
        }

        public static void Insert(Int32 prodRegID, String dataKey, String dataValue, SqlConnection conn, SqlTransaction tran)
        {
            if (prodRegID < 1) throw new ArgumentNullException("prodRegID");
            if (dataKey.IsNullOrEmptyString()) throw new ArgumentNullException("dataKey");
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegisteredData_Insert);
            cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
            cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
            cmd.Parameters.AddWithValue("@DataValue", dataValue);
            cmd.ExecuteNonQuery();
        }

        public static void Update(Int32 prodRegDataID, String dataKey, String dataValue)
        {
            if (prodRegDataID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredData_Update);
                cmd.Parameters.AddWithValue("@ProdRegDataID", prodRegDataID);
                cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
                cmd.Parameters.AddWithValue("@DataValue", dataValue);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateByDataKey(Int32 prodRegID, String dataKey, String dataValue, String updatedBy)
        {
            if (prodRegID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredData_UpdateByDataKey);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
                cmd.Parameters.AddWithValue("@DataValue", dataValue);
                cmd.Parameters.AddWithValue("@UpdatedBy", updatedBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Delete(Int32 prodRegDataID)
        {
            if (prodRegDataID < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredData_Delete);
                cmd.Parameters.AddWithValue("@ProdRegDataID", prodRegDataID);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// This will insert/update
        /// </summary>
        /// <param name="prodRegID"></param>
        /// <param name="dataKey"></param>
        /// <param name="dataValue"></param>
        /// <param name="conn"></param>
        /// <param name="tran"></param>
        public static void Save(Int32 prodRegID, String dataKey, String dataValue, String updatedBy
            , SqlConnection conn, SqlTransaction tran)
        {
            if (prodRegID < 1) throw new ArgumentNullException("prodRegID");
            if (dataKey.IsNullOrEmptyString()) throw new ArgumentNullException("dataKey");
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegisteredData_UpdateByDataKey);
            cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
            cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
            cmd.Parameters.AddWithValue("@DataValue", dataValue);
            cmd.Parameters.AddWithValue("@UpdatedBy", updatedBy);
            cmd.ExecuteNonQuery();
        }
    }
}
