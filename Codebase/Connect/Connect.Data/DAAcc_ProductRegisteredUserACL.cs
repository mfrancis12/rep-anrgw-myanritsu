﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegisteredUserACL
    {
        public static DataTable SelectByProdRegID(Int32 prodRegID)
        {
            if (prodRegID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredUserACL_SelectUsers);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                return db.FillInDataTable(cmd);
                
            }
        }

        public static DataTable Select(Int32 prodRegID, Guid accountID, Guid userMembershipId)
        {
            if (prodRegID < 1 || accountID.IsNullOrEmptyGuid() || userMembershipId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredUserACL_Select);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", userMembershipId);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataRow Insert(Int32 prodRegID, Guid accountID, Guid userMembershipId
            , String statusCode
            , String requestedByEmail)
        {
            if (prodRegID < 1 || accountID.IsNullOrEmptyGuid() || userMembershipId.IsNullOrEmptyGuid()) return null;
            if (statusCode.IsNullOrEmptyString()) statusCode = "active";
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredUserACL_Insert);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", userMembershipId);
                cmd.Parameters.AddWithValue("@RequestedBy", requestedByEmail.ConvertNullToEmptyString());
                cmd.Parameters.AddWithValue("@StatusCode", statusCode.ConvertNullToEmptyString());
                return db.FillInDataRow(cmd);
            }
        }

        public static Int32 Delete(Int32 prodRegUserACLID)
        {
            if (prodRegUserACLID < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegisteredUserACL_Delete);
                cmd.Parameters.AddWithValue("@ProdRegUserACLID", prodRegUserACLID);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
