﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegisteredWF
    {
        public static void WF_Insert(Guid cartWebAccessKey
            , Guid membershipId
            , String userFullName
            , String userIPAddress
            , String userPhone
            , String userFax
            , out Boolean verifyCompany
            , out Boolean verifyProductSN
            , out String productOwnerRegion
            , out Boolean autoApproved
            , out Dictionary<String, Guid> prodRegWebAccessKeys
            )
        {
            // This method only supports one model at a time although the backend design supports multiple model numbers.
            if (cartWebAccessKey.IsNullOrEmptyGuid()) throw new ArgumentException("cartWebAccessKey");
            if (membershipId.IsNullOrEmptyGuid()) throw new ArgumentException("membershipId");
            if (userIPAddress.IsNullOrEmptyString()) throw new ArgumentException("userIPAddress");

            autoApproved = false;
            verifyCompany = false;
            verifyProductSN = false;
            productOwnerRegion = "";
            prodRegWebAccessKeys = new Dictionary<String, Guid>();
            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    #region " cart info "
                    Dictionary<DataRow, DataTable> cartItems;
                    DataRow cartRow = DAAcc_ProductRegCart.SelectByWebAccessKey(cartWebAccessKey, out cartItems, conn, tran);
                    if (cartRow == null || cartItems == null || cartItems.Count < 1) throw new ArgumentNullException("cartWebAccessKey");
                    Guid accountID = ConvertUtility.ConvertToGuid(cartRow["AccountID"], Guid.Empty);
                    String emailAddress = cartRow["LatestUserEmail"].ToString().Trim();
                    String modelNumber = String.Empty;
                    foreach (DataRow kr in cartItems.Keys)
                    {
                        String mn = ConvertUtility.ConvertNullToEmptyString(kr["ModelNumber"]).Trim();
                        if (modelNumber.IsNullOrEmptyString()) modelNumber = mn;
                        else
                        {
                            if(!modelNumber.Equals(mn, StringComparison.InvariantCultureIgnoreCase)) 
                                throw new ArgumentException("Duplicate model numbers.  You can only register one model at a time.");
                        }
                        break;
                    }
                    
                    if (modelNumber == null) throw new ArgumentNullException("modelNumber");
                    
                    #endregion

                    #region " account info "
                    SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Acc.SelectByAccountID);
                    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                    DataRow accountRow = SQLUtility.FillInDataRow(cmd);
                    if (accountRow == null) throw new ArgumentException("Invalid accountID.");
                    Boolean isVerifiedAndApproved = ConvertUtility.ConvertToBoolean(accountRow["IsVerifiedAndApproved"], false);
                    #endregion

                    #region " product config "
                    cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Erp_ProductConfig_SelectByModelNumber);
                    SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                    DataRow mnRow = db.FillInDataRow(cmd);
                    if (mnRow == null) throw new ArgumentException("Unsupported model number.");
                    verifyCompany = ConvertUtility.ConvertToBoolean(mnRow["VerifyCompany"], false);
                    verifyProductSN = ConvertUtility.ConvertToBoolean(mnRow["VerifyProductSN"], false);
                    productOwnerRegion = ConvertUtility.ConvertNullToEmptyString(mnRow["ProductOwnerRegion"]);
                    #endregion

                    #region " product registration "
                    String prodRegStatusCode = "submitted";
                    if (!verifyCompany && !verifyProductSN)// auto approved
                    {
                        prodRegStatusCode = "active";
                    }


                    Dictionary<String, String> additionalData = null;
                    int totalRegisteredSN = 0;
                    foreach (DataRow kr in cartItems.Keys)
                    {
                        String mn = modelNumber;//kr["ModelNumber"].ToString().Trim();
                        String sn = kr["SerialNumber"].ToString().Trim();
                        if (sn.IsNullOrEmptyString()) continue;
                        Guid prodregWAK = Guid.Empty;
                        additionalData = new Dictionary<String, String>();
                        additionalData.Add("SubmittedBy_Email", emailAddress);
                        additionalData.Add("SubmittedBy_FullName", userFullName);
                        additionalData.Add("SubmittedBy_IP", userIPAddress);
                        if (!userPhone.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Phone", userPhone.Trim());
                        if (!userFax.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Fax", userFax.Trim());
                        DataTable tbSNData = cartItems[kr];
                        if (tbSNData != null)
                        {
                            foreach (DataRow rSNData in tbSNData.Rows)
                            {
                                String dataKey = rSNData["DataKey"].ToString();
                                if (dataKey.IsNullOrEmptyString()) continue;
                                if (!additionalData.Keys.Contains(dataKey))
                                {
                                    additionalData.Add(dataKey, rSNData["DataValue"].ToString());
                                }
                            }
                        }

                        bool added = DAAcc_ProductRegistered.Insert(accountID
                            , membershipId
                            , mn, sn
                            , prodRegStatusCode
                            , additionalData
                            , conn, tran
                            , out prodregWAK);
                        if (added)
                        {
                            totalRegisteredSN++;
                            if (!prodregWAK.IsNullOrEmptyGuid()) prodRegWebAccessKeys.Add(sn, prodregWAK);
                        }
                    } 
                    #endregion

                    //bool allReigstered = DAAcc_ProductRegistered.Insert(accountID, modelNumber, serialNumbers, prodRegStatusCode, conn, tran);
                    if (totalRegisteredSN < 1) throw new ArgumentException("Unable to register products.");
                    DAAcc_ProductRegCart.Delete(cartWebAccessKey, conn, tran); //delete the cart after registration is done

                    tran.Commit();
                    autoApproved = prodRegStatusCode.Equals("active", StringComparison.InvariantCultureIgnoreCase);
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void WF_SupportRenewRequest(Int32 prodRegID
            , String userFullName
            , String userIP
            , String userEmail
            , String modelToRenew
            , String userCountryCode
            , List<KeyValuePair<String, String>> addOnFormData)
        {
            if (prodRegID < 1) return;

            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    if (addOnFormData == null) addOnFormData = new List<KeyValuePair<string, string>>();

                    #region " renew info log "
                    Random rand = new Random();
                    String renewPrefix = String.Format("RenewalReq{0}_{1}", rand.Next(1000, 9999), DateTime.UtcNow.ToString("MMM-dd-yyyy"));

                    KeyValuePair<string, string> kvpLog;
                    
                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_MN", renewPrefix), modelToRenew);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_RequestedBy", renewPrefix), userFullName);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_IP", renewPrefix), userIP);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_GEO", renewPrefix), userIP);
                    addOnFormData.Add(kvpLog);
                    
                    #endregion

                    if (addOnFormData != null)
                    {
                        foreach (KeyValuePair<String, String> kvp in addOnFormData)
                        {
                            Data.DAAcc_ProductRegisteredData.Save(prodRegID, kvp.Key, kvp.Value, userFullName, conn, tran);
                        }
                    }
                    

                    Data.DAAcc_ProductRegistered.UpdateStatusCode(prodRegID, "supportrenewsubmitted", conn, tran);
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void WF_AdmUpdateForm(Int32 prodRegID
           , String userFullName
           , String userIP
           , String userEmail
           , String model
           , String userCountryCode
           , List<KeyValuePair<String, String>> addOnFormData)
        {
            if (prodRegID < 1) return;

            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    if (addOnFormData == null) addOnFormData = new List<KeyValuePair<string, string>>();

                    #region " renew info log "
                    Random rand = new Random();
                    String renewPrefix = String.Format("AdmFormUpdate{0}_{1}", rand.Next(1000, 9999), DateTime.UtcNow.ToString("MMM-dd-yyyy"));

                    KeyValuePair<string, string> kvpLog;

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_MN", renewPrefix), model);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_RequestedBy", renewPrefix), userFullName);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_IP", renewPrefix), userIP);
                    addOnFormData.Add(kvpLog);

                    kvpLog = new KeyValuePair<string, string>(String.Format("{0}_GEO", renewPrefix), userIP);
                    addOnFormData.Add(kvpLog);

                    #endregion

                    if (addOnFormData != null)
                    {
                        foreach (KeyValuePair<String, String> kvp in addOnFormData)
                        {
                            Data.DAAcc_ProductRegisteredData.Save(prodRegID, kvp.Key, kvp.Value, userFullName, conn, tran);
                        }
                    }


                    //Data.DAAcc_ProductRegistered.UpdateStatusCode(prodRegID, "supportrenewsubmitted", conn, tran);
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        #region " OLD "
        //public static Boolean WF_Insert_OLD(Guid accountID
        //   , String userEmailAddress
        //   , String userIPAddress
        //   , String companyInfo
        //   , String modelNumber
        //   , List<String> serialNumbers
        //   , out Boolean verifyCompany
        //   , out Boolean verifyProductSN
        //   , out String productOwnerRegion
        //   )
        //{
        //    if (accountID.IsNullOrEmptyGuid()) throw new ArgumentException("accountID");
        //    if (userEmailAddress.IsNullOrEmptyString()) throw new ArgumentException("userEmailAddress");
        //    if (companyInfo.IsNullOrEmptyString()) throw new ArgumentException("companyInfo");
        //    if (userIPAddress.IsNullOrEmptyString()) throw new ArgumentException("userIPAddress");
        //    if (serialNumbers == null || serialNumbers.Count < 1) throw new ArgumentException("serialNumbers");

        //    verifyCompany = false;
        //    verifyProductSN = false;
        //    productOwnerRegion = "";
        //    DBConnect db = new DBConnect();
        //    SqlTransaction tran = null;
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        tran = conn.BeginTransaction();
        //        try
        //        {
        //            #region " account info "
        //            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Acc.SelectByAccountID);
        //            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
        //            DataRow accountRow = SQLUtility.FillInDataRow(cmd);
        //            if (accountRow == null) throw new ArgumentException("Invalid accountID.");
        //            Boolean isVerifiedAndApproved = ConvertUtility.ConvertToBoolean(accountRow["IsVerifiedAndApproved"], false);
        //            #endregion

        //            #region " product config "
        //            cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Erp_ProductConfig_SelectByModelNumber);
        //            SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        //            DataRow mnRow = db.FillInDataRow(cmd);
        //            if (mnRow == null) throw new ArgumentException("Unsupported model number.");
        //            verifyCompany = ConvertUtility.ConvertToBoolean(mnRow["VerifyCompany"], false);
        //            verifyProductSN = ConvertUtility.ConvertToBoolean(mnRow["VerifyProductSN"], false);
        //            productOwnerRegion = ConvertUtility.ConvertNullToEmptyString(mnRow["ProductOwnerRegion"]);
        //            #endregion

        //            String prodRegStatusCode = "submitted";
        //            if (!verifyCompany && !verifyProductSN)// US products
        //            {
        //                prodRegStatusCode = "active";
        //            }


        //            Dictionary<String, String> additionalData = null;
        //            int totalRegisteredSN = 0;
        //            foreach (String sn in serialNumbers)
        //            {
        //                additionalData = new Dictionary<String, String>();
        //                additionalData.Add("SubmittedBy", userEmailAddress);
        //                additionalData.Add("SubmittedByIP", userIPAddress);
        //                if (sn.IsNullOrEmptyString()) continue;
        //                bool added = DAAcc_ProductRegistered.Insert(accountID, modelNumber, sn
        //                    , prodRegStatusCode
        //                    , additionalData
        //                    , conn, tran);
        //                if (added) totalRegisteredSN++;
        //            }

        //            //bool allReigstered = DAAcc_ProductRegistered.Insert(accountID, modelNumber, serialNumbers, prodRegStatusCode, conn, tran);
        //            if (totalRegisteredSN < 1) throw new ArgumentException("Unable to register products.");

        //            tran.Commit();
        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            tran.Rollback();
        //            //throw ex;
        //            return false;
        //        }


        //    }
        //} 
        #endregion
    }
}
