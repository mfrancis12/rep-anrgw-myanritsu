﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_Data
    {
        public static DataRow Select(Int32 prodRegDataID)
        {
            if (prodRegDataID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.Select);
                SQLUtility.AddParam(ref cmd, "@ProdRegDataID", prodRegDataID);
                return db.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectByProdRegID(Int32 prodRegID)
        {
            if (prodRegID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.SelectByProdRegID);
                SQLUtility.AddParam(ref cmd, "@ProdRegID", prodRegID);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.SelectByWebToken);
                SQLUtility.AddParam(ref cmd, "@WebToken", webToken);
                return db.FillInDataTable(cmd);
            }
        }

        public static void Insert(Int32 prodRegID, String dataKey, String dataValue)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                Insert(prodRegID, dataKey, dataValue, conn, null);
            }
        }

        public static void Insert(Int32 prodRegID, String dataKey, String dataValue, SqlConnection conn, SqlTransaction tran)
        {
            if (prodRegID < 1) throw new ArgumentNullException("prodRegID");
            if (dataKey.IsNullOrEmptyString()) throw new ArgumentNullException("dataKey");
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ProdReg_Data.Insert);
            SQLUtility.AddParam(ref cmd, "@ProdRegID", prodRegID);
            SQLUtility.AddParam(ref cmd, "@DataKey", dataKey.Trim());
            SQLUtility.AddParam(ref cmd, "@DataValue", dataValue);
            cmd.ExecuteNonQuery();
        }

        public static void Update(Int32 prodRegDataID, String dataKey, String dataValue)
        {
            if (prodRegDataID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.Update);
                SQLUtility.AddParam(ref cmd, "@ProdRegDataID", prodRegDataID);
                SQLUtility.AddParam(ref cmd, "@DataKey", dataKey.Trim());
                SQLUtility.AddParam(ref cmd, "@DataValue", dataValue);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Save(Int32 prodRegID, String dataKey, String dataValue)
        {
            if (prodRegID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.InsertUpdate);
                SQLUtility.AddParam(ref cmd, "@ProdRegID", prodRegID);
                SQLUtility.AddParam(ref cmd, "@DataKey", dataKey.Trim());
                SQLUtility.AddParam(ref cmd, "@DataValue", dataValue);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateSupportEntryMapping_Renew(Int32 supEntryId,String supportTypeCode)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.UpdateSupportEntryMappingRenew);
                SQLUtility.AddParam(ref cmd, "@SupportTypeEntryId", supEntryId);
                SQLUtility.AddParam(ref cmd, "@SupportTypeCode", supportTypeCode);
               cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateByDataKey(Int32 prodRegID, String dataKey, String dataValue, String updatedBy)
        {
            if (prodRegID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.UpdateByDataKey);
                SQLUtility.AddParam(ref cmd, "@ProdRegID", prodRegID);
                SQLUtility.AddParam(ref cmd, "@DataKey", dataKey.Trim());
                SQLUtility.AddParam(ref cmd, "@DataValue", dataValue);
                SQLUtility.AddParam(ref cmd, "@UpdatedBy", updatedBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Delete(Int32 prodRegDataID)
        {
            if (prodRegDataID < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Data.Delete);
                SQLUtility.AddParam(ref cmd, "@ProdRegDataID", prodRegDataID);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// This will insert/update
        /// </summary>
        /// <param name="prodRegID"></param>
        /// <param name="dataKey"></param>
        /// <param name="dataValue"></param>
        /// <param name="conn"></param>
        /// <param name="tran"></param>
        public static void Save(Int32 prodRegID, String dataKey, String dataValue, String updatedBy
            , SqlConnection conn, SqlTransaction tran)
        {
            if (prodRegID < 1) throw new ArgumentNullException("prodRegID");
            if (dataKey.IsNullOrEmptyString()) throw new ArgumentNullException("dataKey");
            DBConnect db = new DBConnect();
            
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ProdReg_Data.UpdateByDataKey);
            SQLUtility.AddParam(ref cmd, "@ProdRegID", prodRegID);
            SQLUtility.AddParam(ref cmd, "@DataKey", dataKey.Trim());
            SQLUtility.AddParam(ref cmd, "@DataValue", dataValue);
            SQLUtility.AddParam(ref cmd, "@UpdatedBy", updatedBy);
            cmd.ExecuteNonQuery();
        }
    }
}
