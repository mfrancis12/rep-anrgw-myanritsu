﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_Item
    {
        public static DataRow Insert(SqlConnection conn, SqlTransaction tran, Int32 prodRegId, String itemType, String modelNumber, String serialNumber
            , String statusCode, Boolean includePrimaryMn, Boolean includeTaggedMn
            , String addedBy)
        {
            if (prodRegId < 1 || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Item_Insert);
            cmd.Parameters.AddWithValue("@ProdRegID", prodRegId);
            cmd.Parameters.AddWithValue("@ItemType", itemType.Trim());
            cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
            cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
            cmd.Parameters.AddWithValue("@StatusCode", statusCode.Trim());
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@InclResrc_PrimaryMN", includePrimaryMn);
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@InclResrc_TaggedMN", includeTaggedMn);
            cmd.Parameters.AddWithValue("@AddedBy", addedBy.Trim());
            return SQLUtility.FillInDataRow(cmd);
        }

        public static void Update(SqlConnection conn, SqlTransaction tran, Int32 prodRegItemId, String modelNumber, String serialNumber, String statusCode, Boolean includePrimaryMn, Boolean includeTaggedMn
            , String updatedBy)
        {
            if (prodRegItemId < 1 || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) throw new ArgumentException();
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Item_Update);
            cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
            cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
            cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
            cmd.Parameters.AddWithValue("@StatusCode", statusCode.Trim());
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@InclResrc_PrimaryMN", includePrimaryMn);
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@InclResrc_TaggedMN", includeTaggedMn);
            cmd.Parameters.AddWithValue("@UpdatedBy", updatedBy.Trim());
            cmd.ExecuteNonQuery();
        }

        public static void Update(Int32 prodRegItemId, String modelNumber, String serialNumber,
                                    String statusCode, Boolean includeTaggedMn, String updatedBy)
        {
            if (prodRegItemId < 1 || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) throw new ArgumentException();
            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Item_Update);
                    cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
                    cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                    cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                    cmd.Parameters.AddWithValue("@StatusCode", statusCode.Trim());
                    SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@InclResrc_TaggedMN", includeTaggedMn);
                    cmd.Parameters.AddWithValue("@UpdatedBy", updatedBy.Trim());
                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                }
            }
        }

        public static void UpdateStatus(Int32 prodRegItemId, String statusCode, String updatedBy)
        {
            if (prodRegItemId < 1 || statusCode.IsNullOrEmptyString()) throw new ArgumentException();
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Item.UpdateStatus);
                SQLUtility.AddParam(ref cmd, "@ProdRegItemID", prodRegItemId);
                SQLUtility.AddParam(ref cmd, "@StatusCode", statusCode);
                SQLUtility.AddParam(ref cmd, "@UpdatedBy", updatedBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Int32 prodRegItemId, string deletedBy)
        {
            if (prodRegItemId < 1) throw new ArgumentException();
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_Delete);
                cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
                cmd.Parameters.AddWithValue("@DeletedBy", deletedBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteForJPSW(Int32 prodRegItemId)
        {
            if (prodRegItemId < 1) throw new ArgumentException();
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_ForJPSW_Delete);
                cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable ForUser_SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_ForUser_SelectByWebToken);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Item.SelectByWebToken);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataTable(cmd);
            }
        }



        public static DataTable SelectWithFilter(int prodRegId, String modelConfigType, String mn, String sn)
        {
            if (prodRegId == 0) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_SelectWithFilter);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelConfigType", modelConfigType);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", mn.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", sn.Trim());
                return db.FillInDataTable(cmd);
            }
        }

        /// <summary>
        /// Returns products of user in current account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="currentUserMembershipId"></param>
        /// <returns></returns>
        public static DataTable SelectAllByAccountID(Guid accountId, Guid currentUserMembershipId)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_SelectByAccountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", currentUserMembershipId);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByProdRegItemId(int prodRegitemId)
        {
            if (prodRegitemId == 0) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_SelectByProdRegItemId);
                cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegitemId);

                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectRegCount(Guid membershipId, String modelConfigType)
        {
            if (membershipId.IsNullOrEmptyGuid() || modelConfigType.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Item.SelectRegCount);
                SQLUtility.AddParam(ref cmd, "@MembershipId", membershipId);
                SQLUtility.AddParam(ref cmd, "@ModelConfigType", modelConfigType);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
        public static DataTable SelectByWebModelAndTeams(DataTable dtTeamIds, string masterModel)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Item.SelectByModelAndTeams);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeamIds);
                SQLUtility.AddParam(ref cmd, "@MasterModel", masterModel);
                return db.FillInDataTable(cmd);
            }
        }

    }
}
