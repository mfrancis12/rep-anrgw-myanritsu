﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_ItemConfig_JPDL
    {
        private const String Itemtype = "JPSW";

        public static void Select(Int32 prodRegItemId, out DataRow dataConfig, out DataTable dataUsb)
        {
            dataConfig = null;
            dataUsb = null;

            if (prodRegItemId < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                Select(prodRegItemId, conn, null, out dataConfig, out dataUsb);
            }
        }

        public static void Select(Int32 prodRegItemId, SqlConnection conn, SqlTransaction tran, out DataRow dataConfig, out DataTable dataUsb)
        {
            dataConfig = null;
            dataUsb = null;
            if (prodRegItemId < 1) return;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ProdReg_ItemConfig.JPDL_Select);
            cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
            DataSet ds = SQLUtility.FillInDataSet(cmd);
            if (ds == null || ds.Tables.Count < 1 || ds.Tables[0].Rows.Count < 1) return;
            dataConfig = ds.Tables[0].Rows[0];
            if (ds.Tables.Count > 1) dataUsb = ds.Tables[1];
        }

        public static DataTable SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_ForJPSW_SelectByWebToken);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static Int32 Insert(Int32 prodRegId, String modelNumber, String serialNumber
            , String statusCode, Boolean includePrimaryMn, Boolean includeTaggedMn, String addedBy
            , String jpExportModelTarget, DateTime jpSupportContract)
        {
            Int32 prodRegItemId = 0;
            if (prodRegId < 1 || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return prodRegItemId;

            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    DataRow r = DAAcc_ProductRegistered_Item.Insert(conn, tran, prodRegId, Itemtype, modelNumber, serialNumber, statusCode, includePrimaryMn
                        , includeTaggedMn, addedBy);
                    if (r == null) throw new ArgumentException("Unable to create new record.");
                    prodRegItemId = ConvertUtility.ConvertToInt32(r["ProdRegItemID"], 0);

                    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_ItemConfig_JPDL_Insert);
                    cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
                    cmd.Parameters.AddWithValue("@JPExport_ModelTarget", jpExportModelTarget);
                    SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@JPSupportContract", jpSupportContract);
                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                }

                return prodRegItemId;
            }
        }

        public static void Update(Int32 prodRegItemId, String modelNumber, String serialNumber
           , String statusCode, Boolean includePrimaryMn, Boolean includeTaggedMn, String updatedBy
           , String jpExportModelTarget, DateTime jpSupportContract)
        {
            if (prodRegItemId < 1 || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return;

            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    DAAcc_ProductRegistered_Item.Update(conn, tran, prodRegItemId, modelNumber, serialNumber, statusCode, includePrimaryMn
                        , includeTaggedMn, updatedBy);

                    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_ItemConfig_JPDL_Update);
                    cmd.Parameters.AddWithValue("@ProdRegItemID", prodRegItemId);
                    cmd.Parameters.AddWithValue("@JPExport_ModelTarget", jpExportModelTarget);
                    SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@JPSupportContract", jpSupportContract);
                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                }
            }
        }

        public static void DeleteForJPSW(Int32 prodRegItemId)
        {
            DAAcc_ProductRegistered_Item.DeleteForJPSW(prodRegItemId);
        }
    }
}
