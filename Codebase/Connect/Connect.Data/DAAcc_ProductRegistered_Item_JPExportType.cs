﻿using System.Data;
using System.Data.SqlClient;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_Item_JPExportType
    {
        public static DataTable SelectAllExportType()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Item_JPExportType_SelectAll);
                return db.FillInDataTable(cmd);
            }
        }
    }
}
