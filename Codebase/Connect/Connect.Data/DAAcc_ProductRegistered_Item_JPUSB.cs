﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_Item_JPUSB
    {
        public static DataTable SelectByUSBKey(String usbNo)
        {
            if (usbNo.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Item_JPUSB.SelectByUSBKey);
                SQLUtility.AddParam(ref cmd, "@USBNo", usbNo);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
