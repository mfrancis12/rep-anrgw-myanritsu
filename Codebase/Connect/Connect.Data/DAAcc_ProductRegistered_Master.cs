﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProductRegistered_Master
    {
        public static DataRow Insert(Guid accountId, String mn, String sn, String createdBy)
        {
            if (accountId.IsNullOrEmptyGuid() || mn.IsNullOrEmptyString() || sn.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_Insert);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                //cmd.Parameters.AddWithValue("@Description", desc.Trim());
                cmd.Parameters.AddWithValue("@MasterModel", mn.Trim());
                cmd.Parameters.AddWithValue("@MasterSerial", sn.Trim());
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow Insert(Guid accountId, String mn, String sn, String createdBy, Dictionary<String, String> additionalData)
        {
            if (accountId.IsNullOrEmptyGuid() || mn.IsNullOrEmptyString() || sn.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                DataRow regData;
                try
                {

                    SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Acc_ProductRegistered_Master_Insert);
                    SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                    //cmd.Parameters.AddWithValue("@Description", desc.Trim());
                    cmd.Parameters.AddWithValue("@MasterModel", mn.Trim());
                    cmd.Parameters.AddWithValue("@MasterSerial", sn.Trim());
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    regData = SQLUtility.FillInDataRow(cmd);
                    Int32 prodRegId = ConvertUtility.ConvertToInt32(regData["ProdRegID"], 0);
                    if (prodRegId > 0 && additionalData != null && additionalData.Count > 0)
                    {
                        foreach (String datakey in additionalData.Keys)
                        {
                            DAAcc_ProductRegistered_Data.Insert(prodRegId, datakey,
                                ConvertUtility.ConvertNullToEmptyString(additionalData[datakey]).Trim(),
                                conn, tran
                                );
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                return regData;
            }
        }

        public static void Update(Guid webToken, String mn, String sn)
        {
            if (webToken.IsNullOrEmptyGuid() || mn.IsNullOrEmptyString() || sn.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_Update);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                cmd.Parameters.AddWithValue("@MasterModel", mn.Trim());
                cmd.Parameters.AddWithValue("@MasterSerial", sn.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_SelectByWebToken);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectBySN(Guid accountId, String mn, String sn)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_SelectSN);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", mn.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", sn.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow CheckIfOkToDelete(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_CheckToDelete);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Delete(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_Delete);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                //SQLUtility.AddParam(ref cmd, "@DeletedBy", deletedBy);
                //SQLUtility.AddParam(ref cmd, "@ModelConfigType", modelConfigType);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable ForUser_Select(Guid accountId, Guid userMembershipId, String includeType, Int32 cultureGroupIdByCountryCode)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Master.ForUser_Select);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@UserMembershipId", userMembershipId);
                SQLUtility.AddParam(ref cmd, "@IncludeType", includeType);
                SQLUtility.AddParam(ref cmd, "@CultureGroupIDByCountryCode", cultureGroupIdByCountryCode);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable ForUser_SelectWithPaging(Int32 startRowIndex, Int32 maximumRows, Guid accountId, Guid userMembershipId, String includeType, out Int32 totalCount)
        {
            totalCount = 0;
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_ForUser_SelectWithPaging);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@UserMembershipId", userMembershipId);
                cmd.Parameters.AddWithValue("@IncludeType", includeType.Trim());
                cmd.Parameters.AddWithValue("@StartRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@MaximumRow", maximumRows);

                SqlParameter totalOutputParm = new SqlParameter("@TotalRecords", SqlDbType.Int);
                totalOutputParm.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(totalOutputParm);

                DataTable tb = SQLUtility.FillInDataTable(cmd);
                totalCount = ConvertUtility.ConvertToInt32(totalOutputParm.Value, 0);
                return tb;
            }
        }

        public static void ForUser_UpdateDesc(Int32 prodRegId, String desc)
        {
            if (prodRegId < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_ForUser_UpdateDesc);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegId);
                cmd.Parameters.AddWithValue("@Description", desc.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static DataSet SelectModelSerial(DataTable dtTeams, string modelNumber, string serialNumber)
        {
            //if (webToken.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Master.SelectModelSerial);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeams);
                SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.AddParam(ref cmd, "@SerialNumber", serialNumber);
                return SQLUtility.FillInDataSet(cmd);
            }
        }

        public static DataTable SelectByAccMnSn(Guid accountId
            , String masterModel
            , String masterSerial)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_SelectByAccMnSn);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@MasterModel", masterModel);
                SQLUtility.AddParam(ref cmd, "@MasterSerial", masterSerial);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectProductFilters(DataTable dtTeamIds, int? productModelFilterId = null, int? productFunctionFilterId = null, string category = "Model")
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_ProductModelFilter_SelectByTeamIds);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeamIds);
                if (!productModelFilterId.HasValue)
                    SQLUtility.AddParam(ref cmd, "@Category", category);
                SQLUtility.AddParam(ref cmd, "@ProductModelFilterID", productModelFilterId ?? -1, -1);
                SQLUtility.AddParam(ref cmd, "@ProductFunctionFilterID", productFunctionFilterId ?? -1, -1);
                return db.FillInDataTable(cmd);
            }
        }
        public static DataTable SelectProductFilters(DataTable dtTeamIds, string modelKey, string functionkey)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_ProductModelFilter_SelectByTeamIds);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeamIds);
                if (!string.IsNullOrEmpty(modelKey))
                    SQLUtility.AddParam(ref cmd, "@ModelKey", modelKey);
                if (!string.IsNullOrEmpty(functionkey))
                    SQLUtility.AddParam(ref cmd, "@FunctionKey", functionkey);
                return db.FillInDataTable(cmd);
            }
        }
        public static DataTable SelectByModelAndTeams(DataTable dtTeamIds, string masterModel)
        {
            if (masterModel.IsNullOrEmptyString()) return null;
            var db = new DBConnect();
            using (var conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_SelectByModelAndTeams);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeamIds);
                SQLUtility.AddParam(ref cmd, "@MasterModel", masterModel);
                return db.FillInDataTable(cmd);
            }
        }
        public static DataTable SelectRegisteredProducts(DataTable dtTeamIds, string mk, string fk, string ok)
        {
            if (dtTeamIds.Rows.Count < 1) return null;
            var db = new DBConnect();
            using (var conn = db.GetOpenedConn())
            {
                var cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProdReg_Master.SelectRegisteredProducts);
                SQLUtility.AddParam(ref cmd, "@AccountIDs", dtTeamIds);
                if (!string.IsNullOrEmpty(mk))
                    SQLUtility.AddParam(ref cmd, "@ModelKey", mk);
                if (!string.IsNullOrEmpty(fk))
                    SQLUtility.AddParam(ref cmd, "@FunctionKey", fk);
                if (!string.IsNullOrEmpty(ok))
                    SQLUtility.AddParam(ref cmd, "@OptionKey", ok);
                var regProducts = db.FillInDataTable(cmd);
                return regProducts;
            }
        }

        public static DataTable CheckItemsToDelete(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            var db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Master_Delete_CheckItems);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@WebToken", webToken);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static bool HasDeletedProductRegistrations(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return false;
            var db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProductRegistered_Deletions_Check);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);               
                var retValue = cmd.ExecuteScalar();
                return ConvertUtility.ConvertToInt32(retValue, 0) > 0;
            }
        }
    }
}
