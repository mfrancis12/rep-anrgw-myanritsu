﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_Profile
    {
        public static DataRow SelectByProfileID(Int32 profileID)
        {
            if (profileID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_Profile_SelectByProfileID);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                return db.FillInDataRow(cmd);
            }
        }

        public static DataRow Insert(Guid accountID, String profileType, String profileStatus, String requestedBy, DateTime expOnUTC)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_Profile_Insert);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                cmd.Parameters.AddWithValue("@ProfileType", profileType);
                cmd.Parameters.AddWithValue("@ProfileStatus", profileStatus);
                cmd.Parameters.AddWithValue("@RequestedBy", requestedBy);
                cmd.Parameters.AddWithValue("@ExpOnUTC", expOnUTC);                
                return db.FillInDataRow(cmd);
            }
        }

        public static void Update(Int32 profileID, String profileStatus, DateTime expOnUTC, String lastApprovedBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_Profile_Update);                
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                cmd.Parameters.AddWithValue("@ProfileStatus", profileStatus);
                cmd.Parameters.AddWithValue("@LastApprovedBy", lastApprovedBy);
                cmd.Parameters.AddWithValue("@ExpOnUTC", expOnUTC);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
