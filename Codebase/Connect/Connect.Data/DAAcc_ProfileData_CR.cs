﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAAcc_ProfileData_CR
    {
        public static DataTable SelectByProfileID(Int32 profileID)
        {
            if (profileID < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProfileData_CR_SelectByProfileID);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                return db.FillInDataTable(cmd);
            }
        }

        public static Int32 DeleteByProfileDataID(Int32 profileDataID)
        {
            if (profileDataID < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProfileData_CR_DeleteByProfileDataID);
                cmd.Parameters.AddWithValue("@ProfileDataID", profileDataID);
                return cmd.ExecuteNonQuery();
            }
        }

        public static DataRow Insert(Int32 profileID
            , String dataKey
            , String dataValue
            , Boolean isReadOnly
            , String lastModifiedBy
            , String createdBy
            , String displayTextClassKey
            , String displayTextResKey
            )
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Acc_ProfileData_CR_Insert);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
                cmd.Parameters.AddWithValue("@DataValue", dataValue.Trim());
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsReadOnly", isReadOnly);
                cmd.Parameters.AddWithValue("@LastModifiedBy", lastModifiedBy);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                cmd.Parameters.AddWithValue("@DisplayTextClassKey", displayTextClassKey);
                cmd.Parameters.AddWithValue("@DisplayTextResKey", displayTextResKey);
                return db.FillInDataRow(cmd);
            }
        }
    }


}
