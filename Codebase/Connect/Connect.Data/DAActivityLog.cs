﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAActivityLog
    {
        public static int Insert(String emailID, String areaModified, String taskperformed, String action, String ipAddress,String modelConfigType)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ActivityLog.AdminActivityLog_Insert);

                cmd.Parameters.AddWithValue("@EmailID", emailID.Trim());
                cmd.Parameters.AddWithValue("@AreaModified", areaModified);
                cmd.Parameters.AddWithValue("@TaskPerformed", taskperformed);
                cmd.Parameters.AddWithValue("@ActionPerformed", action.Trim());
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress.Trim());
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                return cmd.ExecuteNonQuery();
            }
        }

        public static int ClientLogInsert(String emailID, String filePath, String taskperformed, long fileSize, String ipAddress,String modelConfig)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ActivityLog.UserActivityLog_Insert);

                cmd.Parameters.AddWithValue("@EmailID", emailID.Trim());
                cmd.Parameters.AddWithValue("@FilePath", filePath);
                cmd.Parameters.AddWithValue("@TaskPerformed", taskperformed);
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress.Trim());
                cmd.Parameters.AddWithValue("@FileSize", fileSize);
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfig);
                return cmd.ExecuteNonQuery();
            }
        }

        public static Boolean SubmitWebmasterContactDataToDatabase(int UserID, String url,
       String userIP,int culturegroupid,
         String comments)
        {
            var result = false;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.WebMasterContact.Insert);

                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Parameters.AddWithValue("@CultureGroupId", culturegroupid);
                cmd.Parameters.AddWithValue("@Comments", comments);
                cmd.Parameters.AddWithValue("@UserIP", userIP);
                cmd.Parameters.AddWithValue("@URL", url);
                cmd.ExecuteNonQuery();
            }
            return result;

        }
    }
}
