﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using ApiSdk;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Web;
namespace Anritsu.Connect.Data
{
    public class DAAdminProductFilterFunction
    {
        public static bool Insert(int modelConfigID, string model, string function, string option, string createdBy)
        {
            DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_AdminProductFilterFunction.Insert);
                    cmd.Parameters.AddWithValue("@modelConfigID", modelConfigID);
                    cmd.Parameters.AddWithValue("@Model", model);
                    cmd.Parameters.AddWithValue("@Function", function);
                    cmd.Parameters.AddWithValue("@Option", option);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    SQLUtility.FillInDataRow(cmd);

                    tran.Commit();
                }
                catch (Exception)
                {
                    tran.Rollback();
                    return false;
                }
                return true;
            }
        }

        public static void Delete(int modelConfigID, string model, string function, string option, string modifiedBy)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_AdminProductFilterFunction.Delete);
                cmd.Parameters.AddWithValue("@modelConfigID", modelConfigID);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@Function", function);
                cmd.Parameters.AddWithValue("@Option", option);
                cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
