﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAAnr_Profile
    {
        public static DataTable SelectCount()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectCount);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAllFiltered(Guid profileKey, Guid accountID, String profileType, String profileStatus, String submittedByUserEmail, String approvedBy)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectAllFiltered);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@ProfileKey", profileKey);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProfileType", profileType.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProfileStatus", profileStatus.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SubmittedByUserEmail", submittedByUserEmail.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ApprovedBy", approvedBy.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByProfileID(Int32 profileID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectByProfileID);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByProfileKey(Guid profileKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectByProfileKey);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@ProfileKey", profileKey);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow Insert(Guid accountID, String profileType, List<KeyValuePair<String,String>> keyValues, String createdBy)
        {
            if (accountID.IsNullOrEmptyGuid()) throw new ArgumentNullException("accountID");
             if(profileType.IsNullOrEmptyString()) throw new ArgumentNullException("profileType");
            if(keyValues == null || keyValues.Count < 1) throw new ArgumentNullException("keyValues");
            
            SqlTransaction tran = null;
            try
            {                
                using (SqlConnection conn = DABase.GetOpenedConn())
                {
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.AnrProfile.Profile_Insert);
                    cmd.Parameters.AddWithValue("@AccountID", accountID);
                    cmd.Parameters.AddWithValue("@ProfileType", profileType.Trim());
                    cmd.Parameters.AddWithValue("@SubmittedByUserEmail", createdBy.Trim());
                    DataRow r = SQLUtility.FillInDataRow(cmd);
                    if (r != null)
                    {
                        Int32 profileID = ConvertUtility.ConvertToInt32(r["ProfileID"], 0);
                        foreach(KeyValuePair<String,String> kv in keyValues)
                        {
                            DAAnr_ProfileData.Insert(profileID, kv.Key, kv.Value, createdBy, conn, tran);
                        }                    
                    }
                    tran.Commit();
                    return r;

                }
            }
            catch(Exception ex)
            {
                if(tran != null) tran.Rollback();
                throw ex;
            }
        }

        public static void UpdateStatus(Int32 profileID, String profileStatus, String approvedBy)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_UpdateStatus);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                cmd.Parameters.AddWithValue("@ProfileStatus", profileStatus.Trim());
                cmd.Parameters.AddWithValue("@ApprovedBy", approvedBy.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateCustomerMessage(Int32 profileID, String customerMessage)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_UpdateCustomerMessage);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                cmd.Parameters.AddWithValue("@MessageToCustomer", customerMessage.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable SelectProductProfiles(Guid accountID, String profileStatus)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectProductProfiles);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProfileStatus", profileStatus.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectFilteredProfiles(Guid accountID, String profileStatus, String profileType)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_SelectFilteredProfiles);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProfileStatus", profileStatus.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProfileType", profileType.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static Int32 AssignApprover(Int32 profileID, String assignedApprover)
        {
            if (profileID < 1) return 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.Profile_AssignApprover);
                cmd.Parameters.AddWithValue("@ProfileID", profileID);
                cmd.Parameters.AddWithValue("@AssignedApprover", assignedApprover.Trim());
                return cmd.ExecuteNonQuery();
            }
        }
    }
}