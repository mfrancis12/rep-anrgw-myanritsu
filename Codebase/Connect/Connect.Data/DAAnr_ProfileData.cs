﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAAnr_ProfileData
    {
        public static List<String> ReadOnlyDataFields = new List<string>() { "accountid", "createdonutc", "modifiedonutc", "crstatus", "crprofileid" };

        public static DataTable SelectByProfileID(Int32 profileID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return SelectByProfileID(profileID, conn, null);
            }
        }

        public static DataTable SelectByProfileID(Int32 profileID, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.AnrProfile.ProfileData_SelectByProfileID);
            cmd.Parameters.AddWithValue("@ProfileID", profileID);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataRow SelectByProfileDataID(Int32 profileDataID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.ProfileData_SelectByProfileDataID);
                cmd.Parameters.AddWithValue("@ProfileDataID", profileDataID);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow Insert(Int32 profileID, String dataKey, String dataValue, String createdBy)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return Insert(profileID, dataKey, dataValue, createdBy, conn, null);
            }
        }

        public static DataRow Insert(Int32 profileID, String dataKey, String dataValue, String createdBy, SqlConnection conn, SqlTransaction tran)
        {
            Boolean isReadOnly = false;
            if (ReadOnlyDataFields.Contains(dataKey.Trim().ToLowerInvariant())) isReadOnly = true;

            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.AnrProfile.ProfileData_Insert);
            cmd.Parameters.AddWithValue("@ProfileID", profileID);
            cmd.Parameters.AddWithValue("@DataKey", dataKey.Trim());
            cmd.Parameters.AddWithValue("@DataValue", dataValue.Trim());
            cmd.Parameters.AddWithValue("@CreatedBy", createdBy.Trim());
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsReadOnly", isReadOnly);            
            return SQLUtility.FillInDataRow(cmd);
        }

        public static void UpdateByProfileDataID(Int32 profileDataID, String dataValue, String lastModifiedBy)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.AnrProfile.ProfileData_UpdateByProfileDataID);
                cmd.Parameters.AddWithValue("@ProfileDataID", profileDataID);
                cmd.Parameters.AddWithValue("@DataValue", dataValue.Trim());
                cmd.Parameters.AddWithValue("@LastModifiedBy", lastModifiedBy.Trim());
                cmd.ExecuteNonQuery();
            }
        }
    }
}