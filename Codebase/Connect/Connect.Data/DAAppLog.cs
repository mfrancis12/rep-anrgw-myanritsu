﻿using System;
using System.Data.SqlClient;

namespace Anritsu.Connect.Data
{
    public static class DAAppLog
    {
        public static void LogError(
            String environment,
            String pageUrl,
            String httpReferer,
            Int32 httpCode,
            String clientIp,
            String shortMessage,
            String stackTrace,
            String method,
            String httpRequest,
            String userName,
            String clientHostName,
            String userAgent,
            String hostName
            )
        {
            DBLog db = new DBLog();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBLog.SQL_SP.ExceptionLog_Add);
                cmd.Parameters.AddWithValue("@AppDescription", "CONNECT");
                cmd.Parameters.AddWithValue("@Environment", environment);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@HttpReferer", httpReferer);
                cmd.Parameters.AddWithValue("@HttpCode", httpCode);
                cmd.Parameters.AddWithValue("@ClientIP", clientIp);
                cmd.Parameters.AddWithValue("@ClientHostName", clientHostName);
                cmd.Parameters.AddWithValue("@ShortMessage", shortMessage);
                cmd.Parameters.AddWithValue("@StackTrace", stackTrace);
                cmd.Parameters.AddWithValue("@Method", method);
                cmd.Parameters.AddWithValue("@HttpRequest", httpRequest);
                cmd.Parameters.AddWithValue("@UserName", userName);
                
                cmd.Parameters.AddWithValue("@Host", hostName);
                cmd.Parameters.AddWithValue("@UserAgent", userAgent);
                cmd.Parameters.AddWithValue("@ExceptionLogApplicationTypeID", 1);

                cmd.ExecuteNonQuery();
            }
        }
    }
}
