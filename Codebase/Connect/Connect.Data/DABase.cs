﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal static class DABase
    {
        private static class DB
        {
            public class ConnectDB
            {
                public static String SP_Schema
                {
                    get { return ConfigUtility.AppSettingGetValue("Connect.DB.SPSchema"); }
                }

                public static String ConnStr
                {
                    get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
                }

                public static Int32 CommandTimeout
                {
                    get
                    {
                        int timeout;
                        int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout"), out timeout);
                        return timeout;
                    }
                }
            }

            public class GlobalWebDB
            {
                public static String SP_Schema
                {
                    get { return ConfigUtility.AppSettingGetValue("Connect.DB.GlobalWeb.SPSchema"); }
                }

                public static String ConnStr
                {
                    get { return ConfigUtility.ConnStrGetValue("Connect.DB.GlobalWeb.ConnStr"); }
                }

                public static Int32 CommandTimeout
                {
                    get
                    {
                        int timeout;
                        int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.GlobalWeb.CmdTimeout"), out timeout);
                        return timeout;
                    }
                }

                public class InlineSQL
                {
                    public String ISO_Country_Select
                    {
                        get
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("SELECT * FROM [dbo].[AR_ISO_Country] (NOLOCK)");
                            return sb.ToString();
                        }
                    }
                }
            }
        }

        public static class SP
        {
            public class Sec
            {
                public const String UserCache_SelectByUserCacheID = "[uSP_Sec_UserCache_SelectByUserCacheID]";
                public const String UserCache_SelectByMembershipId = "[uSP_Sec_UserCache_SelectByMembershipId]";
                public const String UserCache_SelectByMembershipIdDataKey = "[uSP_Sec_UserCache_SelectByMembershipIdDataKey]";
                public const String UserCache_InsertUpdate = "[uSP_Sec_UserCache_InsertUpdate]";

                public const String UserMembership_SelectAllFiltered = "[uSP_Sec_UserMembership_SelectAllFiltered]";
                public const String UserMembership_AdminUserSearch = "[uSP_Sec_UserMembership_UserSearch]";
                public const String UserMembership_SelectByEmailAddress2222_ToBeDeleted = "[uSP_Sec_UserMembership_SelectByEmailAddress]";
                public const String UserMembership_SelectByEmailAddressOrUserID = "[uSP_Sec_UserMembership_SelectByEmailAddressOrUserID]";
                public const String UserMembership_SelectByMembershipId = "[uSP_Sec_UserMembership_SelectByMembershipId]";
                public const String UserMembership_SelectHistoryByMembershipId = "[uSP_Sec_UserMembership_SelectHistoryByMembershipId]";
                public const String UserMembership_SelectByGWUserId = "[uSP_Sec_UserMembership_SelectByGWUserId]";
                public const String UserMembership_Delete = "[uSP_Sec_UserMembership_Delete]";
                public const String UserMembership_DeleteFromAccount = "[uSP_Sec_UserMembership_DeleteFromAccount]";


                public const String UserMembership_Insert = "[uSP_Sec_UserMembership_Insert]";
                public const String UserMembership_Update = "[uSP_Sec_UserMembership_Update]";
                public const String UserMembership_UpdateUserEmailInfo = "[uSP_Sec_UserMembership_UpdateUserEmailInfo]";
                public const String UserMembership_UpdateUserNameInEnglish = "[uSP_Sec_UserMembership_UpdateUserNameInEnglish]";
                public const String UserMembership_UpdateLastActivity = "[uSP_Sec_UserMembership_UpdateLastActivity]";
                public const String UserMembership_UpdateUnlock = "[uSP_Sec_UserMembership_UpdateUnlock]";
                public const String UserMembership_SelectByAccountID = "[uSP_Sec_UserMembership_SelectByAccountID]";
                public const String UserMembership_SelectByAccountID_ForUser = "[uSP_Sec_UserMembership_SelectByAccountID_ForUser]";
                public const String UserMembership_SelectLastActiveByMinutes = "[uSP_Sec_UserMembership_SelectLastActiveByMinutes]";

                public const String UserAccountRole_Select = "[uSP_Sec_UserAccountRole_Select]";
                public const String UserAccountRole_SelectAll = "[uSP_Sec_UserAccountRole_SelectAll]";
                public const String UserAccountRole_SelectForUserAdmin = "[uSP_Sec_UserAccountRole_SelectByMembershipId]";
                public const String UserAccountRole_SelectAvailableAccounts = "[uSP_Sec_UserAccountRole_SelectAvailableAccounts]";
                public const String UserAccountRole_SelectSearchedAccounts = "[uSP_Sec_UserAccountRole_SelectAutoSearchAccounts]";
                public const String UserAccountRole_Insert = "[uSP_Sec_UserAccountRole_Insert]";
                public const String UserAccountRole_Delete = "[uSP_Sec_UserAccountRole_Delete]";
                public const String UserAccountRole_DeleteByAccountIDMembershipIdRole = "[uSP_Sec_UserAccountRole_DeleteByAccountIDMembershipIdRole]";
                public const String UserAccountRole_DeleteByAccountIDMembershipId = "[uSP_Sec_UserAccountRole_DeleteByAccountIDMembershipId]";
                public const String UserAccountRole_SelectTotalAccounts = "[uSP_Sec_UserAccountRole_SelectTotalAccounts]";
                public const String Sec_Role_SelectAll = "[uSP_Sec_Role_SelectAll]";
                public const String UserAccount_Organazation_SelectSearchedAccounts = "[uSP_Package_Organization_SelectAutoSearchAccounts]";
                public const String UserMembership_SelectByTeamOwnerId = "[uSP_Sec_UserMembership_SelectByTeamOwnerId]";
            }

            public class Res
            {
                public const String ContentStore_Select = "[uSP_Res_ContentStore_Select]";
                public const String SelectResourceKey = "[uSP_Res_ContentStore_SelectResourceKey]";
                public const String SelectByClassKeyResourceKey = "[uSP_Res_ContentStore_SelectByClassKeyResourceKey]";
                public const String SelectByContentID = "[uSP_Res_ContentStore_SelectByContentID]";
                public const String Insert = "[uSP_Res_ContentStore_Insert]";
                public const String Update = "[uSP_Res_ContentStore_Update]";
                public const String InsertUpdate = "[uSP_Res_ContentStore_InsertUpdate]";
                public const String DeleteByContentID = "[uSP_Res_ContentStore_DeleteByContentID]";
                public const String DeleteByClassKeyResKey = "[uSP_Res_ContentStore_DeleteByClassKeyResKey]";

                public const String SelectDistinctLocale = "[uSP_Res_ContentStore_SelectDistinctLocale]";
                public const String SelectFiltered = "[uSP_Res_ContentStore_SelectFiltered]";
                public const String SelectForGoogleTranslateAPI = "[uSP_Res_ContentStore_SelectForGoogleTranslateAPI]";
                public const String ContentSupportedLocale_SelectAll = "[uSP_Res_ContentSupportedLocale_SelectAll]";

                public const String SelectClassKeyLocale = "[uSP_Res_ContentStore_SelectClassKeyLocale]";
                public const String SelectByClassKeyLocale = "[uSP_Res_ContentStore_SelectByClassKeyLocale]";
            }

            public class Acc
            {
                public const String SelectByAccountID = "[uSP_Acc_AccountMaster_SelectByAccountID]";
                public const String SelectByCompany = "[uSP_Acc_AccountMaster_SelectByCompany]";
                public const String SelectAccountHistoryByID = "[uSP_Acc_AccountMaster_SelectHistoryByAccountID]";
                public const String AccountMaster_SelectAllFiltered = "[uSP_Acc_AccountMaster_SelectAllFiltered]";
                public const String AccountMaster_SearchFilteredResult = "[uSP_Acc_AccountMaster_SearchFilteredResult]";
                public const String AccountMaster_SelectByMembershipId = "[uSP_Acc_AccountMaster_SelectByMembershipId]";
                public const String Insert = "[uSP_Acc_AccountMaster_Insert]";
                public const String Update = "[uSP_Acc_AccountMaster_Update]";
                public const String Delete = "[uSP_Acc_AccountMaster_Delete]";
                public const String UpdateApprove = "[uSP_Acc_AccountMaster_UpdateApprove]";
                public const String UpdateDisApprove = "[uSP_Acc_AccountMaster_UpdateDisApprove]";
                public const String UpdateNotes = "[uSP_Acc_AccountMaster_UpdateNotes]";
                public const String UpdateAgreeTerms = "[uSP_Acc_AccountMaster_UpdateAgreeTerms]";
                public const String AccountVerifications_Select = "[uSP_Acc_AccountVerifications_Select]";
                public const String SelectAccountsByPackageID = "[uSP_CompanyOrganization_Select]";
            }

            public class Site
            {
                public const String Sitemap_SelectByPageUrl = "[uSP_Site_Sitemap_SelectByPageUrl]";
                public const String Sitemap_Delete = "[uSP_Site_Sitemap_Delete]";
                public const String Sitemap_Update = "[uSP_Site_Sitemap_Update]";
                public const String Sitemap_Insert = "[uSP_Site_Sitemap_Insert]";
                public const String SelectForBreadCrumbByPageUrl = "[uSP_Site_Sitemap_SelectForBreadCrumbByPageUrl]";

                public const String Feature_SelectAll = "[uSP_Site_Feature_SelectAll]";
                public const String Feature_SelectByFeatureID = "[uSP_Site_Feature_SelectByFeatureID]";
                public const String Feature_SelectByFeatureKey = "[uSP_Site_Feature_SelectByFeatureKey]";

                public const String Module_SelectAll = "[uSP_Site_Module_SelectAll]";
                public const String Module_SelectByFeatureID = "[uSP_Site_Module_SelectByFeatureID]";
                public const String Module_SelectByModuleID = "[uSP_Site_Module_SelectByModuleID]";

                public const String Module_Update = "[uSP_Site_Module_Update]";
                public const String Module_Insert = "[uSP_Site_Module_Insert]";
                public const String Module_Delete = "[uSP_Site_Module_Delete]";
                public const String ModuleSecurity_SelectByModuleID = "[uSP_Site_ModuleSecurity_SelectByModuleID]";

                public const String ModulePage_SelectByModuleID = "[uSP_Site_ModulePage_SelectByModuleID]";
                public const String ModulePage_SelectByPageUrl = "[uSP_Site_ModulePage_SelectByPageUrl]";
                public const String ModulePage_Insert = "[uSP_Site_ModulePage_Insert]";
                public const String ModulePage_Update = "[uSP_Site_ModulePage_Update]";
                public const String ModulePage_Delete = "[uSP_Site_ModulePage_Delete]";

                public const String ModuleSetting_SelectByModuleID = "[uSP_Site_ModuleSetting_SelectByModuleID]";
                public const String ModuleSetting_DeleteByModuleSettingID = "[uSP_Site_ModuleSetting_DeleteByModuleSettingID]";
                public const String ModuleSetting_InsertUpdate = "[uSP_Site_ModuleSetting_InsertUpdate]";
                public const String ModuleSetting_SelectByModuleIDSettingKey = "[uSP_Site_ModuleSetting_SelectByModuleIDSettingKey]";

            }

            public class DynamicForm
            {
                public const String Form_Insert = "[uSP_frm_Form_Insert]";
                public const String Form_Update = "[uSP_Frm_Form_Update]";
                public const String Form_Delete = "[uSP_Frm_Form_DeleteByFormID]";

                public const String Form_SelectAll = "[uSP_Frm_Form_SelectAll]";
                public const String Form_SelectByFormID = "[uSP_frm_Form_SelectByFormID]";
                public const String Form_SelectByModuleID = "[uSP_frm_Form_SelectByModuleID]";
                public const String Form_SelectModuleIDFieldsetIDByFwscID = "[uSP_Frm_Form_SelectModuleIDFieldsetIDByFwscID]";
                public const String Form_SelectModelsByFormID = "[uSP_frm_Form_SelectModelsByFormID]";

                public const String Form_Control_SelectAll = "[uSP_Frm_FormControl_SelectAll]";
                public const String Form_Control_SelectByControlID = "[uSP_Frm_FormControl_SelectByControlID]";

                public const String FormFieldset_SelectByFieldsetID = "[uSP_Frm_FormFieldset_SelectByFieldsetID]";
                public const String FormFieldset_SelectByFormID = "[uSP_Frm_FormFieldset_SelectByFormID]";
                public const String FormFieldset_SelectByModuleID = "[uSP_Frm_FormFieldset_SelectByModuleID]";
                public const String FormFieldset_Insert = "[uSP_Frm_FormFieldset_Insert]";
                public const String FormFieldset_Update = "[uSP_Frm_FormFieldset_Update]";
                public const String FormFieldset_DeleteByFieldsetID = "[uSP_Frm_FormFieldset_DeleteByFieldsetID]";

                public const String FormFieldsetControlProperty_Select = "[uSP_Frm_FormFieldsetControlProperty_Select]";
                public const String FormFieldsetControlProperty_InsertUpdate = "[uSP_Frm_FormFieldsetControlProperty_InsertUpdate]";
                public const String FormFieldsetControlProperty_DeleteByPropertyID = "[uSP_Frm_FormFieldsetControlProperty_DeleteByPropertyID]";

                public const String FormFieldsetControl_SelectByFieldsetID = "[uSP_Frm_FormFieldsetControl_SelectByFieldsetID]";
                public const String FormFieldsetControl_SelectByControlID = "[uSP_Frm_FormFieldsetControl_SelectByControlID]";
                public const String FormFieldsetControl_SelectByFwscID = "[uSP_Frm_FormFieldsetControl_SelectByFwscID]";
                public const String FormFieldsetControl_Update = "[uSP_Frm_FormFieldsetControl_Update]";
                public const String FormFieldsetControl_Insert = "[uSP_Frm_FormFieldsetControl_Insert]";
                public const String FormFieldsetControl_Delete = "[uSP_Frm_FormFieldsetControl_Delete]";
            }

            public class DynamicFormSubmit
            {
                public const String FormSubmitMaster_Insert = "[uSP_Frm_FormSubmitMaster_Insert]";

                public const String FormSubmitData_InsertStr = "[uSP_Frm_FormSubmitData_InsertStr]";
            }

            public class ControlDataSource
            {
                public const String FormControlDataSource_SelectAll = "[uSP_Frm_FormControlDataSource_SelectAll]";
                public const String FormControlDataSource_SelectByDataSourceID = "[uSP_Frm_FormControlDataSource_SelectByDataSourceID]";
                public const String FormControlDataSource_Insert = "[uSP_Frm_FormControlDataSource_Insert]";
                public const String FormControlDataSource_Update = "[uSP_Frm_FormControlDataSource_Update]";
                public const String FormControlDataSource_DeleteByDataSourceID = "[uSP_Frm_FormControlDataSource_DeleteByDataSourceID]";

                public const String FormControlDataSourceData_SelectByDSDataID = "[uSP_Frm_FormControlDataSourceData_SelectByDSDataID]";
                public const String FormControlDataSourceData_SelectByDataSourceID = "[uSP_Frm_FormControlDataSourceData_SelectByDataSourceID]";
                public const String FormControlDataSourceData_Insert = "[uSP_Frm_FormControlDataSourceData_Insert]";
                public const String FormControlDataSourceData_Update = "[uSP_Frm_FormControlDataSourceData_Update]";
                public const String FormControlDataSourceData_DeleteByDSDataID = "[uSP_Frm_FormControlDataSourceData_DeleteByDSDataID]";

            }

            public class Profile_Address
            {
                public const String DeleteByAccountID = "[uSP_Profile_Address_DeleteByAccountID]";
                public const String DeleteByAddressID = "[uSP_Profile_Address_DeleteByAddressID]";
                public const String Update = "[uSP_Profile_Address_Update]";
                public const String Insert = "[uSP_Profile_Address_Insert]";
                public const String SelectByMemberShipID = "[uSP_Profile_Address_SelectByAddressID]";
                public const String SelectByEmailID = "[uSP_Profile_Address_SelectByEmailID]";
                public const String SelectByAccountID = "[uSP_Profile_Address_SelectByAccountID]";
                public const String SelectHistoryByAccountID = "[uSP_Profile_Address_History_SelectByAccountID]";


            }

            public class Notification
            {
                public const String NotificationDisplayAreas_SelectAll = "[uSP_NotificationDisplayAreas_SelectAll]";
                public const String Notification_Insert = "[uSP_Notification_Insert]";
                public const String Notification_SelectByNotificationID = "[uSP_Notification_SelectByID]";
                public const String Notification_DeleteByNotificationID = "[uSP_Notification_DeleteByID]";
                public const String Notification_SelectAll = "[uSP_Notification_SelectAll]";
                public const String Notification_Update = "[uSP_Notification_Update]";
                public const String Notification_SelectActiveOrDefault = "[uSP_Notification_SelectActiveOrDefault]";
            }

            public class ActivityLog
            {
                public const String AdminActivityLog_Insert = "[uSP_AdminActivityLog_Insert]";
                public const String UserActivityLog_Insert = "[uSP_ClientActivityLog_Insert]";
            }
            public class WebMasterContact
            {
                public const String Insert = "[uSP_ContactUS_Insert]";
            }
            public class Package
            {
                public const String System_SelectAll = "[uSP_Package_System_SelectAll]";
                public const String Package_Insert = "[uSP_Package_Insert]";
                public const String PackageFiles_Insert = "[uSP_PackageFiles_Insert]";
                public const String Package_SelectByPackageID = "[uSP_Package_SelectByID]";
                public const String Package_Org_Insert = "[uSP_Package_Org_Insert]";
                public const String Package_Org_SelectByPackageID = "[uSP_Package_Org_SelectByPackageId]";
                public const String Package_Org_DeleteMapping = "[uSP_Package_Org_Delete]";
                public const String Package_DeleteByID = "[uSP_Package_Delete]";
                public const String Package_Update = "[uSP_Package_Update]";
                public const String PackageWithFiles_Select = "[uSP_PackageWithFiles_Select]";
                public const String PackageFiles_Update = "[uSP_PackageFiles_Update]";
                public const String EmailAddressByAccount_Select = "[uSP_EmailAddressByAccount_Select]";
                public const String PackageModels_Insert = "[uSP_PackageModels_Insert]";
                public const String Package_SelectAll = "[uSP_Package_SelectAll]";
                public const String Package_SelectAllByAccountID = "[uSP_Package_Organization_SelectByAccount]";
                public const String Package_Org_BulkUpdate = "[uSP_Package_Org_BulkUpdate]";
                public const String Package_SelectAllByModelNumber = "[uSP_Package_SelectAllByModelNumber]";
                public const String PackageWithFilesByAccount_Select = "[uSP_Select_PackageWithFilesByAccount]";
                public const String PackagesByAccount_Select = "[uSP_Select_PackagesByAccount]";
                public const String PackageWithFilesByPackageID_Select = "[uSP_Select_PackageWithFilesByPackageID]";
                public const String PackageFilesByPackageID_Select = "[uSP_Select_PackageFilesByPackageID]";
                public const String PackageUserAccessBulkUpdate = "[uSP_Package_User_BulkUpdate]";
                public const String JpPackageGroupsSelect = "[uSP_Select_JPPackageGroups]";
                public const String JpPackageSubFoldersSelect = "[uSP_Select_PackageSubFoldersByPackageID]";
                public const String JpPackageSubFolderFilesSelect = "[uSP_Select_JpSubFolderPackageFiles]";
                public const String JpPackageInfoSelect = "[uSP_Select_JPPackageInfo]";
                public const String JpPackageAssignUser = "[uSP_Package_User_Assign]";
                public const String PackageComments_Copy = "[uSP_PackageFileComments_Copy]";
                public const String GetAutoDownloadPackageFilesInfo = "[uSP_GetAutoDownloadPackageFilesInfo]";

            }



            //public class Erp
            //{
            //    public const String ProductConfig_SelectByModelNumber = "[uSP_Erp_ProductConfig_SelectByModelNumber]";
            //    public const String ProductConfig_SelectWithFilter = "[uSP_Erp_ProductConfig_SelectWithFilter]";
            //}
        }

        public static SqlConnection GetOpenedConn()
        {
            return SQLUtility.ConnOpen(DB.ConnectDB.ConnStr);
        }

        public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
        {
            return SQLUtility.MakeSPCmd(conn, tran, DB.ConnectDB.SP_Schema, spName, DB.ConnectDB.CommandTimeout);
        }
    }
}
