﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DACfg_ModelConfig
    {
        public static DataRow GetByModelNumber(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfig.SelectByModel);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                DataRow r = db.FillInDataRow(cmd);
                return r;
            }
        }

        public static DataRow GetByModelConfgType(String modelNumber, String modelConfigType)
        {
            if (modelNumber.IsNullOrEmptyString() || modelConfigType.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfig.GetByModelConfgType);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType.Trim());
                DataRow r = db.FillInDataRow(cmd);
                return r;
            }
        }
        public static DataTable GetByModelConfgType(String modelNumber, String modelConfigType, bool exactMatch)
        {
            if (modelNumber.IsNullOrEmptyString() || modelConfigType.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfig.GetByModelConfgType);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType.Trim());
                cmd.Parameters.AddWithValue("@ExactMatch", exactMatch);
                return db.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByModel(String modelNumber, SqlConnection conn, SqlTransaction tran)
        {
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ModelConfig.SelectByModel);
            SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataTable SelectAllActiveByModel(String modelNumber, SqlConnection conn, SqlTransaction tran)
        {
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ModelConfig.SelectAllActiveByModel);
            SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
            return SQLUtility.FillInDataTable(cmd);
        }

        

        //public static void Update(String modelNumber
        //   , String modelConfigType
        //   , String modelConfigStatus
        //   , String initialStatus
        //   , Boolean useModelTags
        //   , Boolean verifyCompany
        //   , Boolean verifyRegistration
        //   , Boolean userACLRequired
        //   , Boolean isPaidSupportModel
        //   , String submittedBy
                 
        //   )
        //{
        //    if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
        //    DBConnect db = new DBConnect();
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfig.UpdateModelConfig);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelConfigType", modelConfigType);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelConfigStatus", modelConfigStatus);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@InitialStatus", initialStatus);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UseModelTags", useModelTags);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyCompany", verifyCompany);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyRegistration", verifyCompany);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UserACLRequired", userACLRequired);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@HasSupportExpiration", isPaidSupportModel);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@UpdatedByEmail", submittedBy);
                
               
        //        cmd.ExecuteNonQuery();
        //    }
        //}

        public static void InsertUpdate(String modelNumber
           , String modelConfigType
           , String modelConfigStatus
           , String initialStatus
           , Boolean useModelTags
           , Boolean verifyCompany
           , Boolean verifyRegistration
           , Boolean userACLRequired
           , Boolean isPaidSupportModel
           , String submittedBy
           , String internalNote
           , String assignedOwner

           )
        {
            if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfig.InsertUpdateModelConfig);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelConfigType", modelConfigType);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelConfigStatus", modelConfigStatus);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@InitialStatus", initialStatus);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UseModelTags", useModelTags);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyCompany", verifyCompany);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyRegistration", verifyRegistration);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UserACLRequired", userACLRequired);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@HasSupportExpiration", isPaidSupportModel);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SubmittedBy", submittedBy);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@InternalNote", internalNote);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@AssignedOwner", assignedOwner);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
