﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DACfg_ModelConfigStatus
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelConfigTypeStatus.SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}