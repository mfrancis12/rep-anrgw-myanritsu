﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DACfg_ModelMaster
    {
        public static DataTable SelectByModel(String modelNumber, Boolean exactMatch)
        {
            if(modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelMaster.SelectByModel);
                SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.AddParam(ref cmd, "@ExactMatch", exactMatch);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void SelectConfigurations(String modelNumber, out DataTable tbModelConfig, out DataTable tbModelTags)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tbModelConfig = DACfg_ModelConfig.SelectByModel(modelNumber, conn, null);
                tbModelTags = DACfg_ModelTags.SelectByModel(modelNumber, conn, null);
            }
        }

        public static void SelectActiveConfigurations(String modelNumber, out DataTable tbModelConfig, out DataTable tbModelTags)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tbModelConfig = DACfg_ModelConfig.SelectAllActiveByModel(modelNumber, conn, null);
                tbModelTags = DACfg_ModelTags.SelectByModel(modelNumber, conn, null);
            }
        }

        //public static void Update(String modelNumber
        //  ,  String imageUrl
        //   , Boolean validateSN
        //   , String snPattern
        //   , Guid addOnFormID
        //   , Boolean addOnFormPerSN
        //   , Guid feedbackFormID
        //   , Boolean allowReg
        //   , Boolean showSupportAgreement
        //   )
        //{
        //    if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
         
        //    DBConnect db = new DBConnect();
        //    using (SqlConnection conn = db.GetOpenedConn())
        //    {
        //        SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelMaster.update);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@CustomImageUrl", imageUrl);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ValidateSN", validateSN);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@SNPattern", snPattern);
        //        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@FeedbackFormID", feedbackFormID);
        //        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AddOnFormID", addOnFormID);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AddOnFormPerSN", addOnFormPerSN);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AllowSelfReg", allowReg);
        //        SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ShowSupportAgreement", showSupportAgreement);
        //        cmd.ExecuteNonQuery();
        //    }
        //}


        public static void InsertUpdate(String modelNumber
          , String imageUrl
          , Boolean validateSn
          , String snPattern
          , Guid addOnFormId
          , Boolean addOnFormPerSn
          , Guid feedbackFormId
          , Boolean allowReg
          , Boolean showSupportAgreement
          , Boolean showWarrantyInfo
          )
        {
            if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelMaster.InsertUpdate);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CustomImageUrl", imageUrl);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ValidateSN", validateSn);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SNPattern", snPattern);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@FeedbackFormID", feedbackFormId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AddOnFormID", addOnFormId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AddOnFormPerSN", addOnFormPerSn);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AllowSelfReg", allowReg);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ShowSupportAgreement", showSupportAgreement);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ShowWarrantyInfo", showWarrantyInfo);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
