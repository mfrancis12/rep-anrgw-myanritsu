﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DACfg_ModelTags
    {
        public static DataTable SelectByModelNumber(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByModel(modelNumber.Trim(), conn, null);
            }
        }

        public static DataTable SelectByModel(String modelNumber, SqlConnection conn, SqlTransaction tran)
        {
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ModelTags.SelectByModel);
            SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataRow Insert(String modelNumber, String tagModelNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || tagModelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelTags.Insert);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@TagModelNumber", tagModelNumber.Trim());
                return db.FillInDataRow(cmd);
            }
        }

        public static Int32 Delete(Int32 modelTagId)
        {
            if (modelTagId < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelTags.Delete);
                cmd.Parameters.AddWithValue("@ModelTagID", modelTagId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteAll(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelTags.DeleteByModelNumber);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                cmd.ExecuteNonQuery();
            }

        }
    }
}
