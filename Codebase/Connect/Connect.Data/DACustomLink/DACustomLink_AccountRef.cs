﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DACustomLink_AccountRef
    {
        public static DataTable SelectByCustomLinkID(Int32 customLinkId)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByCustomLinkID(customLinkId, conn, null);
            }
        }

        internal static DataTable SelectByCustomLinkID(Int32 customLinkId, SqlConnection conn, SqlTransaction tran)
        {
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.RefAccount_SelectByCustomLinkID);
            SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static void Insert(Int32 customLinkId, Guid accountId, String createdBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefAccount_Insert);
                SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteByCustomLinkAccRefID(Int32 customLinkAccRefId)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefAccount_DeleteByCustomLinkAccRefID);
                SQLUtility.AddParam(ref cmd, "@CustomLinkAccRefID", customLinkAccRefId);
                cmd.ExecuteNonQuery();
            }
        }

        public static void SetShowForAll(Boolean showForAll, Int32 customLinkId, String createdBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefAccount_SetShowForAll);
                SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                SQLUtility.AddParam(ref cmd, "@ShowForAll", showForAll);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
