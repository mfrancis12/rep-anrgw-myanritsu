﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DACustomLink_Master
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.Master_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Insert(String lnkOwnerRole, String lnkInternalName, String displayContainerId,
            Int32 lnkOrder, String targetUrl, String linkTarget, String iconImageUrl, String wrapperCss, DateTime startOnUtc, DateTime expireOnUtc, String createdBy)
        {
            DBConnect db = new DBConnect();
            SqlTransaction tran = null;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.Master_Insert);
                    SQLUtility.AddParam(ref cmd, "@LnkOwnerRole", lnkOwnerRole);
                    SQLUtility.AddParam(ref cmd, "@LnkInternalName", lnkInternalName);
                    SQLUtility.AddParam(ref cmd, "@DisplayContainerID", displayContainerId);
                    SQLUtility.AddParam(ref cmd, "@LnkOrder", lnkOrder);
                    SQLUtility.AddParam(ref cmd, "@TargetUrl", targetUrl);
                    SQLUtility.AddParam(ref cmd, "@LinkTarget", linkTarget);
                    SQLUtility.AddParam(ref cmd, "@IconImageUrl", iconImageUrl);
                    SQLUtility.AddParam(ref cmd, "@WrapperCss", wrapperCss);
                    SQLUtility.AddParam(ref cmd, "@StartOnUTC", startOnUtc);
                    SQLUtility.AddParam(ref cmd, "@ExpireOnUTC", expireOnUtc);
                    SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);

                    DataRow row = SQLUtility.FillInDataRow(cmd);
                    tran.Commit();
                    return row;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void Update(Int32 customLinkId, String lnkInternalName,String displayContainerId,
            Int32 lnkOrder, String targetUrl, String linkTarget, String iconImageUrl, String wrapperCss, DateTime startOnUtc, DateTime expireOnUtc, String modifiedBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlTransaction tran = conn.BeginTransaction();
                try
                {
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.Master_Update);
                    SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                    SQLUtility.AddParam(ref cmd, "@LnkInternalName", lnkInternalName);
                    SQLUtility.AddParam(ref cmd, "@DisplayContainerID", displayContainerId);
                    SQLUtility.AddParam(ref cmd, "@LnkOrder", lnkOrder);
                    SQLUtility.AddParam(ref cmd, "@TargetUrl", targetUrl);
                    SQLUtility.AddParam(ref cmd, "@LinkTarget", linkTarget);
                    SQLUtility.AddParam(ref cmd, "@IconImageUrl", iconImageUrl);
                    SQLUtility.AddParam(ref cmd, "@WrapperCss", wrapperCss);
                    SQLUtility.AddParam(ref cmd, "@StartOnUTC", startOnUtc);
                    SQLUtility.AddParam(ref cmd, "@ExpireOnUTC", expireOnUtc);
                    SQLUtility.AddParam(ref cmd, "@ModifiedBy", modifiedBy);

                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }

        public static void Delete(Int32 customLinkId)
        {
            DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.Master_Delete);
                    SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                    cmd.ExecuteNonQuery();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }
    }
}
