﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DACustomLink_OwnerRoles
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.OwnerRoles_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
