﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DACustomLink_RefSisProd
    {
        public static DataTable SelectByCustomLinkID(Int32 customLinkId)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByCustomLinkID(customLinkId, conn, null);
            }
        }

        internal static DataTable SelectByCustomLinkID(Int32 customLinkId, SqlConnection conn, SqlTransaction tran)
        {
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.RefSisProd_SelectByCustomLinkID);
            SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static void Insert(Int32 customLinkId, Int32 sisPid, String createdBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefSisProd_Insert);
                SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                SQLUtility.AddParam(ref cmd, "@SisPID", sisPid);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteByRefSisProdID(Int32 refSisProdId)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefSisProd_DeleteByRefSisProdID);
                SQLUtility.AddParam(ref cmd, "@RefSisProdID", refSisProdId);
                cmd.ExecuteNonQuery();
            }
        }

        public static void SetShowForAll(Boolean showForAll, Int32 customLinkId, String createdBy)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.RefSisProd_SetShowForAll);
                SQLUtility.AddParam(ref cmd, "@CustomLinkID", customLinkId);
                SQLUtility.AddParam(ref cmd, "@ShowForAll", showForAll);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
