﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DALnk_LinkDisplay
    {
        public static DataTable Select(
            String pageUrl,
            String locationType,
            String accountRegion,
            String modelNumber,
            Guid accountId
            )
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.Display_Select);
                SQLUtility.AddParam(ref cmd, "@PageUrl", pageUrl);
                SQLUtility.AddParam(ref cmd, "@LocationType", locationType);
                SQLUtility.AddParam(ref cmd, "@AccountRegion", accountRegion.ConvertNullToEmptyString().Trim().ToUpperInvariant());
                SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
