﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DALnk_LinkDisplaySettings
    {
        public static DataTable SelectByLnkID(Int32 lnkId)
        {
            if (lnkId < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByLnkID(lnkId, conn, null);
            }
        }

        public static DataTable SelectByLnkID(Int32 lnkId, SqlConnection conn, SqlTransaction tran)
        {
            if (lnkId < 1) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_CustomLink.LinkSettings_SelectByLnkID);
            SQLUtility.AddParam(ref cmd, "@LnkID", lnkId);
            return SQLUtility.FillInDataTable(cmd);
        }
    }
}
