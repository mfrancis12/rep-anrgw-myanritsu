﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DACustomLink
{
    public static class DALnk_LinkMaster
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.LinkMaster_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByLnkID(Int32 lnkId, out DataTable config, out DataTable settings)
        {
            config = null;
            settings = null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_CustomLink.LinkMaster_SelectByLnkID);
                SQLUtility.AddParam(ref cmd, "@LnkID", lnkId);
                DataRow row = SQLUtility.FillInDataRow(cmd);
                if (row == null) return null;
                config = DALnk_LinkConfig.SelectByLnkID(lnkId, conn, null);
                settings = DALnk_LinkDisplaySettings.SelectByLnkID(lnkId, conn, null);
                return row;
            }
        }
    }
}
