﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DADownload
    {
        private static String GetGwDownloadUrl(string dlUrl)
        {
            String baseUrl = ConfigUtility.GetStringProperty("Connect.GWDownloadBaseUrl", "http://www.anritsu.com");
            //if (dlUrl.StartsWith("/en-US"))
            //    dlUrl = dlUrl.Replace("/en-US/", "/");
            return (baseUrl + dlUrl).ToLower();
        }

        private static String GetJapanDownloadUrl(String modelName, String fileName)
        {
            //String baseUrl = ConfigUtility.GetStringProperty("Connect.JPDownloadBaseUrl", "http://www.anritsu.com");
            //return baseUrl + "?id=" + id.ToString();
            return String.Format("/{0}/{1}", modelName, fileName);
        }

        public static DataTable GetDownloadTableSchema()
        {
            DataTable tb = new DataTable("Downloads");
            DataColumn col;

            col = new DataColumn("DownloadID", SqlDbType.Int.GetType());
            tb.Columns.Add(col);

            col = new DataColumn("CategoryID", SqlDbType.Int.GetType());
            tb.Columns.Add(col);

            col = new DataColumn("CategoryEngName", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("DownloadURL", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("DownloadTitle", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("ModelNumber", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("ReleaseDateUTC", typeof(DateTime));
            tb.Columns.Add(col);

            col = new DataColumn("Version", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("SizeKB", typeof(double));
            tb.Columns.Add(col);

            col = new DataColumn("SizeMB", typeof(double));
            tb.Columns.Add(col);

            col = new DataColumn("FileName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("FileExt", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("IsJPDL", typeof(Boolean));
            col.DefaultValue = false;
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_Target", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_UpDate", typeof(DateTime));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_Category", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_ModelName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_VersionFileName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_EncodeFlag", typeof(Int32));
            tb.Columns.Add(col);

            col = new DataColumn("ShowNewFlag", typeof(Boolean));
            col.DefaultValue = false;
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_Comment", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPDL_SoftTitle", typeof(String));
            col.DefaultValue = "n/a";
            tb.Columns.Add(col);

            tb.AcceptChanges();
            return tb;
        }

        public static DataTable SelectByModel(List<String> modelTags, 
            List<String> jpDownloadUserIDs,
            int gwCultureGroupId,
            Boolean includeDlSrcGw,
            Boolean includeDlSrcJp
            )
        {
            if (modelTags == null || modelTags.Count < 1) return null;
            DataTable tb = GetDownloadTableSchema();

            if (includeDlSrcGw)
            {
                #region " GW DOWNLOADS "
                DBGlobalWeb gwdb = new DBGlobalWeb();
                using (SqlConnection conn = gwdb.GetOpenedConn())
                {
                    SqlCommand cmd = gwdb.MakeCmdInline(conn, null, DBGlobalWeb.SQL_Inline.GWDownload_SelectByModel);
                    cmd.Parameters.AddWithValue("@ModelNumber", String.Empty);
                    cmd.Parameters.AddWithValue("@CultureGroupId", gwCultureGroupId);
                    foreach (String mn in modelTags)
                    {
                        if (String.IsNullOrEmpty(mn)) continue;
                        cmd.Parameters["@ModelNumber"].Value = mn.Trim();
                        SqlDataReader r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            String fileExt = ConvertUtility.ConvertToFileExtention(r["FilePath"], "n/a");
                            if (fileExt.Equals(".asp", StringComparison.InvariantCultureIgnoreCase)) continue;
                            DataRow rGwDl = tb.NewRow();
                            InitConnectDownloadInfoFromUSDownload(ref rGwDl, r);
                            tb.Rows.Add(rGwDl);
                        }
                        r.Close();
                        tb.AcceptChanges();

                    }

                }
                #endregion
            }
            if (includeDlSrcJp)
            {
                #region " JP DOWNLOADS "
                String jpDownloadUserIdStr = String.Empty;
                if (jpDownloadUserIDs != null && jpDownloadUserIDs.Count > 0)
                    jpDownloadUserIdStr = String.Join(";", jpDownloadUserIDs.ToArray());

                DBJpDl jpdldb = new DBJpDl();
                using (SqlConnection conn = jpdldb.GetOpenedConn())
                {
                    using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
                    {
                        SqlCommand cmd = jpdldb.MakeCmdSP(conn, null, DBJpDl.SQL_SP.SelectObjectByModelv1);
                        cmd.Parameters.AddWithValue("@ModelName", String.Empty);
                        cmd.Parameters.AddWithValue("@CommaSeperatedJPDLUserIDs", jpDownloadUserIdStr);

                        SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
                        cmdForVersion.Parameters.AddWithValue("@ModelName", String.Empty);

                        foreach (String mn in modelTags)
                        {
                            if (String.IsNullOrEmpty(mn)) continue;
                            cmd.Parameters["@ModelName"].Value = mn.Trim();
                            SqlDataReader r = cmd.ExecuteReader();

                            cmdForVersion.Parameters["@ModelName"].Value = mn.Trim();
                            DataRow rVersion = SQLUtility.FillInDataRow(cmdForVersion);
                            while (r.Read())
                            {
                                DataRow rGwDL = tb.NewRow();
                                InitConnectDownloadInfoFromJPDownload(ref rGwDL, r, rVersion, gwCultureGroupId, mn);
                                tb.Rows.Add(rGwDL);
                            }
                            r.Close();
                            tb.AcceptChanges();

                        }
                    }
                }
                #endregion
            }
            return tb;
        }

        internal static void InitConnectDownloadInfoFromUSDownload(ref DataRow connectDownloadRow
            , SqlDataReader gwDownloadRow)
        {
            connectDownloadRow["DownloadID"] = ConvertUtility.ConvertToInt32(gwDownloadRow["DownloadID"], 0);
            Int32 categoryId = ConvertUtility.ConvertToInt32(gwDownloadRow["CategoryID"], 0);
            connectDownloadRow["CategoryID"] = categoryId;
            connectDownloadRow["CategoryEngName"] = gwDownloadRow["CategoryName"].ToString();
            connectDownloadRow["DownloadURL"] = GetGwDownloadUrl(gwDownloadRow["DownloadURL"].ToString());
            connectDownloadRow["DownloadTitle"] = gwDownloadRow["Title"].ToString();
            connectDownloadRow["ModelNumber"] = gwDownloadRow["ModelNumber"].ToString();
            if (gwDownloadRow["ReleaseDateUTC"] != DBNull.Value)
            {

                //DateTime releaseDate = Convert.ToDateTime(gwDownloadRow["ReleaseDateUTC"]);
                //string culture= CultureInfo.CurrentUICulture.ToString();
                //DateTime test =
                connectDownloadRow["ReleaseDateUTC"] = gwDownloadRow["ReleaseDateUTC"];
                    //DateTime.Parse(releaseDate.ToShortDateString(), CultureInfo.CurrentUICulture);
                    //releaseDate.ToShortDateString.ToString("MMM dd, yyyy", System.Threading.Thread.CurrentThread.CurrentUICulture);
                //DateTime test = DateTime.ParseExact(releaseDate.ToString(), , null);
                //connectDownloadRow["ReleaseDateUTC"] = DateTime.ParseExact(releaseDate.ToString(), CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, null);
                //DateTime.ParseExact(gwDownloadRow["ReleaseDateUTC"].ToString(), CultureInfo.CurrentCulture.DateTimeFormat.ToString(),null);
            }
            //Category should be shownn empty as default(changed from "n/a" to string.empty - **shaswati
            connectDownloadRow["Version"] = ConvertUtility.ConvertToString(gwDownloadRow["Version"], string.Empty);

            String gwdlSizeKb = ConvertUtility.ConvertToString(gwDownloadRow["SizeKB"], "n/a").Trim();
            if (gwdlSizeKb != "n/a")
            {
                if (gwdlSizeKb.EndsWith("KB", StringComparison.InvariantCultureIgnoreCase))
                {
                    Double sizeInKb;
                    Double.TryParse(gwdlSizeKb.Replace(" ", "").Replace("KB", ""), out sizeInKb);
                    if (sizeInKb > 0)
                    {
                        connectDownloadRow["SizeKB"] = sizeInKb;
                        connectDownloadRow["SizeMB"] = sizeInKb / 1024;
                    }

                }
                else if (gwdlSizeKb.EndsWith("MB", StringComparison.InvariantCultureIgnoreCase))
                {
                    Double sizeInMb;
                    Double.TryParse(gwdlSizeKb.Replace(" ", "").Replace("MB", ""), out sizeInMb);
                    if (sizeInMb > 0)
                    {
                        connectDownloadRow["SizeKB"] = (sizeInMb * 1024.00);
                        connectDownloadRow["SizeMB"] = sizeInMb;
                    }
                }
            }

            //connectDownloadRow["SizeKB"] = ConvertUtility.ConvertToString(gwDownloadRow["SizeKB"], "n/a");
            connectDownloadRow["FileName"] = ConvertUtility.ConvertToFileName(gwDownloadRow["FilePath"], "n/a");
            connectDownloadRow["FileExt"] = ConvertUtility.ConvertToFileExtention(gwDownloadRow["FilePath"], "n/a");
            connectDownloadRow["IsJPDL"] = false;
            DateTime dtLastModDate = ConvertUtility.ConvertToDateTime(gwDownloadRow["ModifiedOnUTC"], DateTime.MinValue);
            if (dtLastModDate.AddDays(10) > DateTime.UtcNow) connectDownloadRow["ShowNewFlag"] = true;

        }

        internal static void InitConnectDownloadInfoFromJPDownload(ref DataRow connectDownloadRow
            , SqlDataReader jpDownloadRow, DataRow jpDownloadVersionRow
            , Int32 cultureGroupId, String modelProcessed)
        {
            if (connectDownloadRow == null || jpDownloadRow == null) return;
            int id = ConvertUtility.ConvertToInt32(jpDownloadRow["ID"], 0);
            connectDownloadRow["DownloadID"] = id;
            connectDownloadRow["CategoryID"] = 6; //software download is 6
            

            /*connectDownloadRow["Version"] = String.Empty;//"n/a";
            if (softTitleSections != null & softTitleSections.Length > 0)
            {
                String versionNumber = softTitleSections[0];
                if (softTitleSections[0].StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
                {
                    Regex verRgx = new Regex("([0-9]{2}$)", RegexOptions.IgnoreCase);
                    if (verRgx.IsMatch(versionNumber)) versionNumber = versionNumber.Insert(versionNumber.Length - 2, ".");

                    Regex verPrefixRgx = new Regex("^v[0-9]*.[0-9]*$", RegexOptions.IgnoreCase);
                    versionNumber = "Ver" + versionNumber.Substring(1);
                    connectDownloadRow["Version"] = versionNumber;
                }
            }

            if (string.IsNullOrEmpty(jpdlCategory))
            {
                //if (softTitleSections != null & softTitleSections.Length > 0) jpdlCategory = softTitle.Substring(softTitle.IndexOf(" "));
                //else jpdlCategory = softTitle;
                jpdlCategory = softTitle;
            }
            connectDownloadRow["CategoryEngName"] = jpdlCategory;
             * */

            #region " don't touch this "
            /*
             "Category" column in Japan donwload database IS "VERSION" in CONNECT
             "SoftTitle" column in Japan download database IS partially category and DOWNLOAD TYPE.
             */
            string jpdlCategory = jpDownloadRow["Category"].ToString();
            String softTitle = jpDownloadRow["SoftTitle"].ToString().ConvertNullToEmptyString();
            String[] softTitleSections = softTitle.Split(' ');

            #region " version = category "
            string versionText = ConvertUtility.ConvertNullToEmptyString( jpdlCategory );
            if (versionText.IsNullOrEmptyString() && true & softTitleSections.Length > 0)
            {
                String versionNumber = softTitleSections[0];
                if (softTitleSections[0].StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
                {
                    Regex verRgx = new Regex("([0-9]{2}$)", RegexOptions.IgnoreCase);
                    if (verRgx.IsMatch(versionNumber)) versionNumber = versionNumber.Insert(versionNumber.Length - 2, ".");

                    Regex verPrefixRgx = new Regex("^v[0-9]*.[0-9]*$", RegexOptions.IgnoreCase);
                    versionNumber = "Ver" + versionNumber.Substring(1);
                    versionText = versionNumber;
                }
            }
            connectDownloadRow["Version"] = versionText;
            #endregion

            #region " download type "
            connectDownloadRow["CategoryEngName"] = softTitle;

            //**Commenting out this block as Download type should be shown with v XXXX**
            //Changed On 1/24/2013 - Shaswati
            //if (softTitleSections != null & softTitleSections.Length > 0 
            //    && softTitleSections[0].StartsWith("v", StringComparison.InvariantCultureIgnoreCase)
            //    )
            //{
            //    String jpDownloadType = softTitle.Replace(softTitleSections[0], String.Empty);
            //    connectDownloadRow["CategoryEngName"] = jpDownloadType;
            //}
            #endregion

            #endregion

            connectDownloadRow["IsJPDL"] = true;
            connectDownloadRow["DownloadURL"] = GetJapanDownloadUrl(modelProcessed.Trim(), ConvertUtility.ConvertToFileName(jpDownloadRow["FileName"], ""));
            connectDownloadRow["DownloadTitle"] = jpDownloadRow["SoftName"].ToString();
            connectDownloadRow["ModelNumber"] = jpDownloadRow["ModelName"].ToString();


            if (jpDownloadRow["ReleaseDate"] != DBNull.Value) connectDownloadRow["ReleaseDateUTC"] = jpDownloadRow["ReleaseDate"];
            //connectDownloadRow["Version"] = ConvertUtility.ConvertToString(jpDownloadRow["SoftTitle"], "n/a");
            String jpFileSize = ConvertUtility.ConvertToString(jpDownloadRow["FileSize"], "n/a");
            if (jpFileSize != "n/a")
            {
                Double sizeInMb;
                Double.TryParse(jpFileSize.Replace(" ", "").Replace("MB", ""), out sizeInMb);
                if (sizeInMb > 0)
                {
                    connectDownloadRow["SizeKB"] = (sizeInMb * 1024.00);
                    connectDownloadRow["SizeMB"] = sizeInMb;
                }
            }

            //connectDownloadRow["SizeKB"] = ConvertUtility.ConvertToString(jpDownloadRow["FileSize"] + "MB", "n/a");
            connectDownloadRow["FileName"] = ConvertUtility.ConvertToFileName(jpDownloadRow["FileName"], "n/a");
            connectDownloadRow["FileExt"] = ConvertUtility.ConvertToFileExtention(jpDownloadRow["FileName"], "n/a");

            #region " additional information for JP downloads "
            connectDownloadRow["JPDL_Target"] = jpDownloadRow["Target"].ToString();
            connectDownloadRow["JPDL_UpDate"] = ConvertUtility.ConvertToDateTime(jpDownloadRow["UpDate"].ToString(), DateTime.MinValue);
            connectDownloadRow["JPDL_Category"] = jpDownloadRow["Category"].ToString();
            connectDownloadRow["JPDL_EncodeFlag"] = ConvertUtility.ConvertToInt32(jpDownloadRow["EncodeFlag"], 0);

            connectDownloadRow["JPDL_ModelName"] = modelProcessed.Trim();
            if (jpDownloadVersionRow != null)
            {
                if (cultureGroupId == 2) //JP
                {
                    connectDownloadRow["JPDL_VersionFileName"] = jpDownloadVersionRow["FileName"].ToString();
                    //connectDownloadRow["Version"] = rVersion["Message"].ToString();
                    //connectDownloadRow["JPDL_Comment"] = jpDownloadRow["Comment"].ToString();
                }
                else
                {
                    connectDownloadRow["JPDL_VersionFileName"] = jpDownloadVersionRow["FileNameEng"].ToString();
                    //connectDownloadRow["Version"] = rVersion["MessageEng"].ToString();
                    //connectDownloadRow["JPDL_Comment"] = jpDownloadRow["CommentEng"].ToString();
                }
            }

            if (cultureGroupId == 2) //JP
            {
                connectDownloadRow["JPDL_Comment"] = jpDownloadRow["Comment"].ToString();
            }
            else
            {
                connectDownloadRow["JPDL_Comment"] = jpDownloadRow["CommentEng"].ToString();
            }

            #endregion
            DateTime dtLastModDate = ConvertUtility.ConvertToDateTime(jpDownloadRow["UpDate"], DateTime.MinValue);
            if (dtLastModDate.AddDays(10) > DateTime.UtcNow) connectDownloadRow["ShowNewFlag"] = true;
            connectDownloadRow["JPDL_SoftTitle"] = jpDownloadRow["SoftTitle"].ToString();

        }

        public static DataTable GetJPVersionTable(List<String> modelTags)
        {
            if (modelTags == null || modelTags.Count < 1) return null;
            DBJpDl jpdldb = new DBJpDl();
            DataTable tb = null;
            using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
            {
                SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
                cmdForVersion.Parameters.AddWithValue("@ModelName", String.Empty);
                
                foreach (String mn in modelTags)
                {
                    cmdForVersion.Parameters["@ModelName"].Value = mn.ConvertNullToEmptyString();
                    DataTable tbNew = jpdldb.FillInDataTable(cmdForVersion);
                    if (tb == null) tb = tbNew;
                    else tb.Merge(tbNew);
                }
            }
            return tb;
        }

        public static DataTable SelectDownloadObjectUpdateInJPDB(String modelName, String modelTarget, DateTime supportExp
            , List<String> jpDownloadUserIDs)
        {
            String jpDownloadUserIdStr = String.Empty;
            if (jpDownloadUserIDs != null && jpDownloadUserIDs.Count > 0)
                jpDownloadUserIdStr = String.Join(";", jpDownloadUserIDs.ToArray());

            DBJpDl jpdldb = new DBJpDl();
            using (SqlConnection conn = jpdldb.GetOpenedConn())
            {
                SqlCommand cmd = jpdldb.MakeCmdSP(conn, null, DBJpDl.SQL_SP.Select_ObjectUpdateByModel);
                cmd.Parameters.AddWithValue("@ModelName", modelName);
                cmd.Parameters.AddWithValue("@CommaSeperatedJPDLUserIDs", jpDownloadUserIdStr);
                cmd.Parameters.AddWithValue("@ModelTarget", modelTarget);
                if (supportExp == DateTime.MinValue) cmd.Parameters.AddWithValue("@SupportExp", DBNull.Value);
                else cmd.Parameters.AddWithValue("@SupportExp", supportExp);
                return jpdldb.FillInDataTable(cmd);
            }
        }
        private static string CheckPIMMappedModelNumber(string modelNumber)
        {
            string cmsmodelnumber = modelNumber;
            try
            {
                var pimDb = new DBPIM();
                using (var conn = pimDb.GetOpenedConn())
                {
                    var cmd = pimDb.MakeCmdSP(conn, null, "uSP_CheckPIMMappedModelNumber");

                    cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                    cmd.Parameters.Add("@CMSModelNumber", SqlDbType.NVarChar,4000).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    // read output value from @NewId
                    cmsmodelnumber = cmd.Parameters["@CMSModelNumber"].Value.ToString();
                    conn.Close();
                }
            }
            catch
            {
                cmsmodelnumber = modelNumber;
            }
            return cmsmodelnumber;
        }
        public static DataTable GetDownloadInfoFromCMSDB( string ModelNumbers, int cultureGroupId)
        {
            var erpDb = new DBErp();
            using (var conn = erpDb.GetOpenedConn())
            {
                var cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.GetCMSDownloadInfo);
                cmd.Parameters.AddWithValue("@CultureCode", cultureGroupId);
                cmd.Parameters.AddWithValue("@ModelNumbers", CheckPIMMappedModelNumber(ModelNumbers));

                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
