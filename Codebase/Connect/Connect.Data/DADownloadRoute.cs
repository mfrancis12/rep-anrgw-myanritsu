﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DADownloadRoute
    {
        public static DataRow SelectByTokenID(Guid routeTokenId, Boolean updateAccessLog)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.GW_DownloadRoutingToken_SelectByTokenID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UpdateAccessLog", updateAccessLog);
                return SQLUtility.FillInDataRow(cmd);

            }
        }

        public static DataRow Insert(String downloadType,
            String fileName,
            String fileNameForUser,
            String downloadCategory,
            String modelNumber,
            String requestedUserEmail,
            String requestedUserName,
            String requestedUserSessionId,
            String requestedUserIp,
            Guid requestedAccountId,
            Guid requestedProdRegWak,
            Int32 encodeFlag)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.GW_DownloadRoutingToken_Insert);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@DownloadType", downloadType.Trim().ToUpperInvariant());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@FileName", fileName.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@FileNameForUser", fileNameForUser.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserEmail", requestedUserEmail.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserIP", requestedUserIp.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@EncodeFlag", encodeFlag);

                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserName", requestedUserName.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserSessionID", requestedUserSessionId.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@DownloadCategory", downloadCategory.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RequestedAccountID", requestedAccountId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RequestedProdRegWAK", requestedProdRegWak);
                return SQLUtility.FillInDataRow(cmd);

            }
        }

        public static Int32 UpdateTokenExp(Guid routeTokenId, DateTime newTokenExpUtc)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.GW_DownloadRoutingToken_UpdateTokenExp);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenId);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@TokenExp", newTokenExpUtc);
                return cmd.ExecuteNonQuery();

            }
        }

        public static Int32 UpdateForceExpire(Guid routeTokenId)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.GW_DownloadRoutingToken_UpdateForceExpire);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenId);
                return cmd.ExecuteNonQuery();

            }
        }
    }
}
