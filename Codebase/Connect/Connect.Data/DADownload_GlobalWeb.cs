﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using System.Configuration;
using System.Collections.Generic;
using ApiSdk;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
namespace Anritsu.Connect.Data
{
    public static class DADownload_GlobalWeb
    {
        private static String GetGwDownloadUrl(string dlUrl)
        {
            String baseUrl = ConfigUtility.GetStringProperty("Connect.GWDownloadBaseUrl", "http://www.anritsu.com");
            //if (dlUrl.StartsWith("/en-US"))
            //    dlUrl = dlUrl.Replace("/en-US/", "/");
            return (baseUrl + dlUrl).ToLower();
        }

        public static object Select(String modelNumber, Int32 gwCultureGroupId, bool includeRegistrationRequiredDwl = false)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            if (gwCultureGroupId < 1) gwCultureGroupId = 1;

            var apiUri =
                new Uri(string.Format("{0}{1}", ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.DownloadsUrl"], "GetDownLoads"));//?cultureGroupId=" + gwCultureGroupId.ToString() + "&modelNumber=MT8220T"));
            //get target db 
            var targetDb = ConfigUtility.AppSettingGetValue("Connect.Web.GWSiteCoreAPI.TargetDB");
            if (string.IsNullOrEmpty(targetDb)) targetDb = "web";
            var apiParams = new Dictionary<string, string>
            {
                {"cultureGroupId", gwCultureGroupId.ToString()},
                {"modelNumber", modelNumber},
                {"targetDB",targetDb},
                {"includeRegistrationRequiredDwl",includeRegistrationRequiredDwl.ToString()},
            };
            //apiParams.Add("modelNumber", "mt8220t");
            var rsClass = new ResponseClass();
            try
            {

                var response = rsClass.CallMethod<string>(apiUri, apiParams,false);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var parseResponse = Newtonsoft.Json.Linq.JObject.Parse(response.Content.ToString());
                    var serialize=new JavaScriptSerializer();
                    var downloads = serialize.Deserialize<List<Downloads_PublicData>>(parseResponse["ResponseContent"].ToString());
                    return downloads;
                }
                return null;
                //   return response.StatusCode == HttpStatusCode.OK ? Convert.ToString(response.Content) : string.Empty;

            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("CONNECT", HttpContext.Current.Request.Url.Host, ex, HttpContext.Current);
                return null;
            }
            
            //DBGlobalWeb gwdb = new DBGlobalWeb();
            //using (SqlConnection conn = gwdb.GetOpenedConn())
            //{
            //    SqlCommand cmd = gwdb.MakeCmdInline(conn, null, DBGlobalWeb.SQL_Inline.GWDownload_SelectByModel);
            //    cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
            //    cmd.Parameters.AddWithValue("@CultureGroupId", gwCultureGroupId);
            //    return SQLUtility.FillInDataTable(cmd);
            //}
        }
        
    }
    [Serializable]
    public class Downloads_PublicData
    {
        public Int32 DownloadID { get; set; }
        public Int32 CategoryID { get; set; }
        public String CategoryType { get; set; }
        public String Title { get; set; }
        public String ModelNumber { get; set; }
        public String FilePath { get; set; }
        public String DownloadPath { get; set; }
        public String Size { get; set; }
        public String Version { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public string RedirectToMyAnritsu { get; set; }
    }
}
