﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DADownload_JPNonUSB
    {
        private static String GetJapanDownloadUrl(String modelName, String fileName)
        {
            //String baseUrl = ConfigUtility.GetStringProperty("Connect.JPDownloadBaseUrl", "http://www.anritsu.com");
            //return baseUrl + "?id=" + id.ToString();
            return String.Format("/{0}/{1}", modelName, fileName);
        }

        public static DataTable Select(String modelNumber, Int32 gwCultureGroupId, List<String> userEmails)
        {
            DataTable tb = GetTableSchema();
            Boolean isJapanese = gwCultureGroupId == 2;
            String jpDownloadUserIdStr = String.Empty;
            if (userEmails != null && userEmails.Count > 0)
                jpDownloadUserIdStr = String.Join(";", userEmails.ToArray());

            DBJpDl jpdldb = new DBJpDl();
            using (SqlConnection conn = jpdldb.GetOpenedConn())
            {
                using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
                {
                    SqlCommand cmd = jpdldb.MakeCmdSP(conn, null, DBJpDl.SQL_SP.SelectObjectByModelv2);
                    cmd.Parameters.AddWithValue("@ModelName", modelNumber.Trim());
                    cmd.Parameters.AddWithValue("@JPDLUserEmails", jpDownloadUserIdStr);

                    SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
                    cmdForVersion.Parameters.AddWithValue("@ModelName", modelNumber.Trim());
                    DataRow rVersion = SQLUtility.FillInDataRow(cmdForVersion);

                    DataTable tbData = SQLUtility.FillInDataTable(cmd);
                    if (tbData == null) return null;
                    foreach (DataRow rowData in tbData.Rows)
                    {
                        DataRow newRow = tb.NewRow();
                        InitData(ref newRow, rowData, rVersion, isJapanese, modelNumber);
                        tb.Rows.Add(newRow);
                    }
                    tb.AcceptChanges();
                }
            }
            return tb;
        }

        internal static void InitData(ref DataRow newRow, DataRow japanDownloadRow, DataRow versionData, Boolean isJapanese, String requestedMn)
        {
            if (newRow == null || japanDownloadRow == null) return;
            newRow["DownloadID"] = ConvertUtility.ConvertToInt32(japanDownloadRow["ID"], 0);
            newRow["CategoryName"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["Category"]);
            newRow["DownloadURL"] = GetJapanDownloadUrl(requestedMn.Trim(), ConvertUtility.ConvertToFileName(japanDownloadRow["FileName"], ""));
            newRow["DownloadTitle"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["SoftName"]);
            newRow["ModelNumber"] = requestedMn;
            DateTime releasedDate = ConvertUtility.ConvertToDateTime(japanDownloadRow["ReleaseDate"], DateTime.MinValue);
            if (releasedDate != DateTime.MinValue) newRow["ReleaseDate"] = releasedDate;

            String jpFileSize = ConvertUtility.ConvertToString(japanDownloadRow["FileSize"], "n/a");
            if (jpFileSize != "n/a")
            {
                Double sizeInMb;
                Double.TryParse(jpFileSize.Replace(" ", "").Replace("MB", ""), out sizeInMb);
                if (sizeInMb > 0)
                {
                    newRow["SizeKB"] = (sizeInMb * 1024.00);
                    newRow["SizeMB"] = sizeInMb;
                }
            }

            newRow["FileName"] = ConvertUtility.ConvertToFileName(japanDownloadRow["FileName"], "n/a");
            newRow["FileExt"] = ConvertUtility.ConvertToFileExtention(japanDownloadRow["FileName"], "n/a");
            //newRow["ModelTarget"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["ModelT"]);
            newRow["DownloadTarget"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["Target"]);
            
            newRow["ShowNewFlag"] = false;
            DateTime updatedDate = ConvertUtility.ConvertToDateTime(japanDownloadRow["UpDate"], DateTime.MinValue);
            if (updatedDate != DateTime.MinValue)
            {
                newRow["UpDate"] = updatedDate;
                if (updatedDate.AddDays(10) > DateTime.UtcNow) newRow["ShowNewFlag"] = true;
            }

            newRow["ModelName"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["ModelName"]);
            if (ConvertUtility.ConvertToInt32(japanDownloadRow["EncodeFlag"], 0) == 1) newRow["EncodeFlag"] = true;
            else newRow["EncodeFlag"] = false;

            newRow["SoftTitle"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["SoftTitle"]);
            newRow["SoftName"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["SoftName"]);

            if (isJapanese)
            {
                if (versionData != null)
                {
                    newRow["VersionFileName"] = ConvertUtility.ConvertNullToEmptyString(versionData["FileName"]);
                }
                newRow["CommentText"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["Comment"]);
            }
            else
            {
                if (versionData != null)
                {
                    newRow["VersionFileName"] = ConvertUtility.ConvertNullToEmptyString(versionData["FileNameEng"]);
                }
                newRow["CommentText"] = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["CommentEng"]);
            }

            #region " don't touch this "
            /*
             "Category" column in Japan donwload database IS "VERSION" in CONNECT
             "SoftTitle" column in Japan download database IS partially category and DOWNLOAD TYPE.
             */
            string jpdlCategory = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["Category"]).Trim();
            String softTitle = ConvertUtility.ConvertNullToEmptyString(japanDownloadRow["SoftTitle"]).Trim();
            String[] softTitleSections = softTitle.Split(' ');
            
            #region " version = category "
            string versionText = ConvertUtility.ConvertNullToEmptyString(jpdlCategory);
            if (versionText.IsNullOrEmptyString() && true & softTitleSections.Length > 0)
            {
                String versionNumber = softTitleSections[0];
                if (softTitleSections[0].StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
                {
                    Regex verRgx = new Regex("([0-9]{2}$)", RegexOptions.IgnoreCase);
                    if (verRgx.IsMatch(versionNumber)) versionNumber = versionNumber.Insert(versionNumber.Length - 2, ".");

                    Regex verPrefixRgx = new Regex("^v[0-9]*.[0-9]*$", RegexOptions.IgnoreCase);
                    versionNumber = "Ver " + versionNumber.Substring(1);
                    versionText = versionNumber;
                }
            }
            if (versionText.IsNullOrEmptyString()) versionText = "Ver 0.xx";
            newRow["Version"] = versionText;
            if (softTitle.StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
            {
                newRow["DownloadType"] = softTitle.Substring(softTitle.IndexOf(' ')).Trim();
            }
            else
                newRow["DownloadType"] = softTitle;
            #endregion

            #endregion
        }

        internal static DataTable GetTableSchema()
        {
            DataTable tb = new DataTable("JapanNonUSB");
            DataColumn col;

            col = new DataColumn("DownloadID", SqlDbType.Int.GetType());
            tb.Columns.Add(col);

            col = new DataColumn("CategoryName", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("DownloadType", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("DownloadURL", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("DownloadTitle", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("ModelNumber", typeof(String));
            //col.MaxLength = 4000;
            tb.Columns.Add(col);

            col = new DataColumn("ReleaseDate", typeof(DateTime));
            tb.Columns.Add(col);

            col = new DataColumn("Version", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("SizeKB", typeof(double));
            tb.Columns.Add(col);

            col = new DataColumn("SizeMB", typeof(double));
            tb.Columns.Add(col);

            col = new DataColumn("FileName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("FileExt", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("ModelTarget", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("DownloadTarget", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("UpDate", typeof(DateTime));
            tb.Columns.Add(col);

            col = new DataColumn("ModelName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("VersionFileName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("EncodeFlag", typeof(Boolean));
            tb.Columns.Add(col);

            col = new DataColumn("ShowNewFlag", typeof(Boolean));
            col.DefaultValue = false;
            tb.Columns.Add(col);

            col = new DataColumn("CommentText", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("SoftTitle", typeof(String));
            col.DefaultValue = "n/a";
            tb.Columns.Add(col);

            col = new DataColumn("SoftName", typeof(String));
            tb.Columns.Add(col);

            col = new DataColumn("JPUSB_Required", typeof(Boolean));
            col.DefaultValue = false;
            tb.Columns.Add(col);

            //col = new DataColumn("JPUSB_USBNoToCheck", typeof(String));
            //col.DefaultValue = String.Empty;
            //tb.Columns.Add(col);

            tb.AcceptChanges();
            return tb;
        }
    }
}
