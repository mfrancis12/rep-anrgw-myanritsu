﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DADownload_JPUSB
    {
        public static DataTable Select(String modelNumber, Int32 gwCultureGroupId, String userEmail, String usbNo)
        {
            if (modelNumber.IsNullOrEmptyString() || userEmail.IsNullOrEmptyString() || usbNo.IsNullOrEmptyString()) return null;
            DataTable tb = DADownload_JPNonUSB.GetTableSchema(); //same schema
            Boolean isJapanese = gwCultureGroupId == 2;

            DBJpDl jpdldb = new DBJpDl();
            using (SqlConnection conn = jpdldb.GetOpenedConn())
            {
                using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
                {
                    SqlCommand cmd = jpdldb.MakeCmdSP(conn, null, DBJpDl.SQL_SP.Dongle_SelectObjectByDonglev2);
                    SQLUtility.AddParam(ref cmd, "@ModelName", modelNumber);
                    SQLUtility.AddParam(ref cmd, "@USBNo", usbNo);
                    SQLUtility.AddParam(ref cmd, "@UserEmail", userEmail);                   

                    SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
                    SQLUtility.AddParam(ref cmdForVersion, "@ModelName", modelNumber);
                    DataRow rVersion = SQLUtility.FillInDataRow(cmdForVersion);

                    DataTable tbData = SQLUtility.FillInDataTable(cmd);
                    if (tbData == null) return null;
                    foreach (DataRow rowData in tbData.Rows)
                    {
                        DataRow newRow = tb.NewRow();
                        DADownload_JPNonUSB.InitData(ref newRow, rowData, rVersion, isJapanese, modelNumber);
                        newRow["JPUSB_Required"] = true;
                        //newRow["JPUSB_USBNoToCheck"] = usbNo.Trim();
                        tb.Rows.Add(newRow);
                    }
                    tb.AcceptChanges();
                }
            }
            return tb;
        }
    }
}
