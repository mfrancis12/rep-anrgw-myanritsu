﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DADownload_JPVersionTable
    {
        public static DataTable GetVersionTable(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBJpDl jpdldb = new DBJpDl();
            using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
            {
                SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
                cmdForVersion.Parameters.AddWithValue("@ModelName", modelNumber.ConvertNullToEmptyString());
                return jpdldb.FillInDataTable(cmdForVersion);                
            }
        }
    }
}
