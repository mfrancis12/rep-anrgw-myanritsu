﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAErpData
    {
        public static DataTable GSNDB_SelectByModelSerial(String modelNumber, String serialNumber)
        {
            //old SelectInfoInGlobalSerialNumberSystemErpDB
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindModelSerialNumberInGlobalSerialNumberSystem);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable GSNDB_Search(String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() && serialNumber.IsNullOrEmptyString()) return null;
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.GlobalSN_ManufaSrc_Search);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.ConvertNullToEmptyString());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", serialNumber.ConvertNullToEmptyString());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable GlobalWebWarrantyDB_SelectByModelSerial(String modelNumber, String serialNumber)
        {
            //old SelectInfoInGlobalSerialNumberSystemErpDB
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindProductByModelSerialNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }
        public static DataTable GSNDB_SearchSNByMN(String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() && serialNumber.IsNullOrEmptyString()) return null;
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.GlobalSN_ManufaSrc_SearchByMN);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.ConvertNullToEmptyString());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SerialNumber", serialNumber.ConvertNullToEmptyString());
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
