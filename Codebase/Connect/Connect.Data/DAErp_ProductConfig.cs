﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAErp_ProductConfig
    {
        public static DataRow SelectByModelNumber(String modelNumber, out DataTable tbModelTags)
        {
            tbModelTags = null;
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_SelectByModelNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                DataRow r = db.FillInDataRow(cmd);
                if (r != null)
                {
                    tbModelTags = DAErp_ProductConfig_ModelTag.SelectByModelNumber(modelNumber, conn, null);
                }
                return r;
            }
        }
                 
        //public static DataTable SelectWithFilter(String modelNumber, String productOwnerRegion)
        //{
        //    //DBConnect db = new DBConnect();
        //    //using (SqlConnection conn = db.GetOpenedConn())
        //    //{
        //    //    SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_SelectWithFilter);
        //    //    SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
        //    //    SQLUtility.SqlParam_AddAsString(ref cmd, "@ProductOwnerRegion", productOwnerRegion.Trim());
        //    //    cmd.Parameters.AddWithValue("@IsPaidSupportModel", DBNull.Value);
        //    //    return db.FillInDataTable(cmd);
        //    //}
        //    return SelectWithFilter(modelNumber, productOwnerRegion, null, null);
        //}

        public static DataTable SelectWithFilter(String modelNumber, String productOwnerRegion
            , Boolean? isPaidSupportModel, Boolean? isActive)
        {
            //if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_SelectWithFilter);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProductOwnerRegion", productOwnerRegion.Trim());
                //SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsPaidSupportModel", isPaidSupportModel);
                if (isPaidSupportModel.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsPaidSupportModel", (Boolean)isPaidSupportModel);
                else cmd.Parameters.AddWithValue("@IsPaidSupportModel", DBNull.Value);

                if (isActive.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsActive", (Boolean)isActive);
                else cmd.Parameters.AddWithValue("@IsActive", DBNull.Value);
                return db.FillInDataTable(cmd);
            }
        }

        public static void InsertUpdate(String modelNumber
            , String productOwnerRegion
            , Boolean isPaidSupportModel
            , Boolean verifyCompany
            , Boolean verifyProductSN
            , Boolean userACLRequired
            , String imageUrl
            , Guid addOnFormID
            , Boolean addOnFormPerSN
            , Guid feedbackFormID
            , Boolean skipSerialNumberValidation
            , Boolean isVisibleToCustomers
            )
        {
            if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
            if (productOwnerRegion.IsNullOrEmptyString()) throw new ArgumentNullException("productOwnerRegion");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_InsertUpdate);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ProductOwnerRegion", productOwnerRegion);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsPaidSupportModel", isPaidSupportModel);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyCompany", verifyCompany);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@VerifyProductSN", verifyProductSN);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@UserACLRequired", userACLRequired);               
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ImageUrl", imageUrl);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AddOnFormID", addOnFormID);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AddOnFormPerSN", addOnFormPerSN);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@FeedbackFormID", feedbackFormID);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@SNValidationNotRequired", skipSerialNumberValidation);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsVisibleToCustomers", isVisibleToCustomers);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_Delete);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                cmd.ExecuteNonQuery();
            }

        }

        public static void DeleteModelNumber(String modelNumber)
        {
            SqlTransaction tran;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    Data.DAErp_ProductConfig_ModelTag.DeleteAll(modelNumber);
                    Data.DAErp_ProductConfig.Delete(modelNumber);
                       
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }
    }
}
