﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAErp_ProductConfig_ModelTag
    {
        public static DataTable SelectByModelNumber(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByModelNumber(modelNumber.Trim(), conn, null);
            }
        }

        public static DataTable SelectByModelNumber(String modelNumber, SqlConnection conn, SqlTransaction tran)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, tran, db.SQLSP_Erp_ProductConfig_ModelTag_SelectByModelNumber);
            cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
            return db.FillInDataTable(cmd);
        }

        public static DataRow Insert(String modelNumber, String tagModelNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || tagModelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_ModelTag_Insert);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@TagModelNumber", tagModelNumber.Trim());
                return db.FillInDataRow(cmd);
            }
        }

        public static Int32 Delete(Int32 modelTagID)
        {
            if (modelTagID < 1) return 0;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_ModelTag_Delete);
                cmd.Parameters.AddWithValue("@ModelTagID", modelTagID);
                return cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteAll(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductConfig_ModelTag_DeleteByModelNumber);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ModelNumber", modelNumber.Trim());
                cmd.ExecuteNonQuery();
            }

        }
        

    }
}
