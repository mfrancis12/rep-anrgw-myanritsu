﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAErp_ProductOwnerRegion
    {
        public static DataRow SelectByRegionCode(String regionCode)
        {
            if (regionCode.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductOwnerRegion_Select);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RegionCode", regionCode.ToUpperInvariant());
                return db.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, db.SQLSP_Erp_ProductOwnerRegion_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
