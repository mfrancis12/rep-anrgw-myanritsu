﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAFrm_Form
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow SelectByFormID(Guid formId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_SelectByFormID);
                cmd.Parameters.AddWithValue("@FormID", formId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataTable SelectBy_FormID(Guid formId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_SelectByFormID);
                cmd.Parameters.AddWithValue("@FormID", formId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectModelsByFormID(Guid formId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_SelectModelsByFormID);
                cmd.Parameters.AddWithValue("@FormID", formId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByModuleID(Int32 moduleId, Boolean createIfNew)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_SelectByModuleID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@CreateIfNotExist", createIfNew);
                return SQLUtility.FillInDataRow(cmd);
            }
            
        }

        public static DataRow SelectByModuleID(Int32 fwscId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return SelectModuleIDFieldsetIDByFwscID(fwscId, conn, null);
            }

        }

        public static DataRow SelectModuleIDFieldsetIDByFwscID(Int32 fwscId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.DynamicForm.Form_SelectModuleIDFieldsetIDByFwscID);
            cmd.Parameters.AddWithValue("@FwscID", fwscId);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static String ResClassKey(Guid formId)
        {
            if (formId.IsNullOrEmptyGuid()) return String.Empty;
            return String.Format("FRM_{0}", formId.ToString().ToUpperInvariant());
        }

        public static void Update(Guid formId, String formName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_Update);
                cmd.Parameters.AddWithValue("@FormID", formId);
                cmd.Parameters.AddWithValue("@FormName", formName.Trim());
                cmd.ExecuteNonQuery();
            }

        }

        public static DataRow Insert(String formName, String formType)
        {
            if (formName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_Insert);
                cmd.Parameters.AddWithValue("@FormName", formName.Trim());
                cmd.Parameters.AddWithValue("@FormType", formType.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static void Delete(Guid formId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_Delete);
                cmd.Parameters.AddWithValue("@FormID", formId);
                cmd.ExecuteNonQuery();
            }

        }
    }
}
