﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormControl
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_Control_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow SelectByControlID(Int32 controlId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.Form_Control_SelectByControlID);
                cmd.Parameters.AddWithValue("@ControlID", controlId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }
    }
}
