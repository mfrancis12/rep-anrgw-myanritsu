﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormControlDataSource
    {
        public static String ResClassKey(Int32 dataSourceId)
        {
            return String.Format("DataSource_{0}", dataSourceId);
        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSource_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow SelectByDataSourceID(Int32 dataSourceId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSource_SelectByDataSourceID);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataRow Insert(String dataSourceName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSource_Insert);
                cmd.Parameters.AddWithValue("@DataSourceName", dataSourceName.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static void Update(Int32 dataSourceId, String dataSourceName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSource_Update);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                cmd.Parameters.AddWithValue("@DataSourceName", dataSourceName.Trim());
                cmd.ExecuteNonQuery();
            }

        }

        public static Int32 DeleteByDataSourceID(Int32 dataSourceId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSource_DeleteByDataSourceID);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                return cmd.ExecuteNonQuery();
            }

        }
    }
}
