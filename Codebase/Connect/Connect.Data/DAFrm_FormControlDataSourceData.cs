﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormControlDataSourceData
    {

        public static String ResKeyItemText(Int32 dsDataId)
        {
            return String.Format("DSDaataID_{0}_ItemText", dsDataId);
        }

        public static String ResKeyItemValue(Int32 dsDataId)
        {
            return String.Format("DSDaataID_{0}_ItemValue", dsDataId);
        }

        public static DataRow SelectByDSDataID(Int32 dsDataId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSourceData_SelectByDSDataID);
                cmd.Parameters.AddWithValue("@DSDataID", dsDataId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataTable SelectByDataSourceID(Int32 dataSourceId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSourceData_SelectByDataSourceID);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow Insert(Int32 dataSourceId, Boolean isSelected, Int32 orderNum)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSourceData_Insert);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsSelected", isSelected);
                cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static void Update(Int32 dsDataId, Boolean isSelected, Int32 orderNum)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.ControlDataSource.FormControlDataSourceData_Update);
                cmd.Parameters.AddWithValue("@DSDataID", dsDataId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsSelected", isSelected);
                cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                cmd.ExecuteNonQuery();
            }

        }

        public static Int32 DeleteByDSDataID(Int32 dsDataId, Int32 dataSourceId)
        {
            SqlTransaction tran = null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.ControlDataSource.FormControlDataSourceData_DeleteByDSDataID);
                    cmd.Parameters.AddWithValue("@DSDataID", dsDataId);
                    Int32 affectedRows = cmd.ExecuteNonQuery();

                    String classKey = DAFrm_FormControlDataSource.ResClassKey(dataSourceId);
                    DARes_ContentStore.DeleteByClassKeyResKey(classKey, ResKeyItemText(dsDataId), conn, tran);
                    DARes_ContentStore.DeleteByClassKeyResKey(classKey, ResKeyItemValue(dsDataId), conn, tran);
                    tran.Commit();
                    return affectedRows;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }

        }
    }
}
