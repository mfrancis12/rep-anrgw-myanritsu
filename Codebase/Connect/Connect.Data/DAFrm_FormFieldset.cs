﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormFieldset
    {
        public static DataTable SelectByModuleID(Int32 moduleId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_SelectByModuleID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectByFormID(Guid formId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_SelectByFormID);
                cmd.Parameters.AddWithValue("@FormID", formId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow SelectByFieldsetID(Int32 fieldsetId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_SelectByFieldsetID);
                cmd.Parameters.AddWithValue("@FieldsetID", fieldsetId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static Int32 Insert(Guid formId, Int32 orderNum, Boolean isInternal, String legendInEnglish)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_Insert);
                cmd.Parameters.AddWithValue("@FormID", formId);
                cmd.Parameters.AddWithValue("@ResClassKey", DAFrm_Form.ResClassKey(formId));
                cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsInternal", isInternal);
                cmd.Parameters.AddWithValue("@LegendInEng", legendInEnglish.Trim());
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["FieldsetID"], 0);
            }

        }

        public static void Update(Int32 fieldsetId, Int32 orderNum, Boolean isInternal)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_Update);
                cmd.Parameters.AddWithValue("@FieldsetID", fieldsetId);
                cmd.Parameters.AddWithValue("@OrderNum", orderNum);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsInternal", isInternal);
                cmd.ExecuteNonQuery();
            }

        }

        public static Int32 DeleteByFieldsetID(Int32 fieldsetId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldset_DeleteByFieldsetID);
                cmd.Parameters.AddWithValue("@FieldsetID", fieldsetId);
                return cmd.ExecuteNonQuery();
            }

        }

        public static String GetResourceResKeyPrefix(Int32 fieldsetId)
        {
            return String.Format("FSRes_{0}_", fieldsetId);
        }
    }
}
