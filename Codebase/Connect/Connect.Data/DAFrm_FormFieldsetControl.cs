﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormFieldsetControl
    {
        public static DataRow SelectByFwscID(Int32 fwscId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_SelectByFwscID);
                cmd.Parameters.AddWithValue("@FwscID", fwscId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataRow SelectByControlID(Int32 controlId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_SelectByControlID);
                cmd.Parameters.AddWithValue("@ControlID", controlId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataTable SelectByFieldsetID(Int32 fieldsetId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_SelectByFieldsetID);
                cmd.Parameters.AddWithValue("@FieldsetID", fieldsetId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static Int32 Insert(Int32 fieldsetId, Int32 controlId, Int32 controlOrder, Boolean isRequired, String fieldName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_Insert);
                cmd.Parameters.AddWithValue("@FieldsetID", fieldsetId);
                cmd.Parameters.AddWithValue("@ControlID", controlId);
                cmd.Parameters.AddWithValue("@ControlOrder", controlOrder);
                cmd.Parameters.AddWithValue("@FieldName", fieldName);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsRequired", isRequired);
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["FwscID"], 0);
            }

        }

        public static void Update(Int32 fwscId, Int32 controlOrder, Boolean isRequired, Int32 dataSourceId, String fieldName)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_Update);
                cmd.Parameters.AddWithValue("@FwscID", fwscId);
                cmd.Parameters.AddWithValue("@ControlOrder", controlOrder);
                cmd.Parameters.AddWithValue("@DataSourceID", dataSourceId);
                cmd.Parameters.AddWithValue("@FieldName", fieldName);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsRequired", isRequired);
                cmd.ExecuteNonQuery();
            }

        }

        public static String GenerateResourceKey(Int32 fieldsetId, Int32 fwscId, String control)
        {
            return String.Format("{0}_FwscID_{1}_{2}", DAFrm_FormFieldset.GetResourceResKeyPrefix(fieldsetId), fwscId, control);
        }

        public static Int32 Delete(Int32 fwscId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControl_Delete);
                cmd.Parameters.AddWithValue("@FwscID", fwscId);
                return cmd.ExecuteNonQuery();                
                
            }

        }
    }
}
