﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormFieldsetControlProperty
    {
        public static DataTable SelectByFwscID(Int32 fwscID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControlProperty_Select);
                cmd.Parameters.AddWithValue("@FwscID", fwscID);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static void InsertUpdate(Int32 fwscId, String propertyKey, String propertyValue)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControlProperty_InsertUpdate);
                cmd.Parameters.AddWithValue("@FwscID", fwscId);
                cmd.Parameters.AddWithValue("@PropertyKey", propertyKey.Trim());
                cmd.Parameters.AddWithValue("@PropertyValue", propertyValue.Trim());
                cmd.ExecuteNonQuery();
            }

        }

        public static Int32 DeleteByPropertyID(Int32 propertyID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.DynamicForm.FormFieldsetControlProperty_Select);
                cmd.Parameters.AddWithValue("@PropertyID", propertyID);
                return cmd.ExecuteNonQuery();
            }

        }
    }
}
