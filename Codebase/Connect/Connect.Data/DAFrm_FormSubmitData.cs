﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormSubmitData
    {
        public static DataRow Insert(Int32 formSubmitId
            , String fieldInternalName
            , String submittedValueStr
            , SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.DynamicFormSubmit.FormSubmitData_InsertStr);
            cmd.Parameters.AddWithValue("@FormSubmitID", formSubmitId);
            //if(fwscID < 1) cmd.Parameters.AddWithValue("@FwscID", DBNull.Value);
            //else cmd.Parameters.AddWithValue("@FwscID", fwscID);
            cmd.Parameters.AddWithValue("@FieldInternalName", fieldInternalName.Trim());
            cmd.Parameters.AddWithValue("@SubmittedValueStr", submittedValueStr.Trim());
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataRow Insert(Int32 formSubmitId
            , String fieldInternalName
            , String submittedValueStr)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return Insert(formSubmitId, fieldInternalName, submittedValueStr, conn, null);
            }
        }
    }
}
