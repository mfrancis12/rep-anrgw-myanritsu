﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Data
{
    public static class DAFrm_FormSubmitMaster
    {
        public static DataRow Insert(Guid formId
            , String formInternalName
            , String submittedUserEmail
            , Guid submittedAccountId
            , SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.DynamicFormSubmit.FormSubmitMaster_Insert);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@FormID", formId);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@FormInternalName", formInternalName.Trim());
            SQLUtility.SqlParam_AddAsString(ref cmd, "@SubmittedUserEmail", submittedUserEmail.Trim());
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@SubmittedAccountID", submittedAccountId);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataRow Insert(Guid formId
            , String formInternalName
            , String submittedUserEmail
            , Guid submittedAccountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return Insert(formId, formInternalName, submittedUserEmail, submittedAccountId, conn, null);
            }
        }

        public static DataRow Insert(Guid formId
            , String formInternalName
            , String submittedUserEmail
            , Guid submittedAccountId
            , Dictionary<String, String> keyValues)
        {
            if(formId.IsNullOrEmptyGuid()) throw new ArgumentNullException("formId");
            if (submittedUserEmail.IsNullOrEmptyString()) throw new ArgumentNullException("submittedUserEmail");
            if (keyValues == null || keyValues.Count < 1) throw new ArgumentNullException("keyValues");

            SqlTransaction tran = null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                try
                {
                    tran = conn.BeginTransaction();
                    DataRow rFrm = Insert(formId, formInternalName, submittedUserEmail, submittedAccountId, conn, tran);
                    if (rFrm == null) throw new ArgumentException("Unable to save data.");
                    Int32 formSubmitId = ConvertUtility.ConvertToInt32(rFrm["FormSubmitID"], 0);
                    ConvertUtility.ConvertToGuid(rFrm["FormSubmitKey"], Guid.Empty);

                    foreach (String key in keyValues.Keys)
                    {
                        DAFrm_FormSubmitData.Insert(formSubmitId, key.Trim(), keyValues[key].Trim(), conn, tran);
                    }

                    tran.Commit();
                    return rFrm;
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }
    }
}
