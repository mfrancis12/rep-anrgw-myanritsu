﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DAGlobalWeb_CalCert
    {
        public static DataTable SearchCalCert(String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DBGlobalWeb db = new DBGlobalWeb();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGlobalWeb.SQL_SP.CalCert.Calibration_Certificates_Search_Get);
                SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.AddParam(ref cmd, "@SerialNumber", serialNumber);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow GetCalibrationCerttificateFile(string gwUserId, string modelNumber, string serialNumber)
        {
            String userIp = WebUtility.GetUserIP();
            DBGlobalWeb db = new DBGlobalWeb();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGlobalWeb.SQL_SP.CalCert.Calibration_Certificate_Download);
                SQLUtility.AddParam(ref cmd, "@Model_Number", modelNumber);
                SQLUtility.AddParam(ref cmd, "@Serial_Number", serialNumber);
                SQLUtility.AddParam(ref cmd, "@UserId", gwUserId);
                SQLUtility.AddParam(ref cmd, "@IPAddress", userIp);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
    }
}
