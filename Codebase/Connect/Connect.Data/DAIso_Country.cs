﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAIso_Country
    {
        public static DataTable SelectAll()
        {
            DBIso db = new DBIso();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdInline(conn, null, DBIso.SQL_Inline.ISO_Country_Select);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectCountryWithGlobalWebRegion()
        {
            DBIso db = new DBIso();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdInline(conn, null, DBIso.SQL_Inline.ISO_CountryCultureGroup);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static Int32 GetCultureGroupIDByCultureCode(String llCC)
        {
            if (llCC.IsNullOrEmptyString()) return 1;
            DBIso db = new DBIso();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBIso.SQL_SP.GetCultureGroupID);
                cmd.Parameters.AddWithValue("@CultureCode", llCC.Trim());
                DataRow r = SQLUtility.FillInDataRow(cmd);

                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["CultureGroupId"], 1);
            }
        }

        public static string GetDefaultCultureCode(string cultureCode)
        {
            if (cultureCode.IsNullOrEmptyString()) return "en-US";
            DBIso db = new DBIso();
            using (var conn = db.GetOpenedConn())
            {
                var cmd = db.MakeCmdSP(conn, null, DBIso.SQL_SP.CultureGroups_GetGroupCode);
                cmd.Parameters.AddWithValue("@CultureCode", cultureCode);
                var rootDir = cmd.ExecuteScalar().ToString();
                return rootDir;
            }
        }

        public static DataRow GetISOCountryInfoByCountryCode(String alpha2)
        {
            if (alpha2.IsNullOrEmptyString()) return null;
            DBIso db = new DBIso();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBIso.SQL_SP.AR_ISO_Country_Select_ByAlpha2Code);
                cmd.Parameters.AddWithValue("@Alpha2Code", alpha2.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }
    }
}
