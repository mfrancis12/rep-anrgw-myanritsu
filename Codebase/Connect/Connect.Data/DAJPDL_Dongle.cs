﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAJPDL_Dongle
    {
        //public static DataRow CheckDongle(String usbNo, String email)
        //{
        //    DataTable tb = FindDongle(usbNo.ConvertNullToEmptyString().Trim(), true);
        //    if(tb == null || tb.Rows.Count != 1) 
        //    //DBConnect db = new DBConnect();
        //    //using (SqlConnection conn = db.GetOpenedConn())
        //    //{
        //    //    SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_JPDongle_Validate);
        //    //    cmd.Parameters.AddWithValue("@USBNo", usbNo.ConvertNullToEmptyString().Trim());
        //    //    cmd.Parameters.AddWithValue("@UserEmail", email.ConvertNullToEmptyString().Trim());
        //    //    return db.FillInDataRow(cmd);
        //    //}
        //}

        public static DataTable SelectByModel(Dictionary<String, String> models,
           List<String> jpDownloadUserIDs,
           int gwCultureGroupId,
           Boolean includeDlSrcJp
           )
        {
            #region " To Be deleted "
            return null;
            //if (models == null || models.Count < 1 || !includeDLSrcJP) return null;
            //DataTable tb = DADownload.GetDownloadTableSchema();

            //String jpDownloadUserIDStr = String.Empty;
            //if (jpDownloadUserIDs != null && jpDownloadUserIDs.Count > 0)
            //    jpDownloadUserIDStr = String.Join(";", jpDownloadUserIDs.ToArray());

            //DBJpDl jpdldb = new DBJpDl();
            //using (SqlConnection conn = jpdldb.GetOpenedConn())
            //{
            //    using (SqlConnection connForVersion = jpdldb.GetOpenedConn())
            //    {
            //        SqlCommand cmd = jpdldb.MakeCmdSP(conn, null, DBJpDl.SQL_SP.Dongle_SelectObjectByDongle);
            //        cmd.Parameters.AddWithValue("@ModelName", String.Empty);
            //        cmd.Parameters.AddWithValue("@ModelTarget", String.Empty);
            //        cmd.Parameters.AddWithValue("@CommaSeperatedJPDLUserIDs", jpDownloadUserIDStr);

            //        SqlCommand cmdForVersion = jpdldb.MakeCmdSP(connForVersion, null, DBJpDl.SQL_SP.SelectVersionByModel);
            //        cmdForVersion.Parameters.AddWithValue("@ModelName", String.Empty);

            //        foreach (String mn in models.Keys)
            //        {
            //            if (String.IsNullOrEmpty(mn)) continue;
            //            String modelTarget = models[mn];

            //            cmd.Parameters["@ModelName"].Value = mn.Trim();
            //            cmd.Parameters["@ModelTarget"].Value = modelTarget.Trim();
            //            SqlDataReader r = cmd.ExecuteReader();

            //            cmdForVersion.Parameters["@ModelName"].Value = mn.Trim();
            //            DataRow rVersion = SQLUtility.FillInDataRow(cmdForVersion);
            //            while (r.Read())
            //            {
            //                DataRow rGwDL = tb.NewRow();
            //                DADownload.InitConnectDownloadInfoFromJPDownload(ref rGwDL, r, rVersion, gwCultureGroupID, mn);
            //                tb.Rows.Add(rGwDL);
            //            }
            //            r.Close();
            //            tb.AcceptChanges();

            //        }
            //    }
            //}
            //return tb;
            #endregion
        }

        public static DataTable FindDongle(String keyword, Boolean exactMatchUsbNo)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_JPDongle_Find);
                cmd.Parameters.AddWithValue("@Keyword", keyword.ConvertNullToEmptyString().Trim());
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ExactMatchUSBNo", exactMatchUsbNo);
                return db.FillInDataTable(cmd);
            }
        }

        public static void Insert(String usbKey, String usbEmail, String adminEmail)
        {
            if (usbKey.IsNullOrEmptyString() || usbEmail.IsNullOrEmptyString() ) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_JPDongle_Insert);
                cmd.Parameters.AddWithValue("@USBNo", usbKey.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@USBEmail", usbEmail.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@CreatedBy", adminEmail.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static void Update(String usbKey, String usbStatusCode)
        {
            if (usbKey.IsNullOrEmptyString() || usbStatusCode.IsNullOrEmptyString()) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_JPDongle_Update);
                cmd.Parameters.AddWithValue("@USBNo", usbKey.ConvertNullToEmptyString().Trim());
                cmd.Parameters.AddWithValue("@USBKeyStatus", usbStatusCode.ConvertNullToEmptyString().Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow Delete(String usbKey)
        {
            if (usbKey.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_JPDongle_Delete);
                cmd.Parameters.AddWithValue("@USBNo", usbKey.ConvertNullToEmptyString().Trim());
                return db.FillInDataRow(cmd);
            }
        }
    }
}
