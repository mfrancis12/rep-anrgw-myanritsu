﻿using System.Data;
using System.Data.SqlClient;

namespace Anritsu.Connect.Data
{
    public static class DAJPDL_Login
    {
        public static DataTable SelectAll()
        {
            DBJpDl db = new DBJpDl();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBJpDl.SQL_SP.Login_SelectAll);
                return db.FillInDataTable(cmd);
            }
        }
    }
}
