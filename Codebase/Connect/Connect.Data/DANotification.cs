﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DANotification
    {
        public static DataTable SelectNotificationDisplayAreas()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.NotificationDisplayAreas_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Insert(String notificationName, int displayAreaId, String internalComments,String createdBy, Boolean isActive)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_Insert);
                cmd.Parameters.AddWithValue("@NotificationName", notificationName.Trim());
                cmd.Parameters.AddWithValue("@DisplayAreaId", displayAreaId);
                cmd.Parameters.AddWithValue("@InternalComments", internalComments.Trim());
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy.Trim());
                cmd.Parameters.AddWithValue("@IsActive", isActive);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByNotificationID(int notificationId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_SelectByNotificationID);
                cmd.Parameters.AddWithValue("@NotificationID", notificationId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Delete(int notificationId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_DeleteByNotificationID);
                cmd.Parameters.AddWithValue("@NotificationID", notificationId);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void Update(int notificationId, String name, String internalComments, int displayAreaId, String modifiedBy, Boolean isActive)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_Update);
                cmd.Parameters.AddWithValue("@NotificationID", notificationId);
                cmd.Parameters.AddWithValue("@Name", name.Trim());
                cmd.Parameters.AddWithValue("@DisplayAreaID", displayAreaId);
                cmd.Parameters.AddWithValue("@InternalComments", internalComments.Trim());
                cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                cmd.Parameters.AddWithValue("@IsActive", isActive);
                cmd.ExecuteNonQuery();
            }
        }
        public static DataTable SelectActiveOrDefaultNotification()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Notification.Notification_SelectActiveOrDefault);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
