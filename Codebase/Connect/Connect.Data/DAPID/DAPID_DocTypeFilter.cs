﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_DocTypeFilter
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByDocTypeFilterID(Int32 docTypeFilterId)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.SelectByFilterID);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByDocTypeFilterID(Int32 docTypeFilterId, out DataTable tbFilterItems)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            tbFilterItems = null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.SelectByFilterID);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                DataRow row = SQLUtility.FillInDataRow(cmd);
                if (row != null)
                {
                    tbFilterItems = DAPID_DocTypeFilterItem.SelectByDocTypeFilterID(docTypeFilterId, conn);
                }
                return row;
            }
            
        }

        public static DataRow Insert(String docTypeFilterName, String docTypeFilterOwnerRole, String createdBy)
        {
            if (docTypeFilterName.IsNullOrEmptyString()) throw new ArgumentNullException("docTypeFilterName");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.Insert);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterName", docTypeFilterName);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterOwnerRole", docTypeFilterOwnerRole);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Update(Int32 docTypeFilterId, String docTypeFilterName)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            if (docTypeFilterName.IsNullOrEmptyString()) throw new ArgumentNullException("docTypeFilterName");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.Update);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterName", docTypeFilterName);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Int32 docTypeFilterId)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilter.Delete);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
