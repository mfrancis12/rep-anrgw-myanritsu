﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_DocTypeFilterItem
    {
        public static DataTable SelectByDocTypeFilterID(Int32 docTypeFilterId)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByDocTypeFilterID(docTypeFilterId, conn);
            }
        }

        public static DataTable SelectByDocTypeFilterID(Int32 docTypeFilterId, SqlConnection conn)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilterItem.SelectByDocTypeFilterID);
            SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataRow Insert(Int32 docTypeFilterId, Int32 sisDocTypeId, String createdBy)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");
            if (sisDocTypeId < 1) throw new ArgumentNullException("sisDocTypeId");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilterItem.Insert);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                SQLUtility.AddParam(ref cmd, "@SIS_DocTypeID", sisDocTypeId);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void DeleteByDocTypeFilterID(Int32 docTypeFilterId)
        {
            if (docTypeFilterId < 1) throw new ArgumentNullException("docTypeFilterId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilterItem.DeleteByDocTypeFilterID);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterID", docTypeFilterId);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 DeleteByDocTypeFilterItemID(Int32 docTypeFilterItemId)
        {
            if (docTypeFilterItemId < 1) throw new ArgumentNullException("docTypeFilterItemId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_DocTypeFilterItem.DeleteByDocTypeFilterItemID);
                SQLUtility.AddParam(ref cmd, "@DocTypeFilterItemID", docTypeFilterItemId);
                DataRow row = SQLUtility.FillInDataRow(cmd);
                if (row == null) return 0;
                return ConvertUtility.ConvertToInt32(row["DocTypeFilterID"], 0);
            }
        }
    }
}
