﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_DocumentInfo
    {
        public static DataTable SelectDocuments(Int32 pid)
        {
            if (pid < 1) return null;
            DBPID db = new DBPID();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBPID.SPs.DocInfo.SelectByPID);
                SQLUtility.AddParam(ref cmd, "@PID", pid);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
