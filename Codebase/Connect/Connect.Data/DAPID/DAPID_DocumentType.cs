﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_DocumentType
    {
        public static DataTable SelectAll()
        {
            DBPID db = new DBPID();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBPID.SPs.DocType.SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
