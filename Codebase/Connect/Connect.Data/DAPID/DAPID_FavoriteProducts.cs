﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_FavoriteProducts
    {
        public static DataTable SelectByAccountID(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_FavoriteProducts.SelectByAccountID);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Insert(Guid accountId, Int32 pid)
        {
            if (accountId.IsNullOrEmptyGuid() || pid < 1) return null;

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_FavoriteProducts.Insert);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@PID", pid);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Delete(Guid accountId, Int32 pid)
        {
            if (pid < 1 || accountId.IsNullOrEmptyGuid()) return;

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_FavoriteProducts.Delete);
                SQLUtility.AddParam(ref cmd, "@AccountID", accountId);
                SQLUtility.AddParam(ref cmd, "@PID", pid);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
