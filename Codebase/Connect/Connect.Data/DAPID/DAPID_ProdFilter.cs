﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_ProdFilter
    {
        public static DataTable SelectAll()
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByProdFilterID(Int32 prodFilterId)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.SelectByFilterID);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByProdFilterID(Int32 prodFilterId, out DataTable tbFilterItems)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            tbFilterItems = null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.SelectByFilterID);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                DataRow row = SQLUtility.FillInDataRow(cmd);
                if (row != null)
                {
                    tbFilterItems = DAPID_ProdFilterItem.SelectByProdFilterID(prodFilterId, conn);
                }
                return row;
            }
            
        }

        public static DataRow Insert(String filterName, String filterOwnerRole, String createdBy)
        {
            if (filterName.IsNullOrEmptyString()) throw new ArgumentNullException("filterName");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.Insert);
                SQLUtility.AddParam(ref cmd, "@FilterName", filterName);
                SQLUtility.AddParam(ref cmd, "@FilterOwnerRole", filterOwnerRole);
                SQLUtility.AddParam(ref cmd, "@CreatedBy", createdBy);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Update(Int32 prodFilterId, String filterName)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            if (filterName.IsNullOrEmptyString()) throw new ArgumentNullException("filterName");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.Update);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                SQLUtility.AddParam(ref cmd, "@FilterName", filterName);
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Int32 prodFilterId)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilter.Delete);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
