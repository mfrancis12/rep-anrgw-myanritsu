﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_ProdFilterItem
    {
        public static DataTable SelectByProdFilterID(Int32 prodFilterId)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                return SelectByProdFilterID(prodFilterId, conn);
            }
        }

        public static DataTable SelectByProdFilterID(Int32 prodFilterId, SqlConnection conn)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            DBConnect db = new DBConnect();
            SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilterItem.SelectByProdFilterID);
            SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataRow Insert(Int32 prodFilterId, Int32 sisPid, String addedBy)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");
            if (sisPid < 1) throw new ArgumentNullException("sisPid");
            if (addedBy.IsNullOrEmptyString()) throw new ArgumentNullException("addedBy");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilterItem.Insert);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                SQLUtility.AddParam(ref cmd, "@SIS_PID", sisPid);
                SQLUtility.AddParam(ref cmd, "@addedBy", addedBy);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void DeleteByProdFilterID(Int32 prodFilterId)
        {
            if (prodFilterId < 1) throw new ArgumentNullException("prodFilterId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilterItem.DeleteByProdFilterID);
                SQLUtility.AddParam(ref cmd, "@ProdFilterID", prodFilterId);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prodFilterItemId"></param>
        /// <returns>ProdFilterID</returns>
        public static Int32 DeleteByProdFilterItemID(Int32 prodFilterItemId)
        {
            if (prodFilterItemId < 1) throw new ArgumentNullException("prodFilterItemId");

            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_PID_ProdFilterItem.DeleteByProdFilterItemID);
                SQLUtility.AddParam(ref cmd, "@ProdFilterItemID", prodFilterItemId);
                DataRow row = SQLUtility.FillInDataRow(cmd);
                if (row == null) return 0;
                return ConvertUtility.ConvertToInt32(row["ProdFilterID"], 0);
            }
        }
    }
}
