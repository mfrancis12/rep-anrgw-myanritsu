﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    public static class DAPID_ProductInfo
    {
        public static DataTable SelectAll(Int32 preferredPidLangId)
        {
            List<Int32> pidSupportedLangIDs = new List<int> { 1,2,3 };

            if (preferredPidLangId != 0 && !pidSupportedLangIDs.Contains(preferredPidLangId))
                preferredPidLangId = 1;
            DBPID db = new DBPID();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBPID.SPs.ProdInfo.SelectAllByPreferredLangID);
                if (preferredPidLangId == 0)
                    cmd.Parameters.AddWithValue("@PreferredLangID", DBNull.Value);
                else
                    SQLUtility.AddParam(ref cmd, "@PreferredLangID", preferredPidLangId);                
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectTexts(Int32 pid)
        {
            if (pid < 1) return null;
            DBPID db = new DBPID();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBPID.SPs.ProdInfo.ProductInfo_SelectText);
                SQLUtility.AddParam(ref cmd, "@PID", pid);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
