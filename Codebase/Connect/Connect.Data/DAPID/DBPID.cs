﻿using System;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data.DAPID
{
    internal class DBPID  : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("PID.DB.SPSchema"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("PID.DB.ConnStr"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("PID.DB.CmdTimeout"), out timeout);
                return timeout;
            }
        }

        internal class SPs
        {
            public class DocType
            {
                public const String SelectAll = "[uSP_MyAnritsu_DocumentType_SelectAll]";
            }

            public class ProdInfo
            {
                public const String SelectAllByPreferredLangID = "[uSP_MyAnritsu_Products_SelectAll]";
                public const String ProductInfo_SelectText = "[uSP_MyAnritsu_ProductInfo_SelectText]";
            }

            public class DocInfo
            {
                public const String SelectByPID = "[uSP_MYAnritsu_Documents_SelectByPID]";
            }

            
        }
    }
}
