﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAPackages
    {
        public static DataTable SelectPackageSystem()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.System_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static long InsertPackageWithFiles(String packName, String packPath, int systemId, bool isEnabled, String createdBy,
            DataTable dtFiles, DataTable dtPackageModels, String modelConfigType, String supType, String pckGrpName, bool assignToAllUsers, long copyFromPackageId = 0)
        {
            long packageID = 0;
            using (SqlConnection connection = DABase.GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("InsertPackageWithFiles");
                SqlCommand cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.Package_Insert);
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                try
                {
                    cmd.Parameters.AddWithValue("@PackageName", packName);
                    if (!string.IsNullOrEmpty(pckGrpName))
                        cmd.Parameters.AddWithValue("@PackageGroupName", pckGrpName);
                    cmd.Parameters.AddWithValue("@PackagePath", packPath);
                    cmd.Parameters.AddWithValue("@SystemID", systemId);
                    cmd.Parameters.AddWithValue("@IsEnabled", isEnabled);
                    cmd.Parameters.AddWithValue("@ModalConfigType", modelConfigType);
                    if (!String.IsNullOrEmpty(supType))
                        cmd.Parameters.AddWithValue("@SupportTypeCode", supType);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    cmd.Parameters.AddWithValue("@CopyFromPackageId", copyFromPackageId);

                    DataRow r = SQLUtility.FillInDataRow(cmd);
                    if (r == null) throw new ApplicationException();
                    packageID = ConvertUtility.ConvertToInt64(r["PackageID"], 0);

                    //Inserting files in to DB
                    cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageFiles_Insert);
                    cmd.Parameters.AddWithValue("@PackageID", packageID);
                    cmd.Parameters.AddWithValue("@FileTable", dtFiles);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    //cmd.Parameters.AddWithValue("@CopyFromPackageId", copyFromPackageId);
                    //cmd.Parameters.AddWithValue("@CurrentPackagePath", packPath);

                    int effected = cmd.ExecuteNonQuery();

                    //Insert models 
                    if (dtPackageModels != null && dtPackageModels.Rows.Count > 0)
                    {
                        cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageModels_Insert);
                        cmd.Parameters.AddWithValue("@PackageID", packageID);
                        cmd.Parameters.AddWithValue("@PackageModels", dtPackageModels);
                        cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                        cmd.ExecuteNonQuery();
                    }


                    if (packageID > 0 && effected > 0)
                        transaction.Commit();
                    //Assign all the users from supportType entry list to this package (packageID)
                    if (assignToAllUsers)
                    {
                        AssignPackgeUsers(supType, connection, packageID);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }
                return packageID;
            }
        }

        private static void AssignPackgeUsers(string supType, SqlConnection connection, long packageID)
        {
            SqlCommand cmd;
            SqlTransaction tran = connection.BeginTransaction("AssignPackageToUsers");
            try
            {
                cmd = DABase.MakeCmdSP(connection, tran, DABase.SP.Package.JpPackageAssignUser);
                cmd.Connection = connection;
                cmd.Transaction = tran;
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                cmd.Parameters.AddWithValue("@SupportTypecode", supType);
                int assigned = cmd.ExecuteNonQuery();
                if (assigned > 0)
                {
                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw new ApplicationException(ex.Message);
            }
        }


        public static long UpdatePackageWithFiles(long packageId, String packName, String packPath, int systemId, bool isEnabled, String modifiedBy, DataTable dtFiles,
            DataTable dtPackageModels, String modelConfigType, String supportTypeCode, String pckGrpName)
        {
            using (SqlConnection connection = DABase.GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("UpdatePackageWithFiles");
                SqlCommand cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.Package_Update);
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                try
                {
                    cmd.Parameters.AddWithValue("@PackageID", packageId);
                    cmd.Parameters.AddWithValue("@PackageName", packName);
                    if (!string.IsNullOrEmpty(pckGrpName))
                        cmd.Parameters.AddWithValue("@PackageGroupName", pckGrpName);
                    cmd.Parameters.AddWithValue("@PackagePath", packPath);
                    cmd.Parameters.AddWithValue("@SystemID", systemId);
                    cmd.Parameters.AddWithValue("@IsEnabled", isEnabled);
                    cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                    if (supportTypeCode != null)
                        cmd.Parameters.AddWithValue("@SupportTypeCode", supportTypeCode);
                    cmd.ExecuteNonQuery();

                    //DataRow r = SQLUtility.FillInDataRow(cmd);
                    //if (r == null) throw new ApplicationException();
                    //packageID = ConvertUtility.ConvertToInt64(r["PackageID"], 0);

                    //Inserting files in to DB
                    cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageFiles_Insert);
                    cmd.Parameters.AddWithValue("@PackageID", packageId);
                    cmd.Parameters.AddWithValue("@FileTable", dtFiles);
                    cmd.Parameters.AddWithValue("@CreatedBy", modifiedBy);
                    cmd.ExecuteNonQuery();

                    //Insert models 
                    if (dtPackageModels != null && dtPackageModels.Rows.Count > 0)
                    {
                        cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageModels_Insert);
                        cmd.Parameters.AddWithValue("@PackageID", packageId);
                        cmd.Parameters.AddWithValue("@PackageModels", dtPackageModels);
                        cmd.Parameters.AddWithValue("@CreatedBy", modifiedBy);
                        int effectedModels = cmd.ExecuteNonQuery();
                    }
                    if (packageId > 0)
                        transaction.Commit();

                    AssignPackgeUsers(supportTypeCode, connection, packageId);
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                        throw new ApplicationException(ex.Message);
                    }
                    catch (Exception ex2)
                    {

                    }
                }
                return packageId;
            }
        }

        public static void UpdatePackage(long packageID, String packName, String packPath, int systemId, bool isEnabled, String modifiedBy, DataTable dtPackageModels,
            String modelConfigType, String supportTypeCode, String pckGrpName)
        {
            using (SqlConnection connection = DABase.GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("UpdatePackageWithModels");
                SqlCommand cmd = DABase.MakeCmdSP(connection, null, DABase.SP.Package.Package_Update);
                cmd.Connection = connection;
                cmd.Transaction = transaction;

                try
                {
                    cmd.Parameters.AddWithValue("@PackageID", packageID);
                    cmd.Parameters.AddWithValue("@PackageName", packName);
                    if (!string.IsNullOrEmpty(pckGrpName))
                        cmd.Parameters.AddWithValue("@PackageGroupName", pckGrpName);
                    cmd.Parameters.AddWithValue("@PackagePath", packPath);
                    cmd.Parameters.AddWithValue("@SystemID", systemId);
                    cmd.Parameters.AddWithValue("@IsEnabled", isEnabled);
                    cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                    if (supportTypeCode != null)
                        cmd.Parameters.AddWithValue("@SupportTypeCode", supportTypeCode);
                    //transaction 1
                    int effectedPackageRows = cmd.ExecuteNonQuery();
                    //Insert models 
                    if (dtPackageModels != null && dtPackageModels.Rows.Count > 0)
                    {
                        cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageModels_Insert);
                        cmd.Parameters.AddWithValue("@PackageID", packageID);
                        cmd.Parameters.AddWithValue("@PackageModels", dtPackageModels);
                        cmd.Parameters.AddWithValue("@CreatedBy", modifiedBy);
                        //trnsaction 2
                        cmd.ExecuteNonQuery();
                    }

                    if (effectedPackageRows > 0)
                        transaction.Commit();

                    //AssignPackgeUsers(supportTypeCode, connection, packageID);
                }
                catch (Exception ex)
                {

                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);

                }
            }
        }

        public static DataSet SelectByPackageID(long packageID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_SelectByPackageID);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                return SQLUtility.FillInDataSet(cmd);
            }
        }

        public static bool InsertPackageAssignedOrg(long packageID, Guid accountID, String createdBy, DateTime? expiryDate)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_Org_Insert);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                if (expiryDate != DateTime.MinValue)
                    cmd.Parameters.AddWithValue("@ExpiryDate", expiryDate);
                return (cmd.ExecuteNonQuery() > 0);
            }
        }

        public static DataTable SelectOrganizationsByPackage(long packageID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_Org_SelectByPackageID);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static bool DeleteOrganizationPackageMapping(long packageID, Guid accountID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_Org_DeleteMapping);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                return (cmd.ExecuteNonQuery() > 0);
            }
        }

        public static bool DeletePackage(long packageID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_DeleteByID);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                return (cmd.ExecuteNonQuery() > 0);
            }
        }

        public static DataSet SelectPackagesWithFiles(String modelConfigType)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackageWithFiles_Select);
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                return SQLUtility.FillInDataSet(cmd);
            }
        }

        public static bool UpdatePackageFiles(long packageID, String modifiedBy, DataTable dtFiles)
        {
            using (SqlConnection connection = DABase.GetOpenedConn())
            {
                SqlTransaction transaction;
                // Start transaction.
                transaction = connection.BeginTransaction("UpdatePackageFiles");
                SqlCommand cmd = DABase.MakeCmdSP(connection, transaction, DABase.SP.Package.PackageFiles_Update);
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                try
                {
                    //Update existing files and insert new files in to DB
                    cmd.Parameters.AddWithValue("@PackageID", packageID);
                    cmd.Parameters.AddWithValue("@FileTable", dtFiles);
                    cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    int effected = cmd.ExecuteNonQuery();
                    if (effected > 0)
                    {
                        transaction.Commit();
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                        return false;
                    }
                    catch (Exception ex2)
                    {

                    }
                }
                return false;
            }
        }

        public static DataTable SelectAllEmailsByPackage(long packageID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.EmailAddressByAccount_Select);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAllActivePackages(String packageName, Guid accountID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_SelectAll);
                cmd.Parameters.AddWithValue("@PackageName", packageName);
                cmd.Parameters.AddWithValue("@AccountID", accountID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectAllActivePackagesByAccountID(Guid accID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_SelectAllByAccountID);
                cmd.Parameters.AddWithValue("@AccountID", accID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static bool SelectAllActivePackagesByModelNumber(string modelNumber, DataTable dtTeams)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_SelectAllByModelNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                cmd.Parameters.AddWithValue("@AccountIDs", dtTeams);
                var retValue = cmd.ExecuteScalar();
                return ConvertUtility.ConvertToInt32(retValue, 0) > 0;
            }
        }

        public static bool BulkUpdateOrgPackages(Guid accID, DataTable packageExpDateTable)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.Package_Org_BulkUpdate);
                cmd.Parameters.AddWithValue("@AccountID", accID);
                cmd.Parameters.AddWithValue("@PackageExpDateList", packageExpDateTable);
                return (cmd.ExecuteNonQuery() > 0);
            }

        }

        public static DataSet SelectPackagesWithFilesByAccountID(Guid accID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackageWithFilesByAccount_Select);
                cmd.Parameters.AddWithValue("@AccountID", accID);
                return SQLUtility.FillInDataSet(cmd);
            }
        }

        public static DataTable SelectPackagesByAccountID(DataTable dtTeams, string modelNumner, bool isLicensePkg)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackagesByAccount_Select);
                cmd.Parameters.AddWithValue("@AccountIDs", dtTeams);
                cmd.Parameters.AddWithValue("@IsLicensPackages", isLicensePkg);
                cmd.Parameters.AddWithValue("@MasterModel", modelNumner);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectPackageFilesByPackageID(long packageID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackageFilesByPackageID_Select);
                cmd.Parameters.AddWithValue("@PackageID", packageID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static bool BulkUpdateUserPackageAccess(DataTable dtEntries, Int64 packageId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackageUserAccessBulkUpdate);
                cmd.Parameters.AddWithValue("@PackageId", packageId);
                cmd.Parameters.AddWithValue("@SupportEntriesList", dtEntries);
                return (cmd.ExecuteNonQuery() > 0);
            }

        }

        #region JP Packages

        public static DataTable SelectJpPackageGroups(String modelNumber, String serialNumber, Guid membershipId, String packageType, String modelConfigType, String pkgGroupName, DataTable dtTeams)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.JpPackageGroupsSelect);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@PackageType", packageType);
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                cmd.Parameters.AddWithValue("@PackageGroupName", pkgGroupName);
                cmd.Parameters.AddWithValue("@AccountIDs", dtTeams);
                return SQLUtility.FillInDataTable(cmd);
            }
        }


        public static void SelectJpPackageInfo(String modelNumber, String serialNumber, Guid membershipId, String modelConfigType
            , out bool hasJpDownloadPackages, out bool hasJpLicensePacjages, out bool hasJpVersionPackages, DataTable dtTeams)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.JpPackageInfoSelect);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@ModelConfigType", modelConfigType);
                cmd.Parameters.AddWithValue("@AccountIDs", dtTeams);
                //Add the output parameter to the command object
                var outPutDownLoad = new SqlParameter
                {
                    ParameterName = "@HasJpDownLoadPackages",
                    SqlDbType = SqlDbType.Bit,
                    Direction = ParameterDirection.Output
                };
                var outPutLicense = new SqlParameter
                {
                    ParameterName = "@HasJpLicensePackages",
                    SqlDbType = SqlDbType.Bit,
                    Direction = ParameterDirection.Output
                };
                var outPutVersion = new SqlParameter
                {
                    ParameterName = "@HasJpVersionPackages",
                    SqlDbType = SqlDbType.Bit,
                    Direction = ParameterDirection.Output
                };
                cmd.Parameters.AddRange(new[] { outPutDownLoad, outPutLicense, outPutVersion });
                cmd.ExecuteNonQuery();
                hasJpDownloadPackages = (bool)outPutDownLoad.Value;
                hasJpLicensePacjages = (bool)outPutLicense.Value;
                hasJpVersionPackages = (bool)outPutVersion.Value;
            }
        }

        public static DataTable SelectJpPackageSubFolderFiles(long pkgId, String subFolderName, String pkgPath, String locale)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.JpPackageSubFolderFilesSelect);
                cmd.Parameters.AddWithValue("@PackageID", pkgId);
                cmd.Parameters.AddWithValue("@SubFolder", subFolderName);
                cmd.Parameters.AddWithValue("@PackagePath", pkgPath);
                cmd.Parameters.AddWithValue("@Locale", locale);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectJpPackageSubFoldersByPackageId(long packageId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.JpPackageSubFoldersSelect);
                cmd.Parameters.AddWithValue("@PackageID", packageId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
        #endregion

        public static void CopyPackageComments(long packageId, long copyFromPackageId, bool copyAllComments = true)
        {
            //try
            //{
            using (var conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.PackageComments_Copy);
                cmd.Parameters.AddWithValue("@PackageID", packageId);
                cmd.Parameters.AddWithValue("@CopyFromPackageId", copyFromPackageId);
                cmd.Parameters.AddWithValue("@CopyAllComments", copyAllComments);
                cmd.ExecuteNonQuery();
            }
            //}
            //catch (Exception ex)
            //{
            //    throw new ApplicationException(ex.Message);
            //}
        }

        #region Auto Download
        public static DataTable GetAutoDownloadPackageFiles(String modelNumber, String serialNumber,string emailAddress,string modelconfig,DataTable dtTeams)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Package.GetAutoDownloadPackageFilesInfo);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress);
                cmd.Parameters.AddWithValue("@ModelConfigType", modelconfig);
                cmd.Parameters.AddWithValue("@AccountIDs", dtTeams);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByModel(String modelNumber, Boolean exactMatch)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ModelMaster.SelectByModel);
                SQLUtility.AddParam(ref cmd, "@ModelNumber", modelNumber);
                SQLUtility.AddParam(ref cmd, "@ExactMatch", exactMatch);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
        #endregion
    }
}
