﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAProdReg_IntModel
    {
        public static DataTable SelectByProdRegID(Int32 prodRegId)
        {
            if (prodRegId < 1) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_ProdReg_IntModel_SelectByProdRegID);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void DeleteByIntMnTagID(Int32 intMnTagId)
        {
            if (intMnTagId < 1) return;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_ProdReg_IntModel_DeleteByIntMnTagID);
                cmd.Parameters.AddWithValue("@IntMnTagID", intMnTagId);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow Insert(Int32 prodRegId, String intModelNumber, String createdBy, DateTime optionalExpirationDate)
        {
            if (prodRegId < 1 || intModelNumber.IsNullOrEmptyString() || createdBy.IsNullOrEmptyString()) return null;
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, db.SQLSP_ProdReg_IntModel_Insert);
                cmd.Parameters.AddWithValue("@ProdRegID", prodRegId);
                cmd.Parameters.AddWithValue("@IntModelNumber", HttpUtility.HtmlEncode(intModelNumber));
                cmd.Parameters.AddWithValue("@CreatedBy", HttpUtility.HtmlEncode(createdBy));
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@ExpireOnUTC", optionalExpirationDate);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
    }
}
