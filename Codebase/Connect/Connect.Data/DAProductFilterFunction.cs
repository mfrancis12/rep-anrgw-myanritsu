﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using ApiSdk;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Web;

namespace Anritsu.Connect.Data
{
    public static class DAProductFilterFunction
    {

        public static DataTable SelectByModelConfigID(int modelConfigID)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProductFilterFunction.SelectByModelConfigId);
                cmd.Parameters.AddWithValue("@ModelConfigId", modelConfigID);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByFilterKeywords(int modelConfigID, string model, string function, string option)
        {
            DBConnect db = new DBConnect();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBConnect.SP_ProductFilterFunction.SelectByFilterKeywords);
                cmd.Parameters.AddWithValue("@ModelConfigId", modelConfigID);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@Function", function);
                cmd.Parameters.AddWithValue("@Option", option);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static bool Insert(int modelConfigID, string model, string function, string option, string createdBy)
        {
             DBConnect db = new DBConnect();
            SqlTransaction tran;
            using (SqlConnection conn = db.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    SqlCommand cmd = db.MakeCmdSP(conn, tran, DBConnect.SP_ProductFilterFunction.Insert);
                    cmd.Parameters.AddWithValue("@ModelConfigID", modelConfigID);
                    cmd.Parameters.AddWithValue("@Model", model);
                    cmd.Parameters.AddWithValue("@Function", function);
                    cmd.Parameters.AddWithValue("@Option", option);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    SQLUtility.FillInDataRow(cmd);

                    tran.Commit();
                }
                catch (Exception)
                {
                    tran.Rollback();
                    return false;
                }
                return true;
            }
        }

        public static void Delete(int modelConfigID, string model, string function, string option)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DBConnect.SP_ProductFilterFunction.Delete);
                cmd.Parameters.AddWithValue("@ModelConfigID", modelConfigID);
                cmd.Parameters.AddWithValue("@Model", model);
                cmd.Parameters.AddWithValue("@Function", function);
                cmd.Parameters.AddWithValue("@Option", option);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
