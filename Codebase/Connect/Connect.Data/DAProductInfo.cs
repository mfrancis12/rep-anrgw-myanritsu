﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using ApiSdk;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Web;
namespace Anritsu.Connect.Data
{
    public static class DAProductInfo
    {
        public static DataRow SelectByModelNumberInGWDB(String modelNumber, String serialNumber, Int32 gwCultureGroupID)
        {
            DBGlobalWeb gwdb = new DBGlobalWeb();
            using (SqlConnection conn = gwdb.GetOpenedConn())
            {
                SqlCommand cmd = gwdb.MakeCmdInline(conn, null, DBGlobalWeb.SQL_Inline.FindProductByModelNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                cmd.Parameters.AddWithValue("@CultureGroupId", gwCultureGroupID);
                return SQLUtility.FillInDataRow(cmd);
            }

        }
        public static object SelectByModelNumberInGWDB_Api(String modelNumber, String serialNumber, Int32 gwCultureGroupID)
        {
            var apiUri =
                new Uri(string.Format("{0}{1}", ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.DownloadsUrl"], "GetDownLoads"));
            var apiParams = new Dictionary<string, string>();
            apiParams.Add("cultureGroupId", gwCultureGroupID.ToString());
            apiParams.Add("modelNumber", modelNumber);
            var rsClass = new ResponseClass();
            try
            {

                var response = rsClass.CallMethod<string>(apiUri, apiParams);
                return response.StatusCode == HttpStatusCode.OK
                    ? Convert.ToString(response.Content) : string.Empty;

            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("CONNECT", HttpContext.Current.Request.Url.Host, ex, HttpContext.Current);
                return string.Empty;
            }

        }


        public static DataRow SelectByModelNumberInErpDB(String modelNumber, String serialNumber, String erpSource)
        {
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindProductByModelSerialNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                cmd.Parameters.AddWithValue("@CountryCode", erpSource.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectWarrantyInfoByModelSerialNumberInErpDB(String modelNumber, String serialNumber)
        {
            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindProductWarrantyByModelNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
                cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        //moved to DAErpData
        //public static DataTable SelectInfoInGlobalSerialNumberSystemErpDB(String modelNumber, String serialNumber)
        //{
        //    if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
        //    DBErp erpDB = new DBErp();
        //    using (SqlConnection conn = erpDB.GetOpenedConn())
        //    {
        //        SqlCommand cmd = erpDB.MakeCmdSP(conn, null, DBErp.SQL_SP.FindModelSerialNumberInGlobalSerialNumberSystem);
        //        cmd.Parameters.AddWithValue("@ModelNumber", modelNumber.Trim());
        //        cmd.Parameters.AddWithValue("@SerialNumber", serialNumber.Trim());
        //        return SQLUtility.FillInDataTable(cmd);
        //    }
        //}

        //public static Boolean IsInGlobalSerialNumberSystemDB(String modelNumber, String serialNumber)
        //{
        //    DataTable tb = DAErpData.GSNDB_SelectByModelSerial(modelNumber, serialNumber);
        //    return (tb != null && tb.Rows.Count > 0);
        //}

        public static Boolean IsValidMnSn(String modelNumber, String serialNumber, Boolean snValidationNotRequired)
        {
            if (serialNumber.StartsWith("GWTEST") || serialNumber.StartsWith("anritsu-dwl")) return true;
            if (snValidationNotRequired) return true;
            Boolean found;
            DataTable tb = DAErpData.GSNDB_SelectByModelSerial(modelNumber, serialNumber);
            found = (tb != null && tb.Rows.Count > 0);

            if (!found)//check in Warranty DB
            {
                tb = DAErpData.GlobalWebWarrantyDB_SelectByModelSerial(modelNumber, serialNumber);
                found = (tb != null && tb.Rows.Count > 0);
            }

            return found;
        }

        public static DataTable WarrantyInfo_SelectByProdRegWebToken(DataTable dtTeams, string modelNumber, string serialNumber, out Boolean showWarrantyInfo)
        {
            showWarrantyInfo = true;
            //if (webToken.IsNullOrEmptyGuid()) return null;
            var dsModelSerialWithWarrantyInfo = DAAcc_ProductRegistered_Master.SelectModelSerial(dtTeams, modelNumber, serialNumber);
            DataTable modelSerial = (dsModelSerialWithWarrantyInfo.Tables.Count > 0) ? dsModelSerialWithWarrantyInfo.Tables[0] : null;
            showWarrantyInfo = (dsModelSerialWithWarrantyInfo.Tables.Count > 1 && !String.IsNullOrEmpty(Convert.ToString(dsModelSerialWithWarrantyInfo.Tables[1].Rows[0][0])))
                ? Convert.ToBoolean(dsModelSerialWithWarrantyInfo.Tables[1].Rows[0][0]) : true;
            if (modelSerial == null || modelSerial.Rows.Count < 1) return null;

            DataTable tbWarrantyData = null;

            DBErp erpDb = new DBErp();
            using (SqlConnection conn = erpDb.GetOpenedConn())
            {
                SqlCommand cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindProductWarrantyByModelNumber);
                cmd.Parameters.AddWithValue("@ModelNumber", String.Empty);
                cmd.Parameters.AddWithValue("@SerialNumber", String.Empty);

                foreach (DataRow r in modelSerial.Rows)
                {
                    String mn = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
                    String sn = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);

                    if (mn.IsNullOrEmptyString() || sn.IsNullOrEmptyString()) continue;
                    cmd.Parameters["@ModelNumber"].Value = mn.Trim();
                    cmd.Parameters["@SerialNumber"].Value = sn.Trim();
                    DataTable tb = SQLUtility.FillInDataTable(cmd);
                    if (tb != null)
                    {
                        if (tbWarrantyData == null) tbWarrantyData = tb;
                        else tbWarrantyData.Merge(tb);
                    }
                }
            }
            return tbWarrantyData;
        }
        public static DataTable SelectAllModelNumberInErpDB()
        {
            var erpDb = new DBErp();
            using (var conn = erpDb.GetOpenedConn())
            {
                var cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.FindAllProductInfo);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static string GetPIMMappedModelProductName(string modelNumber)
        {
            string productName = modelNumber;
            try
            {
                var pimDb = new DBPIM();
                using (var conn = pimDb.GetOpenedConn())
                {
                    var cmd = pimDb.MakeCmdSP(conn, null, "uSP_GetProductName");

                    cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                    cmd.Parameters.Add("@ProductName", SqlDbType.NVarChar, 4000).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    productName = cmd.Parameters["@ProductName"].Value.ToString();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                productName = modelNumber;
            }
            return productName;
        }
        public static string CheckPIMMappedModelNumber(string modelNumber)
        {
            string cmsmodelnumber = modelNumber;
            try
            {
                var pimDb = new DBPIM();
                using (var conn = pimDb.GetOpenedConn())
                {
                    var cmd = pimDb.MakeCmdSP(conn, null, "uSP_CheckPIMMappedModelNumber");

                    cmd.Parameters.AddWithValue("@ModelNumber", modelNumber);
                    cmd.Parameters.Add("@CMSModelNumber", SqlDbType.NVarChar,4000).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    cmsmodelnumber = cmd.Parameters["@CMSModelNumber"].Value.ToString();
                    conn.Close();
                }
            }
            catch(Exception ex)
            {
                cmsmodelnumber = modelNumber;
            }
            return cmsmodelnumber;
        }
        public static DataTable GetProductInfoFromCMSDB(string cultureGroupId,string ModelNumbers)
        {
            DataSet ds = new DataSet("SQLDatabase");
            // Create & Fill Parameter 1
            DataTable dt1 = new DataTable("ModelNumbersTable");
            dt1.Columns.Add("ModelNumber", typeof(string));
            string[] strarray = ModelNumbers.Split(',');
            foreach (string str in strarray)
            {
                DataRow row = dt1.NewRow();
                row["ModelNumber"] = (CheckPIMMappedModelNumber(str));
                dt1.Rows.Add(row);
            }

            var erpDb = new DBErp();
            using (var conn = erpDb.GetOpenedConn())
            {
                var cmd = erpDb.MakeCmdSP(conn, null, DBErp.SQL_SP.GetCMSProductBasicInfo);
                
                cmd.Parameters.AddWithValue("@CultureCode", cultureGroupId);
                // Parameter 1
                SqlParameter param1 = new SqlParameter("@ModelNumbers", SqlDbType.Structured)
                {
                    TypeName = "dbo.ModelNumbersTable",
                    Value = dt1
                };
                cmd.Parameters.Add(param1);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        
    }
}
