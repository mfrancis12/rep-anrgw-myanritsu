﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DAProfile_Address
    {
        public static Int32 DeleteByAccountID(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.DeleteByAccountID);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return cmd.ExecuteNonQuery();
            }

        }

        public static Int32 DeleteByAddressID(Int32 addressId)
        {
            if (addressId < 1) return 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.DeleteByAddressID);
                cmd.Parameters.AddWithValue("@AddressID", addressId);
                return cmd.ExecuteNonQuery();
            }

        }

        public static Int32 Insert(Guid accountId
            , String addressName
            , String address1
            , String address2
            , String cityTown
            , String stateProvince
            , String zipPostalCode
            , String countryCode
            , String phoneNumber
            , String faxNumber)
        {
            if (accountId.IsNullOrEmptyGuid()) return 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
               return Insert(accountId
                , addressName
                , address1
                , address2
                , cityTown
                , stateProvince
                , zipPostalCode
                , countryCode
                , phoneNumber
                , faxNumber
                , conn
                , null);
            }

        }

        public static Int32 Insert(Guid accountId
            , String addressName
            , String address1
            , String address2
            , String cityTown
            , String stateProvince
            , String zipPostalCode
            , String countryCode
            , String phoneNumber
            , String faxNumber
            , SqlConnection conn
            , SqlTransaction tran)
        {
            if (accountId.IsNullOrEmptyGuid()) return 0;
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Profile_Address.Insert);
            cmd.Parameters.AddWithValue("@AccountID", accountId);
            cmd.Parameters.AddWithValue("@AddressName", addressName.Trim());
            cmd.Parameters.AddWithValue("@Address1", address1.Trim());
            cmd.Parameters.AddWithValue("@Address2", address2.Trim());
            cmd.Parameters.AddWithValue("@CityTown", cityTown.Trim());
            cmd.Parameters.AddWithValue("@StateProvince", stateProvince.Trim());
            cmd.Parameters.AddWithValue("@ZipPostalCode", zipPostalCode.Trim());
            cmd.Parameters.AddWithValue("@CountryCode", countryCode.Trim());
            cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber.Trim());
            cmd.Parameters.AddWithValue("@FaxNumber", faxNumber.Trim());
            DataRow r = SQLUtility.FillInDataRow(cmd);
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["AddressID"], 0);

        }
        /*
        public static void InsertProfileAddress(
            Guid accountId
            , String addressName
            , String address1
            , String address2
            , String cityTown
            , String stateProvince
            , String zipPostalCode
            , String countryCode
            , String phoneNumber
            , String faxNumber)
        {
            SqlTransaction tran;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                tran = conn.BeginTransaction();
                try
                {
                    if (!accountId.IsNullOrEmptyGuid())
                    {
                        Int32 companyAddressId = Insert(accountId, addressName, address1, address2
                            , cityTown, stateProvince, zipPostalCode, countryCode, phoneNumber, faxNumber, conn, tran);

                        DAAcc_AccountMaster.UpdateCompanyAddressID(accountId, companyAddressId, conn, tran);
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
        }
        
        public static void Update(Int32 addressId
            , String addressName
            , String address1
            , String address2
            , String cityTown
            , String stateProvince
            , String zipPostalCode
            , String countryCode
            , String phoneNumber
            , String faxNumber
            , Boolean isVerified
            , Boolean isCreateHistory)
        {
            if (addressId < 1) return;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.Update);
                cmd.Parameters.AddWithValue("@AddressID", addressId);
                cmd.Parameters.AddWithValue("@AddressName", addressName.Trim());
                cmd.Parameters.AddWithValue("@Address1", address1.Trim());
                cmd.Parameters.AddWithValue("@Address2", address2.Trim());
                cmd.Parameters.AddWithValue("@CityTown", cityTown.Trim());
                cmd.Parameters.AddWithValue("@StateProvince", stateProvince.Trim());
                cmd.Parameters.AddWithValue("@ZipPostalCode", zipPostalCode.Trim());
                cmd.Parameters.AddWithValue("@CountryCode", countryCode.Trim());
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber.Trim());
                cmd.Parameters.AddWithValue("@FaxNumber", faxNumber.Trim());
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsVerified", isVerified);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@CreateHistory", isCreateHistory);
                cmd.ExecuteNonQuery();
            }

        }
        */
        public static DataTable SelectByAccountID(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.SelectByAccountID);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataRow SelectByMemberShipID(Guid ownerMemberShipID)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.SelectByMemberShipID);
                cmd.Parameters.AddWithValue("@MembershipId", ownerMemberShipID);
                return SQLUtility.FillInDataRow(cmd);
            }

        }

        public static DataRow SelectHistoryByAccountID(Guid accountId)
        {
            if (accountId.IsNullOrEmptyGuid()) return null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.SelectHistoryByAccountID);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return SQLUtility.FillInDataRow(cmd);
            }

        }
        public static DataRow SelectByEmailID(String emailAddress)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Profile_Address.SelectByEmailID);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress);
                return SQLUtility.FillInDataRow(cmd);
            }

        }
        
    }
}
