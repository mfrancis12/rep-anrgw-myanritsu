﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DARes_ContentStore
    {
        public static DataRow ContentStore_Select(String classKey, String resourceKey, String locale)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return ContentStore_Select(classKey, resourceKey, locale, false, conn, null);
            }
        }

        public static DataRow ContentStore_Select(String classKey, String resourceKey, String locale, Boolean? exactMatch)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return ContentStore_Select(classKey, resourceKey, locale, exactMatch, conn, null);
            }
        }

        public static DataRow ContentStore_Select(String classKey, String resourceKey, String locale, Boolean? exactMatch, SqlConnection conn, SqlTransaction tran)
        {
            if (locale.IsNullOrEmptyString()) locale = "en";
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Res.ContentStore_Select);
            cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
            cmd.Parameters.AddWithValue("@ResourceKey", resourceKey.Trim());
            cmd.Parameters.AddWithValue("@Locale", locale);
            if (exactMatch.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@ExactMatch", (Boolean)exactMatch);
            else cmd.Parameters.AddWithValue("@ExactMatch", DBNull.Value);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataTable SelectResourceKey(String classKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectResourceKey);
                cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByClassKeyResourceKey(String classKey, String resourceKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectByClassKeyResourceKey);
                cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
                cmd.Parameters.AddWithValue("@ResourceKey", resourceKey.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByContentID(Int32 contentId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectByContentID);
                cmd.Parameters.AddWithValue("@ContentID", contentId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static Int32 DeleteByContentID(Int32 contentId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.DeleteByContentID);
                cmd.Parameters.AddWithValue("@ContentID", contentId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static Int32 DeleteByClassKeyResKey(String classKey, String resKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return DeleteByClassKeyResKey(classKey, resKey, conn, null);
            }
        }

        public static Int32 DeleteByClassKeyResKey(String classKey, String resKey, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Res.DeleteByClassKeyResKey);
            cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
            cmd.Parameters.AddWithValue("@ResourceKey", resKey.Trim());
            return cmd.ExecuteNonQuery();
        }

        public static void Update(Int32 contentId, String contentStr, Boolean allowAutoTranslate, Boolean encodeHtml)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.Update);
                cmd.Parameters.AddWithValue("@ContentID", contentId);
                if(encodeHtml) SQLUtility.SqlParam_AddEncodedHtml(ref cmd, "@ContentStr", contentStr);
                else cmd.Parameters.AddWithValue("@ContentStr", contentStr);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@AllowAutoTranslate", allowAutoTranslate);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Insert(String classKey, String resourceKey, String locale, String contentStr, Boolean encodeHtml)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                if (locale.IsNullOrEmptyString()) locale = "en";
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.Insert);
                cmd.Parameters.AddWithValue("@ClassKey", classKey.Trim());
                cmd.Parameters.AddWithValue("@ResourceKey", resourceKey.Trim());
                cmd.Parameters.AddWithValue("@Locale", locale);

                if (encodeHtml) SQLUtility.SqlParam_AddEncodedHtml(ref cmd, "@ContentStr", contentStr);
                else cmd.Parameters.AddWithValue("@ContentStr", contentStr);                
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["ContentID"], 0);
            }
        }

        public static DataTable SelectDistinctLocale()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectDistinctLocale);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectFiltered(String classKey, String resKey, String locale, String contentKeyword)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectFiltered);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ClassKey", classKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ResourceKey", resKey.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@Locale", locale.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ContentKeyword", contentKeyword.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable ContentSupportedLocale_SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.ContentSupportedLocale_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectForGoogleTranslateAPI(Int32 startContentId, Int32 endContentId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectForGoogleTranslateAPI);
                cmd.Parameters.AddWithValue("@StartContentID", startContentId);
                cmd.Parameters.AddWithValue("@EndContentID", endContentId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void InsertUpdate(String classKey, String resourceKey, String locale, String contentStr)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.InsertUpdate);
                SQLUtility.AddParam(ref cmd, "@ClassKey", classKey);
                SQLUtility.AddParam(ref cmd, "@ResourceKey", resourceKey);
                SQLUtility.AddParam(ref cmd, "@Locale", locale);
                cmd.Parameters.AddWithValue("@ContentStr", contentStr);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable SelectClassKeyLocale()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectClassKeyLocale);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByClassKeyLocale(String classKey, String locale)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Res.SelectByClassKeyLocale);
                SQLUtility.AddParam(ref cmd, "@ClassKey", classKey);
                SQLUtility.AddParam(ref cmd, "@Locale", locale);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
