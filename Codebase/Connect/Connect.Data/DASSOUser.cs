﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.AnrCommon.CoreLib;
using System.Data.SqlClient;

namespace Anritsu.Connect.Data
{
    public static class DASSOUser
    {
        public static DataRow SelectByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            DBGlobalWeb gwdb = new DBGlobalWeb();
            using (SqlConnection conn = gwdb.GetOpenedConn())
            {
                SqlCommand cmd = gwdb.MakeCmdSP(conn, null, DBGlobalWeb.SQL_SP.Sql_SSOSp.User_SelectByEmailOrUserName);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());
                DataTable tb = SQLUtility.FillInDataTable(cmd);
                if (tb == null || tb.Rows.Count != 1) return null;
                return tb.Rows[0];
            }
        }
    }
}
