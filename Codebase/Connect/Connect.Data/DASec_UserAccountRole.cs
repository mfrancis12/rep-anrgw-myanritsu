﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{

    public static class DASec_UserAccountRole
    {

        public static DataTable Sec_Role_SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.Sec_Role_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable Select(Guid membershipId, Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return Select(membershipId, accountId, conn, null);
            }
        }

        public static DataTable Select(Guid membershipId, Guid accountId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Sec.UserAccountRole_Select);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataTable SelectForUserAdmin(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_SelectForUserAdmin);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                return SQLUtility.FillInDataTable(cmd);
            }
            
        }

        public static DataTable SelectAvailableAccounts(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_SelectAvailableAccounts);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectSearchAccounts(Guid membershipId, String searchTxt)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_SelectSearchedAccounts);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@SearchText", searchTxt);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static void Insert(Guid membershipId, Guid accountId, String role, String addedByEmail, String addedByName, String addedFromIp)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                Insert(membershipId, accountId, role, addedByEmail, addedByName, addedFromIp, conn, null);
            }
        }

        public static void Insert(Guid membershipId, Guid accountId, String role
            , String addedByEmail, String addedByName, String addedFromIp
            , SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Sec.UserAccountRole_Insert);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
            cmd.Parameters.AddWithValue("@Role", role);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@AddedByEmail", addedByEmail);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@AddedByName", addedByName);
            SQLUtility.SqlParam_AddAsString(ref cmd, "@AddedFromIP", addedFromIp);
            cmd.ExecuteNonQuery();
        }

        public static Int32 Delete(Int32 userAccountRefId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_Delete);
                cmd.Parameters.AddWithValue("@UserAccountRefID", userAccountRefId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Guid membershipId, Guid accountId, String role)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_DeleteByAccountIDMembershipIdRole);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.Parameters.AddWithValue("@Role", role.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static void Delete(Guid membershipId, Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_DeleteByAccountIDMembershipId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 SelectTotalAccounts(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccountRole_SelectTotalAccounts);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["TotalAccounts"], 0);
            }
        }

        public static DataTable SelectSearchAccountsAssignedToPackage(long packageId, String searchTxt)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserAccount_Organazation_SelectSearchedAccounts);
                SQLUtility.SqlParam_AddAsLongInt(ref cmd, "@PackageId", packageId);
                cmd.Parameters.AddWithValue("@SearchText", searchTxt);
                return SQLUtility.FillInDataTable(cmd);
            }

        }
    }
}
