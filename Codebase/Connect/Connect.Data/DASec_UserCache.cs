﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASec_UserCache
    {
        public static DataRow SelectByUserCacheID(Int32 userCacheId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserCache_SelectByUserCacheID);
                cmd.Parameters.AddWithValue("@UserCacheID", userCacheId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByMembershipIdDataKey(Guid membershipId, String dataKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return SelectByMembershipIdDataKey(membershipId, dataKey, conn, null);
            }
        }

        public static DataRow SelectByMembershipIdDataKey(Guid membershipId, String dataKey, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Sec.UserCache_SelectByMembershipIdDataKey);
            cmd.Parameters.AddWithValue("@MembershipId", membershipId);
            cmd.Parameters.AddWithValue("@DataKey", dataKey);
            return SQLUtility.FillInDataRow(cmd);
        }
        

        public static DataTable SelectByMembershipId(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserCache_SelectByMembershipId);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow InsertUpdate(Guid membershipId, String dataKey, String dataValue)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserCache_InsertUpdate);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@DataKey", dataKey);
                cmd.Parameters.AddWithValue("@DataValue", dataValue);
                cmd.ExecuteNonQuery();

                return SelectByMembershipIdDataKey(membershipId, dataKey);
            }
        }
    }
}
