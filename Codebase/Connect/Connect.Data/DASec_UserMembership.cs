﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASec_UserMembership
    {
        public static DataSet SelectByMembershipId(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return SelectByMembershipId(membershipId, conn, null);
            }
        }

        public static DataRow SelectHistoryByMembershipId(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                return SelectHistoryByMembershipId(membershipId, conn, null);
            }
        }

        public static DataSet SelectByMembershipId(Guid membershipId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Sec.UserMembership_SelectByMembershipId);
            cmd.Parameters.AddWithValue("@MembershipId", membershipId);
            return SQLUtility.FillInDataSet(cmd);
        }

        public static DataRow SelectHistoryByMembershipId(Guid membershipId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.MakeCmdSP(conn, tran, DABase.SP.Sec.UserMembership_SelectHistoryByMembershipId);
            cmd.Parameters.AddWithValue("@MembershipId", membershipId);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static Int32 Delete(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_Delete);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static Int32 DeleteFromAccount(Guid membershipId, Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_DeleteFromAccount);
                cmd.Parameters.AddWithValue("@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static DataTable SelectAllFiltered(Guid membershipId, String emailAddress, String lastName, String firstName, Boolean? isAdmin)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectAllFiltered);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@EmailAddress", emailAddress);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@LastName", lastName);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@FirstName", firstName);
                if (isAdmin.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsAdministrator", isAdmin.Value);
                else cmd.Parameters.AddWithValue("@IsAdministrator", DBNull.Value);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable AdminUserSearch(Guid membershipId,
            String emailAddress,
            String lastNameOrFirstName,
            String orgName,
            String companyName,
            Boolean? isAdmin)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_AdminUserSearch);

                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@EmailAddress", emailAddress);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@LastNameOrFirstName", lastNameOrFirstName);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@OrgName", orgName);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", companyName);
                if (isAdmin.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsAdministrator", isAdmin.Value);
                else cmd.Parameters.AddWithValue("@IsAdministrator", DBNull.Value);

                return SQLUtility.FillInDataTable(cmd);
            }
        }

        //public static SqlDataReader AdminUserSearchAsReader(Guid membershipId,
        //    String emailAddress,
        //    String lastNameOrFirstName,
        //    String orgName,
        //    String companyName,
        //    Boolean? isAdmin)
        //{
        //    SqlConnection conn = DABase.GetOpenedConn();
        //        SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_AdminUserSearch);

        //        SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@EmailAddress", emailAddress);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@LastNameOrFirstName", lastNameOrFirstName);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@OrgName", orgName);
        //        SQLUtility.SqlParam_AddAsString(ref cmd, "@CompanyName", companyName);
        //        if (isAdmin.HasValue) SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsAdministrator", isAdmin.Value);
        //        else cmd.Parameters.AddWithValue("@IsAdministrator", DBNull.Value);

        //        return SQLUtility.FillInDataReader(cmd);            
        //}

        public static DataSet SelectByEmailAddressOrUserID(String emailAddressOrUserId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectByEmailAddressOrUserID);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserID", emailAddressOrUserId.ConvertNullToEmptyString().Trim());
                return SQLUtility.FillInDataSet(cmd);
            }
        }

        public static DataSet SelectByGWUserId(Int32 gwUserId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectByGWUserId);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                return SQLUtility.FillInDataSet(cmd);
            }
        }


        public static DataSet Insert(Int32 gwUserId
            , String emailAddress
            , String userLoginId
            , String lastName
            , String firstName
            , Boolean isApproved
            , Boolean isLockedOut
            , DateTime lastLoginUtc
            , DateTime lastActivityUtc
            , DateTime lastLockOutUtc
            , String phoneNumber
            , String faxNumber
            , String jobTitle
            , String userAddressCountryCode
            , String middleName
            , String userStreetAddress1
            , String userStreetAddress2
            , String userCity
            , String userState
            , String userZipCode)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_Insert);
                if (gwUserId < 1) cmd.Parameters.AddWithValue("@GWUserID", DBNull.Value);
                else cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@EmailAddress", HttpUtility.HtmlEncode(emailAddress));
                cmd.Parameters.AddWithValue("@UserID", HttpUtility.HtmlEncode(userLoginId));
                cmd.Parameters.AddWithValue("@LastName", HttpUtility.HtmlEncode(lastName));
                cmd.Parameters.AddWithValue("@FirstName", HttpUtility.HtmlEncode(firstName));
                cmd.Parameters.AddWithValue("@IsApproved", isApproved ? 1 : 0);
                cmd.Parameters.AddWithValue("@IsLockedOut", isLockedOut ? 1 : 0);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastLoginUTC", lastLoginUtc);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastActivityUTC", lastActivityUtc);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastLockOutUTC", lastLockOutUtc);
                cmd.Parameters.AddWithValue("@JobTitle", jobTitle);
                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                cmd.Parameters.AddWithValue("@FaxNumber", faxNumber);
                cmd.Parameters.AddWithValue("@UserAddressCountryCode", userAddressCountryCode);
                cmd.Parameters.AddWithValue("@MiddleName", HttpUtility.HtmlEncode(middleName));
                cmd.Parameters.AddWithValue("@UserStreetAddress1", userStreetAddress1);
                cmd.Parameters.AddWithValue("@UserStreetAddress2", userStreetAddress2);
                cmd.Parameters.AddWithValue("@UserCity", userCity);
                cmd.Parameters.AddWithValue("@UserState", userState);
                cmd.Parameters.AddWithValue("@UserZipCode", userZipCode);

                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) throw new ArgumentException("Unable to save user.");
                Guid membershipId = ConvertUtility.ConvertToGuid(r["MembershipId"], Guid.Empty);
                return SelectByMembershipId(membershipId, conn, null);
            }
        }

        public static Int32 Update(Guid membershipId, Int32 gwUserId, String emailAddress, String userId
            , String lastName, String firstName, Boolean isApproved
            , Boolean isLockedOut, DateTime lastLoginUtc, DateTime lastActivityUtc
            , DateTime lastLockOutUtc
            , String phoneNumber, String faxNumber, String jobTitle, String userAddressCountryCode, String middleName
            , String userStreetAddress1
            , String userStreetAddress2
            , String userCity
            , String userState
            , String userZipCode
            , Boolean isCreateHistory)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_Update);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@EmailAddress", HttpUtility.HtmlEncode(emailAddress));
                cmd.Parameters.AddWithValue("@UserID", HttpUtility.HtmlEncode(userId));
                cmd.Parameters.AddWithValue("@LastName", lastName);
                cmd.Parameters.AddWithValue("@FirstName", firstName);
                cmd.Parameters.AddWithValue("@IsApproved", isApproved ? 1 : 0);
                cmd.Parameters.AddWithValue("@IsLockedOut", isLockedOut ? 1 : 0);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastLoginUTC", lastLoginUtc);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastActivityUTC", lastActivityUtc);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@LastLockOutUTC", lastLockOutUtc);

                cmd.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                cmd.Parameters.AddWithValue("@FaxNumber", faxNumber);
                cmd.Parameters.AddWithValue("@JobTitle", jobTitle);
                cmd.Parameters.AddWithValue("@UserAddressCountryCode", userAddressCountryCode);
                cmd.Parameters.AddWithValue("@MiddleName", HttpUtility.HtmlEncode(middleName));
                cmd.Parameters.AddWithValue("@UserStreetAddress1", userStreetAddress1);
                cmd.Parameters.AddWithValue("@UserStreetAddress2", userStreetAddress2);
                cmd.Parameters.AddWithValue("@UserCity", userCity);
                cmd.Parameters.AddWithValue("@UserState", userState);
                cmd.Parameters.AddWithValue("@UserZipCode", userZipCode);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@CreateHistory", isCreateHistory);
                return cmd.ExecuteNonQuery();
            }
        }

        public static Int32 UpdateToNotify(Guid membershipId, Int32 gwUserId, String emailAddress, String userId
            , String lastName, String firstName, Boolean isApproved
            , Boolean isLockedOut, DateTime lastLoginUtc, DateTime lastActivityUtc
            , DateTime lastLockOutUtc
            , String phoneNumber, String faxNumber, String jobTitle, String userAddressCountryCode, String middleName
            , String userStreetAddress1
            , String userStreetAddress2
            , String userCity
            , String userState
            , String userZipCode
            , Boolean isCreateHistory
           )
        {
            int jpProductCount = 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlTransaction tran = conn.BeginTransaction();
                try
                {
                    Update(membershipId, gwUserId, emailAddress, userId, lastName, firstName, isApproved, isLockedOut, DateTime.UtcNow, lastActivityUtc, lastLockOutUtc, phoneNumber, faxNumber, jobTitle
                     , userAddressCountryCode, middleName, userStreetAddress1, userStreetAddress2, userCity, userState, userZipCode, isCreateHistory);
                    if (!membershipId.IsNullOrEmptyGuid())
                    {
                        //Data.DAAcc_ProductRegistered.SelectRegisteredJPProductByMembershipId(membershipId, out jpProductCount);
                        DataRow rowRegCount = DAAcc_ProductRegistered_Item.SelectRegCount(membershipId, "downloads-jp");
                        if (rowRegCount != null) jpProductCount = ConvertUtility.ConvertToInt32(rowRegCount["RegItemCount"], 0);
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            return jpProductCount;
        }



        public static Int32 UpdateUserNameInEnglish(Guid membershipId, String lastNameinEnglish, String firstNameInEnglish)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_UpdateUserNameInEnglish);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@LastNameInEnglish", lastNameinEnglish);
                cmd.Parameters.AddWithValue("@FirstNameInEnglish", firstNameInEnglish);
                return cmd.ExecuteNonQuery();
            }

        }

        /// <summary>
        /// Returns if email is individual or group email address
        /// </summary>
        /// <param name="membershipId"></param>
        /// <param name="isindividualEmailAddress"></param>
        /// <returns></returns>
        public static Int32 UpdateUserEmailInfo(Guid membershipId, Boolean isindividualEmailAddress)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_UpdateUserEmailInfo);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                cmd.Parameters.AddWithValue("@IsIndividualEmailAddress", isindividualEmailAddress ? 1 : 0);
                return cmd.ExecuteNonQuery();
            }

        }

        public static Int32 UpdateLastActivity(Guid membershipId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_UpdateLastActivity);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static Int32 UpdateUnlock(Guid membershipId, Boolean isLockOut)
        {
            if (membershipId.IsNullOrEmptyGuid()) return 0;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_UpdateUnlock);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@MembershipId", membershipId);
                SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@IsLockedOut", isLockOut);
                return cmd.ExecuteNonQuery();
            }
        }

        public static DataTable SelectByAccountId(Guid accountId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectByAccountID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@AccountID", accountId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectLastActiveUsers(Int32 lastActiveMinutes)
        {
            if (lastActiveMinutes < 1) lastActiveMinutes = 10;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectLastActiveByMinutes);
                cmd.Parameters.AddWithValue("@LastMinute", lastActiveMinutes);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByTeamOwnerId(Guid accountId)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Sec.UserMembership_SelectByTeamOwnerId);
                cmd.Parameters.AddWithValue("@AccountID", accountId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
      
    }
}
