﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASite_Feature
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Feature_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByFeatureID(Int32 featureId)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Feature_SelectByFeatureID);
                cmd.Parameters.AddWithValue("@FeatureID", featureId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectByFeatureKey(Guid featureKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Feature_SelectByFeatureID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@FeatureKey", featureKey);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
    }
}
