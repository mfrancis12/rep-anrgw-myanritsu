﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Anritsu.Connect.Data
{
    public static class DASite_LegacySsoUrl
    {
        public static DataRow SelectBySsoUrlID(Int32 ssoUrlId)
        {
            if (ssoUrlId < 1) return null;
            var db = new DBConnect();
            using (var conn = db.GetOpenedConn())
            {
                var cmd = db.MakeCmdSP(conn, null, db.SQLSP_Site_LegacySsoUrl_SelectBySsoUrlID);
                cmd.Parameters.AddWithValue("@SsoUrlID", ssoUrlId);
                return db.FillInDataRow(cmd);
            }
        }
    }
}
