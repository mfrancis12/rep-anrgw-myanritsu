﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    public static class DASite_Module
    {
        public static DataRow SelectByModuleID(Int32 moduleId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_SelectByModuleID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByFeatureID(Int32 featureId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_SelectByFeatureID);
                cmd.Parameters.AddWithValue("@FeatureID", featureId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static Int32 Insert(Int32 featureId, String moduleName, Boolean showHeader, Boolean noBorder, Boolean isPublic, Boolean hideFromLoggedInUser)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_Insert);
                cmd.Parameters.AddWithValue("@FeatureID", featureId);
                cmd.Parameters.AddWithValue("@ModuleName", moduleName.Trim());
                cmd.Parameters.AddWithValue("@ShowHeader", showHeader ? 1:0);
                cmd.Parameters.AddWithValue("@NoBorder", noBorder ? 1 : 0);
                cmd.Parameters.AddWithValue("@IsPublic", isPublic ? 1 : 0);
                cmd.Parameters.AddWithValue("@HideFromLoggedInUser", hideFromLoggedInUser ? 1 : 0);
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["ModuleID"], 0);
            }
        }

        public static void Update(Int32 moduleId, Int32 featureId, String moduleName, Boolean showHeader, Boolean noBorder, Boolean isPublic, Boolean hideFromLoggedInUser)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_Update);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                cmd.Parameters.AddWithValue("@FeatureID", featureId);
                cmd.Parameters.AddWithValue("@ModuleName", moduleName.Trim());
                cmd.Parameters.AddWithValue("@ShowHeader", showHeader ? 1 : 0);
                cmd.Parameters.AddWithValue("@NoBorder", noBorder ? 1 : 0);
                cmd.Parameters.AddWithValue("@IsPublic", isPublic ? 1 : 0);
                cmd.Parameters.AddWithValue("@HideFromLoggedInUser", hideFromLoggedInUser ? 1 : 0);
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Delete(Int32 moduleId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Module_Delete);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                return cmd.ExecuteNonQuery();
            }
        }

        public static String GetResourceClassKey(Int32 moduleId)
        {
            return String.Format("MDL_{0}", moduleId);
        }
    }
}
