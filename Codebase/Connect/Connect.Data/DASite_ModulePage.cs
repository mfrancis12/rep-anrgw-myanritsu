﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASite_ModulePage
    {
        public static DataTable SelectByPageUrl(String pageUrl)
        {
            if (pageUrl.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModulePage_SelectByPageUrl);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable SelectByModuleID(Int32 moduleId)
        {
            if (moduleId < 1) return null;
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModulePage_SelectByModuleID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static Int32 Insert(Int32 moduleId, String pageUrl, String placeHolder)
        {
            if (moduleId < 1 || String.IsNullOrEmpty(pageUrl) ) throw new ArgumentNullException("Invalid parameters.");
            if (String.IsNullOrEmpty(placeHolder)) placeHolder = "cphContentTop";
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModulePage_Insert);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl);
                cmd.Parameters.AddWithValue("@PlaceHolder", placeHolder.Trim());
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return 0;
                return ConvertUtility.ConvertToInt32(r["PageRefID"], 0);
            }
        }

        public static void Update(Int32 pageRefId, Int32 loadOrder, String placeHolder)
        {
            if (String.IsNullOrEmpty(placeHolder)) placeHolder = "cphContentTop";
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModulePage_Update);
                cmd.Parameters.AddWithValue("@PageRefID", pageRefId);
                cmd.Parameters.AddWithValue("@LoadOrder", loadOrder);
                cmd.Parameters.AddWithValue("@PlaceHolder", placeHolder.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 Delete(Int32 pageRefId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModulePage_Delete);
                cmd.Parameters.AddWithValue("@PageRefID", pageRefId);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
