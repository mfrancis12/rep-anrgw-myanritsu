﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASite_ModuleSetting
    {
        public static DataTable SelectByModuleID(Int32 moduleId)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModuleSetting_SelectByModuleID);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByModuleIDSettingKey(Int32 moduleId, String settingKey)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModuleSetting_SelectByModuleIDSettingKey);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                cmd.Parameters.AddWithValue("@SettingKey", settingKey);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void Save(Int32 moduleId, String settingKey, String settingValue)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModuleSetting_InsertUpdate);
                cmd.Parameters.AddWithValue("@ModuleID", moduleId);
                cmd.Parameters.AddWithValue("@SettingKey", settingKey);
                cmd.Parameters.AddWithValue("@SettingValue", settingValue.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        public static Int32 DeleteByModuleSettingID(Int32 moduleSettingId)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.ModuleSetting_DeleteByModuleSettingID);
                cmd.Parameters.AddWithValue("@ModuleSettingID", moduleSettingId);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
