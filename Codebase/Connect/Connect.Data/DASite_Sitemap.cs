﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DASite_Sitemap
    {
        public static DataTable SelectForBreadCrumbByPageUrl(String pageURL)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.SelectForBreadCrumbByPageUrl);
                cmd.Parameters.AddWithValue("@PageUrl", pageURL.Trim());
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow SelectByPageUrl(String pageURL)
        {
            using (SqlConnection conn = DABase.GetOpenedConn())
            {
                SqlCommand cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Sitemap_SelectByPageUrl);
                cmd.Parameters.AddWithValue("@PageUrl", pageURL.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static Boolean Insert(String pageUrl, String parentPageUrl)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Sitemap_Insert);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl.Trim());
                cmd.Parameters.AddWithValue("@ParentPageUrl", parentPageUrl.Trim());
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public static Boolean Update(String pageUrl, String parentPageUrl)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Sitemap_Update);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl.Trim());
                cmd.Parameters.AddWithValue("@ParentPageUrl", parentPageUrl.Trim());
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public static Int32 Delete(String pageUrl)
        {
            using (var conn = DABase.GetOpenedConn())
            {
                var cmd = DABase.MakeCmdSP(conn, null, DABase.SP.Site.Sitemap_Delete);
                cmd.Parameters.AddWithValue("@PageUrl", pageUrl.Trim());
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
