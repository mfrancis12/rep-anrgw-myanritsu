﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    public static class DA_State
    {
        public static DataTable GetAllStates(string countryValue)
        {
             DBIso db = new DBIso();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBIso.SQL_SP.States_GetAll); 
                cmd.Parameters.AddWithValue("@CountryValue", countryValue);
                DataTable tb = SQLUtility.FillInDataTable(cmd);
                return tb;
            }
        }
    }
}
