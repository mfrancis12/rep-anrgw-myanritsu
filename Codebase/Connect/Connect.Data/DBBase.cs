﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal abstract class DBBase
    {
        internal virtual String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("DB.SpSchema"); }
        }

        internal virtual String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("DB.ConnStr"); }
        }

        internal virtual Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("DB.CmdTimeout"), out timeout);
                return timeout;
            }
        }

        internal virtual SqlConnection GetOpenedConn()
        {
            return SQLUtility.ConnOpen(ConnStr);
        }

        internal virtual SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
        {
            return SQLUtility.MakeSPCmd(conn, tran, SP_Schema, spName, CommandTimeout);
        }

        internal virtual SqlCommand MakeCmdInline(SqlConnection conn, SqlTransaction tran, String sql)
        {
            return SQLUtility.MakeInlineCmd(conn, tran, sql, CommandTimeout);
        }

        internal DataTable FillInDataTable(SqlCommand sqCmd)
        {
            return SQLUtility.FillInDataTable(sqCmd, string.Empty);
        }

        internal DataTable FillInDataTable(SqlCommand sqCmd, string tbName)
        {
            return SQLUtility.FillInDataTable(sqCmd, tbName);
        }

        internal DataRow FillInDataRow(SqlCommand sqCmd)
        {
            return SQLUtility.FillInDataRow(sqCmd);
        }

        internal SqlDataReader FillInDataReader(SqlCommand sqCmd)
        {
            return SQLUtility.FillInDataReader(sqCmd);
        }

        internal SqlDataReader FillInDataReader(SqlCommand sqCmd, CommandBehavior cmdBehavior)
        {
            return SQLUtility.FillInDataReader(sqCmd, cmdBehavior);
        }
    }
}
