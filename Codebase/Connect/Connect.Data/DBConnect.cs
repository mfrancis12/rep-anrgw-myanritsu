﻿using System;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal class DBConnect : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.SPSchema"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout"), out timeout);
                return timeout;
            }
        }

        #region " Stored Procedures "
        public readonly String SQLSP_Erp_ProductConfig_SelectByModelNumber = "[uSP_Erp_ProductConfig_SelectByModelNumber]";
        public readonly String SQLSP_Erp_ProductConfig_SelectWithFilter = "[uSP_Erp_ProductConfig_SelectWithFilter]";
        public readonly String SQLSP_Erp_ProductConfig_SelectAllWithProdOwnerRegionFilter = "[uSP_Erp_ProductConfig_SelectAllWithProdOwnerRegionFilter]";
        public readonly String SQLSP_Erp_ProductConfig_InsertUpdate = "[uSP_Erp_ProductConfig_InsertUpdate]";
        public readonly String SQLSP_Erp_ProductConfig_Delete = "[uSP_Erp_ProductConfig_Delete]";

        public readonly String SQLSP_Erp_ProductConfig_ModelTag_SelectByModelNumber = "[uSP_Erp_ProductConfig_ModelTags_SelectByModelNumber]";
        public readonly String SQLSP_Erp_ProductConfig_ModelTag_Insert = "[uSP_Erp_ProductConfig_ModelTags_Insert]";
        public readonly String SQLSP_Erp_ProductConfig_ModelTag_Delete = "[uSP_Erp_ProductConfig_ModelTags_Delete]";
        public readonly String SQLSP_Erp_ProductConfig_ModelTag_DeleteByModelNumber = "[uSP_Erp_ProductConfig_ModelTags_DeleteByModelNumber]";

        
        public readonly String SQLSP_Acc_ProductRegistered_SelectWithPaging = "[uSP_Acc_ProductRegistered_SelectWithPaging]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectSerialNumbers = "uSP_Acc_ProductRegistered_SelectSerialNumbers";
        public readonly String SQLSP_Acc_ProductRegistered_SelectActiveModels = "uSP_Acc_ProductRegistered_SelectActiveModels";
        public readonly String SQLSP_Acc_ProductRegistered_SelectActive = "uSP_Acc_ProductRegistered_SelectActive";
        public readonly String SQLSP_Acc_ProductRegistered_SelectByAccountID = "[uSP_Acc_ProductRegistered_SelectByAccountID]";
        //public readonly String SQLSP_Acc_ProductRegistered_JP_SelectByMembershipID = "[uSP_Acc_ProductRegistered_JP_SelectByMembershipID]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectActiveByWebAccessKey = "[uSP_Acc_ProductRegistered_SelectActiveByWebAccessKey]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectByWebAccessKeyForAdmin = "[uSP_Acc_ProductRegistered_SelectByWebAccessKeyForAdmin]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectByProdRegID = "[uSP_Acc_ProductRegistered_SelectByProdRegID]";
        public readonly String SQLSP_Acc_ProductRegistered_UpdateUSB = "[uSP_Acc_ProductRegistered_UpdateJapanUSBNo]";
        

        public readonly String SQLSP_Acc_ProductRegistered_Insert = "[uSP_Acc_ProductRegistered_Insert]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectWithFilter = "[uSP_Acc_ProductRegistered_SelectWithFilter]";
        public readonly String SQLSP_Acc_ProductRegistered_SelectNonActive = "[uSP_Acc_ProductRegistered_SelectNonActive]";
        public readonly String SQLSP_Acc_ProductRegistered_Admin_SelectWithFilter = "[uSP_Acc_ProductRegistered_Admin_SelectWithFilter]";
        public readonly String SQLSP_Acc_ProductRegistered_UpdateStatusCode = "[uSP_Acc_ProductRegistered_UpdateStatusCode]";
        public readonly String SQLSP_Acc_ProductRegistered_UpdateStatusCodeByProdRegID = "[uSP_Acc_ProductRegistered_UpdateStatusCodeByProdRegID]";
        public readonly String SQLSP_Acc_ProductRegistered_Update = "[uSP_Acc_ProductRegistered_Update]";
        public readonly String SQLSP_Acc_ProductRegistered_Delete = "[uSP_Acc_ProductRegistered_Delete]";
        public readonly String SQLSP_Acc_ProductRegistered_FindAccountsByUSBNo = "[uSP_Acc_ProductRegistered_FindAccountsByUSBNo]";
        

        public readonly String SQLSP_Acc_ProductRegisteredData_Select = "[uSP_Acc_ProductRegisteredData_Select]";
        public readonly String SQLSP_Acc_ProductRegisteredData_SelectByProdRegID = "[uSP_Acc_ProductRegisteredData_SelectByProdRegID]";
        public readonly String SQLSP_Acc_ProductRegisteredData_Insert = "[uSP_Acc_ProductRegisteredData_Insert]";
        public readonly String SQLSP_Acc_ProductRegisteredData_Update = "[uSP_Acc_ProductRegisteredData_Update]";
        public readonly String SQLSP_Acc_ProductRegisteredData_Delete = "[uSP_Acc_ProductRegisteredData_Delete]";
        public readonly String SQLSP_Acc_ProductRegisteredData_UpdateByDataKey = "[uSP_Acc_ProductRegisteredData_UpdateByDataKey]";

        public readonly String SQLSP_ProdReg_IntModel_SelectByProdRegID = "[uSP_ProdReg_IntModel_SelectByProdRegID]";
        public readonly String SQLSP_ProdReg_IntModel_DeleteByIntMnTagID = "[uSP_ProdReg_IntModel_DeleteByIntMnTagID]";
        public readonly String SQLSP_ProdReg_IntModel_Insert = "[uSP_ProdReg_IntModel_Insert]";
        public readonly String SQLSP_IntModelMaster_SelectAll = "[uSP_IntModelMaster_SelectAll]";

        public readonly String SQLSP_Acc_ProductRegWorkflow_Master_Select = "[usp_Acc_ProductRegWorkflow_Master_Select]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Master_SelectStatus = "[uSP_Acc_ProductRegWorkflow_Master_SelectStatus]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Master_Insert = "[usp_Acc_ProductRegWorkflow_Master_Insert]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Task_Select = "[usp_Acc_ProductRegWorkflow_Task_Select]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Task_SelectByWF = "[usp_Acc_ProductRegWorkflow_Task_SelectByWF]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Task_Insert = "[usp_Acc_ProductRegWorkflow_Task_Insert]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_Task_UpdateAsCompleted = "[usp_Acc_ProductRegWorkflow_Task_UpdateAsCompleted]";
        
        public readonly String SQLSP_Acc_ProductRegWorkflow_TaskData_Insert = "[uSP_Acc_ProductRegWorkflow_TaskData_Insert]";
        public readonly String SQLSP_Acc_ProductRegWorkflow_TaskData_SelectByWFTaskID = "[usp_Acc_ProductRegWorkflow_TaskData_SelectByWFTaskID]";

        public readonly String SQLSP_Erp_ProductOwnerRegion_Select = "[uSP_Erp_ProductOwnerRegion_Select]";
        public readonly String SQLSP_Erp_ProductOwnerRegion_SelectAll = "[uSP_Erp_ProductOwnerRegion_SelectAll]";

        public readonly String SQLSP_Acc_ProductRegCartItemData_SelectByCartItemID = "[uSP_Acc_ProductRegCartItemData_SelectByCartItemID]";
        public readonly String SQLSP_Acc_ProductRegCartItemData_InsertUpdate = "[uSP_Acc_ProductRegCartItemData_InsertUpdate]";
        //public readonly String SQLSP_Acc_ProductRegCartItemData_Update = "[uSP_Acc_ProductRegCartItemData_Update]";
        public readonly String SQLSP_Acc_ProductRegCartItemData_Delete = "[uSP_Acc_ProductRegCartItemData_Delete]";
        public readonly String SQLSP_Acc_ProductRegCartItem_SelectByProdRegCartID = "[uSP_Acc_ProductRegCartItem_SelectByProdRegCartID]";
        public readonly String SQLSP_Acc_ProductRegCartItem_SelectByCartItemID = "[uSP_Acc_ProductRegCartItem_SelectByCartItemID]";
        public readonly String SQLSP_Acc_ProductRegCartItem_Insert = "[uSP_Acc_ProductRegCartItem_Insert]";
        public readonly String SQLSP_Acc_ProductRegCartItem_Delete = "[uSP_Acc_ProductRegCartItem_Delete]";
        public readonly String SQLSP_Acc_ProductRegCartItem_DeleteAllByWebAccessKey = "[uSP_Acc_ProductRegCartItem_DeleteAllByWebAccessKey]";
        public readonly String SQLSP_Acc_ProductRegCartItem_UpdateAddOnFormStatus = "[uSP_Acc_ProductRegCartItem_UpdateAddOnFormStatus]";

        public readonly String SQLSP_Acc_ProductRegCart_SelectByWebAccessKey = "[uSP_Acc_ProductRegCart_SelectByWebAccessKey]";
        public readonly String SQLSP_Acc_ProductRegCart_SelectByAccountID = "[uSP_Acc_ProductRegCart_SelectByAccountID]";
        public readonly String SQLSP_Acc_ProductRegCart_Insert = "[uSP_Acc_ProductRegCart_Insert]";
        public readonly String SQLSP_Acc_ProductRegCart_Update = "[uSP_Acc_ProductRegCart_Update]";
        public readonly String SQLSP_Acc_ProductRegCart_Delete = "[uSP_Acc_ProductRegCart_Delete]";

        public readonly String SQLSP_Acc_ProductRegisteredStatus_SelectAll = "[Acc_ProductRegisteredStatus_SelectAll]";
        public readonly String SQLSP_Acc_ProductRegisteredUserACL_SelectUsers = "[uSP_Acc_ProductRegisteredUserACL_SelectUsers]";
        public readonly String SQLSP_Acc_ProductRegisteredUserACL_Select = "[uSP_Acc_ProductRegisteredUserACL_Select]";
        public readonly String SQLSP_Acc_ProductRegisteredUserACL_Insert = "[uSP_Acc_ProductRegisteredUserACL_Insert]";
        public readonly String SQLSP_Acc_ProductRegisteredUserACL_Delete = "[uSP_Acc_ProductRegisteredUserACL_Delete]";

        public readonly String GW_DownloadRoutingToken_SelectByTokenID = "[uSP_GW_DownloadRoutingToken_SelectByTokenID]";
        public readonly String GW_DownloadRoutingToken_Insert = "[uSP_GW_DownloadRoutingToken_Insert]";
        public readonly String GW_DownloadRoutingToken_UpdateTokenExp = "[uSP_GW_DownloadRoutingToken_UpdateTokenExp]";
        public readonly String GW_DownloadRoutingToken_UpdateForceExpire = "[uSP_GW_DownloadRoutingToken_UpdateForceExpire]";

        internal readonly String SQLSP_Site_LegacySsoUrl_SelectBySsoUrlID = "[uSP_Site_LegacySsoUrl_SelectBySsoUrlID]";

        internal readonly String SQLSP_AccountJPUserRef_SelectByAccountID = "[uSP_Acc_AccountJPUserRef_SelectByAccountID]";
        internal readonly String SQLSP_AccountJPUserRef_Insert = "[uSP_Acc_AccountJPUserRef_Insert]";
        internal readonly String SQLSP_AccountJPUserRef_DeleteByJPUserRefID = "[uSP_Acc_AccountJPUserRef_DeleteByJPUserRefID]";
        internal readonly String SQLSP_AccountJPUserRef_DeleteByAccountID = "[uSP_Acc_AccountJPUserRef_DeleteByAccountID]";

        //internal readonly String SQLSP_JPDongle_Validate = "[uSP_JapanUSBDongle_Validate]";
        internal readonly String SQLSP_JPDongle_Find = "[uSP_JapanUSBDongle_Find]";
        internal readonly String SQLSP_JPDongle_Insert = "[uSP_JapanUSBDongle_Insert]";
        internal readonly String SQLSP_JPDongle_Delete = "[uSP_JapanUSBDongle_Delete]";
        internal readonly String SQLSP_JPDongle_Update = "[uSP_JapanUSBDongle_Update]";

        internal readonly String SQLSP_Acc_ProductRegistered_Master_SelectWithFilter = "[uSP_Acc_ProductRegistered_Master_SelectWithFilter]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_SelectSN = "[uSP_Acc_ProductRegisteredMaster_SelectBySN]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_SelectByWebToken = "[uSP_Acc_ProductRegistered_Master_SelectByWebToken]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_Insert = "[uSP_Acc_ProductRegistered_Master_Insert]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_Update = "[uSP_Acc_ProductRegistered_Master_Update]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_Delete = "[uSP_Acc_ProductRegistered_Master_Delete]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_CheckToDelete = "[uSP_Acc_ProductRegistered_Master_CheckToDelete]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_ForUser_SelectWithPaging = "[uSP_Acc_ProductRegistered_Master_ForUser_SelectWithPaging]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_ForUser_UpdateDesc = "[uSP_Acc_ProductRegistered_Master_ForUser_Update]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_SelectByAccMnSn = "[uSP_Acc_ProductRegistered_Master_SelectByAccMnSn]";
        internal readonly String SQLSP_Acc_ProductRegistered_Master_Delete_CheckItems = "[uSP_Acc_ProductRegistered_Master_Delete_CheckItems]";

        internal readonly String SQLSP_Acc_ProductRegistered_Item_Insert = "[uSP_Acc_ProductRegistered_Item_Insert]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_Update = "[uSP_Acc_ProductRegistered_Item_Update]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_Delete = "[uSP_Acc_ProductRegistered_Item_Delete]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_SelectWithFilter = "[uSP_Acc_ProductRegistered_Item_SelectWithFilter]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_SelectByAccountID = "[uSP_Acc_ProductRegistered_Item_SelectByAccountID]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_SelectByProdRegItemId = "[uSP_Acc_ProductRegistered_Item_SelectByProdRegItemId]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_JPExportType_SelectAll = "[uSP_Acc_ProductRegistered_Item_JPExportType_SelectAll]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_ForJPSW_SelectByWebToken = "[uSP_Acc_ProductRegistered_Item_ForJPSW_SelectByWebToken]";
        internal readonly String SQLSP_Acc_ProductRegistered_ItemConfig_JPDL_Insert = "[uSP_Acc_ProductRegistered_ItemConfig_JPDL_Insert]";
        internal readonly String SQLSP_Acc_ProductRegistered_ItemConfig_JPDL_Update = "[uSP_Acc_ProductRegistered_ItemConfig_JPDL_Update]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_ForUser_SelectByWebToken = "[uSP_Acc_ProductRegistered_Item_ForUser_SelectByWebToken]";

        internal readonly String SQLSP_Acc_ProductRegistered_Item_ForJPSW_Update =
            "[uSP_Acc_ProductRegistered_Item_ForJPSW_Update]";
        internal readonly String SQLSP_Acc_ProductRegistered_Item_ForJPSW_Delete =
            "[uSP_Acc_ProductRegistered_Item_ForJPSW_Delete]";

        internal readonly String SQLSP_Acc_ProductRegistered_Deletions_Check = "[uSP_Acc_ProductRegistered_Deletions_Check]";        

        #endregion
        #region Model Filter Sp's
        internal readonly String SQLSP_ProductModelFilter_SelectByTeamIds = "[uSP_Filters_ModelFilters_Select]";
        #endregion
        internal readonly String SQLSP_Acc_ProductRegistered_Master_SelectByModelAndTeams = "[uSP_Acc_ProductRegistered_Master_SelectByModelAndTeams]";

        internal class SP_ProdReg_Master
        {
            public const String SelectModelSerial = "[uSP_Acc_ProductRegistered_Master_SelectModelSerial]";
            public const String ForUser_Select = "[uSP_Acc_ProductRegistered_Master_ForUser_Select]";
			public const String SelectRegisteredProducts = "[uSP_Acc_UserRegisteredProducts]";
        }

        internal class SP_ProdReg_Item
        {
            public const String SelectByWebToken = "[uSP_Acc_ProductRegistered_Item_SelectByWebToken]";
            public const String SelectRegCount = "[uSP_Acc_ProductRegistered_Item_SelectRegCount]";
            public const String UpdateStatus = "[uSP_Acc_ProductRegistered_Item_UpdateStatus]";
            public const String SelectByModelAndTeams = "[uSP_Acc_ProductRegistered_Item_SelectByModelAndTeams]";
        }

        internal class SP_ProdReg_Item_JPUSB
        {
            public const String SelectByUSBKey = "[uSP_Acc_ProductRegistered_Item_JPUSB_SelectByUSBKey]";
            
        }

        internal class SP_ProdReg_Item_Status
        {
            public const String SelectAll = "[uSP_Acc_ProductRegistered_Item_Status_SelectAll]";
        }

        internal class SP_ProdReg_ItemConfig
        {
            public const String JPDL_Select = "[uSP_Acc_ProductRegistered_ItemConfig_JPDL_Select]";
        }

        internal class SP_ProdReg_Data
        {
            public const String Select = "[uSP_Acc_ProductRegistered_Data_Select]";
            public const String SelectByProdRegID = "[uSP_Acc_ProductRegistered_Data_SelectByProdRegID]";
            public const String SelectByWebToken = "[uSP_Acc_ProductRegistered_Data_SelectByWebToken]";
            public const String Insert = "[uSP_Acc_ProductRegistered_Data_Insert]";
            public const String Update = "[uSP_Acc_ProductRegistered_Data_Update]";
            public const String InsertUpdate = "[uSP_Acc_ProductRegistered_Data_InsertUpdate]";
            public const String Delete = "[uSP_Acc_ProductRegistered_Data_Delete]";
            public const String UpdateByDataKey = "[uSP_Acc_ProductRegistered_Data_UpdateByDataKey]";
            public const String UpdateSupportEntryMappingRenew = "[Usp_UpdateSupportEntryMapping_Renew]";
        }

        internal class SP_ModelMaster
        {
            public const String SelectByModel = "[uSP_Cfg_ModelMaster_SelectByModel]";
            //public const String update = "[uSP_Cfg_ModelMaster_UpdateModelConfg]"; TO BE DELETED
            public const String InsertUpdate = "[uSP_Cfg_ModelMaster_InsertUpdateModelConfg]";
        }

        internal class SP_ModelConfig
        {
            public const String SelectByModel = "[uSP_Cfg_ModelConfig_SelectByModel]";
            public const String SelectAllActiveByModel = "[uSP_Cfg_ModelConfig_SelectAllActiveByModel]";
            public const String GetByModelConfgType = "[uSP_Cfg_ModelConfig_SelectByModelConfgType]";
            public const String InsertUpdateModelConfig = "[uSP_Cfg_ModelConfig_InsertUpdateModelConfg]";
           // public const String UpdateModelConfig = "[uSP_Cfg_ModelConfig_UpdateModelConfg]";
        }

        internal class SP_ModelConfigType
        {
            public const String SelectAll = "[uSP_Cfg_ModelConfigType_SelectAll]";
        }

        internal class SP_ModelConfigTypeStatus
        {
            public const String SelectAll = "[uSP_Cfg_ModelConfigStatus_SelectAll]";
        }

        internal class SP_ModelTags
        {
            public const String SelectByModel = "[uSP_Cfg_ModelTags_SelectByModel]";
            public const String Insert = "[uSP_Cfg_ModelTags_Insert]";
            public const String Delete = "[uSP_Cfg_ModelTags_Delete]";
            public const String DeleteByModelNumber = "[uSP_Cfg_ModelTags_DeleteByModelNumber]";
        }

        internal class SP_Account_Config_DistPortal
        {
            public const String Insert = "[uSP_Acc_Account_Config_DistPortal_Insert]";
            public const String Update = "[uSP_Acc_Account_Config_DistPortal_Update]";
            public const String Select = "[uSP_Acc_Account_Config_DistPortal_Select]";
        }

        internal class SP_Account_Config_DistPortal_Status
        {
            public const String SelectAll = "[uSP_Acc_Account_Config_DistPortal_Status_SelectAll]";
        }

        internal class SP_PID_FavoriteProducts
        {
            public const String SelectByAccountID = "[uSP_PID_FavoriteProducts_SelectByAccountID]";
            public const String Insert = "[uSP_PID_FavoriteProducts_Insert]";
            public const String Delete = "[uSP_PID_FavoriteProducts_Delete]";
        }

        internal class SP_PID_ProdFilter
        {
            public const String SelectAll = "[uSP_Acc_Account_Config_DistPortal_ProdFilter_SelectAll]";
            public const String SelectByFilterID = "[uSP_Acc_Account_Config_DistPortal_ProdFilter_SelectByProdFilterID]";
            public const String Insert = "[uSP_Acc_Account_Config_DistPortal_ProdFilter_Insert]";
            public const String Update = "[uSP_Acc_Account_Config_DistPortal_ProdFilter_Update]";
            public const String Delete = "[uSP_Acc_Account_Config_DistPortal_ProdFilter_Delete]";

        }

        internal class SP_PID_ProdFilterItem
        {
            public const String Insert = "[uSP_Acc_Account_Config_DistPortal_ProdFilterItem_Insert]";
            public const String DeleteByProdFilterItemID = "[uSP_Acc_Account_Config_DistPortal_ProdFilterItem_DeleteByProdFilterItemID]";
            public const String DeleteByProdFilterID = "[uSP_Acc_Account_Config_DistPortal_ProdFilterItem_DeleteByProdFilterID]";
            public const String SelectByProdFilterID = "[uSP_Acc_Account_Config_DistPortal_ProdFilterItem_SelectByProdFilterID]";
        }

        internal class SP_PID_DocTypeFilter
        {
            public const String SelectAll = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilter_SelectAll]";
            public const String SelectByFilterID = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilter_SelectByDocTypeFilterID]";
            public const String Insert = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilter_Insert]";
            public const String Update = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilter_Update]";
            public const String Delete = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilter_Delete]";

        }

        internal class SP_PID_DocTypeFilterItem
        {
            public const String Insert = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilterItem_Insert]";
            public const String DeleteByDocTypeFilterItemID = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilterItem_DeleteByDocTypeFilterItemID]";
            public const String DeleteByDocTypeFilterID = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilterItem_DeleteByDocTypeFilterID]";
            public const String SelectByDocTypeFilterID = "[uSP_Acc_Account_Config_DistPortal_DocTypeFilterItem_SelectByDocTypeFilterID]";
        }

        internal class SP_CustomLink
        {
            public const String Master_SelectAll = "[uSP_CustomLink_Master_SelectAll]";
            public const String Master_Insert = "[uSP_CustomLink_Master_Insert]";
            public const String Master_Update = "[uSP_CustomLink_Master_Update]";
            public const String Master_Delete = "[uSP_CustomLink_Master_Delete]";

            public const String DisplayContainer_SelectAll = "[uSP_CustomLink_DisplayContainer_SelectAll]";
            public const String OwnerRoles_SelectAll = "[uSP_CustomLink_OwerRoles_SelectAll]";

            public const String RefAccount_SelectByCustomLinkID = "[uSP_CustomLink_AccountRef_SelectByCustomLinkID]";
            public const String RefAccount_Insert = "[uSP_CustomLink_AccountRef_Insert]";
            public const String RefAccount_DeleteByCustomLinkAccRefID = "[uSP_CustomLink_AccountRef_DeleteByCustomLinkAccRefID]";
            public const String RefAccount_SetShowForAll = "[uSP_CustomLink_AccountRef_SetShowForAll]";

            public const String RefSisProd_SelectByCustomLinkID = "[uSP_CustomLink_RefSisProd_SelectByCustomLinkID]";
            public const String RefSisProd_Insert = "[uSP_CustomLink_RefSisProd_Insert]";
            public const String RefSisProd_DeleteByRefSisProdID = "[uSP_CustomLink_RefSisProd_DeleteByRefSisProdID]";
            public const String RefSisProd_SetShowForAll = "[uSP_CustomLink_RefSisProd_SetShowForAll]";

            //below to be deleted later
            public const String Admin_SelectAll = "[uSP_LnkAdmin_SelectAll]";
            public const String Display_Select = "[uSP_Lnk_LinkSelect]";

            public const String LinkLocationType_SelectAll = "[uSP_Lnk_LocationType_SelectAll]";
            public const String LinkMaster_SelectAll = "[uSP_Lnk_LinkMaster_SelectAll]";
            public const String LinkMaster_SelectByLnkID = "[uSP_Lnk_LinkMaster_SelectByLnkID]";
            public const String LinkSettings_SelectByLnkID = "[uSP_Lnk_LinkSetting_SelectByLnkID]";
            public const String LinkConfig_SelectByLnkID = "[uSP_Lnk_LinkConfig_SelectByLnkID]";
        }

        internal class SP_ProductFilterFunction
        {
            public const String SelectByModelConfigId = "[uSP_ProductFilterFunction_SelectByModelConfigID]";
            public const String SelectByFilterKeywords = "[uSP_ProductFilterFunction_SelectByFilterKeywords]";
            public const String Insert = "[uSP_ProductFilterFunction_Insert]";
            public const String Delete = "[uSP_ProductFilterFunction_Delete]";

        }

        internal class SP_AdminProductFilterFunction
        {
            public const String Insert = "[uSP_AdminProductFilters_Insert]";
            public const String Delete = "[uSP_AdminProductFilters_Update_DeleteStatus]";

        }

        
    }
}
