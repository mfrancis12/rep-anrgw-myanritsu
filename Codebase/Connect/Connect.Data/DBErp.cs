﻿using System;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal class DBPIM : DBBase
    {
        internal override String ConnStr
        {
            get { return ConfigUtility.AppSettingGetValue("PIMConnectionString"); }
        }
        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout.Erp"), out timeout);
                return timeout;
            }
        }
    }
    internal class DBErp : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.SpSchema.Erp"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr.Erp"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout.Erp"), out timeout);
                return timeout;
            }
        }

        public class SQL_Inline_TBD
        {
            public static String FindProductByModelSerialNumber
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT * FROM [ERP].[GWTY_ProductInfo] (NOLOCK) ");
                    sb.Append("WHERE [ModelNumber] = @ModelNumber AND [SerialNumber] = @SerialNumber ");
                    return sb.ToString();
                }
            }

            public static String FindProductWarrantyByModelNumber
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT w.*, wt.* FROM ERP.[GWTY_WarrantyInfo] w (NOLOCK) ");
                    sb.Append("INNER JOIN ERP.[GWTY_ProductInfo] p (NOLOCK) ON p.[SNId] = w.[SNId] ");
                    sb.Append("INNER JOIN [ERP].[GWTY_WarrantyType] wt (NOLOCK) ON w.[WarrantyTypeId] = wt.[WarrantyTypeId] ");
                    sb.Append("WHERE p.[ModelNumber] = @ModelNumber AND p.[SerialNumber] = @SerialNumber ");
                    sb.Append("AND [InternalCode] <> 'SVC 90' ");
                    return sb.ToString();
                }
            }

            public static String FindModelSerialNumberInGlobalSerialNumberSystem
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT * FROM [ERP].[GlobalSN_ManufaSrc] (NOLOCK) ");
                    sb.Append("WHERE [ModelNumber] = @ModelNumber AND [SerialNumber] = @SerialNumber ");
                    return sb.ToString();
                }
            }
        }

        public class SQL_SP
        {
            public const String FindProductByModelSerialNumber = "[uSP_Connect_GWTY_ProductInfo_SelectByMNSN]";
            public const String FindProductWarrantyByModelNumber = "[uSP_Connect_GWTY_WarrantyInfo_SelectByMNSN]";
            public const String FindModelSerialNumberInGlobalSerialNumberSystem = "[uSP_Connect_GlobalSN_ManufaSrc_SelectByMNSN]";
            public const String GlobalSN_ManufaSrc_Search = "[uSP_Connect_GlobalSN_ManufaSrc_Search]";
            public const String GlobalSN_ManufaSrc_SearchByMN = "[uSP_Connect_GlobalSN_ManufaSrc_SearchSNByMN]";
            public const String FindAllProductInfo = "[uSP_Connect_GWTY_ProductInfo_SelectAll]";
            public const String GetCMSProductBasicInfo = "[uSP_GlobalWeb_Get_ProductInfo]";
            public const String GetCMSDownloadInfo = "[uSP_GlobalWeb_Get_DownloadInfo]";
        }
    }
}
