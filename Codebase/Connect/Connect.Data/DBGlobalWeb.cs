﻿using System;
using System.Text;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Data
{
    internal class DBGlobalWeb : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.SpSchema.GW"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr.GW"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout.GW"), out timeout);
                return timeout;
            }
        }

        public class SQL_Inline
        {


            public static String FindProductByModelNumber
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT *, SerialNumber = @SerialNumber FROM [dbo].[AR_Products] (NOLOCK) p ");
                    sb.Append("INNER JOIN [dbo].[AR_Products_Culture] (NOLOCK) pc ON p.[ProductId] = pc.[ProductId] AND pc.[CultureGroupId] = @CultureGroupId ");
                    sb.Append("WHERE [ModelNumber] = @ModelNumber ");
                    return sb.ToString();
                }
            }

            public static String GWDownload_SelectByModel
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT dp.[DownloadID], dcd.[CategoryID], dcc.[CategoryName], dc.[Title], p.[ModelNumber] ");
                    sb.Append(", dbo.uFN_GWDownload_GetDownloadLandingPageURL(dp.[DownloadID],@CultureGroupID) AS 'DownloadURL' ");
                    sb.Append(", d.[SizeKB], d.[Version], d.[ReleaseDateUTC], d.[FilePath], d.[ModifiedOnUTC], d.[CreatedOnUTC] ");
                    sb.Append("FROM AR_Download_Product (NOLOCK) dp ");
                    sb.Append("INNER JOIN AR_Products (NOLOCK) p ON dp.[ProductID] = p.[ProductID] ");
                    sb.Append("INNER JOIN AR_Download (NOLOCK) d ON dp.[DownloadID] = d.[DownloadID] AND d.[PubliclyVisible] = 1 ");
                    sb.Append("INNER JOIN AR_Download_Culture (NOLOCK) dc ON dp.[DownloadID] = dc.[DownloadID] AND dc.[CultureGroupId] = @CultureGroupId ");
                    sb.Append("INNER JOIN AR_Download_Category_Download (NOLOCK) dcd ON dp.[DownloadID] = dcd.[DownloadID] ");
                    sb.Append("INNER JOIN AR_Download_Categories_Culture (NOLOCK) dcc ON dcd.[CategoryID] = dcc.[CategoryID] AND dcc.[CultureGroupID] = @CultureGroupID ");
                    
                    //this is commented out for Phase 1 Go LIVE.  When JPDL is ready, we should update this
                    sb.Append("WHERE p.[ModelNumber] = @ModelNumber AND d.[FilePath] NOT LIKE '%.asp%'");
                    //sb.Append("WHERE p.[ModelNumber] = @ModelNumber");
                    return sb.ToString();
                }
            }
        }

        public class SQL_SP
        {
            public const String AR_Resources_GetCultureGroupID = "[uSP_AR_Resources_GetCultureGroupID]";

            internal class CalCert
            {
                public const string Calibration_Certificates_Search_Get = "[uSP_Repair_Calibration_Certificates_Search_Get]";
                public const string Calibration_Certificate_Download = "[uSP_Repair_Calibration_Certificates_DownloadCert_Get]";
            }

            internal class Sql_SSOSp
            {
                public const String User_SelectByEmailOrUserName = "[uSP_GW_SSO_Saml_User_SelectByEmailOrUserName]";
            }
        }
    }
}
