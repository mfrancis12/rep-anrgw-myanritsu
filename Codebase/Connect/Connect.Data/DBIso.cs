﻿using System;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal class DBIso : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.ISO.SPSchema"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ISO.ConnStr"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.ISO.CmdTimeout"), out timeout);
                return timeout;
            }
        }

        public static class SQL_Inline
        {
            public static String ISO_Country_Select
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT * FROM [dbo].[AR_ISO_Country] (NOLOCK)");
                    return sb.ToString();
                }
            }

            public static String ISO_CountryCultureGroup
            {
                get
                {
                    var sb = new StringBuilder();
                    sb.Append("SELECT cm.CultureGroupId, cm.CultureCode, c.Alpha2, c.ISOCountryName ");
	                sb.Append("FROM [dbo].[AR_ISO_Country] c (NOLOCK) ");
	                sb.Append("INNER JOIN [dbo].[AR_AppResources_CultureGroupMaster] cgm (NOLOCK) ON c.[DefaultCultureGroupId] = cgm.[CultureGroupId] ");
                    sb.Append("INNER JOIN [dbo].[AR_AppResources_CultureMaster] cm (NOLOCK) ON cgm.[DefaultCultureId] = cm.[CultureId] ");
                    return sb.ToString();
                }
            }
        }

        public static class SQL_SP
        {
            public const String GetCultureGroupID = "[uSP_AR_Resources_CultureGroupID_GetIfExists]";
            public const String AR_ISO_Country_Select_ByAlpha2Code = "[uSP_AR_ISO_Country_Select_ByAlpha2Code]";
            public const String States_GetAll = "[uSP_ContactUs_States_Get]";
            public const string CultureGroups_GetGroupCode = "[uSP_AR_Resources_GetCultureGroupCode]";
        }
    }
}
