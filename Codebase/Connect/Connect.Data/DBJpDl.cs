﻿using System;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal class DBJpDl : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.SpSchema.JPDL"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr.JPDL"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout.JPDL"), out timeout);
                return timeout;
            }
        }

        public class SQL_Inline
        {


            //public static String JapanDownload_SelectObjectByModel
            //{
            //    get
            //    {
            //        StringBuilder sql = new StringBuilder();
            //        sql.Append("SELECT wo.*, wc.Comment, wc.CommentEng FROM [dbo].[W2_Object] wo (NOLOCK) ");
            //        sql.Append("LEFT OUTER JOIN [dbo].[W2_Comment] wc (NOLOCK) ON wo.[CommentID] = wc.[ID] ");
            //        sql.Append("WHERE wo.[ReleaseDate] <= GETUTCDATE() AND wo.[Target] NOT IN ('YG', 'MG') AND wo.[ModelName] = @ModelName");
            //        return sql.ToString();
            //    }
            //}

            //public static String JapanDownload_SelectVersionByModel
            //{
            //    get
            //    {
            //        return "SELECT * FROM [dbo].[W2_VersionFile] (NOLOCK) WHERE [ServiceType] = 0 AND [ModelName] = @ModelName";
            //    }
            //}
        }

        public class SQL_SP
        {
            public const String SelectObjectByModelv1 = "[uSP_Connect_SelectObjectByModel]";
            public const String SelectObjectByModelv2 = "[uSP_Connect_SelectObjectByModelv2]";
            public const String SelectVersionByModel = "[uSP_Connect_SelectVersionByModel]";
            public const String Login_SelectAll = "[uSP_Connect_Login_SelectAll]";
            public const String Dongle_SelectObjectByDonglev1 = "[uSP_Connect_SelectObjectByDongle]";
            public const String Dongle_SelectObjectByDonglev2 = "[uSP_Connect_SelectObjectByDonglev2]";
            public const String Select_ObjectUpdateByModel = "[uSP_Connect_Select_ObjectUpdateByModel]";
        }
    }
}
