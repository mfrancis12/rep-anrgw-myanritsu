﻿using System;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Data
{
    internal class DBLog : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("Connect.DB.SpSchema.AppLog"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr.AppLog"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout;
                int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout.AppLog"), out timeout);
                return timeout;
            }
        }
       

        public class SQL_SP
        {
            public const String ExceptionLog_Add = "[uSP_ExceptionLog_Add]";
        }
    }
}
