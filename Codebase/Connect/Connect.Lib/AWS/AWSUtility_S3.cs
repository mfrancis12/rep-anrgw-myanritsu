﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.AnrCommon.CoreLib;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using Amazon.CloudFront;
using Amazon;
using Amazon.S3.Transfer;
using System.Web;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.AWS
{
    public class AWSUtility_S3
    {
        private String _AWSS3_AccessKey;
        private String _AWSS3_SecretKey;

        private Int32 _AWSS3_SignedUrlExpInMinutes = 1440;//1day

        private String AWSS3_AccessKey
        {
            get
            {
                return _AWSS3_AccessKey;
            }
        }

        private String AWSS3_SecretKey
        {
            get
            {
                return _AWSS3_SecretKey;
            }
        }

        public Int32 AWSS3_SignedUrlExpInMinutes
        {
            get
            {
                return _AWSS3_SignedUrlExpInMinutes;
            }
        }
        private RegionEndpoint _AWS_S3_Endpoint;
        private TransferUtility _AWS_S3_TransferUtil;
        private AmazonS3Client _AWS_S3_Client;
        private String _AWS_AccessKey;
        private String _AWS_SecretKey;
        private Int32 _AWS_S3_Timeout;
        private String _AWS_S3_EndpointName;
        private String _AWS_S3_BucketName;
        private String _AWS_S3_UploadPath;
        private Boolean _AWS_CF_Enabled = false;
        private String _AWS_CF_DistributionID;
        private Int32 _AWS_CF_MaxAgeSeconds;
        public AWSUtility_S3(String s3BucketConfigPrefix)
        {
            InitConfig(s3BucketConfigPrefix);

         
        }
        private void InitConfig(String s3BucketConfigPrefix)
        {
            _AWSS3_AccessKey = ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.AWSAccessKey", s3BucketConfigPrefix));
            _AWSS3_SecretKey = ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.AWSSecretKey", s3BucketConfigPrefix));

            _AWSS3_SignedUrlExpInMinutes = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.Bucket.SignedUrlExpInMinutes", s3BucketConfigPrefix)), 1440);
            ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.EndPoint", s3BucketConfigPrefix));


            _AWS_AccessKey = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.AWSAccessKey");
            _AWS_SecretKey = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.AWSSecretKey");
            _AWS_S3_Timeout = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("AWS.S3.Timeout"), 5);
            _AWS_S3_EndpointName = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.EU.LocationConstraint");
            _AWS_S3_BucketName = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.EU.BucketName");
            _AWS_S3_UploadPath = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.EU.AssistantFileDownloadPath");
            _AWS_CF_Enabled = ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("AWS.CF.Enable"), false);
            _AWS_CF_DistributionID = ConfigUtility.AppSettingGetValue("AWS.CF.DistributionID");
            _AWS_CF_MaxAgeSeconds = ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("AWS.CF.MaxAgeSeconds"), 7776000);

            _AWS_S3_Endpoint = RegionEndpoint.GetBySystemName(_AWS_S3_EndpointName);
            _AWS_S3_TransferUtil = new TransferUtility(_AWS_AccessKey, _AWS_SecretKey, _AWS_S3_Endpoint);
            _AWS_S3_Client = new AmazonS3Client(_AWS_AccessKey, _AWS_SecretKey, _AWS_S3_Endpoint);

        }
        public String AWS_S3_UploadFile(Byte[] fileData, String fileName, String friendlyFileName, String fileType)
        {
            String awsS3ObjectName = String.Empty;
            if (fileData == null || fileData.Length < 1) return awsS3ObjectName;
            using (MemoryStream ms = new MemoryStream(fileData))
            {
                awsS3ObjectName = AWS_S3_UploadFile(ms, fileName, friendlyFileName, fileType);
            }
            return awsS3ObjectName;
        }

        public String AWS_S3_UploadFile(Stream inputStream, String fileName, String friendlyFileName, String fileType)
        {
            try
            {
                if (inputStream == null || friendlyFileName.IsNullOrEmptyString() || fileType.IsNullOrEmptyString()) return String.Empty;

                String prefix = _AWS_S3_UploadPath;
                String bucket = _AWS_S3_BucketName;
                String awsS3ObjectName =  AWS_S3_GetAwsS3ObjectName(fileName, prefix);

                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
              
                request.BucketName = bucket;
                request.ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256;
                request.Key = awsS3ObjectName;// awsS3ObjectName.ToLowerInvariant();
                request.ContentType = fileType;
                inputStream.Seek(0, SeekOrigin.Begin);
                request.InputStream = inputStream;

             
                string dlmode = "attachment";
                String fileExt = Path.GetExtension(friendlyFileName);
               
               
                String contentDispoFileName = friendlyFileName.ToLower();
                request.Headers.ContentDisposition = String.Format("{0};filename={1};", dlmode, contentDispoFileName);
               
                request.Headers.CacheControl = String.Format("max-age={0},must-revalidate", _AWS_CF_MaxAgeSeconds);
              
                _AWS_S3_TransferUtil.Upload(request);

                return awsS3ObjectName;
            }
            catch (Exception ex)
            {
              throw ex;
            }

        }
        private string AWS_S3_GetAwsS3ObjectName(String targetFileName, String s3FolderPrefix)
        {
            String awsS3ObjectName = targetFileName.Replace("\\", "/").ToLowerInvariant();
            if (awsS3ObjectName.StartsWith("/")) awsS3ObjectName = awsS3ObjectName.Substring(1);

            if (!String.IsNullOrEmpty(s3FolderPrefix))
            {
                awsS3ObjectName = String.Format("{0}{1}", s3FolderPrefix, awsS3ObjectName);
            }
            return awsS3ObjectName;
        }
       

       
       

        public String GeneratePreSignedURL(String objectKeyName, String awsS3KeyPrefix, String awsRegionConfigCode)
        {
            DateTime expDate = DateTime.UtcNow.AddMinutes(AWSS3_SignedUrlExpInMinutes);
            return GeneratePreSignedURL(objectKeyName, awsS3KeyPrefix, expDate, awsRegionConfigCode);
        }

        public String GeneratePreSignedURL(String objectKeyName, String awsS3KeyPrefix, DateTime expDate, String awsRegionConfigCode)
        {
            String url = String.Empty;
            String awsS3ObjectName = GetAwsS3ObjectName(objectKeyName, awsS3KeyPrefix);
            String configType = "DL1";
            String configRegionCode = awsRegionConfigCode.ToUpperInvariant();
            String bucketName = ReadS3BucketConfig(configType, configRegionCode, "BucketName");
            String signedUrlHostToReplace = ReadS3BucketConfig(configType, configRegionCode, "SignedUrlHostToReplace");
            String signedUrlHostToReplaceWith = ReadS3BucketConfig(configType, configRegionCode, "SignedUrlHostToReplaceWith");

            using (AmazonS3Client client = GetS3Client(configType, awsRegionConfigCode))
            {
                GetPreSignedUrlRequest req = new GetPreSignedUrlRequest();
                req.BucketName = bucketName;
                //req.WithBucketName(bucketName);
                req.Key = awsS3ObjectName;
                //req.WithKey(awsS3ObjectName);
                req.Expires = expDate;
                //req.WithExpires(expDate);
                req.Protocol=Protocol.HTTP;
                //req.WithProtocol(Protocol.HTTP);
                url = client.GetPreSignedURL(req);
                url = url.Replace(signedUrlHostToReplace, signedUrlHostToReplaceWith);
            }
            return url;
        }

        private String GetAwsS3ObjectName(String targetFileName, String keyprefix)
        {
            String awsS3ObjectName = targetFileName;
            if (targetFileName.StartsWith("/")) awsS3ObjectName = targetFileName.Substring(1);

            if (!String.IsNullOrEmpty(keyprefix))
            {
                awsS3ObjectName = String.Format("{0}{1}", keyprefix, targetFileName);
            }
            return awsS3ObjectName.ToLowerInvariant();
        }

        private AmazonS3Client GetS3Client(String type, String regionCfgKey)
        {

            AmazonS3Client s3Client;
            if (String.IsNullOrEmpty(type) || regionCfgKey.IsNullOrEmptyString())
            {
                s3Client = new AmazonS3Client(AWSS3_AccessKey, AWSS3_SecretKey);
            }
            else
            {
                Amazon.RegionEndpoint regionEndpoint = Amazon.RegionEndpoint.GetBySystemName(ReadS3BucketConfig(type, regionCfgKey, "LocationConstraint"));
                s3Client = new AmazonS3Client(AWSS3_AccessKey, AWSS3_SecretKey, regionEndpoint);
            }
            return s3Client;
        }

        public static String ReadS3BucketConfig(String type, String regionCfgKey, String parmKey)
        {
            return ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.{1}.{2}", type, regionCfgKey, parmKey));
        }

        public static String ReadS3BucketConfig(String type, String parmKey)
        {
            return ConfigUtility.AppSettingGetValue(String.Format("{0}.AWS.S3.{1}", type, parmKey));
        }


        public List<S3Object> GetS3Objects(String type, String regionCfgKey, String prefix, String delimeter)
        {
            List<S3Object> resources = new List<S3Object>();
            string _bucketName = ReadS3BucketConfig(type, regionCfgKey, "BucketName");
            AmazonS3Client s3Client=GetS3Client(type,regionCfgKey);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = _bucketName;
            if (!String.IsNullOrEmpty(delimeter))
                request.Delimiter = "/";
            //request.WithDelimiter("/");
            request.Prefix = prefix + "/";
            //request.WithPrefix(prefix + "/");
            request.MaxKeys = 10000;
            //request.WithMaxKeys(10000);
            do
            {
                  ListObjectsResponse response = s3Client.ListObjects(request);
                   resources.AddRange(response.S3Objects.Where(
                    res =>
                    {
                        try { return res.Size > 0 && !String.IsNullOrEmpty(Path.GetExtension(res.Key)); }
                        catch (Exception ex)
                        {
                            if (ex.HResult == 
                            int.Parse(ConfigUtility.AppSettingGetValue("Connect.Web.IllegalCharactersExceptionHResult")))
                            {
                                throw new ArgumentException(res.Key);
                            }
                            else { throw; }
                        }
                    }
                    ));
                  if (response.IsTruncated)
                  {
                      request.Marker = response.NextMarker;
                  }
                  else
                  {
                      request = null;
                  }
            } while (request != null);
            return resources;
        }
        public S3ObjectsWithPrefixes GetS3ObjectsWithPrefixes(String prefix, String delimeter, String type, String regionCfgKey)
        {

            List<S3Object> content = new List<S3Object>();
            List<string> prefixes = new List<string>();

            if (!prefix.EndsWith("/"))
                prefix = prefix + "/";
            ListObjectsRequest request = new ListObjectsRequest();

            request.BucketName = ReadS3BucketConfig(type, regionCfgKey, "BucketName");
            request.Delimiter = delimeter;
            request.Prefix = prefix;//.Replace("&", "amp;");
            request.MaxKeys = 10000;

            do
            {
                //CommonPrefixes
                AmazonS3Client s3Client = GetS3Client(type, regionCfgKey);

                ListObjectsResponse response = s3Client.ListObjects(request);
                content.AddRange(response.S3Objects);
                prefixes.AddRange(response.CommonPrefixes);

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
            S3ObjectsWithPrefixes s3objwithPrefixes = new S3ObjectsWithPrefixes() { CommonPrefixes = prefixes, Content = content };
            return s3objwithPrefixes;
        }
        public String GenerateTAUDLPreSignedURL(String objectKeyName, String configType, String regionCfgKey)
        {
            DateTime expDate = DateTime.UtcNow.AddMinutes(AWSS3_SignedUrlExpInMinutes);
            String url = String.Empty;


            String bucketName = ReadS3BucketConfig(configType, regionCfgKey, "BucketName");
            String signedUrlHostToReplace = ReadS3BucketConfig(configType, "SignedUrlHostToReplace");
            String signedUrlHostToReplaceWith = ReadS3BucketConfig(configType, "SignedUrlHostToReplaceWith");
            using (AmazonS3Client client = GetS3Client(configType,regionCfgKey))
            {
                GetPreSignedUrlRequest req = new GetPreSignedUrlRequest();
                req.BucketName = bucketName;
                //req.WithBucketName(bucketName);
                req.Key = objectKeyName;
                //req.WithKey(objectKeyName);
                req.Expires = expDate;
                //req.WithExpires(expDate);
                req.Protocol=Protocol.HTTP;
                //req.WithProtocol(Protocol.HTTPS);
                url = client.GetPreSignedURL(req);
                url = url.Replace(signedUrlHostToReplace, signedUrlHostToReplaceWith);
            }
            return url;
        }
    }
}
