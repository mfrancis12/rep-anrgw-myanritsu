﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.AWS
{
    public static class AWSUtility_Settings
    {
        private static String _PID_KeyPairID;
        private static String _PID_PEM;
        private static String _PID_Prefix;
        private static String _PID_BaseUrlHost;

        private static String _DL2_KeyPairID;
        private static String _DL2_PEM;
        private static String _DL2_BaseUrlHost;
        private static String _DL2_Prefix_Download;
        private static String _DL2_Prefix_JPVersionTable;

        private static String _TAUDL_KeyPairID;
        private static String _TAUDL_PEM;
        private static String _TAUDL_Prefix;
        private static String _TAUDL_BaseUrlHost;

        private static String _JPDL_KeyPairID;
        private static String _JPDL_PEM;
        private static String _JPDL_Prefix;
        private static String _JPDL_BaseUrlHost;

        static AWSUtility_Settings()
        {
           InitSettings();
        }

        private static void InitSettings()
        {
            String pidCFPemPath = HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("PID.AWS.CloudFront.PEM"));
            using (StreamReader srPID = new StreamReader(pidCFPemPath))
            {
                _PID_PEM = srPID.ReadToEnd().ConvertNullToEmptyString().Replace("\n", "");
            }
            _PID_KeyPairID = ConfigUtility.AppSettingGetValue("PID.AWS.CloudFront.KeyPairID");
            _PID_Prefix = ConfigUtility.AppSettingGetValue("PID.AWS.CloudFront.Prefix");
            _PID_BaseUrlHost = ConfigUtility.AppSettingGetValue("PID.AWS.CloudFront.BaseUrl");

            String tauPemPath = HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("TAUDL.AWS.CloudFront.PEM"));
            using (StreamReader srPID = new StreamReader(tauPemPath))
            {
                _JPDL_PEM = _TAUDL_PEM = srPID.ReadToEnd().ConvertNullToEmptyString().Replace("\n", "");
            }
            _TAUDL_KeyPairID = ConfigUtility.AppSettingGetValue("TAUDL.AWS.CloudFront.KeyPairID");
            _TAUDL_Prefix = ConfigUtility.AppSettingGetValue("TAUDL.AWS.CloudFront.Prefix");
            _TAUDL_BaseUrlHost = ConfigUtility.AppSettingGetValue("TAUDL.AWS.CloudFront.BaseUrl");

            #region Jp settings
            _JPDL_KeyPairID = ConfigUtility.AppSettingGetValue("JPDL.AWS.CloudFront.KeyPairID");
            _JPDL_Prefix = ConfigUtility.AppSettingGetValue("JPDL.AWS.CloudFront.Prefix");
            _JPDL_BaseUrlHost = ConfigUtility.AppSettingGetValue("JPDL.AWS.CloudFront.BaseUrl");
            #endregion


            String dl2CFPemPath = HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("DL2.AWS.CloudFront.PEM"));
            using (StreamReader srDL2 = new StreamReader(dl2CFPemPath))
            {
                _DL2_PEM = srDL2.ReadToEnd().ConvertNullToEmptyString().Replace("\n", "");
            }
            _DL2_KeyPairID = ConfigUtility.AppSettingGetValue("DL2.AWS.CloudFront.KeyPairID");
            _DL2_Prefix_Download = ConfigUtility.AppSettingGetValue("DL2.AWS.CloudFront.Prefix.Download");
            _DL2_Prefix_JPVersionTable = ConfigUtility.AppSettingGetValue("DL2.AWS.CloudFront.Prefix.JPVersionTable");
            _DL2_BaseUrlHost = ConfigUtility.AppSettingGetValue("DL2.AWS.CloudFront.BaseUrl");
        }

        //public static String PID_KeyPairID;
        //public static String PID_PEM;
        //public static String PID_Prefix;
        //public static String PID_BaseUrlHost;

        //public static String DL2_KeyPairID;
        //public static String DL2_PEM;
        //public static String DL2_Prefix;
        //public static String DL2_BaseUrlHost;

        public static String PID_KeyPairID_Get()
        {
            if (_PID_KeyPairID.IsNullOrEmptyString()) InitSettings();
            return _PID_KeyPairID;
        }

        public static String PID_PEM_Get()
        {
            if (_PID_PEM.IsNullOrEmptyString()) InitSettings();
            return _PID_PEM;
        }

        public static String PID_Prefix_Get()
        {
            if (_PID_Prefix.IsNullOrEmptyString()) InitSettings();
            return _PID_Prefix;
        }

        public static String PID_BaseUrlHost_Get()
        {
            if (_PID_BaseUrlHost.IsNullOrEmptyString()) InitSettings();
            return _PID_BaseUrlHost;
        }

        #region TAU Keys

        public static String TAU_KeyPairID_Get()
        {
            if (_TAUDL_KeyPairID.IsNullOrEmptyString()) InitSettings();
            return _TAUDL_KeyPairID;
        }

        public static String TAU_PEM_Get()
        {
            if (_TAUDL_PEM.IsNullOrEmptyString()) InitSettings();
            return _TAUDL_PEM;
        }

        public static String TAU_Prefix_Get()
        {
            if (_TAUDL_Prefix.IsNullOrEmptyString()) InitSettings();
            return _TAUDL_Prefix;
        }

        public static String TAU_BaseUrlHost_Get()
        {
            if (_TAUDL_BaseUrlHost.IsNullOrEmptyString()) InitSettings();
            return _TAUDL_BaseUrlHost;
        }
        public static String JP_PEM_Get()
        {
            if (_JPDL_PEM.IsNullOrEmptyString()) InitSettings();
            return _JPDL_PEM;
        }

        public static String JP_BaseUrlHost_Get()
        {
            if (_JPDL_BaseUrlHost.IsNullOrEmptyString()) InitSettings();
            return _JPDL_BaseUrlHost;
        }
        public static String JP_KeyPairID_Get()
        {
            if (_JPDL_KeyPairID.IsNullOrEmptyString()) InitSettings();
            return _JPDL_KeyPairID;
        }

        #endregion


        public static String DL2_KeyPairID_Get()
        {
            if (_DL2_KeyPairID.IsNullOrEmptyString()) InitSettings();
            return _DL2_KeyPairID;
        }

        public static String DL2_PEM_Get()
        {
            if (_DL2_PEM.IsNullOrEmptyString()) InitSettings();
            return _DL2_PEM;
        }

        public static String DL2_Prefix_Download_Get()
        {
            if (_DL2_Prefix_Download.IsNullOrEmptyString()) InitSettings();
            return _DL2_Prefix_Download;
        }

        public static String DL2_Prefix_JPVersionTable_Get()
        {
            if (_DL2_Prefix_JPVersionTable.IsNullOrEmptyString()) InitSettings();
            return _DL2_Prefix_JPVersionTable;
        }

        public static String DL2_BaseUrlHost_Get()
        {
            if (_DL2_BaseUrlHost.IsNullOrEmptyString()) InitSettings();
            return _DL2_BaseUrlHost;
        }
    }
}
