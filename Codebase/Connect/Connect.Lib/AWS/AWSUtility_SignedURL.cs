﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
namespace Anritsu.Connect.Lib.AWS
{
   public class AWSUtility_CloudFrontSignedURL
    {
        /// <summary>
        /// This Generates a signed http url using a canned policy.
        /// To create the PEM file and KeyPairID please visit https://aws-portal.amazon.com/gp/aws/developer/account/index.html?action=access-key
        /// </summary>
        /// <param name="resourceURL">The URL of the distribution item you are signing.</param>
        /// <param name="expiryTime">UTC time to expire the signed URL</param>
        /// <param name="pemFileLocation">The path and name to the PEM file. Can be either Relative or Absolute.</param>
        /// <param name="keypairId">The ID of the private key used to sign the request</param>
        /// <param name="urlEncode">Whether to URL encode the result</param>
        /// <returns>A String that is the signed http request.</returns>
        public static string GetPreSignedURLWithPEMFile(string resourceURL, DateTime expiryTime, string pemFileLocation, string keypairId, bool urlEncode)
        {
            if (pemFileLocation.StartsWith("~"))
            {
                var baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
                pemFileLocation = Path.GetFullPath(baseDirectory + pemFileLocation.Replace("~", string.Empty));
            }
            String pemKey = String.Empty;
            using (System.IO.StreamReader myStreamReader = new System.IO.StreamReader(pemFileLocation))
            {
                pemKey = myStreamReader.ReadToEnd();
            }
            pemKey = pemKey.Replace("\n", "");
            return GetPreSignedURLWithPEMKey(resourceURL, expiryTime, pemKey, keypairId, urlEncode);
        }

        /// <summary>
        /// This Generates a signed http url using a canned policy.
        /// To create the PEM file and KeyPairID please visit https://aws-portal.amazon.com/gp/aws/developer/account/index.html?action=access-key
        /// </summary>
        /// <param name="resourceURL">The URL of the distribution item you are signing.</param>
        /// <param name="expiryTime">UTC time to expire the signed URL</param>
        /// <param name="pemFileLocation">The actual pem file</param>
        /// <param name="keypairId">The ID of the private key used to sign the request</param>
        /// <param name="urlEncode">Whether to URL encode the result</param>
        /// <returns>A String that is the signed http request.</returns>
        public static string GetPreSignedURLWithPEMKey(string resourceURL, DateTime expiryTime, string keyPEM, string keypairId, bool urlEncode)
        {
            string xmlKey = JavaScience.opensslkey.DecodePEMKey(keyPEM);
            return GetPreSignedURLWithXMLKey(resourceURL, expiryTime, xmlKey, keypairId, urlEncode);
        }

        /// <summary>
        /// This Generates a signed http url using a canned policy.
        /// To create the PEM file and KeyPairID please visit https://aws-portal.amazon.com/gp/aws/developer/account/index.html?action=access-key
        /// </summary>
        /// <param name="resourceURL">The URL of the distribution item you are signing.</param>
        /// <param name="expiryTime">UTC time to expire the signed URL</param>
        /// <param name="pemFileLocation">The actual pem file</param>
        /// <param name="keypairId">The ID of the private key used to sign the request</param>
        /// <param name="urlEncode">Whether to URL encode the result</param>
        /// <returns>A String that is the signed http request.</returns>
        public static string GetPreSignedURLWithXMLKey(string resourceURL, DateTime expiryTime, string keyXML, string keypairId, bool urlEncode)
        {
            long expiry = (long)(expiryTime.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
            string policy = String.Format(
                @"{{""Statement"":[{{""Resource"":""{0}"",""Condition"":{{""DateLessThan"":{{""AWS:EpochTime"":{1}}}}}}}]}}",
                resourceURL, expiry);

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            RSACryptoServiceProvider.UseMachineKeyStore = false;
            rsa.FromXmlString(keyXML);

            string signature = UrlSafe(rsa.SignData(Encoding.UTF8.GetBytes(policy), new SHA1CryptoServiceProvider()));

            string formatStr = urlEncode ?
            "{0}%3fExpires%3d{1}%26Signature%3d{2}%26Key-Pair-Id%3d{3}" :
            "{0}?Expires={1}&Signature={2}&Key-Pair-Id={3}";

            return string.Format(formatStr, resourceURL, expiry, signature, keypairId);
        }

        private static string UrlSafe(byte[] data)
        {
            return Convert.ToBase64String(data)
            .Replace('+', '-').Replace('=', '_').Replace('/', '~');
        }
    }
}
