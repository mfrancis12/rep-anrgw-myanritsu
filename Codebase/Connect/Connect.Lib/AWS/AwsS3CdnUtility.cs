﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.AWS
{
   public static class AwsS3CdnUtility
    {
       public static String GenerateTauCdnUrl(String filePath, Int32 durationInSecs)
       {
           String fileRelUrl = filePath.Replace(@"\", "/");
           if (fileRelUrl.StartsWith("/")) fileRelUrl = fileRelUrl.Substring(1);
           String cloudFrontUrl = String.Format("{0}/{1}", AWS.AWSUtility_Settings.TAU_BaseUrlHost_Get(), fileRelUrl);
           DateTime expireUtc = DateTime.UtcNow.AddDays(durationInSecs);
           return AWS.AWSUtility_CloudFrontSignedURL.GetPreSignedURLWithPEMKey(cloudFrontUrl, expireUtc, AWS.AWSUtility_Settings.TAU_PEM_Get(), AWS.AWSUtility_Settings.TAU_KeyPairID_Get(), false);
       }
       public static String GenerateJpCdnUrl(String filePath, Int32 durationInSecs)
       {
           String fileRelUrl = filePath.Replace(@"\", "/");
           if (fileRelUrl.StartsWith("/")) fileRelUrl = fileRelUrl.Substring(1);
           String cloudFrontUrl = String.Format("{0}/{1}", AWS.AWSUtility_Settings.JP_BaseUrlHost_Get(), fileRelUrl);
           DateTime expireUtc = DateTime.UtcNow.AddDays(durationInSecs);
           return AWS.AWSUtility_CloudFrontSignedURL.GetPreSignedURLWithPEMKey(cloudFrontUrl, expireUtc, AWS.AWSUtility_Settings.JP_PEM_Get(), AWS.AWSUtility_Settings.JP_KeyPairID_Get(), false);
       }
    }
}
