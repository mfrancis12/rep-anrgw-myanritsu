﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.S3.Model;

namespace Anritsu.Connect.Lib.AWS
{
    public class S3ObjectsWithPrefixes
    {
        public List<string> CommonPrefixes { get; set; }
        public List<S3Object> Content { get; set; }
    }
}
