﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Account
{
    [Serializable]
    public class Acc_AccountMaster_History
    {
        public Guid AccountID { get; set; }
        public String AccountName { get; set; }
        public String AccountStatusCode { get; set; }
        public String CompanyName { get; set; }
        public String CompanyNameInRuby { get; set; }
        public String LangInput { get; set; }
        public Int32 CompanyAddressID { get; set; }
        public Boolean IsVerifiedAndApproved { get; set; }
        public Boolean InfoUpdateRequired { get; set; }        
        public Boolean AgreedToTerms { get; set; }
        public DateTime AgreedToTermsOnUTC { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public String CompanyAddress { get; internal set; }
    }
}



