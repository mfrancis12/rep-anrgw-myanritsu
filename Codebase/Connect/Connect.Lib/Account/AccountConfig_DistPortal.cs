﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Account
{
    [Serializable]
    public class AccountConfig_DistPortal
    {
        public Guid AccountID { get; set; }
        public String StatusCode { get; set; }
        public Boolean SIS_AccessType_Public { get; set; }
        public Boolean SIS_AccessType_Private { get; set; }
        public Int32 SIS_ProdFilter { get; set; }
        public Int32 SIS_DocTypeFilter { get; set; }
        public String ModifiedBy { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
