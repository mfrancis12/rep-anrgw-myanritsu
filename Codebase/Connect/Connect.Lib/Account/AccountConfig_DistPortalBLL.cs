﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.Account
{
    public static class AccountConfig_DistPortalBLL
    {
        internal static AccountConfig_DistPortal InitFromData(DataRow r)
        {
            if (r == null) return null;
            AccountConfig_DistPortal obj = new AccountConfig_DistPortal();
            obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            obj.StatusCode = ConvertUtility.ConvertNullToEmptyString(r["StatusCode"]);
            obj.SIS_AccessType_Public = ConvertUtility.ConvertToBoolean(r["SIS_AccessType_Public"], false);
            obj.SIS_AccessType_Private = ConvertUtility.ConvertToBoolean(r["SIS_AccessType_Private"], false);
            obj.ModifiedBy = ConvertUtility.ConvertNullToEmptyString(r["ModifiedBy"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(r["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.SIS_ProdFilter = ConvertUtility.ConvertToInt32(r["SIS_ProdFilter"], 0);//0 is default null, show all products
            obj.SIS_DocTypeFilter = ConvertUtility.ConvertToInt32(r["SIS_DocTypeFilter"], 0);//0 is default null, show all doc types
            return obj;
        }

        public static AccountConfig_DistPortal Select(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DataRow r = Data.DAAcc_Account_Config_DistPortal.Select(accountID);
            return InitFromData(r);
        }

        public static void Insert(Guid accountID, String createdBy, Int32 sis_ProdFilter, Int32 sis_DocTypeFilter)
        {
            if (accountID.IsNullOrEmptyGuid()) throw new ArgumentNullException("accountID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            
            Boolean sis_Public = true;
            Boolean sis_Private = true;
            String status = "pending";
            Data.DAAcc_Account_Config_DistPortal.Insert(accountID, status, sis_Public, sis_Private, sis_ProdFilter, sis_DocTypeFilter, createdBy);
        }

        public static void Update(Guid accountID, String statusCode,
            Boolean sis_AccessType_Public, Boolean sis_AccessType_Private, Int32 sis_ProdFilter, Int32 sis_DocTypeFilter, String modifiedBy)
        {
            if (accountID.IsNullOrEmptyGuid()) throw new ArgumentNullException("accountID");
            if (modifiedBy.IsNullOrEmptyString()) throw new ArgumentNullException("modifiedBy");
            if (statusCode.IsNullOrEmptyString()) throw new ArgumentNullException("statusCode");
            Data.DAAcc_Account_Config_DistPortal.Update(accountID, statusCode, sis_AccessType_Public, sis_AccessType_Private
                , sis_ProdFilter, sis_DocTypeFilter, modifiedBy);
        }
    }
}
