﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Account
{
    public class AccountConfig_DistPortal_Status
    {
        public String DistPortalStatusCode { get; set; }
        public String DisplayText { get; set; }
    }
}
