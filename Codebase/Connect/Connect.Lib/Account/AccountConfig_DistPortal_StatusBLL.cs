﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Account
{
    public static class AccountConfig_DistPortal_StatusBLL
    {
        internal static AccountConfig_DistPortal_Status InitFromData(DataRow r)
        {
            if (r == null) return null;
            AccountConfig_DistPortal_Status obj = new AccountConfig_DistPortal_Status();
            obj.DistPortalStatusCode = ConvertUtility.ConvertNullToEmptyString(r["DistPortalStatusCode"]);
            obj.DisplayText = obj.DistPortalStatusCode;//shas, please localize it
            return obj;
        }

        public static List<AccountConfig_DistPortal_Status> SelectAll()
        {
            DataTable tb = Data.DAAcc_Account_Config_DistPortal_Status.SelectAll();
            if (tb == null) return null;
            List<AccountConfig_DistPortal_Status> list = new List<AccountConfig_DistPortal_Status>();
            foreach (DataRow r in tb.Rows)
            {
                list.Add(InitFromData(r));
            }
            return list;
        }
    }
}
