﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Account
{
    [Serializable]
    public class AccountLookup
    {
        public Guid AccountID { get; set; }
        public String DisplayLongName { get; set; }
    }
}
