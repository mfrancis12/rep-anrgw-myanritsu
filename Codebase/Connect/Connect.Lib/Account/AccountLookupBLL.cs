﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Account
{
    public static class AccountLookupBLL
    {
        internal const String DATACACHE_ACCLOOKUP = "AccLookupALL";

        private static AccountLookup InitData(DataRow row)
        {
            if (row == null) return null;

            AccountLookup obj = new AccountLookup();
            obj.AccountID = ConvertUtility.ConvertToGuid(row["AccountID"], Guid.Empty);
            obj.DisplayLongName = ConvertUtility.ConvertNullToEmptyString(row["CompanyOrgName"]);
            return obj;
        }

        public static List<AccountLookup> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_ACCLOOKUP;

            ConnectDataCache cache = new ConnectDataCache();
            List<AccountLookup> list = cache.GetCachedItem(cacheKey) as List<AccountLookup>;
            if (list == null || refreshFromDB)
            {
                list = new List<AccountLookup>();
                DataTable tb = Data.DAAcc_AccountMaster.SelectAllFiltered(Guid.Empty, String.Empty, String.Empty, String.Empty,
                    String.Empty, null);
                if (tb == null) return null;
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));

                // 1hr exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddHours(1));
            }

            return list;
        }

        public static List<AccountLookup> SelectAll(String keyword)
        {
            if (keyword.IsNullOrEmptyString()) return null;
            List<AccountLookup> list = SelectAll(false);
            if (list == null) return null;
            var query = from acc in list
                        where acc.DisplayLongName.Contains(keyword, RegexOptions.IgnoreCase) 
                        select acc;
            if (query == null) return null;
            return query.ToList();
        }
    }
}
