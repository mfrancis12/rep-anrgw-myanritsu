﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Account
{
    [Serializable]
    public class Profile_Address
    {
        //public Int32 AddressID { get; set; }
        //public Guid AccountID { get; set; }
        //public String AddressName { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String CityTown { get; set; }
        public String StateProvince { get; set; }
        public String ZipPostalCode { get; set; }
        public String CountryCode { get; set; }
        public String PhoneNumber { get; set; }
        public String FaxNumber { get; set; }
        //public Boolean IsVerified { get; set; }
        //public DateTime ModifiedOnUTC { get; set; }
        //public DateTime CreatedOnUTC { get; set; }
        public String AddressText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                //if(!String.IsNullOrEmpty(AddressName))
                //    sb.AppendFormat("{0}\r\n", AddressName);
                if (!String.IsNullOrEmpty(Address1))
                    sb.AppendFormat("{0}\r\n", Address1);
                if (!String.IsNullOrEmpty(Address2))
                    sb.AppendFormat("{0}\r\n", Address2);
                if (!String.IsNullOrEmpty(CityTown))
                    sb.AppendFormat("{0}\r\n", CityTown);
                if (!String.IsNullOrEmpty(ZipPostalCode))
                    sb.AppendFormat("{0}\r\n", ZipPostalCode);
                if (!String.IsNullOrEmpty(CountryCode))
                    sb.AppendFormat("{0}\r\n", CountryCode);
                if (!String.IsNullOrEmpty(PhoneNumber))
                    sb.AppendFormat("{0}\r\n", PhoneNumber);
                if (!String.IsNullOrEmpty(FaxNumber))
                    sb.AppendFormat("{0}\r\n", FaxNumber);
                return sb.ToString();
            }
        }

        public bool IsNullOrEmpty()
        {
            var address = this;
            return (address.Address1.IsNullOrEmptyString() && address.Address2.IsNullOrEmptyString()
                && address.CityTown.IsNullOrEmptyString() && address.StateProvince.IsNullOrEmptyString()
               && address.ZipPostalCode.IsNullOrEmptyString() && address.CountryCode.IsNullOrEmptyString()
               && address.PhoneNumber.IsNullOrEmptyString() && address.FaxNumber.IsNullOrEmptyString());
        }
    }
}
