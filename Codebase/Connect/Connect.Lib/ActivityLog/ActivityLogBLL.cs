﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ActivityLog
{
    public static class ActivityLogBLL
    {
        public static int Insert(String emailID, String areaModified, String taskperformed, String action, String ipAddress, String modelConfigType)
        {
            return Data.DAActivityLog.Insert(emailID, areaModified, taskperformed, action, ipAddress, modelConfigType);
        }
        public static int ClientLogInsert(String emailID, String filePath, String taskperformed, long fileSize, String ipAddress,String modelConfig)
        {
            return Data.DAActivityLog.ClientLogInsert(emailID, filePath, taskperformed, fileSize, ipAddress,modelConfig);
        }
    }
}
