﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ActivityLog
{
    /// <summary>
    /// Type of tasks that can be performed by the Admin/User
    /// </summary>
    public static class TasksPerformed
    {
        public const String Add = "Add";
        public const String Modify = "Modify";
        public const String Delete = "Delete";
        public const String Sync = "Sync";
    }

    public static class ActivityLogNotes
    {
        #region Admin logs

        //replace the place holder with the notification id

        public const String NOTIFICATION_ADD_ATTEMPT = "Attempting to add Export Notice - Name: {0}";
        public const String NOTIFICATION_ADD_SUCCESS = "[Success] New Export Notice added - Name: {0}";

        public const String NOTIFICATION_MODIFY_ATTEMPT = "Attempting to modify Export Notice - Name: {0}";
        public const String NOTIFICATION_MODIFY_SUCCESS = "[Success] New values - Name: {0}";

        public const String NOTIFICATION_DELETE_ATTEMPT = "Attempting to delete Export Notice - Name: {0}";
        public const String NOTIFICATION_DELETE_SUCCESS = "[Success] Export Notice deleted - Name: {0}";
        


        public const String PACKAGE_ADD_ATTEMPT = "Attempting to add Package - Name: {0}";
        public const String PACKAGE_ADD_SUCCESS = "[Success] New Package added - Name: {0}, Path: {1}, Accounts Mapped: None, Files Exposed: {2}";
        public const String PACKAGE_ADD_FAIL = "[Failed] Add Package failed; Package - Name: {0}";

        public const String PACKAGE_ADDACC_ATTEMPT = "Attempting to add Account:{0} to Package - Name: {1}";
        public const String PACKAGE_ADDACC_SUCCESS = "[Success] New Account:{0} added to the Package - Name: {1}";
        public const String PACKAGE_ADDACC_FAIL = "[Failed] Add Package failed; Package - Name: {0}";

        public const String PACKAGE_MODIFY_ATTEMPT = "Attempting to modify details for Package - Name: {0}";
        public const String PACKAGE_MODIFY_SUCCESS = "[Success] New values - Name: {0}, Path: {1}, Accounts Mapped: None, Files Exposed: {2}";
        public const String PACKAGE_MODIFY_FAIL = "[Failed] modify Package failed; Package - Name: {0}";

        public const String PACKAGE_DELETE_ATTEMPT = "Attempting to delete Package - Name: {0}";
        public const String PACKAGE_DELETE_SUCCESS = "[Success] Package deleted - Name: {0}";
        public const String PACKAGE_DELETE_FAIL = "[Failed] Delete Package failed; Package - Name: {0}";

        public const String PACKAGE_DELETEACC_ATTEMPT = "Attempting to remove Account - Name: {0} from Package Name:{1}";
        public const String PACKAGE_DELETEACC_SUCCESS = "[Success] Account - Name: {0} removed from Package Name:{1}";
        public const String PACKAGE_DELETEACC_FAIL = "[Failed] Remove Account Name:{0} from the Package - Name: {0}";

        public const String PACKAGE_SYNC = "Approver issued a request to synchronise Package Name:{0} to Live site.";

        public const String SUPPORTTYPEENTRY_ADD_ATTEMPT = "Attempting to add Support type entry with Model {0} Serial {1} Email or Organization {2}";
        public const String SUPPORTTYPEENTRY_ADD_SUCCESS = "[Success] New Support type entry with Model {0} Serial {1} Email or Organization {2}";
        public const String SUPPORTTYPEENTRY_ADD_FAIL = "[Failed] Add Support type entry with Model {0} Serial {1} Email or Organization {2}";

        public const String SUPPORTTYPEENTRY_MODIFY_ATTEMPT = "Attempting to modify Support type entry with Model {0} Serial {1} Email or Organization {2}";
        public const String SUPPORTTYPEENTRY_MODIFY_SUCCESS = "[Success] New values for Support type entry with Model {0} Serial {1} Email or Organization {2}";
        public const String SUPPORTTYPEENTRY_MODIFY_FAIL = "[Failed] Modify Support type entry with Model {0} Serial {1} Email or Organization {2} failed";

        public const String SUPPORTTYPEENTRY_DELETE_ATTEMPT = "Attempting to delete Support type entry with Model {0} Serial {1} Email or Organization {2}";
        public const String SUPPORTTYPEENTRY_DELETE_SUCCESS = "[Success] Support type entry with Model {0} Serial {1} Email or Organization {2} deleted";
        public const String SUPPORTTYPEENTRY_DELETE_FAIL = "[Failed] Deleting Support type entry with Model {0} Serial {1} Email or Organization {2} failed";

        #endregion

        #region Client logs

        public const String FILE_DOWNLOAD_ATTEMPT = "Attempting to download file - Name:{0},File Size: {1} KB";
        public const String EXPORTNOTICE_DISPLAYED = "Export notice displayed - acknowledgement requested.";
        public const String EXPORTNOTICE_ACK_SUCCESS = "[Success] Export notice was acknowledged. Access to requested resources permitted.";
        public const String EXPORTNOTICE_ACK_FAILED = "[FAILED] Export notice was not acknowledged. Access to requested resources denied.";
        #endregion
    }

}
