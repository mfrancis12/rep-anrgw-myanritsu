﻿using System;
using System.Net.Http;
using System.Web;

namespace ApiSdk
{
    public class ApiCacheFactory : BaseAppCacheFactory
    {
        public object GetCacheData(String cacheKey)
        {
            var dataFound = TryRetrieveDataFromCache(cacheKey);
          return dataFound;
        }

        public void SetCachedData(String cacheKey, Object data)
        {
            SetCacheData(cacheKey, data);
        }

    }
}