﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;
namespace Anritsu.Connect.Lib
{
    public static class AppLog
    {
        public static void LogError(Exception exception, HttpContext context)
        {
            if (exception == null) return;

            string link = null;
            int httpCode = 0;
            if (exception is HttpException)
            {
                HttpException httpEx = (HttpException)exception;
                httpCode = httpEx.GetHttpCode();
            }

            string formData = null;
            string clientip = null;
            string referer = null;
            string shortMessage = null;
            string stackTrace = null;
            string methodName = null;
            string className = null;
            string userAgent = null;
            string hostName = null;
            string userName = null;
            string clientHostName = null;
            string serializedRequest = null;
            string environment = null;
            HttpRequest request = null;
            Exception ex = exception.GetBaseException();

            shortMessage = HttpUtility.HtmlEncode(ex.Message);

            if (ex.StackTrace != null && ex.StackTrace.Length > 0)
                stackTrace = HttpUtility.HtmlEncode(ex.StackTrace);

            if (ex.InnerException != null)
            {
                stackTrace += "\r\n======== INNER EXCEPTION ==================\r\n";
                stackTrace += String.Format("Inner Exception Message: {0}\r\n", ex.InnerException.Message);
                stackTrace += String.Format("Inner Exception Source: {0}\r\n", ex.InnerException.Source);
                stackTrace += String.Format("Inner Exception TargetSite: {0}\r\n", ex.InnerException.TargetSite);
                stackTrace += String.Format("Inner Exception StackTrace: {0}\r\n", ex.InnerException.StackTrace);
                stackTrace += "\r\n======== INNER EXCEPTION END ==================\r\n";
            }

            methodName = ex.TargetSite != null ? ex.TargetSite.Name : null;
            className = ex.TargetSite != null ? ex.TargetSite.ReflectedType.ToString() : null;

            try
            {
                if (context != null)
                {
                    request = context.Request;
                    link = HttpUtility.HtmlEncode(request.Url.ToString());

                    if (request.Form != null)
                        formData = request.Form.ToString();

                    clientip = WebUtility.GetUserIP();
                    clientHostName = request.UserHostName;

                    if (request.ServerVariables["HTTP_REFERER"] != null)
                        referer = request.ServerVariables["HTTP_REFERER"];

                    userAgent = request.UserAgent;
                    userName = request.LogonUserIdentity.Name;
                    hostName = request.ServerVariables["SERVER_NAME"];

                    /*System.IO.StringWriter sWriter = new System.IO.StringWriter();
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(request.GetType());
                    xmlSerializer.Serialize(sWriter, request);
                    serializedRequest = sWriter.ToString();*/
                    serializedRequest = request.Params.ToString();
                    environment = request.RawUrl;
                }

                Data.DAAppLog.LogError("CONNECT", context.Request.RawUrl,
                    referer, 
                    httpCode, 
                    clientip, shortMessage, stackTrace,
                    className + "." + methodName,
                    serializedRequest,
                    userName,
                    clientHostName,
                    userAgent,
                    hostName);
            }
            catch (Exception localException)
            {
                #region "  cannot log error, send email to webmasters "
                if (HttpContext.Current != null)
                {
                    StringBuilder sbBody = new StringBuilder();
                    sbBody.Append("ExceptionUtility.LogError method cannot log the following error to database.");
                    sbBody.Append("\nApplication : CONNECT");
                    sbBody.Append("\n");
                    sbBody.Append("Exception ...\n\n");
                    sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                    sbBody.Append(string.Format("Class Name : {0}\n\n", className));
                    sbBody.Append(string.Format("Method Name : {0}\n\n", methodName));
                    sbBody.Append(string.Format("Stack Trace : {0}\n\n", stackTrace));
                    sbBody.Append(string.Format("Current Url : {0}\n", link));
                    sbBody.Append(string.Format("Referring Url: {0}\n", referer));
                    sbBody.Append(string.Format("Client IP : {0}\n", clientip));
                    sbBody.Append(string.Format("Client HostName : {0}\n", clientHostName));
                    sbBody.Append(string.Format("Host : {0}\n", hostName));
                    sbBody.Append(string.Format("User Agent : {0}\n", userAgent));
                    sbBody.Append(string.Format("HttpCode : {0}\n\n", httpCode));
                    sbBody.Append(string.Format("RawURL : {0}\n\n", context.Request.RawUrl));
                    sbBody.Append("\n\n ===================== unable to log error due to the following =======================\n\n");
                    sbBody.Append(string.Format("Short Message : {0}\n\n", localException.Message));
                    sbBody.Append("Class Name : ExceptionUtility\n\n");
                    sbBody.Append("Method Name : LogError(AnritsuAppsEnum enumApp, ExceptionApplicationType applicationType, Exception exception, bool emailNotification)\n");
                    sbBody.Append(string.Format("Stack Trace {0}\n\n", localException.StackTrace));
                    sbBody.AppendFormat("\n\nServer IP : {0}\n\n"
                        , HttpContext.Current.Request.ServerVariables["REMOTE_HOST"]);


                    //if (!AppEnvironment.GetWebServerName().Equals(string.Empty))//this is to prevent sending emails at compile time.
                    {
                        EM.EmailWebRef.SendEmailCallRequest emReq = new EM.EmailWebRef.SendEmailCallRequest();
                        emReq.ToEmailAddresses = new String[]{ConfigUtility.AppSettingGetValue("Connect.Lib.AppLog.EmailTo")};
                        emReq.FromEmailAddress = "WebMasterNoReply@anritsu.com";
                        emReq.IsHtmlEmailBody = false;
                        emReq.EmailSubject = string.Format("Unable to log error on {0}", request.RawUrl); 
                        emReq.EmailBody = sbBody.ToString();                        
                        EM.EmailServiceManager.SendEmail(emReq);
                    }
                }
                #endregion
            }
        }
    }
}
