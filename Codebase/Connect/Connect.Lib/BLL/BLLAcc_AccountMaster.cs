﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLAcc_AccountMaster
    {
        public static Lib.Account.Acc_AccountMaster OldAccountMaster { get; set; }
        //internal static Acc_AccountMaster InitFromData(DataRow r, DataTable jpDownloadUserIDs)
        internal static Acc_AccountMaster InitFromData(DataRow r)
        {
            if (r == null) return null;
            Acc_AccountMaster obj = new Acc_AccountMaster();
            obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            obj.AccountName = ConvertUtility.ConvertNullToEmptyString(r["AccountName"]);
            obj.AccountStatusCode = ConvertUtility.ConvertNullToEmptyString(r["AccountStatusCode"]);
            obj.LangInput = ConvertUtility.ConvertNullToEmptyString(r["LangInput"]);
            obj.CompanyName = ConvertUtility.ConvertNullToEmptyString(r["CompanyName"]);
            obj.CompanyNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["CompanyNameInRuby"]);
            //obj.RUBY = ConvertUtility.ConvertNullToEmptyString(r["RUBY"]);
            //obj.CompanyNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["CompanyNameInEnglish"]);
            obj.OwnerMemberShipID = ConvertUtility.ConvertToGuid(r["OwnerMemberShipID"], Guid.Empty);
            obj.IsVerifiedAndApproved = ConvertUtility.ConvertToBoolean(r["IsVerifiedAndApproved"], false);
            obj.InfoUpdateRequired = ConvertUtility.ConvertToBoolean(r["InfoUpdateRequired"], false);
            obj.AdditinalNotes = ConvertUtility.ConvertNullToEmptyString(r["AdditionalNotes"]);
            obj.AgreedToTerms = ConvertUtility.ConvertToBoolean(r["AgreedToTerms"], false);
            obj.AgreedToTermsOnUTC = ConvertUtility.ConvertToDateTime(r["AgreedToTermsOnUTC"], DateTime.MinValue);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.CompanyAddress = String.Empty;
            if (!obj.OwnerMemberShipID.IsNullOrEmptyGuid())
            {
                obj.CompanyAddress = GenerateAddressString(r, false, false);
            }
            obj.CompanyCountryCode = ConvertUtility.ConvertNullToEmptyString(r["CountryCode"]);
            
            int accCultureGroupId = 1;
            String accCultureGroup = "en-US";
            Lib.BLL.BLLIso_Country.GetCultureGroupInfoByCountry(obj.CompanyCountryCode, out accCultureGroupId, out accCultureGroup);
            obj.GWCultureGroupIDByCountryCode = accCultureGroupId;
            obj.GWCultureGroupByCountryCode = accCultureGroup;

            obj.Config_DistributorPortal = AccountConfig_DistPortalBLL.Select(obj.AccountID);
            return obj;
        }

        public static Acc_AccountMaster SelectByAccountID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DataRow r = Data.DAAcc_AccountMaster.SelectByAccountID(accountID);
            return InitFromData(r);

        }    
        

        public static DataTable SelectAll()
        {
            return Data.DAAcc_AccountMaster.SelectAllFiltered(Guid.Empty, String.Empty, String.Empty, String.Empty,
                String.Empty, null);
        }

        public static DataTable SelectAllFiltered(List<SearchByOption> searchOptions)
        {
            if (searchOptions == null || searchOptions.Count < 1) return null;
            Guid accountID = Guid.Empty;
            String companyName = String.Empty;
            String orgName = String.Empty;
            String companyCountryCode = String.Empty;
            String accountStatusCode = String.Empty;
            Boolean? isVerifiedAndApproved = null;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "AccountID":
                        accountID = ConvertUtility.ConvertToGuid(sbo.SearchObject, Guid.Empty);
                        break;
                    case "OrgName":
                        orgName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        companyName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyCountryCode":
                        companyCountryCode = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "IsVerifiedAndApproved":
                        isVerifiedAndApproved = ConvertUtility.ConvertToBoolean(sbo.SearchObject, false);
                        break;
                    case "AccountStatusCode":
                        accountStatusCode = Convert.ToString(sbo.SearchObject);
                        break;
                }
            }
            return Data.DAAcc_AccountMaster.SelectAllFiltered(accountID, companyName, orgName, companyCountryCode,
                accountStatusCode, isVerifiedAndApproved);
        }


        public static DataTable SearchFilteredResult(List<SearchByOption> searchOptions)
        {
            if (searchOptions == null || searchOptions.Count < 1) return null;
            Guid accountID = Guid.Empty;
            String companyName = String.Empty;
            String orgName = String.Empty;
            String companyCountryCode = String.Empty;
            String accountStatusCode = String.Empty;
            Boolean? isVerifiedAndApproved = null;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "AccountID":
                        accountID = ConvertUtility.ConvertToGuid(sbo.SearchObject, Guid.Empty);
                        break;
                    case "OrgName":
                        orgName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        companyName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyCountryCode":
                        companyCountryCode = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "IsVerifiedAndApproved":
                        isVerifiedAndApproved = ConvertUtility.ConvertToBoolean(sbo.SearchObject, false);
                        break;
                }
            }
            return Data.DAAcc_AccountMaster.SearchFilteredResult(accountID, companyName, orgName, companyCountryCode,
                accountStatusCode, isVerifiedAndApproved);
        }

        public static DataTable SelectByCompany(String companyName)
        {
            if (companyName.IsNullOrEmptyString()) return null;
            return Data.DAAcc_AccountMaster.SelectByCompany(companyName);
        }

        public static List<Acc_AccountMaster> SelectByMembershipId(Guid membershipId)
        {
            if (membershipId.IsNullOrEmptyGuid()) return null;

            DataTable tb = Data.DAAcc_AccountMaster.SelectByMembershipId(membershipId);
            if (tb == null) return null;
            List<Acc_AccountMaster> lst = new List<Acc_AccountMaster>();
            foreach (DataRow r in tb.Rows)
            {
                Guid accountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
                //DataTable tbJPDownloadUsers = Data.DAAcc_AccountJPUserRef.SelectByAccountID(accountID);
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static DataTable SelectByMembershipIdAsTable(Guid membershipId)
        {
            if (membershipId.IsNullOrEmptyGuid()) return null;
            return Data.DAAcc_AccountMaster.SelectByMembershipId(membershipId);
        }

        public static Guid CreateAccount(Guid managerUserMembershipId
            , String accountName
            , String companyName
            , String companyNameInRuby
            , String addedFromIp
            , String addedByEmail
            , String addedByName
            )
        {
            return Data.DAAcc_AccountMaster.CreateAccount(managerUserMembershipId,
                accountName,
                companyName,
                companyNameInRuby,
                addedFromIp,
                addedByEmail,
                addedByName
                );
        }



        //public static Guid Insert(String accountName, 
        //    Guid managerMembershipID,
        //    String companyName, 
        //    String companyNameInEnglish, 
        //    String companyInfoURL)
        //{
        //    DataRow r = Data.DAAcc_AccountMaster.Insert(accountName.Trim(), 
        //        managerMembershipID,
        //        companyName,
        //        companyNameInEnglish,
        //        companyInfoURL
        //        );
        //    if (r == null) return Guid.Empty;
        //    return ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
        //}

        public static Boolean Update(Guid accountID,
            String accountName,
            String companyName,
            String companyNameInRuby,
            Boolean IsCreateHistory)
        {
            //Account.Acc_AccountMaster acc = BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            //Boolean IsCreateHistory = false;
            //if (!acc.AccountName.Equals(accountName) || !acc.CompanyName.Equals(companyName) || !acc.CompanyNameInRuby.Equals(companyNameInRuby))
            //{
            //    IsCreateHistory = true;
            //    //App_Lib.AnrSso.ConnectSsoUser user = UIHelper.GetCurrentUserOrSignIn();
            //}
            return Data.DAAcc_AccountMaster.Update(accountID,
                accountName.Trim(),
                companyName.Trim(),
                companyNameInRuby,
                IsCreateHistory) > 0;
        }

        public static void UpdateNotes(Guid accountID, String additinalNotes)
        {
            if (accountID.IsNullOrEmptyGuid()) return;
            Data.DAAcc_AccountMaster.UpdateNotes(accountID, additinalNotes);
        }

        public static void VerifyAccount(Guid accountID, String verifiedBy, String internalComment)
        {
            if (accountID.IsNullOrEmptyGuid()) return;
            Data.DAAcc_AccountMaster.VerifyAccount(accountID, verifiedBy, internalComment);
        }

        public static void UnVerifyAccount(Guid accountID, String unVerifiedBy, String internalComment)
        {
            if (accountID.IsNullOrEmptyGuid()) return;
            Data.DAAcc_AccountMaster.UnVerifyAccount(accountID, unVerifiedBy, internalComment);
        }

        public static void AddUserToExistingAccount(Guid accountID, Guid membershipId, String role
            , String addedFromIP
            , String addedByEmail
            , String addedByName)
        {
            if (accountID.IsNullOrEmptyGuid()) return;
            if (membershipId.IsNullOrEmptyGuid()) return;
            if (role.IsNullOrEmptyString()) return;
            Data.DAAcc_AccountMaster.AddUserToExistingAccount(accountID, membershipId, role
                , addedFromIP
                , addedByEmail
                , addedByName);
        }

        public static Boolean DeleteByAccountID(Guid accountID)
        {
            return Data.DAAcc_AccountMaster.Delete(accountID) > 0;
        }

        public static DataTable UserMembership_SelectByAccountID_ForUser(Guid accountID)
        {

            return Data.DAAcc_AccountMaster.UserMembership_SelectByAccountID_ForUser(accountID);
        }

        public static DataTable UserMembership_SelectByAccountID_ForAdmin(Guid accountID)
        {

            return Data.DASec_UserMembership.SelectByAccountId(accountID);
        }

        public static Int32 SelectTotalAccounts(Guid membershipId)
        {
            return Data.DASec_UserAccountRole.SelectTotalAccounts(membershipId);
        }

        public static void UpdateAgreeTerms(Guid accountID)
        {
            Data.DAAcc_AccountMaster.UpdateAgreeTerms(accountID);
        }

        public static DataTable AccountVerifications_Select(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            return Data.DAAcc_AccountMaster.AccountVerifications_Select(accountID);
        }
        

        #region User update notification

        public static void UserAccUpdateNotifyAdmin(Guid accountID, Boolean IsAccountCreateHistory, Boolean IsProfileAddrCreateHistory, Security.Sec_UserMembership user, out String errorMsg)
        {
            Lib.Content.GlobalWebCultureGroup gwCultureGroup =
                  Lib.Content.GlobalWebCultureGroupBLL.SelectByCountryCode(ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdateNotifyAdminGroup"));
            int cultureGroupId = 1;
            if (gwCultureGroup != null) cultureGroupId = gwCultureGroup.CultureGroupId;
            errorMsg = String.Empty;

            #region " send email(s) to admin for notification"

            List<EM.EmailWebRef.ReplaceKeyValueType> kvListForAdmin = GetKeyValuesForAdmin(accountID, IsAccountCreateHistory, IsProfileAddrCreateHistory,user);
            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest email =
               new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdate.NotifyAdmin");
            email.DistributionListKey = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdate.DistributionKey");
            email.CultureGroupId = cultureGroupId;
            email.SubjectReplacementKeyValues = kvListForAdmin.ToArray();
            email.BodyReplacementKeyValues = kvListForAdmin.ToArray();
            EM.EmailWebRef.BaseResponseType resp = EM.EmailServiceManager.SendDistributedTemplatedEmail(email);
            if (resp != null) errorMsg = resp.ResponseMessage;

            #endregion
        }


        private static List<EM.EmailWebRef.ReplaceKeyValueType> GetKeyValuesForAdmin(Guid accountID,
           Boolean IsAccountCreateHistory, Boolean IsProfileAddrCreateHistory, Security.Sec_UserMembership user)
        {
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;

            #region " user and account information "
            if (user != null)
            {
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[MembershipID]]";
                kv.ReplacementValue = Convert.ToString(user.MembershipId);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
                kvList.Add(kv);
                if (OldAccountMaster != null)
                {
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-CompanyName]]";
                    kv.ReplacementValue =(OldAccountMaster.CompanyName!=acc.CompanyName)?string.Format("'{0}' changed to '{1}' ", OldAccountMaster.CompanyName, acc.CompanyName):acc.CompanyName;
                    kvList.Add(kv);
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-AccountName]]";
                    kv.ReplacementValue =(OldAccountMaster.AccountName!=acc.AccountName)? string.Format("'{0}' changed to '{1}' ", OldAccountMaster.AccountName, acc.AccountName):acc.AccountName;
                    kvList.Add(kv);
                }
                else
                {
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-CompanyName]]";
                    kv.ReplacementValue = acc.CompanyName;
                    kvList.Add(kv);
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-AccountName]]";
                    kv.ReplacementValue = acc.AccountName;
                    kvList.Add(kv);
                }
                

               
            }
            #endregion

            #region "Url information"
            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[AccountID]]";
            kv.ReplacementValue = Convert.ToString(accountID);
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[IsAccChanged]]";
            kv.ReplacementValue = IsAccountCreateHistory.ToString();
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[IsAddChanged]]";
            kv.ReplacementValue = IsProfileAddrCreateHistory.ToString();
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[BaseUrl]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);

            String adminNotificationUrlFormat =  ConfigUtility.AppSettingGetValue("Connect.Web.OrgUserUpdate.AdminGroupNotification") +
            "accid=" + "[[AccountID]]" + "&accchngstatus=" + "[[IsAccChanged]]" + "&addchngstatus=" + "[[IsAddChanged]]" + "&memid=" + "[[MembershipID]]";


            //StringBuilder notifyUrl = new StringBuilder();
            //notifyUrl.Append("<ul>");
            String notifyUrl = adminNotificationUrlFormat.Replace("[[AccountID]]", accountID.ToString()).
                 Replace("[[IsAccChanged]]", IsAccountCreateHistory.ToString()).Replace("[[IsAddChanged]]", IsProfileAddrCreateHistory.ToString()).Replace("[[MembershipID]]", user.MembershipId.ToString());
            String notificationLnk = String.Empty;
            //notifyUrl.AppendFormat("<a href='{0}'>User Update History</a>",
            //     adminNotificationUrlFormat.Replace("[[AccountID]]", accountID.ToString()).
            //     Replace("[[IsAccChanged]]", IsAccountCreateHistory.ToString()).Replace("[[IsAddChanged]]", IsProfileAddrCreateHistory.ToString()));
            //notifyUrl.Append("</ul>");
            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[AdminGroupNotificationUrl]]";
            kv.ReplacementValue = notifyUrl;
            kvList.Add(kv);
            #endregion

            return kvList;
        }
        #endregion

        public static DataTable SelectAccountsByPackage(long? packageID)
        {
            return Data.DAAcc_AccountMaster.SelectAccountsByPackageID(packageID);
        }

        public static String GenerateAddressString(DataRow drAccount, Boolean includePhone, Boolean includeFax)
        {
            if (null != drAccount) return String.Empty;

            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");
            //String strTemp = r["AddressName"].ToString();
            //if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            String strTemp = drAccount["Address1"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = drAccount["Address2"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = drAccount["CityTown"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>, ", strTemp);

            strTemp = drAccount["StateProvince"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>", strTemp);

            strTemp = drAccount["ZipPostalCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span>", strTemp);

            strTemp = drAccount["CountryCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span><br/>", strTemp);

            if (includePhone)
            {
                strTemp = drAccount["PhoneNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Phone: {0}</span><br/>", strTemp);
            }
            if (includeFax)
            {
                strTemp = drAccount["FaxNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Fax: {0}</span><br/>", strTemp);
            }
            sb.Append("</div>");
            return sb.ToString();
        }
    }
}
