﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLAcc_AccountMasterHistory
    {
        internal static Acc_AccountMaster_History InitFromData(DataRow r)
        {
            if (r == null) return null;
            Acc_AccountMaster_History obj = new Acc_AccountMaster_History();
            obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            obj.AccountName = ConvertUtility.ConvertNullToEmptyString(r["AccountName"]);
            obj.AccountStatusCode = ConvertUtility.ConvertNullToEmptyString(r["AccountStatusCode"]);
            obj.LangInput = ConvertUtility.ConvertNullToEmptyString(r["LangInput"]);
            obj.CompanyName = ConvertUtility.ConvertNullToEmptyString(r["CompanyName"]);
            obj.CompanyNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["CompanyNameInRuby"]);
            obj.CompanyAddressID = ConvertUtility.ConvertToInt32(r["CompanyAddressID"], 0);
                    
            obj.IsVerifiedAndApproved = ConvertUtility.ConvertToBoolean(r["IsVerifiedAndApproved"], false);
            obj.InfoUpdateRequired = ConvertUtility.ConvertToBoolean(r["InfoUpdateRequired"], false);
            obj.AgreedToTerms = ConvertUtility.ConvertToBoolean(r["AgreedToTerms"], false);
            obj.AgreedToTermsOnUTC = ConvertUtility.ConvertToDateTime(r["AgreedToTermsOnUTC"], DateTime.MinValue);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.CompanyAddress = String.Empty;
            //if (obj.CompanyAddressID > 0)
            //{
            //    obj.CompanyAddress = Lib.BLL.BLLProfile_Address.SelectAsStringByAddressID(obj.CompanyAddressID, false, false);
            //}
            return obj;
        }


        public static Acc_AccountMaster_History SelectAccountHistoryByID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            return InitFromData(Data.DAAcc_AccountMaster.SelectAccountHistoryByID(accountID));

        }
    }
}
