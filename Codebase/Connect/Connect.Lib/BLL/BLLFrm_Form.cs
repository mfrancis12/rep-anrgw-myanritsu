﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_Form
    {
        internal static Frm_Form InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_Form obj = new Frm_Form();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FormID = ConvertUtility.ConvertToGuid(r["FormID"], Guid.Empty);
            obj.ModuleID = ConvertUtility.ConvertToInt32(r["ModuleID"], 0);
            obj.FormName = ConvertUtility.ConvertNullToEmptyString(r["FormName"]);
            obj.FormType = ConvertUtility.ConvertNullToEmptyString(r["FormType"]);
            //obj.AfterSubmitURL = r["AfterSubmitURL"].ToString();
            obj.LockDelete = ConvertUtility.ConvertToBoolean(r["LockDelete"], false);
            obj.ModuleInfo = BLL.BLLSite_Module.SelectByModuleID(obj.ModuleID);
            return obj;
        }

        public static DataTable SelectAllAsTable()
        {
            return Data.DAFrm_Form.SelectAll();
        }

        public static List<Frm_Form> SelectAll()
        {
            DataTable tb = Data.DAFrm_Form.SelectAll();
            if (tb == null) return null;
            List<Frm_Form> lst = new List<Frm_Form>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static void SelectAll(out List<Frm_Form> addOnForms, out List<Frm_Form> feedbackForms)
        {
            addOnForms = feedbackForms = null;
            DataTable tb = Data.DAFrm_Form.SelectAll();
            if (tb == null) return;
            addOnForms = new List<Frm_Form>();
            feedbackForms = new List<Frm_Form>();
            foreach (DataRow r in tb.Rows)
            {
                Frm_Form f = InitFromData(r);
                if (f.IsFeedbackForm) feedbackForms.Add(f);
                else addOnForms.Add(f);
            }
        }

        public static Frm_Form SelectByModuleID(Int32 moduleID, Boolean createIfNew)
        {
            return InitFromData(Data.DAFrm_Form.SelectByModuleID(moduleID, createIfNew));
        }

        public static Frm_Form SelectByFormID(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return null;
            return InitFromData(Data.DAFrm_Form.SelectByFormID(formID));
        }

        public static DataTable SelectBy_FormID(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return null;
            return Data.DAFrm_Form.SelectBy_FormID(formID);
        }

        public static String ResClassKey(Guid formID)
        {
            return Data.DAFrm_Form.ResClassKey(formID);
        }

        public static void Update(Guid formID, String formName)
        {
            Data.DAFrm_Form.Update(formID, formName);
        }

        public static Guid Insert(String formName, String formType)
        {
            DataRow r = Data.DAFrm_Form.Insert(formName, formType);
            if (r == null) return Guid.Empty;
            return ConvertUtility.ConvertToGuid(r["FormID"], Guid.Empty);
        }

        public static void Delete(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return;
            Data.DAFrm_Form.Delete(formID);
        }

        public static DataTable SelectModelsByFormID(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return null;
            return Data.DAFrm_Form.SelectModelsByFormID(formID);
        }
    }
}
