﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;
namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormControl
    {
        private static List<Frm_FormControl> _FormControls = null;
        public static List<Frm_FormControl> FormControls
        {
            get {
                if (_FormControls == null) _FormControls = SelectAll();
                return _FormControls;
            }
        }

        internal static Frm_FormControl InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormControl obj = new Frm_FormControl();
            obj.ControlID = ConvertUtility.ConvertToInt32(r["ControlID"], 0);
            obj.ControlName = ConvertUtility.ConvertNullToEmptyString(r["ControlName"]);
            obj.ControlPath = ConvertUtility.ConvertNullToEmptyString(r["ControlPath"]);
            obj.IsDataSourceRequired = ConvertUtility.ConvertToBoolean(r["IsDataSourceRequired"], false);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<Frm_FormControl> SelectAll()
        {
            DataTable tb = Data.DAFrm_FormControl.SelectAll();
            if (tb == null) return null;
            List<Frm_FormControl> lst = new List<Frm_FormControl>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Frm_FormControl SelectByControlID(Int32 controlID)
        {
            if (controlID < 1) return null;
            return InitFromData(Data.DAFrm_FormControl.SelectByControlID(controlID));
        }
    }
}
