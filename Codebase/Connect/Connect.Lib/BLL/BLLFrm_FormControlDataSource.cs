﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;
namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormControlDataSource
    {
        internal static Frm_FormControlDataSource InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormControlDataSource obj = new Frm_FormControlDataSource();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.DataSourceID = ConvertUtility.ConvertToInt32(r["DataSourceID"], 0);
            obj.DataSourceName = ConvertUtility.ConvertNullToEmptyString(r["DataSourceName"]);
            obj.DataItems = BLL.BLLFrm_FormControlDataSourceData.SelectByDataSourceID(obj.DataSourceID);
            return obj;
        }

        public static String ResClassKey(Int32 dataSourceID)
        {
            return Data.DAFrm_FormControlDataSource.ResClassKey(dataSourceID);
        }

        public static List<Frm_FormControlDataSource> SelectAll()
        {
            List<Frm_FormControlDataSource> lst = new List<Frm_FormControlDataSource>();
            DataTable tb = Data.DAFrm_FormControlDataSource.SelectAll();
            if (tb == null) return null;
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static Frm_FormControlDataSource SelectByDataSourceID(Int32 dataSourceID)
        {

            return InitFromData(Data.DAFrm_FormControlDataSource.SelectByDataSourceID(dataSourceID));
        }

        public static Int32 Insert(String dataSourceName)
        {
            if (dataSourceName.IsNullOrEmptyString()) throw new ArgumentNullException("dataSourceName");
            DataRow r = Data.DAFrm_FormControlDataSource.Insert(dataSourceName.Trim());
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["DataSourceID"], 0);
        }

        public static void Update(Int32 dataSourceID, String dataSourceName)
        {
            if (dataSourceID < 1) return;
            Data.DAFrm_FormControlDataSource.Update(dataSourceID, dataSourceName.Trim());
        }

        public static Boolean DeleteByDataSourceID(Int32 dataSourceID)
        {
            return Data.DAFrm_FormControlDataSource.DeleteByDataSourceID(dataSourceID) > 0;

        }
    }
}
