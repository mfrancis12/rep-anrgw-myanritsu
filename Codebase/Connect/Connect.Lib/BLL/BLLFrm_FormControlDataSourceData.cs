﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormControlDataSourceData
    {
        internal static Frm_FormControlDataSourceData InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormControlDataSourceData obj = new Frm_FormControlDataSourceData();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.DataSourceID = ConvertUtility.ConvertToInt32(r["DataSourceID"], 0);
            obj.DSDataID = ConvertUtility.ConvertToInt32(r["DSDataID"], 0);
            obj.IsSelected = ConvertUtility.ConvertToBoolean(r["IsSelected"], false);
            obj.OrderNum = ConvertUtility.ConvertToInt32(r["OrderNum"], 0);
            return obj;
        }

        public static Frm_FormControlDataSourceData SelectByDSDataID(Int32 dsDataID)
        {
            return InitFromData(Data.DAFrm_FormControlDataSourceData.SelectByDSDataID(dsDataID));
        }

        public static List<Frm_FormControlDataSourceData> SelectByDataSourceID(Int32 dataSourceID)
        {
            DataTable tb = Data.DAFrm_FormControlDataSourceData.SelectByDataSourceID(dataSourceID);
            if (tb == null) return null;
            List<Frm_FormControlDataSourceData> lst = new List<Frm_FormControlDataSourceData>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Int32 Insert(Int32 dataSourceID, Boolean isSelected, Int32 orderNum)
        {
            DataRow r = Data.DAFrm_FormControlDataSourceData.Insert(dataSourceID, isSelected, orderNum);
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["DSDataID"], 0);

        }

        public static void Update(Int32 dsDataID, Boolean isSelected, Int32 orderNum)
        {
            Data.DAFrm_FormControlDataSourceData.Update(dsDataID, isSelected, orderNum);
        }

        public static Boolean DeleteByDSDataID(Int32 dsDataID, Int32 datasourceID)
        {
            return Data.DAFrm_FormControlDataSourceData.DeleteByDSDataID(dsDataID, datasourceID) > 0;

        }

        public static String ResKeyItemText(Int32 dsDataID)
        {
            return Data.DAFrm_FormControlDataSourceData.ResKeyItemText(dsDataID);
        }

        public static String ResKeyItemValue(Int32 dsDataID)
        {
            return Data.DAFrm_FormControlDataSourceData.ResKeyItemValue(dsDataID);
        }
    }
}
