﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormFieldset
    {
        internal static Frm_FormFieldset InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormFieldset obj = new Frm_FormFieldset();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FieldsetID = ConvertUtility.ConvertToInt32(r["FieldsetID"], 0);
            obj.FormID = ConvertUtility.ConvertToGuid(r["FormID"], Guid.Empty);
            obj.IsInternal = ConvertUtility.ConvertToBoolean(r["IsInternal"], false);
            obj.OrderNum = ConvertUtility.ConvertToInt32(r["OrderNum"], 0);
            return obj;
        }

        public static Frm_FormFieldset SelectByFieldsetID(Int32 fieldsetID)
        {
            return InitFromData(Data.DAFrm_FormFieldset.SelectByFieldsetID(fieldsetID));
        }

        public static List<Frm_FormFieldset> SelectByFormID(Guid formID)
        {
            DataTable tb = Data.DAFrm_FormFieldset.SelectByFormID(formID);
            if (tb == null) return null;
            List<Frm_FormFieldset> lst = new List<Frm_FormFieldset>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static List<Frm_FormFieldset> SelectByModuleID(Int32 moduleID)
        {
            DataTable tb = Data.DAFrm_FormFieldset.SelectByModuleID(moduleID);
            if (tb == null) return null;
            List<Frm_FormFieldset> lst = new List<Frm_FormFieldset>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Int32 Insert(Guid formID, Int32 orderNum, Boolean isInternal, String legendInEnglish)
        {
            return Data.DAFrm_FormFieldset.Insert(formID, orderNum, isInternal, legendInEnglish);
        }

        public static void Update(Int32 fieldsetID, Int32 orderNum, Boolean isInternal)
        {
            Data.DAFrm_FormFieldset.Update(fieldsetID, orderNum, isInternal);
        }

        public static Boolean DeleteByFieldsetID(Int32 fieldsetID)
        {
            return Data.DAFrm_FormFieldset.DeleteByFieldsetID(fieldsetID) > 0;
        }

        public static String GetResourceResKeyPrefix(Int32 fieldsetID)
        {
            return Data.DAFrm_FormFieldset.GetResourceResKeyPrefix(fieldsetID);
        }
    }
}
