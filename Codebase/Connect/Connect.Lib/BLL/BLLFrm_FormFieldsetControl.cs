﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormFieldsetControl
    {
        internal static Frm_FormFieldsetControl InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormFieldsetControl obj = new Frm_FormFieldsetControl();
            obj.ControlID = ConvertUtility.ConvertToInt32(r["ControlID"], 0);
            obj.ControlOrder = ConvertUtility.ConvertToInt32(r["ControlOrder"], 0);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FieldsetID = ConvertUtility.ConvertToInt32(r["FieldsetID"], 0);
            obj.FwscID = ConvertUtility.ConvertToInt32(r["FwscID"], 0);
            obj.FormID = ConvertUtility.ConvertToGuid(r["FormID"], Guid.Empty);
            obj.IsRequired = ConvertUtility.ConvertToBoolean(r["IsRequired"], false);
            obj.Properties = BLLFrm_FormFieldsetControlProperty.SelectByFwscID(obj.FwscID);
            obj.ControlInfo = BLLFrm_FormControl.SelectByControlID(obj.ControlID);
            obj.DataSourceID = ConvertUtility.ConvertToInt32(r["DataSourceID"], 0);
            obj.FieldName = r["FieldName"].ToString();
            return obj;
        }

        public static Frm_FormFieldsetControl SelectByFwscID(Int32 fwscID)
        {
            if (fwscID < 1) return null;
            return InitFromData(Data.DAFrm_FormFieldsetControl.SelectByFwscID(fwscID));
        }

        public static Frm_FormFieldsetControl SelectByControlID(Int32 controlID)
        {
            return InitFromData(Data.DAFrm_FormFieldsetControl.SelectByControlID(controlID));
        }

        public static List<Frm_FormFieldsetControl> SelectByFieldsetID(Int32 fieldsetID)
        {
            DataTable tb = Data.DAFrm_FormFieldsetControl.SelectByFieldsetID(fieldsetID);
            if (tb == null) return null;
            List<Frm_FormFieldsetControl> lst = new List<Frm_FormFieldsetControl>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Int32 Insert(Int32 fieldsetID, Int32 controlID, Int32 controlOrder, Boolean isRequired
            , Guid formID, String engDisplayText, String engReportText, String fieldName)
        {
            Int32 fwscID = Data.DAFrm_FormFieldsetControl.Insert(fieldsetID, controlID, controlOrder, isRequired, fieldName);
            if (fwscID > 0)
            {
                String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(formID);
                String resKeyDisplayText = GenerateResourceKey(fieldsetID, fwscID, "DisplayText");
                String resKeyReportText = GenerateResourceKey(fieldsetID, fwscID, "ReportText");
                Data.DARes_ContentStore.Insert(classKey, resKeyDisplayText, "en", engDisplayText.Trim(), true);
                Data.DARes_ContentStore.Insert(classKey, resKeyReportText, "en", engReportText.Trim(), true);
            }
            return fwscID;
        }

        public static void Update(Int32 fwscID, Int32 controlOrder, Boolean isRequired, Int32 dataSourceID, String fieldName)
        {
            Data.DAFrm_FormFieldsetControl.Update(fwscID, controlOrder, isRequired, dataSourceID, fieldName);
        }

        public static Boolean Delete(Int32 fwscID)
        {
            return Data.DAFrm_FormFieldsetControl.Delete(fwscID) > 0;
        }

        public static String GenerateResourceKey(Int32 fieldsetID, Int32 fwscID, String control)
        {
            return Data.DAFrm_FormFieldsetControl.GenerateResourceKey(fieldsetID, fwscID, control);
                // String.Format("{0}_FwscID_{1}_{2}", Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(fieldsetID), fwscID, control);
        }
    }
}
