﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.DynamicForm;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormFieldsetControlProperty
    {
        internal static Frm_FormFieldsetControlProperty InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormFieldsetControlProperty obj = new Frm_FormFieldsetControlProperty();
            obj.FwscID = ConvertUtility.ConvertToInt32(r["FwscID"], 0);
            obj.PropertyID = ConvertUtility.ConvertToInt32(r["PropertyID"], 0);
            obj.PropertyKey = ConvertUtility.ConvertNullToEmptyString(r["PropertyKey"]);
            obj.PropertyValue = ConvertUtility.ConvertNullToEmptyString(r["PropertyValue"]);
            return obj;
        }

        public static List<Frm_FormFieldsetControlProperty> SelectByFwscID(Int32 fwscID)
        {
            DataTable tb = Data.DAFrm_FormFieldsetControlProperty.SelectByFwscID(fwscID);
            if (tb == null) return null;
            List<Frm_FormFieldsetControlProperty> lst = new List<Frm_FormFieldsetControlProperty>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static void InsertUpdate(Int32 fwscID, String propertyKey, String propertyValue)
        {
            Data.DAFrm_FormFieldsetControlProperty.InsertUpdate(fwscID, propertyKey.Trim(), propertyValue.Trim());
        }

        public static Boolean DeleteByPropertyID(Int32 propertyID)
        {
            return Data.DAFrm_FormFieldsetControlProperty.DeleteByPropertyID(propertyID) > 0;
        }

        public static Frm_FormFieldsetControlProperty Find(List<Frm_FormFieldsetControlProperty> lst, String propertyKey)
        {
            if (lst == null || propertyKey.IsNullOrEmptyString()) return null;
            var pty = from p in lst
                      where p.PropertyKey.Equals(propertyKey, StringComparison.InvariantCultureIgnoreCase)
                      select p;
            return pty.FirstOrDefault <Frm_FormFieldsetControlProperty>();
        }
    }
}
