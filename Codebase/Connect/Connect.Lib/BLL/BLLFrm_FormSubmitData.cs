﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.Connect.Lib.DynamicForm;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormSubmitData
    {
        internal static Frm_FormSubmitData InitFromData(DataRow r)
        {
            if (r == null) return null;
            Frm_FormSubmitData obj = new Frm_FormSubmitData();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FieldKey = ConvertUtility.ConvertNullToEmptyString(r["FieldKey"]);
            obj.FormSubmitID = ConvertUtility.ConvertToInt32(r["FormSubmitID"], 0);
            obj.FwscID = ConvertUtility.ConvertToInt32(r["FwscID"], 0);
            obj.LastUpdatedOnUTC = ConvertUtility.ConvertToDateTime(r["LastUpdatedOnUTC"], DateTime.MinValue);
            obj.SubmitDataID = ConvertUtility.ConvertToInt32(r["SubmitDataID"], 0);
            obj.SubmittedUser = ConvertUtility.ConvertNullToEmptyString(r["SubmittedUser"]);
            obj.SubmittedValueBinary = ConvertUtility.ConvertNullToEmptyString(r["SubmittedValueBinary"]);
            obj.SubmittedValueStr = ConvertUtility.ConvertNullToEmptyString(r["SubmittedValueStr"]);
            return obj;
        }
    }
}
