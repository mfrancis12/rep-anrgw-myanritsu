﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.Connect.Lib.DynamicForm;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLFrm_FormSubmitMaster
    {
        //internal static Frm_FormSubmitMaster_ToBeDeleted InitFromData(DataRow r)
        //{
        //    if (r == null) return null;
        //    Frm_FormSubmitMaster_ToBeDeleted obj = new Frm_FormSubmitMaster_ToBeDeleted();
        //    obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
        //    obj.FormID = ConvertUtility.ConvertToGuid(r["FormID"], Guid.Empty);
        //    obj.FormSubmitID = ConvertUtility.ConvertToInt32(r["FormSubmitID"], 0);
        //    obj.FormSubmitKey = ConvertUtility.ConvertToGuid(r["FormSubmitKey"], Guid.Empty);
        //    obj.InitialSubmitUser = ConvertUtility.ConvertNullToEmptyString(r["InitialSubmitUser"]);
        //    obj.LastUpdatedOnUTC = ConvertUtility.ConvertToDateTime(r["LastUpdatedOnUTC"], DateTime.MinValue);
        //    return obj;
        //}

        public static Boolean Insert(Guid formID
            , String formInternalName
            , String submittedUserEmail
            , Guid submittedAccountID
            , Dictionary<String, String> keyValues
            , out Int32 formSubmitID
            , out Guid formSubmitKey)
        {
            formSubmitID = 0;
            formSubmitKey = Guid.Empty;
            
            if (formID.IsNullOrEmptyGuid()) throw new ArgumentNullException("formID");
            if (submittedUserEmail.IsNullOrEmptyString()) throw new ArgumentNullException("submittedUserEmail");
            if (keyValues == null || keyValues.Count < 1) throw new ArgumentNullException("keyValues");

            DataRow r = Data.DAFrm_FormSubmitMaster.Insert(formID, formInternalName, submittedUserEmail, submittedAccountID, keyValues);
            if (r == null) return false;
            formSubmitID = ConvertUtility.ConvertToInt32(r["FormSubmitID"], 0);
            formSubmitKey = ConvertUtility.ConvertToGuid(r["FormSubmitKey"], Guid.Empty);
            return formSubmitID > 0;
        }

       
    }
}
