﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLIso_Country
    {
        public const String ISOCountryNameClassKey = "ISOCountryNames";

        public static DataTable SelectAll()
        {
            return Data.DAIso_Country.SelectAll(); 
        }

        public static Int32 GetCultureGroupIDByCultureCode(String llCC)
        {
            return Data.DAIso_Country.GetCultureGroupIDByCultureCode(llCC);
        }

        public static string GetDefaultCultureCode(string cultureCode)
        {
            return Data.DAIso_Country.GetDefaultCultureCode(cultureCode);
        }

        public static DataRow GetISOCountryInfoByCountryCode(String alpha2)
        {
            return Data.DAIso_Country.GetISOCountryInfoByCountryCode(alpha2);
        }

        public static string   GetIsoCountryInfoByBrowserLang(String alpha2)
        {
            var dataRow=Data.DAIso_Country.GetISOCountryInfoByCountryCode(alpha2);
            if (dataRow == null) return string.Empty;
            return ConvertUtility.ConvertToString(dataRow["CultureCode"], String.Empty);
        }

        public static String GetCultureCodeByCountry(String countryCodeAlpha2)
        {
            DataRow rCountryInfo = Data.DAIso_Country.GetISOCountryInfoByCountryCode(countryCodeAlpha2);
            if (rCountryInfo == null) return "en-US";
            return rCountryInfo["CultureCode"].ToString();
        }

        public static void GetCultureGroupInfoByCountry(String countryCodeAlpha2, out int cultureGroupId, out String cultureCode)
        {
            cultureGroupId = 1;
            cultureCode = "en-US";

            DataRow rCountryInfo = Data.DAIso_Country.GetISOCountryInfoByCountryCode(countryCodeAlpha2);
            if (rCountryInfo != null)
            {
                cultureCode = rCountryInfo["CultureCode"].ToString();
                cultureGroupId = ConvertUtility.ConvertToInt32(rCountryInfo["DefaultCultureGroupId"], 1);
            }
        }
    }
}
