﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLProfile_Address
    {
        internal static Profile_Address InitFromData(DataRow r)
        {
            if (r == null) return null;
            Profile_Address obj = new Profile_Address();
            obj.Address1 = ConvertUtility.ConvertNullToEmptyString(r["Address1"]);
            obj.Address2 = ConvertUtility.ConvertNullToEmptyString(r["Address2"]);
            obj.CityTown = ConvertUtility.ConvertNullToEmptyString(r["CityTown"]);
            obj.CountryCode = ConvertUtility.ConvertNullToEmptyString(r["CountryCode"]);
            obj.FaxNumber = ConvertUtility.ConvertNullToEmptyString(r["FaxNumber"]);
            obj.PhoneNumber = ConvertUtility.ConvertNullToEmptyString(r["PhoneNumber"]);
            obj.StateProvince = ConvertUtility.ConvertNullToEmptyString(r["StateProvince"]);
            obj.ZipPostalCode = ConvertUtility.ConvertNullToEmptyString(r["ZipPostalCode"]);
            return obj;
        }       
        public static List<Profile_Address> SelectByAccountID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DataTable tb = Data.DAProfile_Address.SelectByAccountID(accountID);
            if (tb == null) return null;
            List<Profile_Address> lst = new List<Profile_Address>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;

        }

        public static Profile_Address SelectByMemberShipID(Guid ownerMemberShipID)
        {
            if (ownerMemberShipID.IsNullOrEmptyGuid()) return null;
            return InitFromData(Data.DAProfile_Address.SelectByMemberShipID(ownerMemberShipID));
        }

        public static String SelectAsStringByMemberShipID(Guid ownerMemberShipID)
        {
            return SelectAsStringByMemberShipID(ownerMemberShipID, true, true);
        }

        public static String SelectHistoryAsStringByAccountID(Guid accountID)
        {
            return SelectHistoryAsStringByAccountID(accountID, false, false);
        }

        public static String SelectAsStringByMemberShipID(Guid ownerMemberShipID, Boolean includePhone, Boolean includeFax)
        {
            if (ownerMemberShipID.IsNullOrEmptyGuid()) return String.Empty;
            DataRow r = Data.DAProfile_Address.SelectByMemberShipID(ownerMemberShipID);
            if (r == null) return String.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");

            String strTemp = r["Address1"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = r["Address2"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = r["CityTown"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>, ", strTemp);

            strTemp = r["StateProvince"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>", strTemp);

            strTemp = r["ZipPostalCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span>", strTemp);

            strTemp = r["CountryCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span><br/>", strTemp);

            if (includePhone)
            {
                strTemp = r["PhoneNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Phone: {0}</span><br/>", strTemp);
            }
            if (includeFax)
            {
                strTemp = r["FaxNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Fax: {0}</span><br/>", strTemp);
            }
            sb.Append("</div>");
            return sb.ToString();
        }

        public static String SelectHistoryAsStringByAccountID(Guid accountID, Boolean includePhone, Boolean includeFax)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DataRow r = Data.DAProfile_Address.SelectHistoryByAccountID(accountID);
            if (r == null) return String.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");
            String strTemp = r["AddressName"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = r["Address1"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = r["Address2"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span><br/>", strTemp);

            strTemp = r["CityTown"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>, ", strTemp);

            strTemp = r["StateProvince"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>{0}</span>", strTemp);

            strTemp = r["ZipPostalCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span>", strTemp);

            strTemp = r["CountryCode"].ToString();
            if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("&nbsp;<span class='addrtext'>{0}</span><br/>", strTemp);

            if (includePhone)
            {
                strTemp = r["PhoneNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Phone: {0}</span><br/>", strTemp);
            }
            if (includeFax)
            {
                strTemp = r["FaxNumber"].ToString();
                if (!strTemp.IsNullOrEmptyString()) sb.AppendFormat("<span class='addrtext'>Fax: {0}</span><br/>", strTemp);
            }
            sb.Append("</div>");
            return sb.ToString();
        }

        public static Profile_Address SelectByEmailID(String emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress)) return null;
            return InitFromData(Data.DAProfile_Address.SelectByEmailID(emailAddress));
        }
    }
}
