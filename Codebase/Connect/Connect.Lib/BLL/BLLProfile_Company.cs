﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Reflection;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Lib.BLL
{
    //public static class BLLProfile_Company
    //{
    //    internal static Profile_Company InitFromData(DataRow r)
    //    {
    //        if (r == null) return null;
    //        Profile_Company obj = new Profile_Company();
    //        obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
    //        obj.AnualSalesAmount = ConvertUtility.ConvertNullToEmptyString(r["AnualSalesAmount"]);
    //        obj.Capital = ConvertUtility.ConvertNullToEmptyString(r["Capital"]);
    //        obj.CompanyAddressID = ConvertUtility.ConvertToInt32(r["CompanyAddressID"], 0);
    //        obj.CompanyInfoURL = ConvertUtility.ConvertNullToEmptyString(r["CompanyInfoURL"]);
    //        obj.CompanyName = ConvertUtility.ConvertNullToEmptyString(r["CompanyName"]);
    //        obj.CompanyNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["CompanyNameInEnglish"]);
    //        obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
    //        obj.HeadOfficeAddressID = ConvertUtility.ConvertToInt32(r["HeadOfficeAddressID"], 0);
    //        obj.IsImporterStockRentalBusiness = ConvertUtility.ConvertToBoolean(r["IsImporterStockRentalBusiness"], false);
    //        obj.MainBusinessActivity = ConvertUtility.ConvertNullToEmptyString(r["MainBusinessActivity"]);
    //        obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
    //        obj.NameOfPresident = ConvertUtility.ConvertNullToEmptyString(r["NameOfPresident"]);
    //        obj.NumberOfEmployees = ConvertUtility.ConvertToInt32(r["NumberOfEmployees"], 0);
    //        return obj;
    //    }

    //    public static Boolean DeleteByAccountID(Guid accountID)
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return false;
    //        return Data.DAProfile_Company.DeleteByAccountID(accountID) > 0;

    //    }

    //    public static Guid CreateNewAccount(Guid managerMemebershipId
    //        , String companyName
    //       , String companyNameInEnglish
    //       , Profile_Address companyAddress
    //       , Profile_Address headOfficeAddress
    //       , String companyInfoURL
    //       , String mainBusinessActivity
    //       , Boolean isImporterStockRentalBusiness
    //       , String nameOfPresident
    //       , String capital
    //       , Int32 numberOfEmployees
    //       , String anualSalesAmount
    //       )
    //    {
    //        Guid accountID = Data.DAProfile_Company.CreateAccountAndCompanyProfile(managerMemebershipId
    //        ,  companyName
    //           , companyNameInEnglish
    //           , companyAddress.AddressName
    //           , companyAddress.Address1
    //           , companyAddress.Address2
    //           , companyAddress.CityTown
    //           , companyAddress.StateProvince
    //           , companyAddress.ZipPostalCode
    //           , companyAddress.CountryCode
    //           , companyAddress.PhoneNumber
    //           , companyAddress.FaxNumber

    //           , headOfficeAddress.AddressName
    //           , headOfficeAddress.Address1
    //           , headOfficeAddress.Address2
    //           , headOfficeAddress.CityTown
    //           , headOfficeAddress.StateProvince
    //           , headOfficeAddress.ZipPostalCode
    //           , headOfficeAddress.CountryCode
    //           , headOfficeAddress.PhoneNumber
    //           , headOfficeAddress.FaxNumber
    //           , companyInfoURL
    //           , mainBusinessActivity
    //           , isImporterStockRentalBusiness
    //           , nameOfPresident
    //           , capital
    //           , numberOfEmployees
    //           , anualSalesAmount);

    //        return accountID;
    //    }

    //    public static void InsertUpdate(Guid accountID
    //        , String companyName
    //        , String companyNameInEnglish
    //        , Profile_Address companyAddress
    //        , Profile_Address headOfficeAddress
    //        , String companyInfoURL
    //        , String mainBusinessActivity
    //        , Boolean isImporterStockRentalBusiness
    //        , String nameOfPresident
    //        , String capital
    //        , Int32 numberOfEmployees
    //        , String anualSalesAmount
    //        )
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return;
    //        Int32 companyAddressID = BLL.BLLProfile_Address.InsertUpdate(companyAddress);
    //        Int32 headOfficeAddressID = BLL.BLLProfile_Address.InsertUpdate(headOfficeAddress);
    //        Profile_Company existingProfile = SelectByAccountID(accountID);
    //        if (existingProfile == null)//new
    //        {

    //            Data.DAProfile_Company.Insert(accountID
    //                , companyName
    //                , companyNameInEnglish
    //                , companyAddressID
    //                , headOfficeAddressID
    //                , companyInfoURL
    //                , mainBusinessActivity
    //                , isImporterStockRentalBusiness
    //                , nameOfPresident
    //                , capital
    //                , numberOfEmployees
    //                , anualSalesAmount);
    //        }
    //        else
    //        {
    //            Data.DAProfile_Company.Update(accountID
    //               , companyName
    //               , companyNameInEnglish
    //               , companyAddressID
    //               , headOfficeAddressID
    //               , companyInfoURL
    //               , mainBusinessActivity
    //               , isImporterStockRentalBusiness
    //               , nameOfPresident
    //               , capital
    //               , numberOfEmployees
    //               , anualSalesAmount);
    //        }
    //    }

    //    public static void Insert(Guid accountID
    //        , String companyName
    //        , String companyNameInEnglish
    //        , Int32 companyAddressID
    //        , Int32 headOfficeAddressID
    //        , String companyInfoURL
    //        , String mainBusinessActivity
    //        , Boolean isImporterStockRentalBusiness
    //        , String nameOfPresident
    //        , String capital
    //        , Int32 numberOfEmployees
    //        , String anualSalesAmount
    //        )
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return;
    //        Data.DAProfile_Company.Insert(accountID
    //            , companyName
    //            , companyNameInEnglish
    //            , companyAddressID
    //            , headOfficeAddressID
    //            , companyInfoURL
    //            , mainBusinessActivity
    //            , isImporterStockRentalBusiness
    //            , nameOfPresident
    //            , capital
    //            , numberOfEmployees
    //            , anualSalesAmount);

    //    }

    //    public static void Update(Guid accountID
    //        , String companyName
    //        , String companyNameInEnglish
    //        , Int32 companyAddressID
    //        , Int32 headOfficeAddressID
    //        , String companyInfoURL
    //        , String mainBusinessActivity
    //        , Boolean isImporterStockRentalBusiness
    //        , String nameOfPresident
    //        , String capital
    //        , Int32 numberOfEmployees
    //        , String anualSalesAmount
    //        )
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return;
    //        Data.DAProfile_Company.Update(accountID
    //            , companyName
    //            , companyNameInEnglish
    //            , companyAddressID
    //            , headOfficeAddressID
    //            , companyInfoURL
    //            , mainBusinessActivity
    //            , isImporterStockRentalBusiness
    //            , nameOfPresident
    //            , capital
    //            , numberOfEmployees
    //            , anualSalesAmount);

    //    }

    //    public static Profile_Company SelectByAccountID(Guid accountID)
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return null;
    //        return InitFromData(Data.DAProfile_Company.SelectByAccountID(accountID));

    //    }

    //    public static DataTable SelectByAccountIDAsDataTable(Guid accountID)
    //    {
    //        if (accountID.IsNullOrEmptyGuid()) return null;
    //        DataRow r= Data.DAProfile_Company.SelectByAccountID(accountID);
    //        if (r == null) return null;
    //        return r.Table;

    //    }

    //    public static List<KeyValuePair<String, String>> ExtractCompanyProfileData(Profile_Company cp)
    //    {
    //        if (cp == null) return null;
    //        List<KeyValuePair<String, String>> lst = new List<KeyValuePair<String, String>>();
    //        KeyValuePair<String, String> kv = new KeyValuePair<string,string>();
    //        Type cpType = cp.GetType();
    //        PropertyInfo[] properties = cpType.GetProperties();
    //        if (properties == null) return null;
    //        foreach (PropertyInfo pi in properties)
    //        {
    //            String key = pi.Name;
    //            Object value = pi.GetValue(cp, null);
    //            if (pi.Name == "HeadOfficeAddressID" || pi.Name == "CompanyAddressID")
    //            {
    //                Lib.Account.Profile_Address addr = Lib.BLL.BLLProfile_Address.SelectByAddressID(ConvertUtility.ConvertToInt32(value, 0));
    //                if (addr != null)
    //                {
    //                    value = addr.AddressText;   
    //                }
    //            }

                
    //            kv = new KeyValuePair<string,string>(key, value.ToString());
    //            lst.Add(kv);
    //        }
    //        return lst;
    //    }
        
        
    //}
}
