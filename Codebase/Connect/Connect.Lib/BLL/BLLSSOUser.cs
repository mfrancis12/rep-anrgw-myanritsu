﻿using Anritsu.Connect.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSSOUser
    {
        public static DataRow SelectByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
                return DASSOUser.SelectByEmailOrUserName(emailAddressOrUserName);
        }
    }
}
