﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Security;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSec_Role
    {
        internal static Sec_Role InitFromData(DataRow r)
        {
            if (r == null) return null;
            Sec_Role obj = new Sec_Role();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.Role = ConvertUtility.ConvertNullToEmptyString(r["Role"]);
            obj.RoleName = ConvertUtility.ConvertNullToEmptyString(r["RoleName"]);
            
            return obj;
        }

        public static List<Sec_Role> Select(Guid membershipId, Guid accountID)
        {
            DataTable tb = Data.DASec_UserAccountRole.Select(membershipId, accountID);
            if (tb == null) return null;
            List<Sec_Role> lst = new List<Sec_Role>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static List<String> SelectJustRoles(Guid membershipId, Guid accountID)
        {
            DataTable tb = Data.DASec_UserAccountRole.Select(membershipId, accountID);
            if (tb == null) return null;
            List<String> lst = new List<String>();
            foreach (DataRow r in tb.Rows) lst.Add(r["Role"].ToString());
            return lst;
        }

        public static DataTable SelectJustRolesAsTable(Guid membershipId, Guid accountID)
        {
            return Data.DASec_UserAccountRole.Select(membershipId, accountID);
            
        }

        public static List<Sec_Role> SelectAll()
        {
            DataTable tb = Data.DASec_UserAccountRole.Sec_Role_SelectAll();
            if (tb == null) return null;
            List<Sec_Role> lst = new List<Sec_Role>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static List<String> SelectAllJustRoles()
        {
            DataTable tb = Data.DASec_UserAccountRole.SelectAll();
            if (tb == null) return null;
            List<String> lst = new List<String>();
            foreach (DataRow r in tb.Rows) lst.Add(r["Role"].ToString());
            if (!lst.Contains("siteadmin")) lst.Add("siteadmin");
            return lst;
        }

        //public static Boolean CanApproveProfileWorkflows(Guid membershipId, Guid anritsuMasterAccountID)
        //{
        //    if (membershipId.IsNullOrEmptyGuid()) return false;
        //    Boolean isValid = false;
        //    List<Lib.Account.Acc_AccountMaster> accList = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(membershipId);
        //    if (accList != null)
        //    {
        //        foreach (Lib.Account.Acc_AccountMaster acc in accList)
        //        {
        //            if (acc.AccountID == anritsuMasterAccountID)
        //            {
        //                List<string> roles = Lib.BLL.BLLSec_Role.SelectJustRoles(membershipId, acc.AccountID);
        //                if (roles.Contains("siteadmin") || roles.Contains("pfapprover") || roles.Contains("pfadmin"))
        //                {
        //                    isValid = true;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return isValid;

        //}

        //public static Boolean IsProfileAdmin(Guid membershipId, Guid anritsuMasterAccountID)
        //{
        //    if (membershipId.IsNullOrEmptyGuid()) return false;
        //    Boolean isValid = false;
        //    List<Lib.Account.Acc_AccountMaster> accList = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(membershipId);
        //    if (accList != null)
        //    {
        //        foreach (Lib.Account.Acc_AccountMaster acc in accList)
        //        {
        //            if (acc.AccountID == anritsuMasterAccountID)
        //            {
        //                List<string> roles = Lib.BLL.BLLSec_Role.SelectJustRoles(membershipId, acc.AccountID);
        //                if (roles.Contains("siteadmin") || roles.Contains("pfadmin"))
        //                {
        //                    isValid = true;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return isValid;

        //}

        public static Boolean Delete(Int32 userAccountRefID)
        {
            return Data.DASec_UserAccountRole.Delete(userAccountRefID) > 0;
        }

        public static void Delete(Guid membershipId, Guid accountID, String role)
        {
            Data.DASec_UserAccountRole.Delete(membershipId, accountID, role);
        }

        public static void Delete(Guid membershipId, Guid accountID)
        {
            Data.DASec_UserAccountRole.Delete(membershipId, accountID);
        }

        public static void Insert(Guid membershipId, Guid accountID, String role
            , String addedFromIP
            , String addedByEmail
            , String addedByName)
        {
            Data.DASec_UserAccountRole.Insert(membershipId, accountID, role, addedByEmail, addedByName, addedFromIP);
        }
    }
}
