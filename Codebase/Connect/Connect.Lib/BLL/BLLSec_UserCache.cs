﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Security;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSec_UserCache
    {
        internal static Sec_UserCache InitFromData(DataRow r)
        {
            if (r == null) return null;
            Sec_UserCache obj = new Sec_UserCache();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.DataKey = ConvertUtility.ConvertNullToEmptyString(r["DataKey"]);
            obj.DataValue = ConvertUtility.ConvertNullToEmptyString(r["DataValue"]);
            obj.LastUpdatedUTC = ConvertUtility.ConvertToDateTime(r["LastUpdatedUTC"], DateTime.MinValue);
            obj.MembershipId = ConvertUtility.ConvertToGuid(r["MembershipId"], Guid.Empty);
            obj.UserCacheID = ConvertUtility.ConvertToInt32(r["UserCacheID"], 0);
            return obj;
        }

        public static Sec_UserCache SelectByUserCacheID(Int32 userCacheID)
        {
            return InitFromData ( Data.DASec_UserCache.SelectByUserCacheID(userCacheID) );
        }

        public static Sec_UserCache SelectByUserCacheID(Guid membershipId, String dataKey)
        {
            return InitFromData(Data.DASec_UserCache.SelectByMembershipIdDataKey(membershipId, dataKey));
        }

        public static List<Sec_UserCache> SelectByMembershipId(Guid membershipId)
        {
            DataTable tb = Data.DASec_UserCache.SelectByMembershipId(membershipId);
            if (tb == null) return null;
            List<Sec_UserCache> lst = new List<Sec_UserCache>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static Sec_UserCache Save(Sec_UserCache userCache)
        {
            if (userCache == null) throw new ArgumentNullException("userCache");
            if (userCache.MembershipId.IsNullOrEmptyGuid()) throw new ArgumentNullException("MembershipId");
            if (userCache.DataKey.IsNullOrEmptyString()) throw new ArgumentNullException("DataKey");
            DataRow r = Data.DASec_UserCache.InsertUpdate(userCache.MembershipId
                , userCache.DataKey
                , ConvertUtility.ConvertNullToEmptyString(userCache.DataValue));
            return InitFromData(r);
        }

        public static Sec_UserCache Save(Guid membershipId, String dataKey, String dataValue)
        {
            if (membershipId.IsNullOrEmptyGuid()) throw new ArgumentNullException("MembershipId");
            if (dataKey.IsNullOrEmptyString()) throw new ArgumentNullException("DataKey");

            DataRow r = Data.DASec_UserCache.InsertUpdate(membershipId
                , dataKey
                , ConvertUtility.ConvertNullToEmptyString(dataValue));
            return InitFromData(r);
        }
    }
}
