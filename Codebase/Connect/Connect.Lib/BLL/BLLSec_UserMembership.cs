﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using EM = Anritsu.AnrCommon.EmailSDK;
using Sec_UserMembership = Anritsu.Connect.Lib.Security.Sec_UserMembership;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSec_UserMembership
    {
        internal static Sec_UserMembership InitFromData(DataSet dsMemberInfo)
        {
            if (dsMemberInfo == null || dsMemberInfo.Tables.Count==0) return null;
            DataRow r = (dsMemberInfo.Tables[0].Rows.Count>0)? dsMemberInfo.Tables[0].Rows[0]:null;
            if (r == null) return null;
            Sec_UserMembership obj = new Sec_UserMembership();
            obj.MembershipId = ConvertUtility.ConvertToGuid(r["MembershipId"], Guid.Empty);
            obj.GWUserId = ConvertUtility.ConvertToInt32(r["GWUserId"].ToString(), 0);
            obj.Email = ConvertUtility.ConvertNullToEmptyString(r["EmailAddress"]);
            obj.UserID = ConvertUtility.ConvertNullToEmptyString(r["UserID"]);
            obj.LastName = ConvertUtility.ConvertNullToEmptyString(r["LastName"]);
            obj.LastNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["LastNameInRuby"]);
            obj.LastNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["LastNameInEnglish"]);
            obj.FirstName = ConvertUtility.ConvertNullToEmptyString(r["FirstName"]);
            obj.FirstNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["FirstNameInRuby"]);
            obj.FirstNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["FirstNameInEnglish"]);
            obj.MiddleName = ConvertUtility.ConvertNullToEmptyString(r["MiddleName"]);
            obj.MiddleNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["MiddleNameInRuby"]);

            obj.PhoneNumber = ConvertUtility.ConvertNullToEmptyString(r["PhoneNumber"]);
            obj.FaxNumber = ConvertUtility.ConvertNullToEmptyString(r["FaxNumber"]);
            obj.JobTitle = ConvertUtility.ConvertNullToEmptyString(r["JobTitle"]);
            obj.UserAddressCountryCode = ConvertUtility.ConvertNullToEmptyString(r["UserAddressCountryCode"]);
            obj.IsIndividualEmailAddress = ConvertUtility.ConvertToBoolean(r["IsIndividualEmailAddress"], true);
            obj.IsApproved = ConvertUtility.ConvertToBoolean(r["IsApproved"], false);
            obj.IsLockedOut = ConvertUtility.ConvertToBoolean(r["IsLockedOut"], true);

            obj.LastLoginDate = ConvertUtility.ConvertToDateTime(r["LastLoginUTC"], DateTime.MinValue);
            obj.LastLockOutUTC = ConvertUtility.ConvertToDateTime(r["LastLockOutUTC"], DateTime.MinValue);
            obj.UserStreetAddress1 = ConvertUtility.ConvertNullToEmptyString(r["UserStreetAddress1"]);
            obj.UserStreetAddress2 = ConvertUtility.ConvertNullToEmptyString(r["UserStreetAddress2"]);
            obj.UserCity = ConvertUtility.ConvertNullToEmptyString(r["UserCity"]);
            obj.UserState = ConvertUtility.ConvertNullToEmptyString(r["UserState"]);
            obj.UserZipCode = ConvertUtility.ConvertNullToEmptyString(r["UserZipCode"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.IsAdministrator = ConvertUtility.ConvertToBoolean(r["IsAdministrator"], false);
            if (dsMemberInfo.Tables[1] != null && dsMemberInfo.Tables[1].Rows.Count > 0)
            {
                //get Team List
                // var teamList = ConvertUtility.ConvertToString(r["TeamList"], string.Empty);
                // if (!string.IsNullOrEmpty(teamList))
                obj.TeamsList = dsMemberInfo.Tables[1];
            }
            return obj;
        }
        internal static Sec_UserMembership InitFromData(DataRow r)
        {
            if (r == null) return null;
            Sec_UserMembership obj = new Sec_UserMembership();
            obj.MembershipId = ConvertUtility.ConvertToGuid(r["MembershipId"], Guid.Empty);
            obj.GWUserId = ConvertUtility.ConvertToInt32(r["GWUserId"].ToString(), 0);
            obj.Email = ConvertUtility.ConvertNullToEmptyString(r["EmailAddress"]);
            obj.UserID = ConvertUtility.ConvertNullToEmptyString(r["UserID"]);
            obj.LastName = ConvertUtility.ConvertNullToEmptyString(r["LastName"]);
            obj.LastNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["LastNameInRuby"]);
            obj.LastNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["LastNameInEnglish"]);
            obj.FirstName = ConvertUtility.ConvertNullToEmptyString(r["FirstName"]);
            obj.FirstNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["FirstNameInRuby"]);
            obj.FirstNameInEnglish = ConvertUtility.ConvertNullToEmptyString(r["FirstNameInEnglish"]);
            obj.MiddleName = ConvertUtility.ConvertNullToEmptyString(r["MiddleName"]);
            obj.MiddleNameInRuby = ConvertUtility.ConvertNullToEmptyString(r["MiddleNameInRuby"]);

            obj.PhoneNumber = ConvertUtility.ConvertNullToEmptyString(r["PhoneNumber"]);
            obj.FaxNumber = ConvertUtility.ConvertNullToEmptyString(r["FaxNumber"]);
            obj.JobTitle = ConvertUtility.ConvertNullToEmptyString(r["JobTitle"]);
            obj.UserAddressCountryCode = ConvertUtility.ConvertNullToEmptyString(r["UserAddressCountryCode"]);
            obj.IsIndividualEmailAddress = ConvertUtility.ConvertToBoolean(r["IsIndividualEmailAddress"], true);
            obj.IsApproved = ConvertUtility.ConvertToBoolean(r["IsApproved"], false);
            obj.IsLockedOut = ConvertUtility.ConvertToBoolean(r["IsLockedOut"], true);

            obj.LastLoginDate = ConvertUtility.ConvertToDateTime(r["LastLoginUTC"], DateTime.MinValue);
            obj.LastLockOutUTC = ConvertUtility.ConvertToDateTime(r["LastLockOutUTC"], DateTime.MinValue);
            obj.UserStreetAddress1 = ConvertUtility.ConvertNullToEmptyString(r["UserStreetAddress1"]);
            obj.UserStreetAddress2 = ConvertUtility.ConvertNullToEmptyString(r["UserStreetAddress2"]);
            obj.UserCity = ConvertUtility.ConvertNullToEmptyString(r["UserCity"]);
            obj.UserState = ConvertUtility.ConvertNullToEmptyString(r["UserState"]);
            obj.UserZipCode = ConvertUtility.ConvertNullToEmptyString(r["UserZipCode"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.IsAdministrator = ConvertUtility.ConvertToBoolean(r["IsAdministrator"], false);
            
            return obj;
        }


        public static Sec_UserMembership SelectByMembershipId(Guid membershipId)
        {
            return InitFromData(Data.DASec_UserMembership.SelectByMembershipId(membershipId));
        }

        public static Sec_UserMembership SelectHistoryByMembershipId(Guid membershipId)
        {
            return InitFromData(Data.DASec_UserMembership.SelectHistoryByMembershipId(membershipId));
        }

        public static void Delete(Guid membershipId)
        {
            Data.DASec_UserMembership.Delete(membershipId);
        }

        public static void DeleteFromAccount(Guid membershipId, Guid accountID)
        {
            Data.DASec_UserMembership.DeleteFromAccount(membershipId, accountID);
        }

        public static DataTable SelectAllFiltered(Guid membershipId, String emailAddress, String lastName, String firstName, Boolean? isAdmin)
        {
            return Data.DASec_UserMembership.SelectAllFiltered(membershipId, emailAddress, lastName, firstName, isAdmin);
        }

        public static Sec_UserMembership SelectByEmailAddressOrUserID(String emailAddressOrUserID)
        {
            return InitFromData(Data.DASec_UserMembership.SelectByEmailAddressOrUserID(emailAddressOrUserID));
        }

        public static Sec_UserMembership SelectByGWUserId(Int32 gwUserID)
        {
            return InitFromData(Data.DASec_UserMembership.SelectByGWUserId(gwUserID));
        }

        public static Sec_UserMembership Insert(Sec_UserMembership user)
        {
            if (user == null) throw new ArgumentNullException("user");
            DataSet dsMember = Data.DASec_UserMembership.Insert(user.GWUserId
                , user.Email
                , user.UserID
                , user.LastName
                , user.FirstName
                , user.IsApproved
                , user.IsLockedOut
                , user.LastLoginDate
                , user.LastActivityDate
                , user.LastLockOutUTC
                , user.PhoneNumber
                , user.FaxNumber
                , user.JobTitle
                , user.UserAddressCountryCode
                , user.MiddleName
                , user.UserStreetAddress1
                , user.UserStreetAddress2
                , user.UserCity
                , user.UserState
                , user.UserZipCode);
            return InitFromData(dsMember);
        }

        public static void Update(Sec_UserMembership user)
        {
            if (user == null) throw new ArgumentNullException("user");
            Security.Sec_UserMembership usrInfo = BLL.BLLSec_UserMembership.SelectByMembershipId(user.MembershipId);
            Boolean isCreateHistory = false;
            //if ((!user.Email.Equals(usrInfo.Email) || !user.LastName.Equals(usrInfo.LastName) || !user.FirstName.Equals(usrInfo.FirstName) || !user.PhoneNumber.Equals(usrInfo.PhoneNumber) || !user.FaxNumber.Equals(usrInfo.FaxNumber) ||
            //    !user.JobTitle.Equals(usrInfo.JobTitle) || !user.UserAddressCountryCode.Equals(usrInfo.UserAddressCountryCode) || !user.MiddleName.Equals(usrInfo.MiddleName) || !user.UserStreetAddress1.Equals(usrInfo.UserStreetAddress1) || !user.UserStreetAddress2.Equals(usrInfo.UserStreetAddress2) ||
            //    !user.UserCity.Equals(usrInfo.UserCity) || !user.UserState.Equals(usrInfo.UserState) || !user.UserZipCode.Equals(usrInfo.UserZipCode)) && usrInfo.LastLoginDate != DateTime.MinValue)
            //{
            //    isCreateHistory = true;
            //}
            int jpProductsRegistered = 0;
            jpProductsRegistered = Data.DASec_UserMembership.UpdateToNotify(user.MembershipId
              , user.GWUserId
              , user.Email
              , user.UserID
              , user.LastName
              , user.FirstName
              , user.IsApproved
              , user.IsLockedOut
              , DateTime.UtcNow
              , user.LastActivityDate
              , user.LastLockOutUTC
              , user.PhoneNumber
              , user.FaxNumber
              , user.JobTitle
              , user.UserAddressCountryCode
              , user.MiddleName
              , user.UserStreetAddress1
              , user.UserStreetAddress2
              , user.UserCity
              , user.UserState
              , user.UserZipCode
              , isCreateHistory);

            #region User profile update notification
            if (isCreateHistory && jpProductsRegistered > 0)
            {
                String msg = String.Empty;
                Lib.BLL.BLLSec_UserMembership.UserProfileUpdateNotifyAdmin(user.MembershipId, user, out msg);
                if (!string.IsNullOrEmpty(msg))
                    throw new ArgumentException("Unable to send email - " + msg);
            }
            #endregion
        }


        public static void UpdateUserNameInEnglish(Guid membershipId,
           String lastNameinEnglish,
           String firstNameinEnglish)
        {
            if (membershipId.IsNullOrEmptyGuid()) return;
            Data.DASec_UserMembership.UpdateUserNameInEnglish(membershipId, lastNameinEnglish, firstNameinEnglish);
        }

        public static void UpdateUserEmailInfo(Guid membershipId, Boolean isindividualEmailAddress)
        {
            if (membershipId.IsNullOrEmptyGuid()) return;
            Data.DASec_UserMembership.UpdateUserEmailInfo(membershipId, isindividualEmailAddress);
        }

        public static void UpdateLastActivity(Guid membershipId)
        {
            if (membershipId.IsNullOrEmptyGuid()) return;
            Data.DASec_UserMembership.UpdateLastActivity(membershipId);
        }

        public static void UpdateLockOut(Guid membershipId, Boolean isLockOut)
        {
            if (membershipId.IsNullOrEmptyGuid()) return;
            Data.DASec_UserMembership.UpdateUnlock(membershipId
                , isLockOut);
        }

        public static Sec_UserMembership AddNewUserPendingLogin(Guid accountID
           , String emailAddress
           , String firstName
           , String lastName
           , Boolean isManager
           , String addedFromIP
           , String addedByEmail
           , String addedByName)
        {
            return AddNewUserPendingLogin(accountID, emailAddress, firstName, lastName, isManager, addedFromIP, addedByEmail, addedByName
                , String.Empty, String.Empty, String.Empty, String.Empty);
        }

        public static Sec_UserMembership AddNewUserPendingLogin(Guid accountID
            , String emailAddress
            , String firstName
            , String lastName
            , Boolean isManager
            , String addedFromIP
            , String addedByEmail
            , String addedByName
            , String jobTitle
            , String phoneNumber
            , String faxNumber
            , String userAddressCountryCode
            )
        {
            if (emailAddress.IsNullOrEmptyString() || !emailAddress.Contains("@")) throw new ArgumentNullException("emailAddress");
            Sec_UserMembership usr = SelectByEmailAddressOrUserID(emailAddress.Trim());
            if (usr == null)
            {
                Sec_UserMembership newUser = new Sec_UserMembership();
                newUser.GWUserId = 0;
                newUser.Email = emailAddress;
                newUser.UserID = String.Empty;
                newUser.LastName = lastName;
                newUser.FirstName = firstName;
                newUser.LastLoginDate = DateTime.MinValue;
                newUser.LastActivityDate = DateTime.MinValue;
                newUser.LastLockOutUTC = DateTime.MinValue;
                newUser.IsApproved = true;
                newUser.IsLockedOut = false;
                newUser.IsAdministrator = false;
                newUser.CreatedOnUTC = DateTime.UtcNow;
                newUser.JobTitle = jobTitle;
                newUser.PhoneNumber = phoneNumber;
                newUser.FaxNumber = faxNumber;
                newUser.UserAddressCountryCode = userAddressCountryCode;
                newUser.MiddleName = String.Empty;
                newUser.UserStreetAddress1 = String.Empty;
                newUser.UserStreetAddress2 = String.Empty;
                newUser.UserCity = String.Empty;
                newUser.UserState = String.Empty;
                newUser.UserZipCode = String.Empty;

                usr = Insert(newUser);
            }
            if (usr == null) throw new ArgumentException("Unable to create new user.");

            Lib.BLL.BLLSec_Role.Insert(usr.MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleUser, addedFromIP, addedByEmail, addedByName);
            if (isManager) Lib.BLL.BLLSec_Role.Insert(usr.MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManager, addedFromIP, addedByEmail, addedByName);
            return usr;
        }



        public static DataTable SelectByAccountId(Guid accountID)
        {
            return Data.DASec_UserMembership.SelectByAccountId(accountID);
        }

        public static List<Sec_UserMembership> SelectByAccountIdAsList(Guid accountID)
        {
            DataTable tb = Data.DASec_UserMembership.SelectByAccountId(accountID);
            if (tb == null) return null;
            List<Sec_UserMembership> lst = new List<Sec_UserMembership>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static DataTable AdminUserSearch(List<SearchByOption> searchOptions)
        {
            Guid membershipId = Guid.Empty;
            String emailAddress = String.Empty;
            String lastNameOrFirstName = String.Empty;
            String orgName = String.Empty;
            String companyName = String.Empty;
            Boolean? isAdmin = null;

            if (searchOptions != null)
            {
                foreach (SearchByOption sbo in searchOptions)
                {
                    switch (sbo.SearchByField)
                    {
                        case "MembershipID":
                            membershipId = ConvertUtility.ConvertToGuid(sbo.SearchObject, Guid.Empty);
                            break;
                        case "EmailAddress":
                            emailAddress = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "LastNameOrFirstName":
                            lastNameOrFirstName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "OrgName":
                            orgName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "CompanyName":
                            companyName = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "IsAdministrator":
                            isAdmin = ConvertUtility.ConvertToBoolean(sbo.SearchObject, false);
                            break;
                    }
                }
            }
            return Data.DASec_UserMembership.AdminUserSearch(membershipId, emailAddress, lastNameOrFirstName, orgName, companyName, isAdmin);
        }

        public static DataTable LastActiveUsers(Int32 lastActiveMinutes)
        {
            return Data.DASec_UserMembership.SelectLastActiveUsers(lastActiveMinutes);
        }



        #region User update notification

        public static void UserProfileUpdateNotifyAdmin(Guid membershipId, Security.Sec_UserMembership user, out String errorMsg)
        {
            Lib.Content.GlobalWebCultureGroup gwCultureGroup =
                  Lib.Content.GlobalWebCultureGroupBLL.SelectByCountryCode(ConfigUtility.AppSettingGetValue("Connect.Web.UserProfileUpdateNotifyAdmin"));
            int cultureGroupId = 1;
            if (gwCultureGroup != null) cultureGroupId = gwCultureGroup.CultureGroupId;
            errorMsg = String.Empty;

            #region " send email(s) to admin for notification"
            // Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            List<EM.EmailWebRef.ReplaceKeyValueType> kvListForAdmin = GetKeyValuesForAdmin(membershipId, user);
            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest email =
               new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.UserProfileUpdate.NotifyAdmin");
            email.DistributionListKey = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdate.DistributionKey");
            email.CultureGroupId = cultureGroupId;
            email.SubjectReplacementKeyValues = kvListForAdmin.ToArray();
            email.BodyReplacementKeyValues = kvListForAdmin.ToArray();
            EM.EmailWebRef.BaseResponseType resp = EM.EmailServiceManager.SendDistributedTemplatedEmail(email);
            if (resp != null) errorMsg = resp.ResponseMessage;

            #endregion
        }


        private static List<EM.EmailWebRef.ReplaceKeyValueType> GetKeyValuesForAdmin(Guid membershipId,
           Security.Sec_UserMembership user)
        {

            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;

            #region " user information "
            if (user != null)
            {
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
            }
            #endregion

            #region "Account information"
            List<Lib.Account.Acc_AccountMaster> accounts = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(membershipId);
            StringBuilder Accounts = new StringBuilder();
            char[] characters = new char[] { ' ', ',' };
            foreach (Lib.Account.Acc_AccountMaster acc in accounts)
            {
                if (!String.IsNullOrEmpty(acc.CompanyName))
                {
                    Accounts.Append(acc.CompanyName + ", ");
                }
            }

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[Account-CompanyName]]";
            kv.ReplacementValue = Accounts.ToString().TrimEnd(characters);
            kvList.Add(kv);
            #endregion

            #region "Url information"
            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[MembershipID]]";
            kv.ReplacementValue = Convert.ToString(membershipId);
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[BaseUrl]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);

            #endregion

            return kvList;
        }
        #endregion

        public static Sec_UserMembership SelectByTeamOwnerId(Guid accountId)
        {
            return InitFromData(Data.DASec_UserMembership.SelectByTeamOwnerId(accountId));
        }

    }
}
