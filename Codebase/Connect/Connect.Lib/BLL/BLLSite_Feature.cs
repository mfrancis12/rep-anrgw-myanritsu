﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSite_Feature
    {
        internal static Site_Feature InitFromData(DataRow r)
        {
            if (r == null) return null;
            Site_Feature obj = new Site_Feature();
            obj.ControlPath = ConvertUtility.ConvertNullToEmptyString(r["ControlPath"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FeatureID = ConvertUtility.ConvertToInt32(r["FeatureID"], 0);
            obj.FeatureKey = ConvertUtility.ConvertToGuid(r["FeatureKey"], Guid.Empty);
            obj.FeatureName = ConvertUtility.ConvertNullToEmptyString(r["FeatureName"]);
            obj.EditContentUrl = ConvertUtility.ConvertNullToEmptyString(r["EditContentUrl"]);
            obj.StaticResourceClassKey = ConvertUtility.ConvertNullToEmptyString(r["StaticResourceClassKey"]);
            return obj;
        }

        public static Site_Feature SelectByFeatureID(Int32 featureID)
        {
            return InitFromData(Data.DASite_Feature.SelectByFeatureID(featureID));
        }

        public static Site_Feature SelectByFeatureKey(Guid featureKey)
        {
            return InitFromData(Data.DASite_Feature.SelectByFeatureKey(featureKey));
        }

        public static List<Site_Feature> SelectAll()
        {
            DataTable tb = Data.DASite_Feature.SelectAll();
            if (tb == null) return null;
            List<Site_Feature> lst = new List<Site_Feature>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }


    }
}
