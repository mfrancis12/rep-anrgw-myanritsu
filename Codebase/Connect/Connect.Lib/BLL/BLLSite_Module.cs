﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSite_Module
    {
        internal static Site_Module InitFromData(DataRow r)
        {
            if (r == null) return null;
            Site_Module obj = new Site_Module();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.FeatureID = ConvertUtility.ConvertToInt32(r["FeatureID"], 0);
            obj.HideFromLoggedInUser = ConvertUtility.ConvertToBoolean(r["HideFromLoggedInUser"], false);
            obj.IsPublic = ConvertUtility.ConvertToBoolean(r["IsPublic"], false);
            obj.ModuleID = ConvertUtility.ConvertToInt32(r["ModuleID"], 0);
            obj.ModuleName = ConvertUtility.ConvertNullToEmptyString(r["ModuleName"]);
            obj.NoBorder = ConvertUtility.ConvertToBoolean(r["NoBorder"], false);
            obj.ShowHeader = ConvertUtility.ConvertToBoolean(r["ShowHeader"], false);
            obj.FeatureInfo = BLLSite_Feature.SelectByFeatureID(obj.FeatureID);
            return obj;
        }

        public static Site_Module SelectByModuleID(Int32 moduleID)
        {
            return InitFromData(Data.DASite_Module.SelectByModuleID(moduleID));
        }

        public static List<Site_Module> SelectAll()
        {
            DataTable tb = Data.DASite_Module.SelectAll();
            if (tb == null) return null;
            List<Site_Module> lst = new List<Site_Module>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static List<Site_Module> SelectByFeatureID(Int32 featureID)
        {
            DataTable tb = Data.DASite_Module.SelectByFeatureID(featureID);
            if (tb == null) return null;
            List<Site_Module> lst = new List<Site_Module>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Int32 Insert(Int32 featureID, String moduleName, Boolean showHeader, Boolean noBorder, Boolean isPublic, Boolean hideFromLoggedInUser)
        {
            return Data.DASite_Module.Insert(featureID, moduleName, showHeader, noBorder, isPublic, hideFromLoggedInUser);
        }

        public static void Update(Int32 moduleID, Int32 featureID, String moduleName, Boolean showHeader, Boolean noBorder, Boolean isPublic, Boolean hideFromLoggedInUser)
        {
            Data.DASite_Module.Update(moduleID, featureID, moduleName, showHeader, noBorder, isPublic, hideFromLoggedInUser);
        }

        public static Boolean Delete(Int32 moduleID)
        {
            if (moduleID < 1) return false;
            return Data.DASite_Module.Delete(moduleID) > 0;
        }

        public static String GetResourceClassKey(Int32 moduleID)
        {
            return Data.DASite_Module.GetResourceClassKey(moduleID);
        }
    }
}
