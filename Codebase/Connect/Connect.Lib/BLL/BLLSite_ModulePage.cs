﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSite_ModulePage
    {
        internal static Site_ModulePage InitFromData(DataRow r)
        {
            if (r == null) return null;
            Site_ModulePage obj = new Site_ModulePage();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.LoadOrder = ConvertUtility.ConvertToInt32(r["LoadOrder"], 0);
            obj.ModuleID = ConvertUtility.ConvertToInt32(r["ModuleID"], 0);
            obj.PageRefID = ConvertUtility.ConvertToInt32(r["PageRefID"], 0);
            obj.PageUrl = ConvertUtility.ConvertNullToEmptyString(r["PageUrl"]);
            obj.PlaceHolder = ConvertUtility.ConvertNullToEmptyString(r["PlaceHolder"]);
            obj.ModuleInfo = BLLSite_Module.InitFromData(r);
            return obj;
        }

        public static List<Site_ModulePage> SelectByPageUrl(String pageUrl)
        {
            DataTable tb = Data.DASite_ModulePage.SelectByPageUrl(pageUrl);
            if (tb == null) return null;
            List<Site_ModulePage> lst = new List<Site_ModulePage>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static List<Site_ModulePage> SelectByModuleID(Int32 moduleID)
        {
            DataTable tb = Data.DASite_ModulePage.SelectByModuleID(moduleID);
            if (tb == null) return null;
            List<Site_ModulePage> lst = new List<Site_ModulePage>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Int32 Insert(Int32 moduleID, String pageUrl, String placeHolder)
        {
            return Data.DASite_ModulePage.Insert(moduleID, pageUrl, placeHolder);
        }

        public static void Update(Int32 pageRefID, Int32 loadOrder, String placeHolder)
        {
            Data.DASite_ModulePage.Update(pageRefID, loadOrder, placeHolder);
        }

        public static Boolean Delete(Int32 pageRefID)
        {
            return Data.DASite_ModulePage.Delete(pageRefID) > 0;
        }
    }
}
