﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSite_ModuleSetting
    {
        internal static Site_ModuleSetting InitFromData(DataRow r)
        {
            if (r == null) return null;
            Site_ModuleSetting obj = new Site_ModuleSetting();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.ModuleID = ConvertUtility.ConvertToInt32(r["ModuleID"], 0);
            obj.ModuleSettingID = ConvertUtility.ConvertToInt32(r["ModuleSettingID"], 0);
            obj.SettingKey = ConvertUtility.ConvertNullToEmptyString(r["SettingKey"]);
            obj.SettingValue = ConvertUtility.ConvertNullToEmptyString(r["SettingValue"]);
            return obj;
        }

        public static List<Site_ModuleSetting> SelectByModuleID(Int32 moduleID)
        {
            DataTable tb = Data.DASite_ModuleSetting.SelectByModuleID(moduleID);
            if (tb == null) return null;
            List<Site_ModuleSetting> lst = new List<Site_ModuleSetting>();
            foreach (DataRow r in tb.Rows) lst.Add(InitFromData(r));
            return lst;
        }

        public static Site_ModuleSetting SelectByModuleIDSettingKey(Int32 moduleID, String settingKey)
        {
            return InitFromData(Data.DASite_ModuleSetting.SelectByModuleIDSettingKey(moduleID, settingKey));
        }

        public static void Save(Int32 moduleID, String settingKey, String settingValue)
        {
            Data.DASite_ModuleSetting.Save(moduleID, settingKey, settingValue);
        }

        public static Boolean DeleteByModuleSettingID(Int32 moduleSettingID)
        {
            return Data.DASite_ModuleSetting.DeleteByModuleSettingID(moduleSettingID) > 0;
        }
    }
}
