﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.Connect.Lib.UI;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLLSite_Sitemap
    {
        internal static Site_Sitemap InitFromData(DataRow r)
        {
            if (r == null) return null;
            Site_Sitemap obj = new Site_Sitemap();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.PageUrl = ConvertUtility.ConvertNullToEmptyString(r["PageUrl"]);
            obj.ParentPageUrl = ConvertUtility.ConvertNullToEmptyString(r["ParentPageUrl"]);
            return obj;
        }

        public static List<Site_Sitemap> SelectForBreadCrumbByPageUrl(String pageURL)
        {
            List<Site_Sitemap> lst = new List<Site_Sitemap>();
            DataTable tb = Data.DASite_Sitemap.SelectForBreadCrumbByPageUrl(pageURL);
            if (tb == null) return lst;
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static Site_Sitemap SelectByPageUrl(String pageURL)
        {
            return InitFromData(Data.DASite_Sitemap.SelectByPageUrl(pageURL));
        }

        public static Boolean Insert(String pageURL, String parentPageURL)
        {
            return Data.DASite_Sitemap.Insert(pageURL, parentPageURL);
        }

        public static Boolean Update(String pageURL, String parentPageURL)
        {
            return Data.DASite_Sitemap.Update(pageURL, parentPageURL);
        }

        public static Int32 Delete(String pageURL)
        {
            return Data.DASite_Sitemap.Delete(pageURL);
        }
    }
}
