﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Data;

namespace Anritsu.Connect.Lib.BLL
{
    public static class BLL_State
    {
        public static DataTable GetAllStates(string countryValue)
        {
            return DA_State.GetAllStates(countryValue);
        }
    }
}
