﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Cfg
{
    [Serializable]
    public class ModelConfig
    {
        public Int32 ModelConfigID { get; set; }
        public String ModelNumber { get; set; }
        public String ModelConfigType { get; set; }
        public String ModelConfigStatus { get; set; }
        public String Reg_InitialStatus { get; set; }
        public Boolean UseModelTags { get; set; }
        public Boolean Verify_Company { get; set; }
        public Boolean Verify_Registration { get; set; }
        public Boolean UseUserACL { get; set; }
        public Boolean HasSupportExpiration { get; set; }
        public String AssignedOwner { get; set; }
        // don't include other things like created by, internal note to reduce memory use.  use other methods if u need those info to show in admin
    }
}
