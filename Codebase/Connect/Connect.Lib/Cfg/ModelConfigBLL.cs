﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.Cfg
{
    public static class ModelConfigBLL
    {
        internal static ModelConfig InitFromData(DataRow r)
        {
            if (r == null) return null;
            ModelConfig obj = new ModelConfig();
            obj.ModelConfigID = ConvertUtility.ConvertToInt32(r["ModelConfigID"], 0);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.ModelConfigType = ConvertUtility.ConvertNullToEmptyString(r["ModelConfigType"]);
            obj.ModelConfigStatus = ConvertUtility.ConvertNullToEmptyString(r["ModelConfigStatus"]);
            obj.Reg_InitialStatus = ConvertUtility.ConvertNullToEmptyString(r["Reg_InitialStatus"]);
            obj.UseModelTags = ConvertUtility.ConvertToBoolean(r["UseModelTags"], false);
            obj.Verify_Company = ConvertUtility.ConvertToBoolean(r["Verify_Company"], false);
            obj.Verify_Registration = ConvertUtility.ConvertToBoolean(r["Verify_Registration"], false);
            obj.UseUserACL = ConvertUtility.ConvertToBoolean(r["UseUserACL"], false);
            obj.HasSupportExpiration = ConvertUtility.ConvertToBoolean(r["HasSupportExpiration"], false);
            obj.AssignedOwner = ConvertUtility.ConvertNullToEmptyString(r["AssignedOwner"]);
            return obj;
        }

        public static ModelConfig SelectByModelNumber(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DataRow r = Data.DACfg_ModelConfig.GetByModelNumber(modelNumber);
            return InitFromData(r);
        }

        public static ModelConfig SelectByModelAndConfigType(String modelNumber, String modelConfigType)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            DataRow r = Data.DACfg_ModelConfig.GetByModelConfgType(modelNumber, modelConfigType);
            return InitFromData(r);
        }

        // public static void Update(ModelConfig mcfg, String modifiedBy)
        //{
        //    if (mcfg == null || mcfg.ModelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("mcfg");
        //    Data.DACfg_ModelConfig.Update(mcfg.ModelNumber
        //        , mcfg.ModelConfigType
        //        , mcfg.ModelConfigStatus
        //        , mcfg.Reg_InitialStatus
        //        , mcfg.UseModelTags
        //        , mcfg.Verify_Company
        //        , mcfg.Verify_Registration
        //        , mcfg.UseUserACL
        //        , mcfg.HasSupportExpiration
        //        , modifiedBy);
              
        //}

         public static void InsertUpdate(ModelConfig mcfg,String internalNotes, String submittedBy)
         {
             if (mcfg == null || mcfg.ModelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("mcfg");
             Data.DACfg_ModelConfig.InsertUpdate(mcfg.ModelNumber
                 , mcfg.ModelConfigType
                 , mcfg.ModelConfigStatus
                 , mcfg.Reg_InitialStatus
                 , mcfg.UseModelTags
                 , mcfg.Verify_Company
                 , mcfg.Verify_Registration
                 , mcfg.UseUserACL
                 , mcfg.HasSupportExpiration
                 , submittedBy
                 , internalNotes
                 ,mcfg.AssignedOwner);

         }

         public static DataTable SelectByModelAndConfigType(String modelNumber,bool exactMatch,String configType)
         {
             return Data.DACfg_ModelConfig.GetByModelConfgType(modelNumber, configType, exactMatch);
         }
       
    }
}
