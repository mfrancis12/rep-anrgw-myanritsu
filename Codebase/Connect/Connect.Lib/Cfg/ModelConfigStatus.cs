﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Cfg
{
    [Serializable]
    public class ModelConfigStatus
    {

        public const String STATUS_ACTIVE = "active";
        public const String STATUS_TOREVIEW = "to-be-reviewed";
        public const String STATUS_DISABLE = "disabled";

        public String StatusDisplayText { get; set; }
        public String ModelConfigStatusCode { get; set; }
    }
   
}
