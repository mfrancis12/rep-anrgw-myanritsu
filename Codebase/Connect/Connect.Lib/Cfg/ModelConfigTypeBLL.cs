﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Cfg
{
    public static class ModelConfigTypeBLL
    {
        internal static ModelConfigTypeInfo InitFromData(DataRow r)
        {
            if (r == null) return null;
            ModelConfigTypeInfo obj = new ModelConfigTypeInfo();
            obj.ModelConfigType = ConvertUtility.ConvertNullToEmptyString(r["ModelConfigType"]);
            obj.EmailDistKey_NotifyReg = ConvertUtility.ConvertNullToEmptyString(r["EmailDistKey_NotifyReg"]);
            obj.ManageRole = ConvertUtility.ConvertNullToEmptyString(r["ManageRole"]);
            switch (obj.ModelConfigType)
            {
                case "downloads-us":
                    obj.ModelConfigTypeDisplayText = "Downloads US-MMD";
                    break;
                case "downloads-dk":
                    obj.ModelConfigTypeDisplayText = "Downloads DK-M";
                    break;
                case "downloads-jp":
                    obj.ModelConfigTypeDisplayText = "Downloads JP-M";
                    break;
                case "downloads-jp-an":
                    obj.ModelConfigTypeDisplayText = "Downloads JP-AN";
                    break;
                case "downloads-uk":
                    obj.ModelConfigTypeDisplayText = "Downloads MB/MC";
                    break;
                case "sis":
                    obj.ModelConfigTypeDisplayText = "Distributor EMEA";
                    break;
            }
            return obj;
        }

        public static List<ModelConfigTypeInfo> SelectAll()
        {
            List<ModelConfigTypeInfo> list = new List<ModelConfigTypeInfo>();
            DataTable tb = Data.DACfg_ModelConfigType.SelectAll();
            if (tb == null) return null;
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitFromData(row));
            }
            return list;
        }
    }
}
