﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Cfg
{
    [Serializable]
    public class ModelConfigTypeInfo
    {
        public const String CONFIGTYPE_UNKNOWN = "unknown";
        public const String CONFIGTYPE_US_MMD = "downloads-us";
        public const String CONFIGTYPE_DENMARK = "downloads-dk";
        public const String CONFIGTYPE_JP = "downloads-jp";
        public const String CONFIGTYPE_JPAN = "downloads-jp-an";
        public const String CONFIGTYPE_UK_ESD = "downloads-uk";
        //public const String CONFIGTYPE_SIS = "sis";

        public String ModelConfigType { get; set; }
        public String ModelConfigTypeDisplayText { get; set; }
        public String EmailDistKey_NotifyReg { get; set; }
        public String ManageRole { get; set; }
    }
}
