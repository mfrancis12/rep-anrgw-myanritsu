﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Cfg
{
    public static class ModelConfig_ModelTagBLL
    {
        public static DataTable SelectByModelNumber(String modelNumber)
        {
            if (String.IsNullOrEmpty(modelNumber)) return null;
            return Data.DACfg_ModelTags.SelectByModelNumber(modelNumber);
        }

        public static Int32 Insert(String modelNumber, String tagModelNumber)
        {
            if (String.IsNullOrEmpty(modelNumber) || String.IsNullOrEmpty(tagModelNumber)) return 0;
            DataRow r = Data.DACfg_ModelTags.Insert(modelNumber.Trim(), tagModelNumber.Trim());
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["ModelTagID"], 0);
        }

        public static Boolean Delete(Int32 modelTagID)
        {
            if (modelTagID < 1) return false;
            return Data.DACfg_ModelTags.Delete(modelTagID) > 0;
        }

        public static void DeleteAll(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
            Data.DACfg_ModelTags.DeleteAll(modelNumber.Trim());
        }
    }
}
