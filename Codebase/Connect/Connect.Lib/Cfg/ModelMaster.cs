﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Cfg
{
    [Serializable]
    public class ModelMaster
    {
        public String ModelNumber { get; set; }
        public String CustomImageUrl { get; set; }
        public Boolean Reg_ValidateSN { get; set; }
        public String Reg_SNPattern { get; set; }
        public Guid Reg_FeedbackFormID { get; set; }
        public Guid Reg_AddOnFormID { get; set; }
        public Boolean Reg_AddOnFormPerSN { get; set; }
        public Boolean Reg_AllowSelfReg { get; set; }
        public Boolean ShowSupportAgreement { get; set; }
        public Boolean ShowWarrantyInfo { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
