﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Cfg
{
    [Serializable]
    public class ProductConfig : ModelMaster
    {
        public List<ModelConfig> ModelConfigs { get; set; }
        public List<String> ModelTags { get; set; }
    }
}
