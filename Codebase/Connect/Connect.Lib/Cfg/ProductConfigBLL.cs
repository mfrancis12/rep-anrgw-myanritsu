﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Cfg
{
    public static class ProductConfigBLL
    {
        internal static ProductConfig InitFromData(DataRow r, DataTable tbModelConfigs, DataTable tbModelTags)
        {
            if (r == null) return null;
            ProductConfig obj = new ProductConfig();
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.CustomImageUrl = ConvertUtility.ConvertNullToEmptyString(r["CustomImageUrl"]);
            obj.Reg_ValidateSN = ConvertUtility.ConvertToBoolean(r["Reg_ValidateSN"], false);
            obj.Reg_SNPattern = ConvertUtility.ConvertNullToEmptyString(r["Reg_SNPattern"]);
            obj.Reg_FeedbackFormID = ConvertUtility.ConvertToGuid(r["Reg_FeedbackFormID"], Guid.Empty);
            obj.Reg_AddOnFormID = ConvertUtility.ConvertToGuid(r["Reg_AddOnFormID"], Guid.Empty);
            obj.Reg_AddOnFormPerSN = ConvertUtility.ConvertToBoolean(r["Reg_AddOnFormPerSN"], false);
            obj.Reg_AllowSelfReg = ConvertUtility.ConvertToBoolean(r["Reg_AllowSelfReg"], false);
            obj.ShowSupportAgreement = ConvertUtility.ConvertToBoolean(r["ShowSupportAgreement"], false);
            obj.ShowWarrantyInfo = ConvertUtility.ConvertToBoolean(r["ShowWarrantyInfo"], false);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            
            if (tbModelConfigs != null)
            {
                obj.ModelConfigs = new List<ModelConfig>();
                foreach (DataRow config in tbModelConfigs.Rows)
                {
                    obj.ModelConfigs.Add(ModelConfigBLL.InitFromData(config));
                }
            }
            if (tbModelTags != null)
            {
                obj.ModelTags = new List<string>();
                foreach (DataRow tag in tbModelTags.Rows)
                {
                    obj.ModelTags.Add(tag["TagModelNumber"].ToString());
                }
            }
            return obj;
        }

        public static List<ProductConfig> FindAllActiveByModelKeyword(String modelKeyword, Boolean onlyAllowSelfRegModels)
        {
            DataTable tb = Data.DACfg_ModelMaster.SelectByModel(modelKeyword, false);
            if (tb == null) return null;

            List<ProductConfig> list = new List<ProductConfig>();
            foreach (DataRow row in tb.Rows)
            {
                Boolean reg_AllowSelfReg = ConvertUtility.ConvertToBoolean(row["Reg_AllowSelfReg"], false);
                if (onlyAllowSelfRegModels && !reg_AllowSelfReg) continue;

                DataTable tbModelTags = null;
                DataTable tbModelConfigs = null;
                Data.DACfg_ModelMaster.SelectActiveConfigurations(row["ModelNumber"].ToString(), out tbModelConfigs, out tbModelTags);

                if (tbModelConfigs.Rows.Count > 0)
                {
                    list.Add(InitFromData(row, tbModelConfigs, tbModelTags));
                }
            }
            return list;
        }

        public static List<ProductConfig> FindByModelKeyword(String modelKeyword, Boolean onlyAllowSelfRegModels)
        {
            DataTable tb = Data.DACfg_ModelMaster.SelectByModel(modelKeyword, false);
            if (tb == null) return null;

            List<ProductConfig> list = new List<ProductConfig>();
            foreach (DataRow row in tb.Rows)
            {
                Boolean reg_AllowSelfReg = ConvertUtility.ConvertToBoolean(row["Reg_AllowSelfReg"], false);
                if (onlyAllowSelfRegModels && !reg_AllowSelfReg) continue;

                DataTable tbModelTags = null;
                DataTable tbModelConfigs = null;
                Data.DACfg_ModelMaster.SelectConfigurations(row["ModelNumber"].ToString(), out tbModelConfigs, out tbModelTags);
                list.Add(InitFromData(row, tbModelConfigs, tbModelTags));
            }
            return list;
        }

        public static ProductConfig SelectByModelNumber(String modelNumber)
        {
            DataTable tb = Data.DACfg_ModelMaster.SelectByModel(modelNumber, true);
            if (tb == null || tb.Rows.Count != 1) return null;
            DataTable tbModelTags = null;
            DataTable tbModelConfigs = null;
            DataRow row = tb.Rows[0];
            Data.DACfg_ModelMaster.SelectConfigurations(row["ModelNumber"].ToString(), out tbModelConfigs, out tbModelTags);
            return InitFromData(row, tbModelConfigs, tbModelTags);
        }

        //public static void Update(ProductConfig pcfg)
        //{
        //    if (pcfg == null || pcfg.ModelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("pcfg");
        //    Data.DACfg_ModelMaster.Update(pcfg.ModelNumber
        //        , pcfg.CustomImageUrl
        //        , pcfg.Reg_ValidateSN
        //        , pcfg.Reg_SNPattern
        //        , pcfg.Reg_AddOnFormID
        //        , pcfg.Reg_AddOnFormPerSN
        //        , pcfg.Reg_FeedbackFormID
        //        , pcfg.Reg_AllowSelfReg
        //        , pcfg.ShowSupportAgreement);
        //}

        public static void InsertUpdate(ProductConfig pcfg)
        {
            if (pcfg == null || pcfg.ModelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("pcfg");
            Data.DACfg_ModelMaster.InsertUpdate(pcfg.ModelNumber
                , pcfg.CustomImageUrl
                , pcfg.Reg_ValidateSN
                , pcfg.Reg_SNPattern
                , pcfg.Reg_AddOnFormID
                , pcfg.Reg_AddOnFormPerSN
                , pcfg.Reg_FeedbackFormID
                , pcfg.Reg_AllowSelfReg
                , pcfg.ShowSupportAgreement
                , pcfg.ShowWarrantyInfo);
        }
        public static DataTable SelectModelNumbersForAutoComplete(String modelKeyword, Boolean onlyAllowSelfRegModels)
        {
            return Data.DACfg_ModelMaster.SelectByModel(modelKeyword, false);
        }
    }
}
