﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib
{
    public class ConnectDataCache
    {
        private static String _CacheDependencyFile;
        private static ObjectCache _MemCache = MemoryCache.Default;
        private CacheItemPolicy _CachePolicy = null;
        private CacheEntryRemovedCallback _CacheRemovedCallback = null;

        public static List<String> CacheDependencyFiles()
        {
            if (_CacheDependencyFile.IsNullOrEmptyString())
            {
                _CacheDependencyFile = HttpContext.Current.Server.MapPath( ConfigUtility.AppSettingGetValue("Connect.Lib.DataCache.CacheMonitor") );
            }
            return new List<String>(){_CacheDependencyFile };
        }

        public void AddToCacheWithSlidingExpiration(String cacheKey, Object cacheItem, TimeSpan exp) 
        {
            CacheItemPolicy cachePolicy = new CacheItemPolicy();
            cachePolicy.SlidingExpiration = exp;
            SetCache(cacheKey, cacheItem, cachePolicy);
        }

        public void AddToCacheWithAbsoluteExpiration(String cacheKey, Object cacheItem, DateTimeOffset exp)
        {
            CacheItemPolicy cachePolicy = new CacheItemPolicy();
            cachePolicy.AbsoluteExpiration = exp;
            SetCache(cacheKey, cacheItem, cachePolicy);
        }

        private void SetCache(String cacheKey, Object cacheItem, CacheItemPolicy cachePolicy)
        {
            if (cacheItem == null)
                RemoveCachedItem(cacheKey);

            if (cachePolicy == null)
            {
                cachePolicy = new CacheItemPolicy();
                cachePolicy.AbsoluteExpiration = DateTimeOffset.UtcNow.AddHours(1);
            }

            _CacheRemovedCallback = new CacheEntryRemovedCallback(this.MyCachedItemRemovedCallback);
            _CachePolicy = cachePolicy;
            _CachePolicy.Priority = CacheItemPriority.Default;
            _CachePolicy.RemovedCallback = _CacheRemovedCallback;
            List<String> monitorFiles = CacheDependencyFiles();
            _CachePolicy.ChangeMonitors.Add(new HostFileChangeMonitor(monitorFiles));
            _MemCache.Set(cacheKey, cacheItem, _CachePolicy);
        }

        public Object GetCachedItem(String cacheKey) 
        { 
            if(_MemCache.Contains(cacheKey))
                return _MemCache[cacheKey] as Object; 
            return null;
        }

        public void RemoveCachedItem(String cacheKey) 
        { 
            if (_MemCache.Contains(cacheKey)) 
            { 
                _MemCache.Remove(cacheKey); 
            } 
        }

        private void MyCachedItemRemovedCallback(CacheEntryRemovedArguments arguments) 
        { 
            // Log these values from arguments list 
            String strLog = String.Concat("Reason: ", arguments.RemovedReason.ToString(), 
                " | Key-Name: ", arguments.CacheItem.Key, " | Value-Object: ", arguments.CacheItem.Value.ToString()); 
        }

        public Int64 CacheItemCount()
        {
            return _MemCache.GetCount();
        }
    }
}
