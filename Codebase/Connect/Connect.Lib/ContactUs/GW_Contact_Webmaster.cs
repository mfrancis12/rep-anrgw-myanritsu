﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;

namespace Anritsu.Connect.Lib.ContactUs
{
    public static class GW_Contact_Webmaster
    {
        /// <summary>
        /// Send email to notify webmaster
        /// </summary>
        /// <param name="acc"></param>
        /// <param name="user"></param>
        /// <param name="userIP"></param>
        /// <param name="userGeoCountryCode"></param>
        /// <param name="url"></param>
        /// <param name="comments"></param>
        /// <param name="emailSent"></param>
        /// <param name="messageOut"></param>
        /// <returns></returns>
        public static Boolean SubmitRequest(Lib.Account.Acc_AccountMaster acc,
          Lib.Security.Sec_UserMembership user,
          String userIP,
          String userGeoCountryCode,
          String url,
          String comments,
          out Boolean emailSent,
          out String messageOut)
        {
            messageOut = String.Empty;
            emailSent = false;

            try
            {
                String msg = String.Empty;
                emailSent = Webmaster_NotifyRequest(user, acc, url, comments, out msg);
                if (!emailSent) throw new ArgumentException(msg);

                return true;
            }
            catch (Exception ex)
            {
                messageOut = ex.Message;
                return false;
            }
        }

        public static Boolean SubmitWebmasterContactDataToDatabase(int UserID, String url,
      String userIP, int culturegroupid,
        String comments)
        {
            bool result = false;
            result = Data.DAActivityLog.SubmitWebmasterContactDataToDatabase(UserID, url, userIP, culturegroupid, comments);

            return result;
        }
        public static Boolean Webmaster_NotifyRequest(Lib.Security.Sec_UserMembership user
            , Lib.Account.Acc_AccountMaster account
            , String url
            , String comments
            , out String errorMsg)
        {
            if (user == null) throw new ArgumentNullException("user");
            if (account == null) throw new ArgumentNullException("account");
            errorMsg = String.Empty;

            #region " send email(s) to web master"
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = ContactWebMaster_GetKeyValuesForAdmin(user, account, url, comments);
            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest email =
               new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ContactUs.NotifyWebmaster");
            email.DistributionListKey = ConfigUtility.AppSettingGetValue("Connect.Web.ContactUs.DistributionKey");
            email.CultureGroupId = 1;
            email.SubjectReplacementKeyValues = kvList.ToArray();
            email.BodyReplacementKeyValues = kvList.ToArray();
            EM.EmailWebRef.BaseResponseType resp = EM.EmailServiceManager.SendDistributedTemplatedEmail(email);
            if (resp != null) errorMsg = resp.ResponseMessage;
            return (resp != null && resp.StatusCode == 0);
            #endregion


        }

        private static List<EM.EmailWebRef.ReplaceKeyValueType> ContactWebMaster_GetKeyValuesForAdmin(
             Lib.Security.Sec_UserMembership user
             , Lib.Account.Acc_AccountMaster account
             , String url
             , String comments)
        {
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;

            if (user != null)
            {
                #region " user information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Phone]]";
                kv.ReplacementValue = user.PhoneNumber;
                kvList.Add(kv);


                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
                kvList.Add(kv);
                #endregion
            }

            if (account != null)
            {
                #region " account information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountID]]";
                kv.ReplacementValue = account.AccountID.ToString().ToUpperInvariant();
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountName]]";
                kv.ReplacementValue = account.AccountName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyName]]";
                kv.ReplacementValue = account.CompanyName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyAddress]]";
                kv.ReplacementValue = account.CompanyAddress;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyCountryCode]]";
                kv.ReplacementValue = account.CompanyCountryCode;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountStatusCode]]";
                kv.ReplacementValue = account.AccountStatusCode;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-IsVerifiedAndApproved]]";
                kv.ReplacementValue = account.IsVerifiedAndApproved.ToString().ToUpperInvariant();
                kvList.Add(kv);
                #endregion
            }

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[Comments]]";
            kv.ReplacementValue = comments;
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[RequestDateTime]]";
            kv.ReplacementValue = DateTime.UtcNow.ToString();
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[Url]]";
            kv.ReplacementValue = url;
            kvList.Add(kv);
                      
            return kvList;
        }

    }
}
