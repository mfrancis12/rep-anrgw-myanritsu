﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Content
{
    public class GlobalWebCultureGroup
    {
        public Int32 CultureGroupId { get; set; }
        public String CultureCode { get; set; }

        public GlobalWebCultureGroup()
        {
            CultureCode = "en-US";
            CultureGroupId = 1;
        }

        public GlobalWebCultureGroup(String llCC, Int32 cultureGroupId)
        {
            CultureCode = llCC;
            CultureGroupId = cultureGroupId;
        }
    }
}
