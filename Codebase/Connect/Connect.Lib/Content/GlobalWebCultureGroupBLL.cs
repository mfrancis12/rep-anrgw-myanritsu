﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Content
{
    public static class GlobalWebCultureGroupBLL
    {
        private static GlobalWebCultureGroup InitFromData(DataRow r)
        {
            if (r == null) return null;
            return new GlobalWebCultureGroup(r["CultureCode"].ToString(), ConvertUtility.ConvertToInt32(r["DefaultCultureGroupId"], 1));
        }

        public static GlobalWebCultureGroup SelectByCountryCode(String alpha2CountryCode)
        {
            return InitFromData( Data.DAIso_Country.GetISOCountryInfoByCountryCode(alpha2CountryCode) );
        }
    }
}
