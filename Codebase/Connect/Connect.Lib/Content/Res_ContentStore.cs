﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Content
{
    public class Res_ContentStore
    {
        public Int32 ContentID { get; set; }
        public String ClassKey { get; set; }
        public String ResourceKey { get; set; }
        public String Locale { get; set; }
        public String LocaleDisplayText { get; internal set; }
        public String ContentStr { get; set; }
        public String ContentBinary { get; set; }
        public Boolean AllowAutoTranslate { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
