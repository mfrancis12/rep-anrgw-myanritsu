﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Content;

namespace Anritsu.Connect.Lib.Content
{
    public static class Res_ContentStoreBLL
    {
        internal static Res_ContentStore InitFromData(DataRow r)
        {
            if (r == null) return null;
            Res_ContentStore obj = new Res_ContentStore();
            obj.ClassKey = ConvertUtility.ConvertNullToEmptyString(r["ClassKey"]);
            obj.ContentBinary = ConvertUtility.ConvertNullToEmptyString(r["ContentBinary"]);
            obj.ContentID = ConvertUtility.ConvertToInt32(r["ContentID"], 0);
            obj.ContentStr = ConvertUtility.ConvertNullToEmptyString(r["ContentStr"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.Locale = ConvertUtility.ConvertNullToEmptyString(r["Locale"]);
            obj.LocaleDisplayText = ConvertUtility.ConvertNullToEmptyString(r["LocaleDisplayText"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.ResourceKey = ConvertUtility.ConvertNullToEmptyString(r["ResourceKey"]);
            obj.AllowAutoTranslate = ConvertUtility.ConvertToBoolean(r["AllowAutoTranslate"], true);
            return obj;
        }

        public static Res_ContentStore Select(String classKey, String resourceKey, CultureInfo locale)
        {
            if (classKey.IsNullOrEmptyString()) throw new ArgumentNullException("classKey");
            if (resourceKey.IsNullOrEmptyString()) throw new ArgumentNullException("resourceKey");
            if (locale == null) throw new ArgumentNullException("locale");
            return InitFromData(Data.DARes_ContentStore.ContentStore_Select(classKey, resourceKey, locale.Name));
        }

        public static Res_ContentStore Select(String classKey, String resourceKey, CultureInfo locale, Boolean useExactMatch)
        {
            if (classKey.IsNullOrEmptyString()) throw new ArgumentNullException("classKey");
            if (resourceKey.IsNullOrEmptyString()) throw new ArgumentNullException("resourceKey");
            if (locale == null) throw new ArgumentNullException("locale");
            return InitFromData(Data.DARes_ContentStore.ContentStore_Select(classKey, resourceKey, locale.Name, useExactMatch));
        }

        public static Boolean GetContentStr(String classKey, String resourceKey, CultureInfo locale, Boolean decodeHtml, out String contentStr)
        {
            contentStr = string.Empty;
            Res_ContentStore obj = Select(classKey, resourceKey, locale);
            if (obj == null) return false;
            contentStr = ConvertUtility.ConvertNullToEmptyString(obj.ContentStr);
            if (decodeHtml) contentStr = System.Web.HttpUtility.HtmlDecode(contentStr);
            return true;
        }

        public static DataTable SelectByClassKeyResourceKey(String classKey, String resourceKey)
        {
            return Data.DARes_ContentStore.SelectByClassKeyResourceKey(classKey, resourceKey);
        }

        public static DataTable SelectResourceKey(String classKey)
        {
            return Data.DARes_ContentStore.SelectResourceKey(classKey);
        }

        public static Res_ContentStore SelectByContentID(Int32 contentID)
        {
            return InitFromData(Data.DARes_ContentStore.SelectByContentID(contentID));
        }

        public static Int32 Insert(String classKey, String resourceKey, String locale, String contentStr, Boolean encodeHtml)
        {
            return Data.DARes_ContentStore.Insert(classKey, resourceKey, locale, contentStr, encodeHtml);
        }

        public static void Update(Int32 contentID, String contentStr, Boolean allowAutoTranslate, Boolean encodeHtml)
        {
            Data.DARes_ContentStore.Update(contentID, contentStr, allowAutoTranslate, encodeHtml);
        }

        public static Int32 DeleteByContentID(Int32 contentID)
        {
            return Data.DARes_ContentStore.DeleteByContentID(contentID);
        }

        public static void DeleteByClassKeyResKey(String classKey, String resKey)
        {
            Data.DARes_ContentStore.DeleteByClassKeyResKey(classKey, resKey);
        }


        public static DataTable SelectDistinctLocale()
        {
            return Data.DARes_ContentStore.SelectDistinctLocale();
        }

        public static DataTable SelectFiltered(List<SearchByOption> searchOptions)
        {
            String classKey = String.Empty;
            String resKey = String.Empty; 
            String locale = String.Empty;
            String contentKeyword = String.Empty;

            if (searchOptions != null)
            {
                foreach (SearchByOption sbo in searchOptions)
                {
                    switch (sbo.SearchByField)
                    {
                        case "ClassKey":
                            classKey = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ResKey":
                            resKey = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "Locale":
                            locale = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ContentKeyword":
                            contentKeyword = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                    }
                }
            }
            return Data.DARes_ContentStore.SelectFiltered(classKey, resKey, locale, contentKeyword);
        }

        public static DataTable ContentSupportedLocale_SelectAll()
        {
            return Data.DARes_ContentStore.ContentSupportedLocale_SelectAll();
        }

        public static DataTable SelectForGoogleTranslateAPI(Int32 startContentID, Int32 endContentID)
        {
            return Data.DARes_ContentStore.SelectForGoogleTranslateAPI(startContentID, endContentID);
        }

        public static void InsertUpdate(String classKey, String resourceKey, String locale, String contentStr)
        {
            Data.DARes_ContentStore.InsertUpdate(classKey, resourceKey, locale, contentStr);
        }

        public static DataTable SelectClassKeyLocale()
        {
            return Data.DARes_ContentStore.SelectClassKeyLocale();
        }

        public static DataTable SelectByClassKeyLocale(String classKey, String locale)
        {
            if (classKey.IsNullOrEmptyString() || locale.IsNullOrEmptyString()) return null;
            return Data.DARes_ContentStore.SelectByClassKeyLocale(classKey, locale);
        }
    }
}
