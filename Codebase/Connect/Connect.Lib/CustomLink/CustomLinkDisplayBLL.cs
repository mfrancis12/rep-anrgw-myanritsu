﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLinkDisplayBLL
    {
        public static DataTable Select(String pageUrl,
            String locationType,
            String accountRegion,
            String modelNumber,
            Guid accountID)
        {
            return Data.DACustomLink.DALnk_LinkDisplay.Select(pageUrl, locationType, accountRegion, modelNumber, accountID);
        }
    }
}
