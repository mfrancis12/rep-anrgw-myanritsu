﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class CustomLink_AccountRef
    {
        public Int32 CustomLinkAccRefID { get; set; }
        public Int32 CustomLinkID { get; set; }
        public Guid AccountID { get; set; }
        public String CreatedBy { get; set; }
        public String CustomLinkInternalName { get; set; }
        public String AccName { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
