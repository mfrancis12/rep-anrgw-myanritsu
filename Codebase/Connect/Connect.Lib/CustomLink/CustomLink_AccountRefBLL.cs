﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLink_AccountRefBLL
    {
        internal const String DATACACHE_CUSLNK_AccRefPrefix = "CusLnkRfAcc_";

        private static String GetDataCacheKey(Int32 customLinkID)
        {
            return String.Format("{0}{1}", DATACACHE_CUSLNK_AccRefPrefix, customLinkID);
        }

        private static CustomLink_AccountRef InitData(DataRow row)
        {
            if (row == null) return null;

            CustomLink_AccountRef obj = new CustomLink_AccountRef();
            obj.CustomLinkAccRefID = ConvertUtility.ConvertToInt32(row["CustomLinkAccRefID"], 0);
            obj.CustomLinkID = ConvertUtility.ConvertToInt32(row["CustomLinkID"], 0);
            obj.AccountID = ConvertUtility.ConvertToGuid(row["AccountID"], Guid.Empty);
            obj.AccName = ConvertUtility.ConvertNullToEmptyString(row["AccName"]);
            obj.CustomLinkInternalName = ConvertUtility.ConvertNullToEmptyString(row["CustomLinkInternalName"]);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<CustomLink_AccountRef> SelectByCustomLinkID(Int32 customLinkID, Boolean refreshFromDB)
        {
            if (customLinkID < 1) return null;

            String cacheKey = GetDataCacheKey(customLinkID);

            ConnectDataCache cache = new ConnectDataCache();
            List<CustomLink_AccountRef> list = cache.GetCachedItem(cacheKey) as List<CustomLink_AccountRef>;
            if (list == null || refreshFromDB)
            {
                list = new List<CustomLink_AccountRef>();
                DataTable tb = Data.DACustomLink.DACustomLink_AccountRef.SelectByCustomLinkID(customLinkID);
                if (tb == null) return null;
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 2hr exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddHours(2));
            }

            return list;
        }

        public static Boolean IsValidCustomLink(Int32 customLinkID, Guid accountID, Boolean refreshFromDB)
        {
            List<CustomLink_AccountRef> list = SelectByCustomLinkID(customLinkID, refreshFromDB);
            if (list == null || list.Count < 1) return false;
            var query = from ar in list
                        where ar.AccountID.IsNullOrEmptyGuid() || ar.AccountID.Equals(accountID)
                        select ar;
            if (query == null) return false;
            return query.Count() > 0;
        }

        public static void Insert(Int32 customLinkID, Guid accountID, String createdBy)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            Data.DACustomLink.DACustomLink_AccountRef.Insert(customLinkID, accountID, createdBy);
            RemoveFromCache(customLinkID);
        }

        public static void DeleteByCustomLinkAccRefID(Int32 customLinkAccRefID)
        {
            if (customLinkAccRefID < 1) throw new ArgumentNullException("customLinkAccRefID");
            Data.DACustomLink.DACustomLink_AccountRef.DeleteByCustomLinkAccRefID(customLinkAccRefID);
            //bug RemoveFromCache(customLinkID);
        }

        public static bool IsShowForAll(Int32 customLinkID, Boolean refreshFromDB)
        {
            List<CustomLink_AccountRef> list = SelectByCustomLinkID(customLinkID, refreshFromDB);
            if (list == null) return false;
            var query = from clar in list
                        where clar.AccountID == Guid.Empty
                        select clar;
            if (query == null) return false;
            return query.Count() > 0;
            
        }

         public static void SetShowForAll(Boolean showForAll, Int32 customLinkID, String createdBy)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            Data.DACustomLink.DACustomLink_AccountRef.SetShowForAll(showForAll, customLinkID, createdBy);
            RemoveFromCache(customLinkID);
        }

         private static void RemoveFromCache(Int32 customLinkID)
         {
             try
             {
                 String cacheKey = GetDataCacheKey(customLinkID);
                 ConnectDataCache cache = new ConnectDataCache();
                 cache.RemoveCachedItem(cacheKey);
             }
             catch { }
         }
    }
}
