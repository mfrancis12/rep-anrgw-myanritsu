﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class CustomLink_DisplayContainer
    {
        public String DisplayContainerID { get; set; }
        public String ContainerName { get; set; }
        public Int32 MaxCount { get; set; }
        public Boolean ImageIconRequired { get; set; }
        public Boolean WrapperCssRequired { get; set; }
        public Boolean EnableRefAcc { get; set; }
        public Boolean EnableRefSisProd { get; set; }
        public Boolean EnableRefModel { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
