﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLink_DisplayContainerBLL
    {
        internal const String DATACACHE_CUSLNK_DISTCONTAINERALL = "CusLnk_DistContainerALL";

        private static CustomLink_DisplayContainer InitData(DataRow row)
        {
            if (row == null) return null;

            CustomLink_DisplayContainer obj = new CustomLink_DisplayContainer();
            obj.DisplayContainerID = ConvertUtility.ConvertNullToEmptyString(row["DisplayContainerID"]);
            obj.ContainerName = ConvertUtility.ConvertNullToEmptyString(row["ContainerName"]);
            obj.MaxCount = ConvertUtility.ConvertToInt32(row["MaxCount"], 0);
            obj.ImageIconRequired = ConvertUtility.ConvertToBoolean(row["ImageIconRequired"], false);
            obj.WrapperCssRequired = ConvertUtility.ConvertToBoolean(row["WrapperCssRequired"], false);
            obj.EnableRefAcc = ConvertUtility.ConvertToBoolean(row["EnableRefAcc"], false);
            obj.EnableRefSisProd = ConvertUtility.ConvertToBoolean(row["EnableRefSisProd"], false);
            obj.EnableRefModel = ConvertUtility.ConvertToBoolean(row["EnableRefModel"], false);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<CustomLink_DisplayContainer> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_CUSLNK_DISTCONTAINERALL;

            ConnectDataCache cache = new ConnectDataCache();
            List<CustomLink_DisplayContainer> list = cache.GetCachedItem(cacheKey) as List<CustomLink_DisplayContainer>;
            if (list == null || refreshFromDB)
            {
                DataTable tb = Data.DACustomLink.DACustomLink_DisplayContainer.SelectAll();
                if (tb == null) return null;
                list = new List<CustomLink_DisplayContainer>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static CustomLink_DisplayContainer SelectByID(String displayContainerID)
        {
            if (displayContainerID.IsNullOrEmptyString()) return null;
            List<CustomLink_DisplayContainer> list = SelectAll(false);
            if(list == null) return null;
            var query = from dc in list
                        where dc.DisplayContainerID.Equals(displayContainerID, StringComparison.InvariantCultureIgnoreCase)
                        select dc;
            if (query == null) return null;
            return query.First();
        }
    }
}
