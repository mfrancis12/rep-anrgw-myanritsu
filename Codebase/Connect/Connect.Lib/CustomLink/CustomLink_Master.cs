﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class CustomLink_Master
    {
        public Int32 CustomLinkID { get; set; }
        public String LnkOwnerRole { get; set; }
        public String LnkInternalName { get; set; }
        public String LnkResClass { get; set; }
        public String DisplayContainerID { get; set; }
        public CustomLink_DisplayContainer DisplayContainer { get; set; }
        public Int32 LnkOrder { get; set; }
        public String TargetUrl { get; set; }
        public String LinkTarget { get; set; }
        public String IconImageUrl { get; set; }
        public String WrapperCss { get; set; }
        public DateTime StartOnUTC { get; set; }
        public DateTime ExpireOnUTC { get; set; }
        public String ModifiedBy { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
