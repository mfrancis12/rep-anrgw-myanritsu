﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLink_MasterBLL
    {
        internal const String DATACACHE_CUSLNK_ALL = "CusLnk_MasterALL";

        private static CustomLink_Master InitData(DataRow row)
        {
            if (row == null) return null;
            CustomLink_Master obj = new CustomLink_Master();
            obj.CustomLinkID = ConvertUtility.ConvertToInt32(row["CustomLinkID"], 0);
            obj.LnkOwnerRole = ConvertUtility.ConvertNullToEmptyString(row["LnkOwnerRole"]);
            obj.LnkInternalName = ConvertUtility.ConvertNullToEmptyString(row["LnkInternalName"]);
            obj.LnkResClass = ConvertUtility.ConvertNullToEmptyString(row["LnkResClass"]);
            obj.DisplayContainerID = ConvertUtility.ConvertNullToEmptyString(row["DisplayContainerID"]);
            obj.DisplayContainer = CustomLink_DisplayContainerBLL.SelectByID(obj.DisplayContainerID);
            obj.LnkOrder = ConvertUtility.ConvertToInt32(row["LnkOrder"], 1);
            obj.TargetUrl = ConvertUtility.ConvertNullToEmptyString(row["TargetUrl"]);
            obj.LinkTarget = ConvertUtility.ConvertNullToEmptyString(row["LinkTarget"]);
            obj.IconImageUrl = ConvertUtility.ConvertNullToEmptyString(row["IconImageUrl"]);
            obj.WrapperCss = ConvertUtility.ConvertNullToEmptyString(row["WrapperCss"]);
            obj.StartOnUTC = ConvertUtility.ConvertToDateTime(row["StartOnUTC"], DateTime.MinValue);
            obj.ExpireOnUTC = ConvertUtility.ConvertToDateTime(row["ExpireOnUTC"], DateTime.MaxValue);
            obj.ModifiedBy = ConvertUtility.ConvertNullToEmptyString(row["ModifiedBy"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(row["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);

            return obj;
        }

        public static List<CustomLink_Master> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_CUSLNK_ALL;

            ConnectDataCache cache = new ConnectDataCache();
            List<CustomLink_Master> list = cache.GetCachedItem(cacheKey) as List<CustomLink_Master>;
            if (list == null || refreshFromDB)
            {
                DataTable tb = Data.DACustomLink.DACustomLink_Master.SelectAll();
                if (tb == null) return null;
                list = new List<CustomLink_Master>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static List<CustomLink_Master> SelectByLinkAdminRoles(Boolean refreshFromDB, List<String> linkAdminRoles)
        {
            List<CustomLink_Master> list = SelectAll(refreshFromDB);
            if (list == null) return null;
            var query = from clm in list
                        where linkAdminRoles.Contains(clm.LnkOwnerRole)
                        select clm;
            if (query == null) return null;
            return query.ToList();

        }

        public static List<CustomLink_Master> SelectByDisplayContainerID(Boolean refreshFromDB, String displayContainerID
            , Boolean includeExpired, Boolean includePreLive, Account.Acc_AccountMaster requestedAcc, Int32 sisPID, String downloadModel)
        {
            if (requestedAcc == null) return null;

            CustomLink_DisplayContainer dc = CustomLink_DisplayContainerBLL.SelectByID(displayContainerID);
            if (dc == null) return null;
            Int32 maxCount = dc.MaxCount;
            if (maxCount < 1) maxCount = 3;
            DateTime utcNow = DateTime.UtcNow;

            List<CustomLink_Master> list = SelectAll(refreshFromDB);
            if (list == null) return null;

            //var query = (from clm in list
            //            where clm.DisplayContainerID.Equals(displayContainerID, StringComparison.InvariantCultureIgnoreCase)
            //            && (includePreLive || clm.StartOnUTC < utcNow )
            //            && (includeExpired || clm.ExpireOnUTC > utcNow )
            //            orderby clm.StartOnUTC descending
            //            select clm).Take(maxCount);

            var query = (from clm in list
                         where clm.DisplayContainerID.Equals(displayContainerID, StringComparison.InvariantCultureIgnoreCase)
                         && (includePreLive || clm.StartOnUTC < utcNow)
                         && (includeExpired || clm.ExpireOnUTC > utcNow)
                         orderby clm.StartOnUTC descending
                         select clm);
            if (query == null) return null;

            List<CustomLink_Master> eligibleList = query.ToList();
            List<CustomLink_Master> results = new List<CustomLink_Master>();
            foreach (CustomLink_Master lm in eligibleList)
            {
                //ignore if the account country is not in the link owner region
                if (!lm.LnkOwnerRole.EndsWith(requestedAcc.GWCultureGroupByCountryCode, StringComparison.InvariantCultureIgnoreCase))
                    continue;

                Boolean allChecked = true;

                if (dc.EnableRefAcc)
                {
                    if (!CustomLink_AccountRefBLL.IsValidCustomLink(lm.CustomLinkID, requestedAcc.AccountID, refreshFromDB))
                    {
                        allChecked = false;
                    }
                }

                if (dc.EnableRefSisProd)
                {
                    if (!CustomLink_RefSisProdBLL.IsValidCustomLink(lm.CustomLinkID, sisPID, refreshFromDB))
                    {
                        allChecked = false;
                    }
                }

                if (dc.EnableRefModel && !downloadModel.IsNullOrEmptyString())
                {

                }
                if (allChecked)
                {
                    results.Add(lm);
                }
            }

            var finalQuery = (from lm1 in results
                              orderby lm1.StartOnUTC descending
                              select lm1).Take(maxCount);
            if (finalQuery == null) return null;
            return finalQuery.ToList();
        }

        public static CustomLink_Master SelectByCustomLinkID(Boolean refreshFromDB, Int32 customLinkID)
        {
            if (customLinkID < 1) return null;

            List<CustomLink_Master> list = SelectAll(refreshFromDB);
            if (list == null) return null;
            var query = from clm in list
                         where clm.CustomLinkID == customLinkID
                         select clm;
            if (query == null || query.Count() < 1) return null;
            return query.First();

        }

        public static Int32 Insert(String lnkOwnerRole, String lnkInternalName, String displayContainerID,
            Int32 lnkOrder, String targetUrl, String linkTarget, String iconImageUrl, String wrapperCss, DateTime startOnUTC, 
            DateTime expireOnUTC, String createdBy)
        {
            if (lnkOwnerRole.IsNullOrEmptyString()) throw new ArgumentNullException("lnkOwnerRole");
            if (lnkInternalName.IsNullOrEmptyString()) throw new ArgumentNullException("lnkInternalName");
            if (displayContainerID.IsNullOrEmptyString()) throw new ArgumentNullException("displayContainerID");
            if (targetUrl.IsNullOrEmptyString()) throw new ArgumentNullException("targetUrl");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            if (expireOnUTC == DateTime.MinValue) expireOnUTC = DateTime.UtcNow.AddYears(100);
            if (linkTarget.IsNullOrEmptyString()) linkTarget = "_self";

            DataRow row = Data.DACustomLink.DACustomLink_Master.Insert(lnkOwnerRole, lnkInternalName, displayContainerID, lnkOrder, targetUrl,
                linkTarget, iconImageUrl, wrapperCss, startOnUTC, expireOnUTC, createdBy);
            Int32 customLinkID = ConvertUtility.ConvertToInt32(row["CustomLinkID"], 0);
            CustomLink_MasterBLL.SelectAll(true);

            String lnkResClass = ConvertUtility.ConvertNullToEmptyString(row["LnkResClass"]);
            Content.Res_ContentStoreBLL.Insert(lnkResClass, "Link.Text", "en", lnkInternalName, true);

            return customLinkID;
        }

        public static void Update(Int32 customLinkID, String lnkInternalName, String displayContainerID,
            Int32 lnkOrder, String targetUrl, String linkTarget, String iconImageUrl, String wrapperCss, DateTime startOnUTC, DateTime expireOnUTC, String modifiedBy)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            if (lnkInternalName.IsNullOrEmptyString()) throw new ArgumentNullException("lnkInternalName");
            if (displayContainerID.IsNullOrEmptyString()) throw new ArgumentNullException("displayContainerID");
            if (targetUrl.IsNullOrEmptyString()) throw new ArgumentNullException("targetUrl");
            if (modifiedBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            if (expireOnUTC == DateTime.MinValue) expireOnUTC = DateTime.UtcNow.AddYears(100);
            if (linkTarget.IsNullOrEmptyString()) linkTarget = "_self";

            Data.DACustomLink.DACustomLink_Master.Update(customLinkID, lnkInternalName, displayContainerID, lnkOrder, targetUrl,
                linkTarget, iconImageUrl, wrapperCss, startOnUTC, expireOnUTC, modifiedBy);
            CustomLink_MasterBLL.SelectAll(true);
        }

        public static void Delete(Int32 customLinkID)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            Data.DACustomLink.DACustomLink_Master.Delete(customLinkID);
            CustomLink_MasterBLL.SelectAll(true);
        }
    }
}
