﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class CustomLink_OwnerRole
    {
        public String Role { get; set; }
        public String RoleName { get; set; }
    }
}
