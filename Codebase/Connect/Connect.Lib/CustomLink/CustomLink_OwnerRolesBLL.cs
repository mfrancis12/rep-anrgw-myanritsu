﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLink_OwnerRolesBLL
    {
        internal const String DATACACHE_CUSLNK_OWNERROLES = "CusLnk_OwnerRoles";

        private static CustomLink_OwnerRole InitData(DataRow row)
        {
            if (row == null) return null;

            CustomLink_OwnerRole obj = new CustomLink_OwnerRole();
            obj.Role = ConvertUtility.ConvertNullToEmptyString(row["Role"]);
            obj.RoleName = ConvertUtility.ConvertNullToEmptyString(row["RoleName"]);
            return obj;
        }

        public static List<CustomLink_OwnerRole> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_CUSLNK_OWNERROLES;

            ConnectDataCache cache = new ConnectDataCache();
            List<CustomLink_OwnerRole> list = cache.GetCachedItem(cacheKey) as List<CustomLink_OwnerRole>;
            if (list == null || refreshFromDB)
            {
                DataTable tb = Data.DACustomLink.DACustomLink_OwnerRoles.SelectAll();
                if (tb == null) return null;
                list = new List<CustomLink_OwnerRole>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }
    }
}
