﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class CustomLink_RefSisProd
    {
        public Int32 RefSisProdID { get; set; }
        public Int32 CustomLinkID { get; set; }
        public Int32 SisPID { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
