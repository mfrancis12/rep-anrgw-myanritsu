﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class CustomLink_RefSisProdBLL
    {
        internal const String DATACACHE_CUSLNK_AccRefPrefix = "CusLnkRfSisPrd_";

        private static String GetDataCacheKey(Int32 customLinkID)
        {
            return String.Format("{0}{1}", DATACACHE_CUSLNK_AccRefPrefix, customLinkID);
        }

        private static CustomLink_RefSisProd InitData(DataRow row)
        {
            if (row == null) return null;

            CustomLink_RefSisProd obj = new CustomLink_RefSisProd();
            obj.RefSisProdID = ConvertUtility.ConvertToInt32(row["RefSisProdID"], 0);
            obj.CustomLinkID = ConvertUtility.ConvertToInt32(row["CustomLinkID"], 0);
            obj.SisPID = ConvertUtility.ConvertToInt32(row["SisPID"], 0);
            
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<CustomLink_RefSisProd> SelectByCustomLinkID(Int32 customLinkID, Boolean refreshFromDB)
        {
            if (customLinkID < 1) return null;

            String cacheKey = GetDataCacheKey(customLinkID);

            ConnectDataCache cache = new ConnectDataCache();
            List<CustomLink_RefSisProd> list = cache.GetCachedItem(cacheKey) as List<CustomLink_RefSisProd>;
            if (list == null || refreshFromDB)
            {
                list = new List<CustomLink_RefSisProd>();
                DataTable tb = Data.DACustomLink.DACustomLink_RefSisProd.SelectByCustomLinkID(customLinkID);
                if (tb == null) return null;
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 2hr exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddHours(2));
            }

            return list;
        }

        public static Boolean IsValidCustomLink(Int32 customLinkID, Int32 sisPID, Boolean refreshFromDB)
        {
            List<CustomLink_RefSisProd> list = SelectByCustomLinkID(customLinkID, refreshFromDB);
            if (list == null || list.Count < 1) return false;
            var query = from sisP in list
                        where sisP.SisPID == 0 || sisP.SisPID == sisPID
                        select sisP;
            if (query == null) return false;
            return query.Count() > 0;
        }

        public static void Insert(Int32 customLinkID, Int32 sisPID, String createdBy)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            Data.DACustomLink.DACustomLink_RefSisProd.Insert(customLinkID, sisPID, createdBy);
            RemoveFromCache(customLinkID);
        }

        public static void DeleteByRefSisProdID(Int32 refSisProdID)
        {
            if (refSisProdID < 1) throw new ArgumentNullException("refSisProdID");
            Data.DACustomLink.DACustomLink_RefSisProd.DeleteByRefSisProdID(refSisProdID);
        }

        public static bool IsShowForAll(Int32 customLinkID, Boolean refreshFromDB)
        {
            List<CustomLink_RefSisProd> list = SelectByCustomLinkID(customLinkID, refreshFromDB);
            if (list == null) return false;
            var query = from clar in list
                        where clar.SisPID < 1
                        select clar;
            if (query == null) return false;
            return query.Count() > 0;

        }

        public static void SetShowForAll(Boolean showForAll, Int32 customLinkID, String createdBy)
        {
            if (customLinkID < 1) throw new ArgumentNullException("customLinkID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            Data.DACustomLink.DACustomLink_RefSisProd.SetShowForAll(showForAll, customLinkID, createdBy);
            RemoveFromCache(customLinkID);
        }

        private static void RemoveFromCache(Int32 customLinkID)
        {
            try
            {
                String cacheKey = GetDataCacheKey(customLinkID);
                ConnectDataCache cache = new ConnectDataCache();
                cache.RemoveCachedItem(cacheKey);
            }
            catch { }
        }
    }
}
