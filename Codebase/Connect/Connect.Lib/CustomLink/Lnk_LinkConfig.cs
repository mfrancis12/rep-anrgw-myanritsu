﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class Lnk_LinkConfig
    {
        public Int32 LnkCfgID { get; set; }
        public Int32 LnkID { get; set; }
        public String GWRegion { get; set; }
        public String TargetUrl { get; set; }
        public String ImageUrl { get; set; }
        public String LnkWrapperCssClass { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
