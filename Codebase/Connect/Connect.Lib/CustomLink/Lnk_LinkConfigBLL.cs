﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class Lnk_LinkConfigBLL
    {
        internal static Lnk_LinkConfig InitData(DataRow row)
        {
            if (row == null) return null;

            Lnk_LinkConfig obj = new Lnk_LinkConfig();
            obj.LnkCfgID = ConvertUtility.ConvertToInt32(row["LnkCfgID"], 0);
            obj.LnkID = ConvertUtility.ConvertToInt32(row["LnkID"], 0);
            obj.GWRegion = ConvertUtility.ConvertNullToEmptyString(row["GWRegion"]);
            obj.TargetUrl = ConvertUtility.ConvertNullToEmptyString(row["TargetUrl"]);
            obj.ImageUrl = ConvertUtility.ConvertNullToEmptyString(row["ImageUrl"]);
            obj.LnkWrapperCssClass = ConvertUtility.ConvertNullToEmptyString(row["LnkWrapperCssClass"]);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        //public static List<Lnk_LinkConfig> SelectByLnkID(Int32 lnkID)
        //{
        //    if (lnkID < 1) return null;
        //    String cacheKey = GetCacheKeyByLnkID(lnkID);

        //    ConnectDataCache cache = new ConnectDataCache();
        //    List<Lnk_LinkConfig> list = cache.GetCachedItem(cacheKey) as List<Lnk_LinkConfig>;
        //    if (list == null || refreshFromDB)
        //    {
        //        DataTable tb = Data.DACustomLink.DALnk_LinkConfig.SelectByLnkID(lnkID);
        //        if (tb == null) return null;
        //        list = new List<Lnk_LinkConfig>();
        //        foreach (DataRow r in tb.Rows)
        //            list.Add(InitData(r));
        //        // 1 year exp.  when admin add new list, we should clear the cache
        //        cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
        //    }

        //    return list;
        //}
    }
}
