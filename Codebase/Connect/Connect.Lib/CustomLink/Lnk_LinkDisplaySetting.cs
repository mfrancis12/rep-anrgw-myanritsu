﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class Lnk_LinkDisplaySetting
    {
        public Int32 LnkSettingID { get; set; }
        public Int32 LnkID { get; set; }
        public String PageUrl { get; set; }
        public String LocationType { get; set; }
        public Int32 LoadOrder { get; set; }
        public String GWRegionByAddress { get; set; }
        public String ModelNumber { get; set; }
        public Guid AccountID { get; set; }
        public Boolean Visible { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
