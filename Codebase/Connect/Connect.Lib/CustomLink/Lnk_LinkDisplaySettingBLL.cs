﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class Lnk_LinkDisplaySettingBLL
    {
        //internal const String DATACACHE_LNKSETT_PREFIX = "CustomLink_Setting_";

        internal static Lnk_LinkDisplaySetting InitData(DataRow row)
        {
            if (row == null) return null;

            Lnk_LinkDisplaySetting obj = new Lnk_LinkDisplaySetting();
            obj.LnkSettingID = ConvertUtility.ConvertToInt32(row["LnkSettingID"], 0);
            obj.LnkID = ConvertUtility.ConvertToInt32(row["LnkID"], 0);
            obj.PageUrl = ConvertUtility.ConvertNullToEmptyString(row["PageUrl"]);
            obj.LocationType = ConvertUtility.ConvertNullToEmptyString(row["LocationType"]);
            obj.LoadOrder = ConvertUtility.ConvertToInt32(row["LoadOrder"], 0);
            obj.GWRegionByAddress = ConvertUtility.ConvertNullToEmptyString(row["GWRegionByAddress"]);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(row["ModelNumber"]);
            obj.AccountID = ConvertUtility.ConvertToGuid(row["AccountID"], Guid.Empty);
            obj.Visible = ConvertUtility.ConvertToBoolean(row["Visible"], true);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        //private static String GetCacheKeyByLnkID(Int32 lnkID)
        //{
        //    if (lnkID < 1) return String.Empty;
        //    return String.Format("{0}{1}", DATACACHE_LNKSETT_PREFIX, lnkID);
        //}

        //public static List<Lnk_LinkDisplaySetting> SelectByLnkID(Boolean refreshFromDB, Int32 lnkID)
        //{
        //    if (lnkID < 1) return null;
        //    String cacheKey = GetCacheKeyByLnkID(lnkID);

        //    ConnectDataCache cache = new ConnectDataCache();
        //    List<Lnk_LinkDisplaySetting> list = cache.GetCachedItem(cacheKey) as List<Lnk_LinkDisplaySetting>;
        //    if (list == null || refreshFromDB)
        //    {
        //        DataTable tb = Data.DACustomLink.DALnk_LinkDisplaySettings.SelectByLnkID(lnkID);
        //        if (tb == null) return null;
        //        list = new List<Lnk_LinkDisplaySetting>();
        //        foreach (DataRow r in tb.Rows)
        //            list.Add(InitData(r));
        //        // 1 year exp.  when admin add new list, we should clear the cache
        //        cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
        //    }

        //    return list;
        //}
    }
}
