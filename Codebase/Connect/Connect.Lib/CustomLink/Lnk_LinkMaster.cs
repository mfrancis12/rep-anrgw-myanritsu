﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class Lnk_LinkMaster
    {
        public Int32 LnkID { get; set; }
        public String LnkName { get; set; }
        public String LnkTarget { get; set; }
        public String LnkClassKey { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public List<Lnk_LinkConfig> LinkConfigs { get; set; }
        public List<Lnk_LinkDisplaySetting> LinkDisplaySettings { get; set; }
    }
}
