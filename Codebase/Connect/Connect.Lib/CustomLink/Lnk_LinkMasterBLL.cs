﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class Lnk_LinkMasterBLL
    {
        internal const String DATACACHE_LNKMAST_ALL = "CustomLink_Mast_ALL";

        private static Lnk_LinkMaster InitData(DataRow row, DataTable configs, DataTable settings)
        {
            if (row == null) return null;

            Lnk_LinkMaster obj = new Lnk_LinkMaster();
            obj.LnkID = ConvertUtility.ConvertToInt32(row["LnkID"], 0);
            obj.LnkName = ConvertUtility.ConvertNullToEmptyString(row["LnkName"]);
            obj.LnkTarget = ConvertUtility.ConvertNullToEmptyString(row["LnkTarget"]);
            obj.LnkClassKey = String.Format("STLNK_{0}", obj.LnkID);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            obj.LinkConfigs = new List<Lnk_LinkConfig>();
            obj.LinkDisplaySettings = new List<Lnk_LinkDisplaySetting>();

            if (configs != null)
            {
                foreach (DataRow rCfg in configs.Rows)
                    obj.LinkConfigs.Add(Lnk_LinkConfigBLL.InitData(rCfg));
            }

            if (settings != null)
            {
                foreach (DataRow rSetting in settings.Rows)
                    obj.LinkDisplaySettings.Add(Lnk_LinkDisplaySettingBLL.InitData(rSetting));
            }
            return obj;
        }

        //private static String GetCacheKeyByLnkID(Int32 lnkID)
        //{
        //    if (lnkID < 1) return String.Empty;
        //    return String.Format("{0}{1}", DATACACHE_LNKMAST_PREFIX, lnkID);
        //}

        public static List<Lnk_LinkMaster> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_LNKMAST_ALL;

            ConnectDataCache cache = new ConnectDataCache();
            List<Lnk_LinkMaster> list = cache.GetCachedItem(cacheKey) as List<Lnk_LinkMaster>;
            if (list == null || refreshFromDB)
            {
                DataTable tb = Data.DACustomLink.DALnk_LinkMaster.SelectAll();
                if (tb == null) return null;
                list = new List<Lnk_LinkMaster>();
                foreach (DataRow r in tb.Rows)
                {
                    Int32 lnkID = ConvertUtility.ConvertToInt32(r["LnkID"], 0);
                    if (lnkID < 1) continue;
                    DataTable tbConfigs = null;
                    DataTable tbSettings = null;
                    DataRow linkInfoRow = Data.DACustomLink.DALnk_LinkMaster.SelectByLnkID(lnkID, out tbConfigs, out tbSettings);

                    list.Add(InitData(linkInfoRow, tbConfigs, tbSettings));
                }

                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static List<Lnk_LinkMaster> SelectFiltered(Boolean refreshFromDB, List<String> locationTypes)
        {
            List<Lnk_LinkMaster> list = SelectAll(refreshFromDB);
            if (list == null) return null;
            var query = from lnk in list 
                        .Where(s => s.LinkDisplaySettings == s.LinkDisplaySettings
                        .Where(setting => locationTypes.Contains(setting.LocationType)))
                        select lnk;
            return query.ToList();
        }

        public static Lnk_LinkMaster SelectByLnkID(Int32 lnkID)
        {
            List<Lnk_LinkMaster> list = SelectAll(false);
            var query = from lnk in list
                        where lnk.LnkID == lnkID
                        select lnk;
            if (query == null || query.First() == null)
            {
                list = SelectAll(true);
                query = from lnk in list
                            where lnk.LnkID == lnkID
                            select lnk;
            }

            if (query == null) return null;
            return query.First();
        }
    }
}
