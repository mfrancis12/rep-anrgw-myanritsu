﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.CustomLink
{
    [Serializable]
    public class Lnk_LocationType
    {
        public String LinkLocationType { get; set; }
        public Int32 MaxLinks { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
