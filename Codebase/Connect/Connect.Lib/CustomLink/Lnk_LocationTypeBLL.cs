﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.CustomLink
{
    public static class Lnk_LocationTypeBLL
    {
        internal const String DATACACHE_LOCTYPES = "CustomLink_LocTypes";

        private static Lnk_LocationType InitData(DataRow row)
        {
            if (row == null) return null;

            Lnk_LocationType obj = new Lnk_LocationType();
            obj.LinkLocationType = ConvertUtility.ConvertNullToEmptyString(row["LinkLocationType"]);
            obj.MaxLinks = ConvertUtility.ConvertToInt32(row["MaxLinks"], 0);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<Lnk_LocationType> SelectAll(Boolean refreshFromDB)
        {
            String cacheKey = DATACACHE_LOCTYPES;

            ConnectDataCache cache = new ConnectDataCache();
            List<Lnk_LocationType> list = cache.GetCachedItem(cacheKey) as List<Lnk_LocationType>;
            if (list == null || refreshFromDB)
            {
                DataTable tb = Data.DACustomLink.DALnk_LocationType.SelectAll();
                if (tb == null) return null;
                list = new List<Lnk_LocationType>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }
    }
}
