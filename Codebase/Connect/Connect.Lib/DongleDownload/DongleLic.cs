﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Anritsu.Connect.Lib.DongleDownload
{
    [Serializable]
    public class DongleLic
    {
        public DataTable DongleInfo { get; set; }
        public DateTime SessionIssuedOn { get; set; }
        public Boolean IsUserValidated { get; set; }
    }
}
