﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.DongleDownload
{
    public static class DongleLicBLL
    {
        public static DongleLic CreateNew(String usbNo, String usbPwd)
        {
            if (!Lib.DongleDownload.DongleLicBLL.IsValidUSBNoAndPassword(usbNo, usbPwd)) return null;

            DataRow dongleRow = FindDongleByUSBNo(usbNo.ConvertNullToEmptyString().Trim());
            if (dongleRow == null) return null;
            
            #region " USBPwd validatoin "
            //cannot validate against the database because we don't have the usbcode data in JP download database.  I suppose we have to hardcode it.
            //if (!usbPwd.Equals(dongleRow["USBCode"].ToString(), StringComparison.InvariantCultureIgnoreCase)) return null;
            Regex usbPwdRgx = new Regex(ConfigUtility.AppSettingGetValue("Connect.Lib.JPDongle.USBCodes"), RegexOptions.IgnoreCase);
            if (!usbPwdRgx.IsMatch(usbPwd)) return null;
            #endregion


            DongleLic dlic = new DongleLic();
            dlic.DongleInfo = dongleRow.Table;
            dlic.SessionIssuedOn = DateTime.UtcNow;
            dlic.IsUserValidated = false;
            return dlic;
        }

        public static String GetValue(DataTable dongleData, String fieldName)
        {
            if (fieldName.IsNullOrEmptyString() || dongleData == null || dongleData.Rows.Count != 1) return String.Empty;
            return ConvertUtility.ConvertNullToEmptyString(dongleData.Rows[0][fieldName]);
        }     

        public static Boolean IsValidUSBNoAndPassword(String usbNo, String pwd)
        {
            if (usbNo.IsNullOrEmptyString() || pwd.IsNullOrEmptyString()) return false;
            Regex rgx = new Regex("[a-zA-Z0-9-_]", RegexOptions.IgnoreCase);
            Regex usbPwdRgx = new Regex(ConfigUtility.AppSettingGetValue("Connect.Lib.JPDongle.USBCodes"), RegexOptions.IgnoreCase);
            return rgx.IsMatch(usbNo) && usbPwdRgx.IsMatch(pwd);
                //(pwd.Equals("webgatekey", StringComparison.InvariantCultureIgnoreCase)
                //|| pwd.Equals("katsuyo", StringComparison.InvariantCultureIgnoreCase));

        }

        public static Boolean CheckDongle(String usbNo, String email)
        {
            DataRow row = FindDongleByUSBNo(usbNo.ConvertNullToEmptyString().Trim());
            if(row == null) return false;
            String ownerEmail = ConvertUtility.ConvertNullToEmptyString(row["EmailAddress"]);
            String usbStatus =  ConvertUtility.ConvertNullToEmptyString(row["USBKeyStatus"]);
            return ownerEmail.Equals(email.ConvertNullToEmptyString(), StringComparison.InvariantCultureIgnoreCase)
                && usbStatus.Equals("active", StringComparison.InvariantCultureIgnoreCase);
        }

        public static DataTable FindDongle(String keyword)
        {
            return FindDongle(keyword.ConvertNullToEmptyString().Trim(), false);
        }

        public static DataRow FindDongleByUSBNo(String usbNo)
        {
            DataTable tb = Data.DAJPDL_Dongle.FindDongle(usbNo.ConvertNullToEmptyString().Trim(), true);
            if (tb == null || tb.Rows.Count != 1) return null;
            return tb.Rows[0];
        }

        public static DataTable FindDongle(String keyword, Boolean exactMatchUSBNo)
        {
            return Data.DAJPDL_Dongle.FindDongle(keyword.ConvertNullToEmptyString().Trim(), exactMatchUSBNo);
        }

        public static Boolean Insert(String usbNo, String usbEmail, String adminEmail)
        {
            if (usbNo.IsNullOrEmptyString() || usbEmail.IsNullOrEmptyString()) return false;
            if (!IsValidUSBNoAndPassword(usbNo, "webgatekey")) return false;
            Data.DAJPDL_Dongle.Insert(usbNo, usbEmail, adminEmail);
            return true;
        }

        public static void Update(String usbNo, String usbStatusCode)
        {
            if (usbNo.IsNullOrEmptyString() || usbStatusCode.IsNullOrEmptyString()) return;
            Data.DAJPDL_Dongle.Update(usbNo, usbStatusCode);
        }

        public static Boolean Delete(String usbKey)
        {
            if (usbKey.IsNullOrEmptyString()) return false;
            DataRow resultRow = Data.DAJPDL_Dongle.Delete(usbKey.ConvertNullToEmptyString().Trim());
            if (resultRow == null) return false;
            Int32 returnCode = ConvertUtility.ConvertToInt32(resultRow["IsDeleted"], 0);
            return returnCode == 1;
        }
    }
}
