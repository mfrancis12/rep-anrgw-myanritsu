﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DownloadRoute
{
    [Serializable]
    public enum DownloadTypeCode
    {
        JPDL, JPDLUSB, JPVERTBL
    }
}
