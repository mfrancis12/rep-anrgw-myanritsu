﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DownloadRoute
{
    [Serializable]
    public class GW_DownloadRoutingToken
    {
        public Guid RouteTokenID { get; set; }
        public String DownloadType { get; set; }
        public String FileName { get; set; }
        public String FileNameForUser { get; set; }
        public String DownloadCategory { get; set; }
        public String ModelNumber { get; set; }
        public String RequestedUserEmail { get; set; }
        public String RequestedUserName { get; set; }
        public String RequestedUserSessionID { get; set; }
        public String RequestedUserIP { get; set; }
        public DateTime TokenExp { get; set; }
        public Boolean IsForcedExpired { get; set; }
        public Int32 EncodeFlag { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
