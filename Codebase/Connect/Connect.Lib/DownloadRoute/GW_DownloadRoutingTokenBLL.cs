﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.DownloadRoute
{
    public static class GW_DownloadRoutingTokenBLL
    {
        internal static GW_DownloadRoutingToken InitFromData(DataRow r)
        {
            if (r == null) return null;
            GW_DownloadRoutingToken obj = new GW_DownloadRoutingToken();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.DownloadType = ConvertUtility.ConvertNullToEmptyString(r["DownloadType"]);
            obj.FileName = ConvertUtility.ConvertNullToEmptyString(r["FileName"]);
            obj.FileNameForUser = ConvertUtility.ConvertNullToEmptyString(r["FileNameForUser"]);
            obj.DownloadCategory = ConvertUtility.ConvertNullToEmptyString(r["DownloadCategory"]);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.IsForcedExpired = ConvertUtility.ConvertToBoolean(r["IsForcedExpired"], false);
            obj.RequestedUserEmail = ConvertUtility.ConvertNullToEmptyString(r["RequestedUserEmail"]);
            obj.RequestedUserName = ConvertUtility.ConvertNullToEmptyString(r["RequestedUserName"]);
            obj.RequestedUserSessionID = ConvertUtility.ConvertNullToEmptyString(r["RequestedUserSessionID"]);
            obj.RequestedUserIP = ConvertUtility.ConvertNullToEmptyString(r["RequestedUserIP"]);
            obj.RouteTokenID = ConvertUtility.ConvertToGuid(r["RouteTokenID"], Guid.Empty);
            obj.TokenExp = ConvertUtility.ConvertToDateTime(r["TokenExp"], DateTime.MinValue);
            obj.EncodeFlag = ConvertUtility.ConvertToInt32(r["EncodeFlag"], 0);
            return obj;
        }

        public static GW_DownloadRoutingToken SelectByTokenID(Guid routeTokenID)
        {
            return SelectByTokenID(routeTokenID, false);
        }

        public static GW_DownloadRoutingToken SelectByTokenID(Guid routeTokenID, Boolean updateAccess)
        {
            if (routeTokenID.IsNullOrEmptyGuid()) return null;
            GW_DownloadRoutingToken obj = InitFromData(Data.DADownloadRoute.SelectByTokenID(routeTokenID, updateAccess));
            return obj;
        }

        public static Guid Insert(DownloadTypeCode downloadType,
            String fileName,
            String fileNameForUser,
            String downloadCategory,
            String modelNumber,
            String requestedUserEmail,
            String requestedUserName,
            String requestedUserSessionID,
            String requestedUserIP,
            Guid requestedAccountID,
            Guid requestedProdRegWAK,
            Int32 encodeFlag)
            //out String urlToDownload)
        {
            //urlToDownload = String.Empty;
            DataRow r = Data.DADownloadRoute.Insert(Enum.GetName(typeof(DownloadTypeCode), downloadType),
                fileName,
                fileNameForUser,
                downloadCategory,
                modelNumber,
                requestedUserEmail,
                requestedUserName,
                requestedUserSessionID,
                requestedUserIP,
                requestedAccountID,
                requestedProdRegWAK,
                encodeFlag
                );
            if (r == null) return Guid.Empty;
            Guid token = ConvertUtility.ConvertToGuid(r["RouteTokenID"], Guid.Empty);
            //if (!token.IsNullOrEmptyGuid()) urlToDownload = GetDownloadURL(token, fileNameForUser);
            return token;
        }

        public static Boolean UpdateTokenExp(Guid routeTokenID, DateTime newTokenExpUTC)
        {
            return Data.DADownloadRoute.UpdateTokenExp(routeTokenID, newTokenExpUTC) > 0;
        }

        public static Boolean ForceToExpire(Guid routeTokenID, DateTime newTokenExpUTC)
        {
            return Data.DADownloadRoute.UpdateForceExpire(routeTokenID) > 0;
        }

        //public static String GetDownloadURL(Guid tokenID, String filePath)
        //{
        //    if (tokenID.IsNullOrEmptyGuid() || filePath.IsNullOrEmptyString()) return String.Empty;
        //    return String.Format("{0}/{1}/{2}"
        //        , ConfigUtility.AppSettingGetValue("Connect.Lib.DownloadRoute.BaseUrl")
        //        , tokenID.ToString()
        //        , filePath.Replace(@"\", "/")).ToLowerInvariant();
        //}

       

        
    }
}
