﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_Form
    {
        public Guid FormID { get; set; }
        public Int32 ModuleID { get; set; }
        public String FormName { get; set; }
        public String FormType { get; set; }
        public String FormNameClassKey { get { return String.Format("FRM_{0}", FormID.ToString().ToUpperInvariant()); } }
        public DateTime CreatedOnUTC { get; set; }
        public UI.Site_Module ModuleInfo { get; internal set; }
        public Boolean IsFeedbackForm { get { return FormType == "feedback"; } }
        public Boolean LockDelete { get; set; }
    }
}
