﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormControl
    {
        public Int32 ControlID { get; set; }
        public String ControlName { get; set; }
        public String ControlPath { get; set; }
        public Boolean IsDataSourceRequired { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
