﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormControlDataSource
    {
        public Int32 DataSourceID { get; set; }
        public String DataSourceName { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public List<Frm_FormControlDataSourceData> DataItems { get; internal set; }
    }
}
