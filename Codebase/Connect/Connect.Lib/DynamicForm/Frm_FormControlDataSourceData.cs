﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormControlDataSourceData
    {
        public Int32 DSDataID { get; set; }
        public Int32 DataSourceID { get; set; }
        public Boolean IsSelected { get; set; }
        public Int32 OrderNum { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
