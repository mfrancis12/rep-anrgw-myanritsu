﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormFieldset
    {
        public Int32 FieldsetID { get; set; }
        public Guid FormID { get; set; }
        public Int32 OrderNum { get; set; }
        public Boolean IsInternal { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
