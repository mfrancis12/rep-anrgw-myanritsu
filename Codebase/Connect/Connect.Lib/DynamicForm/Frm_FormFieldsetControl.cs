﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormFieldsetControl
    {
        public Int32 FwscID { get; set; }
        public Int32 FieldsetID { get; set; }
        public Guid FormID { get; set; }
        public Int32 ControlID { get; set; }
        public Int32 ControlOrder { get; set; }
        public Boolean IsRequired { get; set; }
        public Int32 DataSourceID { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public String FieldName { get; set; }
        public Frm_FormControl ControlInfo { get; internal set; }
        public List<Frm_FormFieldsetControlProperty> Properties { get; internal set; }
    }
}
