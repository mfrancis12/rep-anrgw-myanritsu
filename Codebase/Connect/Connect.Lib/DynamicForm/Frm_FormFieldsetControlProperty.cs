﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormFieldsetControlProperty
    {
        public Int32 PropertyID { get; set; }
        public Int32 FwscID { get; set; }
        public String PropertyKey { get; set; }
        public String PropertyValue { get; set; }
    }
}
