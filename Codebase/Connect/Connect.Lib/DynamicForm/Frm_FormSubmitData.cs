﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormSubmitData
    {
        public Int32 SubmitDataID { get; set; }
        public Int32 FormSubmitID { get; set; }
        public Int32 FwscID { get; set; }
        public String FieldKey { get; set; }
        public String SubmittedValueStr { get; set; }
        public String SubmittedValueBinary { get; set; }
        public String SubmittedUser { get; set; }
        public DateTime LastUpdatedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
