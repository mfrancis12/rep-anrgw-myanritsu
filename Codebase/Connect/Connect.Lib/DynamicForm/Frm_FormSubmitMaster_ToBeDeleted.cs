﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public class Frm_FormSubmitMaster_ToBeDeleted
    {
        public Int32 FormSubmitID { get; set; }
        public Guid FormSubmitKey { get; set; }
        public Guid FormID { get; set; }
        public String InitialSubmitUser { get; set; }
        public DateTime LastUpdatedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
