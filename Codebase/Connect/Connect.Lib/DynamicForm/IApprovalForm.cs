﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.DynamicForm
{
    public interface IDynamicForm
    {
        String SelectedValue { get; set; }
    }
}
