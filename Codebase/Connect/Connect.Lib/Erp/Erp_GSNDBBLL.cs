﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.Erp
{
    public static class Erp_GSNDBBLL
    {
        public static DataTable GSNDB_Search(String modelNumber, String serialNumber)
        {
            return Data.DAErpData.GSNDB_Search(modelNumber.ConvertNullToEmptyString().Trim(), serialNumber.ConvertNullToEmptyString().Trim());
        }
        public static List<String> GSNDB_SearchSerialNumbers(String modelNumber, String serialNumber)
        {
            List<String> lstSerialNumbers = new List<string>();
            var resultTable = Data.DAErpData.GSNDB_SearchSNByMN(modelNumber.ConvertNullToEmptyString().Trim(), serialNumber.ConvertNullToEmptyString().Trim());
            if (resultTable != null && resultTable.Rows.Count > 0)
            {
                foreach (DataRow dr in resultTable.Rows)
                {
                    if (!lstSerialNumbers.Contains(Convert.ToString(dr["SerialNumber"])))
                        lstSerialNumbers.Add(Convert.ToString(dr["SerialNumber"]));
                }
            }
            return lstSerialNumbers;
        }
    }
}
