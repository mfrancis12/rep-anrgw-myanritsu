﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Erp
{
    public static class Erp_ProductConfigBLL
    {
        //internal static Erp_ProductConfig_TBD InitFromData(DataRow r, DataTable tbModelTags)
        //{
        //    if (r == null) return null;
        //    Erp_ProductConfig_TBD obj = new Erp_ProductConfig_TBD();
        //    obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
        //    obj.IsPaidSupportModel = ConvertUtility.ConvertToBoolean(r["IsPaidSupportModel"], false);
        //    obj.IsActive = ConvertUtility.ConvertToBoolean(r["IsActive"], false);
        //    obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
        //    obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
        //    obj.ProductOwnerRegion = ConvertUtility.ConvertNullToEmptyString(r["ProductOwnerRegion"]);
        //    //obj.ScheduleServiceUrl = ConvertUtility.ConvertNullToEmptyString(r["ScheduleServiceUrl"]);
        //    //obj.GetQuoteUrl = ConvertUtility.ConvertNullToEmptyString(r["GetQuoteUrl"]);
        //    //obj.GetServiceQuoteUrl = ConvertUtility.ConvertNullToEmptyString(r["GetServiceQuoteUrl"]);
        //    obj.ImageUrl = ConvertUtility.ConvertNullToEmptyString(r["ImageUrl"]);
        //    obj.VerifyCompany = ConvertUtility.ConvertToBoolean(r["VerifyCompany"], false);
        //    obj.VerifyProductSN = ConvertUtility.ConvertToBoolean(r["VerifyProductSN"], false);
        //    obj.AddOnFormID = ConvertUtility.ConvertToGuid(r["AddOnFormID"], Guid.Empty);
        //    obj.AddOnFormPerSN = ConvertUtility.ConvertToBoolean(r["AddOnFormPerSN"], false);
        //    if (tbModelTags != null)
        //    {
        //        obj.ModelTags = new List<string>();
        //        foreach (DataRow rModelTag in tbModelTags.Rows)
        //        {
        //            obj.ModelTags.Add(rModelTag["TagModelNumber"].ToString().Trim());
        //        }
        //    }
        //    obj.FeedbackFormID = ConvertUtility.ConvertToGuid(r["FeedbackFormID"], Guid.Empty);
        //    obj.UserACLRequired = ConvertUtility.ConvertToBoolean(r["UserACLRequired"], false);
        //    obj.SkipSerialNumberValidation = ConvertUtility.ConvertToBoolean(r["SNValidationNotRequired"], false);
        //    obj.IsVisibleToUser = ConvertUtility.ConvertToBoolean(r["IsVisibleToCustomers"], true);
        //    obj.SNPattern = ConvertUtility.ConvertNullToEmptyString(r["SNPattern"]);
        //    return obj;
        //}

        //public static Erp_ProductConfig_TBD SelectByModelNumber(String modelNumber)
        //{
        //    if (modelNumber.IsNullOrEmptyString()) return null;
        //    DataTable tbModelTags = null;
        //    DataRow r = Data.DAErp_ProductConfig.SelectByModelNumber(modelNumber, out tbModelTags);
        //    return InitFromData(r, tbModelTags);
        //}

        //public static List<Erp_ProductConfig_TBD> SelectWithFilter(String modelNumber, String productOwnerRegion
        //    , Boolean isPaidSupportModel, Boolean isActive)
        //{
        //    DataTable tb = Data.DAErp_ProductConfig.SelectWithFilter(modelNumber.Trim(), 
        //        productOwnerRegion.Trim(), 
        //        isPaidSupportModel,
        //        isActive);
        //    if (tb == null) return null;
        //    List<Erp_ProductConfig_TBD> lst = new List<Erp_ProductConfig_TBD>();
        //    foreach (DataRow r in tb.Rows)
        //    {
        //        DataTable tbModelTags = Data.DAErp_ProductConfig_ModelTag.SelectByModelNumber(r["ModelNumber"].ToString());
        //        lst.Add(InitFromData(r, tbModelTags));
        //    }
        //    return lst;
        //}

        //public static DataTable SelectWithFilter(String modelNumber, String productOwnerRegion)
        //{
        //    return Data.DAErp_ProductConfig.SelectWithFilter(modelNumber.Trim(),
        //        productOwnerRegion.Trim(), null, null);
        //}

        //public static DataTable SelectWithFilter(String modelNumber, String productOwnerRegion, Boolean isActive)
        //{
        //    return Data.DAErp_ProductConfig.SelectWithFilter(modelNumber.Trim(),
        //        productOwnerRegion.Trim(), null, isActive);
        //}

        //public static void InsertUpdate(Erp_ProductConfig_TBD pcfg)
        //{
        //    if (pcfg == null || pcfg.ModelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("pcfg");
        //    Data.DAErp_ProductConfig.InsertUpdate(pcfg.ModelNumber
        //        , pcfg.ProductOwnerRegion
        //        , pcfg.IsPaidSupportModel
        //        , pcfg.VerifyCompany
        //        , pcfg.VerifyProductSN
        //        , pcfg.UserACLRequired
        //        , pcfg.ImageUrl
        //        , pcfg.AddOnFormID
        //        , pcfg.AddOnFormPerSN
        //        , pcfg.FeedbackFormID
        //        , pcfg.SkipSerialNumberValidation
        //        , pcfg.IsVisibleToUser);
        //}

        //public static void Delete(String modelNumber)
        //{
        //    if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
        //    Data.DAErp_ProductConfig.Delete(modelNumber.Trim());
        //}

        //public static void DeleteAll(String modelNumber)
        //{
        //    if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
        //    Data.DAErp_ProductConfig.DeleteModelNumber(modelNumber.Trim());
        //}
    }
}
