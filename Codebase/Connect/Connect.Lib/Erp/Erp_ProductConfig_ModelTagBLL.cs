﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Erp
{
    public static class Erp_ProductConfig_ModelTagBLL
    {
        public static DataTable SelectByModelNumber(String modelNumber)
        {
            if (String.IsNullOrEmpty(modelNumber)) return null;
            return Data.DAErp_ProductConfig_ModelTag.SelectByModelNumber(modelNumber);
        }

        public static Int32 Insert(String modelNumber, String tagModelNumber)
        {
            if (String.IsNullOrEmpty(modelNumber) || String.IsNullOrEmpty(tagModelNumber)) return 0;
            DataRow r = Data.DAErp_ProductConfig_ModelTag.Insert(modelNumber.Trim(), tagModelNumber.Trim());
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["ModelTagID"], 0);
        }

        public static Boolean Delete(Int32 modelTagID)
        {
            if (modelTagID < 1) return false;
            return Data.DAErp_ProductConfig_ModelTag.Delete(modelTagID) > 0;
        }

        public static void DeleteAll(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) throw new ArgumentNullException("modelNumber");
            Data.DAErp_ProductConfig_ModelTag.DeleteAll(modelNumber.Trim());
        }
    }
}
