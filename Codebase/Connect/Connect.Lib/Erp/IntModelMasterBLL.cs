﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Erp
{
    public static class IntModelMasterBLL
    {
        public static DataTable SelectAll()
        {
            return Data.DAIntModelMaster.SelectAll();
        }
    }
}
