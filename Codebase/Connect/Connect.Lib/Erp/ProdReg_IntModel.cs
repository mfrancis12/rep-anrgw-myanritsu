﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Erp
{
    [Serializable]
    public class ProdReg_IntModel
    {
        public Int32 IntMnTagID { get; set; }
        public Int32 ProdRegID { get; set; }
        public String IntModelNumber { get; set; }
        public String USBNo { get; set; }
        public String ModelTarget { get; set; }
        public DateTime ExpireOnUTC { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
