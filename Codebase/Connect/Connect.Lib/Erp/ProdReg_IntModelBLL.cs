﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Lib.Erp
{
    public static class ProdReg_IntModelBLL
    {
        internal static ProdReg_IntModel InitFromData(DataRow r)
        {
            if (r == null) return null;
            ProdReg_IntModel obj = new ProdReg_IntModel();
            obj.IntMnTagID = ConvertUtility.ConvertToInt32(r["IntMnTagID"], 0);
            obj.ProdRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
            obj.IntModelNumber = ConvertUtility.ConvertNullToEmptyString(r["IntModelNumber"]);
            obj.USBNo = ConvertUtility.ConvertNullToEmptyString(r["USBNo"]);
            obj.ModelTarget = ConvertUtility.ConvertNullToEmptyString(r["ModelTarget"]);
            obj.CreatedBy = r["CreatedBy"].ToString();
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.ExpireOnUTC = ConvertUtility.ConvertToDateTime(r["ExpireOnUTC"], DateTime.MinValue);          
            return obj;
        }

        public static DataTable SelectByProdRegIDAsDataTable(Int32 prodRegID)
        {
            if (prodRegID < 1) return null;
            return Data.DAProdReg_IntModel.SelectByProdRegID(prodRegID);
        }

        public static List<ProdReg_IntModel> SelectByProdRegID(Int32 prodRegID, out List<ProdReg_IntModel> expiredIntModels)
        {
            expiredIntModels = null;
            if (prodRegID < 1) return null;
            List<ProdReg_IntModel> lst = new List<ProdReg_IntModel>();
            expiredIntModels = new List<ProdReg_IntModel>();
            DataTable tb = SelectByProdRegIDAsDataTable(prodRegID);
            if (tb == null || tb.Rows.Count < 1) return null;
            foreach (DataRow r in tb.Rows)
            {
                ProdReg_IntModel im = InitFromData(r);
                if (im == null) continue;
                if (im.ExpireOnUTC == DateTime.MinValue || im.ExpireOnUTC > DateTime.UtcNow)
                    lst.Add(im);
                else
                    expiredIntModels.Add(im);
            }
            return lst;
        }

        public static List<ProdReg_IntModel> GetIncludedmodelsByProdRegID(Int32 prodRegID, out List<ProdReg_IntModel> inludedIntModels)
        {
            inludedIntModels = null;
            if (prodRegID < 1) return null;
            List<ProdReg_IntModel> lst = new List<ProdReg_IntModel>();
            inludedIntModels = new List<ProdReg_IntModel>();
            DataTable tb = SelectByProdRegIDAsDataTable(prodRegID);
            if (tb == null || tb.Rows.Count < 1) return null;
            foreach (DataRow r in tb.Rows)
            {
                ProdReg_IntModel im = InitFromData(r);
                if (im == null) continue;
                    lst.Add(im);
            }
            return lst;
        }

        public static void DeleteByIntMnTagID(Int32 intMnTagID)
        {
            Data.DAProdReg_IntModel.DeleteByIntMnTagID(intMnTagID);
        }

        public static Int32 Insert(Int32 prodRegID, String intModelNumber, String createdBy, DateTime optionalExpirationDate)
        {
            DataRow r = Data.DAProdReg_IntModel.Insert(prodRegID, intModelNumber, createdBy, optionalExpirationDate);
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["IntMnTagID"], 0);
        }
    }
}
