﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ISO
{
    [Serializable]
    public class CountryCodeWithGlobalWebRegion
    {
        public Int32 CultureGroupId { get; set; }
        public String GlobalWebRegionCultureCode { get; set; }
        public String CountryCode { get; set; }
    }
}
