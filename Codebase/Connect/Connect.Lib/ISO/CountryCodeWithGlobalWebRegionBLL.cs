﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ISO
{
    public static class CountryCodeWithGlobalWebRegionBLL
    {
        internal const String DATACACHE = "CcGwRgnALL";

        private static CountryCodeWithGlobalWebRegion InitData(DataRow row)
        {
            if (row == null) return null;

            CountryCodeWithGlobalWebRegion obj = new CountryCodeWithGlobalWebRegion();
            obj.CultureGroupId = ConvertUtility.ConvertToInt32(row["CultureGroupId"], 0);
            obj.GlobalWebRegionCultureCode = ConvertUtility.ConvertNullToEmptyString(row["CultureCode"]);
            obj.CountryCode = ConvertUtility.ConvertNullToEmptyString(row["Alpha2"]);
            return obj;
        }

        public static List<CountryCodeWithGlobalWebRegion> SelectAll(Boolean refreshFromDB)
        {
            ConnectDataCache cache = new ConnectDataCache();
            List<CountryCodeWithGlobalWebRegion> list = cache.GetCachedItem(DATACACHE) as List<CountryCodeWithGlobalWebRegion>;
            if (list == null || refreshFromDB)
            {
                list = new List<CountryCodeWithGlobalWebRegion>();
                DataTable tb = Data.DAIso_Country.SelectCountryWithGlobalWebRegion();
                if (tb == null) return null;
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r));
                // 1yr exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(DATACACHE, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static List<CountryCodeWithGlobalWebRegion> SelectByRegionCode(String globalWebRegionCultureCode)
        {
            if (globalWebRegionCultureCode.IsNullOrEmptyString()) return null;
            List<CountryCodeWithGlobalWebRegion> list = SelectAll(false);
            var query = from gwr in list
                        where gwr.GlobalWebRegionCultureCode.Equals(globalWebRegionCultureCode, StringComparison.InvariantCultureIgnoreCase)
                        select gwr;
            if (query == null) return null;
            return query.ToList();
        }

        public static CountryCodeWithGlobalWebRegion SelectByCountryCode(String countryCode)
        {
            if (countryCode.IsNullOrEmptyString()) return null;
            List<CountryCodeWithGlobalWebRegion> list = SelectAll(false);
            var query = from gwr in list
                        where gwr.CountryCode.Equals(countryCode, StringComparison.InvariantCultureIgnoreCase)
                        select gwr;
            if (query == null || query.Count() < 1) return null;
            return query.First();
        }
    }
}
