﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Notification
{
    public class Notification
    {
        public int NotificationID { get; set; }
        public String NotificationName { get; set; }
        public int DisplayAreaID { get; set; }
        public String InternalComments { get; set; }
        public String NotificationClassKey { get { return String.Format("NOTIFICATION_{0}", NotificationID.ToString().ToUpperInvariant()); } }
        public DateTime CreatedOnUTC { get; set; }
        public Boolean IsActive { get; set; }
    }
}
