﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Notification
{
    public static class NotificationBLL
    {
        internal static Notification InitNotificationData(DataRow r)
        {
            if (r == null) return null;
            Notification objNotification = new Notification();

            objNotification.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            objNotification.NotificationID = ConvertUtility.ConvertToInt32(r["NotificationID"], 0);
            objNotification.NotificationName = ConvertUtility.ConvertNullToEmptyString(r["NotificationName"]);
            objNotification.InternalComments = ConvertUtility.ConvertNullToEmptyString(r["InternalComments"]);
            objNotification.DisplayAreaID = ConvertUtility.ConvertToInt32(r["DisplayAreaID"], 0);
            objNotification.IsActive = ConvertUtility.ConvertToBoolean(r["IsActive"], false);

            return objNotification;
        }

        public static DataTable SelectNotificationDisplayAreas()
        {
            return Data.DANotification.SelectNotificationDisplayAreas();
        }

        public static int Insert(String notificationName, int displayAreaId,String internalComments, String createdBy, Boolean isActive)
        {
            DataRow r = Data.DANotification.Insert(notificationName, displayAreaId, internalComments, createdBy, isActive);
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["NotificationID"], 0);
        }

        public static Notification SelectByNotificationID(int notificationID)
        {
            if (notificationID==0) return null;
            return InitNotificationData(Data.DANotification.SelectByNotificationID(notificationID));
        }

        public static void Delete(int notificationID)
        {
            if (notificationID==0) return;
            Data.DANotification.Delete(notificationID);
        }

        public static DataTable SelectAllAsTable()
        {
            return Data.DANotification.SelectAll();
        }

        public static void Update(int notificationID, String name, String internalComments, int displayAreaId, String modifiedBy, Boolean isActive)
        {
            Data.DANotification.Update(notificationID, name, internalComments, displayAreaId, modifiedBy, isActive);
        }
        public static int? SelectActiveOrDefaultNotification()
        {
            DataTable dtNotfication = Data.DANotification.SelectActiveOrDefaultNotification();
            if (dtNotfication == null || dtNotfication.Rows.Count == 0) return null;
                return ConvertUtility.ConvertToInt32(dtNotfication.Rows[0]["NotificationID"],0);
        }
    }
}
