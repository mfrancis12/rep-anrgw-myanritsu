﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDDocTypeFilter
    {
        public Int32 DocTypeFilterID { get; set; }
        public String DocTypeFilterName { get; set; }
        public String DocTypeFilterOwnerRole { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public List<PIDDocTypeFilterItem> FilterItems { get; set; }
    }
}
