﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDDocTypeFilterBLL
    {
        internal const String SIS_DATACACHE_DocTypeFilter_PREFIX = "SIS_DocTypeFilter_";
        internal const String SIS_DATACACHE_DocTypeFilter_ALL = "SIS_DocTypeFilter_ALL";

        private static PIDDocTypeFilter InitData(DataRow row, DataTable tbFilterItems)
        {
            if (row == null) return null;

            PIDDocTypeFilter obj = new PIDDocTypeFilter();
            obj.DocTypeFilterID = ConvertUtility.ConvertToInt32(row["DocTypeFilterID"], 0);
            obj.DocTypeFilterName = ConvertUtility.ConvertNullToEmptyString(row["DocTypeFilterName"]);
            obj.DocTypeFilterOwnerRole = ConvertUtility.ConvertNullToEmptyString(row["DocTypeFilterOwnerRole"]);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            if (tbFilterItems != null)
            {
                obj.FilterItems = new List<PIDDocTypeFilterItem>();
                foreach (DataRow r in tbFilterItems.Rows)
                {
                    obj.FilterItems.Add(PIDDocTypeFilterItemBLL.InitData(r, "en"));
                }
            }
            return obj;
        }

        public static List<PIDDocTypeFilter> SelectAll_ListOnly()
        {
            String cacheKey = SIS_DATACACHE_DocTypeFilter_ALL;
            
            ConnectDataCache cache = new ConnectDataCache();
            List<PIDDocTypeFilter> list = cache.GetCachedItem(cacheKey) as List<PIDDocTypeFilter>;
            if (list == null)
            {
                DataTable tb = Data.DAPID.DAPID_DocTypeFilter.SelectAll();
                if (tb == null) return null;
                list = new List<PIDDocTypeFilter>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r, null));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static PIDDocTypeFilter SelectByFilterID(Int32 docTypeFilterID, Int32 preferredPIDLangID)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");

            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_DocTypeFilter_PREFIX, docTypeFilterID);
            ConnectDataCache cache = new ConnectDataCache();
            PIDDocTypeFilter obj = cache.GetCachedItem(cacheKey) as PIDDocTypeFilter;
            if (obj == null)
            {
                DataTable tbItems = null;
                DataRow row = Data.DAPID.DAPID_DocTypeFilter.SelectByDocTypeFilterID(docTypeFilterID, out tbItems);
                obj = InitData(row, tbItems);
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, obj, DateTimeOffset.Now.AddYears(1));
            }
            return obj;
        }

        public static Int32 Insert(String docTypeFilterName, String docTypeFilterOwnerRole, String createdBy)
        {
            if (docTypeFilterName.IsNullOrEmptyString()) throw new ArgumentNullException("docTypeFilterName");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            DataRow row = Data.DAPID.DAPID_DocTypeFilter.Insert(docTypeFilterName, docTypeFilterOwnerRole, createdBy);
            if (row == null) return 0;

            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_DocTypeFilter_ALL);

            return ConvertUtility.ConvertToInt32(row["DocTypeFilterID"], 0);
        }

        public static void Update(Int32 docTypeFilterID, String docTypeFilterName)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");
            if (docTypeFilterName.IsNullOrEmptyString()) throw new ArgumentNullException("docTypeFilterName");
            Data.DAPID.DAPID_DocTypeFilter.Update(docTypeFilterID, docTypeFilterName);
            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_DocTypeFilter_ALL);

            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_DocTypeFilter_PREFIX, docTypeFilterID);
            cache.RemoveCachedItem(cacheKey);
        }

        public static void Delete(Int32 docTypeFilterID)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");
            Data.DAPID.DAPID_DocTypeFilter.Delete(docTypeFilterID);
            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_DocTypeFilter_ALL);
        }
    }
}
