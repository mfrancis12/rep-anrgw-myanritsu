﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDDocTypeFilterItem
    {
        public Int32 DocTypeFilterItemID { get; set; }
        public Int32 DocTypeFilterID { get; set; }
        public Int32 SIS_DocTypeID { get; set; }
        public String SIS_DocTypeFullName { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
