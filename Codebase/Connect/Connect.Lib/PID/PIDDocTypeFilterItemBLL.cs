﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDDocTypeFilterItemBLL
    {
        internal static PIDDocTypeFilterItem InitData(DataRow row, String lang)
        {
            if (row == null) return null;

            PIDDocTypeFilterItem obj = new PIDDocTypeFilterItem();
            obj.DocTypeFilterItemID = ConvertUtility.ConvertToInt32(row["DocTypeFilterItemID"], 0);
            obj.DocTypeFilterID = ConvertUtility.ConvertToInt32(row["DocTypeFilterID"], 0);            
            obj.SIS_DocTypeID = ConvertUtility.ConvertToInt32(row["SIS_DocTypeID"], 0);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            obj.SIS_DocTypeFullName = PIDDocumentTypeBLL.SelectCategoryAndDocTypeNameByID(obj.SIS_DocTypeID, lang);
            return obj;
        }

        public static List<PIDDocTypeFilterItem> SelectByDocTypeFilterID(Int32 docTypeFilterID, String lang)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");
            DataTable tb = Data.DAPID.DAPID_DocTypeFilterItem.SelectByDocTypeFilterID(docTypeFilterID);
            if (tb == null) return null;
            List<PIDDocTypeFilterItem> list = new List<PIDDocTypeFilterItem>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitData(row, lang));
            }
            return list;
        }

        public static Int32 Insert(Int32 docTypeFilterID, Int32 sis_DocTypeID, String createdBy)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");
            if (sis_DocTypeID < 1) throw new ArgumentNullException("sis_DocTypeID");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");

            DataRow row = Data.DAPID.DAPID_DocTypeFilterItem.Insert(docTypeFilterID, sis_DocTypeID, createdBy);
            if (row == null) return 0;

            ClearDocTypeFilterItemCache(docTypeFilterID);

            return ConvertUtility.ConvertToInt32(row["DocTypeFilterItemID"], 0);
        }

        public static void DeleteByDocTypeFilterID(Int32 docTypeFilterID)
        {
            if (docTypeFilterID < 1) throw new ArgumentNullException("docTypeFilterID");

            Data.DAPID.DAPID_DocTypeFilterItem.DeleteByDocTypeFilterID(docTypeFilterID);

            ClearDocTypeFilterItemCache(docTypeFilterID);
        }

        public static void DeleteByDocTypeFilterItemID(Int32 docTypeFilterItemID)
        {
            if (docTypeFilterItemID < 1) throw new ArgumentNullException("docTypeFilterItemID");
            Int32 docTypeFilterID = Data.DAPID.DAPID_DocTypeFilterItem.DeleteByDocTypeFilterItemID(docTypeFilterItemID);
            ClearDocTypeFilterItemCache(docTypeFilterID);
        }

        private static void ClearDocTypeFilterItemCache(Int32 docTypeFilterID)
        {
            ConnectDataCache cache = new ConnectDataCache();
            String cacheKey = String.Format("{0}{1}", PIDDocTypeFilterBLL.SIS_DATACACHE_DocTypeFilter_PREFIX, docTypeFilterID);
            cache.RemoveCachedItem(cacheKey);
        }
    }
}
