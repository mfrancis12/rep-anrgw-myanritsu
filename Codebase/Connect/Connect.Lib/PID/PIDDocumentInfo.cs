﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDDocumentInfo
    {
        public Int32 DocumentID { get; set; }
        public Int32 PID { get; set; }
        public Int32 DocumentTypeID { get; set; }
        public String DocumentType { get; set; }
        public Int32 LanguageID { get; set; }
        public String Language { get; set; }
        public Int32 AccessTypeID { get; set; }
        public String AccessType { get; set; }
        public String CatName { get; set; }
        public String DocumentName { get; set; }
        public String FileName { get; set; }
        public Double FileSize { get; set; }
        public String LinkName { get; set; }
        public String FileLink { get; set; }
        public String Version { get; set; }
        public String Description { get; set; }
        public String FullDownloadURL { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        
    }
}
