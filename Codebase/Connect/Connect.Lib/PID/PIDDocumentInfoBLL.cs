﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.PID
{
    public static class PIDDocumentInfoBLL
    {
        private const String SIS_DATACACHE_DOCINFO_PREFIX = "SIS_DocInfo_";

        private static PIDDocumentInfo InitData(DataRow row)
        {
            if (row == null) return null;

            PIDDocumentInfo obj = new PIDDocumentInfo();
            obj.DocumentID = ConvertUtility.ConvertToInt32(row["DocumentID"], 0);
            obj.PID = ConvertUtility.ConvertToInt32(row["PID"], 0);
            obj.DocumentTypeID = ConvertUtility.ConvertToInt32(row["DocumentTypeID"], 0);
            obj.DocumentType = String.Empty;
            obj.LanguageID = ConvertUtility.ConvertToInt32(row["LanguageID"], 1);
            obj.AccessTypeID = ConvertUtility.ConvertToInt32(row["AccessType"], 0);
            obj.CatName = ConvertUtility.ConvertNullToEmptyString(row["CatName"]).Replace(" ", "_");

            obj.DocumentName = ConvertUtility.ConvertNullToEmptyString(row["DocumentName"]);
            obj.FileName = ConvertUtility.ConvertNullToEmptyString(row["FileName"]);
            obj.LinkName = ConvertUtility.ConvertNullToEmptyString(row["LinkName"]);
            obj.FileLink = ConvertUtility.ConvertNullToEmptyString(row["FileLink"]);
            obj.Version = ConvertUtility.ConvertNullToEmptyString(row["Version"]);
            obj.FileSize = ConvertUtility.ConvertToDouble(row["FileSize"], 0.00) / 1024.00;//KB to MB
            obj.ReleaseDate = ConvertUtility.ConvertToDateTime(row["ReleaseDate"], DateTime.MinValue);
            obj.ModifiedDate = ConvertUtility.ConvertToDateTime(row["ModifiedDate"], DateTime.MinValue);

            obj.FullDownloadURL = ConvertUtility.ConvertNullToEmptyString(row["DocUrl"]);
            //MA-1395
            //if (obj.FullDownloadURL.StartsWith("/"))
            //{
            //    obj.FullDownloadURL = PIDUtility.GenerateCDNURL(obj.FullDownloadURL, 24);
            //}
            return obj;
        }

        public static List<PIDDocumentInfo> SelectByPID(Int32 pid)
        {
            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_DOCINFO_PREFIX, pid);

            ConnectDataCache cache = new ConnectDataCache();
            List<PIDDocumentInfo> docs = cache.GetCachedItem(cacheKey) as List<PIDDocumentInfo>;
            if (docs == null)
            {
                docs = new List<PIDDocumentInfo>();
                DataTable tb = Data.DAPID.DAPID_DocumentInfo.SelectDocuments(pid);
                if (tb == null) return null;
                foreach (DataRow row in tb.Rows)
                {
                    docs.Add(InitData(row));
                }
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, docs, DateTimeOffset.Now.AddHours(1));
            }

            return docs;
        }

        public static List<PIDDocumentInfo> FilterByCategory(List<PIDDocumentInfo> docData, String categoryCode)
        {
            if (docData == null) return null;
            var query = from d in docData
                        where d.CatName.Equals(categoryCode, StringComparison.InvariantCultureIgnoreCase)
                        select d;
            return query.ToList();
        }
    }
}
