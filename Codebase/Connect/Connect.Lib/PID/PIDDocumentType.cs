﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDDocumentType
    {
        public Int32 DocumentTypeID { get; set; }
        public String CategoryName { get; set; }
        public String DocumentTypeName { get; set; }
        public String CategoryAndDocumentTypeName { get; set; }

       
    }
}
