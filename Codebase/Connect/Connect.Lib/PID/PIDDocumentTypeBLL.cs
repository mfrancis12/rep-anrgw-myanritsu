﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDDocumentTypeBLL
    {
        internal const String SIS_DATACACHE_DocType_PREFIX = "SIS_DocType_";
        internal const String SIS_DATACACHE_DocType_ALL = "SIS_DocType_ALL";

        private static DataTable PIDDocumentTypes
        {
            get
            {
                String cacheKey = SIS_DATACACHE_DocType_ALL;
                ConnectDataCache cache = new ConnectDataCache();
                DataTable tb = cache.GetCachedItem(cacheKey) as DataTable;
                if (tb == null)
                {
                    tb = RefreshData();
                }
                return tb;
            }
        }

        private static PIDDocumentType InitData(DataRow row, String llCC)
        {
            if (row == null) return null;
            if (llCC.IsNullOrEmptyString()) llCC = "en";

            PIDDocumentType obj = new PIDDocumentType();
            obj.DocumentTypeID = ConvertUtility.ConvertToInt32(row["DocumentTypeID"], 0);
            obj.CategoryName = ConvertUtility.ConvertNullToEmptyString(row["CatName"]).Replace(" ", "_");
            obj.DocumentTypeName = ConvertUtility.ConvertNullToEmptyString(row["EnglishText"]);
            if(llCC.StartsWith("ja", StringComparison.InvariantCultureIgnoreCase))
                obj.DocumentTypeName = ConvertUtility.ConvertNullToEmptyString(row["JapaneseText"]);
            else if (llCC.StartsWith("zh-cn", StringComparison.InvariantCultureIgnoreCase))
                obj.DocumentTypeName = ConvertUtility.ConvertNullToEmptyString(row["ChineseText"]);
            else if (llCC.StartsWith("zh-sg", StringComparison.InvariantCultureIgnoreCase))
                obj.DocumentTypeName = ConvertUtility.ConvertNullToEmptyString(row["ChineseText"]);
            obj.CategoryAndDocumentTypeName = String.Format("{0} - {1}", obj.CategoryName, obj.DocumentTypeName);
            return obj;
        }

        private static DataTable RefreshData()
        {
            String cacheKey = SIS_DATACACHE_DocType_ALL;
            ConnectDataCache cache = new ConnectDataCache();
            DataTable tb = Data.DAPID.DAPID_DocumentType.SelectAll();
            if (tb == null) cache.RemoveCachedItem(cacheKey);
            else
            {                
                // 4hr exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, tb, DateTimeOffset.Now.AddHours(4));
            }

            return tb;
        }

        public static List<PIDDocumentType> SelectAll(String lang)
        {
            DataTable tb = PIDDocumentTypes;
            if (tb == null) return null;
            List<PIDDocumentType> list = new List<PIDDocumentType>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitData(row, lang));
            }
            return list;
        }

        public static String SelectDocTypeNameByID(Int32 docTypeID, String lang)
        {
            DataTable tb = PIDDocumentTypes;
            if (tb == null) return String.Empty;
            DataRow[] rows = tb.Select(String.Format("DocumentTypeID = {0}", docTypeID));
            if (rows == null || rows.Length < 1) return String.Empty;

            PIDDocumentType pdt = InitData(rows[0], lang);
            if (pdt == null) return String.Empty;
            return pdt.DocumentTypeName;
        }

        public static String SelectCategoryAndDocTypeNameByID(Int32 docTypeID, String lang)
        {
            DataTable tb = PIDDocumentTypes;
            if (tb == null) return String.Empty;
            DataRow[] rows = tb.Select(String.Format("DocumentTypeID = {0}", docTypeID));
            if (rows == null || rows.Length < 1) return String.Empty;

            PIDDocumentType pdt = InitData(rows[0], lang);
            if (pdt == null) return String.Empty;
            return pdt.CategoryAndDocumentTypeName;
        }
    }
}
