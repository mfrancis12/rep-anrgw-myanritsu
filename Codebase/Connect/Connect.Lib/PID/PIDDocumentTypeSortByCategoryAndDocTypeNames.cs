﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    public class PIDDocumentTypeSortByCategoryAndDocTypeNames : IComparer<PIDDocumentType>
    {
        public int Compare(PIDDocumentType dt1, PIDDocumentType dt2)
        {
            return dt1.CategoryAndDocumentTypeName.CompareTo(dt2.CategoryAndDocumentTypeName);
        }
    }
}
