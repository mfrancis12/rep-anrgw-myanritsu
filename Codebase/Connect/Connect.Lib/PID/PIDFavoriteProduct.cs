﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDFavoriteProduct
    {
        public Int32 FavProdID { get; set; }
        public Int32 PID { get; set; }
        public Guid AccountID { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
