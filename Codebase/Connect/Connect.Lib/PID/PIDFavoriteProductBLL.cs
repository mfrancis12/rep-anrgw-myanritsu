﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDFavoriteProductBLL
    {
        private static PIDFavoriteProduct InitData(DataRow row)
        {
            if (row == null) return null;
            PIDFavoriteProduct obj = new PIDFavoriteProduct();
            obj.FavProdID = ConvertUtility.ConvertToInt32(row["FavProdID"], 0);
            obj.PID = ConvertUtility.ConvertToInt32(row["PID"], 0);
            obj.AccountID = ConvertUtility.ConvertToGuid(row["AccountID"], Guid.Empty);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<PIDFavoriteProduct> SelectByAccountID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            DataTable tb = Data.DAPID.DAPID_FavoriteProducts.SelectByAccountID(accountID);
            if (tb == null) return null;
            List<PIDFavoriteProduct> list = new List<PIDFavoriteProduct>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitData(row));
            }
            return list;
        }

        public static void Insert(Guid accountID, Int32 pid)
        {
            if (accountID.IsNullOrEmptyGuid() || pid < 1) return;
            Data.DAPID.DAPID_FavoriteProducts.Insert(accountID, pid);
        }

        public static void Delete(Guid accountID, Int32 pid)
        {
            if (pid < 1 || accountID.IsNullOrEmptyGuid()) return;
            Data.DAPID.DAPID_FavoriteProducts.Delete(accountID, pid);
           
        }
    }
}
