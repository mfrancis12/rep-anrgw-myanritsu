﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDProdFilter
    {
        public Int32 ProdFilterID { get; set; }
        public String FilterName { get; set; }
        public String FilterOwnerRole { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public List<PIDProdFilterItem> FilterItems { get; set; }
    }
}
