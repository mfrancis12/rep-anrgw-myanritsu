﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDProdFilterBLL
    {
        internal const String SIS_DATACACHE_PRODFILTER_PREFIX = "SIS_ProdFilter_";
        internal const String SIS_DATACACHE_PRODFILTER_ALL = "SIS_ProdFilter_ALL";

        private static PIDProdFilter InitData(DataRow row, DataTable tbFilterItems, Int32 preferredPIDLangID)
        {
            if (row == null) return null;

            PIDProdFilter obj = new PIDProdFilter();
            obj.ProdFilterID = ConvertUtility.ConvertToInt32(row["ProdFilterID"], 0);
            obj.FilterName = ConvertUtility.ConvertNullToEmptyString(row["FilterName"]);
            obj.FilterOwnerRole = ConvertUtility.ConvertNullToEmptyString(row["FilterOwnerRole"]);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(row["CreatedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            if (tbFilterItems != null)
            {
                obj.FilterItems = new List<PIDProdFilterItem>();
                foreach (DataRow r in tbFilterItems.Rows)
                {
                    obj.FilterItems.Add(PIDProdFilterItemBLL.InitData(r, preferredPIDLangID));
                }
            }
            return obj;
        }

        public static List<PIDProdFilter> SelectAll_ListOnly()
        {
            String cacheKey = SIS_DATACACHE_PRODFILTER_ALL;
            
            ConnectDataCache cache = new ConnectDataCache();
            List<PIDProdFilter> list = cache.GetCachedItem(cacheKey) as List<PIDProdFilter>;
            if (list == null)
            {
                DataTable tb = Data.DAPID.DAPID_ProdFilter.SelectAll();
                if (tb == null) return null;
                list = new List<PIDProdFilter>();
                foreach (DataRow r in tb.Rows)
                    list.Add(InitData(r, null, 1));
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, list, DateTimeOffset.Now.AddYears(1));
            }

            return list;
        }

        public static PIDProdFilter SelectByFilterID(Int32 prodFilterID, Int32 preferredPIDLangID)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");

            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_PRODFILTER_PREFIX, prodFilterID);
            ConnectDataCache cache = new ConnectDataCache();
            PIDProdFilter obj = cache.GetCachedItem(cacheKey) as PIDProdFilter;
            if (obj == null)
            {
                DataTable tbItems = null;
                DataRow row = Data.DAPID.DAPID_ProdFilter.SelectByProdFilterID(prodFilterID, out tbItems);
                obj = InitData(row, tbItems, preferredPIDLangID);
                // 1 year exp.  when admin add new list, we should clear the cache
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, obj, DateTimeOffset.Now.AddYears(1));
            }
            return obj;
        }

        public static Int32 Insert(String filterName, String filterOwnerRole, String createdBy)
        {
            if (filterName.IsNullOrEmptyString()) throw new ArgumentNullException("filterName");
            if (createdBy.IsNullOrEmptyString()) throw new ArgumentNullException("createdBy");
            DataRow row = Data.DAPID.DAPID_ProdFilter.Insert(filterName, filterOwnerRole, createdBy);
            if (row == null) return 0;

            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_PRODFILTER_ALL);

            return ConvertUtility.ConvertToInt32(row["ProdFilterID"], 0);
        }

        public static void Update(Int32 prodFilterID, String filterName)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");
            if (filterName.IsNullOrEmptyString()) throw new ArgumentNullException("filterName");
            Data.DAPID.DAPID_ProdFilter.Update(prodFilterID, filterName);
            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_PRODFILTER_ALL);

            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_PRODFILTER_PREFIX, prodFilterID);
            cache.RemoveCachedItem(cacheKey);
        }

        public static void Delete(Int32 prodFilterID)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");
            Data.DAPID.DAPID_ProdFilter.Delete(prodFilterID);
            ConnectDataCache cache = new ConnectDataCache();
            cache.RemoveCachedItem(SIS_DATACACHE_PRODFILTER_ALL);
        }
    }
}
