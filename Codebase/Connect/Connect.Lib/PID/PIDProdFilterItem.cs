﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDProdFilterItem
    {
        public Int32 ProdFilterItemID { get; set; }
        public Int32 ProdFilterID { get; set; }
        public Int32 SIS_PID { get; set; }
        public String SIS_ModelNumber { get; set; }
        public String SIS_ProductName { get; set; }
        public String AddedBy { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
