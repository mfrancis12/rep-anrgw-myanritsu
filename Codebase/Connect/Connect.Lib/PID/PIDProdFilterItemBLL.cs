﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDProdFilterItemBLL
    {
        internal static PIDProdFilterItem InitData(DataRow row, Int32 preferredPIDLangID)
        {
            if (row == null) return null;

            PIDProdFilterItem obj = new PIDProdFilterItem();
            obj.ProdFilterItemID = ConvertUtility.ConvertToInt32(row["ProdFilterItemID"], 0);
            obj.ProdFilterID = ConvertUtility.ConvertToInt32(row["ProdFilterID"], 0);            
            obj.SIS_PID = ConvertUtility.ConvertToInt32(row["SIS_PID"], 0);
            obj.AddedBy = ConvertUtility.ConvertNullToEmptyString(row["AddedBy"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(row["CreatedOnUTC"], DateTime.MinValue);
            obj.SIS_ModelNumber = "n/a";
            obj.SIS_ProductName = "n/a";
            PIDProductInfo sisProdInfo = PIDProductInfoBLL.SelectByPID(obj.SIS_PID, preferredPIDLangID);
            if (sisProdInfo != null)
            {
                obj.SIS_ModelNumber = sisProdInfo.ModelNumber;
                obj.SIS_ProductName = sisProdInfo.ProductName;
            }
            return obj;
        }

        public static List<PIDProdFilterItem> SelectByProdFilterID(Int32 prodFilterID, Int32 preferredPIDLangID)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");
            DataTable tb = Data.DAPID.DAPID_ProdFilterItem.SelectByProdFilterID(prodFilterID);
            if (tb == null) return null;
            List<PIDProdFilterItem> list = new List<PIDProdFilterItem>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitData(row, preferredPIDLangID));
            }
            return list;
        }

        public static Int32 Insert(Int32 prodFilterID, Int32 sis_PID, String addedBy)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");
            if (sis_PID < 1) throw new ArgumentNullException("sis_PID");
            if (addedBy.IsNullOrEmptyString()) throw new ArgumentNullException("addedBy");

            DataRow row = Data.DAPID.DAPID_ProdFilterItem.Insert(prodFilterID, sis_PID, addedBy);
            if (row == null) return 0;

            ClearProdFilterItemCache(prodFilterID);

            return ConvertUtility.ConvertToInt32(row["ProdFilterItemID"], 0);
        }

        public static void DeleteByProdFilterID(Int32 prodFilterID)
        {
            if (prodFilterID < 1) throw new ArgumentNullException("prodFilterID");

            Data.DAPID.DAPID_ProdFilterItem.DeleteByProdFilterID(prodFilterID);

            ClearProdFilterItemCache(prodFilterID);
        }

        public static void DeleteByProdFilterItemID(Int32 prodFilterItemID)
        {
            if (prodFilterItemID < 1) throw new ArgumentNullException("prodFilterItemID");
            Int32 prodFilterID = Data.DAPID.DAPID_ProdFilterItem.DeleteByProdFilterItemID(prodFilterItemID);
            ClearProdFilterItemCache(prodFilterID);
        }

        private static void ClearProdFilterItemCache(Int32 prodFilterID)
        {
            ConnectDataCache cache = new ConnectDataCache();
            String cacheKey = String.Format("{0}{1}", PIDProdFilterBLL.SIS_DATACACHE_PRODFILTER_PREFIX, prodFilterID);
            cache.RemoveCachedItem(cacheKey);
        }
    }
}
