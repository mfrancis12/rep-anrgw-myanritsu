﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDProductInfo
    {
        public Int32 PID { get; set; }
        public String ModelNumber { get; set; }
        public String ProductName { get; set; }
        public String ProductImageUrl { get; set; }
        public Int32 AccessType { get; set; }
        public Int32 StatusID { get; set; }
        public Int32 LanguageID { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Boolean IsObsolete { get; set; }
    }
}
