﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Runtime.Caching;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDProductInfoBLL
    {
        private const String SIS_DATACACHE_ALLPRODUCTS = "SIS_Products";

        private static List<PIDProductInfo> GetSISProducts(Boolean refreshData)
        {
            ConnectDataCache cache = new ConnectDataCache();
            List<PIDProductInfo> sisProducts = cache.GetCachedItem(SIS_DATACACHE_ALLPRODUCTS) as List<PIDProductInfo>;
            if (sisProducts == null || refreshData)
            {
                sisProducts = PIDProductInfoBLL.SelectAllFromDB();
                cache.AddToCacheWithAbsoluteExpiration(SIS_DATACACHE_ALLPRODUCTS, sisProducts, DateTimeOffset.Now.AddHours(PIDUtility.PID_RefreshIntervalHour));
            }
            return sisProducts;
        }       

        private static PIDProductInfo InitData(DataRow row)
        {
            if (row == null) return null;
            PIDProductInfo obj = new PIDProductInfo();
            obj.PID = ConvertUtility.ConvertToInt32(row["PID"], 0);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(row["ProductModelNumber"]);
            if (obj.ModelNumber.Length > 100) 
                obj.ModelNumber = obj.ModelNumber.Substring(0, 100) + "...";
            obj.ProductName = ConvertUtility.ConvertNullToEmptyString(row["ProductLocalName"]);
            if(obj.ProductName.IsNullOrEmptyString())
                obj.ProductName = ConvertUtility.ConvertNullToEmptyString(row["ProductName"]);
            
            obj.ProductImageUrl = String.Empty;
            String productImageRelUrl = ConvertUtility.ConvertNullToEmptyString(row["ProductImageRelUrl"]);
            if (!productImageRelUrl.IsNullOrEmptyString())
            {
                String signedUrl = PIDUtility.GenerateCDNURL(productImageRelUrl.Trim(), 24);
                obj.ProductImageUrl = signedUrl;
            }
            
            obj.ReleaseDate = ConvertUtility.ConvertToDateTime(row["ReleaseDate"], DateTime.MinValue);
            obj.AccessType = ConvertUtility.ConvertToInt32(row["AccessType"], 0);
            obj.StatusID = ConvertUtility.ConvertToInt32(row["StatusID"], 0);
            obj.LanguageID = ConvertUtility.ConvertToInt32(row["LanguageID"], 1);

            obj.ExpiryDate = ConvertUtility.ConvertToDateTime(row["ExpiryDate"], DateTime.MinValue);
            obj.IsObsolete = (obj.StatusID == 4 && obj.ExpiryDate <= DateTime.UtcNow);
            return obj;
        }

        private static List<PIDProductInfo> SelectAllFromDB()
        {

            DataTable tb = Data.DAPID.DAPID_ProductInfo.SelectAll(0);
            if (tb == null) return null;
            List<PIDProductInfo> list = new List<PIDProductInfo>();
            foreach (DataRow row in tb.Rows)
                list.Add(InitData(row));
            return list;
        }

        public static void RefreshData()
        {
            GetSISProducts(true);
        }

        public static List<PIDProductInfo> SearchSISProducts(Int32 preferredPIDLangID, String keyword)
        {
            if (!PIDUtility.PID_SupportedLanguageIDs.Contains(preferredPIDLangID)) preferredPIDLangID = 1;

            List<PIDProductInfo> sisProductList = GetSISProducts(false);
            
            var filteredList = (from p1 in sisProductList
                                where (
                                 keyword.IsNullOrEmptyString()
                                 || p1.ModelNumber.Contains(keyword, RegexOptions.IgnoreCase)
                                 || p1.ProductName.Contains(keyword, RegexOptions.IgnoreCase)
                                 || p1.PID.ToString().Equals(keyword, StringComparison.InvariantCultureIgnoreCase)
                                 )
                                select p1);

            return CleanUpFilteredList(filteredList.ToList(), preferredPIDLangID);
        }

        private static List<PIDProductInfo> CleanUpFilteredList(List<PIDProductInfo> filterdList, Int32 preferredPIDLangID)
        {
            if (filterdList == null || filterdList.Count() < 1) return null;

            var filteredUniquePIDs = (from p1 in filterdList
                                      select p1.PID).Distinct();

            List<PIDProductInfo> finalList = new List<PIDProductInfo>();
            foreach (Int32 pid in filteredUniquePIDs)
            {
                PIDProductInfo pi = FindProductInfo(filterdList.ToList(), pid, preferredPIDLangID);
                if (pi != null) finalList.Add(pi);
            }
            return finalList;
        }

        private static PIDProductInfo FindProductInfo(List<PIDProductInfo> listToSearchIn, Int32 pid, Int32 preferredPIDLangID)
        {
            if (listToSearchIn == null) return null;

            if (!PIDUtility.PID_SupportedLanguageIDs.Contains(preferredPIDLangID)) preferredPIDLangID = 1;
            var q1 = from p1 in listToSearchIn
                     where p1.PID == pid && p1.LanguageID == preferredPIDLangID
                     select p1;
            if (q1 == null || q1.Count() < 1)
            {
                //default english
                q1 = from p1 in listToSearchIn
                     where p1.PID == pid && p1.LanguageID == 1
                     select p1;
            }
            if (q1 == null || q1.Count() < 1) return null;

            return q1.First();
        }

        public static List<PIDProductInfo> GetFavoriteProducts(Int32 preferredPIDLangID, Guid accountID)
        {
            if (!PIDUtility.PID_SupportedLanguageIDs.Contains(preferredPIDLangID)) preferredPIDLangID = 1;
            List<PIDFavoriteProduct> favoriteProducts = Lib.PID.PIDFavoriteProductBLL.SelectByAccountID(accountID);
            if (favoriteProducts == null) return null;

            List<PIDProductInfo> sisProductList = GetSISProducts(false);
            
            var filteredList = from p1 in sisProductList
                        join fav1 in favoriteProducts on p1.PID equals fav1.PID
                        select p1;
            return CleanUpFilteredList(filteredList.ToList(), preferredPIDLangID);
        }

        public static PIDProductInfo SelectByPID(Int32 pid, Int32 preferredPIDLangID)
        {
            if (pid < 1) return null;
            if (!PIDUtility.PID_SupportedLanguageIDs.Contains(preferredPIDLangID)) preferredPIDLangID = 1;

            List<PIDProductInfo> sisProductList = GetSISProducts(false);
            return FindProductInfo(sisProductList, pid, preferredPIDLangID);
        }

       
    }
}
