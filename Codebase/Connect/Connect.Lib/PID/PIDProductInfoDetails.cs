﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    public class PIDProductInfoDetails
    {
        public List<PIDProductInfoText> TextInfo { get; set; }
    }
}
