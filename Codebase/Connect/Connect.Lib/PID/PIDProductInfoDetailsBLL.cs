﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDProductInfoDetailsBLL
    {
        private const String SIS_DATACACHE_PRODUCTINFO_PREFIX = "SIS_ProdInfo_";

        private static PIDProductInfoText PIDProductInfoText_InitData(DataRow row)
        {
            if (row == null) return null;
            PIDProductInfoText obj = new PIDProductInfoText();
            obj.TextID = ConvertUtility.ConvertToInt32(row["TextID"], 0);
            obj.TextTypeID = ConvertUtility.ConvertToInt32(row["TextTypeID"], 0);
            obj.TextFormatID = ConvertUtility.ConvertToInt32(row["TextFormatID"], 0);
            obj.LanguageID = ConvertUtility.ConvertToInt32(row["LanguageID"], 0);
            obj.TextString = ConvertUtility.ConvertNullToEmptyString(row["Text"]);
            return obj;
        }

        public static PIDProductInfoDetails SelectDetails(Int32 pid)
        {
            String cacheKey = String.Format("{0}{1}", SIS_DATACACHE_PRODUCTINFO_PREFIX, pid);

            ConnectDataCache cache = new ConnectDataCache();
            PIDProductInfoDetails details = cache.GetCachedItem(cacheKey) as PIDProductInfoDetails;
            if (details == null)
            {
                details = new PIDProductInfoDetails();
                details.TextInfo = SelectTexts(pid);
                cache.AddToCacheWithAbsoluteExpiration(cacheKey, details, DateTimeOffset.Now.AddHours(1));
            }            

            return details;
        }

        private static List<PIDProductInfoText> SelectTexts(Int32 pid)
        {
            DataTable tb = Data.DAPID.DAPID_ProductInfo.SelectTexts(pid);
            if (tb == null) return null;
            List<PIDProductInfoText> list = new List<PIDProductInfoText>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(PIDProductInfoText_InitData(row));
            }
            return list;
        }

        private static String SearchTextInfo(List<PIDProductInfoText> data, Int32 preferredSISLangID, Int32 textTypeID)
        {
            if (data == null || data.Count < 1) return String.Empty;
            if (!PIDUtility.PID_SupportedLanguageIDs.Contains(preferredSISLangID))
                preferredSISLangID = 1;

            var query = from t1 in data
                        where t1.TextTypeID == textTypeID && t1.LanguageID == preferredSISLangID
                        select t1;
            if (query == null || query.Count() < 1)
            {
                //english default
                query = from t1 in data
                        where t1.TextTypeID == textTypeID && t1.LanguageID == 1
                        select t1;
            }
            if (query == null || query.Count() < 1) return String.Empty;
            PIDProductInfoText textInfo = query.First();
            if (textInfo == null || textInfo.TextString.IsNullOrEmptyString()) return String.Empty;
            return textInfo.TextString.Trim();
        }

        public static String Text_GetDescription(List<PIDProductInfoText> data, Int32 preferredSISLangID)
        {
            return SearchTextInfo(data, preferredSISLangID, 1);
        }

        public static String Text_GetSpec(List<PIDProductInfoText> data, Int32 preferredSISLangID)
        {
            return SearchTextInfo(data, preferredSISLangID, 2);
        }
    }
}
