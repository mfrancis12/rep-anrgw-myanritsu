﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.PID
{
    [Serializable]
    public class PIDProductInfoText
    {
        public Int32 TextID { get; set; }
        public Int32 TextTypeID { get; set; }
        public Int32 LanguageID { get; set; }
        public Int32 TextFormatID { get; set; }
        public String TextString { get; set; }
    }
}
