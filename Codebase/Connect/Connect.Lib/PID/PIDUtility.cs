﻿using System;
using System.Collections.Generic;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.PID
{
    public static class PIDUtility
    {
        public const Int32 PID_RefreshIntervalHour = 4;
        

        public static Int32 PID_ConvertToSISLangID(String browserLang)
        {
            if (browserLang.IsNullOrEmptyString()) return 1;

            Int32 sisLangID = 1;//english

            browserLang = browserLang.ToLowerInvariant();
            if (browserLang.StartsWith("en"))
                sisLangID = 1;
            else if (browserLang.StartsWith("ja"))
                sisLangID = 2;
            else if (browserLang.StartsWith("fr"))
                sisLangID = 3;
            else if (browserLang.StartsWith("de"))
                sisLangID = 4;
            else if (browserLang.StartsWith("it"))
                sisLangID = 5;
            else if (browserLang.StartsWith("es"))
                sisLangID = 6;
            else if (browserLang.StartsWith("zh-cn") || browserLang.StartsWith("zh-sg"))
                sisLangID = 7;
            else if (browserLang.StartsWith("zh-tw") || browserLang.StartsWith("zh-hk"))
                sisLangID = 8;
            else if (browserLang.StartsWith("zh"))
                sisLangID = 7;
            else if (browserLang.StartsWith("ko"))
                sisLangID = 9;
            else
                sisLangID = 1;

            return sisLangID;
        }

        public static List<Int32> PID_SupportedLanguageIDs
        {
            get
            {
                //-1	Neutral
                //1	English
                //2	Japanese
                //3	French
                //4	German
                //5	Italian
                //6	Spanish
                //7	Chinese_simplified
                //8	Chinese_traditional
                //9	Korean
                return new List<Int32>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            }
        }

        public static String GenerateCDNURL(String filePath, Int32 durationHours)
        {
            String awsPreFix = AWS.AWSUtility_Settings.PID_Prefix_Get();
            String fileRelUrl = filePath.Replace(@"\", "/").ToLowerInvariant();
            if (fileRelUrl.StartsWith("/")) fileRelUrl = fileRelUrl.Substring(1);
            if (!awsPreFix.IsNullOrEmptyString()) fileRelUrl = awsPreFix + fileRelUrl;
            String cloudFrontURL = String.Format("{0}/{1}", AWS.AWSUtility_Settings.PID_BaseUrlHost_Get(), fileRelUrl).ToLowerInvariant();
            DateTime expireUTC = DateTime.UtcNow.AddHours(durationHours);

            //return AWS.AWSUtility_SignedURL.GetPreSignedURLWithPEMFile(cloudFrontURL, expireUTC, pemPath, keyPairID, false);
            return AWS.AWSUtility_CloudFrontSignedURL.GetPreSignedURLWithPEMKey(cloudFrontURL, expireUTC, AWS.AWSUtility_Settings.PID_PEM_Get(), AWS.AWSUtility_Settings.PID_KeyPairID_Get(), false);
            //return AWS.AWSUtility_CloudFront.GenerateCloudFrontPrivateURLCanned(cloudFrontURL, durationUnit, duration, pathToPolicyStatement, pathToPrivateKey, privateKeyID);
        }
    }
}
