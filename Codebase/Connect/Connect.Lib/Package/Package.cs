﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Anritsu.Connect.EntityFramework.ContextHelper;

namespace Anritsu.Connect.Lib.Package
{
    [Serializable]
    public class Package
    {
        public long PackageID { get; set; }
        public String PackageName { get; set; }
        public String PackageGroupName { get; set; }
        public String PackagePath { get; set; }
        public DataTable ModelNumbers { get; set; }
        public int SystemID { get; set; }
        public String SystemName { get; set; }
        public bool Enabled { get; set; }
        public String CreatedBy { get; set; }
        public String ModifiedBy { get; set; }
        public List<PackageFile> FileList { get; set; }
        public String PackageIntact { get; set; }
        public String ModelConfigType { get; set; }
        public String SupportTypeCode { get; set; }
        public long CopyFromPackageId { get; set; }

        public Package()
        {
        }

        public Package(long packageId, String packageName, String packagePath, int packagetypeId, bool isActive, String modelConfigtype, String supTypeCode, String packageGroupName, long copyFromPackageId = 0)
        {
            PackageID = packageId;
            PackageGroupName = packageGroupName;
            PackageName = packageName;
            PackagePath = packagePath;
            SystemID = packagetypeId;
            Enabled = isActive;
            SupportTypeCode = supTypeCode;
            ModelConfigType = modelConfigtype;
            CopyFromPackageId = copyFromPackageId;
        }

        public static class TauPackageTypes
        {
            public const String EnginerringPackage = "Engineering Package";
            public const String CommercialPackage = "Commercial Package";
            public const String LicensesPackage = "License Package";
        }
        public static class JpPackageTypes
        {
            public const String DownloadPackage = "Downloads";
            public const String LicensesPackage = "Licenses";
            public const String VersionMatchTablePackage = "Version Match Table";
        }
    }

}
