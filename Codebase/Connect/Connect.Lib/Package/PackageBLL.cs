﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Amazon.S3.Model;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.AWS;
using Data = Anritsu.Connect.Data;

namespace Anritsu.Connect.Lib.Package
{
    public static class PackageBLL
    {
        private static Package InitPackageData(DataSet dataSet)
        {
            if (dataSet == null || dataSet.Tables.Count == 0 || dataSet.Tables[0] == null) return null;
            DataTable dtPackage = dataSet.Tables[0];
            DataTable dtPackageModels = dataSet.Tables[1];
            if (dtPackage == null || dtPackage.Rows.Count == 0) return null;
            DataRow dataRow = dtPackage.Rows[0];
            Package objPackage = new Package();
            if (dataRow != null)
            {
                objPackage.PackageID = ConvertUtility.ConvertToInt64(dataRow["PackageID"], 0);
                objPackage.PackageName = ConvertUtility.ConvertNullToEmptyString(dataRow["PackageName"]);
                objPackage.PackageGroupName = ConvertUtility.ConvertNullToEmptyString(dataRow["PackageGroupName"]);
                objPackage.PackagePath = ConvertUtility.ConvertNullToEmptyString(dataRow["PackagePath"]);
                objPackage.SystemID = ConvertUtility.ConvertToInt32(dataRow["SystemID"], 0);
                objPackage.Enabled = ConvertUtility.ConvertToBoolean(dataRow["IsEnabled"], false);
                objPackage.ModelConfigType = ConvertUtility.ConvertNullToEmptyString(dataRow["ModelConfigType"]);
                objPackage.SupportTypeCode = ConvertUtility.ConvertNullToEmptyString(dataRow["SupportTypeCode"]);
                objPackage.CopyFromPackageId = ConvertUtility.ConvertToInt64(dataRow["CopyFromPackageId"], 0);
            }
            else
                return null;
            if (dtPackageModels != null && dtPackageModels.Rows.Count > 0)
            {
                objPackage.ModelNumbers = dtPackageModels;
            }
            return objPackage;
        }

        public static DataTable SelectPackageSystems()
        {
            return Data.DAPackages.SelectPackageSystem();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packagePath"></param>
        /// <param name="prefix"></param>
        /// <param name="type"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static List<S3Object> GetPackaeFileList(String prefix, String regionCfgKey, String type, String delimeter)
        {
            var awsclient = new AWSUtility_S3(type);
            return awsclient.GetS3Objects(type, regionCfgKey, prefix, delimeter);
        }

        public static long InsertPackageWithFiles(Package package, DataTable packageFiles, String createdBy, DataTable dtPackageModels, bool assignToAllUsers = false)
        {
            try
            {
                return Data.DAPackages.InsertPackageWithFiles(package.PackageName, package.PackagePath, package.SystemID, package.Enabled,
                    createdBy, packageFiles, dtPackageModels, package.ModelConfigType, package.SupportTypeCode, package.PackageGroupName, assignToAllUsers, package.CopyFromPackageId);
            }
            catch (Exception ex)
            {
                AppLog.LogError(ex, HttpContext.Current);
            }
            return 0;
        }

        public static long UpdatePackageWithFiles(Package package, DataTable packageFiles, String createdBy, DataTable dtPackageModels)
        {
            return Data.DAPackages.UpdatePackageWithFiles(package.PackageID, package.PackageName, package.PackagePath, package.SystemID,
                package.Enabled, createdBy, packageFiles, dtPackageModels, package.ModelConfigType, package.SupportTypeCode, package.PackageGroupName);
        }

        public static void UpdatePackage(Package package, String createdBy, DataTable dtPackageModels, String pckGrpName)
        {
            Data.DAPackages.UpdatePackage(package.PackageID, package.PackageName, package.PackagePath, package.SystemID, package.Enabled,
                createdBy, dtPackageModels, package.ModelConfigType, package.SupportTypeCode, pckGrpName);
        }

        public static Package SelectByPackageID(long packageID)
        {
            if (packageID == 0) return null;
            return InitPackageData(Data.DAPackages.SelectByPackageID(packageID));
        }

        public static bool InsertPackageAssignedOrg(long packageID, Guid accountID, String createdBy, DateTime? expDate)
        {
            return Data.DAPackages.InsertPackageAssignedOrg(packageID, accountID, createdBy, expDate);
        }
        public static DataTable SelectOrganizationsByPackage(long packageID)
        {
            return Data.DAPackages.SelectOrganizationsByPackage(packageID);
        }
        public static bool DeleteOrganizationPackageMapping(long packageID, Guid accountID)
        {
            return Data.DAPackages.DeleteOrganizationPackageMapping(packageID, accountID);
        }
        public static bool DeletePackage(long packageID)
        {
            return Data.DAPackages.DeletePackage(packageID);
        }
        public static DataSet SelectPackagesWithFiles(String modelConfigType = null)
        {
            return Data.DAPackages.SelectPackagesWithFiles(modelConfigType);
        }
        public static DataTable SelectAllActivePackages(String packageName, Guid accid)
        {
            return Data.DAPackages.SelectAllActivePackages(packageName, accid);
        }
        public static bool UpdatePackageFiles(long packageID, String modifiedBy, DataTable dtFiles)
        {
            return Data.DAPackages.UpdatePackageFiles(packageID, modifiedBy, dtFiles);
        }
        public static DataTable SelectAllEmailsByPackage(long packageID)
        {
            return Data.DAPackages.SelectAllEmailsByPackage(packageID);
        }
        public static DataTable SelectAllActivePackagesByAccountID(Guid accID)
        {
            return Data.DAPackages.SelectAllActivePackagesByAccountID(accID);
        }
        public static bool BulkUpdateOrgPackages(Guid accID, DataTable packageExpDateTable)
        {
            return Data.DAPackages.BulkUpdateOrgPackages(accID, packageExpDateTable);
        }
        public static bool ActivePackagesByModelNumberCount(string modelNumber, DataTable dtTeams)
        {
            return Data.DAPackages.SelectAllActivePackagesByModelNumber(modelNumber, dtTeams);
        }
        public static DataSet SelectPackagesWithFilesByAccountID(Guid accID)
        {
            return Data.DAPackages.SelectPackagesWithFilesByAccountID(accID);
        }
        public static DataTable SelectPackagesByAccountID(DataTable dtTeams, string modelNumner, bool isLicensePkg)
        {
            return Data.DAPackages.SelectPackagesByAccountID(dtTeams, modelNumner, isLicensePkg);
        }

        public static DataTable SelectPackageFilesByPackageID(long packageID)
        {
            return Data.DAPackages.SelectPackageFilesByPackageID(packageID);
        }

        public static bool BulkUpdateUserPackageAccess(DataTable dtEntries, Int64 packageId)
        {
            return Data.DAPackages.BulkUpdateUserPackageAccess(dtEntries, packageId);
        }

        #region JP Packages

        public static DataTable SelectJpPackageGroups(String modelNumber, String serialNumber, Guid membershipId, String packageType, String modelConfigType, String pkgGroupName, DataTable dtTeams)
        {
            var results = Data.DAPackages.SelectJpPackageGroups(modelNumber, serialNumber, membershipId, packageType, modelConfigType, pkgGroupName, dtTeams);
            return results.Columns.Contains("PackagePath") ? SortPackagesByDesc(results) : results;
        }

        public static DataTable SelectJpPackageSubFoldersByPackageId(long packageId)
        {
            return Data.DAPackages.SelectJpPackageSubFoldersByPackageId(packageId);
        }

        private static DataTable SortPackagesByDesc(DataTable results)
        {
            //return sorted packages 
            if (results == null || results.Rows.Count <= 0) return null;
            results.Columns.Add("PackageFolderName");
            foreach (DataRow dr in results.Rows)
            {
                dr["PackageFolderName"] = GetVirtualName(Convert.ToString(dr["PackagePath"]));
                results.AcceptChanges();
            }
            string sortExpression = string.Format("{0} {1}", "PackageFolderName", "desc");
            results.DefaultView.Sort = sortExpression;
            return results.DefaultView.ToTable();
        }

        private static String GetVirtualName(String pkgPath)
        {
            if (String.IsNullOrEmpty(pkgPath)) return String.Empty;
            if (pkgPath.LastIndexOf('/') > 0)
            {
                return pkgPath.Substring(pkgPath.LastIndexOf('/') + 1);
            }
            return String.Empty;
        }

        public static DataTable SelectJpPackageSubFolderFiles(long pkgId, String subFolderName, String pkgPath, String locale)
        {
            return Data.DAPackages.SelectJpPackageSubFolderFiles(pkgId, subFolderName, pkgPath, locale);
        }

        public static void SelectJpPackageInfo(String modelNumber, String serialNumber, Guid membershipId,
            String modelConfigType
            , out bool hasJpDownloadPackages, out bool hasJpLicensePacjages, out bool hasJpVersionPackages, DataTable dtTeams)
        {
            Data.DAPackages.SelectJpPackageInfo(modelNumber, serialNumber, membershipId, modelConfigType,
                out hasJpDownloadPackages, out hasJpLicensePacjages, out hasJpVersionPackages, dtTeams);
        }

        #endregion

        public static void CopyPackageComments(long packageId, long copyFromPackageId, bool copyAllComments = true)
        {
            Data.DAPackages.CopyPackageComments(packageId, copyFromPackageId, copyAllComments);
        }
        /// <summary>
        /// Get package files based on modelnumber and serial number, email address
        /// </summary>
        /// <param name="modelNumber"></param>
        /// <param name="serialNumber"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static DataTable GetAutoDownloadPackageFiles(String modelNumber, String serialNumber, string emailAddress,string modelconfig, DataTable dtTeams)
        {
            return Data.DAPackages.GetAutoDownloadPackageFiles(modelNumber, serialNumber, emailAddress,modelconfig, dtTeams);
        }

        public static bool ShowAutoDownloadInfoByModelNumber(String modelNumber)
        {
            bool showAutoDownload = false;
            DataTable dt = Data.DAPackages.SelectByModel(modelNumber, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["ShowAutoDownloadInfo"].ToString().ToUpper() == "TRUE")
                    {
                        showAutoDownload = true;
                    }
                }
            }

            return showAutoDownload;
        }
    }
}
