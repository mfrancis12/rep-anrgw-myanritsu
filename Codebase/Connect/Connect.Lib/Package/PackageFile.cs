﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Package
{
    public class PackageFile
    {
        public long FileID { get; set; }
        public long PackageID { get; set; }
        public String FilePath { get; set; }
        //File size in bytes
        public long FileSize { get; set; }
        //File last modified date in GMT/UTC
        public DateTime FileLastModified { get; set; }
        public String CreatedBy { get; set; }
    }
}
