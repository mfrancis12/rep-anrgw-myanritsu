﻿using System;

namespace Anritsu.Connect.Lib.Package
{
    public class PackageType
    {
        public const String Download = "Downloads";
        public const String Licence = "Licenses";
        public const String VersionMatching = "Version Matching Table";
    }
}
