﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib
{
    [Serializable]
    public enum ProdReg_ItemStatus
    {
        active, notapproved, processing, submitted, supportrenewsubmitted, waitingforreply
    }
}
