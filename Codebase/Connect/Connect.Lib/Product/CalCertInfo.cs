﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Product
{
    [Serializable]
    public class CalCertInfo
    {
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public String CalCertFileName { get; set; }
        public DateTime CalCertDate { get; set; }
        public DateTime CalCertDueDate { get; set; }
    }
}
