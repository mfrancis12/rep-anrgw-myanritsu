﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Product
{
    public static class CalCertInfoBLL
    {
        private static CalCertInfo InitFromData(DataRow r)
        {
            if (r == null) return null;
            CalCertInfo obj = new CalCertInfo();
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);
            obj.CalCertFileName = ConvertUtility.ConvertNullToEmptyString(r["Cal_Pdf"]) + ".pdf";
            obj.CalCertDate = ConvertUtility.ConvertToDateTime(r["Cal_Date"], DateTime.MinValue);
            obj.CalCertDueDate = ConvertUtility.ConvertToDateTime(r["Cal_Due_Date"], DateTime.MinValue);            
            return obj;
        }

        public static List<CalCertInfo> Search(String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DataTable tb = Data.DAGlobalWeb_CalCert.SearchCalCert(modelNumber, serialNumber);
            if (tb == null) return null;
            List<CalCertInfo> lst = new List<CalCertInfo>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static DataRow GetCalCertInfoForDownload(Int32 gwUserID, String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
         
            return Data.DAGlobalWeb_CalCert.GetCalibrationCerttificateFile(gwUserID.ToString(), modelNumber, serialNumber);

        }
    }
}
