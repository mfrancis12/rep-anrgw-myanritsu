﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Data;

namespace Anritsu.Connect.Lib.Product
{
    public static class DownloadsBLL
    {
        public static object GlobalWebDownloads_Select(String modelNumber, Int32 gwCultureGroupID)
        {
            return Data.DADownload_GlobalWeb.Select(modelNumber, gwCultureGroupID);
        }

        private static List<Downloads_PublicData> InitFromDataFromCMSData(DataTable dtModels)
        {
            if (dtModels == null || dtModels.Rows.Count < 1) return null;
            var modelList = new List<Downloads_PublicData>();
            foreach (DataRow model in dtModels.Rows)
            {
                var modeltemp = new Downloads_PublicData();
                modeltemp.ModelNumber = ConvertUtility.ConvertNullToEmptyString(model["ModelNumber"]);
                modeltemp.DownloadID = ConvertUtility.ConvertToInt32(model["DownloadID"],0);
                modeltemp.CategoryID = ConvertUtility.ConvertToInt32(model["CategoryID"], 0);
                modeltemp.CategoryType = ConvertUtility.ConvertNullToEmptyString(model["CategoryType"]);
                modeltemp.Title = ConvertUtility.ConvertNullToEmptyString(model["Title"]);
                modeltemp.FilePath = ConvertUtility.ConvertNullToEmptyString(model["FilePath"]);
                modeltemp.DownloadPath = ConvertUtility.ConvertNullToEmptyString(model["DownloadPath"]);
                modeltemp.Size = ConvertUtility.ConvertNullToEmptyString(model["Size"]);
                modeltemp.Version = ConvertUtility.ConvertNullToEmptyString(model["Version"]);
                modeltemp.ReleaseDate = ConvertUtility.ConvertToDateTime(model["ReleaseDate"], DateTime.MinValue);
                modeltemp.Updated = ConvertUtility.ConvertToDateTime(model["Updated"], DateTime.MinValue);
                modeltemp.Created = ConvertUtility.ConvertToDateTime(model["Created"],DateTime.MinValue);
                modeltemp.RedirectToMyAnritsu = ConvertUtility.ConvertNullToEmptyString(model["RedirectToMyAnritsu"]);
              

                modelList.Add(modeltemp);
            }
            return modelList;
        }
        public static List<Downloads_PublicData> GetDownloadsFromCMS(String modelNumber, Int32 gwCultureGroupID)
        {

            return InitFromDataFromCMSData(Data.DADownload.GetDownloadInfoFromCMSDB(modelNumber, gwCultureGroupID));
        }
        public static DataTable JPNonUSBDownloads_Select(String modelNumber, Int32 gwCultureGroupID, List<String> userEmails)
        {
            return Data.DADownload_JPNonUSB.Select(modelNumber, gwCultureGroupID, userEmails);
        }

        public static DataTable JPUSBDownloads_Select(String modelNumber, Int32 gwCultureGroupID, String userEmail, String usbNo)
        {
            return Data.DADownload_JPUSB.Select(modelNumber, gwCultureGroupID, userEmail, usbNo);
        }

        public static DataTable JPDownloads_VersionTable_Select(String modelNumber)
        {
            return Data.DADownload_JPVersionTable.GetVersionTable(modelNumber);
        }
    }
}
