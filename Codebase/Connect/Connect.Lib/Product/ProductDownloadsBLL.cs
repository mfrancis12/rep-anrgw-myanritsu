﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Product
{
   public static class ProductDownloadsBLL
    {
        ////public static DataTable SelectByModel(Lib.ProductRegistration.Acc_ProductRegistered regProduct, int gwCultureGroupID)
        ////{
        ////    if (regProduct == null || regProduct.ModelNumber.IsNullOrEmptyString()) return null;
        ////    Lib.Erp.Erp_ProductConfig_TBD productCfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(regProduct.ModelNumber);
        ////    if (productCfg == null) return null;

        ////    String primaryModelNumber = regProduct.ModelNumber.Trim();
        ////    List<String> models = new List<String>();
        ////    if(regProduct.IncludeModelPrimary) models.Add(primaryModelNumber);
        ////    #region " model tags "
        ////    if (regProduct.IncludeModelTags && productCfg.ModelTags != null)
        ////    {
        ////        foreach (string tmn in productCfg.ModelTags)
        ////        {
        ////            if (!tmn.Equals(primaryModelNumber, StringComparison.InvariantCultureIgnoreCase) && !models.Contains(tmn))
        ////                models.Add(tmn);
        ////        }
        ////    }
        ////    #endregion

        ////    #region " add on models "
        ////    if (regProduct.IncludeModelRegSpecific && regProduct.InternalModels != null)
        ////    {
        ////        foreach (Erp.ProdReg_IntModel imn in regProduct.InternalModels)
        ////        {
        ////            String intMN = imn.IntModelNumber.Trim();
        ////            if (!intMN.IsNullOrEmptyString() && !models.Contains(intMN))
        ////                models.Add(intMN);
        ////        }
        ////    }
        ////    #endregion

        ////    List<String> jpUserIDList = Lib.Account.Acc_AccountJPUserRefBLL.SelectByAccountIDAsList(regProduct.AccountID);
        ////    return Data.DADownload.SelectByModel(models, jpUserIDList, gwCultureGroupID, regProduct.IncludeDLSrcGW, regProduct.IncludeDLSrcJP);
        ////}

        ////public static DataTable SelectByModel2(Lib.ProductRegistration.Acc_ProductRegistered regProduct, 
        ////    Lib.Erp.Erp_ProductConfig_TBD productCfg,
        ////    int gwCultureGroupID)
        ////{
        ////    if (regProduct == null || regProduct.ModelNumber.IsNullOrEmptyString() || productCfg == null) return null;

        ////    List<String> models = GenerateModels(regProduct, productCfg);
        ////    if (models == null || models.Count < 1) return null;
        ////    List<String> jpUserIDList = Lib.Account.Acc_AccountJPUserRefBLL.SelectByAccountIDAsList(regProduct.AccountID);
        ////    DataTable tb = Data.DADownload.SelectByModel(models, jpUserIDList, gwCultureGroupID, regProduct.IncludeDLSrcGW, regProduct.IncludeDLSrcJP);
        ////    if (tb == null) return null;

        ////    return tb;
        ////}

        ////public static DataTable SelectJPMatchingTBLByModel(Lib.ProductRegistration.Acc_ProductRegistered regProduct,
        ////     Lib.Erp.Erp_ProductConfig_TBD productCfg)
        ////{
        ////    if (regProduct == null || productCfg == null) return null;
        ////    List<String> models = GenerateModels(regProduct, productCfg);
        ////    if (models == null || models.Count < 1) return null;
        ////    return Data.DADownload.GetJPVersionTable(models);
        ////}

        ////private static List<String> GenerateModels(Lib.ProductRegistration.Acc_ProductRegistered regProduct,
        ////     Lib.Erp.Erp_ProductConfig_TBD productCfg)
        ////{
        ////    List<String> models = new List<String>();
        ////    if (regProduct == null || productCfg == null) return models;

        ////    String primaryModelNumber = regProduct.ModelNumber.Trim();            
        ////    if (regProduct.IncludeModelPrimary) models.Add(primaryModelNumber);

        ////    #region " model tags "
        ////    if (regProduct.IncludeModelTags && productCfg.ModelTags != null)
        ////    {
        ////        foreach (string tmn in productCfg.ModelTags)
        ////        {
        ////            if (!tmn.Equals(primaryModelNumber, StringComparison.InvariantCultureIgnoreCase) && !models.Contains(tmn))
        ////                models.Add(tmn);
        ////        }
        ////    }
        ////    #endregion

        ////    #region " add on models "
        ////    if (regProduct.IncludeModelRegSpecific && regProduct.InternalModels != null)
        ////    {
        ////        foreach (Erp.ProdReg_IntModel imn in regProduct.InternalModels)
        ////        {
        ////            String intMN = imn.IntModelNumber.Trim();
        ////            if (!intMN.IsNullOrEmptyString() && !models.Contains(intMN))
        ////                models.Add(intMN);
        ////        }
        ////    }
        ////    #endregion

        ////    return models;
        ////}
    }
}
