﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.Connect.Lib.Product
{
    [Serializable]
    public class ProductFilterFunction
    {
        public String ModelFilter { get; set; }
        public String FunctionFilter { get; set; }
        public String OptionFilter { get; set; }
    }
}
