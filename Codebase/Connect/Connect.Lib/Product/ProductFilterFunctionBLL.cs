﻿using Anritsu.AnrCommon.CoreLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anritsu.Connect.Lib.Product
{
    public class ProductFilterFunctionBLL
    {
        private static ProductFilterFunction InitFromData(DataRow r)
        {
            if (r == null) return null;
            var obj = new ProductFilterFunction();
            obj.ModelFilter = ConvertUtility.ConvertToString(r["ModelFilter"], String.Empty);
            obj.FunctionFilter = ConvertUtility.ConvertToString(r["FunctionFilter"], String.Empty);
            obj.OptionFilter = ConvertUtility.ConvertToString(r["OptionFilter"], String.Empty);
            return obj;
        }

        public static List<ProductFilterFunction> SelectByModelConfigID(int modelConfigID)
        {
            DataTable tb = Data.DAProductFilterFunction.SelectByModelConfigID(modelConfigID);
            if (tb == null) return null;

            var lst = new List<ProductFilterFunction>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static List<ProductFilterFunction> SelectByFilterKeywords(int modelConfigID, string model, string function, string option)
        {
            if (model.IsNullOrEmptyString()) return null;

            DataTable tb = Data.DAProductFilterFunction.SelectByFilterKeywords(modelConfigID, model, function, option);
            if (tb == null) return null;

            var lst = new List<ProductFilterFunction>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static bool Insert(int modelConfigID, string model, string function, string option, string createdBy)
        {
            if (model.IsNullOrEmptyString()) return false;
            if (createdBy.IsNullOrEmptyString()) return false;

            bool result = true;
            result = Data.DAProductFilterFunction.Insert(modelConfigID, model, function, option, createdBy);

            return result;
        }

        public static void Delete(int modelConfigID, string model, string function, string option)
        {
            Data.DAProductFilterFunction.Delete(modelConfigID, model, function, option);
            return;
        }


        public static bool InsertToAdminLog(int modelConfigID, string model, string function, string option, string createdBy)
        {
            if (model.IsNullOrEmptyString()) return false;
            if (createdBy.IsNullOrEmptyString()) return false;

            bool result = true;
            result = Data.DAAdminProductFilterFunction.Insert(modelConfigID, model, function, option, createdBy);

            return result;
        }

        public static void UpdateDeleteStatusForAdminLog(int modelConfigID, string model, string function, string option, string modifiedBy)
        {
            Data.DAAdminProductFilterFunction.Delete(modelConfigID, model, function, option, modifiedBy);
            return;
        }
    }
}
