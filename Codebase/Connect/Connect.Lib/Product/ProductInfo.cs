﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Product
{
    [Serializable]
    public class ProductInfo
    {
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public String ProductName { get; set; }
        public String ProductImageURL { get; set; }
        public String ProductSmallImageURL { get; set; }
        public String ProductPageURL { get; set; }
        public String DownloadPath { get; set; }
        public String PageURL { get; set; }

        // public List<WarrantyInfo> WarrantyCollection { get; set; }
        public static String ProductDefaultImageUrl = string.Format("{0}/images/legacy-images/images/gwstyle/NoImage.png", ConfigurationManager.AppSettings["GwdataCdnPath"].ToString());
    }

    [Serializable]
    public class ErpProductInfo
    {
        public string ModelNumber { get; set; }
        public string Description { get; set; }
        public int ErpSourceId { get; set; }
    }
}
