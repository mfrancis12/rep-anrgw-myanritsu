﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using System.Configuration;
using ApiSdk;
using System.Net;
using System.Web.Script.Serialization;
using Anritsu.Connect.Data;
using System.Web;

namespace Anritsu.Connect.Lib.Product
{
    public static class ProductInfoBLL
    {
        private static ProductInfo InitFromData(DataRow r)
        {
            if (r == null) return null;
            ProductInfo obj = new ProductInfo();
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);
            obj.ProductName = ConvertUtility.ConvertNullToEmptyString(r["ProductName"]);
            obj.ProductImageURL = ConvertUtility.ConvertNullToEmptyString(r["ProductImage"]);
            String imageCdnHost = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images";

            if (!obj.ProductImageURL.StartsWith("http://"))
            {
                obj.ProductImageURL = String.Format("https://{0}{1}{2}"
                       , imageCdnHost
                       , obj.ProductImageURL.StartsWith("/") ? String.Empty : "/"
                       , obj.ProductImageURL);
            }
            obj.ProductSmallImageURL = ConvertUtility.ConvertNullToEmptyString(r["ProductSmallImage"]);
            if (!obj.ProductSmallImageURL.StartsWith("http://"))
            {
                obj.ProductSmallImageURL = String.Format("https://{0}{1}{2}"
                       , imageCdnHost
                       , obj.ProductSmallImageURL.StartsWith("/") ? String.Empty : "/"
                       , obj.ProductSmallImageURL);
            }
            obj.ProductPageURL = ConvertUtility.ConvertNullToEmptyString(r["ProductPageURL"]);
            if (!obj.ProductPageURL.StartsWith("http://"))
            {
                obj.ProductPageURL = String.Format("http://{0}{1}{2}"
                       , ConfigUtility.AppSettingGetValue("Connect.GlobalWebHostName")
                       , obj.ProductPageURL.StartsWith("/") ? String.Empty : "/"
                       , obj.ProductPageURL);
            }
            return obj;
        }

        public static ProductInfo SelectByModelNumber(String modelNumber, String serialNumber, Int32 gwCultureGroupID)
        {
            try {

               List<ProductInfo> lstProductInfo= GetProductInfo_FromCMSDB(gwCultureGroupID.ToString(), modelNumber);
               if (lstProductInfo != null)
                {
                    lstProductInfo[0].DownloadPath = lstProductInfo[0].DownloadPath ?? string.Empty;
                    lstProductInfo[0].SerialNumber = serialNumber;
                    lstProductInfo[0].ProductPageURL = lstProductInfo[0].PageURL;
                }
                else
                {
                    lstProductInfo[0] = new ProductInfo();
                    lstProductInfo[0].SerialNumber = serialNumber;
                }
                return lstProductInfo[0];
                //// return InitFromData(Data.DAProductInfo.SelectByModelNumberInGWDB(modelNumber.Trim(), serialNumber.Trim(), gwCultureGroupID));
                //var apiUri =
                //      new Uri(string.Format("{0}{1}", ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.DownloadsUrl"], "GetProductsByModelNumber"));//?cultureGroupId=" + gwCultureGroupId.ToString() + "&modelNumber=MT8220T"));
                ////get target db 
                //var targetDb = ConfigUtility.AppSettingGetValue("Connect.Web.GWSiteCoreAPI.TargetDB");
                //if (string.IsNullOrEmpty(targetDb)) targetDb = "web";
                //var apiParams = new Dictionary<string, string>
                //{
                //    {"cultureGroupId", gwCultureGroupID.ToString()},
                //    {"modelNumber", modelNumber},
                //    {"targetDB", targetDb}
                //};
                //var rsClass = new ResponseClass();
                //try
                //{

                //    var response = rsClass.PostMethodList<string>(apiUri, apiParams);
                //    if (response.StatusCode == HttpStatusCode.OK)
                //    {
                //        var parseResponse = Newtonsoft.Json.Linq.JObject.Parse(response.Content.ReadAsStringAsync().Result);
                //        var serialize = new JavaScriptSerializer();
                //        var prInfo = serialize.Deserialize<List<ProductInfo>>(parseResponse["ResponseContent"].ToString());
                //        if (prInfo != null)
                //        {
                //            prInfo[0].DownloadPath = prInfo[0].DownloadPath ?? string.Empty;
                //            prInfo[0].SerialNumber = serialNumber;
                //            prInfo[0].ProductPageURL = prInfo[0].PageURL;
                //        }
                //        else
                //        {
                //            prInfo[0] = new ProductInfo();
                //            prInfo[0].SerialNumber = serialNumber;
                //        }
                //        return prInfo[0];
                //    }
                //   return null;


            }
            catch (Exception ex)
            {
                // ErrorLogUtility.LogError("CONNECT", HttpContext.Current.Request.Url.Host, ex, HttpContext.Current);
                return null;
            }
        }

        private static ProductInfo InitFromDataFromERP(DataRow r)
        {
            if (r == null) return null;
            ProductInfo obj = new ProductInfo();
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);
            obj.ProductName = ConvertUtility.ConvertNullToEmptyString(r["Description"]);
            obj.ProductImageURL = ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine");
            obj.ProductSmallImageURL = ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine");
            //obj.WarrantyCollection = WarrantyInfoBLL.SelectByModelSerialNumber(obj.ModelNumber.Trim(), obj.SerialNumber.Trim());
            return obj;
        }

        private static List<ErpProductInfo> InitFromDataFromERP(DataTable dtModels)
        {
            if (dtModels == null || dtModels.Rows.Count < 1) return null;
            var modelList = new List<ErpProductInfo>();
            foreach (DataRow model in dtModels.Rows)
            {
                var modeltemp = new ErpProductInfo();
                modeltemp.ModelNumber = ConvertUtility.ConvertNullToEmptyString(model["ModelNumber"]);
                modeltemp.Description = ConvertUtility.ConvertNullToEmptyString(model["Description"]);
                modeltemp.ErpSourceId = ConvertUtility.ConvertToInt32(model["ERPSourceId"], 0);
                modelList.Add(modeltemp);
            }
            return modelList;
        }
        private static List<ProductInfo> InitFromDataFromCMSData(DataTable dtModels)
        {
            if (dtModels == null || dtModels.Rows.Count < 1) return null;
            var modelList = new List<ProductInfo>();
            foreach (DataRow model in dtModels.Rows)
            {
                var modeltemp = new ProductInfo();
                modeltemp.ModelNumber = ConvertUtility.ConvertNullToEmptyString(model["ModelNumber"]);
                modeltemp.ProductName = ConvertUtility.ConvertNullToEmptyString(model["ProductName"]);
                modeltemp.ProductImageURL = ConvertUtility.ConvertNullToEmptyString(model["ProductImageURL"]);
                modeltemp.ProductSmallImageURL = ConvertUtility.ConvertNullToEmptyString(model["ProductSmallImageURL"]);
                modeltemp.ProductPageURL = ConvertUtility.ConvertNullToEmptyString(model["ProductPageURL"]);
                modeltemp.PageURL = ConvertUtility.ConvertNullToEmptyString(model["ProductPageURL"]);
                modelList.Add(modeltemp);
            }
            return modelList;
        }
        public static ProductInfo SelectByModelNumberInErpDB(String modelNumber, String serialNumber, String erpSource = "US")
        {
            return InitFromDataFromERP(Data.DAProductInfo.SelectByModelNumberInErpDB(modelNumber.Trim(), serialNumber.Trim(), erpSource));
        }

        public static DataTable FindInGlobalSerialNumberSystemDB(String modelNumber, String serialNumber)
        {
            //return Data.DAProductInfo.SelectInfoInGlobalSerialNumberSystemErpDB(modelNumber, serialNumber);
            return Data.DAErpData.GSNDB_SelectByModelSerial(modelNumber, serialNumber);
        }

        public static Boolean IsInGlobalSerialNumberSystemDB(String modelNumber, String serialNumber)
        {
            DataTable tb = Data.DAErpData.GSNDB_SelectByModelSerial(modelNumber, serialNumber);
            //Data.DAProductInfo.SelectInfoInGlobalSerialNumberSystemErpDB(modelNumber, serialNumber);
            return (tb != null && tb.Rows.Count > 0);
        }

        public static ProductInfo GetProductInfo(Int32 cultureGroupId, Cfg.ProductConfig pcfg, String serialNumber)
        {
            if (pcfg == null) return null;
            String mn = pcfg.ModelNumber.Trim();
            String sn = serialNumber.ConvertNullToEmptyString().Trim();

            Lib.Product.ProductInfo pBasicInfoForCustomer = Lib.Product.ProductInfoBLL.SelectByModelNumber(mn
                , sn
                , cultureGroupId);

            if (pBasicInfoForCustomer == null)
            {
                pBasicInfoForCustomer = Lib.Product.ProductInfoBLL.SelectByModelNumber(mn
                , sn
                , 1);//get it from en-US
            }

            List<String> modelTags = pcfg.ModelTags;
            if (pBasicInfoForCustomer == null && modelTags != null) // pull using model tags
            {
                foreach (String mntag in modelTags)
                {
                    pBasicInfoForCustomer = Lib.Product.ProductInfoBLL.SelectByModelNumber(mntag
                        , sn
                        , cultureGroupId);
                    if (pBasicInfoForCustomer != null) break;
                }
            }
            if (pBasicInfoForCustomer == null)
                pBasicInfoForCustomer = ProductInfoBLL.SelectByModelNumberInErpDB(mn, sn);

            return pBasicInfoForCustomer;
        }
        public static List<ProductInfo> GetProductInfo_FromCMSDB(String cultureGroupId, String modelNumbers)
        {
            List<ProductInfo> prInfo = new List<ProductInfo>();
           

            prInfo = InitFromDataFromCMSData(Data.DAProductInfo.GetProductInfoFromCMSDB(cultureGroupId,modelNumbers));
            return prInfo;
        }
            public static List<ProductInfo> GetProductInfo_Api(String cultureGroupId, String modelNumbers)
        {
            var apiUri =
              new Uri(string.Format("{0}{1}", ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.DownloadsUrl"], "GetProductsByModelNumber"));

            var targetDb = ConfigUtility.AppSettingGetValue("Connect.Web.GWSiteCoreAPI.TargetDB");
            if (string.IsNullOrEmpty(targetDb)) targetDb = "web";

            var postData = new Dictionary<string, string>();
            postData.Add("cultureGroupId", cultureGroupId);
            postData.Add("modelNumber", modelNumbers);
            postData.Add("targetDB", targetDb);
            var req = new ResponseClass();

            var q = req.PostMethodList<String>(apiUri, postData);
            var parseResponse = Newtonsoft.Json.Linq.JObject.Parse(q.Content.ReadAsStringAsync().Result);
            var serialize = new JavaScriptSerializer();
            var prInfo = serialize.Deserialize<List<ProductInfo>>(parseResponse["ResponseContent"].ToString());
            return prInfo;
        }
        public static List<ErpProductInfo> SelectAllModelNumberInErpDB()
        {
            return InitFromDataFromERP(Data.DAProductInfo.SelectAllModelNumberInErpDB());
        }
        public static ProductInfo SelectByModelNumberInErpDBCache(String modelNumber, List<ErpProductInfo> modelList, int erpSource = 15)
        {
            if (modelList == null) return null;
            var model =
                modelList
                    .FirstOrDefault(m => m.ModelNumber.Equals(modelNumber) && m.ErpSourceId == erpSource);
            //set PdouctInfo object
            var prodinfo = new ProductInfo();
            if (model == null) return null;
            prodinfo.ModelNumber = model.ModelNumber;
            prodinfo.ProductName = model.Description;
            return prodinfo;
        }
    }

}
