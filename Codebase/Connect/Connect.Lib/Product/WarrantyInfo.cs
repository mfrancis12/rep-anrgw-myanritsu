﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Product
{
    [Serializable]
    public class WarrantyInfo
    {
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public String EndUserCountryCode { get; set; }
        public String WarrantyDesc { get; set; }
        public String InternalCode { get; set; }
        public String TypeCode { get; set; }
        public DateTime WarrantyExpDate { get; set; }
        public Int32 ItemReference { get; set; }
    }
}
