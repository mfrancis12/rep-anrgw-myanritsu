﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Product
{
    public static class WarrantyInfoBLL
    {
        private static WarrantyInfo InitFromData(DataRow r)
        {
            if (r == null) return null;
            WarrantyInfo obj = new WarrantyInfo();
            obj.EndUserCountryCode = ConvertUtility.ConvertNullToEmptyString(r["EndUserCountryCode"]);
            obj.InternalCode = ConvertUtility.ConvertNullToEmptyString(r["InternalCode"]);
            obj.TypeCode = ConvertUtility.ConvertNullToEmptyString(r["TypeCode"]);
            obj.WarrantyDesc = ConvertUtility.ConvertNullToEmptyString(r["WarrantyDesc"]);
            if (obj.WarrantyDesc.Equals("JP_GENERIC_WTY", StringComparison.InvariantCultureIgnoreCase)) obj.WarrantyDesc = "Warranty";
            obj.WarrantyExpDate = ConvertUtility.ConvertToDateTime(r["WarrantyExpDate"], DateTime.MinValue);
            obj.ItemReference = ConvertUtility.ConvertToInt32(r["ItemReference"], 1);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);
            return obj;
        }

        public static List<WarrantyInfo> SelectByModelSerialNumber(String modelNumber, String serialNumber)
        {
            if (modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString()) return null;
            DataTable tb = Data.DAProductInfo.SelectWarrantyInfoByModelSerialNumberInErpDB(modelNumber, serialNumber);
            if (tb == null) return null;
            List<WarrantyInfo> lst = new List<WarrantyInfo>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }

        public static List<WarrantyInfo> WarrantyInfo_SelectByProdRegWebToken(DataTable dtTeams, string modelNumber, string serialNumber, out Boolean showWarrantyInfo)
        {
            DataTable tb = Data.DAProductInfo.WarrantyInfo_SelectByProdRegWebToken(dtTeams, modelNumber, serialNumber, out showWarrantyInfo);
            if (tb == null) return null;
            List<WarrantyInfo> lst = new List<WarrantyInfo>();
            foreach (DataRow r in tb.Rows)
            {
                lst.Add(InitFromData(r));
            }
            return lst;
        }
    }
}
