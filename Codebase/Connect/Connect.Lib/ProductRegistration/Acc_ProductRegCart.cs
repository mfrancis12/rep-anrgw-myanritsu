﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class Acc_ProductRegCart
    {
        public Int32 ProdRegCartID { get; set; }
        public Guid CartWebAccessKey { get; set; }
        public Guid AccountID { get; set; }
        public String LatestUserEmail { get; set; }
        public List<Acc_ProductRegCartItem> CartItems { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public String ModelNumber { get; set; }
        //{
        //    get
        //    {
        //        if (CartItems == null) return String.Empty;
        //        //you should take out this property when you support multiple models per registration
        //        var models = (from m in CartItems
        //                      select m.ModelNumber).Distinct();
        //        if (models != null && models.Count() < 1) return models.First();
        //        return String.Empty;
        //    }
        //}

        public Boolean IsAddOnFormCompletedForAllItems
        {
            get
            {
                if (CartItems == null) return false;
                var items = (from m in CartItems
                              where m.IsAddOnFormCompleted == false
                              select m);
                return (items == null || items.Count() < 1);
            }
        }

        public List<Acc_ProductRegCartItem> CartItemsToCompleteAddOnForm 
        {
            get
            {
                if (CartItems == null) return null;
                return (from m in CartItems
                             where m.IsAddOnFormCompleted == false
                             select m).ToList();
            }
        }

        public Acc_ProductRegCartItem NextItemForAddOnForm
        {
            get
            {
                if (CartItems == null) return null;
                List<Acc_ProductRegCartItem> lst = CartItemsToCompleteAddOnForm;
                if (lst == null || lst.Count < 1) return null;
                return lst[0];
            }
        }

        public String SerialNumberDelimitedString
        {
            get
            {
                if (CartItems == null || CartItems.Count < 1) return String.Empty;
                StringBuilder sb = new StringBuilder();
                foreach (Acc_ProductRegCartItem item in CartItems)
                    sb.AppendFormat("{0},", item.SerialNumber.Trim());
                String snList = sb.ToString();
                if (snList.EndsWith(",")) snList = snList.Remove(snList.Length - 1, 1);
                return snList;
            }
        }
        //public Boolean IsAddOnFormCompletedForAllItemsExceptOne
        //{
        //    get
        //    {
        //        if (Cartitems == null) return false;
        //        var items = (from m in Cartitems
        //                     where m.IsAddOnFormCompleted == false
        //                     select m);
        //        return (items != null && items.Count() == 1);
        //    }
        //}
    }
}
