﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class Acc_ProductRegCartBLL
    {
        internal static Acc_ProductRegCart InitFromData(DataRow r, Dictionary<DataRow, DataTable> itemData)
        {
            if (r == null) return null;
            Acc_ProductRegCart obj = new Acc_ProductRegCart();
            obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            obj.CartWebAccessKey = ConvertUtility.ConvertToGuid(r["CartWebAccessKey"], Guid.Empty);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.ProdRegCartID = ConvertUtility.ConvertToInt32(r["ProdRegCartID"], 0);
            obj.LatestUserEmail = ConvertUtility.ConvertNullToEmptyString(r["LatestUserEmail"]);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            if (itemData != null)
            {
                obj.CartItems = new List<Acc_ProductRegCartItem>();
                foreach (DataRow rItem in itemData.Keys)
                {
                    Acc_ProductRegCartItem item = Acc_ProductRegCartItemBLL.InitFromData(rItem, itemData[rItem]);
                    if (item != null) obj.CartItems.Add(item);
                }
            }
            return obj;
        }

        public static Acc_ProductRegCart SelectByWebAccessKey(Guid webAccessKey)
        {
            if (webAccessKey.IsNullOrEmptyGuid()) return null;
            Dictionary<DataRow, DataTable> itemData = null;
            DataRow r = Data.DAAcc_ProductRegCart.SelectByWebAccessKey(webAccessKey, out itemData);
            if (r == null) return null;
            return InitFromData(r, itemData);
        }

        public static DataTable SelectByAccountID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            return Data.DAAcc_ProductRegCart.SelectByAccountID(accountID);
        }

        public static Guid CreateNewCart(Guid accountID, String latestUserEmail, String modelNumber)
        {
            DataRow r = Data.DAAcc_ProductRegCart.Insert(accountID, latestUserEmail, modelNumber.Trim());
            if (r == null) return Guid.Empty;
            return ConvertUtility.ConvertToGuid(r["CartWebAccessKey"], Guid.Empty);
        }

        public static void UpdateUser(Guid cartWebAccessKey, String latestUserEmail)
        {
            Data.DAAcc_ProductRegCart.Update(cartWebAccessKey, latestUserEmail);
        }

        public static DataTable GetItemTable(Acc_ProductRegCart prc)
        {
            if (prc == null) return null;
            DataTable tb = new DataTable();

            DataColumn col = new DataColumn("CartItemID");
            col.DataType = typeof(System.Int32);
            col.Unique = true;
            tb.Columns.Add(col);

            col = new DataColumn("ProdRegCartID");
            col.DataType = typeof(System.Int32);
            tb.Columns.Add(col);

            col = new DataColumn("ModelNumber");
            col.DataType = typeof(System.String);
            col.DefaultValue = "-";
            tb.Columns.Add(col);

            col = new DataColumn("SerialNumber");
            col.DataType = typeof(System.String);
            col.DefaultValue = "-";
            tb.Columns.Add(col);

            tb.AcceptChanges();

            if (prc.CartItems != null)
            {
                foreach (Acc_ProductRegCartItem item in prc.CartItems)
                {
                    DataRow r = tb.NewRow();
                    r["CartItemID"] = item.CartItemID;
                    r["ProdRegCartID"] = item.ProdRegCartID;
                    r["ModelNumber"] = item.ModelNumber;
                    r["SerialNumber"] = item.SerialNumber;
                    tb.Rows.Add(r);
                }
                tb.AcceptChanges();
            }

            return tb;
        }

        public static void DeleteCart(Guid cartWebAccessKey)
        {
            if(cartWebAccessKey.IsNullOrEmptyGuid()) return;
            Data.DAAcc_ProductRegCart.Delete(cartWebAccessKey);
        }

       
    }
}
