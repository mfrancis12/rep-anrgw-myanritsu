﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class Acc_ProductRegCartItem
    {
        public Int32 CartItemID { get; set; }
        public Int32 ProdRegCartID { get; set; }
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public Int32 SerialCheckStatus { get; set; }
        public Boolean IsAddOnFormCompleted { get; set; }
        public List<Acc_ProductRegCartItemData> ItemData { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
