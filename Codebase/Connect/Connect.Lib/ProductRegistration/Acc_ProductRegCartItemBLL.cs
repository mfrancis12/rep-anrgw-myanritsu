﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class Acc_ProductRegCartItemBLL
    {
        internal static Acc_ProductRegCartItem InitFromData(DataRow r, DataTable tbData)
        {
            if (r == null) return null;
            Acc_ProductRegCartItem obj = new Acc_ProductRegCartItem();
            obj.CartItemID = ConvertUtility.ConvertToInt32(r["CartItemID"], 0);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(r["ModelNumber"]);
            obj.ProdRegCartID = ConvertUtility.ConvertToInt32(r["ProdRegCartID"], 0);
            obj.IsAddOnFormCompleted = ConvertUtility.ConvertToBoolean(r["IsAddOnFormCompleted"], false);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(r["SerialNumber"]);
            obj.SerialCheckStatus = ConvertUtility.ConvertToInt32(r["SerialCheckStatus"], 0);
            if (tbData != null)
            {
                obj.ItemData = new List<Acc_ProductRegCartItemData>();
                foreach (DataRow rData in tbData.Rows)
                {
                    obj.ItemData.Add(Acc_ProductRegCartItemDataBLL.InitFromData(rData));
                }
            }
            return obj;
        }

        //public static Int32 AddItem(Int32 prodRegCartID, String modelNumber, String serialNumber)
        //{
        //    DataRow r = Data.DAAcc_ProductRegCartItem.Insert(prodRegCartID, modelNumber, serialNumber);
        //    if (r == null) return 0;
        //    return ConvertUtility.ConvertToInt32(r["CartItemID"], 0);
        //}

        public static void AddItem(Guid cartWebAccessKey, String modelNumber, String serialNumber, Boolean snValidationNotRequired)
        {
            if (cartWebAccessKey.IsNullOrEmptyGuid() || modelNumber.IsNullOrEmptyString() || serialNumber.IsNullOrEmptyString())
                return;
            List<String> serials = new List<string>() { serialNumber.Trim() };
            Data.DAAcc_ProductRegCartItem.Insert(cartWebAccessKey, modelNumber, serials, snValidationNotRequired);
        }


        public static Int32 AddItem(Guid cartWebAccessKey, String modelNumber, List<String> serialNumbers, Boolean snValidationNotRequired)
        {
            return Data.DAAcc_ProductRegCartItem.Insert(cartWebAccessKey, modelNumber, serialNumbers, snValidationNotRequired);
        }

        public static void Delete(Int32 cartItemID)
        {
            if (cartItemID < 1) return;
            Data.DAAcc_ProductRegCartItem.Delete(cartItemID);
        }

        public static void DeleteAllByCartWebAccessKey(Guid cartWebAccessKey)
        {
            Data.DAAcc_ProductRegCartItem.DeleteAllByCartWebAccessKey(cartWebAccessKey);
        }

        public static void UpdateAddOnFormStatus(Int32 cartItemID, Boolean isCompleted)
        {
            if (cartItemID < 1) return;
            Data.DAAcc_ProductRegCartItem.UpdateAddOnFormStatus(cartItemID, isCompleted);
        }

        public static void UpdateAddOnFormStatus(List<Int32> cartItemIDs, Boolean isCompleted)
        {
            if (cartItemIDs == null) return;
            Data.DAAcc_ProductRegCartItem.UpdateAddOnFormStatus(cartItemIDs, isCompleted);
        }

        public static Boolean IsContainInItem(List<Acc_ProductRegCartItem> items, Int32 cartItemIDToCheck)
        {
            if (items == null || items.Count < 1) return false;
            var results = from i in items
                          where i.CartItemID == cartItemIDToCheck
                          select i;
            return (results != null && results.Count() > 0);
        }

        public static Acc_ProductRegCartItem FindItemByCartItemID(List<Acc_ProductRegCartItem> items, Int32 cartItemID)
        {
            if (items == null || items.Count < 1) return null;
            return (from i in items
                          where i.CartItemID == cartItemID
                          select i).First();
        }

        public static Boolean CheckIfThereIsSerialNumberNotFound(List<Acc_ProductRegCartItem> lst)
        {
            if (lst == null || lst.Count < 1) return false;
            var foundLst = from i in lst where i.SerialCheckStatus == 0 select i;
            return (foundLst != null && foundLst.Count() > 0);
        }
    }
}
