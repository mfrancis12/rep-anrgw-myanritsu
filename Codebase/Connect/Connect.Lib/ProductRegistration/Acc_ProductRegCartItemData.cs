﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class Acc_ProductRegCartItemData
    {
        public Int32 CartItemDataID { get; set; }
        public Int32 CartItemID { get; set; }
        public String DataKey { get; set; }
        public String DataValue { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
