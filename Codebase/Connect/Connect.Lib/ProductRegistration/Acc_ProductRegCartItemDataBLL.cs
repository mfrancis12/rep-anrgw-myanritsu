﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class Acc_ProductRegCartItemDataBLL
    {
        internal static Acc_ProductRegCartItemData InitFromData(DataRow r)
        {
            if (r == null) return null;
            Acc_ProductRegCartItemData obj = new Acc_ProductRegCartItemData();
            obj.CartItemDataID = ConvertUtility.ConvertToInt32(r["CartItemDataID"], 0);
            obj.CartItemID = ConvertUtility.ConvertToInt32(r["CartItemID"], 0);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
            obj.DataKey = ConvertUtility.ConvertNullToEmptyString(r["DataKey"]);
            obj.DataValue = ConvertUtility.ConvertNullToEmptyString(r["DataValue"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static Int32 InsertUpdate(Int32 cartItemID, String dataKey, String dataValue)
        {
            DataRow r = Data.DAAcc_ProductRegCartItemData.InsertUpdate(cartItemID, dataKey.Trim(), dataValue);
            if (r == null) return 0;
            return ConvertUtility.ConvertToInt32(r["CartItemDataID"], 0);
        }

        public static void InsertUpdate(Int32 cartItemID, Dictionary<String, String> data)
        {
            if (cartItemID < 1 || data == null) return;
            Data.DAAcc_ProductRegCartItemData.InsertUpdate(cartItemID, data);
        }
    }
}
