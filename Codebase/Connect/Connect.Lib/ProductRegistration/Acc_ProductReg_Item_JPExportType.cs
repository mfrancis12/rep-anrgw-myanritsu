﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class Acc_ProductReg_Item_JPExportType
    {
        public String JPExportCode { get; set; }
    }
}
