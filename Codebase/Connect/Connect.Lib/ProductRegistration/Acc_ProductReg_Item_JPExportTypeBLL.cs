﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class Acc_ProductReg_Item_JPExportTypeBLL
    {
        internal static Acc_ProductReg_Item_JPExportType InitFromData(DataRow r)
        {
            if (r == null) return null;
            Acc_ProductReg_Item_JPExportType obj = new Acc_ProductReg_Item_JPExportType();
            obj.JPExportCode = ConvertUtility.ConvertNullToEmptyString(r["JPExportCode"]);
            return obj;
        }


        public static DataTable SelectAllExportType()
        {
            return Data.DAAcc_ProductRegistered_Item_JPExportType.SelectAllExportType();
        }
    }
}
