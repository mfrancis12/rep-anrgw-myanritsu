﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_Data
    {
        public Int32 ProdRegDataID { get; set; }
        public Int32 ProdRegID { get; set; }
        public String DataKey { get; set; }
        public String DataValue { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
