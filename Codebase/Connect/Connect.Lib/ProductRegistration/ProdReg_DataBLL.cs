﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_DataBLL
    {
        private static ProdReg_Data InitData(DataRow data)
        {
            if (data == null) return null;
            ProdReg_Data obj = new ProdReg_Data();
            obj.ProdRegDataID = ConvertUtility.ConvertToInt32(data["ProdRegDataID"], 0);
            obj.ProdRegID = ConvertUtility.ConvertToInt32(data["ProdRegID"], 0);
            obj.DataKey = ConvertUtility.ConvertNullToEmptyString(data["DataKey"]);
            obj.DataValue = ConvertUtility.ConvertNullToEmptyString(data["DataValue"]);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(data["CreatedOnUTC"], DateTime.MinValue);
            return obj;
        }

        public static List<ProdReg_Data> SelectByProdRegID(Int32 prodRegID)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Data.SelectByProdRegID(prodRegID);
            if (tb == null) return null;
            List<ProdReg_Data> list = new List<ProdReg_Data>();
            foreach (DataRow itemData in tb.Rows)
            {
                ProdReg_Data item = InitData(itemData);
                if (item != null) list.Add(item);
            }
            return list;
        }
        public static DataTable SelectRegisterDataByProdRegID(Int32 prodRegID)
        {
           return Data.DAAcc_ProductRegistered_Data.SelectByProdRegID(prodRegID);
        }

        public static List<ProdReg_Data> SelectByWebToken(Guid webToken)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Data.SelectByWebToken(webToken);
            if (tb == null) return null;
            List<ProdReg_Data> list = new List<ProdReg_Data>();
            foreach (DataRow itemData in tb.Rows)
            {
                ProdReg_Data item = InitData(itemData);
                if (item != null) list.Add(item);
            }
            return list;
        }

        public static void Save(Int32 prodRegID, String dataKey, String dataValue)
        {
            if (prodRegID < 1) return;
            if (dataKey.IsNullOrEmptyString()) return;
            Data.DAAcc_ProductRegistered_Data.Save(prodRegID, dataKey, dataValue);
        }

        public static void RenewSubmit(Int32 supEntryId, String supportTypeCode)
        {
            Data.DAAcc_ProductRegistered_Data.UpdateSupportEntryMapping_Renew(supEntryId, supportTypeCode);
        }
    }
}
