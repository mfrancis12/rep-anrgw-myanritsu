﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_Item
    {
        public Int32 ProdRegItemID { get; set; }
        public Int32 ProdRegID { get; set; }
        public String ModelConfigType { get; set; }
        public String ModelNumber { get; set; }
        public String SerialNumber { get; set; }
        public ProdReg_ItemStatus StatusCode { get; set; }
        public Boolean InclResrc_TaggedMN { get; set; }
        public String AddedBy { get; set; }
        public String UpdatedBy { get; set; }
        public DateTime ApprovedOnUTC { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }

        public ProdReg_ItemConfig_JPSW Config_JPSW { get; set; }
        public ProdReg_ItemConfig_SIS Config_SIS { get; set; }
        public ProdReg_ItemConfig_ESD Config_ESD { get; set; }

        //public Erp.Erp_ProductConfig Config_Product { get; set; }
        

        public ProdReg_Item() {}
        
       
    }
}
