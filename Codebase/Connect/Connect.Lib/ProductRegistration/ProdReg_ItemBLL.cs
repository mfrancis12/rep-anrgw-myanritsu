﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_ItemBLL
    {
        public static DataTable ForUser_SelectByWebToken(Guid webToken)
        {
            return Data.DAAcc_ProductRegistered_Item.ForUser_SelectByWebToken(webToken);
        }

        private static ProdReg_Item InitData(DataRow itemData)
        {
            if (itemData == null) return null;
            ProdReg_Item obj = new ProdReg_Item();
            obj.ProdRegID = ConvertUtility.ConvertToInt32(itemData["ProdRegID"], 0);
            obj.ProdRegItemID = ConvertUtility.ConvertToInt32(itemData["ProdRegItemID"], 0);
            obj.ModelConfigType = ConvertUtility.ConvertNullToEmptyString(itemData["ModelConfigType"]);
            obj.ModelNumber = ConvertUtility.ConvertNullToEmptyString(itemData["ModelNumber"]);
            obj.SerialNumber = ConvertUtility.ConvertNullToEmptyString(itemData["SerialNumber"]);
            obj.StatusCode = (ProdReg_ItemStatus)Enum.Parse(typeof(ProdReg_ItemStatus), ConvertUtility.ConvertNullToEmptyString(itemData["StatusCode"]));
            obj.InclResrc_TaggedMN = ConvertUtility.ConvertToBoolean(itemData["InclResrc_TaggedMN"], true);
            obj.AddedBy = ConvertUtility.ConvertNullToEmptyString(itemData["AddedBy"]);
            obj.UpdatedBy = ConvertUtility.ConvertNullToEmptyString(itemData["UpdatedBy"]);
            obj.ApprovedOnUTC = ConvertUtility.ConvertToDateTime(itemData["ApprovedOnUTC"], DateTime.MinValue);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(itemData["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(itemData["CreatedOnUTC"], DateTime.MinValue);

            switch (obj.ModelConfigType)
            {
                case Cfg.ModelConfigTypeInfo.CONFIGTYPE_JP:
                    DataRow configData;
                    DataTable usbData;
                    Data.DAAcc_ProductRegistered_ItemConfig_JPDL.Select(obj.ProdRegItemID, out configData, out usbData);
                    if (configData != null)
                    {
                        obj.Config_JPSW = new ProdReg_ItemConfig_JPSW(configData, usbData);
                    }
                    break;
                case Cfg.ModelConfigTypeInfo.CONFIGTYPE_US_MMD:
                    break;
                    //etc...
            }
            return obj;
        }

        public static List<ProdReg_Item> SelectByWebToken_AsList(Guid webToken)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Item.SelectByWebToken(webToken);
            if (tb == null) return null;
            List<ProdReg_Item> list = new List<ProdReg_Item>();
            foreach (DataRow itemData in tb.Rows)
            {
                ProdReg_Item item = InitData(itemData);
                if (item != null) list.Add(item);
            }
            return list;
        }
        public static List<ProdReg_Item> SelectByModelAndTeams_AsList(DataTable dtTeamIds, string masterModel)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Item.SelectByWebModelAndTeams(dtTeamIds, masterModel);
            if (tb == null) return null;
            List<ProdReg_Item> list = new List<ProdReg_Item>();
            foreach (DataRow itemData in tb.Rows)
            {
                ProdReg_Item item = InitData(itemData);
                if (item != null) list.Add(item);
            }
            return list;
        }


        public static List<ProdReg_Item> SelectWithFilter(int prodRegId, String itemType, String mn, String sn)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Item.SelectWithFilter(prodRegId, itemType, mn, sn);
            if (tb == null) return null;
            List<ProdReg_Item> list = new List<ProdReg_Item>();
            foreach (DataRow itemData in tb.Rows)
            {
                ProdReg_Item item = InitData(itemData);
                if (item != null) list.Add(item);
            }
            return list;
        }

        public static DataTable SelectAllByAccountID(Guid accountID, Guid currentUserMembershipId)
        {
            if (accountID.IsNullOrEmptyGuid() || currentUserMembershipId.IsNullOrEmptyGuid()) return null;
            return Data.DAAcc_ProductRegistered_Item.SelectAllByAccountID(accountID, currentUserMembershipId);

        }

        public static ProdReg_Item SelectByProdRegItemId(int prodRegItemID)
        {
            return InitData(Data.DAAcc_ProductRegistered_Item.SelectByProdRegItemId(prodRegItemID));
        }

        public static void UpdateStatus(Int32 prodRegItemID, String statusCode, String updatedBy)
        {
            Data.DAAcc_ProductRegistered_Item.UpdateStatus(prodRegItemID, statusCode, updatedBy);
        }

        public static Boolean IsValidSupport(ProdReg_Item item)
        {
            if (item == null) return false;
            if (item.Config_JPSW != null && item.Config_JPSW.JPSupportContract != DateTime.MinValue)
            {
                return item.Config_JPSW.JPSupportContract >= DateTime.UtcNow;
            }
            //else if (item.Config_ESD != null)
            //{

            //}
            return true;
        }

        public static void Update(Int32 prodRegItemId, String modelNumber, String serialNumber, String statusCode, Boolean includeTaggedMn, String updatedBy)
        {
            Data.DAAcc_ProductRegistered_Item.Update(prodRegItemId, modelNumber, serialNumber, statusCode, includeTaggedMn, updatedBy);
        }

        public static void Delete(Int32 prodRegItemId, string deletedBy)
        {
            Data.DAAcc_ProductRegistered_Item.Delete(prodRegItemId, deletedBy);
        }
    }
}
