﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_ItemConfig_JPSW
    {
        public String JPExport_ModelTarget { get; set; }
        public DateTime JPSupportContract { get; set; }
        public List<ProdReg_Item_JapanUSB> USBKeys { get; set; }

        public ProdReg_ItemConfig_JPSW() { }

        public ProdReg_ItemConfig_JPSW(DataRow data, DataTable dataUSBKeys)
        {
            if (data == null) return;
            JPExport_ModelTarget = ConvertUtility.ConvertNullToEmptyString(data["JPExport_ModelTarget"]);
            JPSupportContract = ConvertUtility.ConvertToDateTime(data["JPSupportContract"], DateTime.MinValue);
            if (dataUSBKeys != null)
            {
                USBKeys = new List<ProdReg_Item_JapanUSB>();
                foreach (DataRow usb in dataUSBKeys.Rows)
                {
                    USBKeys.Add(new ProdReg_Item_JapanUSB(usb));
                }
            }
        }
    }
}
