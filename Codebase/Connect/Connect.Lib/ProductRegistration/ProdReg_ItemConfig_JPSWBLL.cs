﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_ItemConfig_JPSWBLL
    {
        public static DataTable SelectByWebToken(Guid webToken)
        {
            return Data.DAAcc_ProductRegistered_ItemConfig_JPDL.SelectByWebToken(webToken);
        }

        public static Int32 Insert(Int32 prodRegID, String modelNumber, String serialNumber
            , ProdReg_ItemStatus statusCode, Boolean includePrimaryMN, Boolean includeTaggedMN, String addedBy
            , String jpExport_ModelTarget, DateTime jpSupportContract)
        {
            return Data.DAAcc_ProductRegistered_ItemConfig_JPDL.Insert(prodRegID, modelNumber, serialNumber
                , statusCode.ToString()
                , includePrimaryMN
                , includeTaggedMN
                , addedBy
                , jpExport_ModelTarget
                , jpSupportContract);
        }

        public static void DeleteForJPSW(Int32 prodRegItemID)
        {
            Data.DAAcc_ProductRegistered_ItemConfig_JPDL.DeleteForJPSW(prodRegItemID);
        }
    }
}
