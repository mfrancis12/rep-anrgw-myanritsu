﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_Item_JapanUSB
    {
        public Int32 JPUsbRegItemID { get; set; }
        public String USB_No { get; set; }
        public String USB_OwnerEmail { get; set; }

        public ProdReg_Item_JapanUSB() { }

        public ProdReg_Item_JapanUSB(DataRow data) 
        {
            if (data == null) return;
            JPUsbRegItemID = ConvertUtility.ConvertToInt32(data["JPUsbRegItemID"], 0);
            USB_No = ConvertUtility.ConvertNullToEmptyString(data["USBNo"]);
            USB_OwnerEmail = ConvertUtility.ConvertNullToEmptyString(data["USBEmail"]);
        }
    }
}
