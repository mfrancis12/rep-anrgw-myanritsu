﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_Item_JapanUSBBLL
    {
        public static DataTable SelectByUSBKey(String usbNo)
        {
            return Data.DAAcc_ProductRegistered_Item_JPUSB.SelectByUSBKey(usbNo);
        }
    }
}
