﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_Item_Status
    {
        public String StatusDisplayText { get; set; }
        public String StatusCode { get; set; }
        public Int32 DisplayOrder { get; set; }
    }
}
