﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_Item_StatusBLL
    {
        private static ProdReg_Item_Status InitData(DataRow itemData)
        {
            if (itemData == null) return null;
            ProdReg_Item_Status obj = new ProdReg_Item_Status();
            obj.DisplayOrder = ConvertUtility.ConvertToInt32(itemData["DisplayOrder"], 0);
            obj.StatusCode = ConvertUtility.ConvertNullToEmptyString(itemData["StatusCode"]);
            try
            {
                obj.StatusDisplayText = ConvertUtility.ConvertToString(
                    HttpContext.GetGlobalResourceObject("COMMON", obj.StatusCode.ToLowerInvariant()), obj.StatusCode);
            }
            catch
            {
                obj.StatusDisplayText = obj.StatusCode;
            }
            
            return obj;
        }

        public static List<ProdReg_Item_Status> SelectAll()
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Item_Status.SelectAll();
            if (tb == null) return null;
            List<ProdReg_Item_Status> list = new List<ProdReg_Item_Status>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitData(row));
            }
            return list;
        }
    }
}
