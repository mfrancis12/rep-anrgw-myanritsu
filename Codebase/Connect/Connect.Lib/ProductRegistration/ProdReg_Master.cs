﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    [Serializable]
    public class ProdReg_Master
    {
        public Int32 ProdRegID { get; set; }
        public Guid WebToken { get; set; }
        public Guid AccountID { get; set; }
        public String Description { get; set; }
        public String MasterModel { get; set; }
        public String MasterSerial { get; set; }
        public String CreatedBy { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
