﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using Anritsu.AnrCommon.CoreLib;
using System.Text.RegularExpressions;
using Anritsu.Connect.Data;
using Anritsu.Connect.Lib.UI;

namespace Anritsu.Connect.Lib.ProductRegistration
{
    public static class ProdReg_MasterBLL
    {
        internal static ProdReg_Master InitFromData(DataRow r)
        {
            if (r == null) return null;
            ProdReg_Master obj = new ProdReg_Master();
            obj.ProdRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
            obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            obj.WebToken = ConvertUtility.ConvertToGuid(r["WebToken"], Guid.Empty);
            obj.Description = ConvertUtility.ConvertNullToEmptyString(r["Description"]);
            obj.MasterModel = ConvertUtility.ConvertNullToEmptyString(r["MasterModel"]);
            obj.MasterSerial = ConvertUtility.ConvertNullToEmptyString(r["MasterSerial"]);
            obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(r["CreatedBy"]);
            obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
            obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);

            return obj;
        }

        internal static List<ProdReg_Master> InitFromData(DataTable dt)
        {
            if (dt == null || dt.Rows.Count < 1) return null;
            var ProdReg_Masterlist = new List<ProdReg_Master>();
            foreach (DataRow r in dt.Rows)
            {
                ProdReg_Master obj = new ProdReg_Master();
                //obj.ProdRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
                //obj.AccountID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
                //obj.WebToken = ConvertUtility.ConvertToGuid(r["WebToken"], Guid.Empty);
                //obj.Description = ConvertUtility.ConvertNullToEmptyString(r["Description"]);
                obj.MasterModel = ConvertUtility.ConvertNullToEmptyString(r["MasterModel"]);
                obj.MasterSerial = ConvertUtility.ConvertNullToEmptyString(r["MasterSerial"]);
                // obj.CreatedBy = ConvertUtility.ConvertNullToEmptyString(r["CreatedBy"]);
                // obj.ModifiedOnUTC = ConvertUtility.ConvertToDateTime(r["ModifiedOnUTC"], DateTime.MinValue);
                // obj.CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue);
                ProdReg_Masterlist.Add(obj);
            }
            return ProdReg_Masterlist;
        }

        public static Int32 Insert(Guid accountID,
            String mn,
            String sn,
            String createdBy,
            Dictionary<String, String> additionalData,
            Boolean validateSN,
            out Guid webToken, out Int32 regActiveCount,
            String regSNPattern = null)
        {
            webToken = Guid.Empty;
            regActiveCount = 0;

            if (validateSN)
            {
                Boolean isInGlobalSerialNumberSystem = Data.DAProductInfo.IsValidMnSn(mn.Trim(), sn.Trim(), !validateSN);
                Regex reg = new Regex(regSNPattern);
                Match m = reg.Match(sn);
                if (!isInGlobalSerialNumberSystem && (!m.Success || string.IsNullOrEmpty(regSNPattern)))
                {
                    return -2;//invalid sn
                }
            }

            DataRow r = Data.DAAcc_ProductRegistered_Master.Insert(accountID, mn, sn, createdBy, additionalData);
            if (r == null) return 0;
            Int32 prodRegID = ConvertUtility.ConvertToInt32(r["ProdRegID"], 0);
            webToken = ConvertUtility.ConvertToGuid(r["WebToken"], Guid.Empty);
            regActiveCount = ConvertUtility.ConvertToInt32(r["RegActiveCount"], 0);
            return prodRegID;
        }

        public static void Update(Guid webToken, String mn, String sn)
        {
            Data.DAAcc_ProductRegistered_Master.Update(webToken, mn, sn);
        }

        public static ProdReg_Master SelectBySN(Guid accountID, String mn, String sn)
        {
            return InitFromData(Data.DAAcc_ProductRegistered_Master.SelectBySN(accountID, mn, sn));
        }

        public static ProdReg_Master SelectByWebToken(Guid webToken)
        {
            return InitFromData(Data.DAAcc_ProductRegistered_Master.SelectByWebToken(webToken));
        }

        public static Boolean CheckIfOkToDelete(Guid webToken)
        {
            DataRow r = Data.DAAcc_ProductRegistered_Master.CheckIfOkToDelete(webToken);
            if (r == null) return false;
            return ConvertUtility.ConvertToBoolean(r["OkToDelete"], false);
        }

        public static void Delete(Guid webToken)
        {
            Data.DAAcc_ProductRegistered_Master.Delete(webToken);
        }

        public static DataTable ForUser_SelectWithPaging(Int32 startRowIndex, Int32 maximumRows, Guid accountID, Guid userMembershipID, String includeType, out Int32 totalCount)
        {
            Int32 recordsFound = 0;
            DataTable tb = Data.DAAcc_ProductRegistered_Master.ForUser_SelectWithPaging(startRowIndex, maximumRows, accountID, userMembershipID, includeType, out recordsFound);
            totalCount = recordsFound;
            return tb;
        }

        public static void ForUser_UpdateDesc(Int32 prodRegID, String desc)
        {
            Data.DAAcc_ProductRegistered_Master.ForUser_UpdateDesc(prodRegID, desc.Trim());
        }

        public static DataTable SelectByAccMnSn(Guid accountID
             , String masterModel
             , String masterSerial)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            return Data.DAAcc_ProductRegistered_Master.SelectByAccMnSn(accountID, masterModel, masterSerial);
        }

        public static List<ProdReg_Master> SelectByAccMnSnToList(Guid accountID, String masterModel , String masterSerial)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            var tb = Data.DAAcc_ProductRegistered_Master.SelectByAccMnSn(accountID, masterModel, masterSerial);
            if (tb == null) return null;
            var list = new List<ProdReg_Master>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitFromData(row));
            }
            return list;
        }


        public static Boolean IsExistingMasterRegistration(Guid accountID, String masterModel, String masterSerial)
        {
            DataTable tb = SelectByAccMnSn(accountID, masterModel, masterSerial);
            return tb != null && tb.Rows.Count > 0;
        }

        public static List<ProdReg_Master> ForUser_Select(Guid accountID, Guid userMembershipID, String includeType, Int32 cultureGroupIDByCountryCode)
        {
            DataTable tb = Data.DAAcc_ProductRegistered_Master.ForUser_Select(accountID, userMembershipID, includeType, cultureGroupIDByCountryCode);
            if (tb == null) return null;
            List<ProdReg_Master> list = new List<ProdReg_Master>();
            foreach (DataRow row in tb.Rows)
            {
                list.Add(InitFromData(row));
            }
            return list;
        }

        public static void UpdateAddOnData(Int32 prodRegID, Dictionary<String, String> additionalData)
        {
            if (prodRegID > 0 && additionalData != null && additionalData.Count > 0)
            {
                foreach (String datakey in additionalData.Keys)
                {
                    ProdReg_DataBLL.Save(prodRegID, datakey,
                        ConvertUtility.ConvertNullToEmptyString(additionalData[datakey]).Trim()
                        );
                }
            }
        }

        public static string SelectProductFilters(DataTable dtTeamIds, ref HttpStatusCode clientStatus, string modelKey = "", string functionKey = "")
        {
            var tb = DAAcc_ProductRegistered_Master.SelectProductFilters(dtTeamIds, modelKey, functionKey);
            //get Error message
            var errorMsg = new ErrorInfo()
            {
                ErrorMessage = "No rows found"
            };
            //convert into Json String
            return JsonUtil.ConvertTableToJsonString(tb, ref clientStatus, errorMsg);
        }

        public static DataTable SelectRegisteredProducts(DataTable dtTeamIds, string mk, string fk, string ok)
        {
            var tb = Data.DAAcc_ProductRegistered_Master.SelectRegisteredProducts(dtTeamIds, mk, fk, ok);
            return tb;
        }
        public static List<ProdReg_Master> SelectByModelAndTeams(DataTable dtTeamIds, string masterModel)
        {
            return InitFromData(Data.DAAcc_ProductRegistered_Master.SelectByModelAndTeams(dtTeamIds, masterModel));
        }

        public static DataTable CheckItemsToDelete(Guid webToken)
        {            
            return DAAcc_ProductRegistered_Master.CheckItemsToDelete(webToken);
        }

        public static bool HasDeletedProductRegistrations(Guid accountId)
        {
            return DAAcc_ProductRegistered_Master.HasDeletedProductRegistrations(accountId);
        }
    }
}
