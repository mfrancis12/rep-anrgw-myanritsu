﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Security
{
    public enum Sec_LoginStatusCode
    {
        Success = 1, InvalidLogin = 2, NoAccessToApp = 3
    }
}
