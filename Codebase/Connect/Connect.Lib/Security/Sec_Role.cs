﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Security
{
    [Serializable]
    public class Sec_Role
    {
        public String Role { get; set; }
        public String RoleName { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
