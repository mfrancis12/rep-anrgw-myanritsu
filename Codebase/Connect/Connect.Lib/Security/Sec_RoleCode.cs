﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Security
{
    [Serializable]
    public class Sec_RoleCode
    {
        public const String RoleUser = "accuser";
        public const String RoleManager = "accmgr";
        //public const String RoleProductRegistrationAdmin = "adm-prdreg-admin";
        //public const String RoleProductRegistrationApprover = "adm-prdreg-approver";
        public const String RoleManageProductRegistrationAdmin = "adm-manageprdreg";// can delete master model information
        public const String RoleManageCompany = "adm-verify-company";
        public const String RoleManageFileEnggUKTAU = "adm-mngfile-engg-uk-tau";
        public const String RoleManageFileCommEnggUKTAU = "adm-mngfile-commengg-uk-tau";
        public const String RoleReportJP = "adm-reportjp";
        public const String RoleReportUS = "adm-reportus";
        public const String RoleReportTAU = "adm-reporttau";
        public const String RoleReportMMDFeedback = "adm-report-mmdfeedback";
        public const String RoleDistributorEMEA = "adm-sis-emea";
        public const String RoleDistributorRussia = "adm-sis-rus";
        public const String RoleMngSptTypePackagesJp = "adm-mngsupportconfig-package-jp";


        /// <summary>
        /// Link Admin Roles
        /// </summary>
        public const String RoleLnkAdmENGB = "adm-lnk_en-gb";
        public const String RoleLnkAdmENAU = "adm-lnk_en-au";
        public const String RoleLnkAdmENUS = "adm-lnk_en-us";
        public const String RoleLnkAdmJAJP = "adm-lnk_ja-jp";
        public const String RoleLnkAdmKOKR = "adm-lnk_ko-kr";
        public const String RoleLnkAdmRURU = "adm-lnk_ru-ru";
        public const String RoleLnkAdmZHCN = "adm-lnk_zh-cn";
      
        
        #region new roles
        /// <summary>
        /// Product config roles
        /// </summary>
        public const String RoleProductConfigJPAN = "adm-prdconfig-jp-an";
        public const String RoleProductConfigJPM = "adm-prdconfig-jp-m";
        public const String RoleProductConfigUSMMD = "adm-prdconfig-us-mmd";
        public const String RoleProductConfigUKTAU = "adm-prdconfig-uk-tau";
        public const String RoleProductConfigDKM = "adm-prdconfig-dk-m";
      
     
        /// <summary>
        /// Product registration roles
        /// </summary>
        //public const String RoleProductRegJPSW = "adm-prdreg-jpsw";
        public const String RoleProductRegJPM = "adm-prdreg-jpm";
        public const String RoleProductRegJPAN = "adm-prdreg-jpan";
        public const String RoleProductRegESD = "adm-prdreg-esd";
        public const String RoleProductRegUSMMD = "adm-prdreg-usmmd";
        public const String RoleProductRegDKM = "adm-prdreg-dkm";
        
        #endregion

    }
}
