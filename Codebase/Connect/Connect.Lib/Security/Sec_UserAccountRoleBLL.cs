﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Lib.Security
{
    public static class Sec_UserAccountRoleBLL
    {
        public static DataTable SelectForUserAdmin(Guid membershipId)
        {
            if (membershipId.IsNullOrEmptyGuid()) return null;
            return Data.DASec_UserAccountRole.SelectForUserAdmin(membershipId);
        }

        public static DataTable SelectAvailableAccounts(Guid membershipId)
        {
            if (membershipId.IsNullOrEmptyGuid()) return null;
            return Data.DASec_UserAccountRole.SelectAvailableAccounts(membershipId);
        }

        public static DataTable SelectSearchAccounts(Guid membershipId, String searchTxt)
        {
            if (membershipId.IsNullOrEmptyGuid()) return null;
            return Data.DASec_UserAccountRole.SelectSearchAccounts(membershipId, searchTxt);
        }
        public static DataTable SelectSearchAccountsAssignedToPackage(long packageId, String searchTxt)
        {
            if (packageId == 0) return null;
            return Data.DASec_UserAccountRole.SelectSearchAccountsAssignedToPackage(packageId, searchTxt);
        }
    }
}
