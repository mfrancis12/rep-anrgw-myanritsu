﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Anritsu.Connect.Lib.Security
{
    [Serializable]
    public class Sec_UserCache
    {
        public Int32 UserCacheID { get; internal set; }
        public Guid MembershipId { get; set; }
        public String DataKey { get; set; }
        public String DataValue { get; set; }
        public DateTime LastUpdatedUTC { get; set; }
        public DateTime CreatedOnUTC { get; set; }

        
    }
}
