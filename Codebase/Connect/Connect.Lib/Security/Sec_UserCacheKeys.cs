﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.Security
{
    [Serializable]
    public static class Sec_UserCacheKeys
    {
        public const String SelectedAccountID = "SelectedAccountID";
    }
}
