﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Lib.Security
{
    [Serializable]
    public class Sec_UserMembership
    {
        public Guid MembershipId { get; set; }
        public Int32 GWUserId { get; set; }
        public String Email { get; set; }
        public String UserID { get; set; }
        public String LastName { get; set; }
        public String LastNameInRuby { get; set; }
        public String LastNameInEnglish { get; set; }
        public String FirstName { get; set; }
        public String FirstNameInRuby { get; set; }
        public String FirstNameInEnglish { get; set; }
        public String MiddleName { get; set; }
        public String MiddleNameInRuby { get; set; }
        //public String FullName { get { return FirstName + " " + LastName; } }
        public String FullName { get { return LastName + "," + FirstName; } }
        public Boolean IsIndividualEmailAddress { get; set; }
        public Boolean IsApproved { get; set; }
        public Boolean IsLockedOut { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime LastLockOutUTC { get; set; }

        public String UserStreetAddress1 { get; set; }
        public String UserStreetAddress2 { get; set; }
        public String UserCity { get; set; }
        public String UserState { get; set; }
        public String UserZipCode { get; set; }
        public String PhoneNumber { get; set; }
        public String FaxNumber { get; set; }
        public String JobTitle { get; set; }
        public String UserAddressCountryCode { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public DateTime ModifiedOnUTC { get; set; }
        public Boolean IsAdministrator { get; set; }
        public Boolean IsTempUser { get; set; }
        public DataTable TeamsList { get; set; }
        public Sec_UserMembership() { }

    }

    #region " old one to delete" 
    //[Serializable]
    //public class Sec_UserMembership : System.Web.Security.MembershipUser
    //{
    //    public Guid MembershipId { get { return ConvertUtility.ConvertToGuid(base.ProviderUserKey, Guid.Empty); } }
    //    public Int32 GWUserId { get; set; }
    //    public Guid GWSecretKey { get; set; }
    //    public String UserLoginID { get; set; }
    //    public String LastName { get; set; }
    //    public String FirstName { get; set; }
    //    public String FullName { get { return FirstName + " " + LastName; } }
    //    public String JobTitle { get; set; }
    //    public String CompanyName { get; set; }
    //    public Boolean IsAdministrator { get; set; }

    //    public String UserAddress1 { get; set; }
    //    public String UserAddress2 { get; set; }
    //    public String UserAddressCity { get; set; }
    //    public String UserAddressPostalCode { get; set; }
    //    public String UserAddressState { get; set; }
    //    public String UserAddressCountryCode { get; set; }
    //    public String UserPhone { get; set; }
    //    public String UserFax { get; set; }
    //    public String UserPrimaryCultureCode { get; set; }

    //    public Sec_UserMembership(Guid membershipId
    //        , Int32 gwUserId
    //        , Guid gwSecretKey
    //        , String emailAddress
    //        , String userLoginID
    //        , String firstName
    //        , String lastName
    //        , String jobTitle
    //        , String companyName
    //        , String userAddress1
    //        , String userAddress2
    //        , String userAddressCity
    //        , String userAddressPostalCode
    //        , String userAddressState
    //        , String userAddressCountryCode
    //        , String userPhone
    //        , String userFax
    //        , String userPrimaryCultureCode
    //        , Boolean isApproved
    //        , Boolean isLockedOut
    //        , Boolean isAdministrator
    //        , DateTime lastLoginUTC
    //        , DateTime lastActivityUTC
    //        , DateTime lastLockOutUTC
    //        , DateTime createdOnUTC)
    //        : base("ConnectMembershipProvider"
    //            , String.Format("{0}, {1}", lastName, firstName)
    //            , membershipId
    //            , emailAddress
    //            , String.Empty
    //            , String.Empty
    //            , isApproved
    //            , isLockedOut
    //            , createdOnUTC
    //            , lastLoginUTC
    //            , lastActivityUTC
    //            , DateTime.MinValue
    //            , lastLockOutUTC)
    //    {
    //        GWUserId = gwUserId;
    //        UserLoginID = userLoginID.ConvertNullToEmptyString().Trim();
    //        GWSecretKey = gwSecretKey;
    //        LastName = lastName;
    //        FirstName = firstName;
    //        JobTitle = jobTitle;
    //        CompanyName = companyName;
    //        IsAdministrator = isAdministrator;

    //        UserAddress1 = userAddress1;
    //        UserAddress2 = userAddress2;
    //        UserAddressCity = userAddressCity;
    //        UserAddressPostalCode = userAddressPostalCode;
    //        UserAddressState = userAddressState;
    //        UserAddressCountryCode = userAddressCountryCode;
    //        UserPhone = userPhone;
    //        UserFax = userFax;
    //        UserPrimaryCultureCode = userPrimaryCultureCode;
    //    }
    //} 
    #endregion
}
