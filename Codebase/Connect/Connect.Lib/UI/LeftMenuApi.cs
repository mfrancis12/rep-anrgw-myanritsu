﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using ApiSdk;
using HtmlAgilityPack;

namespace Anritsu.Connect.Lib.UI    
{
    internal static class KeyDef
    {
        public class QSKeys
        {
            public const String SiteLocale = "lang";
            public const String ReturnURL = "ReturnURL";
            public const String TargetURL = "TargetURL";
            public const String SsoUrlID = "sulid";
            public const String PageURL = "pgurl";
            public const String ModuleID = "mdlid";
            public const String FormID = "frmid";
            public const String FieldsetID = "fsid";
            public const String FieldsetControlID = "fwscid";
            public const String ResClassKey = "rckey";
            public const String FormSubmitKey = "frmsmtkey";
            public const String DataSourceID = "dsid";
            public const String ProdRegWFInstID = "prwfiid";
            public const String RegisteredProductWebAccessKey = "rwak";
            public const String ProdRegCartWebAccessKey = "crtwak";
            public const String ProdRegCartItemID = "citm";
            public const String ModelNumber = "mn";
            public const String ProductOwnerRegion = "prgn";
            public const String ProfileType = "pftype";
            public const String ProfileStatus = "pfstatus";
            public const String AccountID = "accid";
            public const String CompanyName = "orgname";
            public const String AccountStatusCode = "accstatus";
            public const String IsAccountVerified = "accveri";
            public const String WFInstID = "wfinstid";
            public const String DownloadSource = "dlsc";
            public const String DownloadID = "dlid";
            public const String AddNewOrg = "addnew";
            public const String MemMembershipId = "memid";
            public const String MemEmailAddress = "mememail";
            public const String MemLastName = "memln";
            public const String MemFirstName = "memfn";
            public const String MemIsAdmin = "memadm";
            public const String AdminContentSearch_Auto = "srhmd";
            public const String AdminContentSearch_PreClassKey = "srhclass";
            public const String AdminContentSearch_PreResKey = "srhreskey";
            public const String AdminContent_ContentID = "contentid";
            public const String IsAccountChanged = "accchngstatus";
            public const String IsAddressChanged = "addchngstatus";
            public const String Admin_USBNo = "admjpusb";
            public const String MyProductsFilter = "filter";
            public const String NotificationId = "nfnid";
            public const String PackageId = "pid";
            public const String Package_Success = "pack_success";
            public const String FileId = "fid";
            public const String PName = "pname";

            public class Sso
            {
                public const String SPToken = "sptkn";
                public const String WReply = "wreply";
            }

            public const String Admin_ProdReg_SelectedTabID = "tab";
            public const String CalCertFileName = "calcert";
            public const String ProdRegItemID = "prid";
            public const String SIS_ProductID = "pid";
            public const String SIS_ProductFilterID = "pfid";
            public const String SIS_DocTypeFilterID = "dtfid";
            public const String Admin_CustomLinkID = "admclnkid";
            public const String IsNew = "isNew";
            public const String EntryId = "Eid";
            public const String SupportTypeCode = "typecode";
            public const String ModelConfigType = "mdlcfg";
        }

      
    }


    public class GwLeftMenuApi
    {
        protected static string GetLeftMenuHtml(string currentLanguage)
        {
            //pull Culture code from the session
            //var gwCultureCode = new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            //if (String.IsNullOrEmpty(gwCultureCode.Name)) gwCultureCode = new CultureInfo("en-US");
            //var gwLeftMenu = GetLeftMenuHtml(gwCultureCode.Name);
            //cache left menu

            var apiUri =
                new Uri(ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.LeftLinksUrl"].Replace("[[CULTURE_CODE]]",
                    Convert.ToString(currentLanguage)));
            var apiParams = new Dictionary<string, string>();

            var rsClass = new ResponseClass();
            var response = rsClass.CallMethod<String>(apiUri, apiParams); 
            return (response.StatusCode == HttpStatusCode.OK)
                ? Convert.ToString(response.Content) : string.Empty;
        }
        public static String GetMyAnritsuLeftMenu(String myAnritsuChildMenu)
        {
            //pull Culture code from the session
            var qaParam = Convert.ToString(HttpContext.Current.Request[KeyDef.QSKeys.SiteLocale]);
            var gwCultureCode =new CultureInfo(Thread.CurrentThread.CurrentCulture.Name);
            if (String.IsNullOrEmpty(gwCultureCode.Name)) gwCultureCode =new CultureInfo("en-US");
            var gwLeftMenu = GetLeftMenuHtml(!string.IsNullOrEmpty(qaParam)?qaParam:gwCultureCode.Name);
            var doc = new HtmlDocument();
            try
            {
                doc.LoadHtml(gwLeftMenu);
                var gloablWebMenuLinks =
                    doc.DocumentNode.SelectNodes(
                        "//div[@class=\"aside-menu\"]/div[@class=\"item\"]/div[@class=\"menu-item\"]")[0];
                var myAnritsuHomeLink = HtmlNode.CreateNode(@"<a href=""/home"" class=""my-anritsu-link haschild"">" + GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlGWMenuMyAnritsuHome.Text") + "</a>");
                gloablWebMenuLinks.ChildNodes.Add(myAnritsuHomeLink);

                //insert My Anritsu Links
                var globalPusher =
                   doc.DocumentNode.SelectNodes(
                       "//div[@class=\"pusher\"]")[0];
                var myAnritsuChildLinks = HtmlNode.CreateNode(myAnritsuChildMenu);
                globalPusher.ChildNodes.Add(myAnritsuChildLinks);
                return doc.DocumentNode.OuterHtml;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        public static List<HyperLink> GetAbstractMyAnritsuChildLinks(bool hasDistributorPortalRole)
        {
            var abstarctlinks = new List<HyperLink>
            {
                new HyperLink()
                {
                    ToolTip =
                        GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlMyProducts.ToolTip"),
                    NavigateUrl = "/myproduct/home",
                    Text = GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlMyProducts.Text")
                },
                new HyperLink()
                {
                    ToolTip =
                        GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlRegisterProduct.ToolTip"),
                    NavigateUrl = "/myproduct/regproduct-select",
                    Text = GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlRegisterProduct.Text")
                },
                new HyperLink()
                {
                    ToolTip =
                       GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlManageAccount.ToolTip"),
                    NavigateUrl = ConfigUtility.AppSettingGetValue("AnrSso.Idp.EditAccount"),
                    Text = GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlManageAccount.Text")                  
                },
                new HyperLink()
                {
                    ToolTip =
                       String.Empty,
                    NavigateUrl = "/myaccount/selectcompany",
                    Text = GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlManageTeam.Text")
                }
            };
            if (hasDistributorPortalRole)
                abstarctlinks.Add(new HyperLink() { ToolTip = GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlDistInfo.ToolTip"), NavigateUrl = "/mydistributorinfo/home", Text = Convert.ToString(GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlDistInfo.Text")) });
            //abstarctlinks.Add(new HyperLink()
            //{
            //    ToolTip =
            //        GetGlobalResourceObject("STCTRL_HeaderCtrl", "hlSignOut.Text"),
            //    NavigateUrl = "/spsignout",
            //    Text = GetGlobalResourceObject("STCTRL_HeaderCtrl", "hlSignOut.Text")
            //});
            return abstarctlinks;
        }

        public static String GenerateSideMenuHtml(List<HyperLink> menu)
        {
            var anritsuSideMenu = new StringBuilder(@"<div class=""aside-menu aside-menu-myanritsu"">");
            anritsuSideMenu.Append(@"<div class=""item active"" data-tag=""/home"" style=""left: 0px;"">");
            anritsuSideMenu.Append(@"<div class=""menu-title""><a href=""/home"" class=""globalweb-link my-anritsu-home"">Back to Menu</a></div>");
            const string subTitle = @"<div class=""menu-subtitle"">{0}</div>";
            anritsuSideMenu.Append(String.Format(subTitle, GetGlobalResourceObject("~/home", "PageTitle")));
            anritsuSideMenu.Append(@"<div class=""menu-item"">");
            //iterate through the links
            foreach (var link in menu)
            {
                if (link.NavigateUrl.Equals(ConfigUtility.AppSettingGetValue("AnrSso.Idp.EditAccount")))
                {
                    var langUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);

                    var lang = langUri.Query.Contains("?") ? langUri.Query.Replace('?', '&') : !string.IsNullOrEmpty(langUri.Query) ? string.Format("&{0}", langUri.Query) : string.Empty;
                    if (!string.IsNullOrEmpty(lang))
                    {
                        link.NavigateUrl = String.Format(ConfigUtility.AppSettingGetValue("AnrSso.Idp.EditAccount") + "?{0}={1}{2}"
                         , KeyDef.QSKeys.ReturnURL
                         , HttpUtility.UrlEncode(String.Format("{0}"
                         , HttpContext.Current.Request.Url.AbsoluteUri)),lang);
                    }
                    else
                    {
                        link.NavigateUrl = String.Format(ConfigUtility.AppSettingGetValue("AnrSso.Idp.EditAccount") + "?{0}={1}"
                            , KeyDef.QSKeys.ReturnURL
                            , HttpUtility.UrlEncode(String.Format("{0}"
                            , HttpContext.Current.Request.Url.AbsoluteUri)));
                    }
                }
                anritsuSideMenu.Append(String.Format(@"<a href=""{0}"">{1}</a>", link.NavigateUrl, link.Text));
            }
            anritsuSideMenu.Append(@"</div></div></div>");
            return anritsuSideMenu.ToString();
        }
        private static String GetGlobalResourceObject(string resClass, string resKey)
        {
            return Convert.ToString(HttpContext.GetGlobalResourceObject(resClass, resKey));
        }
    }
}
