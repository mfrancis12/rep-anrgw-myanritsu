﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.UI
{
    [Serializable]
    public class Site_Feature
    {
        public Int32 FeatureID { get; set; }
        public Guid FeatureKey { get; set; }
        public String FeatureName { get; set; }
        public String ControlPath { get; set; }
        public String StaticResourceClassKey { get; set; }
        public String EditContentUrl { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
