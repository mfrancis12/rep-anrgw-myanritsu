﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Anritsu.Connect.Lib.UI
{
    public static class Site_LegacySsoUrlBLL
    {
        public static DataRow SelectBySsoUrlID(Int32 ssoUrlID)
        {
            if (ssoUrlID < 1) return null;
            return Data.DASite_LegacySsoUrl.SelectBySsoUrlID(ssoUrlID);
        }
    }
}
