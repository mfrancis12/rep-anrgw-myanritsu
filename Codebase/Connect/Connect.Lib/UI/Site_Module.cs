﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.UI
{
    [Serializable]
    public class Site_Module
    {
        public Int32 ModuleID { get; set; }
        public Int32 FeatureID { get; set; }
        public String ModuleName { get; set; }
        public Boolean ShowHeader { get; set; }
        public Boolean NoBorder { get; set; }
        public Boolean IsPublic { get; set; }
        public Boolean HideFromLoggedInUser { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public Site_Feature FeatureInfo { get; set; }
        public String ResourceClassKey { get { return String.Format("MDL_{0}", ModuleID); } }
        public String EditContentUrl 
        { 
            get { 
                if(FeatureInfo == null) return String.Empty;
                return String.Format("{0}?mdlid={1}", FeatureInfo.EditContentUrl, ModuleID); 
            } 
        }
        public String EditSettingsUrl 
        { 
            get { 
                return String.Format("~/siteadmin/moduleadmin/editmodule?mdlid={0}", ModuleID); 
            } 
        }
    }
}
