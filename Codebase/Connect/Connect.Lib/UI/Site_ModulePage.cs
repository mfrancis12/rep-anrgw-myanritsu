﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.UI
{
    [Serializable]
    public class Site_ModulePage
    {
        public Int32 PageRefID { get; set; }
        public String PageUrl { get; set; }
        public Int32 ModuleID { get; set; }
        public String PlaceHolder { get; set; }
        public Int32 LoadOrder { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public Site_Module ModuleInfo { get; set; }
    }
}
