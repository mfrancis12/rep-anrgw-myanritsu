﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.UI
{
    [Serializable]
    public class Site_ModuleSetting
    {
        public Int32 ModuleSettingID { get; set; }
        public Int32 ModuleID { get; set; }
        public String SettingKey { get; set; }
        public String SettingValue { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
