﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Lib.UI
{
    [Serializable]
    public class Site_Sitemap
    {
        public String PageUrl { get; set; }
        public String ParentPageUrl { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}
