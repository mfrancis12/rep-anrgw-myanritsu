﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.AnrDgl
{
    public partial class DglAccess : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            String usbNo = Request.Form["LoginID"].ConvertNullToEmptyString().Trim();
            String passwd = Request.Form["Passwd"].ConvertNullToEmptyString().Trim();
            //test data
           // usbNo = "WG22-JU11-00000007";//MCS-WebDL-ML@zz.anritsu.co.jp
           // passwd = "webgatekey";//"katsuyo";
            //String usbNo = "WG22-JU11-00000006";
            //String passwd = "katsuyo";
            Lib.DongleDownload.DongleLic dlic = CreateNewDongleLic(usbNo, passwd);
            if (dlic == null)
            {
                HandleNoAccess();
            }
            else
            {
                ProductReg_JPUSB_Utility.JapanUSBLicDataSet(dlic);
                Response.Redirect("dglaccesslogin");
            }
        }

        private Lib.DongleDownload.DongleLic CreateNewDongleLic(String usbNo, String usbPwd)
        {
            if (usbNo.IsNullOrEmptyString() || usbPwd.IsNullOrEmptyString()) return null;
            if (!Lib.DongleDownload.DongleLicBLL.IsValidUSBNoAndPassword(usbNo, usbPwd)) return null;
            Lib.DongleDownload.DongleLic dlic = Lib.DongleDownload.DongleLicBLL.CreateNew(usbNo.Trim(), usbPwd.Trim());
            return dlic;

        }


        private void HandleNoAccess()
        {
            ProductReg_JPUSB_Utility.JapanUSBLicDataSet(null);
            //change with localized message later.  Shas, pls check with allison on this one
            //Response.Write(" Your Web Access Key may be expired.<br/>");
            //Response.Write(" Please check the expiration date and registration.<br/>");
            //Response.Write(" For inquiry,contact one of our representatives or send us e-mail below.<br/>");
            //Response.Write(" E-Mail download-support@zy.anritsu.co.jp<br/>");
            //Response.Flush();
            Response.Redirect("/error403-usb");
        }
    }
}