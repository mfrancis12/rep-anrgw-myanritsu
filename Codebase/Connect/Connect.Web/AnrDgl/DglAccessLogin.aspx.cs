﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.AnrDgl
{
    public partial class DglAccessLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
            Lib.DongleDownload.DongleLic dlic = ProductReg_JPUSB_Utility.JapanUSBLicDataGet(false);

            if (!User.Identity.IsAuthenticated || usr == null || dlic == null || dlic.DongleInfo == null)
            {
                HandleNoAccess();
            }
            else
            {
                String usbNo = Lib.DongleDownload.DongleLicBLL.GetValue(dlic.DongleInfo, "USBNo").ConvertNullToEmptyString().Trim();
                if (!Lib.DongleDownload.DongleLicBLL.CheckDongle(usbNo, usr.Email)) // THIS IS IMPORTANT.  CHECK IN  DATABASE TO VALIDATE USBNO AND OWNER
                {
                    HandleNoAccess();
                }
                else
                {
                    dlic.IsUserValidated = true;
                    ProductReg_JPUSB_Utility.JapanUSBLicDataSet(dlic);
                    Response.Redirect(ConfigUtility.AppSettingGetValue("Connect.Lib.JPDongle.RedirectUrl"), true);
                    Response.End();
                }
            }
           
        }

        private void HandleNoAccess()
        {
            ProductReg_JPUSB_Utility.JapanUSBLicDataSet(null);
            //Response.Redirect("dglaccess", true);
            //Response.End();
            Response.Redirect("/error403-usb");
            Response.End();
        }
    }
}