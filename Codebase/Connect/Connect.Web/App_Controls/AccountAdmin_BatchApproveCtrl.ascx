﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountAdmin_BatchApproveCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.AccountAdmin_BatchApproveCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlProdRegBatchApproval" runat="server" ShowHeader="true"
    HeaderText="<%$Resources:ADM_AccountAdmin_BatchApproveCtrl,pnlProdRegBatchApproval.HeaderText%>">
    <div class="settingrow">
        <table>
            <tr>
                <td style="width: 50px;">
                    <span class="settinglabel">
                        <asp:Localize ID="lcalMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalMN.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrMN" runat="server"></asp:Literal>
                </td>
                <td style="width: 10px;">
                    &nbsp;
                </td>
                <td align="right">
                    <span class="settinglabel">
                        <asp:Localize ID="lcalSN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalSN.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" AutoPostBack="true"
                        OnCheckedChanged="chkSelectAll_Clicked" Visible="false" />
                </td>
                <td>
                    <asp:CheckBoxList ID="cbxListInput" runat="server" SkinID="DFrmSkin" DataField="SerialNumber"
                        RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="4" CellPadding="5"
                        CellSpacing="5">
                    </asp:CheckBoxList>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalStatusCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalStatusCode.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="cmbProdRegStatus" runat="server">
        </asp:DropDownList>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalModelTarget" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalModelTarget.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="cmbProdRegModelTarget" runat="server">
            <asp:ListItem Text="not available" Value="none" Selected="True"></asp:ListItem>
            <asp:ListItem Text="UH" Value="UH"></asp:ListItem>
            <asp:ListItem Text="MH" Value="MH"></asp:ListItem>
            <asp:ListItem Text="UG" Value="UG"></asp:ListItem>
            <asp:ListItem Text="MG" Value="MG"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="settingrow">
        <fieldset id="fsIncludeDownloadSrc">
            <legend>
                <asp:Localize ID="lcalIncludeDLSrcOptions" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalIncludeDLSrcOptions.Text%>"></asp:Localize></legend>
            <asp:CheckBox ID="cbxIncludeDlSrcGW" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeDlSrcGW.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeDlSrcJP" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeDlSrcJP.Text%>"
                Checked="true" />
        </fieldset>
    </div>
    <div class="settingrow">
        <fieldset id="fsIncludeModelOptions">
            <legend>
                <asp:Localize ID="lcalIncludeModelOptions" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalIncludeModelOptions.Text%>"></asp:Localize></legend>
            <asp:CheckBox ID="cbxIncludeMNPrimary" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNPrimary.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeMNTags" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNTags.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeMNRegSpfc" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNRegSpfc.Text%>"
                Checked="true" />
        </fieldset>
    </div>
    <div class="settingrow" style="text-align: right;">
        <p class="msg">
            <asp:Literal ID="ltrApproveAllInfoMsg" runat="server"></asp:Literal></p>
        <asp:Button ID="bttApproveAll" runat="server" Text="Approve" OnClick="bttApproveAll_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>
