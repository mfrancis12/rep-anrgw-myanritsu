﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Controls
{
    //public partial class AccountAdmin_BatchApproveCtrl : CustomControlBase.ProdRegAdmin_DetailBase, App_Lib.UI.IStaticLocalizedCtrl
    //{
    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        if (!IsPostBack)
    //        {
    //            if (ProductReg == null)
    //            {
    //                this.Visible = false;
    //                return;
    //            }

    //            BindStatus();
    //            BindAllSnForApproval();
    //        }
    //    }

    //    private void BindStatus()
    //    {
    //        DataTable tb = Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectStatus();
    //        cmbProdRegStatus.Items.Clear();
    //        foreach (DataRow r in tb.Rows)
    //        {
    //            String statusCode = r["StatusCode"].ToString();
    //            String statusText = ConvertUtility.ConvertNullToEmptyString(GetGlobalResourceObject("COMMON", statusCode.ToLowerInvariant()));
    //            ListItem item = new ListItem(statusText, statusCode.ToLowerInvariant());
    //            cmbProdRegStatus.Items.Add(item);
    //        }
    //    }

    //    private void BindAllSnForApproval()
    //    {
    //        Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;
    //        App_Lib.AppCacheFactories.ProductConfigCacheFactory dataCache = new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
    //        Lib.Erp.Erp_ProductConfig_TBD prodCfg = dataCache.GetProductConfigInfo(prodReg.ModelNumber);

    //        DataTable tb = Lib.ProductRegistration.Acc_ProductRegisteredDataBLL.SelectByProdRegID(ProductReg.ProdRegID);
    //        String submittedByEmail = string.Empty;
    //        foreach (DataRow r in tb.Rows)
    //        {
    //            if (r.ItemArray[2].ToString().Equals("SubmittedBy_Email"))
    //            {
    //                submittedByEmail = r.ItemArray[3].ToString();
    //                break;
    //            }
    //        }

    //        List<SearchByOption> searchOptions = UIHelper.AdminProdRegSearchOptionsGet(true);
    //        searchOptions.Add(new SearchByOption("AccountID", prodReg.AccountID));
    //        searchOptions.Add(new SearchByOption("MN", prodReg.ModelNumber));
    //        searchOptions.Add(new SearchByOption("RegStatus", "submitted"));
    //        searchOptions.Add(new SearchByOption("UserEmail", submittedByEmail));

    //        if (searchOptions.Count < 1)
    //        {
    //            ReturnUrl();
    //        }
    //        UIHelper.AdminProdRegSearchOptionsSet(searchOptions);

    //        DataTable tbProdSearch = Lib.ProductRegistration.Acc_ProductRegisteredBLL.AdminProdRegSearch(UIHelper.AdminProdRegSearchOptionsGet(false));
    //        cbxListInput.Items.Clear();

    //        if (tbProdSearch !=null && tbProdSearch.Rows.Count > 0)
    //        {
    //            if (tbProdSearch.Rows.Count == 1)
    //            {
    //                DataRow row = tbProdSearch.Rows[0];
    //                String serialNumber = row["SerialNumber"].ToString();
    //                if (ProductReg.SerialNumber.Equals(serialNumber))
    //                {
    //                    ReturnUrl();
    //                }
    //            }
    //            foreach (DataRow r in tbProdSearch.Rows)
    //            {
    //                String serialNumber = r["SerialNumber"].ToString();
    //                String webAccessKey = r["WebAccessKey"].ToString();
    //                ListItem item = new ListItem(serialNumber, webAccessKey.ToLowerInvariant());
    //                cbxListInput.Items.Add(item);
    //            }
    //        }
    //        else
    //        {
    //            ReturnUrl();
    //        }

    //        if (cbxListInput.Items.Count > 1)
    //            chkSelectAll.Visible = true;

    //        cmbProdRegStatus.SelectedValue = "submitted";
    //        ltrMN.Text = prodReg.ModelNumber;
          
    //        cbxIncludeDlSrcGW.Checked = prodReg.IncludeDLSrcGW;
    //        cbxIncludeDlSrcJP.Checked = prodReg.IncludeDLSrcJP;
    //        cbxIncludeMNPrimary.Checked = prodReg.IncludeModelPrimary;
    //        cbxIncludeMNTags.Checked = prodReg.IncludeModelTags;
    //        cbxIncludeMNRegSpfc.Checked = prodReg.IncludeModelRegSpecific;

    //    }


    //    protected void chkSelectAll_Clicked(Object sender, EventArgs e)
    //    {
    //        if (chkSelectAll.Checked)
    //        {
    //            foreach (ListItem li in cbxListInput.Items)
    //            {
    //                li.Selected = true;
    //            }
    //        }
    //        else
    //        {
    //            foreach (ListItem li in cbxListInput.Items)
    //            {
    //                li.Selected = false;
    //            }
    //        }
    //    }


    //    #region IStaticLocalizedCtrl Members

    //    public string StaticResourceClassKey
    //    {
    //        get { return "ADM_AccountAdmin_BatchApproveCtrl"; }
    //    }

    //    public string GetStaticResource(string resourceKey)
    //    {
    //        return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
    //    }

    //    #endregion


    //    protected void bttApproveAll_Click(object sender, EventArgs e)
    //    {
    //        try
    //        {
    //            ltrApproveAllInfoMsg.Text = String.Empty;
    //            String modelTarget = String.Empty;
    //            if (cmbProdRegModelTarget.SelectedValue != "none") modelTarget = cmbProdRegModelTarget.SelectedValue;
    //            int approvedCount = 0;
    //            foreach (ListItem li in cbxListInput.Items)
    //            {
    //                if (li.Selected)
    //                {
    //                    Guid webAccessKey = Guid.Empty;

    //                    webAccessKey = Guid.Parse(li.Value);
    //                    Lib.ProductRegistration.Acc_ProductRegisteredBLL.Update(webAccessKey
    //               , cmbProdRegStatus.SelectedValue.ToLowerInvariant()
    //               , DateTime.MinValue
    //               , cbxIncludeDlSrcGW.Checked
    //               , cbxIncludeDlSrcJP.Checked
    //               , cbxIncludeMNPrimary.Checked
    //               , cbxIncludeMNTags.Checked
    //               , cbxIncludeMNRegSpfc.Checked
    //               , modelTarget);
    //               approvedCount = approvedCount + 1;
    //                }
                    
    //            }

    //            if(approvedCount > 0)
    //            ltrApproveAllInfoMsg.Text = GetStaticResource("MSG_SNsUpdated");

    //            ClearCache(ProductReg);

    //            UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);//reset
    //            UIHelper.ProdRegDetailDataSet(ProductReg);
    //            BindAllSnForApproval();
              
    //        }
    //        catch (ArgumentException aex)
    //        {
    //            ltrApproveAllInfoMsg.Text = aex.Message;
    //        }
    //    }

    //    private void ReturnUrl()
    //    {
    //        String query = String.Format("?{0}={1}", KeyDef.QSKeys.RegisteredProductWebAccessKey, ProdRegWebAccessKey.ToString());
    //        String url = KeyDef.UrlList.SiteAdminPages.AdminProdRegDetail_RegInfo_ToBeDeleted + query;
    //        WebUtility.HttpRedirect(null, url);
    //    }

    //    private void ClearCache(Acc_ProductRegistered ProductReg)
    //    {
    //        App_Lib.AppCacheFactories.DownloadCacheFactory dlDataCache =
    //               new App_Lib.AppCacheFactories.DownloadCacheFactory();
    //        dlDataCache.TouchCacheFile();
    //        dlDataCache.RemoveFromCache(ProductReg.ProdRegID);
    //    }

    //    }
    
}