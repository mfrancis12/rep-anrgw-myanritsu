﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountAdmin_DetailCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.AccountAdmin_DetailCtrl" %>
<%@ Register Src="AddressCtrl.ascx" TagName="AddressCtrl" TagPrefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlCompanyInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,pnlCompanyInfo.HeaderText%>"
    Visible="false">
    <div class="width-60">

    <div class="settingrow group input-text required">
        <label>
            <asp:Localize ID="lcalCompName" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompName.Text%>"></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="lclCompNameErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>
        <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvCompanyName" runat="server"  Display="Dynamic" ValidationGroup="vgCompInfo"
            ControlToValidate="txtCompanyName" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>--%>
    </div>
    <div class="settingrow required group input-text">
        <label>
            <asp:Localize ID="lcalAccountName" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountName.Text%>"></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="lclAccntName" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
            <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>
         </p>
        <asp:TextBox ID="txtAccountName" runat="server"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvAccountName" runat="server" Display="Dynamic" ValidationGroup="vgCompInfo"
            ControlToValidate="txtAccountName" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>--%>
    </div>
    <div class="settingrow group input-text" id="divCompanyNameInRuby" runat="server">
        <label>
            <asp:Localize ID="lcalRuby" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalRuby.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtCompanyNameInRuby" runat="server"></asp:TextBox>
    </div>
    <div class="settingrow">
        <label>
            <asp:Localize ID="lcalAddress" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAddress.Text%>"></asp:Localize></label>
        <div>
            <uc1:AddressCtrl ID="addrCompanyAddress" runat="server" ValidationGroup="vgCompInfo"
                IsFaxVisible="false" IsFaxRequired="false" IsPhoneVisible="false" IsPhoneRequired="false" />
        </div>
    </div>
    </div>
    <div class="settingrow" id="divIsVerified" runat="server">
        <span class="settinglabelrgt-10">
            <asp:Localize ID="lcalIsVerified" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalIsVerified.Text%>"></asp:Localize></span>
        <asp:Image ID="imgAccountVerifiedAndApproved" runat="server" />
    </div>
    <div class="settingrow" id="divAgreedToTerms" runat="server">
        <span class="settinglabelrgt-10">
            <asp:Localize ID="lcalAgreeToTerms" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAgreeToTerms.Text%>"></asp:Localize></span>
        <asp:Literal ID="ltrAgreedToTerms" runat="server"></asp:Literal>
    </div>
    <div class="settingrow" id="divAgreeToTermsOn" runat="server">
        <span class="settinglabelrgt-10">
            <asp:Localize ID="lcalAgreedToTermsOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAgreedToTermsOnUTC.Text%>"></asp:Localize></span>
        <asp:Literal ID="ltrAgreedToTermsOnUTC" runat="server"></asp:Literal>
    </div>
    <div class="settingrow" style="padding-left: 500px">
        <span class="msg">
            <asp:Literal ID="ltrCompanyMsg" runat="server"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <asp:Button ID="bttDeleteCompany" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttDeleteCompany.Text%>" 
            OnClick="bttDeleteCompany_Click" />
        <asp:Button ID="bttSaveCompany" runat="server" ValidationGroup="vgCompInfo" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttSaveCompany.Text%>"
            SkinID="submit-btn" OnClick="bttSaveCompany_Click" />
    </div>
    <div class="settingrow">
        <fieldset>
            <legend>
                <asp:Localize ID="lcalAccountVerifications" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountVerifications.Text%>"></asp:Localize></legend>
            <p>
                <asp:Localize ID="lcalAccountVerificationDesc" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountVerificationDesc.Text%>"></asp:Localize></p>
            <div class="settingrow group input-textarea width-60">
                <label>
                    <asp:Localize ID="lcalCompanyVerificationInternalComment" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompanyVerificationInternalComment.Text%>"></asp:Localize></label>
                <asp:TextBox ID="txtCompanyVerificationInternalComment" runat="server" TextMode="MultiLine"
                    Style="vertical-align: middle;" MaxLength="300"></asp:TextBox>
                </div>
            <div class="group margin-top-15">
             <asp:Button ID="bttVerifyAndApproveAccount" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttVerifyAndApproveAccount.Text%>"
                    OnClick="bttVerifyAndApproveAccount_Click" />
                <asp:Button ID="bttUnVerifyAccount" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttUnVerifyAccount.Text%>"
                    OnClick="bttUnVerifyAccount_Click" />
            </div>
            <div class="settingrow">
                <asp:GridView ID="gvAccountVerifications" runat="server" AutoGenerateColumns="false"
                    Width="720px">
                    <Columns>
                        <asp:BoundField DataField="ModifiedBy" ItemStyle-Width="200px" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.VerifiedBy.HeaderText%>"
                            ItemStyle-CssClass="gvstyle1-item-lft" />
                        <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.VerifiedOnUTC.HeaderText%>">
                            <ItemTemplate>
                                <asp:Literal ID="ltrVerifiedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"ModifiedOnUTC")) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.ChangedStatus.HeaderText%>">
                            <ItemTemplate>
                                <asp:Literal ID="ltrStatus" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "IsVerified") ) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="InternalComment" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.InternalComment.HeaderText%>"
                            ItemStyle-CssClass="gvstyle1-item-lft" />
                    </Columns>
                </asp:GridView>
            </div>
        </fieldset>
    </div>
    <div class="settingrow">
        <fieldset>
            <legend>
                <asp:Localize ID="lcalAdditionalNote" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAdditionalNote.Text%>"></asp:Localize></legend>
            <label class="">
                <asp:Localize ID="lcalAdditionalNoteDesc" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAdditionalNoteDesc.Text%>"></asp:Localize></label>
            <div class="settingrow group width-60 input-textarea">
                <label>
                    <asp:Localize ID="lcalCompanyAdditinalNotes" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompanyAdditinalNotes.Text%>"></asp:Localize></label>
                <asp:TextBox ID="txtCompanyAdditinalNotes" runat="server" TextMode="MultiLine" Style="vertical-align: middle;"
                    MaxLength="4000"></asp:TextBox>&nbsp;&nbsp;
                
            </div>
            <div class="group input-top-15">
                <asp:Button ID="bttAddNote" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttAddNote.Text%>"
                    OnClick="bttAddNote_Click" />
            </div>
        </fieldset>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_AccountAdmin_DetailCtrl" />
