﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class AccountAdmin_DetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        private Acc_AccountMaster _AccountInfo;
        public Guid AccountID
        {
            get { return ConvertUtility.ConvertToGuid(ViewState["vsAccountID"], Guid.Empty); }
            set { ViewState["vsAccountID"] = value; }
        }

        public Boolean AllowAddNewAccount
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState["vsAllowAddNewAccount"], true); }
            set { ViewState["vsAllowAddNewAccount"] = value; }
        }

        public Acc_AccountMaster AccountInfo
        {
            get
            {
                if (AccountID.IsNullOrEmptyGuid()) return null;
                if (_AccountInfo == null)
                {
                    _AccountInfo = AccountUtility.Admin_AccountInfoGet(AccountID);
                }
                return _AccountInfo;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);
            bttDeleteCompany.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteOrgMsg") + "')";
            bttUnVerifyAccount.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmUnverifyOrgMsg") + "')";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCompanyInfo();

            }
        }

        //<summary>
        //Checks whether user has access to modify company
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            // if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD))
            //Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageCompany);
            return isAdminMode;
        }

        public void BindCompanyInfo()
        {
            divCompanyNameInRuby.Visible = SiteUtility.BrowserLang_IsJapanese();
            if (AccountID.IsNullOrEmptyGuid())
            {
                bttDeleteCompany.Visible = false;
                divAgreedToTerms.Visible = divAgreeToTermsOn.Visible = divIsVerified.Visible = false;
                bttSaveCompany.Visible = AllowAddNewAccount;
                pnlCompanyInfo.Visible = AllowAddNewAccount;
                return;
            }
            Lib.Account.Acc_AccountMaster acc = AccountInfo;
            if (acc == null) return;

            txtCompanyName.Text = acc.CompanyName;
            txtAccountName.Text = acc.AccountName;
            txtCompanyNameInRuby.Text = acc.CompanyNameInRuby;

            if (acc.IsVerifiedAndApproved)
            {
                //bttUnVerifyAccount.Visible = true;
                //bttVerifyAndApproveAccount.Visible = false;
                imgAccountVerifiedAndApproved.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/verified.gif";
            }
            else
            {
                //bttUnVerifyAccount.Visible = false;
                //bttVerifyAndApproveAccount.Visible = true;
                imgAccountVerifiedAndApproved.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/alert.png";
            }
            ltrAgreedToTerms.Text = acc.AgreedToTerms.ToString();
            ltrAgreedToTermsOnUTC.Text = "n/a";
            if (acc.AgreedToTermsOnUTC != DateTime.MinValue) ltrAgreedToTermsOnUTC.Text = GetDate(acc.AgreedToTermsOnUTC);
            if (!acc.OwnerMemberShipID.IsNullOrEmptyGuid()) addrCompanyAddress.LoadAddress(acc.OwnerMemberShipID);

            pnlCompanyInfo.Visible = true;
            txtCompanyAdditinalNotes.Text = acc.AdditinalNotes;

            bttDeleteCompany.Visible = (acc.AccountID != AccountUtility.AnritsuMasterAccountID());

            BindAccountVerifications();

            bttSaveCompany.Visible = bttDeleteCompany.Visible = bttVerifyAndApproveAccount.Visible = bttUnVerifyAccount.Visible = bttAddNote.Visible = pnlCompanyInfo.Enabled = IsAdminMode();

        }

        private void BindAccountVerifications()
        {
            gvAccountVerifications.DataSource = Lib.BLL.BLLAcc_AccountMaster.AccountVerifications_Select(AccountID);
            gvAccountVerifications.DataBind();
        }

        protected void bttVerifyAndApproveAccount_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.VerifyAccount(AccountID, admin.Email, txtCompanyVerificationInternalComment.Text.Trim());
            txtCompanyVerificationInternalComment.Text = String.Empty;
            //bttUnVerifyAccount.Visible = true;
            //bttVerifyAndApproveAccount.Visible = false;
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        protected void bttUnVerifyAccount_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.UnVerifyAccount(AccountID, admin.Email, txtCompanyVerificationInternalComment.Text.Trim());
            txtCompanyVerificationInternalComment.Text = String.Empty;
            //bttUnVerifyAccount.Visible = false;
            //bttVerifyAndApproveAccount.Visible = true;
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        protected void bttAddNote_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.UpdateNotes(AccountID, txtCompanyAdditinalNotes.Text.Trim());
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        public string GetStatus(object isVerifiedObj)
        {
            Boolean isVerified = ConvertUtility.ConvertToBoolean(isVerifiedObj, false);
            if (isVerified)
                return GetStaticResource("VerifiedStatus");
            else
                return GetStaticResource("UnVerifiedStatus");
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        protected void bttSaveCompany_Click(object sender, EventArgs e)
        {
            Page.Validate("vgSaveCompany");
            if (!Page.IsValid) return;
            ltrCompanyMsg.Text = String.Empty;

            try
            {
                //Lib.Account.Profile_Address addr = addrCompanyAddress.GetAddressValue();
                //if (addr == null) throw new ArgumentException(GetStaticResource("MSG_InvalidAddress"));
                if (AccountID.IsNullOrEmptyGuid())
                {
                    App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                    Guid newAccountID = Lib.BLL.BLLAcc_AccountMaster.CreateAccount(user.MembershipId
                        , txtAccountName.Text.Trim()
                        , txtCompanyName.Text.Trim()
                        , txtCompanyNameInRuby.Text.Trim()
                        , WebUtility.GetUserIP()
                        , user.Email
                        , user.FullName);
                    WebUtility.HttpRedirectWithUpdatedQueryStrings(this, String.Format("?{0}={1}", KeyDef.QSKeys.AccountID, newAccountID.ToString()));
                }
                else
                {
                    Lib.BLL.BLLAcc_AccountMaster.Update(AccountID, txtAccountName.Text.Trim(), txtCompanyName.Text.Trim(), txtCompanyNameInRuby.Text.Trim(), false);
                    //Lib.BLL.BLLProfile_Address.Update(addr, false);
                    ltrCompanyMsg.Text = GetStaticResource("MSG_Updated");
                    AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
                }
            }
            catch (Exception ex)
            {
                ltrCompanyMsg.Text = ex.Message;
            }
        }

        protected void bttDeleteCompany_Click(object sender, EventArgs e)
        {
            if (AccountID.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");
            ltrCompanyMsg.Text = String.Empty;

            try
            {
                Lib.BLL.BLLAcc_AccountMaster.DeleteByAccountID(AccountID);
                AccountUtility.AdminOrgSearchOptionsSet(null);
                AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
                WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgSearch);
            }
            catch (Exception ex)
            {
                ltrCompanyMsg.Text = ex.Message;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_AccountAdmin_DetailCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

    }
}