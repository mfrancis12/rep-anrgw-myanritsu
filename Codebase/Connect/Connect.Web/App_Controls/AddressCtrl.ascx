﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.AddressCtrl" %>
<%@ Register Src="~/App_Controls/StateCtrl.ascx" TagPrefix="uc1" TagName="StateCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="false" HeaderText="<%# HeaderText %>">
    <div class="settingrow group input-text">
        <label>
            <asp:Localize ID="lcalAddress1" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalAddress1.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="300"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1" Display="Dynamic" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="revtxtAddress1" runat="server" ControlToValidate="txtAddress1" ValidationGroup="vgCompInfo"
            Display="Dynamic" ValidationExpression='^[^<>%]*$' ErrorMessage="<%$Resources:Common,ERROR_InvalidAddressInput%>"></asp:RegularExpressionValidator>
    </div>
    <div class="settingrow group input-text">
        <label>
            <asp:Localize ID="lcalAddress2" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalAddress2.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="300"></asp:TextBox>
        <asp:RegularExpressionValidator ID="revtxtAddress2" runat="server" ControlToValidate="txtAddress2" ValidationGroup="vgCompInfo"
            Display="Dynamic" ValidationExpression='^[^<>%]*$' ErrorMessage="<%$Resources:Common,ERROR_InvalidAddressInput%>"></asp:RegularExpressionValidator>
    </div>
    <div class="settingrow group input-text">
        <label>
            <asp:Localize ID="lcalCityTown" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalCityTown.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtCityTown" runat="server" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvCityTown" runat="server" ControlToValidate="txtCityTown" Display="Dynamic" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow input-select group">
        <label>
            <asp:Localize ID="lcalCountryCode" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalCountryCode.Text%>"></asp:Localize></label>
        <asp:DropDownList ID="cmbCountries" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbCountries_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div class="settingrow group input-text">
        <label>
            <asp:Localize ID="lcalStateProvince" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalStateProvince.Text%>"></asp:Localize></label>
        <uc1:StateCtrl runat="server" ID="scStates" />
    </div>
    <div class="settingrow group input-text">
        <label>
            <asp:Localize ID="lcalZipPostalCode" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalZipPostalCode.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtZipPostalCode" runat="server" MaxLength="50"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvZipPostalCode" runat="server" ControlToValidate="txtZipPostalCode" Display="Dynamic" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>

    <span class='dfrm-msg-red'>
        <asp:CustomValidator ID="cvCountries" runat="server" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:CustomValidator></span>
    <div id="divPhone" runat="server" class="settingrow" visible="false">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalPhone" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalPhone.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtPhone" runat="server" SkinID="tbx-200" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" Display="Dynamic" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div id="divFax" runat="server" class="settingrow" visible="false">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalFax" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,lcalFax.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtFax" runat="server" SkinID="tbx-200" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvFax" runat="server" ControlToValidate="txtFax" Display="Dynamic" ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow" id="divUpdateButton" runat="server" visible="false">
        <span class="settinglabel-10">&nbsp;</span>
        <asp:Button ID="bttUpdate" runat="server" Text="<%$Resources:STCTRL_AddressCtrl,bttUpdate.Text%>"
            SkinID="SmallButton" />
    </div>
    <asp:HiddenField ID="hlMemberShipID" runat="server" Value="0" />
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_AddressCtrl" />
</anrui:GlobalWebBoxedPanel>
