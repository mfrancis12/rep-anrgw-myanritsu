﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.AnrCommon.GeoLib.BLL;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.UI;
using GEO = Anritsu.AnrCommon.GeoLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class AddressCtrl : UserControl, IStaticLocalizedCtrl
    {
        #region Public Fields

        public String HeaderText { get; set; }

        public String HoldState
        {
            get { return Convert.ToString(ViewState["State"]); }
            set { ViewState["State"] = value; }
        }

        public String State
        {
            get { return scStates.SelectedStateValue; }
        }

        public String Address1
        {
            get
            {
                return txtAddress1.Text;
            }
        }

        public String Address2
        {
            get
            {
                return txtAddress2.Text;
            }
        }

        public String City
        {
            get
            {
                return txtCityTown.Text;
            }
        }

        public String ZipCode
        {
            get
            {
                return txtZipPostalCode.Text;
            }
        }

        public String UserCountry
        {
            get
            {
                return cmbCountries.SelectedValue;
            }
        }

        public String Phone
        {
            get
            {
                return txtPhone.Text;
            }
        }

        public String Fax
        {
            get
            {
                return txtFax.Text;
            }
        }

        public Guid OwnerMemberShipId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(ViewState["OwnerMemberShipId"], Guid.Empty);
            }
            set
            {
                ViewState["OwnerMemberShipId"] = value.ToString();
            }
        }

        public Boolean ShowSaveButton { get; set; }
        public String ValidationGroup { get; set; }
        public Boolean IsPhoneVisible { get; set; }
        public Boolean IsPhoneRequired { get; set; }
        public Boolean IsFaxVisible { get; set; }
        public Boolean IsFaxRequired { get; set; }

        #endregion

        #region Page Events

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (cmbCountries.SelectedItem.Value.Equals("US"))
            {
                scStates.refreshControl(this, e, "US");
                scStates.setState(true, false, HoldState);
            }
            else if (cmbCountries.SelectedItem.Value.Equals("JP"))
            {
                scStates.refreshControl(this, e, "JP");
                scStates.setState(false, true, HoldState);
            }
            else
                scStates.setState(false, false, HoldState);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    BindCountries();
            //    cmbCountries_SelectedIndexChanged(this, e);
            //}
            //else
            //{
            //    scStates._IsUnitedStates = cmbCountries.SelectedValue.Equals("US");
            //    scStates._IsJapan = cmbCountries.SelectedValue.Equals("JP");
            //}
            //SetValidationGroup();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCountries();
                cmbCountries_SelectedIndexChanged(this, e);
            }
            else
            {
                scStates._IsUnitedStates = cmbCountries.SelectedValue.Equals("US");
                scStates._IsJapan = cmbCountries.SelectedValue.Equals("JP");
            }
            SetValidationGroup();

        }

        #endregion

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_AddressCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        private void SetValidationGroup()
        {
            if (ValidationGroup.IsNullOrEmptyString()) return;
            //rfvAddress1.ValidationGroup = ValidationGroup;
            //rfvCityTown.ValidationGroup = ValidationGroup;
            scStates.ValidationGroup = ValidationGroup;
            //rfvZipPostalCode.ValidationGroup = ValidationGroup;
            cvCountries.ValidationGroup = ValidationGroup;
            rfvPhone.ValidationGroup = ValidationGroup;
            rfvPhone.Enabled = IsPhoneRequired;
            rfvFax.ValidationGroup = ValidationGroup;
            rfvFax.Enabled = IsFaxRequired;
            bttUpdate.ValidationGroup = ValidationGroup;

            divFax.Visible = IsFaxVisible;
            divPhone.Visible = IsPhoneVisible;
        }

        private void BindCountries()
        {
            cmbCountries.Items.Clear();
            var tb = BLLIso_Country.SelectAll();
            if (tb == null) return;
            var gi = BLLGeoInfo.GetCountryInfoFromIP4();
            foreach (DataRow r in tb.Rows)
            {
                var li = new ListItem
                {
                    Value = r["Alpha2"].ToString().ToUpperInvariant(),
                    Text =
                        GetGlobalResourceObject(BLLIso_Country.ISOCountryNameClassKey,
                            r["ISOCountryNameResKeyValue"].ToString()).ToString()
                };
                if (gi != null && li.Value.Equals(gi.CountryCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    li.Selected = true;
                }
                cmbCountries.Items.Add(li);
            }
        }

        #endregion

        #region Public Methods

        public void SetAddress(Profile_Address address)
        {
            if (address == null || address.IsNullOrEmpty())
            {
                txtAddress1.Text = string.Empty;
                txtAddress2.Text = string.Empty;
                txtCityTown.Text = string.Empty;
                HoldState = string.Empty;
                txtZipPostalCode.Text = string.Empty;
                cmbCountries.SelectedIndex = -1;
                txtPhone.Text = string.Empty;
                txtFax.Text = string.Empty;
                pnlContainer.Enabled = true;
                return;
            }
           // if (cmbCountries.Items.Count == 0) BindCountries();
            txtAddress1.Text = address.Address1;
            txtAddress2.Text = address.Address2;
            txtCityTown.Text = address.CityTown;
            HoldState = address.StateProvince;
            txtZipPostalCode.Text = address.ZipPostalCode;
            cmbCountries.SelectedValue = address.CountryCode;
            txtPhone.Text = address.PhoneNumber;
            txtFax.Text = address.FaxNumber;
            pnlContainer.Enabled = address.IsNullOrEmpty();
        }

        public void LoadAddress(Guid ownerMemberShipId)
        {
            OwnerMemberShipId = ownerMemberShipId;
            if (OwnerMemberShipId.IsNullOrEmptyGuid()) return;
            SetAddress(BLLProfile_Address.SelectByMemberShipID(OwnerMemberShipId));
        }
     
        public Profile_Address UpdateUserAddress(DataRow ssoUserData, Sec_UserMembership myAnritsuUser)
        {
            //--update the user address
            var connectUser = ConnectSsoUser.SetMyAnritsuUser(myAnritsuUser, ssoUserData);
            //update the user profile data
            BLLSec_UserMembership.Update(connectUser);
            return new Profile_Address()
            {
                Address1 = connectUser.UserStreetAddress1,
                Address2 = connectUser.UserStreetAddress2,
                CityTown = connectUser.UserCity,
                CountryCode = connectUser.UserAddressCountryCode,
                FaxNumber = connectUser.FaxNumber,
                PhoneNumber = connectUser.PhoneNumber,
                ZipPostalCode = connectUser.UserZipCode,
                StateProvince = connectUser.UserState
            };
        }

        
        #endregion

        #region Postback Events

        protected void cmbCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCountry = cmbCountries.SelectedValue;
            if (selectedCountry.Equals("US"))
            {
                scStates.refreshControl(this, e, selectedCountry);
            }
            else if (selectedCountry.Equals("JP"))
            {
                scStates.refreshControl(this, e, selectedCountry);
            }
        }

        #endregion
    }
}