﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminContent_AddNewCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.AdminContent_AddNewCtrl" %>
<anrui:GlobalWebBoxedPanel ID="gwpnlAddNewContent" runat="server" ShowHeader="true" HeaderText='<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,gwpnlAddNewContent.HeaderText %>'>
<div class="settingrow group input-text width-60">
    <label><asp:Localize ID="lcalClassKey" runat="server" Text="<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,lcalClassKey.Text %>"></asp:Localize></label>
    <p class="error-message">
        <asp:Localize ID="lclClsKeyErr" runat="server" Text="<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,rfvClassKey.ErrorMessage %>"></asp:Localize>
    </p>
    <asp:TextBox ID="txtClassKey" runat="server" ValidationGroup="vgAddNewRes"></asp:TextBox>
    <%--<asp:RequiredFieldValidator ID="rfvClassKey" runat="server" ControlToValidate="txtClassKey" ValidationGroup="vgAddNewRes" ErrorMessage='<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,rfvClassKey.ErrorMessage %>'></asp:RequiredFieldValidator>--%>
</div>
<div class="settingrow group input-text width-60">
    <label><asp:Localize ID="lcalResKey" runat="server" Text="<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,lcalResKey.Text %>"></asp:Localize></label>
    <p class="error-message">
        <asp:Localize ID="lclResKeyErr" runat="server" Text="<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,rfvResKey.ErrorMessage %>"></asp:Localize>
     </p>
    <asp:TextBox ID="txtResKey" runat="server" ValidationGroup="vgAddNewRes"></asp:TextBox>
    <%--<asp:RequiredFieldValidator ID="rfvResKey" runat="server" ControlToValidate="txtResKey" ValidationGroup="vgAddNewRes" ErrorMessage='<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,rfvResKey.ErrorMessage %>'></asp:RequiredFieldValidator>--%>
</div>
<div class="settingrow margin-top-15">
<asp:Button ID="bttAddNewContent" runat="server" Text='<%$Resources:ADM_STCTRL_AdminContent_AddNewCtrl,bttAddNewContent.Text %>' onclick="bttAddNewContent_Click"  ValidationGroup="vgAddNewRes" SkinID="submit-btn"/>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_AdminContent_AddNewCtrl" />
</anrui:GlobalWebBoxedPanel>
