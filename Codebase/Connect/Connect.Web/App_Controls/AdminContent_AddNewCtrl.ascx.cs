﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class AdminContent_AddNewCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitClassKey();
            }
        }

        private void InitClassKey()
        {
            List<SearchByOption> lst = ContentSearchUtility.AdminContentSearchOptionsGet(false);
            if (lst != null)
            {
                var classKey = from s in lst
                               where s.SearchByField.Equals("ClassKey")
                               select s;
                if (classKey != null && classKey.Count() > 0)
                {
                    txtClassKey.Text = ConvertUtility.ConvertNullToEmptyString(classKey.First().SearchObject);
                }
            }
        }

        protected void bttAddNewContent_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            Int32 contentID = Lib.Content.Res_ContentStoreBLL.Insert(txtClassKey.Text.Trim(), txtResKey.Text.Trim(), "en", "-", false);
            if (contentID > 0)
            {
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.AdminContentDetails, KeyDef.QSKeys.AdminContent_ContentID, contentID));
            }
        }
    }
}