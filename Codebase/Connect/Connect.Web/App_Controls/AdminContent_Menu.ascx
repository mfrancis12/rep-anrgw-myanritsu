﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminContent_Menu.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.AdminContent_Menu" %>
<anrui:GlobalWebBoxedPanel ID="gwpnlContentLocale" runat="server" ShowHeader="false" HeaderText=" ">
<div class="gwsbrmenu">
    <ul>
        <li><asp:HyperLink ID="hlContentSearch" runat="server" Text='<%$ Resources:ADM_STCTRL_AdminContent_Menu,hlContentSearch.Text %>' NavigateUrl='/siteadmin/contentadmin/contenthome'></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink ID="hlContentAddNew" runat="server" Text='<%$ Resources:ADM_STCTRL_AdminContent_Menu,hlContentAddNew.Text %>' NavigateUrl='/siteadmin/contentadmin/contentaddnew'></asp:HyperLink>
        </li>
   </ul>
   </div>
   <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_AdminContent_Menu" />
   </anrui:GlobalWebBoxedPanel>