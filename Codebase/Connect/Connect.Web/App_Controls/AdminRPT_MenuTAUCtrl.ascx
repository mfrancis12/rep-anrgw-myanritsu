﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminRPT_MenuTAUCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.AdminRPT_MenuTAUCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="false">
<div class="reports-links-wrapper">
    <asp:Localize ID="lcalHtml" runat="server" Text='<%$ Resources:ADM_STCTRL_AdminRPT_MenuTAUCtrl,lcalHtml.Text%>'></asp:Localize>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_AdminRPT_MenuTAUCtrl" />
</anrui:GlobalWebBoxedPanel>