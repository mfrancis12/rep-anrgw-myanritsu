﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResLocalizationItemCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_Content.ResLocalizationItemCtrl" %>
<div class="settingrow" style="margin-left: auto; margin-right: auto;">
    <dx:ASPxGridView ID="dxgvResItems" runat="server" Theme="AnritsuDevXTheme" KeyFieldName="ContentID" Width="100%"  OnCommandButtonInitialize="dxgvResItems_CommandButtonInitialize"
        OnRowUpdating="dxgvResItems_RowUpdating" OnRowInserting="dxgvResItems_RowInserting" OnRowDeleting="dxgvResItems_RowDeleting">
        <Columns>
            <dx:GridViewDataColumn FieldName="ContentID" Caption=" " VisibleIndex="0" Visible="false" ReadOnly="true"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="Locale" Caption="Locale" VisibleIndex="1"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ContentSnippet" Caption="Content" Width="550px" VisibleIndex="2">
                <DataItemTemplate>
                    <%# Eval("ContentSnippet") %>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ContentStr" VisibleIndex="10" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewCommandColumn VisibleIndex="100" Caption=" ">
                <EditButton Visible="True" />
                <NewButton Visible="True" />
                <DeleteButton Visible="true"></DeleteButton>
            </dx:GridViewCommandColumn>
        </Columns>
        <Templates>
            <EditForm>
                <dx:ASPxFormLayout ID="formLayout" runat="server">
                    <Items>     
                         <dx:LayoutItem Caption="Locale" RequiredMarkDisplayMode="Required">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server">
                                    <dx:ASPxLabel ID="dxlblLocaleEdit" runat="server" Text='<%# Eval("Locale") %>' OnInit="dxlblLocaleEdit_Init"></dx:ASPxLabel>
                                    <dx:ASPxComboBox ID="dxcmbLocaleEdit" runat="server" Value='<%# Eval("Locale") %>' OnInit="dxcmbLocaleEdit_Init">
                                        <Items>
                                            <dx:ListEditItem Text="English" Value="en" />
                                            <dx:ListEditItem Text="Japanese" Value="ja" />
                                            <dx:ListEditItem Text="Korean" Value="ko" />
                                            <dx:ListEditItem Text="Russian" Value="ru" />
                                            <dx:ListEditItem Text="Chinese (Traditional)" Value="zh-TW" />
                                            <dx:ListEditItem Text="Chinese (Simplified)" Value="zh-CN" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>                   
                        <dx:LayoutItem Caption="Content" RequiredMarkDisplayMode="Required">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxMemo ID="dxmemoResLocaleEditContent" runat="server" Width="550px" Height="100px" Text='<%# Eval("ContentStr") %>'></dx:ASPxMemo>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>
                </dx:ASPxFormLayout>
                 <div style="text-align: right; padding: 10px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton" runat="server" />
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton" runat="server" />
                    </div>
            </EditForm>
        </Templates>
        <SettingsEditing NewItemRowPosition="Bottom"/>
        <SettingsPager Mode="ShowAllRecords"/>
        <Settings ShowTitlePanel="false" />
        <SettingsBehavior  ConfirmDelete="true"  />
    </dx:ASPxGridView>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ResLocalizationItemCtrl" />
</div>