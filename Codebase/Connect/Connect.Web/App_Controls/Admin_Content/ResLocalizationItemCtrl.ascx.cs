﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;

using DevExpress.Web;
using DevExpress.Web.Data;
namespace Anritsu.Connect.Web.App_Controls.Admin_Content
{
    public partial class ResLocalizationItemCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ClassKey { get { return (ViewState["ResLClassKey"] == null ? String.Empty : ViewState["ResLClassKey"].ToString()); } set { ViewState["ResLClassKey"] = value; } }
        public String ResKey { get { return (ViewState["ResLResKey"] == null ? String.Empty : ViewState["ResLResKey"].ToString()); } set { ViewState["ResLResKey"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty(ClassKey) || String.IsNullOrEmpty(ResKey))
                {
                    this.Visible = false;
                    return;
                }
            }
            BindLocalizedStrings();
        }

        public void BindLocalizedStrings()
        {
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(ClassKey.Trim(), ResKey.Trim());
            if (tb == null || tb.Rows.Count < 1)
            {
                Int32 defaultContentID = CreateDefaultContent();
                if (defaultContentID < 1)
                {
                    this.Visible = false;
                    return;
                }
                tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(ClassKey.Trim(), ResKey.Trim());
            }
            dxgvResItems.DataSource = tb;
            dxgvResItems.DataBind();
        }

        private Int32 CreateDefaultContent()
        {
            return Lib.Content.Res_ContentStoreBLL.Insert(ClassKey.Trim(), ResKey.Trim(), "en", ResKey.Trim(), false);
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void dxgvResItems_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(e.Keys[0], 0);

            ASPxFormLayout formLayout = (ASPxFormLayout)dxgvResItems.FindEditFormTemplateControl("formLayout");
            ASPxMemo dxmemoResLocaleEditContent = formLayout.FindControl("dxmemoResLocaleEditContent") as ASPxMemo;

            string contentStr = dxmemoResLocaleEditContent.Text.Trim();
            Lib.Content.Res_ContentStoreBLL.Update(contentID, contentStr, false, true);
            dxgvResItems.CancelEdit();
            e.Cancel = true;
            BindLocalizedStrings();

        }

        protected void dxlblLocaleEdit_Init(object sender, EventArgs e)
        {
            ASPxLabel dxlblLocaleEdit = sender as ASPxLabel;
            dxlblLocaleEdit.Visible = !dxgvResItems.IsNewRowEditing;
        }

        protected void dxcmbLocaleEdit_Init(object sender, EventArgs e)
        {
            ASPxComboBox dxcmbLocaleEdit = sender as ASPxComboBox;
            dxcmbLocaleEdit.Visible = dxgvResItems.IsNewRowEditing;
        }

        protected void dxgvResItems_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            ASPxFormLayout formLayout = (ASPxFormLayout)dxgvResItems.FindEditFormTemplateControl("formLayout");
            ASPxMemo dxmemoResLocaleEditContent = formLayout.FindControl("dxmemoResLocaleEditContent") as ASPxMemo;
            ASPxComboBox dxcmbLocaleEdit = formLayout.FindControl("dxcmbLocaleEdit") as ASPxComboBox;

            string contentStr = dxmemoResLocaleEditContent.Text.Trim();
            String locale = dxcmbLocaleEdit.Value.ToString();

            if (contentStr.IsNullOrEmptyString() || locale.IsNullOrEmptyString())
            {
                e.Cancel = true;
                return;
            }
            CultureInfo ci = new CultureInfo(locale);
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.Select(ClassKey, ResKey, ci, true);
            if (content == null) Lib.Content.Res_ContentStoreBLL.Insert(ClassKey, ResKey, locale, contentStr, true);
            else throw new ArgumentException("Duplicate locale.");

            dxgvResItems.CancelEdit();
            e.Cancel = true;
            BindLocalizedStrings();
        }

        protected void dxgvResItems_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(e.Keys[0], 0);
            Lib.Content.Res_ContentStoreBLL.DeleteByContentID(contentID);
            e.Cancel = true;
            BindLocalizedStrings();
        }

        protected void dxgvResItems_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
        {
            int isFirstRow = e.VisibleIndex;
            if (isFirstRow == 0)
            {
                // hide the Delete button
                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
                    e.Visible = false;

            }
        }
    }
}