﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomLinkDetailsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_CustomLink.CustomLinkDetailsCtrl" %>
<%@ Register Src="~/App_Controls/Admin_Content/ResLocalizationItemCtrl.ascx" TagPrefix="uc1" TagName="ResLocalizationItemCtrl" %>

     <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkOwnerRole" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkOwnerRole.Text %>" />:</span>
            <dx:ASPxComboBox ID="dxcmbLnkOwnerRoles" runat="server" TextField="RoleName" ValueField="Role" Width="350px"></dx:ASPxComboBox>
    </div>
    <div class="settingrow">
        
       <span class="settinglabel-15">
            <asp:Localize ID="lcalCLnkDisplayContainer" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalCLnkDisplayContainer.Text %>" />:</span>
            <dx:ASPxComboBox ID="dxcmbDisplayContainer" runat="server" TextField="ContainerName" ValueField="DisplayContainerID" Width="350px">
                <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorDisplayMode="ImageWithText" ValidationGroup="vgClnk"></ValidationSettings>
            </dx:ASPxComboBox>
    </div>
    <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalCLnkInternalName" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalCLnkInternalName.Text %>" />:</span>
        <dx:ASPxTextBox ID="dxtxtCLnkInternalName" runat="server" Width="350px" MaxLength="30" ValidateRequestMode="Enabled" HelpText="Maximum 30 characters.">
            <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorDisplayMode="ImageWithText" ValidationGroup="vgCLnk" ValidateOnLeave="true"></ValidationSettings>
        </dx:ASPxTextBox>
    </div>
<asp:Panel ID="pnlCLnkEditMode" runat="server">
    <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalCLnkOrder" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalCLnkOrder.Text %>" />:</span>
        <dx:ASPxTextBox ID="dxtxtClnkOrder" runat="server" Width="50px" MaxLength="5" ValidateRequestMode="Enabled" Text="1">
            <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorDisplayMode="ImageWithText" ValidationGroup="vgCLnk" ValidateOnLeave="true" 
                RegularExpression-ErrorText="invalid! (0 to 9 only)" RegularExpression-ValidationExpression="^\d$"></ValidationSettings>
        </dx:ASPxTextBox>
    </div>
     <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkTargetUrl" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkTargetUrl.Text %>" />:</span>
            <dx:ASPxTextBox ID="dxtxtClnkTargetUrl" runat="server" Width="350px" MaxLength="500" ValidateRequestMode="Enabled" HelpText="Use [[LL-CC]] to dynamically replace with end-user's company address. <br/>Other keys: [[MN]], [[SN]].">
                <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorDisplayMode="ImageWithText" ValidationGroup="vgCLnk" ValidateOnLeave="true">
                    <RegularExpression ValidationExpression="^http://((www|www2|www3).anritsu.com|(www|www1).anritsu.co.jp|www.anritsu.tv|my.anritsu.com)/(.*)" ErrorText="Site not allowed." />
                </ValidationSettings>
        </dx:ASPxTextBox>
    </div>
     <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkTarget" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkTarget.Text %>" />:</span>
            <dx:ASPxComboBox ID="dxcmbClnkTarget" runat="server" Width="100px">
                <Items>
                    <dx:ListEditItem Text="_blank" Value="_blank" Selected="true" />
                    <dx:ListEditItem Text="_self" Value="_self" />
                </Items>
            </dx:ASPxComboBox>
    </div>
    <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkIconImage" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkIconImage.Text %>" />:</span>
        <dx:ASPxTextBox ID="dxtxtClnkIconImage" runat="server" Width="350px" MaxLength="500" HelpText="//dl.cdn-anritsu.com/images/...." >
            <ValidationSettings RequiredField-ErrorText="required!" ErrorDisplayMode="ImageWithText" ValidationGroup="vgCLnk" ValidateOnLeave="true">
                <RegularExpression ValidationExpression="^https:<%=Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath%>/images/legacy-images/images/(.*)" ErrorText="Invalid image site." />
            </ValidationSettings>
        </dx:ASPxTextBox>
    </div>
    <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkStartOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkStartOnUTC.Text %>" />:</span>
            <dx:ASPxDateEdit ID="dxdtStartOnUTC" runat="server"></dx:ASPxDateEdit>
    </div>
    <div class="settingrow">
       <span class="settinglabel-15">
            <asp:Localize ID="lcalClnkExpireOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,lcalClnkExpireOnUTC.Text %>" />:</span>
            <dx:ASPxDateEdit ID="dxdtExpireOnUTC" runat="server"></dx:ASPxDateEdit>
    </div>
    </asp:Panel>
    <div class="settingrow">
        <span class="settinglabel-15">&nbsp;</span>
        <dx:ASPxButton ID="dxbttCustomLinkSave" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,dxbttCustomLinkSave.Text %>" CausesValidation="true" ValidationGroup="vgCLnk" OnClick="dxbttCustomLinkSave_Click"></dx:ASPxButton>
        <dx:ASPxButton ID="dxbttCustomLinkDelete" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,dxbttCustomLinkDelete.Text %>" OnClick="dxbttCustomLinkDelete_Click"
            CausesValidation="false" Visible="false">
            <ClientSideEvents Click="function(s,e) { e.processOnServer = confirm('Are you sure?'); }" />
        </dx:ASPxButton>
    </div>
     <div class="settingrow">
       <span class="settinglabel-15">&nbsp;</span>
        <dx:ASPxLabel ID="lblClnkMsg" runat="server" CssClass="msg"></dx:ASPxLabel>
    </div>

<anrui:GlobalWebBoxedPanel ID="gwpnlTranslationsContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl,gwpnlTranslationsContainer.HeaderText %>">
    <uc1:ResLocalizationItemCtrl runat="server" ID="ResLocalizationItemCtrl1" />
</anrui:GlobalWebBoxedPanel>

<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_CustomLink_CustomLinkDetailsCtrl" /></div>