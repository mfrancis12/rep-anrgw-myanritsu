﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.CustomLink;

using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinkDetailsCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 CustomLinkID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.Admin_CustomLinkID], 0);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDisplayContainers();
                BindLinkOwnerRoles();
                InitInfo();
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private void BindDisplayContainers()
        {
            dxcmbDisplayContainer.DataSource = CustomLink_DisplayContainerBLL.SelectAll(false);
            dxcmbDisplayContainer.DataBind();
        }

        private void BindLinkOwnerRoles()
        {
            List<CustomLink_OwnerRole> list = CustomLink_OwnerRolesBLL.SelectAll(false);

            List<CustomLink_OwnerRole> availableRoles = new List<CustomLink_OwnerRole>();
            foreach (CustomLink_OwnerRole or in list)
            {
                if (LoginUtility.IsAdminRole(or.Role)) 
                    availableRoles.Add(or);
            }
            if (availableRoles.Count < 1) throw new HttpException(403, "Forbidden");

            dxcmbLnkOwnerRoles.DataSource = availableRoles;
            dxcmbLnkOwnerRoles.DataBind();
            dxcmbLnkOwnerRoles.SelectedIndex = 0;
        }

        private void InitInfo()
        {
            Int32 customLinkID = CustomLinkID;
            dxdtStartOnUTC.Date = DateTime.UtcNow;
            dxdtExpireOnUTC.Date = DateTime.UtcNow.AddYears(1);
            if (customLinkID < 1)
            {
                ResLocalizationItemCtrl1.Visible = false;
                gwpnlTranslationsContainer.Visible = false;
                dxcmbDisplayContainer.Enabled = true;
                dxcmbLnkOwnerRoles.Enabled = true;
                pnlCLnkEditMode.Visible = false;
                return;
            }

            Lib.CustomLink.CustomLink_Master clm = Lib.CustomLink.CustomLink_MasterBLL.SelectByCustomLinkID(false, customLinkID);
            if (clm == null) throw new HttpException(404, "Page not found.");
            pnlCLnkEditMode.Visible = true;
            dxbttCustomLinkDelete.Visible = true;
            dxcmbDisplayContainer.Value = clm.DisplayContainerID;
            dxcmbDisplayContainer.Enabled = false;
            dxcmbLnkOwnerRoles.Enabled = false;
            dxcmbLnkOwnerRoles.Value = clm.LnkOwnerRole;
            dxtxtCLnkInternalName.Text = clm.LnkInternalName;
            dxtxtClnkOrder.Text = clm.LnkOrder.ToString();
            dxtxtClnkTargetUrl.Text = clm.TargetUrl;
            dxcmbClnkTarget.Value = clm.LinkTarget;
            dxtxtClnkIconImage.Text = clm.IconImageUrl;
            dxdtStartOnUTC.Date = clm.StartOnUTC;
            dxdtExpireOnUTC.Date = clm.ExpireOnUTC;

            ResLocalizationItemCtrl1.ClassKey = clm.LnkResClass;
            ResLocalizationItemCtrl1.ResKey = "Link.Text";
            ResLocalizationItemCtrl1.Visible = true;

            gwpnlTranslationsContainer.Visible = true;

            dxtxtClnkIconImage.ValidationSettings.RequiredField.IsRequired = clm.DisplayContainer.ImageIconRequired;
            
        }

        protected void dxbttCustomLinkSave_Click(object sender, EventArgs e)
        {
            Page.Validate("vgCLnk");
            if (!Page.IsValid) return;
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 customLinkID = CustomLinkID;
            String ownerRole = ConvertUtility.ConvertNullToEmptyString( dxcmbLnkOwnerRoles.Value );
            String displayContainerID =  ConvertUtility.ConvertNullToEmptyString( dxcmbDisplayContainer.Value );
            String internalName = dxtxtCLnkInternalName.Text.Trim();
            Int32 orderID = ConvertUtility.ConvertToInt32(dxtxtClnkOrder.Text.Trim(), 0);
            String targetUrl = dxtxtClnkTargetUrl.Text.Trim();
            String linkTarget =  ConvertUtility.ConvertNullToEmptyString( dxcmbClnkTarget.Value );
            String iconImage = dxtxtClnkIconImage.Text.Trim();
            DateTime startOnUTC = dxdtStartOnUTC.Date;
            DateTime expireOnUTC = dxdtExpireOnUTC.Date;
            lblClnkMsg.Text = "";
            if (customLinkID < 1)//new
            {
                targetUrl = "http://www.anritsu.com/";
                startOnUTC = DateTime.UtcNow.AddDays(1);
                expireOnUTC = DateTime.UtcNow.AddYears(50);
                customLinkID = Lib.CustomLink.CustomLink_MasterBLL.Insert(ownerRole, internalName, displayContainerID, orderID, targetUrl, linkTarget, iconImage
                  , String.Empty, startOnUTC, expireOnUTC, user.Email);
                Response.Redirect(String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinkDetails, KeyDef.QSKeys.Admin_CustomLinkID, customLinkID));
            }
            else
            {
                Lib.CustomLink.CustomLink_MasterBLL.Update(customLinkID, internalName, displayContainerID, orderID, targetUrl, linkTarget, iconImage
                 , String.Empty, startOnUTC, expireOnUTC, user.Email);
                lblClnkMsg.Text = "Updated successfully.";
            }
        }

        protected void dxbttCustomLinkDelete_Click(object sender, EventArgs e)
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            CustomLink_MasterBLL.Delete(customLinkID);
            Response.Redirect(KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinks);
        }
    }
}