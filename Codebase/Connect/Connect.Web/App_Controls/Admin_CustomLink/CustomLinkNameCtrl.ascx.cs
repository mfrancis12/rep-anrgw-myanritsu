﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.CustomLink;

using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinkNameCtrl : System.Web.UI.UserControl
    {
        public Int32 CustomLinkID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.Admin_CustomLinkID], 0);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Int32 customLinkID = CustomLinkID;
                if (customLinkID < 1)
                {
                    ltrCustomLinkName.Text = "New Custom Link";
                    return;
                }

                Lib.CustomLink.CustomLink_Master clm = Lib.CustomLink.CustomLink_MasterBLL.SelectByCustomLinkID(false, customLinkID);
                if (clm == null) throw new HttpException(404, "Page not found.");
                ltrCustomLinkName.Text = clm.LnkInternalName;
            }
        }
    }
}