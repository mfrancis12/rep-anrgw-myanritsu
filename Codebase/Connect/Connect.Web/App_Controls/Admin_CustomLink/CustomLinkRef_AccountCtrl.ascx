﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomLinkRef_AccountCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_CustomLink.CustomLinkRef_AccountCtrl" %>
    <script type="text/javascript">
        // <![CDATA[
        var postponedCallbackRequired = false;
        function OnOrgSearched(s, e) {
            
            if (dxcpCusLnkRefAccAdd.InCallback())
                postponedCallbackRequired = true;
            else {
                dxcpCusLnkRefAccAdd.PerformCallback();
            }
        }
        function OnEndCallback(s, e) {
            if (postponedCallbackRequired) {
                dxcpCusLnkRefAccAdd.PerformCallback();
                postponedCallbackRequired = false;
            }
        }
        // ]]>
    </script>
<div class="settingrow">
    <span class="settinglabel"><asp:Localize ID="lcalClnkOwnerRole" runat="server" Text="Link Owner Role" />:</span>
    <p style="padding-top: 5px;"><asp:Label ID="lblCLinkOwner" runat="server"></asp:Label></p>
</div>
<div class="settingrow">
    <span class="settinglabel"><asp:Localize ID="lcalCusLnkRefAccShowLinkMode" runat="server" Text="Show Link Mode (within link owner region)"></asp:Localize>:</span>
    <dx:ASPxButton ID="dxbttCusLnkRefAccShowAll" runat="server" Text="All Accounts" OnClick="dxbttCusLnkRefAccShowAll_Click" CausesValidation="false" Theme="AnritsuDevXTheme"></dx:ASPxButton>
    <dx:ASPxButton ID="dxbttCusLnkRefAccCustom" runat="server" Text="Configure accounts" OnClick="dxbttCusLnkRefAccCustom_Click" CausesValidation="false" Theme="AnritsuDevXTheme"></dx:ASPxButton>
</div>
    <div class="settingrow" id="divCusLnkRefAccConfigured" runat="server">
        <span class="settinglabel-15"><asp:Localize ID="lcalCusLnkRefAccConfiguredOrgs" runat="server" Text="Configured Organizations"></asp:Localize>:</span>
        <div style="padding-left: 170px;">
        <dx:ASPxGridView ID="dxgvCustomLinkRefAcc" runat="server" Width="550px" Theme="AnritsuDevXTheme" KeyFieldName="CustomLinkAccRefID" Visible="false" OnRowDeleting="dxgvCustomLinkRefAcc_RowDeleting">
            <Columns>
                <dx:GridViewDataColumn FieldName="CustomLinkAccRefID" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="AccName" VisibleIndex="2" ReadOnly="true" Caption="Organization"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="AccountID" VisibleIndex="3" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}" ></dx:GridViewDataDateColumn>
                <dx:GridViewCommandColumn VisibleIndex="100" Caption=" ">
                    <DeleteButton Visible="true" ></DeleteButton>
                </dx:GridViewCommandColumn>
            </Columns>
            <SettingsPager PageSize="30"></SettingsPager>
            <SettingsBehavior AllowGroup="false"  ConfirmDelete="true"  />
            <Settings ShowGroupPanel="false" ShowFilterRowMenu="false" ShowFilterRow="false" />
        </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxCallbackPanel runat="server" ID="dxcpCusLnkRefAccAdd" ClientInstanceName="dxcpCusLnkRefAccAdd" RenderMode="Table" HideContentOnCallback="true">
         <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                    <PanelCollection>
                         <dx:PanelContent ID="PanelContent3" runat="server">
                                <div class="settingrow">
                                    <div style="padding-left: 172px;">
                                        <dx:ASPxPanel ID="dxpnlCusLnkRefAccSearch" runat="server" DefaultButton="dxbttCusLnkRefAccSearch">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <table border="0">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <dx:ASPxTextBox ID="dxtxtCusLnkRefAccSearchKeyword" runat="server" Width="250px" Theme="AnritsuDevXTheme" HelpText="Search Organizations to enable link" ValidationSettings-ErrorDisplayMode="ImageWithTooltip">
                                                        <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorTextPosition="Right" ValidationGroup="vgCusLnkRefAccAdd"></ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td style="vertical-align: top;">
                                                    <dx:ASPxButton ID="dxbttCusLnkRefAccSearch" runat="server" Text="Search" OnClick="dxbttCusLnkRefAccSearch_Click"  ValidationGroup="vgCusLnkRefAccAdd">
                                                         <ClientSideEvents Click="OnOrgSearched" />
                                                    </dx:ASPxButton>
                                                    </td>
                                            </tr>
                                        </tbody>
                                    </table>     
                                                </dx:PanelContent>
                                            </PanelCollection>
                                            </dx:ASPxPanel>
                                    </div>
                                </div>
                                <div class="settingrow">
                                    <div style="padding-left: 172px;">
                                     <dx:ASPxGridView ID="dxgvCustomLinkRefAccSearchResults" ClientInstanceName="dxgvCustomLinkRefAccSearchResults" runat="server" Width="100%" Visible="false" 
                                    Theme="AnritsuDevXTheme" KeyFieldName="AccountID" AutoGenerateColumns="false" OnCustomCallback="dxgvCustomLinkRefAccSearchResults_CustomCallback">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="#" VisibleIndex="0">
                                            <DataItemTemplate>
                                                <dx:ASPxCheckBox ID="cbCheck" runat="server" AutoPostBack="false" OnLoad="cbCheck_Load" />
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="AccountID" VisibleIndex="1" Visible="false"></dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="DisplayLongName" Caption="Organizations / Accounts" VisibleIndex="20" ReadOnly="true">
                                            <FooterTemplate>
                                                <dx:ASPxButton ID="dxbttAddOrgs" runat="server" Text="Enable Link for selected accounts" OnClick="dxbttAddOrgs_Click"></dx:ASPxButton>
                                            </FooterTemplate>
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <SettingsPager PageSize="99999999"></SettingsPager>
                                </dx:ASPxGridView>
                                        </div>
                                </div>
                        </dx:PanelContent>                                         
                    </PanelCollection>
        </dx:ASPxCallbackPanel>
    <div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_CustomLink_CustomLinkRef_AccountCtrl" /></div>
