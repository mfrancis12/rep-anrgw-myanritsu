﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.CustomLink;

using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinkRef_AccountCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 CustomLinkID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.Admin_CustomLinkID], 0);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitCustomLinkRefAcc();
            if (!IsPostBack)
            {
                //InitCustomLinkRefAcc();
            }
            else
            {
                DataBindCustomLInkRefAccounts(true);
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private void InitCustomLinkRefAcc()
        {
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1)
            {
                this.Visible = false;
                return;
            }

            Lib.CustomLink.CustomLink_Master clm = Lib.CustomLink.CustomLink_MasterBLL.SelectByCustomLinkID(false, customLinkID);
            if (clm == null) throw new HttpException(404, "Page not found.");

            if (clm.DisplayContainer == null || !clm.DisplayContainer.EnableRefAcc)
            {
                this.Visible = false;
                return;
            }
            lblCLinkOwner.Text = clm.LnkOwnerRole;
            Boolean isShowForAll = CustomLink_AccountRefBLL.IsShowForAll(customLinkID, false);
            if (isShowForAll)
            {
                dxbttCusLnkRefAccShowAll.Enabled = false;
                dxbttCusLnkRefAccShowAll.ForeColor = System.Drawing.Color.Black;
                dxbttCusLnkRefAccShowAll.BackColor = System.Drawing.Color.White;
                dxbttCusLnkRefAccCustom.Enabled = true;
                dxbttCusLnkRefAccCustom.ForeColor = System.Drawing.Color.Gray;
                dxcpCusLnkRefAccAdd.Visible = false;
                divCusLnkRefAccConfigured.Visible = false;
            }
            else
            {
                dxbttCusLnkRefAccShowAll.Enabled = true;
                dxbttCusLnkRefAccShowAll.ForeColor = System.Drawing.Color.Gray;

                dxbttCusLnkRefAccCustom.Enabled = false;
                dxbttCusLnkRefAccCustom.ForeColor = System.Drawing.Color.Black;
                dxbttCusLnkRefAccCustom.BackColor = System.Drawing.Color.White;

                dxcpCusLnkRefAccAdd.Visible = true;
                divCusLnkRefAccConfigured.Visible = true;
                DataBindCustomLInkRefAccounts(true);

            }

            dxbttCusLnkRefAccShowAll.Visible = user.IsAdministrator;//only admin can enable links for all accounts
        }

        private void DataBindCustomLInkRefAccounts(bool refreshFromDB)
        {
            Int32 customLinkID = CustomLinkID;
            dxgvCustomLinkRefAcc.DataSource = Lib.CustomLink.CustomLink_AccountRefBLL.SelectByCustomLinkID(customLinkID, refreshFromDB);
            dxgvCustomLinkRefAcc.DataBind();
            dxgvCustomLinkRefAcc.Visible = true;
            DataBind();
        }

        protected void dxbttCusLnkRefAccShowAll_Click(object sender, EventArgs e)
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            CustomLink_AccountRefBLL.SetShowForAll(true, customLinkID, user.Email);
            InitCustomLinkRefAcc();
        }

        protected void dxbttCusLnkRefAccCustom_Click(object sender, EventArgs e)
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            CustomLink_AccountRefBLL.SetShowForAll(false, customLinkID, user.Email);
            InitCustomLinkRefAcc();
        }

       

        protected void dxgvCustomLinkRefAccSearchResults_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView dxgvCustomLinkRefAccSearchResults = (sender as ASPxGridView);
            if (e.ButtonID == "dxbttAddNewLnkRefAcc")
            {
                Guid accountID = ConvertUtility.ConvertToGuid(dxgvCustomLinkRefAccSearchResults.GetRowValues(e.VisibleIndex, "AccountID"), Guid.Empty);
                ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

                Int32 customLinkID = CustomLinkID;
                if (customLinkID < 1) return;

                CustomLink_AccountRefBLL.Insert(customLinkID, accountID, user.Email);
                CustomLink_MasterBLL.SelectAll(true);
            }
        }

        //protected void bttCusLnkRefAccSearch_Click(object sender, EventArgs e)
        //{
        //    String keyword = dxtxtCusLnkRefAccSearchKeyword.Text.Trim();
        //    if (keyword.IsNullOrEmptyString()) return;

        //    List<Lib.Account.AccountLookup> accounts = Lib.Account.AccountLookupBLL.SelectAll(keyword);
        //    dxgvCustomLinkRefAccSearchResults.DataSource = accounts;
        //    dxgvCustomLinkRefAccSearchResults.DataBind();
        //    dxgvCustomLinkRefAccSearchResults.Settings.ShowFooter = (accounts != null && accounts.Count > 0);
        //    dxgvCustomLinkRefAccSearchResults.Visible = true;
        //}

        protected void dxgvCustomLinkRefAcc_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Int32 customLinkAccRefID = ConvertUtility.ConvertToInt32(e.Keys[0], 0);
            CustomLink_AccountRefBLL.DeleteByCustomLinkAccRefID(customLinkAccRefID);
            CustomLink_MasterBLL.SelectAll(true);
            DataBindCustomLInkRefAccounts(true);

            e.Cancel = true;
        }

        protected void dxgvCustomLinkRefAccSearchResults_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            DataBindCustomLInkRefAccounts(true);
        }

        protected void cbCheck_Load(object sender, EventArgs e)
        {
            ASPxCheckBox cb = sender as ASPxCheckBox;

            GridViewDataItemTemplateContainer container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            cb.ClientInstanceName = String.Format("cbCheck{0}", container.VisibleIndex);
            cb.Checked = dxgvCustomLinkRefAccSearchResults.Selection.IsRowSelected(container.VisibleIndex);

            cb.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ dxgvCustomLinkRefAccSearchResults.SelectRowOnPage ({0}, s.GetChecked()); }}", container.VisibleIndex);
        }

        protected void dxbttAddOrgs_Click(object sender, EventArgs e)
        {
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;

            List<object> fieldValues = dxgvCustomLinkRefAccSearchResults.GetSelectedFieldValues(new string[] { "AccountID" });            

            
            foreach (object accountIDObj in fieldValues)
            {
                Guid accountID = ConvertUtility.ConvertToGuid(accountIDObj, Guid.Empty);
                CustomLink_AccountRefBLL.Insert(customLinkID, accountID, user.Email);
            }
            CustomLink_MasterBLL.SelectAll(true);
            DataBindCustomLInkRefAccounts(true);
            dxgvCustomLinkRefAccSearchResults.Visible = false;
        }

        protected void dxbttCusLnkRefAccSearch_Click(object sender, EventArgs e)
        {
            String keyword = dxtxtCusLnkRefAccSearchKeyword.Text.Trim();
            if (keyword.IsNullOrEmptyString()) return;

            List<Lib.Account.AccountLookup> accounts = Lib.Account.AccountLookupBLL.SelectAll(keyword);
            dxgvCustomLinkRefAccSearchResults.DataSource = accounts;
            dxgvCustomLinkRefAccSearchResults.DataBind();
            dxgvCustomLinkRefAccSearchResults.Settings.ShowFooter = (accounts != null && accounts.Count > 0);
            dxgvCustomLinkRefAccSearchResults.Visible = true;
        }

        protected void dxgvCustomLinkRefAccSearchResults_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
           // DataBindCustomLInkRefAccounts(true);
        }
    }
}