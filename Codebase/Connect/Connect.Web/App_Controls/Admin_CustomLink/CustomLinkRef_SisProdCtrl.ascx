﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomLinkRef_SisProdCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_CustomLink.CustomLinkRef_SisProdCtrl" %>
    <script type="text/javascript">
        // <![CDATA[
        var postponedCallbackRequired = false;
        function OnSisProdSearched(s, e) {

            if (dxcpCusLnkRefSisProdAdd.InCallback())
                postponedCallbackRequired = true;
            else {
                dxcpCusLnkRefSisProdAdd.PerformCallback();
            }
        }
        function OnEndCallback(s, e) {
            if (postponedCallbackRequired) {
                dxcpCusLnkRefSisProdAdd.PerformCallback();
                postponedCallbackRequired = false;
            }
        }
        // ]]>
    </script>
<div class="settingrow">
       <span class="settinglabel-15"><asp:Localize ID="lcalClnkOwnerRole" runat="server" Text="Link Owner Role" />:</span>
            <dx:ASPxLabel ID="dxlblCLinkOwner" runat="server"></dx:ASPxLabel>
    </div>
    <div class="settingrow">
        <span class="settinglabel-15"><asp:Localize ID="lcalCusLnkRefSisProdShowLinkMode" runat="server" Text="Show Link Mode"></asp:Localize>:</span>
        <dx:ASPxButton ID="dxbttCusLnkRefSisProdShowAll" runat="server" Text="All SIS Products" CausesValidation="false" Theme="AnritsuDevXTheme" OnClick="dxbttCusLnkRefSisProdShowAll_Click"></dx:ASPxButton>
        <dx:ASPxButton ID="dxbttCusLnkRefSisProdCustom" runat="server" Text="Configure SIS Products" CausesValidation="false" Theme="AnritsuDevXTheme" OnClick="dxbttCusLnkRefSisProdCustom_Click"></dx:ASPxButton>
    </div>
    <div class="settingrow" id="divCusLnkRefSisProdConfigured" runat="server">
        <span class="settinglabel-15"><asp:Localize ID="lcalCusLnkRefSisProdConfiguredSisProds" runat="server" Text="Configured SIS Products"></asp:Localize>:</span>
        <div style="padding-left: 170px;">
        <dx:ASPxGridView ID="dxgvCustomLinkRefSisProd" runat="server" Width="550px" Theme="AnritsuDevXTheme" KeyFieldName="RefSisProdID" Visible="false" 
            OnRowDeleting="dxgvCustomLinkRefSisProd_RowDeleting">
            <Columns>
                <dx:GridViewDataColumn FieldName="RefSisProdID" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SisPID" VisibleIndex="2" ReadOnly="true" Caption="SIS PID"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SisPID" VisibleIndex="3" Caption="SIS Product Name">
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="dxlblSisProductName" runat="server" Text='<%# GetSISProductName(Eval("SisPID")) %>'></dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}" ></dx:GridViewDataDateColumn>
                <dx:GridViewCommandColumn VisibleIndex="100" Caption=" ">
                    <DeleteButton Visible="true"></DeleteButton>
                </dx:GridViewCommandColumn>
            </Columns>
            <SettingsPager PageSize="30"></SettingsPager>
            <SettingsBehavior AllowGroup="false"/>
            <Settings ShowGroupPanel="false" ShowFilterRowMenu="false" ShowFilterRow="false" />
            <Styles Row-HorizontalAlign="Left"></Styles>
        </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxCallbackPanel runat="server" ID="dxcpCusLnkRefSisProdAdd" ClientInstanceName="dxcpCusLnkRefSisProdAdd" RenderMode="Table" HideContentOnCallback="true">
         <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                    <PanelCollection>
                         <dx:PanelContent ID="PanelContent3" runat="server">
                                <div class="settingrow">
                                    <div style="padding-left: 172px;">
                                        <dx:ASPxPanel ID="dxpnlCusLnkRefSisProdSearch" runat="server" DefaultButton="dxbttCusLnkRefSisProdSearch">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <table border="0">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <dx:ASPxTextBox ID="dxtxtCusLnkRefSisProdSearchKeyword" runat="server" Width="250px" Theme="AnritsuDevXTheme" HelpText="Search SIS Products to enable link" ValidationSettings-ErrorDisplayMode="ImageWithTooltip">
                                                        <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" ErrorTextPosition="Right" ValidationGroup="vgCusLnkRefSisProdAdd"></ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td style="vertical-align: top;">
                                                    <dx:ASPxButton ID="dxbttCusLnkRefSisProdSearch" runat="server" Text="Search" ValidationGroup="vgCusLnkRefSisProdAdd" OnClick="dxbttCusLnkRefSisProdSearch_Click">
                                                         <ClientSideEvents Click="OnSisProdSearched" />
                                                    </dx:ASPxButton>
                                                    </td>
                                            </tr>
                                        </tbody>
                                    </table>     
                                                </dx:PanelContent>
                                            </PanelCollection>
                                            </dx:ASPxPanel>
                                    </div>
                                </div>
                                <div class="settingrow">
                                    <div style="padding-left: 172px;">
                                     <dx:ASPxGridView ID="dxgvCustomLinkRefSisProdSearchResults" ClientInstanceName="dxgvCustomLinkRefSisProdSearchResults" runat="server" Width="100%" Visible="false" 
                                    Theme="AnritsuDevXTheme" KeyFieldName="PID" AutoGenerateColumns="false" OnCustomButtonCallback="dxgvCustomLinkRefSisProdSearchResults_CustomButtonCallback">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="#" VisibleIndex="0">
                                            <DataItemTemplate>
                                                <dx:ASPxCheckBox ID="cbCheck" runat="server" AutoPostBack="false" OnLoad="cbCheck_Load" />
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataColumn FieldName="PID" VisibleIndex="1" Caption="SIS PID"></dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="3" ReadOnly="true" Caption="Model Number"></dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn FieldName="ProductName" VisibleIndex="40" ReadOnly="true" Caption="ProductName">
                                            <FooterTemplate>
                                                <dx:ASPxButton ID="dxbttAddSisProds" runat="server" Text="Enable Link for selected SIS Products" OnClick="dxbttAddSisProds_Click"></dx:ASPxButton>
                                            </FooterTemplate>
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                    <SettingsPager PageSize="99999999"></SettingsPager>
                                </dx:ASPxGridView>
                                        </div>
                                </div>
                        </dx:PanelContent>                                         
                    </PanelCollection>
        </dx:ASPxCallbackPanel>
    <div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_CustomLink_CustomLinkRef_SisProdCtrl" /></div>
