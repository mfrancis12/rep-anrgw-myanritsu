﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.CustomLink;

using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinkRef_SisProdCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 CustomLinkID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.Admin_CustomLinkID], 0);

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitCustomLinkRef();
            if (!IsPostBack)
            {
                //InitCustomLinkRefAcc();
            }
            else
            {
                DataBindCustomLInkRefSisProducts(true);
            }

        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private void InitCustomLinkRef()
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1)
            {
                this.Visible = false;
                return;
            }

            Lib.CustomLink.CustomLink_Master clm = Lib.CustomLink.CustomLink_MasterBLL.SelectByCustomLinkID(false, customLinkID);
            if (clm == null) throw new HttpException(404, "Page not found.");

            if (clm.DisplayContainer == null || !clm.DisplayContainer.EnableRefSisProd)
            {
                this.Visible = false;
                return;
            }
            dxlblCLinkOwner.Text = clm.LnkOwnerRole;
            //List<CustomLink_RefSisProd> list = Lib.CustomLink.CustomLink_RefSisProdBLL.SelectByCustomLinkID(customLinkID, false);
            Boolean isShowForAll = CustomLink_RefSisProdBLL.IsShowForAll(customLinkID, false);
            if (isShowForAll)
            {
                dxbttCusLnkRefSisProdShowAll.Enabled = false;
                dxbttCusLnkRefSisProdShowAll.ForeColor = System.Drawing.Color.Black;
                dxbttCusLnkRefSisProdShowAll.BackColor = System.Drawing.Color.White;
                dxbttCusLnkRefSisProdCustom.Enabled = true;
                dxbttCusLnkRefSisProdCustom.ForeColor = System.Drawing.Color.Gray;
                dxcpCusLnkRefSisProdAdd.Visible = false;
                divCusLnkRefSisProdConfigured.Visible = false;
            }
            else
            {
                dxbttCusLnkRefSisProdShowAll.Enabled = true;
                dxbttCusLnkRefSisProdShowAll.ForeColor = System.Drawing.Color.Gray;

                dxbttCusLnkRefSisProdCustom.Enabled = false;
                dxbttCusLnkRefSisProdCustom.ForeColor = System.Drawing.Color.Black;
                dxbttCusLnkRefSisProdCustom.BackColor = System.Drawing.Color.White;

                dxcpCusLnkRefSisProdAdd.Visible = true;
                divCusLnkRefSisProdConfigured.Visible = true;
                DataBindCustomLInkRefSisProducts(false);

            }
        }

        private void DataBindCustomLInkRefSisProducts(bool refreshFromDB)
        {
            Int32 customLinkID = CustomLinkID;
            dxgvCustomLinkRefSisProd.DataSource = Lib.CustomLink.CustomLink_RefSisProdBLL.SelectByCustomLinkID(customLinkID, refreshFromDB);
            dxgvCustomLinkRefSisProd.DataBind();
            dxgvCustomLinkRefSisProd.Visible = true;
            DataBind();
        }

        protected void cbCheck_Load(object sender, EventArgs e)
        {
            ASPxCheckBox cb = sender as ASPxCheckBox;

            GridViewDataItemTemplateContainer container = cb.NamingContainer as GridViewDataItemTemplateContainer;
            cb.ClientInstanceName = String.Format("cbCheck{0}", container.VisibleIndex);
            cb.Checked = dxgvCustomLinkRefSisProdSearchResults.Selection.IsRowSelected(container.VisibleIndex);

            cb.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ dxgvCustomLinkRefSisProdSearchResults.SelectRowOnPage ({0}, s.GetChecked()); }}", container.VisibleIndex);
        }

        protected void dxbttCusLnkRefSisProdShowAll_Click(object sender, EventArgs e)
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            CustomLink_RefSisProdBLL.SetShowForAll(true, customLinkID, user.Email);
            InitCustomLinkRef();
        }

        protected void dxbttCusLnkRefSisProdCustom_Click(object sender, EventArgs e)
        {
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            CustomLink_RefSisProdBLL.SetShowForAll(false, customLinkID, user.Email);
            InitCustomLinkRef();
        }

        protected void dxgvCustomLinkRefSisProdSearchResults_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView dxgvCustomLinkRefSisProdSearchResults = (sender as ASPxGridView);
            if (e.ButtonID == "dxbttAddNewLnkRefSisProd")
            {
                Int32 sisPID = ConvertUtility.ConvertToInt32(dxgvCustomLinkRefSisProdSearchResults.GetRowValues(e.VisibleIndex, "PID"), 0);
                ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

                Int32 customLinkID = CustomLinkID;
                if (customLinkID < 1 || sisPID < 1) return;

                CustomLink_RefSisProdBLL.Insert(customLinkID, sisPID, user.Email);
                CustomLink_MasterBLL.SelectAll(true);
            }
        }

        protected void dxgvCustomLinkRefSisProd_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Int32 refSisProdID = ConvertUtility.ConvertToInt32(e.Keys[0], 0);
            CustomLink_RefSisProdBLL.DeleteByRefSisProdID(refSisProdID);

            DataBindCustomLInkRefSisProducts(true);

            e.Cancel = true;
        }

        protected void dxbttAddSisProds_Click(object sender, EventArgs e)
        {
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 customLinkID = CustomLinkID;
            if (customLinkID < 1) return;
            List<object> fieldValues = dxgvCustomLinkRefSisProdSearchResults.GetSelectedFieldValues(new string[] { "PID" });
            foreach (object sisPIDObj in fieldValues)
            {
                Int32 sisPID = ConvertUtility.ConvertToInt32(sisPIDObj, 0);
                if (sisPID < 1) continue;
                CustomLink_RefSisProdBLL.Insert(customLinkID, sisPID, user.Email);
            }
            DataBindCustomLInkRefSisProducts(true);
            dxgvCustomLinkRefSisProdSearchResults.Visible = false;
            CustomLink_MasterBLL.SelectAll(true);
        }

        protected void dxbttCusLnkRefSisProdSearch_Click(object sender, EventArgs e)
        {
            String keyword = dxtxtCusLnkRefSisProdSearchKeyword.Text.Trim();
            if (keyword.IsNullOrEmptyString()) return;
            
            Int32 sisLangID = GetSISLangID();
            List<PIDProductInfo> searchResults = PIDProductInfoBLL.SearchSISProducts(sisLangID, keyword);
            dxgvCustomLinkRefSisProdSearchResults.DataSource = searchResults;
            dxgvCustomLinkRefSisProdSearchResults.DataBind();
            dxgvCustomLinkRefSisProdSearchResults.Settings.ShowFooter = (searchResults != null && searchResults.Count > 0);
            dxgvCustomLinkRefSisProdSearchResults.Visible = true;
        }

        private Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        public String GetSISProductName(object sisPID)
        {
            Int32 pID = ConvertUtility.ConvertToInt32(sisPID, 0);
            if (pID < 1) return "-";
            Int32 sisLangID = GetSISLangID();
            PIDProductInfo pi = PIDProductInfoBLL.SelectByPID(pID, sisLangID);
            if (pi == null) return "-";
            return String.Format("{0} - [{1}]", pi.ProductName, pi.ModelNumber);
        }
    }
}