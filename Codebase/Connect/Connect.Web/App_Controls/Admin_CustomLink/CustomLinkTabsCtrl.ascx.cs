﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Cfg;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinkTabsCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 CustomLinkID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.Admin_CustomLinkID], 0);

            }
        }

        protected override void OnInit(EventArgs e)
        {
            InitTabs();
            base.OnInit(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                //InitTabs();
            }
        }

        public void InitTabs()
        {
            Int32 customLinkID = CustomLinkID;
            String query = String.Format("?{0}={1}", KeyDef.QSKeys.Admin_CustomLinkID, customLinkID);
            Tab tab;
            int tabIndex = 0;

            if (customLinkID > 0)
            {
                tab = new Tab(GetStaticResource("tbCustomLinkDetails.Text"), "tbCustomLinkDetails");
                tab.VisibleIndex = tabIndex++;
                tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinkDetails + query;
                tab.Enabled = true;
                dxCustomLinkAdminTabs.Tabs.Add(tab);

                Lib.CustomLink.CustomLink_Master clm = Lib.CustomLink.CustomLink_MasterBLL.SelectByCustomLinkID(false, customLinkID);
                if (clm == null) throw new HttpException(404, "Page not found.");

                if (clm.DisplayContainer == null) return;
                if (clm.DisplayContainer.EnableRefAcc)
                {
                    tab = new Tab(GetStaticResource("tbCustomLinkRefAcc.Text"), "tbCustomLinkRefAcc");
                    tab.VisibleIndex = tabIndex++;
                    tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinkRefAcc + query;
                    tab.Enabled = true;
                    dxCustomLinkAdminTabs.Tabs.Add(tab);
                }

                if (clm.DisplayContainer.EnableRefSisProd)
                {
                    tab = new Tab(GetStaticResource("tbCustomLinkRefSisProd.Text"), "tbCustomLinkRefSisProd");
                    tab.VisibleIndex = tabIndex++;
                    tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinkRefSisProd + query;
                    tab.Enabled = true;
                    dxCustomLinkAdminTabs.Tabs.Add(tab);
                }

                if (clm.DisplayContainer.EnableRefModel)
                {

                }
                
            }

            tab = new Tab(GetStaticResource("tbCustomLinkAddNew.Text"), "tbCustomLinkAddNew");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_CustomLinkPages.CustomLinkDetails;
            tab.Enabled = true;
            dxCustomLinkAdminTabs.Tabs.Add(tab);
            
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_CustomLinkTabsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}