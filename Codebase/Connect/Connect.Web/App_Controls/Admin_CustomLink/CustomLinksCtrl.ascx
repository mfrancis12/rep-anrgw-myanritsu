﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomLinksCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_CustomLink.CustomLinksCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisProdFilters" runat="server" ShowHeader="true" HeaderText="Custom Links">
    <div class="settingrow">
        <dx:ASPxGridView ID="dxgvCustomLinks" runat="server" Width="100%" Theme="AnritsuDevXTheme" KeyFieldName="CustomLinkID" DataSourceID="odsCustomLinks">
            <Columns>
                <dx:GridViewDataColumn FieldName="LnkOwnerRole" VisibleIndex="2" ReadOnly="true" GroupIndex="1"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="DisplayContainerID" VisibleIndex="3" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="LnkInternalName" VisibleIndex="4" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="StartOnUTC" Caption="Activate On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn FieldName="ExpireOnUTC" Caption="Expire On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewDataHyperLinkColumn FieldName="CustomLinkID" VisibleIndex="10" Caption=" " Width="30px" Settings-AllowAutoFilter="False">
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="/siteadmin/customlinkadmin/customlinkdetails?admclnkid={0}" Text="details"></PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
            </Columns>
            <SettingsPager PageSize="30"></SettingsPager>
            <SettingsBehavior AllowGroup="true" AutoExpandAllGroups="true" />
            <Settings ShowGroupPanel="true" ShowFilterRowMenu="true" ShowFilterRow="true" />
        </dx:ASPxGridView>
    </div>
    <div class="settingrow" style="text-align: right; padding-top: 20px;">
        <asp:HyperLink ID="hlAddNewLink" runat="server" NavigateUrl="/siteadmin/customlinkadmin/customlinkdetails" Text="Add New Custom Link" Font-Bold="true"></asp:HyperLink>
    </div>
    <asp:ObjectDataSource ID="odsCustomLinks" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_CustomLinks.ODS_CustomLinks" SelectMethod="SelectByLinkAdminRoles">
        <SelectParameters>
            <asp:QueryStringParameter Name="refreshFromDB" QueryStringField="refresh" DbType="Boolean" DefaultValue="true" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_CustomLink_CustomLinksCtrl" />
    </div>
</anrui:GlobalWebBoxedPanel>
