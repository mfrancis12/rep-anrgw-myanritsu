﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.CustomLink;

using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;


namespace Anritsu.Connect.Web.App_Controls.Admin_CustomLink
{
    public partial class CustomLinksCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        
    }
}