﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistPortalAdmin_DocTypeFilterDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_DistPortal.DistPortalAdmin_DocTypeFilterDetailCtrl"  %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisDocTypeFilter" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,pnlContainerSisDocTypeFilter.HeaderText %>">

    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalSisDocTypeFilterFilterName" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lcalSisDocTypeFilterFilterName.Text %>" />:</span>
        <dx:ASPxTextBox ID="dxtxtSisDocTypeFilterFilterName" runat="server" Width="300px" MaxLength="30" ValidateRequestMode="Enabled"  HelpText="Maximum 30 characters.">
            <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" RegularExpression-ValidationExpression='^[^<>%]*$' RegularExpression-ErrorText="Filter Name should not contain <,> and % " ErrorDisplayMode="ImageWithText" ValidationGroup="vgSisDocTypeFilter" ValidateOnLeave="true"></ValidationSettings>
        </dx:ASPxTextBox>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalSisDocTypeFilterFilterOwnerRoles" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lcalSisDocTypeFilterFilterOwnerRoles.Text %>" />:</span>
        <dx:ASPxRadioButtonList runat="server" ID="dxrdolstSisDocTypeFilterFilterOwnerRoles" ReadOnlyStyle-BackColor="WhiteSmoke">
            <Items>
                <dx:ListEditItem Text="Distributor EMEA Admin" Value="adm-sis-emea" />
                <dx:ListEditItem Text="Distributor US Admin" Value="adm-sis-us" />
            </Items>
            <ValidationSettings CausesValidation="true" ValidationGroup="vgSisDocTypeFilter" ErrorDisplayMode="ImageWithText">
                <RequiredField IsRequired="true" ErrorText="required!" />
            </ValidationSettings>
        </dx:ASPxRadioButtonList>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalSisDocTypeFilterCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lcalSisDocTypeFilterCreatedBy.Text %>" />:</span>
        <dx:ASPxLabel ID="dxlblSisDocTypeFilterCreatedBy" runat="server"></dx:ASPxLabel>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalSisDocTypeFilterCreatedOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lcalSisDocTypeFilterCreatedOnUTC.Text %>" />:</span>
        <dx:ASPxLabel ID="dxlblSisDocTypeFilterCreatedOnUTC" runat="server"></dx:ASPxLabel>
    </div>
    <div class="settingrow">
        <span class="msg">
            <asp:Label ID="lblMsg" runat="server"></asp:Label></span>
    </div>
    <div class="settingrow">
        <dx:ASPxButton ID="dxbttSisDocTypeFilterSave" SkinID="button" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,bttSisDocTypeFilterSave.Text %>" OnClick="dxbttSisDocTypeFilterSave_Click" CausesValidation="true" ValidationGroup="vgSisDocTypeFilter">
        </dx:ASPxButton>
        <dx:ASPxButton ID="dxbttSisDocTypeFilterDelete" SkinID="button" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,bttSisDocTypeFilterDelete.Text %>"
            CausesValidation="false" OnClick="dxbttSisDocTypeFilterDelete_Click">
            <ClientSideEvents Click="function(s,e) { e.processOnServer = confirm('Are you sure?'); }" />
        </dx:ASPxButton>
    </div>
</anrui:GlobalWebBoxedPanel>
<div id="divNotAuthorizedMsg" runat="server" class="settingrow" style="text-align: center;" visible="false">
    <span class="msg">
        <asp:Label ID="lblNotAuthorizedMsg" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lblNotAuthorizedMsg.Text %>"></asp:Label></span>
    <a href="/SiteAdmin/DistPortalAdmin/SIS_DocTypeFilterList" title="">SIS Document Types Filter List</a>
</div>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisDocTypeFilterItems" runat="server" ShowHeader="true"
    HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,pnlContainerSisDocTypeFilterItems.HeaderText %>" Visible="false">
    <script type="text/javascript">
        function gvSisDocTypeFilterItems_EndCallback(s, e) {
            if (s.cpShowDeleteConfirmBox)
                pcConfirm.Show();
        }

        function Yes_Click() {
            pcConfirm.Hide();
            gvSisDocTypeFilterItems.DeleteRow(gvSisDocTypeFilterItems.cpRowIndex);
        }

        function No_Click() {
            pcConfirm.Hide()
        }
    </script>
    <div class="settingrow">
        <fieldset>
            <legend><asp:Localize ID="lcalSisDocTypesLegend" runat="server" Text="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,lcalSisDocTypesLegend.Text %>"></asp:Localize></legend>
            <label class="msg">
                <asp:Label ID="lblAddToDocTypeFilter" runat="server"></asp:Label></label>
            <div class="settingrow">
                <dx:ASPxComboBox ID="cmbSISDocTypes" runat="server" CssClass="btnInline" Width="500px" IncrementalFilteringMode="Contains" ValidationSettings-CausesValidation="false">
                </dx:ASPxComboBox>
                <dx:ASPxButton ID="dxbttAddDocTypeFilterItem" runat="server" Text="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,dxbttAddDocTypeFilterItem.Text %>" Theme="AnritsuDevXTheme" OnClick="dxbttAddDocTypeFilterItem_Click" CausesValidation="false"></dx:ASPxButton>
            </div>
        </fieldset>
    </div>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvSisDocTypeFilterItems"  ClientInstanceName="gvSisDocTypeFilterItems" runat="server" AutoGenerateColumns="false" KeyFieldName="DocTypeFilterItemID" Width="100%"
            Theme="AnritsuDevXTheme" OnCustomButtonCallback="gvSisDocTypeFilterItems_CustomButtonCallback" OnInit="gvSisDocTypeFilterItems_Init" OnRowDeleting="gvSisDocTypeFilterItems_RowDeleting">
            <ClientSideEvents EndCallback="gvSisDocTypeFilterItems_EndCallback" />
            <Columns>
                <dx:GridViewDataColumn FieldName="SIS_DocTypeID" VisibleIndex="1" ReadOnly="true" Width="50px" Caption="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,gvSisDocTypeFilterItems.SIS_DocTypeID.Caption %>"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SIS_DocTypeFullName" VisibleIndex="10" ReadOnly="true" Caption="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,gvSisDocTypeFilterItems.SIS_DocTypeName.Caption %>"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="90" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}" Caption="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl,gvSisDocTypeFilterItems.CreatedOnUTC.Caption %>"></dx:GridViewDataDateColumn>
                <dx:GridViewCommandColumn VisibleIndex="999" Width="100px" ButtonType="Link" Caption=" ">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="dxbttDocTypeFilterItemDelete" Text="delete"></dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior AllowSort="true" />
        </dx:ASPxGridView>
        <dx:ASPxPopupControl ID="pcConfirm" runat="server" ClientInstanceName="pcConfirm" CloseAction="CloseButton"
            Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            HeaderText="<% $Resources: common, ConfirmBoxHeaderText %>">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Localize ID="lcalDeleteRow" runat="server" Text="<% $Resources: common, ConfirmDeleteRegMsg %>"></asp:Localize>

                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a href="javascript:Yes_Click()">
                                    <asp:Localize ID="lcalYesMsg" runat="server" Text="<% $Resources: common, YesConfirmMsg %>"></asp:Localize>
                                </a>
                            </td>
                            <td align="center">
                                <a href="javascript:No_Click()">
                                    <asp:Localize ID="lcalNoMsg" runat="server" Text="<% $Resources: common, NoConfirmMsg %>"></asp:Localize>
                                </a>
                            </td>
                        </tr>
                    </table>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </div>
    <asp:ObjectDataSource ID="odsSISDocTypes" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS.ODS_SISProducts"
        SelectMethod="SelectAll" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheKeyDependency="odsSISProdsCKD"></asp:ObjectDataSource>
</anrui:GlobalWebBoxedPanel>
<div class="settingrow">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DistPortalAdmin_DocTypeFilterDetailCtrl" EnableViewState="false" />
</div>
