﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;


namespace Anritsu.Connect.Web.App_Controls.Admin_DistPortal
{
    public partial class DistPortalAdmin_DocTypeFilterDetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 DocTypeFilterID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.SIS_DocTypeFilterID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                BindSisDocumentTypes();
                BindSisDocTypeFilterInfo();
            }
            BindDocuments();
        }

        private void BindSisDocumentTypes()
        {
            Lib.PID.PIDDocumentTypeSortByCategoryAndDocTypeNames sorter = new PIDDocumentTypeSortByCategoryAndDocTypeNames();
            List<PIDDocumentType> list = Lib.PID.PIDDocumentTypeBLL.SelectAll(SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant());
            list.Sort(sorter);

            cmbSISDocTypes.DataSource = list;
            cmbSISDocTypes.TextField = "CategoryAndDocumentTypeName";
            cmbSISDocTypes.ValueField = "DocumentTypeID";
            cmbSISDocTypes.DataBind();
        }

        private void BindDocuments()
        {
            Int32 docTypeFilterID = DocTypeFilterID;
            dxbttSisDocTypeFilterDelete.Visible = (docTypeFilterID < 1) ? false : true;
            if (docTypeFilterID < 1)
            {
                dxrdolstSisDocTypeFilterFilterOwnerRoles.ReadOnly = false;
                return;
            }

            Int32 sisLangID = GetSISLangID();
            PIDDocTypeFilter docTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(DocTypeFilterID, sisLangID);
            gvSisDocTypeFilterItems.DataSource = docTypeFilter.FilterItems;
            gvSisDocTypeFilterItems.DataBind();
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        private void BindSisDocTypeFilterInfo()
        {
            Int32 docTypeFilterID = DocTypeFilterID;
            if (docTypeFilterID < 1)
            {
                dxrdolstSisDocTypeFilterFilterOwnerRoles.ReadOnly = false;
                return;
            }
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            Int32 sisLangID = GetSISLangID();
            PIDDocTypeFilter docTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(DocTypeFilterID, sisLangID);
            if (docTypeFilter == null)
                throw new HttpException(404, "Page not found.");

            if (!LoginUtility.IsAdminRole(docTypeFilter.DocTypeFilterOwnerRole))
            {
                divNotAuthorizedMsg.Visible = true;
                pnlContainerSisDocTypeFilterItems.Visible = false;
                return;
            }

            dxrdolstSisDocTypeFilterFilterOwnerRoles.ReadOnly = true;
            dxtxtSisDocTypeFilterFilterName.Text = docTypeFilter.DocTypeFilterName.Trim();
            dxrdolstSisDocTypeFilterFilterOwnerRoles.Items.FindByValue(docTypeFilter.DocTypeFilterOwnerRole).Selected = true;
            dxlblSisDocTypeFilterCreatedBy.Text = docTypeFilter.CreatedBy;
            dxlblSisDocTypeFilterCreatedOnUTC.Text = docTypeFilter.CreatedOnUTC.ToString("d");

            gvSisDocTypeFilterItems.DataSource = docTypeFilter.FilterItems;
            gvSisDocTypeFilterItems.DataBind();
            pnlContainerSisDocTypeFilterItems.Visible = true;
        }

        protected void gvSisDocTypeFilterItems_Init(object sender, EventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.JSProperties["cpShowDeleteConfirmBox"] = false;
        }

        protected void gvSisDocTypeFilterItems_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            try
            {
                Int32 docTypeFilterItemID = ConvertUtility.ConvertToInt32(e.Keys[gvSisDocTypeFilterItems.KeyFieldName], 0);

                PIDDocTypeFilterItemBLL.DeleteByDocTypeFilterItemID(docTypeFilterItemID);
                try { Cache.Remove("odsSISProdsCKD"); }
                catch { }
                Int32 docTypeFilterID = DocTypeFilterID;
                if (docTypeFilterID < 1) throw new HttpException(404, "Page not found.");


                Int32 sisLangID = GetSISLangID();
                PIDDocTypeFilter DocTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(DocTypeFilterID, sisLangID);
                if (DocTypeFilter == null || !LoginUtility.IsAdminRole(DocTypeFilter.DocTypeFilterOwnerRole))
                    throw new HttpException(404, "Page not found.");
                gvSisDocTypeFilterItems.DataSource = DocTypeFilter.FilterItems;
                gvSisDocTypeFilterItems.DataBind();
                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void dxbttSisDocTypeFilterSave_Click(object sender, EventArgs e)
        {
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Page.Validate("vgSisDocTypeFilter");
            String filterName = dxtxtSisDocTypeFilterFilterName.Text.Trim();
            String ownerRole = dxrdolstSisDocTypeFilterFilterOwnerRoles.SelectedItem.Value.ToString();

            Int32 docTypeFilterID = DocTypeFilterID;
            Int32 sisLangID = GetSISLangID();

            if (docTypeFilterID < 1) //new
            {
                docTypeFilterID = Lib.PID.PIDDocTypeFilterBLL.Insert(filterName, ownerRole, user.Email);
                String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_DistPortalPages.DocTypeFilterDetails, KeyDef.QSKeys.SIS_DocTypeFilterID, docTypeFilterID);
                WebUtility.HttpRedirect(this, url);
            }
            else
            {
                PIDDocTypeFilter docTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(docTypeFilterID, sisLangID);
                if (docTypeFilter == null) throw new HttpException(404, "Page not found.");
                Lib.PID.PIDDocTypeFilterBLL.Update(docTypeFilterID, filterName);
                lblMsg.Text = "Filter updated successfully.";
            }


        }

        protected void dxbttAddDocTypeFilterItem_Click(object sender, EventArgs e)
        {
            lblAddToDocTypeFilter.Text = "";
            if (cmbSISDocTypes.SelectedItem == null)
            {
                lblAddToDocTypeFilter.Text = "Select SIS product first!";
                return;
            }

            Int32 sisDocTypeID = ConvertUtility.ConvertToInt32(cmbSISDocTypes.SelectedItem.Value, 0);
            Int32 docTypeFilterID = DocTypeFilterID;
            if (docTypeFilterID < 1) throw new HttpException(404, "Page not found.");
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Lib.PID.PIDDocTypeFilterItemBLL.Insert(DocTypeFilterID, sisDocTypeID, user.Email);
            Int32 sisLangID = GetSISLangID();
            PIDDocTypeFilter docTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(docTypeFilterID, sisLangID);
            if (docTypeFilter == null || !LoginUtility.IsAdminRole(docTypeFilter.DocTypeFilterOwnerRole))
                throw new HttpException(404, "Page not found.");
            gvSisDocTypeFilterItems.DataSource = docTypeFilter.FilterItems;
            gvSisDocTypeFilterItems.DataBind();
        }

        protected void gvSisDocTypeFilterItems_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gvSisDocTypeFilterItems = (sender as ASPxGridView);
            if (e.ButtonID == "dxbttDocTypeFilterItemDelete")
            {
                Int32 docTypeFilterItemID = ConvertUtility.ConvertToInt32(gvSisDocTypeFilterItems.GetRowValues(e.VisibleIndex, "DocTypeFilterItemID"), 0);
                gvSisDocTypeFilterItems.JSProperties["cpRowIndex"] = e.VisibleIndex;
                gvSisDocTypeFilterItems.JSProperties["cpShowDeleteConfirmBox"] = true;
                //PIDDocTypeFilterItemBLL.DeleteByDocTypeFilterItemID(DocTypeFilterItemID);

                //Int32 DocTypeFilterID = DocTypeFilterID;
                //if (DocTypeFilterID < 1) throw new HttpException(404, "Page not found.");


                //Int32 sisLangID = GetSISLangID();
                //PIDDocTypeFilter DocTypeFilter = PIDDocTypeFilterBLL.SelectByFilterID(DocTypeFilterID, sisLangID);
                //if (DocTypeFilter == null || !LoginUtility.IsAdminRole(DocTypeFilter.FilterOwnerRole))
                //    throw new HttpException(404, "Page not found.");
                //gvSisDocTypeFilterItems.DataSource = DocTypeFilter.FilterItems;
                //gvSisDocTypeFilterItems.DataBind();
            }
        }

        protected void dxbttSisDocTypeFilterDelete_Click(object sender, EventArgs e)
        {
            Int32 docTypeFilterID = DocTypeFilterID;
            if (docTypeFilterID < 1) throw new HttpException(404, "Page not found.");
            PIDDocTypeFilterBLL.Delete(docTypeFilterID);
            WebUtility.HttpRedirect(this, KeyDef.UrlList.SiteAdminPages.Admin_DistPortalPages.DocTypeFilterList);
        }
    }
}