﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistPortalAdmin_DocTypeFilterListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_DistPortal.DistPortalAdmin_DocTypeFilterListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisDocTypeFilters" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_DocTypeFilterListCtrl,pnlContainerSisDocTypeFilters.HeaderText %>">
    <div class="settingrow">
        <dx:ASPxGridView ID="gvSisDocTypeFilters" runat="server" DataSourceID="odsLookupDistPortalDocTypeFilters" Width="100%" Theme="AnritsuDevXTheme">
            <Columns>
                <dx:GridViewDataColumn FieldName="DocTypeFilterOwnerRole" VisibleIndex="1" ReadOnly="true" Width="150px" GroupIndex="1"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="DocTypeFilterName" VisibleIndex="2" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="CreatedBy" VisibleIndex="5" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewDataHyperLinkColumn FieldName="DocTypeFilterID" VisibleIndex="10" Caption=" " Width="30px">
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="/siteadmin/distportaladmin/sis_DocTypeFilterdetails?dtfid={0}" Text="edit"></PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
            </Columns>
            <SettingsBehavior AllowGroup="true" AutoExpandAllGroups="true" />
            <Settings ShowGroupPanel="true" ShowFilterRowMenu="true" ShowFilterRow="true" />
        </dx:ASPxGridView>
    </div>
    <div class="settingrow" style="text-align: right; padding-top: 20px;">
        <asp:HyperLink ID="hlSisDocTypeFilterAddNew" SkinID="blueit" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_doctypefilterdetails" Text="Add New Document Type Filter"></asp:HyperLink>
    </div>

    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DistPortalAdmin_DocTypeFilterListCtrl" />
    </div>
    <asp:ObjectDataSource ID="odsLookupDistPortalDocTypeFilters" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal.ODS_SISDocTypeFilterLookup"
        SelectMethod="SelectFilters"></asp:ObjectDataSource>
</anrui:GlobalWebBoxedPanel>
