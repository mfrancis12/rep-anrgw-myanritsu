﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistPortalAdmin_ProdFilterDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_DistPortal.DistPortalAdmin_ProdFilterDetailCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisProdFilter" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,pnlContainerSisProdFilter.HeaderText %>">

    <div class="settingrow">
        <span class="settinglabel-15">
            <asp:Localize ID="lcalSisProdFilterFilterName" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,lcalSisProdFilterFilterName.Text %>" />:</span>
        <dx:ASPxTextBox ID="dxtxtSisProdFilterFilterName" runat="server" Width="300px" MaxLength="30" ValidateRequestMode="Enabled" HelpText="Maximum 30 characters.">
            <ValidationSettings RequiredField-IsRequired="true" RequiredField-ErrorText="required!" RegularExpression-ValidationExpression='^[^<>%]*$' RegularExpression-ErrorText="Filter Name should not contain <,> and % " ErrorDisplayMode="ImageWithText" ValidationGroup="vgSisProdFilter" ValidateOnLeave="true"></ValidationSettings>
        </dx:ASPxTextBox>
    </div>
    <div class="settingrow">
        <span class="settinglabel-15">
            <asp:Localize ID="lcalSisProdFilterFilterOwnerRoles" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,lcalSisProdFilterFilterOwnerRoles.Text %>" />:</span>
        <dx:ASPxRadioButtonList runat="server" ID="dxrdolstSisProdFilterFilterOwnerRoles" ReadOnlyStyle-BackColor="WhiteSmoke">
            <Items>
                <dx:ListEditItem Text="Distributor EMEA Admin" Value="adm-sis-emea" />
                <dx:ListEditItem Text="Distributor US Admin" Value="adm-sis-us" />
            </Items>
            <ValidationSettings CausesValidation="true" ValidationGroup="vgSisProdFilter" ErrorDisplayMode="ImageWithText">
                <RequiredField IsRequired="true" ErrorText="required!" />
            </ValidationSettings>
        </dx:ASPxRadioButtonList>
    </div>
    <div class="settingrow">
        <span class="settinglabel-15">
            <asp:Localize ID="lcalSisProdFilterCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,lcalSisProdFilterCreatedBy.Text %>" />:</span>
        <dx:ASPxLabel ID="dxlblSisProdFilterCreatedBy" runat="server"></dx:ASPxLabel>
    </div>
    <div class="settingrow">
        <span class="settinglabel-15">
            <asp:Localize ID="lcalSisProdFilterCreatedOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,lcalSisProdFilterCreatedOnUTC.Text %>" />:</span>
        <dx:ASPxLabel ID="dxlblSisProdFilterCreatedOnUTC" runat="server"></dx:ASPxLabel>
    </div>
    <div class="settingrow">
        <span class="msg">
            <asp:Label ID="lblMsg" runat="server"></asp:Label></span>
    </div>
    <div class="settingrow">
        <span class="settinglabel-15">&nbsp;</span>
        <dx:ASPxButton ID="dxbttSisProdFilterSave" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,bttSisProdFilterSave.Text %>" OnClick="dxbttSisProdFilterSave_Click" CausesValidation="true" ValidationGroup="vgSisProdFilter">
        </dx:ASPxButton>
        <dx:ASPxButton ID="dxbttSisProdFilterDelete" Theme="AnritsuDevXTheme" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,bttSisProdFilterDelete.Text %>"
            CausesValidation="false" OnClick="dxbttSisProdFilterDelete_Click">
            <ClientSideEvents Click="function(s,e) { e.processOnServer = confirm('Are you sure?'); }" />
        </dx:ASPxButton>
    </div>
</anrui:GlobalWebBoxedPanel>
<div id="divNotAuthorizedMsg" runat="server" class="settingrow" style="text-align: center;" visible="false">
    <span class="msg">
        <asp:Label ID="lblNotAuthorizedMsg" runat="server" Text="<%$Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,lblNotAuthorizedMsg.Text %>"></asp:Label></span>
    <a href="/SiteAdmin/DistPortalAdmin/SIS_ProdFilterList" title="">SIS Product Filter List</a>
</div>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisProdFilterItems" runat="server" ShowHeader="true"
    HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl,pnlContainerSisProdFilterItems.HeaderText %>">
    <script type="text/javascript">
        function gvSisProdFilterItems_EndCallback(s, e) {
            if (s.cpShowDeleteConfirmBox)
                pcConfirm.Show();
        }

        function Yes_Click() {
            pcConfirm.Hide();
            gvSisProdFilterItems.DeleteRow(gvSisProdFilterItems.cpRowIndex);
        }

        function No_Click() {
            pcConfirm.Hide()
        }
    </script>
    <div class="settingrow">
        <fieldset>
            <legend>Add Product to Filter</legend>
            <label class="msg">
                <asp:Label ID="lblAddToProdFilter" runat="server"></asp:Label></label>
            <div class="settingrow">
                <dx:ASPxComboBox ID="cmbSISProducts" runat="server" CssClass="btnInline" DataSourceID="odsSISProds" TextField="ModelNumber" ValueField="PID" Width="500px"
                    IncrementalFilteringMode="Contains" ValidationSettings-CausesValidation="false">
                </dx:ASPxComboBox>
                <dx:ASPxButton ID="dxbttAddProdFilterItem" runat="server" Text="Add To Product Filter" Theme="AnritsuDevXTheme" OnClick="dxbttAddProdFilterItem_Click" CausesValidation="false"></dx:ASPxButton>
            </div>
        </fieldset>
    </div>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvSisProdFilterItems"  ClientInstanceName="gvSisProdFilterItems" runat="server" AutoGenerateColumns="false" KeyFieldName="ProdFilterItemID" Width="100%"
            Theme="AnritsuDevXTheme" OnCustomButtonCallback="gvSisProdFilterItems_CustomButtonCallback" OnInit="gvSisProdFilterItems_Init" OnRowDeleting="gvSisProdFilterItems_RowDeleting">
            <ClientSideEvents EndCallback="gvSisProdFilterItems_EndCallback" />
            <Columns>
                <dx:GridViewDataColumn FieldName="SIS_PID" VisibleIndex="1" ReadOnly="true" Width="50px"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SIS_ModelNumber" VisibleIndex="5" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SIS_ProductName" VisibleIndex="10" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="90" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewCommandColumn VisibleIndex="999" Width="100px" ButtonType="Link" Caption=" ">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="dxbttProdFilterItemDelete" Text="delete"></dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
            </Columns>
            <SettingsBehavior AllowSort="true"  />
        </dx:ASPxGridView>
        <dx:ASPxPopupControl ID="pcConfirm" runat="server" ClientInstanceName="pcConfirm" CloseAction="CloseButton"
            Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            HeaderText="<% $Resources: common, ConfirmBoxHeaderText %>">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <table width="100%">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Localize ID="lcalDeleteRow" runat="server" Text="<% $Resources: common, ConfirmDeleteRegMsg %>"></asp:Localize>

                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a href="javascript:Yes_Click()">
                                    <asp:Localize ID="lcalYesMsg" runat="server" Text="<% $Resources: common, YesConfirmMsg %>"></asp:Localize>
                                </a>
                            </td>
                            <td align="center">
                                <a href="javascript:No_Click()">
                                    <asp:Localize ID="lcalNoMsg" runat="server" Text="<% $Resources: common, NoConfirmMsg %>"></asp:Localize>
                                </a>
                            </td>
                        </tr>
                    </table>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </div>
    <asp:ObjectDataSource ID="odsSISProds" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS.ODS_SISProducts"
        SelectMethod="SelectAll" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheKeyDependency="odsSISProdsCKD"></asp:ObjectDataSource>
</anrui:GlobalWebBoxedPanel>
<div class="settingrow">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DistPortalAdmin_ProdFilterDetailCtrl" EnableViewState="false" />
</div>
