﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_DistPortal
{
    public partial class DistPortalAdmin_ProdFilterDetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 ProdFilterID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.SIS_ProductFilterID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                BindSisProductFilterInfo();
            }

            BindSisFilterList();
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        private void BindSisFilterList()
        {
            Int32 prodFilterID = ProdFilterID;
            dxbttSisProdFilterDelete.Visible = (prodFilterID < 1) ? false : true;
            if (prodFilterID < 1)
            {
                dxrdolstSisProdFilterFilterOwnerRoles.ReadOnly = false;
                return;
            }

            Int32 sisLangID = GetSISLangID();
            PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
            gvSisProdFilterItems.DataSource = prodFilter.FilterItems;
            gvSisProdFilterItems.DataBind();
        }

       
        private void BindSisProductFilterInfo()
        {
            Int32 prodFilterID = ProdFilterID;
            if (prodFilterID < 1)
            {
                dxrdolstSisProdFilterFilterOwnerRoles.ReadOnly = false;
                return;
            }
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            Int32 sisLangID = GetSISLangID();
            PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
            if (prodFilter == null)
                throw new HttpException(404, "Page not found.");

            if (!LoginUtility.IsAdminRole(prodFilter.FilterOwnerRole))
            {
                divNotAuthorizedMsg.Visible = true;
                pnlContainerSisProdFilterItems.Visible = false;
                return;
            }

            dxrdolstSisProdFilterFilterOwnerRoles.ReadOnly = true;
            dxtxtSisProdFilterFilterName.Text = prodFilter.FilterName.Trim();
            dxrdolstSisProdFilterFilterOwnerRoles.Items.FindByValue(prodFilter.FilterOwnerRole).Selected = true;
            dxlblSisProdFilterCreatedBy.Text = prodFilter.CreatedBy;
            dxlblSisProdFilterCreatedOnUTC.Text = prodFilter.CreatedOnUTC.ToString("d");

            gvSisProdFilterItems.DataSource = prodFilter.FilterItems;
            gvSisProdFilterItems.DataBind();
        }

        protected void gvSisProdFilterItems_Init(object sender, EventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.JSProperties["cpShowDeleteConfirmBox"] = false;
        }

        protected void gvSisProdFilterItems_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            try
            {
                Int32 prodFilterItemID = ConvertUtility.ConvertToInt32(e.Keys[gvSisProdFilterItems.KeyFieldName], 0);

                PIDProdFilterItemBLL.DeleteByProdFilterItemID(prodFilterItemID);
                try { Cache.Remove("odsSISProdsCKD"); }
                catch { }
                Int32 prodFilterID = ProdFilterID;
                if (prodFilterID < 1) throw new HttpException(404, "Page not found.");


                Int32 sisLangID = GetSISLangID();
                PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
                if (prodFilter == null || !LoginUtility.IsAdminRole(prodFilter.FilterOwnerRole))
                    throw new HttpException(404, "Page not found.");
                gvSisProdFilterItems.DataSource = prodFilter.FilterItems;
                gvSisProdFilterItems.DataBind();
                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void dxbttSisProdFilterSave_Click(object sender, EventArgs e)
        {
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Page.Validate("vgSisProdFilter");
            String filterName = dxtxtSisProdFilterFilterName.Text.Trim();
            String ownerRole = dxrdolstSisProdFilterFilterOwnerRoles.SelectedItem.Value.ToString();

            Int32 prodFilterID = ProdFilterID;
            Int32 sisLangID = GetSISLangID();

            if (prodFilterID < 1) //new
            {
                prodFilterID = Lib.PID.PIDProdFilterBLL.Insert(filterName, ownerRole, user.Email);
                String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_DistPortalPages.ProdFilterDetails, KeyDef.QSKeys.SIS_ProductFilterID, prodFilterID);
                WebUtility.HttpRedirect(this, url);
            }
            else
            {
                PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
                if (prodFilter == null) throw new HttpException(404, "Page not found.");
                Lib.PID.PIDProdFilterBLL.Update(prodFilterID, filterName);
                lblMsg.Text = "Filter updated successfully.";
            }


        }


        protected void dxbttAddProdFilterItem_Click(object sender, EventArgs e)
        {
            lblAddToProdFilter.Text = "";
            if (cmbSISProducts.SelectedItem == null)
            {
                lblAddToProdFilter.Text = "Select SIS product first!";
                return;
            }

            Int32 sisPID = ConvertUtility.ConvertToInt32(cmbSISProducts.SelectedItem.Value, 0);
            Int32 prodFilterID = ProdFilterID;
            if (prodFilterID < 1) throw new HttpException(404, "Page not found.");
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Lib.PID.PIDProdFilterItemBLL.Insert(prodFilterID, sisPID, user.Email);
            Int32 sisLangID = GetSISLangID();
            PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
            if (prodFilter == null || !LoginUtility.IsAdminRole(prodFilter.FilterOwnerRole))
                throw new HttpException(404, "Page not found.");
            gvSisProdFilterItems.DataSource = prodFilter.FilterItems;
            gvSisProdFilterItems.DataBind();
        }


        protected void gvSisProdFilterItems_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gvSisProdFilterItems = (sender as ASPxGridView);
            if (e.ButtonID == "dxbttProdFilterItemDelete")
            {
                Int32 prodFilterItemID = ConvertUtility.ConvertToInt32(gvSisProdFilterItems.GetRowValues(e.VisibleIndex, "ProdFilterItemID"), 0);
                gvSisProdFilterItems.JSProperties["cpRowIndex"] = e.VisibleIndex;
                gvSisProdFilterItems.JSProperties["cpShowDeleteConfirmBox"] = true;
                //PIDProdFilterItemBLL.DeleteByProdFilterItemID(prodFilterItemID);

                //Int32 prodFilterID = ProdFilterID;
                //if (prodFilterID < 1) throw new HttpException(404, "Page not found.");


                //Int32 sisLangID = GetSISLangID();
                //PIDProdFilter prodFilter = PIDProdFilterBLL.SelectByFilterID(prodFilterID, sisLangID);
                //if (prodFilter == null || !LoginUtility.IsAdminRole(prodFilter.FilterOwnerRole))
                //    throw new HttpException(404, "Page not found.");
                //gvSisProdFilterItems.DataSource = prodFilter.FilterItems;
                //gvSisProdFilterItems.DataBind();
            }
        }

        protected void dxbttSisProdFilterDelete_Click(object sender, EventArgs e)
        {
            Int32 prodFilterID = ProdFilterID;
            if (prodFilterID < 1) throw new HttpException(404, "Page not found.");
            PIDProdFilterBLL.Delete(prodFilterID);
            WebUtility.HttpRedirect(this, KeyDef.UrlList.SiteAdminPages.Admin_DistPortalPages.ProdFilterList);
        }
    }
}