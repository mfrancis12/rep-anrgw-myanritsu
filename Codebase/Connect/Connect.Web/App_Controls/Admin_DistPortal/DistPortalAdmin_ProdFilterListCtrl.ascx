﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistPortalAdmin_ProdFilterListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_DistPortal.DistPortalAdmin_ProdFilterListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerSisProdFilters" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_DistPortalAdmin_ProdFilterListCtrl,pnlContainerSisProdFilters.HeaderText %>">
    <div class="settingrow">
        <dx:ASPxGridView ID="gvSisProdFilters" runat="server" DataSourceID="odsLookupDistPortalProdFilters" Width="100%" Theme="AnritsuDevXTheme">
            <Columns>
                <dx:GridViewDataColumn FieldName="FilterOwnerRole" VisibleIndex="1" ReadOnly="true" Width="150px" GroupIndex="1"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FilterName" VisibleIndex="2" ReadOnly="true"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="CreatedBy" VisibleIndex="5" ReadOnly="true" Width="150px"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" Caption="Created On" VisibleIndex="9" ReadOnly="true" Width="100px" PropertiesDateEdit-DisplayFormatString="{0:d}"></dx:GridViewDataDateColumn>
                <dx:GridViewDataHyperLinkColumn FieldName="ProdFilterID" VisibleIndex="10" Caption=" " Width="30px">
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="/siteadmin/distportaladmin/sis_prodfilterdetails?pfid={0}" Text="edit"></PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
            </Columns>
            <SettingsBehavior AllowGroup="true" AutoExpandAllGroups="true" />
            <Settings ShowGroupPanel="true" ShowFilterRowMenu="true" ShowFilterRow="true" />
        </dx:ASPxGridView>
    </div>
    <div class="settingrow">
        <asp:HyperLink ID="hlSisProdFilterAddNew" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_prodfilterdetails" Text="Add New Product Filter" SkinID="blueit" ></asp:HyperLink>
    </div>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DistPortalAdmin_ProdFilterListCtrl" />
    </div>
    <asp:ObjectDataSource ID="odsLookupDistPortalProdFilters" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal.ODS_SISProdFilterLookup"
        SelectMethod="SelectProductFilters"></asp:ObjectDataSource>
</anrui:GlobalWebBoxedPanel>
