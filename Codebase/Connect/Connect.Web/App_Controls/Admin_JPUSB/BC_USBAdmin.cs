﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
namespace Anritsu.Connect.Web.App_Controls.Admin_JPUSB
{
    public class BC_USBAdmin : System.Web.UI.UserControl
    {
        public String JapanUSBNo
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.Admin_USBNo]);
            }
        }
    }
}