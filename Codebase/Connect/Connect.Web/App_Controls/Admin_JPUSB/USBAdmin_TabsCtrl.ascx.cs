﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;


namespace Anritsu.Connect.Web.App_Controls.Admin_JPUSB
{
    public partial class USBAdmin_TabsCtrl : BC_USBAdmin, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            InitTabs();
            base.OnInit(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                //InitTabs();
            }
        }

        public void InitTabs()
        {
            String query = String.Empty;
            if(!JapanUSBNo.IsNullOrEmptyString())
            {
                query = String.Format("?{0}={1}", KeyDef.QSKeys.Admin_USBNo, JapanUSBNo);
            }
            Tab tab;
            int tabIndex = 0;

            tab = new Tab(GetStaticResource("TabFindUSB"), "tabFindUSB");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_List + query;
            tab.Enabled = true;
            dxUSBAdmin_JPUSBTabs.Tabs.Add(tab);

            tab = new Tab(GetStaticResource("TabUSBAddNew"), "tabFindUSB");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_AddNew;
            tab.Enabled = true;
            dxUSBAdmin_JPUSBTabs.Tabs.Add(tab);

            if(!String.IsNullOrEmpty(query))
            {
            tab = new Tab(GetStaticResource("TabUSBDetail"), "tabFindUSB");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_Detail + query;
            tab.Enabled = true;
            dxUSBAdmin_JPUSBTabs.Tabs.Add(tab);
            }

           
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_USBAdmin_TabsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}