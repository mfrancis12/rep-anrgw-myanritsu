﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USBAdmin_USBAddNewCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_JPUSB.USBAdmin_USBAddNewCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerUSBAddNew" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_USBAdmin_USBAddNewCtrl,pnlContainerUSBAddNew.HeaderText %>">
<div class="width-60">
<div class="settingrow group input-text required">
    <label><asp:Localize ID="lcalNewUSBNo" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBAddNewCtrl,lcalNewUSBNo.Text %>" />:</label>
    <p class="error-message">
        <asp:Localize ID="lclUsbnoErr" runat="server" Text="<%$Resources:Common,InvalidMsg%>"></asp:Localize>
    </p>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ControlToValidate="txtNewUSBNo" Display="Dynamic" ErrorMessage="<%$Resources:Common,InvalidMsg%>"
        ValidationExpression="^[a-zA-Z0-9-_]+$"></asp:RegularExpressionValidator>
    <asp:TextBox ID="txtNewUSBNo" runat="server"></asp:TextBox>
    <%--<asp:RequiredFieldValidator id="rfvNewUSBNo" runat="server" ControlToValidate="txtNewUSBNo" Display="Dynamic" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="revNewUSBNo" runat="server" 
        ControlToValidate="txtNewUSBNo" Display="Dynamic" ErrorMessage="<%$Resources:Common,InvalidMsg%>"
        ValidationExpression="^[a-zA-Z0-9-_]+$"></asp:RegularExpressionValidator>--%>
</div>
<div class="settingrow group input-text required">
    <label><asp:Localize ID="lcalNewUSBOwnerEmail" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBAddNewCtrl,lcalNewUSBOwnerEmail.Text %>" />:</label>
    <p class="error-message">
        <asp:Localize ID="lclUsbOwnerErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
    </p>
    <asp:TextBox ID="txtNewUSBOwnerEmail" runat="server"></asp:TextBox>
    <%--<asp:RequiredFieldValidator id="rfvNewUSBOwnerEmail" runat="server" ControlToValidate="txtNewUSBOwnerEmail" Display="Dynamic" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
</div>
<div class="settingrow group margin-top-15">
    <asp:Button ID="bttNewUSBAdd" SkinID="submit-btn" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBAddNewCtrl,bttNewUSBAdd.Text %>" OnClick="bttNewUSBAdd_Click"/>
</div>
<div class="settingrow">
    <p class='msg'><asp:Label ID="lblNewUSBMsg" runat="server"></asp:Label></p>
</div>
</div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_USBAdmin_USBAddNewCtrl" EnableViewState="false" /></div>
</anrui:GlobalWebBoxedPanel>