﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.Admin_JPUSB
{
    public partial class USBAdmin_USBAddNewCtrl : BC_USBAdmin, App_Lib.UI.IStaticLocalizedCtrl
    {

        public String ReturnURL
        {
            get
            {
                String returnURL = Request.QueryString[KeyDef.QSKeys.ReturnURL];
                if (String.IsNullOrEmpty(returnURL)) returnURL = "~/siteadmin/usbkeyadmin/usbaddnew";
                return returnURL;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bttNewUSBAdd_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            lblNewUSBMsg.Text = String.Empty;
            try
            {
                Lib.Security.Sec_UserMembership adminUser = LoginUtility.GetCurrentUserOrSignIn();
                String usbNo = txtNewUSBNo.Text.Trim();

                String email = txtNewUSBOwnerEmail.Text.Trim();

                DataTable tbExistingUSB = Lib.DongleDownload.DongleLicBLL.FindDongle(usbNo);
                if (tbExistingUSB != null && tbExistingUSB.Rows.Count > 0)
                {
                    throw new ArgumentException(GetStaticResource("USBKeyExists"));
                    //"This USB key already exists.  You cannot add duplicate key.");
                }

                Lib.Security.Sec_UserMembership usbOwner = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
                if (usbOwner == null)
                {
                    throw new ArgumentException(GetStaticResource("UserDoesNotExist"));
                    //throw new ArgumentException("This user does not exist in this system.");
                }

                List<Lib.Account.Acc_AccountMaster> usbOwnerAccounts = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(usbOwner.MembershipId);
                if (usbOwnerAccounts == null || usbOwnerAccounts.Count < 1)
                {
                    throw new ArgumentException(GetStaticResource("UserNotInOrg"));
                    //throw new ArgumentException("This user does not belong to any organization account.");
                }

                if (!Lib.DongleDownload.DongleLicBLL.IsValidUSBNoAndPassword(usbNo, "webgatekey"))
                {
                    throw new ArgumentException(GetStaticResource("InvalidUsbNo"));
                    // throw new ArgumentException("Invalid USB No. or code.");
                }

                Lib.DongleDownload.DongleLicBLL.Insert(usbNo, usbOwner.Email, adminUser.Email);
                lblNewUSBMsg.Text = String.Format("{0} {1}.", usbNo, GetStaticResource("USBAddedMsg"));
                // added successfully
                txtNewUSBNo.Text = String.Empty;
                txtNewUSBOwnerEmail.Text = String.Empty;

                WebUtility.HttpRedirect(null, ReturnURL);

            }
            catch (Exception ex)
            {
                lblNewUSBMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_USBAdmin_USBAddNewCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}