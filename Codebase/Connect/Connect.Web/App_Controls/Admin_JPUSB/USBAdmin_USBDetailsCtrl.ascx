﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USBAdmin_USBDetailsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_JPUSB.USBAdmin_USBDetailsCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerUSBDetails" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_USBAdmin_USBAddNewCtrl,pnlContainerUSBDetails.HeaderText %>">
<div class="width-60">
<div class="settingrow group input-text">
    <label><asp:Localize ID="lcalUSBNo" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,lcalUSBNo.Text %>" />:</label>
    <asp:TextBox ID="txtUSBNo" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
</div>
<div class="settingrow group input-text">
    <label><asp:Localize ID="lcalUSBOwnerEmail" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,lcalUSBOwnerEmail.Text %>" />:</label>
    <asp:TextBox ID="txtUSBOwnerEmail" runat="server" Enabled="false" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow group input-text">
    <label><asp:Localize ID="lcalCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,lcalCreatedBy.Text %>" />:</label>
    <asp:TextBox ID="txtCreatedBy" runat="server" SkinID="tbx-300" Enabled="false" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow group input-select">
    <label><asp:Localize ID="lcalUSBKeyStatus" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,lcalUSBKeyStatus.Text %>" />:</label>
    <asp:DropDownList ID="cmbUSBKeyStatus" runat="server">
        <asp:ListItem Text="Active" Value="active"></asp:ListItem>
        <asp:ListItem Text="Locked" Value="locked"></asp:ListItem>
    </asp:DropDownList>
</div>
<div class="settingrow group margin-top-15">
    <asp:Button ID="bttUSBUpdate" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,bttUSBUpdate.Text %>" OnClick="bttUSBUpdate_Click" />
    <asp:Button ID="bttUSBDelete" runat="server" Text="<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,bttUSBDelete.Text %>" Enabled="false" OnClick="bttUSBDelete_Click"/>
</div>
<div class="settingrow">
    <p class='msg'><asp:Label ID="lblUSBMsg" runat="server"></asp:Label></p>
</div>
<div class="settingrow">&nbsp;</div>

</div>
<div class="settingrow" style="width: 100%;">
<dx:ASPxGridView ID="gvProdRegsByUSB" runat="server" ClientInstanceName="gvProdRegsByUSB" AutoGenerateColumns="false" Theme="AnritsuDevXTheme" DataSourceID="odsgvProdRegsByUSB" Width="100%">
<Columns>
    <dx:GridViewDataColumn FieldName="ModelNumber" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,gvProdRegsByUSB.ModelNumber.Caption%>'></dx:GridViewDataColumn>
    <dx:GridViewDataColumn FieldName="SerialNumber" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,gvProdRegsByUSB.SerialNumber.Caption%>'></dx:GridViewDataColumn>
    <dx:GridViewDataColumn FieldName="OrgFullName" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,gvProdRegsByUSB.OrgFullName.Caption%>'></dx:GridViewDataColumn>
    <dx:GridViewDataColumn FieldName="IsVerifiedAndApproved" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBDetailsCtrl,gvProdRegsByUSB.IsVerifiedAndApproved.Caption%>' ReadOnly="true"></dx:GridViewDataColumn>
    <dx:GridViewDataHyperLinkColumn FieldName="WebToken" Caption=" ">
        <PropertiesHyperLinkEdit Text='<%$Resources:common,details%>' NavigateUrlFormatString="/prodregadmin/manage_registration?rwak={0}"></PropertiesHyperLinkEdit>
    </dx:GridViewDataHyperLinkColumn>
</Columns>
<SettingsPager PageSize="100"></SettingsPager>
</dx:ASPxGridView>
 <asp:ObjectDataSource ID="odsgvProdRegsByUSB" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_USB.ODS_JPUSB" 
    EnablePaging="false" EnableCaching="true" 
    CacheExpirationPolicy="Sliding" CacheDuration="160" CacheKeyDependency="odsgvProdRegsByUSBCKD"
    SelectMethod="SelectByJPUSBNO">
        <SelectParameters>
            <asp:QueryStringParameter Name="jpUSBNo" QueryStringField="admjpusb" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_USBAdmin_USBDetailsCtrl" EnableViewState="false" /></div>
</anrui:GlobalWebBoxedPanel>