﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.Admin_JPUSB
{
    public partial class USBAdmin_USBDetailsCtrl : BC_USBAdmin, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                HandleODSCache();
                LoadUSBInfo();
            }
        }

        private void LoadUSBInfo()
        {
            String usbNo = JapanUSBNo.Trim();
            if (usbNo.IsNullOrEmptyString())
            {
                throw new HttpException(404, "Page not found.");
            }

            DataTable tbUSBInfo = Lib.DongleDownload.DongleLicBLL.FindDongle(usbNo, true);
            if (tbUSBInfo == null || tbUSBInfo.Rows.Count != 1)
            {
                throw new HttpException(404, "Page not found.");
            }

            DataRow usbInfoRow = tbUSBInfo.Rows[0];
            txtUSBNo.Text = ConvertUtility.ConvertNullToEmptyString(usbInfoRow["USBNo"]);
            txtUSBOwnerEmail.Text = ConvertUtility.ConvertNullToEmptyString(usbInfoRow["EmailAddress"]);
            txtCreatedBy.Text = ConvertUtility.ConvertNullToEmptyString(usbInfoRow["CreatedBy"]);
            cmbUSBKeyStatus.SelectedValue = ConvertUtility.ConvertNullToEmptyString(usbInfoRow["USBKeyStatus"]);
            Int32 associationCount = ConvertUtility.ConvertToInt32(usbInfoRow["RegAssociations"], 0);
            bttUSBDelete.Enabled = (associationCount == 0);
        }

        private void HandleODSCache()
        {
            String odsgvProdRegsByUSBCacheKey = String.Format("admusbpreg_{0}", JapanUSBNo.Trim());
            if (Cache[odsgvProdRegsByUSB.CacheKeyDependency] == null) Cache[odsgvProdRegsByUSB.CacheKeyDependency] = odsgvProdRegsByUSBCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsgvProdRegsByUSB.CacheKeyDependency].ToString().Equals(odsgvProdRegsByUSBCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsgvProdRegsByUSB.CacheKeyDependency);
                Cache[odsgvProdRegsByUSB.CacheKeyDependency] = odsgvProdRegsByUSBCacheKey;
            }

        }

        protected void bttUSBUpdate_Click(object sender, EventArgs e)
        {
            lblUSBMsg.Text = String.Empty;
            try
            {
                String usbNo = JapanUSBNo.Trim();
                Lib.DongleDownload.DongleLicBLL.Update(usbNo, cmbUSBKeyStatus.SelectedValue);
                lblUSBMsg.Text = GetGlobalResourceObject("STCTRL_USBDetail", "Msg_USBStatusUpdated").ToString();
                //"Status updated successfully.";
                Cache.Remove(odsgvProdRegsByUSB.CacheKeyDependency);
                gvProdRegsByUSB.DataBind();
            }
            catch (Exception ex)
            {
                lblUSBMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
            }
        }

        protected void bttUSBDelete_Click(object sender, EventArgs e)
        {
            lblUSBMsg.Text = String.Empty;
            try
            {
                String usbNo = JapanUSBNo.Trim();
                Boolean isDeleted = Lib.DongleDownload.DongleLicBLL.Delete(usbNo);
                if (isDeleted)
                {
                    Cache.Remove(odsgvProdRegsByUSB.CacheKeyDependency);
                    Response.Redirect(KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_List, true);
                }
                else
                {
                    throw new ArgumentException("This USB is associated with product registrations.  Please remove associations first to delete this USB Key completely.");
                }
            }
            catch (Exception ex)
            {
                lblUSBMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}