﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="USBAdmin_USBListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_JPUSB.USBAdmin_USBListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerUSBList" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_USBAdmin_USBListCtrl,pnlContainerUSBList.HeaderText %>">
<div class="settingrow">
<fieldset>
    <legend>
        <asp:Localize ID="lcalFindUSBLegend" runat="server" Text='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,lcalFindUSBLegend.Text%>'></asp:Localize></legend>
    <label>
        <asp:Localize ID="lcalFindUSBDesc" runat="server" Text='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,lcalFindUSBDesc.Text%>'></asp:Localize></label>
    <div class="settingrow group input-text width-60">
        <asp:TextBox ID="txtSearchUSB" runat="server" ValidationGroup="vgSearchJPUSB"></asp:TextBox>
    </div>
    <div class="group margin-top-15">
        <asp:Button ID="bttSearchUSB" runat="server" ValidationGroup="vgSearchJPUSB" Text="Search USB" OnClick="bttSearchUSB_Click" />
    </div>
</fieldset>
</div>
<div class="settingrow">
    <dx:ASPxGridView ID="gvJPUSBList" runat="server" ClientInstanceName="gvJPUSBList" AutoGenerateColumns="false" Theme="AnritsuDevXTheme" DataSourceID="odsUSBSearch" Width="100%">
        <Columns>
            <dx:GridViewDataColumn FieldName="USBNo" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,gvJPUSBList.USBNo.Caption%>'></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="EmailAddress" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,gvJPUSBList.EmailAddress.Caption%>'></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="USBKeyStatus" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,gvJPUSBList.USBKeyStatus.Caption%>'></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="RegAssociations" Caption='<%$Resources:ADM_STCTRL_USBAdmin_USBListCtrl,gvJPUSBList.RegAssociations.Caption%>'></dx:GridViewDataColumn>
            <dx:GridViewDataHyperLinkColumn FieldName="USBNo" Caption=" ">
                <PropertiesHyperLinkEdit Text='<%$Resources:common,details%>' NavigateUrlFormatString="/siteadmin/usbkeyadmin/usbdetail?admjpusb={0}"></PropertiesHyperLinkEdit>
            </dx:GridViewDataHyperLinkColumn>
        </Columns>
        <SettingsPager PageSize="100"></SettingsPager>
    </dx:ASPxGridView>
    <asp:ObjectDataSource ID="odsUSBSearch" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_USB.ODS_JPUSB" 
    EnablePaging="false" EnableCaching="true" 
    CacheExpirationPolicy="Sliding" CacheDuration="160" CacheKeyDependency="odsUSBSearchCKD"
    SelectMethod="FindJPUSB">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtSearchUSB" Name="keyword" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_USBAdmin_USBListCtrl" EnableViewState="false" /></div>
</anrui:GlobalWebBoxedPanel>
