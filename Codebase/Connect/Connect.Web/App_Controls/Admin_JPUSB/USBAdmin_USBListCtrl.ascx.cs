﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.Admin_JPUSB
{
    public partial class USBAdmin_USBListCtrl : BC_USBAdmin, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                HandleODSCache();
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_USBAdmin_USBListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void bttSearchUSB_Click(object sender, EventArgs e)
        {
            gvJPUSBList.DataBind();
        }

        private void HandleODSCache()
        {
            String odsUSBSearchCacheKey = String.Format("admusbcache_{0}", txtSearchUSB.Text.Trim());
            if (Cache[odsUSBSearch.CacheKeyDependency] == null) Cache[odsUSBSearch.CacheKeyDependency] = odsUSBSearchCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsUSBSearch.CacheKeyDependency].ToString().Equals(odsUSBSearchCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsUSBSearch.CacheKeyDependency);
                Cache[odsUSBSearch.CacheKeyDependency] = odsUSBSearchCacheKey;
            }

        }
    }
}