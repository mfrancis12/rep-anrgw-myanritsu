﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationAdminDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.NotificationAdminDetailCtrl" %>


<%@ Register Src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" TagName="ResourceLocalizationItemCtrl"
    TagPrefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,pnlContainer.HeaderText %>'>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var cmbAddNewLang = document.getElementById('<%= cmbAddNewLang.ClientID%>');
            if (cmbAddNewLang != null)
            {
                cmbAddNewLang[0].selected = true;
            }
        });
        function ConfirmOnDelete(deleteMessage) {
            if (confirm(deleteMessage) == true)
                return true;
            else
                return false;
        }
    </script>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalNotificationName" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalNotificationInternalName.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtNotificationName" runat="server" SkinID="tbx-200" ValidationGroup="vgNotificationInfo"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvNotificationInternalName" runat="server" ControlToValidate="txtNotificationName"
            ValidationGroup="vgNotificationInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalNFNDisplayArea" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalNFNDisplayArea.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="ddlDisplayArea" runat="server" SkinID="ddl-210" DataTextField="DisplayText" DataValueField="DataValueField" Enabled="false" AppendDataBoundItems="true">
        </asp:DropDownList>
          <asp:RequiredFieldValidator ID="rfvDisplayArea" runat="server" ControlToValidate="ddlDisplayArea"
            ValidationGroup="vgNotificationInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>" InitialValue="--Select--"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalNFNInternalComments" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalNotificationInternalComments.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtNFNInternalComments" runat="server" SkinID="tbx-500" TextMode="MultiLine" ValidationGroup="vgNotificationInfo"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvNFNInternalComments" runat="server" ControlToValidate="txtNFNInternalComments"
            ValidationGroup="vgNotificationInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
     <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalEnable" runat="server"  Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalEnable.Text%>"></asp:Localize></span>
        <asp:CheckBox ID="chkboxEnable" runat="server" Text="" />
        
    </div>
    <div class="settingrow" style="text-align: right;">
        <asp:Button ID="bttSaveNotification" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,bttSaveNotification.Text%>"
            OnClick="bttSaveNotification_Click" ValidationGroup="vgNotificationInfo" />
        <asp:Button ID="bttDeleteNotification" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,bttDeleteNotification.Text%>"
            OnClick="bttDeleteNotification_Click" Visible="false" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerNotificationText" runat="server" ShowHeader="true"
    HeaderText='<%$ Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,pnlContainerNotificationText.HeaderText %>'>
    <center>
        <div class="settingrow">
            <asp:Localize ID="lcalNotificationTextDesc" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalNotificationTextDesc.Text%>"></asp:Localize></div>
        <div class="settingrow">
            <uc1:ResourceLocalizationItemCtrl ID="reslcNotificatonText" runat="server" />
        </div>
    </center>
     <div class="settingrow">
            <asp:DropDownList ID="cmbAddNewLang" runat="server" DataTextField="DisplayText" DataValueField="Locale" Visible="false">
            </asp:DropDownList>
             <asp:Button ID="bttAddNewLang" runat="server" Text="Add New" SkinID="SmallButton"
                CausesValidation="false" OnClick="bttAddNewLang_Click" Visible="false" />
        </div>
</anrui:GlobalWebBoxedPanel>



