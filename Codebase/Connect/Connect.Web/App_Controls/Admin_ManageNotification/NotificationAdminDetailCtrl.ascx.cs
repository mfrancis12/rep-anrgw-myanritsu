﻿/*
 * Author:        kishore kumar M
 * Created Date:  
 * Modified Date: 04/04/2014
 * Modified By:   kishore kumar M
 * Purpose:       Interface to manage notifications
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Notification;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Data;
using Anritsu.Connect.Web.App_Lib;
using System.Data.SqlClient;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class NotificationAdminDetailCtrl : System.Web.UI.UserControl
    {
        #region Properties/Fields

        public int NotificationID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.NotificationId], 0);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_NotificationAdmin_DetailCtrl"; }
        }

        private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        private String EffectedArea
        {
            get { return "Export Notice"; }
        }
        #endregion
        

        /// <summary>
        /// loads Notification Information and Display area list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            reslcNotificatonText.ClassKey = "NOTIFICATION_" + NotificationID.ToString().ToUpperInvariant();
            reslcNotificatonText.ResKey = "NotificationText";
            bttDeleteNotification.Attributes.Add("onclick", "return ConfirmOnDelete('" + GetStaticResource("ConfirmDeleteMsg") + "')");

            if (!IsPostBack)
            {
                BindNotificationDisplayAreaList();
                BindNotificationInfo();
            }
        }

        /// <summary>
        /// Bind Add New Language Section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(reslcNotificatonText.ClassKey) && (!String.IsNullOrWhiteSpace(reslcNotificatonText.ResKey)))
                InitAddNewLanguageSection();
        }

        #region Postback Events

        /// <summary>
        /// Save (update/add) Notification 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttSaveNotification_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            if (NotificationID == 0)
            {
                //update the admin activity log
                LogAdminActivity(EffectedArea, TasksPerformed.Add, ActivityLogNotes.NOTIFICATION_ADD_ATTEMPT, getCurrentUserEmail, txtNotificationName.Text.Trim());
                //insert the notification
                int notificationID = Lib.Notification.NotificationBLL.Insert(txtNotificationName.Text.Trim(), Int32.Parse(ddlDisplayArea.SelectedValue), txtNFNInternalComments.Text.Trim(), LoginUtility.GetCurrentUser(true).Email, chkboxEnable.Checked);
                if (notificationID != 0)
                {
                    //update the admin activity log
                    LogAdminActivity(EffectedArea, TasksPerformed.Add, ActivityLogNotes.NOTIFICATION_ADD_SUCCESS, getCurrentUserEmail, txtNotificationName.Text.Trim());
                    //redirect after success
                    WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}"
                        , App_Lib.KeyDef.UrlList.SiteAdminPages.NotificationAdmin_Details
                        , App_Lib.KeyDef.QSKeys.NotificationId, notificationID.ToString()));
                }
            }
            else
            {
                //update the admin activity log
                LogAdminActivity(EffectedArea, TasksPerformed.Modify, ActivityLogNotes.NOTIFICATION_MODIFY_ATTEMPT, getCurrentUserEmail, txtNotificationName.Text.Trim());
                Lib.Notification.NotificationBLL.Update(NotificationID, txtNotificationName.Text.Trim(), txtNFNInternalComments.Text.Trim(), Int32.Parse(ddlDisplayArea.SelectedValue), LoginUtility.GetCurrentUser(true).Email, chkboxEnable.Checked);
                //update the admin activity log
                LogAdminActivity(EffectedArea, TasksPerformed.Modify, ActivityLogNotes.NOTIFICATION_MODIFY_SUCCESS, getCurrentUserEmail, txtNotificationName.Text.Trim());
            }
        }

        /// <summary>
        /// deletes the notification and resource strings associated witht he notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttDeleteNotification_Click(object sender, EventArgs e)
        {
            //update the admin activity log
            LogAdminActivity(EffectedArea, TasksPerformed.Delete, ActivityLogNotes.NOTIFICATION_DELETE_ATTEMPT, getCurrentUserEmail, txtNotificationName.Text.Trim());
            Lib.Notification.NotificationBLL.Delete(NotificationID);
            //update the admin activity log
            LogAdminActivity(EffectedArea, TasksPerformed.Delete, ActivityLogNotes.NOTIFICATION_DELETE_SUCCESS, getCurrentUserEmail, txtNotificationName.Text.Trim());
            WebUtility.HttpRedirect(null, App_Lib.KeyDef.UrlList.SiteAdminPages.NotificationAdmin_List);
        }

        /// <summary>
        /// Can add new language
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttAddNewLang_Click(object sender, EventArgs e)
        {
            try
            {
                String newlocale = cmbAddNewLang.SelectedValue;
                Int32 contentID = Lib.Content.Res_ContentStoreBLL.Insert(reslcNotificatonText.ClassKey, reslcNotificatonText.ResKey, newlocale, "-", false);
                if (contentID > 0)
                {
                    WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&returnurl={3}", KeyDef.UrlList.SiteAdminPages.AdminContentDetails, KeyDef.QSKeys.AdminContent_ContentID, contentID, HttpUtility.UrlEncode(Request.RawUrl)));
                }
            }
            catch (SqlException ex)
            {

            }
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Bind NotificationDisplayAreaList
        /// </summary>
        private void BindNotificationDisplayAreaList()
        {
            DataTable displayAreas = Lib.Notification.NotificationBLL.SelectNotificationDisplayAreas();
            ddlDisplayArea.DataSource = displayAreas;
            ddlDisplayArea.DataBind();
            //get the active notification and select that as default
            if (displayAreas.Select("IsActive=1") != null)
            {
                ddlDisplayArea.SelectedValue = displayAreas.Select("IsActive=1")[0]["DataValueField"].ToString();
            }
            //ddlDisplayArea.Items.Insert(0,new ListItem("--Select--"));
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        /// <summary>
        /// Bind Notification form info
        /// </summary>
        private void BindNotificationInfo()
        {
            Notification notificationInfo = NotificationBLL.SelectByNotificationID(NotificationID);

            if (notificationInfo == null)
            {
                pnlContainerNotificationText.Visible = false;
                return;
            }

            txtNotificationName.Text = notificationInfo.NotificationName;
            txtNFNInternalComments.Text = notificationInfo.InternalComments;
            ddlDisplayArea.SelectedValue = notificationInfo.DisplayAreaID.ToString();
            chkboxEnable.Checked = notificationInfo.IsActive;
            bttDeleteNotification.Visible = true;
        }

        /// <summary>
        /// Bind Add New Language Section
        /// </summary>
        private void InitAddNewLanguageSection()
        {

            //Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(reslcNotificatonText.ClassKey, reslcNotificatonText.ResKey);

            DataTable tbSupportedLocales = Lib.Content.Res_ContentStoreBLL.ContentSupportedLocale_SelectAll();

            var filtered = (from l in tbSupportedLocales.AsEnumerable()
                            join n in tb.AsEnumerable() on l["Locale"].ToString() equals n["Locale"].ToString()
                         into f
                            where f.Count() == 0
                            select l);
            DataTable filteredNewLocales = null;
            if (filtered != null && filtered.Count() > 0)
            {
                cmbAddNewLang.Visible = bttAddNewLang.Visible = true;
                filteredNewLocales = filtered.CopyToDataTable();
                DataView dv = filteredNewLocales.DefaultView;
                dv.Sort = "DisplayText ASC";
                cmbAddNewLang.DataSource = dv;
                cmbAddNewLang.DataBind();
            }
        }

        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(getCurrentUserEmail, EffectedArea, taskPerformed, action, WebUtility.GetUserIP(),ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
        }

        #endregion
    }
}