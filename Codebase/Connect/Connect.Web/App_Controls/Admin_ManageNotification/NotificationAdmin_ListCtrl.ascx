﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationAdmin_ListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.NotificationAdmin_ListCtrl" %>


<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_NotificationAdmin_ListCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow">
        <asp:HyperLink ID="hlAddNewForm" runat="server" Text="<%$ Resources:ADM_STCTRL_NotificationAdmin_ListCtrl,hlAddNewNotification.Text %>"
            NavigateUrl="~/siteadmin/NotificationAdmin/notificationdetails"></asp:HyperLink>
    </div>
    <div class="settingrow">
        <dx:ASPxGridView ID="gvNotifications" ClientInstanceName="gvNotifications" runat="server" DataSourceID="sdsNotificationsResults" Theme="AnritsuDevXTheme"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="NotificationID" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="NotificationID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>

                <dx:GridViewDataColumn FieldName="NotificationName" VisibleIndex="1" Caption="Notification Name"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="DisplayAreaName" VisibleIndex="2" Caption="Display Area"></dx:GridViewDataColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="3" Caption="Created On (UTC)">
                    <PropertiesDateEdit DisplayFormatString="d">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataHyperLinkColumn FieldName="NotificationID" VisibleIndex="5" Caption=" "  PropertiesHyperLinkEdit-NavigateUrlFormatString="~/siteadmin/notificationadmin/notificationdetails?nfnid={0}" PropertiesHyperLinkEdit-Text="details">
                   <Settings AllowAutoFilter="False"  />
                </dx:GridViewDataHyperLinkColumn>

            </Columns>
            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>

            <%--  <SettingsText GroupContinuedOnNextPage="Continued On Next Page..." />--%>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="sdsNotificationsResults" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
            SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Notification_SelectAll]"></asp:SqlDataSource>
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_NotificationAdmin_ListCtrl" />

</anrui:GlobalWebBoxedPanel>

