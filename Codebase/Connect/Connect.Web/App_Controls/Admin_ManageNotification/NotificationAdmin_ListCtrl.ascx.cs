﻿/*
 * Author:        kishore kumar M
 * Created Date:  
 * Modified Date: 04/04/2014
 * Modified By:   kishore kumar M
 * Purpose:       Displays the list of notifications
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class NotificationAdmin_ListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_NotificationAdmin_ListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

    }
}