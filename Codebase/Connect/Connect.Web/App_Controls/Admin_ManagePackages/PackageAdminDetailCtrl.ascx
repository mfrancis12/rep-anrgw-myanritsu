﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PackageAdminDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ManagePackages.PackageAdminDetailCtrl" %>
<asp:PlaceHolder runat="server">
<link href="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/css/token-input.css"  rel="stylesheet" />
<script type="text/javascript">
    var CHECKBOXID = '<%=cbxEnable.ClientID%>';
</script>
<script src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/jquery.tokeninput.js"></script>
<script src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/suggest_model.js"></script>
    </asp:PlaceHolder>
<style type="text/css">
    .col { border: 1px solid #FFFFFF; float: left; width: 166px; }
    .add-fileds { text-align: right; width: 155px; display: inline-block; float: none !important; }
</style>

<script type="text/javascript">
    $(function () {
        getLoadValues();
    });

    getLoadValues = function () {
        var packagePath = document.getElementById('<%=txtPackagePath.ClientID%>');
        packagePath.title = packagePath.value;
    }


    function browseFiles() {
        var packagePath = document.getElementById('<%=txtPackagePath.ClientID%>');
       var packpath = document.getElementById('<%=hfPackagePath.ClientID%>');
       if (!window.showModalDialog) {
           window.open("BrowsePackage.aspx", 'Anritsu', "height=400,width=500,top=150,left=500");
       } else {
           var retvalue = window.showModalDialog("BrowsePackage.aspx", 'Anritsu', "dialogHeight: 400px; dialogWidth: 500px; edge: Raised; help: Yes; resizable: Yes; status: No;dialogLeft:500px;dialogTop:150px");

           if (typeof retvalue === 'undefined') {
               if (typeof window.returnValue === 'undefined')
                   packagePath.value = '';
               else {
                   packagePath.title = packagePath.value = packpath.value = window.returnValue;
               }
           }
           else
               packagePath.title = packagePath.value = packpath.value = retvalue;
           return false;
       }
    }
    function filePath(path) {
        var packagePath = document.getElementById('<%=txtPackagePath.ClientID%>');
        var packpath = document.getElementById('<%=hfPackagePath.ClientID%>');
        if (!path) {
            packagePath.value = '';
        }
        else {
            packagePath.title = packagePath.value = packpath.value = path;
        }
    }
     function ConfirmOnDeleteOrRemove(deleteMessage) {
         if (confirm(deleteMessage) == true)
             return true;
         else
             return false;
     }
     function clickButton(e, buttonid) {
         var evt = e ? e : window.event;
         var bt = document.getElementById(buttonid);

         if (bt) {
             if (evt.keyCode == 13) {
                 bt.click();
                 return false;
             }
         }
     }
     function validate() {
         validateTokenizer('token-validator');
         if (Page_ClientValidate())
             return validateTokenizer('token-validator');
     }
</script>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow width-60 group input-text required">
        <label>
            <asp:Localize ID="lcalPackageName" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackageName.Text%>"></asp:Localize>*</label>
        <p class="error-message">
            <asp:Localize ID="lclPckNmeErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtPackageName" runat="server" ValidationGroup="vgPackageInfo"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvPackageName" runat="server" ControlToValidate="txtPackageName"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
    </div>
    <div class="settingrow width-60 group input-text required">
        <label>
            <asp:Localize ID="lcalPackagePath" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackagePath.Text%>"></asp:Localize>*</label>
        <p class="error-message">
            <asp:Localize ID="lclPackgErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtPackagePath" runat="server" Enabled="false" ValidationGroup="vgPackageInfo"></asp:TextBox>
        <asp:HiddenField ID="hfPackagePath" runat="server" Value="" />
        
        <%--<asp:RequiredFieldValidator ID="rfvPackagePath" runat="server" ControlToValidate="txtPackagePath"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
    </div>
    <div class="group margin-top-15">
        <asp:Button OnClientClick="return browseFiles();" ID="btnBrowse" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalBrowse.Text %>" />
    </div>
    <div class="settingrow group input-select width-60 margin-top-15">
        <label>
            <asp:Localize ID="lcalSystem" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalSystem.Text%>"></asp:Localize>*</label>
        <asp:DropDownList ID="ddlSystem" runat="server" DataTextField="SystemName" DataValueField="SystemID" AppendDataBoundItems="true">
        </asp:DropDownList>
    </div>
        <asp:RequiredFieldValidator ID="rfvSystem" runat="server" ControlToValidate="ddlSystem"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>" InitialValue="--Select--"></asp:RequiredFieldValidator>
    <div class="settingrow overflow">
        <span class="settinglabel">
            <span style='float: left'>
                <asp:Localize ID="lcalAssignModel" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalAssignModel.Text%>"></asp:Localize>*</span></span>
        <%--<div style='float: left' id="model">
            <asp:HiddenField ID="hfModels" runat="server" />
            <asp:TextBox ID="txtModelNumber" placeholder="Enter atleast 3 characters of a model" autocomplete="off" runat="server" SkinID="tbx-200"></asp:TextBox>
            <span class="required-field-validator" id="token-validator">Required</span>
        </div>--%>

    </div>
    <div class="group width-60">
        <div id="model">
            <asp:HiddenField ID="hfModels" runat="server" />
            <asp:TextBox ID="txtModelNumber" placeholder="Enter atleast 3 characters of a model" autocomplete="off" runat="server" SkinID="tbx-200"></asp:TextBox>
            <span class="required-field-validator" id="token-validator">Required</span>
        </div>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalEnable" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalEnable.Text%>"></asp:Localize></span>
        <asp:CheckBox ID="cbxEnable" runat="server" Text=" " />
    </div>

    <div class="settingrow">
        <span class="settinglabel  error-msg">
            <asp:Localize ID="lcalErrorMsg" runat="server" Visible="false"></asp:Localize>
        </span>
    </div>
    <div class="settingrow">
        <span class="msg">
            <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label></span>
    </div>
    <div class="settingrow">
        <asp:Button ID="btnSavePackage" runat="server" OnClientClick="return validate();" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,btnSavePackage.Text%>"
            OnClick="btnSavePackage_Click" ValidationGroup="vgPackageInfo" />
        <asp:Button ID="btnDeletePackage" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,btnDeletePackage.Text%>"
            OnClick="btnDeletePackage_Click" Visible="false" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlAddPackageToOrg" runat="server" ShowHeader="true"
    HeaderText="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,pnlAddPackageToOrg.HeaderText%>" DefaultButton="bttSearch">
        <div class="settingrow group input-text width-60">
            <label>
                <asp:Localize ID="lcalSearchCategory" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalSearchCategory.Text %>" /></label>
            <asp:TextBox ID="txtSearch" runat="server"/>
        </div>
    <div class="margin-top-15 group">
        <asp:Button ID="bttSearch" runat="server" CausesValidation="false" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,bttSearch.Text %>'
                OnClick="bttSearch_Click" />
    </div>
        <div class="settingrow group input-select margin-top-15 width-60">
            <%--<div class="col"></div>--%>
            <asp:DropDownList ID="cmbAddToOrg" runat="server" DataTextField="CompanyOrgName"
                DataValueField="AccountID" AppendDataBoundItems="true">
                <asp:ListItem Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,cmbAddToOrg.DefaultItemText%>" Value="selectone" Selected="True"></asp:ListItem>
            </asp:DropDownList>
            
        </div>
    <div class="group margin-top-15">
        <asp:Button ID="bttAddUserToOrg" runat="server" CausesValidation="false" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,bttAddUserToOrg.Text %>'
                OnClick="bttAddPackageToOrg_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlOrgInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,pnlOrgInfo.HeaderText%>" DefaultButton="bttUpdate">
        <asp:GridView ID="gvAssignedOrganizations" runat="server" AutoGenerateColumns="false" OnRowCommand="gvAssignedOrganizations_RowCommand" Width="100%">
            <Columns>
                <%--<asp:BoundField DataField="CompanyName" HeaderText='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CompanyName.Label %>' />--%>
                <asp:TemplateField HeaderText='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CompanyName.Label %>'>
                    <ItemTemplate>
                        <asp:Label ID="lblCompanyName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CompanyName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.AccountName.Label%>">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlAccountName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AccountName") %>' NavigateUrl='<%# GetUrl(DataBinder.Eval(Container.DataItem,"AccountID")) %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="CreatedBy" HeaderText='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.AddedBy.Label %>' />


                <asp:TemplateField HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CreatedOnUTC %>">
                    <ItemTemplate>
                        <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"CreatedOnUTC")) %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText=" ">
                    <ItemTemplate>
                        <asp:Button ID="bttRemoveFromOrg" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.bttRemoveFromOrg.Text %>" OnClientClick='<%#@"return ConfirmOnDeleteOrRemove("""+ GetStaticResource("ConfirmOnRemoveOrg.Text")+@""")" %>'
                           CausesValidation="false" CommandName="RemoveFromOrgCommand"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <EmptyDataTemplate>
                <asp:Localize ID="lcalEmptyOrganization" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalEmptyOrganization.EmptyDataText%>"></asp:Localize>
            </EmptyDataTemplate>
        </asp:GridView>
    <br />

</anrui:GlobalWebBoxedPanel>

