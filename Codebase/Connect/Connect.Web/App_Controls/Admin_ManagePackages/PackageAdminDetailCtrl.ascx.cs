﻿/*
 * Author:        kishore kumar M
 * Created Date:  
 * Modified Date: 04/04/2014
 * Modified By:   kishore kumar M
 * Purpose:       Interface to manage Packages and associated Accounts/Orgs
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Amazon.S3.Model;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.AWS;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;


namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public partial class PackageAdminDetailCtrl : Awss3SettingsCtrlBase
    {
        public long PackageID
        {
            get
            {
                return ConvertUtility.ConvertToInt64(Request.QueryString[App_Lib.KeyDef.QSKeys.PackageId], 0);
            }
        }

        public Boolean PackageCreated
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[App_Lib.KeyDef.QSKeys.Package_Success], false);
            }
        }

        public String ReturnURL
        {
            get
            {
                String returnURL = Request.QueryString[KeyDef.QSKeys.ReturnURL];
                return returnURL;
            }
        }

        private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_PackageAdmin_DetailCtrl"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            txtPackagePath.Text = hfPackagePath.Value;
            btnDeletePackage.Attributes.Add("onclick", "return ConfirmOnDeleteOrRemove('" + GetStaticResource("ConfirmDeleteMsg") + "')");
            if (!IsPostBack)
            {
                BindSystems();
                LoadPackageInfo();
                InitializeManagePackageRoles();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            txtPackagePath.Text = hfPackagePath.Value;
            txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + bttSearch.ClientID + "')");
            this.Page.Form.DefaultButton = btnSavePackage.UniqueID;
        }

        #region PostBack Events

        protected void btnSavePackage_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            long packageIDReturned = 0;
            if (PackageID == 0)
            {
                //pull the files under the package
                List<S3Object> s3objects = PackageBLL.GetPackaeFileList(hfPackagePath.Value.Trim(), TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, "/");
                var filtered = from item in s3objects
                               select new { Key = item.Key, Size = item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                if (s3objects.Count == 0)
                {
                    //Raise no Files under selected package error message
                    lcalErrorMsg.Visible = true;
                    cbxEnable.Enabled = !(String.IsNullOrEmpty(hfModels.Value));
                    lcalErrorMsg.Text = GetStaticResource("lcalNoFilesErrorMsg.Text");
                    return;
                }
                else if (cbxEnable.Checked && String.IsNullOrEmpty(hfModels.Value))
                {
                    //Raise error message (cannot make it active without attaching any model number)
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalNoModelAttachedErrorMsg.Text");
                    return;
                }
                else
                {
                    //create Files list
                    DataTable dtPackageFiles = ToFileListDataTable(filtered);
                    //create Package models
                    DataTable dtPackageModel = GetModelTable(hfModels.Value);
                    //construct Package Object
                    var objPackage = new Package(0,txtPackageName.Text.Trim(),
                        hfPackagePath.Value.Trim(),
                        ConvertUtility.ConvertToInt32(ddlSystem.SelectedValue, 0),cbxEnable.Checked,
                        ModelConfigTypeInfo.CONFIGTYPE_UK_ESD,null,"");

                    //update the admin activity log
                    LogAdminActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADD_ATTEMPT, getCurrentUserEmail, objPackage.PackageName);

                    packageIDReturned = Lib.Package.PackageBLL.InsertPackageWithFiles(objPackage, dtPackageFiles,LoginUtility.GetCurrentUser(true).Email, dtPackageModel);
                    //update the admin activity log with success info
                    if (packageIDReturned > 0)
                    {
                        //update the admin activity log
                        LogAdminActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADD_SUCCESS, getCurrentUserEmail, objPackage.PackageName, objPackage.PackagePath, GetFileList(dtPackageFiles));
                    }
                }
                if (packageIDReturned > 0)
                {
                  
                    //redirect after success
                    String URL = string.Empty;
                    if (String.IsNullOrEmpty(ReturnURL)) URL = String.Format("{0}?{1}={2}&pack_success=true", Request.RawUrl, App_Lib.KeyDef.QSKeys.PackageId, packageIDReturned.ToString());
                    else URL = ReturnURL.Contains("?") ? ReturnURL+"&pack_success=true" : ReturnURL+"?pack_success=true";
                    WebUtility.HttpRedirect(null, URL);
                }
            }
            //update package
            else
            {
                //create Package models
                DataTable dtPackageModel = GetModelTable(hfModels.Value);
                if (ViewState["PackagePath"] != null && (Convert.ToString(ViewState["PackagePath"]) == hfPackagePath.Value))
                {
                    //update only package info
                    var objPackage = new Package() { PackageID = PackageID, PackageName = txtPackageName.Text.Trim(), PackagePath = hfPackagePath.Value.Trim(), SystemID = ConvertUtility.ConvertToInt32(ddlSystem.SelectedValue, 0),
                        Enabled = cbxEnable.Checked,ModelConfigType = ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
                    };

                    //update the admin activity log
                    LogAdminActivity("Package", TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_ATTEMPT, getCurrentUserEmail, objPackage.PackageName);

                    Lib.Package.PackageBLL.UpdatePackage(objPackage, LoginUtility.GetCurrentUser(true).Email, dtPackageModel,"");

                    //update the admin activity log
                    LogAdminActivity("Package", TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_SUCCESS, getCurrentUserEmail, objPackage.PackageName, objPackage.PackagePath, "NO FILES UPDATED");

                    lblMsg.Text = GetStaticResource("lblMsg_PackageUpdated.Text");
                    lblMsg.Visible = true;
                }
                else
                {
                    //pull the files under the package
                    List<S3Object> s3objects = PackageBLL.GetPackaeFileList(hfPackagePath.Value.Trim(), TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, "/");
                    var filtered = from item in s3objects
                                   select new { Key = item.Key, Size = item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                    if (s3objects.Count == 0)
                    {
                        //Raise error message
                        lcalErrorMsg.Visible = true;
                        return;
                    }
                    else
                    {
                        //create Files list
                        DataTable dtPackageFiles = ToFileListDataTable(filtered);

                        //construct Package Object
                        Package objPackage = new Package() { PackageID = PackageID, PackageName = txtPackageName.Text.Trim(), PackagePath = hfPackagePath.Value.Trim(), SystemID = ConvertUtility.ConvertToInt32(ddlSystem.SelectedValue, 0),
                            Enabled = cbxEnable.Checked,ModelConfigType = ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
                        };

                        //update the admin activity log
                        LogAdminActivity("Package", TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_ATTEMPT, getCurrentUserEmail, objPackage.PackageName);

                        packageIDReturned = Lib.Package.PackageBLL.UpdatePackageWithFiles(objPackage, dtPackageFiles, LoginUtility.GetCurrentUser(true).Email, dtPackageModel);

                        //update the admin activity log
                        LogAdminActivity("Package", TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_SUCCESS, getCurrentUserEmail, objPackage.PackageName, objPackage.PackagePath, GetFileList(dtPackageFiles));

                        lblMsg.Text = GetStaticResource("lblMsg_PackageUpdated.Text");
                        lblMsg.Visible = true;
                    }
                }
            }
        }

         private DataTable ToFileListDataTable(dynamic s3Objects)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Key");
            dataTable.Columns.Add("LastModified");
            dataTable.Columns.Add("Size");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            foreach (var n in s3Objects)
            {
                DataRow dr = dataTable.NewRow();
                dr["Key"] = n.Key;
                dr["LastModified"] = n.LastModified.ToUniversalTime();
                dr["Size"] = n.Size;
                dataTable.Rows.Add(dr);
            }
            Thread.CurrentThread.CurrentCulture = currentCulture;
            return dataTable;
        }

        protected void btnDeletePackage_Click(object sender, EventArgs e)
        {
                //update the admin activity log
                LogAdminActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETE_ATTEMPT, getCurrentUserEmail, txtPackageName.Text.Trim());
                Lib.Package.PackageBLL.DeletePackage(PackageID);
                //update the admin activity log
                LogAdminActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETE_SUCCESS, getCurrentUserEmail, txtPackageName.Text.Trim());
                WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.PackageAdmin_List);
        }

        protected void bttSearch_Click(object sender, EventArgs e)
        {
            cmbAddToOrg.Items.Clear();
            cmbAddToOrg.DataSource = Lib.Security.Sec_UserAccountRoleBLL.SelectSearchAccountsAssignedToPackage(PackageID, txtSearch.Text.Trim());
            cmbAddToOrg.DataBind();
            cmbAddToOrg.Focus();
        }

        protected void bttAddPackageToOrg_Click(object sender, EventArgs e)
        {
            Guid accountID = ConvertUtility.ConvertToGuid(cmbAddToOrg.SelectedValue, Guid.Empty);
            if (accountID.IsNullOrEmptyGuid() || PackageID == 0) return;

            Lib.Security.Sec_UserMembership actingUser = LoginUtility.GetCurrentUserOrSignIn();
            //update the admin activity log
            LogAdminActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADDACC_ATTEMPT, getCurrentUserEmail, cmbAddToOrg.SelectedItem.Text, txtPackageName.Text.Trim());
            Lib.Package.PackageBLL.InsertPackageAssignedOrg(PackageID, accountID, actingUser.Email,null);
            //update the admin activity log
            LogAdminActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADDACC_SUCCESS, getCurrentUserEmail, cmbAddToOrg.SelectedItem.Text, txtPackageName.Text.Trim());
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }


        protected void gvAssignedOrganizations_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RemoveFromOrgCommand")
            {
                Guid accountID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                if (accountID.IsNullOrEmptyGuid() || PackageID == 0) return;

                GridViewRow row = (GridViewRow)(((System.Web.UI.WebControls.Button)e.CommandSource).NamingContainer);
                HyperLink hlAccountName = (HyperLink)row.FindControl("hlAccountName");
                System.Web.UI.WebControls.Label lblCompanyName = (System.Web.UI.WebControls.Label)row.FindControl("lblCompanyName");
                StringBuilder sbAccountName = new StringBuilder();
                if (lblCompanyName != null)
                    sbAccountName.Append(lblCompanyName.Text.Trim()+"->");
                if (hlAccountName != null)
                    sbAccountName.Append(hlAccountName.Text);

                //update the admin activity log
                LogAdminActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETEACC_ATTEMPT, getCurrentUserEmail, sbAccountName.ToString(), txtPackageName.Text.Trim());
                Lib.Package.PackageBLL.DeleteOrganizationPackageMapping(PackageID, accountID);
                //update the admin activity log
                LogAdminActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETEACC_SUCCESS, getCurrentUserEmail, sbAccountName.ToString(), txtPackageName.Text.Trim());
                WebUtility.HttpRedirect(null, Request.RawUrl);
            }
        }

        #endregion

        #region Private Methods

        private void InitializeManagePackageRoles()
        {
            //if the user is in "adm-mngfile-engg-uk-tau" then they can only select Engg Packages
           // if (Context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) return;
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) return;
            else if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU))
               {
                foreach (ListItem item in ddlSystem.Items)
                {
                    if (item.Text == Package.TauPackageTypes.EnginerringPackage)
                    {
                        item.Selected = true;
                        ddlSystem.Enabled = false;
                        break;
                    }
                }
            }
        }

        protected string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(getCurrentUserEmail, "Package", taskPerformed, action, WebUtility.GetUserIP(),ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
        }

        private String GetFileList(DataTable dtPackageFiles)
        {
            StringBuilder sbFilelist = new StringBuilder();
            if (dtPackageFiles == null || dtPackageFiles.Rows.Count == 0) return String.Empty;
            else
                for (int i = 0; i < dtPackageFiles.Rows.Count; i++)
                {
                    if (i != dtPackageFiles.Rows.Count - 1)
                        sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], "") + ",");
                    else
                        sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], ""));
                }
            return sbFilelist.ToString();
        }

        private DataTable GetModelTable(string modelList)
        {
            if (String.IsNullOrEmpty(modelList)) return null;
            String[] models = modelList.Split(';');
            if (models == null || models.Length == 0) return null;
            DataTable dtModels = new DataTable();
            dtModels.Columns.Add("ModelNumber");
            foreach (String model in models)
            {
                DataRow drModel = dtModels.NewRow();
                drModel["ModelNumber"] = model;
                dtModels.Rows.Add(drModel);
            }
            return dtModels;
        }

      

        private void BindSystems()
        {
            ddlSystem.DataSource = Lib.Package.PackageBLL.SelectPackageSystems();
            ddlSystem.DataBind();
            ddlSystem.Items.Insert(0, new ListItem("--Select--"));
        }

        /// <summary>
        /// Bind Package form info
        /// </summary>
        private void LoadPackageInfo()
        {

            Package objPackage = PackageBLL.SelectByPackageID(PackageID);
            if (objPackage == null)
            {
                cbxEnable.Enabled = false;
                pnlOrgInfo.Visible = pnlAddPackageToOrg.Visible = false;
                return;
            }
            if (PackageCreated)
            {
                lblMsg.Text = GetStaticResource("lblMsg_PackageCreated.Text");
                lblMsg.Visible = true;
            }
            txtPackageName.Text = objPackage.PackageName;
            txtPackagePath.Text = hfPackagePath.Value = objPackage.PackagePath;
            if (ConvertUtility.ConvertToInt32(objPackage.SystemID, 0) > 0)
                ddlSystem.SelectedValue = ConvertUtility.ConvertToString(objPackage.SystemID, "");
            cbxEnable.Checked = objPackage.Enabled;
            hfModels.Value = GetModelList(objPackage.ModelNumbers);
            if (String.IsNullOrEmpty(hfModels.Value))
                cbxEnable.Enabled = false;
            btnDeletePackage.Visible = true;
            ViewState["PackagePath"] = objPackage.PackagePath;

            gvAssignedOrganizations.DataSource = Lib.Package.PackageBLL.SelectOrganizationsByPackage(PackageID);
            gvAssignedOrganizations.DataBind();
        }

        private string GetModelList(DataTable dataTable)
        {
            if (dataTable == null || dataTable.Rows.Count == 0) return String.Empty;
            StringBuilder sbList = new StringBuilder();

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                if (i != dataTable.Rows.Count - 1)
                {
                    sbList.Append(Convert.ToString(dataTable.Rows[i]["ModelNumber"]));
                    sbList.Append(";");
                }
                else
                    sbList.Append(Convert.ToString(dataTable.Rows[i]["ModelNumber"]));
            }
            return sbList.ToString();
        }
        #endregion

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        public String GetUrl(object accid)
        {
            String accId = accid.ToString();
            String url = String.Format("/siteadmin/orguseradmin/orgdetail?accid={0}"
                         , HttpUtility.UrlEncode(accId));

            return url;
        }
    }
}