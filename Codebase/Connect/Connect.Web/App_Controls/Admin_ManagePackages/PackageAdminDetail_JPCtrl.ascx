﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PackageAdminDetail_JPCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ManagePackages.PackageAdminDetailJpCtrl" %>

<asp:PlaceHolder runat="server">
<link href="//dl.cdn-anritsu.com/appfiles/css/token-input.css" rel="stylesheet" />
<script type="text/javascript">
    var CHECKBOXID = '<%=cbxEnable.ClientID%>';
</script>
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/ui/1.9.2/jquery-ui.js" type="text/javascript"></script>
<link href="//ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="//dl.cdn-anritsu.com/appfiles/js/MyAnritsu_App_JS/jquery.tokeninput.js"></script>
<%--<script src="//gwdata.cdn-anritsu.com/appfiles/js/MyAnritsu_App_JS/suggest_model.js"></script>--%>
<script type="text/javascript">
    var txtPackagePath = '<%=txtPackagePath.ClientID%>';
    var hfPackagePath = '<%=hfPackagePath.ClientID%>';
    var txtPkgGrpName = "#<%=txtPkgGrpName.ClientID%>";
    var PackageAdminDetail_JPCtrlFlag = true;
    var _selectNumber = 0;
    var _all = false;
    var _handle = true;
    function OnGridSelectionChanged1(s, e) {
        if (e.isChangedOnServer == false) {
            if (e.isAllRecordsOnPage && e.isSelected)
                _selectNumber = s.GetVisibleRowsOnPage();
            else if (e.isAllRecordsOnPage && !e.isSelected)
                _selectNumber = 0;
            else if (!e.isAllRecordsOnPage && e.isSelected)
                _selectNumber++;
            else if (!e.isAllRecordsOnPage && !e.isSelected)
                _selectNumber--;
            if (_handle) {
                SelectAllCheckBox.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                _handle = false;
            }
            _handle = true;
        }
    }
    function OnGridEndCallback1(s, e) {
        _selectnumber = s.cpselectedrowsonpage;
    }
</script>
<script type="text/javascript" src="//dl.cdn-anritsu.com/appfiles/js/MyAnritsu_App_JS/support_and_packages.js"></script>

    </asp:PlaceHolder>

<div class="overflow margin-bottom-5">
    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/prodregadmin/productsupport/jpsupporttypeentry" Text='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,pnlContainer.HeaderText %>' CssClass="float-right" runat="server" SkinID="blueit"></asp:HyperLink>
</div>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow group input-text width-60 required">
        <label>
            <asp:Localize ID="lcalPackageName" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackageName.Text%>"></asp:Localize>*</label>
        <p class="error-message">
            <asp:Localize ID="lclPkgErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtPackageName" runat="server" ValidationGroup="vgPackageInfo"></asp:TextBox>
        <asp:HiddenField runat="server" ID="hfCopyPackageId" Value="" />
    </div>
    <div class="settingrow group input-text width-60 required">
        <label>
            <asp:Localize ID="lcalPkgGroupName" runat="server" Text="<%$Resources:ADM_STCTRL_JPPackageAdmin_DetailCtrl,lcalPkgGroupName.Text%>"></asp:Localize>*</label>
        <p class="error-message">
            <asp:Localize runat="server" ID="lclPkgGrpErr" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtPkgGrpName" runat="server" ValidationGroup="vgPackageInfo" placeholder="<%$Resources:Common,placeholderText %>"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvPkgGrpName" runat="server" ControlToValidate="txtPkgGrpName"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
    </div>

    <div class="overflow">
        <div class="settingrow group input-text width-60 required left">
            <table>
                <tr>
                    <td style="text-align: left; vertical-align: top">
                        <label>
                            <asp:Localize ID="lcalPackagePath" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackagePath.Text%>"></asp:Localize>*</label>
                    </td>
                    <td style="text-align: left; vertical-align: top">
                        <p class="error-message">
                            <asp:Localize runat="server" ID="lclPckPthErr" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                        </p>
                        <span class="msg">
                            <asp:Localize ID="lclPackagePathErr" runat="server" Visible="false" />
                        </span>
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
            <asp:TextBox ID="txtPackagePath" runat="server" Enabled="true" ValidationGroup="vgPackageInfo"></asp:TextBox>
            <asp:HiddenField ID="hfPackagePath" runat="server" Value="" />
        </div>
        <div class="width-30 left" style="padding-top: 23px; margin-left: 10px">
            <asp:Button OnClientClick="return browseFiles();" ID="btnBrowse" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalBrowse.Text %>" />
        </div>
        <%-- <asp:RequiredFieldValidator ID="rfvPackagePath" runat="server" ControlToValidate="txtPackagePath"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
    </div>
    <div class="settingrow input-select group width-60">
        <span class="settinglabel">
            <asp:Localize ID="lcalPckgType" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageType%>"></asp:Localize>*</span>
        <asp:DropDownList ID="ddlPckgType" runat="server" SkinID="ddl-210" DataTextField="SystemName" DataValueField="SystemID" AppendDataBoundItems="true">
        </asp:DropDownList>
    </div>
    <asp:RequiredFieldValidator ID="rfvSystem" runat="server" ControlToValidate="ddlPckgType"
        ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>" InitialValue="--Select--"></asp:RequiredFieldValidator>
    <div class="settingrow width-60 required input-text">
        <label>
            <asp:Localize ID="lcalAssignModel" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalAssignModel.Text%>"></asp:Localize>*</label>
        <div class="input_field_model">
            <asp:HiddenField ID="hfModels" runat="server" />
            <div class="modelArea">
                <input id="txtModels" name="txtModels" type="text" style="width: 300px">
            </div>
            
            <span class="required-field-validator" id="token-validator">Required</span>
            <asp:Literal ID="litAddKeyPressEvent" runat="server"></asp:Literal>
        </div>

    </div>
    <div class="settingrow group input-checkbox">
        <p>
            <label>
                <asp:CheckBox ID="cbxEnable" runat="server" Text=" " />
                <asp:Localize ID="lcalEnable" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalEnable.Text%>"></asp:Localize>

            </label>
        </p>
    </div>
    <div class="settingrow group">
        <label>
            <asp:Localize ID="lcalSupportType" runat="server" Text="<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalSupType.Text%>"></asp:Localize></label>
        <dx:ASPxRadioButtonList runat="server" ID="rblSupportTypes" RepeatColumns="2"></dx:ASPxRadioButtonList>
        <asp:RequiredFieldValidator ID="rfvSupType" runat="server" ControlToValidate="rblSupportTypes"
            ValidationGroup="vgPackageInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow group input-checkbox">
        <div class="chkaddpackage-section">
            <div class="chkaddpackage-wrapper">
                <dx:ASPxCheckBox runat="server" ID="chkaddpackage" ClientInstanceName="chkaddpackage_Client">
                </dx:ASPxCheckBox>
            </div>
            <asp:Localize ID="lcalAssignPackage" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,lcalAssignPackage.Text%>"></asp:Localize>
            <%--<div class="lcalAssignPackage-wrapper">
			    <asp:Localize ID="lcalAssignPackage" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,lcalAssignPackage.Text%>" ></asp:Localize>
			</div>--%>
        </div>
    </div>
    <div class="settingrow">
        <span class="settinglabel  error-msg">
            <asp:Localize ID="lcalErrorMsg" runat="server" Visible="false"></asp:Localize>
        </span>
    </div>
    <div class="settingrow">
        <span class="msg">
            <asp:Label ID="lblMsg" runat="server" Visible="false" ViewStateMode="Disabled"></asp:Label></span>
    </div>
    <div class="settingrow">
        <asp:Button ID="btnSavePackage" runat="server" OnClientClick="return validate();" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,btnSavePackage.Text%>"
            OnClick="btnSavePackage_Click" ValidationGroup="vgPackageInfo" SkinID="submit-btn" />
        <asp:Button ID="btnDeletePackage" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,btnDeletePackage.Text%>"
            OnClick="btnDeletePackage_Click" Visible="false" />

        <asp:Button ID="btnCopyPackage" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,btnCopyPackage.Text%>"
            OnClick="btnCopyPackage_OnClick" ValidationGroup="vgPackageInfo" SkinID="submit-btn" Visible="false" />

    </div>
</anrui:GlobalWebBoxedPanel>


<anrui:GlobalWebBoxedPanel ID="pnlSupportEntries" runat="server" ShowHeader="true" Visible="False" HeaderText="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,pnlSupportEntries.Text%>">
    <div class="settingrow">

        <anrui:GlobalWebBoxedPanel ID="pnlSearchFormForSupportTypeEntries" runat="server" ShowHeader="true" Visible="False" HeaderText="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gwpnlSearchOrg.HeaderText%>">

            <asp:HiddenField runat="server" ID="Gwid" />

            <div class="width-60">
                <div class="settingrow group">
                    <label style="line-height: 23px; margin-bottom: 5px">
                        <asp:CheckBox ID="cbxSearchByMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclModelName%>" TextAlign="Left" /></label>
                </div>
                <div class="input-text">
                    <asp:TextBox ID="txtSearchByMN" runat="server" onkeydown="if (window.event.keyCode==13){ return false}"></asp:TextBox>
                </div>
                <div class="settingrow group">
                    <label style="line-height: 23px; margin-bottom: 5px">
                        <asp:CheckBox ID="cbxSearchBySN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclSerial%>" TextAlign="Left" /></label>
                </div>
                <div class="input-text">
                    <asp:TextBox ID="txtSearchBySN" runat="server" onkeydown="if (window.event.keyCode==13){ return false}"></asp:TextBox>
                </div>

                <div class="settingrow group">
                    <label style="line-height: 23px; margin-bottom: 5px">
                        <asp:CheckBox ID="cbxSearchByEmail" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclEmail%>" TextAlign="Left" /></label>
                </div>
                <div class="input-text">
                    <asp:TextBox ID="txtSearchByEmail" runat="server" onkeydown="if (window.event.keyCode==13){ return false}"></asp:TextBox>
                </div>
                <br />

                <table>
                    <tr>
                        <td style="width: 33%"></td>
                        <td style="width: 33%; text-align: center">
                            <div class="settingrow">
                                <span class="settinglabelrgt-15">&nbsp;</span>
                                <span class="msg">
                                    <%--<span class="settinglabel  error-msg">--%>
                                    <asp:Localize ID="lblErrorMsg" runat="server" Visible="false"></asp:Localize>
                                </span>
                            </div>

                        </td>
                        <td style="width: 33%; text-align: right">
                            <asp:Button ID="bttSupportTypeSearch" runat="server" Text="検索" CausesValidation="false" OnClick="bttSupportTypeSearch_Click" />
                        </td>
                    </tr>
                </table>
            </div>

        </anrui:GlobalWebBoxedPanel>


        <fieldset id="fsConfigure" style="position: relative;">
            <legend>
                <asp:Localize ID="lcalSupportEntries" runat="server" Text="Support Types"></asp:Localize></legend>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                <tr>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnSelectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnSelectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="False">
                            <ClientSideEvents Click="function(s, e) { grid.SelectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td style="padding-right: 4px">

                        <dx:ASPxButton ID="btnUnselectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnUnselectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="False">
                            <ClientSideEvents Click="function(s, e) { grid.UnselectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
            <div style="float: right; color: #f5a623">
                <asp:Label runat="server" ID="ltrEntriesMsg" Visible="false"></asp:Label>
            </div>
            <dx:ASPxGridView ID="gvSupportEntries" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" EnableViewState="False"
                OnDataBound="gvSupportEntries_OnDataBound"
                OnBeforePerformDataSelect="gvSupportEntries_OnBeforePerformDataSelect" SettingsPager-PageSize="20" AutoGenerateColumns="False" KeyFieldName="Id">
                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true" />
                <Columns>
                    <dx:GridViewCommandColumn Name="Select" ShowSelectCheckbox="True" VisibleIndex="0">
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ToolTip="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,SelectAllCheckBox.Text%>"
                                ClientSideEvents-CheckedChanged="function(s, e) { grid.SelectAllRowsOnPage(s.GetChecked()); }" ClientInstanceName="SelectAllCheckBox"
                                OnInit="SelectAllCheckBox_Init" />
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="Id" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="Selected" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalModelNumber.Text%>" FieldName="ModelNumber" Width="350" SortIndex="1"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalSerialNumber.Text%>" Width="400" SortIndex="2"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="EmailAdd_Org" VisibleIndex="3" Caption="<%$Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,EmailAddress.Text%>" Width="400" SortIndex="3"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="MemberShipId" SortIndex="3" Visible="False"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="AccountId" SortIndex="3" Visible="False"></dx:GridViewDataColumn>

                    <dx:GridViewDataColumn VisibleIndex="4" Width="50" Caption="<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalSupType.Text%>" SortIndex="4">
                        <DataItemTemplate>
                            <dx:ASPxLabel ID="dxlblSisProductName" runat="server" Text='<%# Eval("SupportTypeCode")+" "+((Eval("SuppDate")!=null)?Convert.ToDateTime(Eval("SuppDate")).ToShortDateString():"Not Set") %>'></dx:ASPxLabel>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <SettingsPager PageSize="20"></SettingsPager>
                <Settings ShowFilterRow="False" />
                <ClientSideEvents SelectionChanged="OnGridSelectionChanged1" EndCallback="OnGridEndCallback1" />
            </dx:ASPxGridView>
            <div class="settingrow" style="margin-top: 20px">
                <asp:Button ID="btnConfigure" Text="Save" runat="server"
                    OnClick="btnSave_Click" />

            </div>
            <div class="settingrow">
                <span class="msg">
                    <asp:Literal ID="ltrSaveMsg" Visible="False" EnableViewState="False" runat="server"></asp:Literal>
                </span>
            </div>
        </fieldset>
    </div>

</anrui:GlobalWebBoxedPanel>

