﻿/*
 * Author:        kishore kumar M
 * Created Date:  15/10/2014
 * Modified Date: 
 * Modified By:   kishore kumar M
 * Purpose:       Interface to manage Japan Packages and associated Users
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.AnrCommon.GeoLib.DAL;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Data.Linq;
using DevExpress.Web;
using DevExpress.XtraPrinting.Native;
using Newtonsoft.Json.Linq;
using EFHelper = Anritsu.Connect.EntityFramework.ContextHelper;
using Anritsu.Connect.EntityFramework;
using System.Web;


namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public partial class PackageAdminDetailJpCtrl : Awss3SettingsCtrlBase
    {
        public const String AreaEffected = "Package-JP";
        public long PackageId
        {
            get
            {
                return ConvertUtility.ConvertToInt64(Request.QueryString[KeyDef.QSKeys.PackageId], 0);
            }
        }

        public Boolean PackageCreated
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[KeyDef.QSKeys.Package_Success], false);
            }
        }
        public Boolean PackageCopied
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[KeyDef.QSKeys.Package_Copied], false);
            }
        }

        public String ReturnUrl
        {
            get
            {
                return Request.QueryString[KeyDef.QSKeys.ReturnURL];
            }
        }

        private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_JPPackageAdmin_DetailCtrl"; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            txtPackagePath.Text = hfPackagePath.Value;
            btnCopyPackage.Visible = (PackageId != 0);
            lcalErrorMsg.Visible = false;
            lblErrorMsg.Visible = false;
            lclPackagePathErr.Visible = false;
            btnDeletePackage.Attributes.Add("onclick", "return ConfirmOnDeleteOrRemove('" + GetStaticResource("ConfirmDeleteMsg") + "')");

            var js = "var currentPath = $(\"#" + txtPackagePath.ClientID + "\").val();\r\n";
            js += "$(\"#" + hfPackagePath.ClientID + "\").val(currentPath);\r\n";
            txtPackagePath.Attributes.Add("onkeyup", js);

            if (!IsPostBack)
            {
                BindSystems();
                BindSupportTypes();
                LoadPackageInfo();
            }

            // Bind KeyPress Event.
            litAddKeyPressEvent.Text = AddKeyPressEventByJavaScript();
        }
        private void BindSupportTypes()
        {

            var suptypes = from t in EFHelper.SuportType.GetSupportTypes(ModelConfigTypeInfo.CONFIGTYPE_JP)
                           select new
                           {
                               t.SupportTypeCode,
                               description = t.SupportTypeCode + t.Description
                           };
            rblSupportTypes.DataSource = suptypes;
            rblSupportTypes.ValueField = "SupportTypeCode";
            rblSupportTypes.TextField = "description";
            rblSupportTypes.DataBind();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            txtPackagePath.Text = hfPackagePath.Value;
            pnlSupportEntries.Visible = (chkaddpackage.Checked && PackageId > 0);
            pnlSearchFormForSupportTypeEntries.Visible = pnlSupportEntries.Visible;
            this.Page.Form.DefaultButton = btnSavePackage.UniqueID;
            if (chkaddpackage.Checked && PackageId > 0)
                gvSupportEntries.DataBind();
            //unbind the checked event if the package is not in edit mode
            if (PackageId > 0)
            {
                chkaddpackage.CheckedChanged += new EventHandler(chkaddpackage_OnCheckdChange);
                chkaddpackage.AutoPostBack = true;
            }
        }

        #region PostBack Events

        protected void btnSavePackage_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            lcalErrorMsg.Visible = false;
            if (PackageId == 0)
            {
                //pull the files under the package
                var s3Objects = PackageBLL.GetPackaeFileList(hfPackagePath.Value.Trim(), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
                var filtered = from item in s3Objects
                               select new { item.Key, item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                if (s3Objects.Count == 0)
                {
                    //Raise no Files under selected package error message
                    lcalErrorMsg.Visible = true;
                    cbxEnable.Enabled = !(String.IsNullOrEmpty(hfModels.Value));
                    lcalErrorMsg.Text = GetStaticResource("lcalNoFilesErrorMsg.Text");
                    lclPackagePathErr.Visible = true;
                    lclPackagePathErr.Text = GetStaticResource("lclPackagePathErr.Text");
                    return;
                }

                if (cbxEnable.Checked && String.IsNullOrEmpty(hfModels.Value))
                {
                    //Raise error message (cannot make it active without attaching any model number)
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalNoModelAttachedErrorMsg.Text");
                    return;
                }

                // Is Model Exist?
                String[] models = hfModels.Value.Split(';').Where(w => !String.IsNullOrEmpty(w)).Select(s => s).ToArray();
                if (models.Length == 0)
                {
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalNoModelAttachedErrorMsg.Text");
                    return;
                }
                if (IsModelExist(models) == false)
                {
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalModelIsNotExist");
                    return;
                }


                //create Files list
                DataTable dtPackageFiles = ToFileListDataTable(filtered);
                //create Package models
                DataTable dtPackageModel = GetModelTable(hfModels.Value);

                //checking duplicate package names
                if (txtPackageName.Text.Length > 0)
                {
                    var exist = Convert.ToBoolean(BLL_PackageFile.GetPackageName(txtPackageName.Text.Trim()));
                    if (exist)
                    {
                        lcalErrorMsg.Visible = true;
                        lcalErrorMsg.Text = GetStaticResource("PkgNameExist");
                        return;
                    }
                }

                //construct Package Object
                var objPackage = new Package(0, txtPackageName.Text.Trim(),
                      hfPackagePath.Value.Trim(),
                      ConvertUtility.ConvertToInt32(ddlPckgType.SelectedValue, 0), cbxEnable.Checked,
                      ModelConfigTypeInfo.CONFIGTYPE_JP, ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), txtPkgGrpName.Text.Trim());

                //update the admin activity log
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADD_ATTEMPT, objPackage.PackageName);

                long packageIdReturned = PackageBLL.InsertPackageWithFiles(objPackage, dtPackageFiles, LoginUtility.GetCurrentUser(true).Email, dtPackageModel, chkaddpackage.Checked);
                //update the admin activity log with success info
                if (packageIdReturned > 0)
                {

                    //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SelAll", "<script type='text/javascript'> function(s, e) { grid.SelectRows(); }</script>");
                    //update the admin activity log
                    LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADD_SUCCESS, objPackage.PackageName, objPackage.PackagePath, GetFileList(dtPackageFiles));
                    //redirect after success
                    String url;
                    if (String.IsNullOrEmpty(ReturnUrl)) url = String.Format("{0}?{1}={2}&pack_success=true", Request.RawUrl, KeyDef.QSKeys.PackageId, packageIdReturned);
                    else url = ReturnUrl.Contains("?") ? ReturnUrl + "&pack_success=true" : ReturnUrl + "?pack_success=true";
                    WebUtility.HttpRedirect(null, url);
                }
            }
            //update package
            else
            {
                // Is Model Exist?
                String[] models = hfModels.Value.Split(';').Where(w => !String.IsNullOrEmpty(w)).Select(s => s).ToArray();
                if (models.Length == 0)
                {
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalNoModelAttachedErrorMsg.Text");
                    return;
                }
                if (IsModelExist(models) == false)
                {
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetStaticResource("lcalModelIsNotExist");
                    return;
                }

                //create Package models
                var dtPackageModel = GetModelTable(hfModels.Value);
                if (ViewState["PackagePath"] != null && (Convert.ToString(ViewState["PackagePath"]) == hfPackagePath.Value))
                {
                    //update only package info
                    var objPackage = new Package(PackageId, txtPackageName.Text.Trim(), hfPackagePath.Value.Trim(), ConvertUtility.ConvertToInt32(ddlPckgType.SelectedValue, 0), cbxEnable.Checked, ModelConfigTypeInfo.CONFIGTYPE_JP, ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), txtPkgGrpName.Text.Trim());

                    //update the admin activity log
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_ATTEMPT, objPackage.PackageName);

                    PackageBLL.UpdatePackage(objPackage, LoginUtility.GetCurrentUser(true).Email, dtPackageModel, txtPkgGrpName.Text);

                    //update the admin activity log
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_SUCCESS, objPackage.PackageName, objPackage.PackagePath, "NO FILES UPDATED");

                    lblMsg.Text = GetStaticResource("lblMsg_PackageUpdated.Text");
                    lblMsg.Visible = true;
                }
                else
                {
                    //pull the files under the package
                    var s3Objects = PackageBLL.GetPackaeFileList(hfPackagePath.Value.Trim(), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
                    var filtered = from item in s3Objects
                                   select new { item.Key, item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                    if (s3Objects.Count == 0)
                    {
                        //Raise error message
                        lcalErrorMsg.Visible = true;
                        lcalErrorMsg.Text = GetStaticResource("lcalNoFilesErrorMsg.Text");

                        lclPackagePathErr.Visible = true;
                        lclPackagePathErr.Text = GetStaticResource("lclPackagePathErr.Text");
                        return;
                    }
                    //create Files list
                    DataTable dtPackageFiles = ToFileListDataTable(filtered);

                    //construct Package Object
                    var objPackage = new Package
                    {
                        PackageID = PackageId,
                        PackageName = txtPackageName.Text.Trim(),
                        PackagePath = hfPackagePath.Value.Trim(),
                        SystemID = ConvertUtility.ConvertToInt32(ddlPckgType.SelectedValue, 0),
                        Enabled = cbxEnable.Checked,
                        ModelConfigType = ModelConfigTypeInfo.CONFIGTYPE_JP,
                        PackageGroupName = txtPkgGrpName.Text,
                        SupportTypeCode = ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value),
                        CopyFromPackageId = ConvertUtility.ConvertToInt32(hfCopyPackageId.Value, 0)
                    };

                    //update the admin activity log
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_ATTEMPT, objPackage.PackageName);

                    PackageBLL.UpdatePackageWithFiles(objPackage, dtPackageFiles, LoginUtility.GetCurrentUser(true).Email, dtPackageModel);

                    try
                    {
                        PackageBLL.CopyPackageComments(objPackage.PackageID, objPackage.CopyFromPackageId, false);
                    }
                    catch (Exception)
                    {
                        Lib.AppLog.LogError(Server.GetLastError(), HttpContext.Current);
                    }

                    //update the admin activity log
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.PACKAGE_MODIFY_SUCCESS, objPackage.PackageName, objPackage.PackagePath, GetFileList(dtPackageFiles));

                    lblMsg.Text = GetStaticResource("lblMsg_PackageUpdated.Text");
                    lblMsg.Visible = true;
                }
                if (!chkaddpackage.Checked)
                {
                    //delete the package association with all SupportEntries
                    BLL_SupportEntry.DeleteUserPackageAccess(PackageId);
                }
            }
        }
        public void CopyPackage()
        {
            lcalErrorMsg.Visible = false;

            var objPackage = new Package(PackageId, string.Format("Copy of {0}", txtPackageName.Text.Trim()),
                    hfPackagePath.Value.Trim(),
                    ConvertUtility.ConvertToInt32(ddlPckgType.SelectedValue, 0), false,
                    ModelConfigTypeInfo.CONFIGTYPE_JP, ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), txtPkgGrpName.Text.Trim(), PackageId);
            var s3Objects = PackageBLL.GetPackaeFileList(hfPackagePath.Value.Trim(), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
            var filtered = from item in s3Objects
                           select new { item.Key, item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
            if (s3Objects.Count == 0)
            {
                //Raise error message
                lcalErrorMsg.Visible = true;
                lcalErrorMsg.Text = GetStaticResource("lcalNoFilesErrorMsg.Text");
                lclPackagePathErr.Visible = true;
                lclPackagePathErr.Text = GetStaticResource("lclPackagePathErr.Text");
            }
            // Is Model Exist?
            String[] models = hfModels.Value.Split(';').Where(w => !String.IsNullOrEmpty(w)).Select(s => s).ToArray();
            if (models.Length == 0)
            {
                lcalErrorMsg.Visible = true;
                lcalErrorMsg.Text = GetStaticResource("lcalNoModelAttachedErrorMsg.Text");
                return;
            }
            if (IsModelExist(models) == false)
            {
                lcalErrorMsg.Visible = true;
                lcalErrorMsg.Text = GetStaticResource("lcalModelIsNotExist");
                return;
            }

            var dtPackageModel = GetModelTable(hfModels.Value);
            //create Files list
            DataTable dtPackageFiles = ToFileListDataTable(filtered);
            if (dtPackageFiles.Rows.Count.Equals(0))
            {
                lcalErrorMsg.Text = GetStaticResource("lcalNoFilesErrorMsg.Text");
                lclPackagePathErr.Visible = true;
                lclPackagePathErr.Text = GetStaticResource("lclPackagePathErr.Text");
                return;
            }
            //checking duplicate package names
            if (objPackage.PackageName.Length > 0)
            {
                var exist = Convert.ToBoolean(BLL_PackageFile.GetPackageName(objPackage.PackageName.Trim()));
                if (exist)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = ConvertUtility.ConvertToString(GetGlobalResourceObject("ADM_STCTRL_SupportTypeAdmin", "lblMsg_PackageAlreadyCopied.Text"), "This package already copied. Change package name and copy.");
                    return;
                }
            }

            long packageIdReturned = PackageBLL.InsertPackageWithFiles(objPackage, dtPackageFiles, LoginUtility.GetCurrentUser(true).Email, dtPackageModel, false);
            //copy the comments
            try
            {
                PackageBLL.CopyPackageComments(packageIdReturned, PackageId, true);
            }
            catch (Exception)
            {
                Lib.AppLog.LogError(Server.GetLastError(), HttpContext.Current);
            }

            if (packageIdReturned > 0)
            {
                if (chkaddpackage.Checked)
                {
                    // Get All SupportType List
                    int totSelected = 0;
                    string modelBySearchForm = null;
                    string serialBySearchForm = null;
                    string emailBySearchForm = null;
                    var allSelectedSupportTypeList = ((List<ProductSupportEntity.ProductSupportListType>)BLL_SupportEntry.GetProductSupportList(ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), null, PackageId, hfModels.Value, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm)).Where(w => w.Selected).Select(s => s.Id).ToList();

                    // Get Searched SupportType List
                    if (cbxSearchByMN.Checked)
                        modelBySearchForm = txtSearchByMN.Text.Trim();
                    if (cbxSearchBySN.Checked)
                        serialBySearchForm = txtSearchBySN.Text.Trim();
                    if (cbxSearchByEmail.Checked)
                        emailBySearchForm = txtSearchByEmail.Text.Trim();
                    var searchedSupportTypeList = ((List<ProductSupportEntity.ProductSupportListType>)BLL_SupportEntry.GetProductSupportList(ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), null, PackageId, hfModels.Value, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm)).Select(s => s.Id).ToList();

                    //get the checked/selected ids
                    List<object> tmpCurrentSupportEntriesIdlist = gvSupportEntries.GetSelectedFieldValues("Id");
                    if (tmpCurrentSupportEntriesIdlist == null || searchedSupportTypeList == null || allSelectedSupportTypeList == null)
                    {
                        lblMsg.Visible = true;
                        lblMsg.Text = "Support types copy failed.";
                        return;
                    }
                    var currentSupportEntriesIdlist = tmpCurrentSupportEntriesIdlist.Cast<int>().ToList();

                    // Merge Selected SupportType List
                    var list = allSelectedSupportTypeList.Except(searchedSupportTypeList.Except(currentSupportEntriesIdlist)).Union(currentSupportEntriesIdlist).ToList();

                    var dtEntries = GetSupportEntriesTable(list);
                    PackageBLL.BulkUpdateUserPackageAccess(dtEntries, packageIdReturned);
                }

                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SelAll", "<script type='text/javascript'> function(s, e) { grid.SelectRows(); }</script>");
                //update the admin activity log
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADD_SUCCESS, objPackage.PackageName, objPackage.PackagePath, GetFileList(dtPackageFiles));
                //redirect after success
                String url;
                if (String.IsNullOrEmpty(ReturnUrl)) url = String.Format("{0}?{1}={2}&pack_copied=true", Request.Path, KeyDef.QSKeys.PackageId, packageIdReturned);
                else url = ReturnUrl.Contains("?") ? ReturnUrl + "&pack_copied=true" : ReturnUrl + "?pack_copied=true";
                WebUtility.HttpRedirect(null, url);
            }

        }

        public DataTable ToFileListDataTable(dynamic s3Objects)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Key");
            dataTable.Columns.Add("LastModified");
            dataTable.Columns.Add("Size");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            foreach (var n in s3Objects)
            {
                var dr = dataTable.NewRow();
                dr["Key"] = n.Key;
                dr["LastModified"] = n.LastModified.ToUniversalTime();
                dr["Size"] = n.Size;
                dataTable.Rows.Add(dr);
            }
            Thread.CurrentThread.CurrentCulture = currentCulture;
            return dataTable;
        }

        protected void btnDeletePackage_Click(object sender, EventArgs e)
        {
            //update the admin activity log
            LogAdminActivity(AreaEffected, TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETE_ATTEMPT, txtPackageName.Text.Trim());
            PackageBLL.DeletePackage(PackageId);
            //update the admin activity log
            LogAdminActivity(AreaEffected, TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETE_SUCCESS, txtPackageName.Text.Trim());
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.PackageAdmin_List_Jp);
        }

        protected void chkaddpackage_OnCheckdChange(object sender, EventArgs e)
        {
            //pnlSupportEntries.Visible = (chkaddpackage.Checked && PackageId>0);
            if (!chkaddpackage.Checked)
            {
                pnlSupportEntries.Visible = false;
                pnlSearchFormForSupportTypeEntries.Visible = false;
            }
        }

        protected void gvSupportEntries_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            // Get Model/Serial/Email datas at the Search form
            string modelBySearchForm = null;
            if (cbxSearchByMN.Checked)
            {
                modelBySearchForm = txtSearchByMN.Text.Trim();
            }

            string serialBySearchForm = null;
            if (cbxSearchBySN.Checked)
            {
                serialBySearchForm = txtSearchBySN.Text.Trim();
            }

            string emailBySearchForm = null;
            if (cbxSearchByEmail.Checked)
            {
                emailBySearchForm = txtSearchByEmail.Text.Trim();
            }

            // Get SupportType List
            int totSelected = 0;
            List<ProductSupportEntity.ProductSupportListType> supportList = BLL_SupportEntry.GetProductSupportList(ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), null, PackageId, hfModels.Value, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm);
            gvSupportEntries.DataSource = supportList;
            ltrEntriesMsg.Text = totSelected + " records are selected out of " + Convert.ToInt32(supportList.Count()) + " records";
            pnlSupportEntries.Visible = true;
            pnlSearchFormForSupportTypeEntries.Visible = true;
        }

        private void InitHieenModelsField()
        {
            String[] assignedModels = new String[] { };
            GetFormValue(ref assignedModels);
            var allModels = String.Empty;
            assignedModels.ForEach(f =>
            {
                if (!String.IsNullOrEmpty(f))
                    allModels += f + ";";
            });
            if (!String.IsNullOrEmpty(allModels))
            {
                hfModels.Value = allModels;
            }
        }

        protected void gvSupportEntries_OnDataBound(object sender, EventArgs e)
        {
            for (int i = 0; i < gvSupportEntries.VisibleRowCount; i++)
            {
                var goldenNugget = (Boolean)gvSupportEntries.GetRowValues(i, new string[] { "Selected" });
                if (goldenNugget)
                {
                    gvSupportEntries.Selection.SelectRow(i);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // Get All SupportType List
            int totSelected = 0;
            string modelBySearchForm = null;
            string serialBySearchForm = null;
            string emailBySearchForm = null;
            var allSelectedSupportTypeList = ((List<ProductSupportEntity.ProductSupportListType>)BLL_SupportEntry.GetProductSupportList(ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), null, PackageId, hfModels.Value, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm)).Where(w=>w.Selected).Select(s => s.Id).ToList();

            // Get Searched SupportType List
            if (cbxSearchByMN.Checked)
                modelBySearchForm = txtSearchByMN.Text.Trim();

            if (cbxSearchBySN.Checked)
                serialBySearchForm = txtSearchBySN.Text.Trim();

            if (cbxSearchByEmail.Checked)
                emailBySearchForm = txtSearchByEmail.Text.Trim();

            var searchedSupportTypeList = ((List<ProductSupportEntity.ProductSupportListType>)BLL_SupportEntry.GetProductSupportList(ConvertUtility.ConvertNullToEmptyString(rblSupportTypes.SelectedItem.Value), null, PackageId, hfModels.Value, out totSelected, modelBySearchForm, serialBySearchForm, emailBySearchForm)).Select(s => s.Id).ToList();


            //get the checked/selected ids
            List<object> tmpCurrentSupportEntriesIdlist = gvSupportEntries.GetSelectedFieldValues("Id");
            if (tmpCurrentSupportEntriesIdlist == null || searchedSupportTypeList == null || allSelectedSupportTypeList == null)
            {
                ltrSaveMsg.Visible = true;
                ltrSaveMsg.Text = "Support types save failed!";
                return;
            }
            var currentSupportEntriesIdlist = tmpCurrentSupportEntriesIdlist.Cast<int>().ToList();

            // Merge Selected SupportType List
            var list = allSelectedSupportTypeList.Except(searchedSupportTypeList.Except(currentSupportEntriesIdlist)).Union(currentSupportEntriesIdlist).ToList();

            //send the entries table and package id to update the user access list
            var dtEntries = GetSupportEntriesTable(list);          
            ltrSaveMsg.Visible = PackageBLL.BulkUpdateUserPackageAccess(dtEntries, PackageId);
            ltrSaveMsg.Text = "Support types are saved sucessfully!";
        }

        protected void SelectAllCheckBox_Init(object sender, EventArgs e)
        {
            var chk = sender as ASPxCheckBox;
            Boolean cbChecked = true;
            //get the naming container
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            Int32 start = grid.VisibleStartIndex;
            Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);
            for (int i = start; i < end; i++)
            {
                if (!grid.Selection.IsRowSelected(i))
                {
                    cbChecked = false;
                    break;
                }
            }
            chk.Checked = cbChecked;
        }

        protected void bttSupportTypeSearch_Click(object sender, EventArgs e)
        {
            if (!cbxSearchByMN.Checked && !cbxSearchBySN.Checked && !cbxSearchByEmail.Checked)
            {
                //Error
                lblErrorMsg.Text = GetStaticResource("lclErrorSearchFormIsEmpty");
                lblErrorMsg.Visible = true;
            }

            gvSupportEntries.DataSource = null;
            pnlSupportEntries.Visible = true;
            pnlSearchFormForSupportTypeEntries.Visible = true;
        }
        #endregion
        #region Private Methods

        private DataTable GetSupportEntriesTable(List<int> list)
        {
            var dataTable = new DataTable();
            if (!list.Any()) return null;
            dataTable.Columns.Add("SupportEntryId");
            foreach (var id in list)
            {
                var dr = dataTable.NewRow();
                dr["SupportEntryId"] = id;
                dataTable.Rows.Add(dr);
            }
            return dataTable;
        }

        protected string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        private void LogAdminActivity(String areEffected, String action, String taskPerformed, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            ActivityLogBLL.Insert(getCurrentUserEmail, areEffected, taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
        }

        public String GetFileList(DataTable dtPackageFiles)
        {
            var sbFilelist = new StringBuilder();
            if (dtPackageFiles == null || dtPackageFiles.Rows.Count == 0) return String.Empty;
            for (var i = 0; i < dtPackageFiles.Rows.Count; i++)
            {
                if (i != dtPackageFiles.Rows.Count - 1)
                    sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], "") + ",");
                else
                    sbFilelist.Append(ConvertUtility.ConvertToString(dtPackageFiles.Rows[i]["Key"], ""));
            }
            return sbFilelist.ToString();
        }

        public DataTable GetModelTable(string modelList)
        {
            if (String.IsNullOrEmpty(modelList)) return null;
            String[] models = modelList.Split(';').Where(w => !String.IsNullOrEmpty(w)).Select(s => s).ToArray();
            if (models.Length == 0) return null;
            var dtModels = new DataTable();
            dtModels.Columns.Add("ModelNumber");
            foreach (String model in models)
            {
                DataRow drModel = dtModels.NewRow();
                drModel["ModelNumber"] = model;
                dtModels.Rows.Add(drModel);
            }
            return dtModels;
        }

        private void BindSystems()
        {
            ddlPckgType.DataSource = EFHelper.PackageTypes.GetPackageTypes(ModelConfigTypeInfo.CONFIGTYPE_JP);
            ddlPckgType.DataBind();
            ddlPckgType.Items.Insert(0, new ListItem("--Select--"));
        }

        /// <summary>
        /// Bind Package form info
        /// </summary>
        private void LoadPackageInfo()
        {
            Package objPackage = PackageBLL.SelectByPackageID(PackageId);
            if (objPackage == null)
            {
                cbxEnable.Enabled = false;
                btnCopyPackage.Visible = false;
                return;
            }
            if (PackageCreated)
            {
                lblMsg.Text = GetStaticResource("lblMsg_PackageCreated.Text");
                lblMsg.Visible = true;
            }

            if (PackageCopied)
            {

                lblMsg.Text = ConvertUtility.ConvertToString(GetGlobalResourceObject("ADM_STCTRL_SupportTypeAdmin", "lblMsg_PackageCopied.Text"), "Package Copied.");
                lblMsg.Visible = true;
            }
            txtPackageName.Text = objPackage.PackageName;
            hfCopyPackageId.Value = ConvertUtility.ConvertToString(objPackage.CopyFromPackageId, "0");
            txtPkgGrpName.Text = objPackage.PackageGroupName;
            txtPackagePath.Text = hfPackagePath.Value = objPackage.PackagePath;
            if (ConvertUtility.ConvertToInt32(objPackage.SystemID, 0) > 0)
                ddlPckgType.SelectedValue = ConvertUtility.ConvertToString(objPackage.SystemID, "");
            cbxEnable.Checked = objPackage.Enabled;
            hfModels.Value = GetModelList(objPackage.ModelNumbers);
            if (String.IsNullOrEmpty(hfModels.Value))
                cbxEnable.Enabled = false;
            rblSupportTypes.Value = objPackage.SupportTypeCode;
            btnCopyPackage.Visible = btnDeletePackage.Visible = true;
            ViewState["PackagePath"] = objPackage.PackagePath;
            chkaddpackage.Visible = lcalAssignPackage.Visible = (PackageId > 0);
            chkaddpackage.Checked = (PackageId > 0 && BLL_SupportEntry.UserLevelPackageAccess(PackageId, rblSupportTypes.Value.ToString()));
        }
        private static string GetModelList(DataTable dataTable)
        {
            if (dataTable == null || dataTable.Rows.Count == 0) return String.Empty;
            var sbList = new StringBuilder();

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                if (i != dataTable.Rows.Count - 1)
                {
                    sbList.Append(Convert.ToString(dataTable.Rows[i]["ModelNumber"]));
                    sbList.Append(";");
                }
                else
                    sbList.Append(Convert.ToString(dataTable.Rows[i]["ModelNumber"]));
            }
            return sbList.ToString();
        }
        #endregion

        private string AddKeyPressEventByJavaScript()
        {
            string js;
            // Init Model TextBox
            InitHieenModelsField();
            String[] models;
            String tmpJs = String.Empty;
            if (hfModels.Value != null)
            {
                models = hfModels.Value.Split(';').Where(w => !String.IsNullOrEmpty(w)).Select(s => s).ToArray();
                if (models.Length == 1)
                {
                    tmpJs = "$(\"#txtModels\").val('" + models[0] + "');\r\n";
                }
                else if (models.Length > 1)
                {
                    for (int i = 0; i < models.Length; i++)
                    {
                        if (String.IsNullOrEmpty(models[i]))
                            continue;

                        //if (i == 0)
                        //{
                        //    tmpJs = "$(\"#txtModels\").val('" + models[0] + "');\r\n";
                        //}
                        //else
                        //{
                        tmpJs += "var h = '<div><input type=\"text\" name=\"models[]\" value=\"" + models[i] + "\" Style=\"width:300px;background-color:#F7F7F7\" readonly=\"readonly\" /> <a href=\"#\" class=\"remove_field\">X</a></div>';\r\n";
                        tmpJs += "$(\".modelArea\").append(h);\r\n";
                        //}
                    }

                }
                else
                {
                    // empty. nothing to do.
                }
            }


            js = "<script type=\"text/javascript\">\r\n";
            js += "function addKeyPressEventByJavaScript() {\r\n";

            js += tmpJs;

            js += "var modelWrapper = $(\".input_field_model\");\r\n";
            js += "$(modelWrapper).on(\"click\", \".remove_field\", function (e) {\r\n";
            js += "e.preventDefault();\r\n";
            js += "$(this).parent('div').remove();\r\n";
            js += "})\r\n";
            js += "\r\n";
            js += "$(modelWrapper).keypress(function (e) {\r\n";
            js += "// Enter Key\r\n";
            js += "if (e.which == 13) {\r\n";
            js += "e.preventDefault();\r\n";
            js += "\r\n";
            js += "var inputModelName = $(\"#txtModels\").val();\r\n";
            js += "var h = '<div><input type=\"text\" name=\"models[]\" value=\"' + inputModelName + '\" Style=\"width:300px;background-color:#F7F7F7\" readonly=\"readonly\"/><a href=\"#\" class=\"remove_field\">X</a></div>';\r\n";
            js += "$(\".modelArea\").append(h);\r\n";
            js += "\r\n";
            js += "// Init input word\r\n";
            js += "$(\"#txtModels\").val(\"\");\r\n";
            js += "\r\n";
            js += "// Input hidden field\r\n";
            js += "var modelNameHistory = $(\"#" + hfModels.ClientID + "\").val();\r\n";
            js += "if (modelNameHistory == inputModelName) return false;\r\n";
            js += "var newModelName = modelNameHistory + ';' + inputModelName;\r\n";
            js += "$(\"#" + hfModels.ClientID + "\").val(newModelName);\r\n";
            js += "return false;\r\n";
            js += "}\r\n";
            js += "});\r\n";

            js += "$(\"#" + btnSavePackage.ClientID + "\").on( 'click', function(){\r\n";
            js += "var ss = sessionStorage;\r\n";
            js += "if( ss ){\r\n";
            js += "var initModelHtml = ss.getItem( 'initModelHtml' );\r\n";
            js += "var lastModelHtml = $(\".modelArea\").html();\r\n";
            js += "var lastModelTextBoxValue = $(\"#txtModels\").val();\r\n";
            js += "lastModelHtml = lastModelHtml.replace( initModelHtml, \"\" );\r\n";
            js += "ss.setItem( 'lastModelHtml', lastModelHtml );\r\n";
            js += "ss.setItem( 'lastModelTextBoxValue', lastModelTextBoxValue );\r\n";
            js += "}\r\n";
            js += "});\r\n";

            js += "$(\"#" + btnDeletePackage.ClientID + "\").on( 'click', function(){\r\n";
            js += "var ss = sessionStorage;\r\n";
            js += "if( ss ){\r\n";
            js += "var initModelHtml = ss.getItem( 'initModelHtml' );\r\n";
            js += "var lastModelHtml = $(\".modelArea\").html();\r\n";
            js += "var lastModelTextBoxValue = $(\"#txtModels\").val();\r\n";
            js += "lastModelHtml = lastModelHtml.replace( initModelHtml, \"\" );\r\n";
            js += "ss.setItem( 'lastModelHtml', lastModelHtml );\r\n";
            js += "ss.setItem( 'lastModelTextBoxValue', lastModelTextBoxValue );\r\n";
            js += "}\r\n";
            js += "});\r\n";

            js += "$(\"#" + btnCopyPackage.ClientID + "\").on( 'click', function(){\r\n";
            js += "var ss = sessionStorage;\r\n";
            js += "if( ss ){\r\n";
            js += "var initModelHtml = ss.getItem( 'initModelHtml' );\r\n";
            js += "var lastModelHtml = $(\".modelArea\").html();\r\n";
            js += "var lastModelTextBoxValue = $(\"#txtModels\").val();\r\n";
            js += "lastModelHtml = lastModelHtml.replace( initModelHtml, \"\" );\r\n";
            js += "ss.setItem( 'lastModelHtml', lastModelHtml );\r\n";
            js += "ss.setItem( 'lastModelTextBoxValue', lastModelTextBoxValue );\r\n";
            js += "}\r\n";
            js += "});\r\n";

            js += "}\r\n";
            js += "addKeyPressEventByJavaScript();\r\n";
            js += @"</script>";

            return js;
        }

        private bool IsModelExist(String[] models)
        {
            foreach (var model in models)
            {
                var pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(model);
                if (pcfg == null)
                {
                    return false;
                }
            }
            return true;
        }

        public class dyn
        {

            public string str1 { get; set; }
            public string str2 { get; set; }
            public string str3 { get; set; }
            public int id { get; set; }
            public Guid? gid1 { get; set; }
            public Guid? gid2 { get; set; }
            public DateTime? DateTime1 { get; set; }
            public string str4 { get; set; }
            public bool Selected { get; set; }

        }

        protected void btnCopyPackage_OnClick(object sender, EventArgs e)
        {
            CopyPackage();

        }

        private void GetFormValue(ref string[] models)
        {
            // Model
            if (Request.Form.Count == 0 || Request.Form.HasKeys() == false)
                return;

            String txtModels = Request.Form.GetValues("txtModels").FirstOrDefault();
            if (String.IsNullOrEmpty(txtModels) == false)
            {
                // something inputted
                models = Request.Form.GetValues("txtModels");
            }

            if (Request.Form.AllKeys.Contains("models[]"))
            {
                models = models.Concat(Request.Form.GetValues("models[]")).ToArray();
            }
        }

        private string ReproductionDynamicTextBox()
        {
            string js;
            js = "<script type=\"text/javascript\" >\r\n";
            js += "function initEntryForm() {\r\n";
            js += "var ss = sessionStorage;\r\n";
            js += "if (ss) {\r\n";
            js += "      var initModelHtml = $(\".modelArea\").html();\r\n";
            js += "      ss.setItem('initModelHtml', initModelHtml);\r\n";
            js += "      var lastModelHtml = ss.getItem('lastModelHtml');\r\n";
            js += "      var lastModelTextBoxValue = ss.getItem('lastModelTextBoxValue');\r\n";
            js += "      if (lastModelHtml) {\r\n";
            js += "            $(\".modelArea\").append(lastModelHtml);\r\n";
            js += "      }\r\n";
            js += "      if (lastModelTextBoxValue) {\r\n";
            js += "                $(\"#txtModels\").val(lastModelTextBoxValue);\r\n";
            js += "      }\r\n";
            js += "      ss.removeItem('lastModelHtml');\r\n";
            js += "      ss.removeItem('lastModelTextBoxValue');\r\n";
            js += "}\r\n";
            js += "}\r\n";
            js += "initEntryForm();\r\n";
            js += "</script>\r\n";
            return js;
        }

    }
}