﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PackageFileDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.PackageFileDetailCtrl" %>
<%@ Register Src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" TagName="ResourceLocalizationItemCtrl"
    TagPrefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,pnlContainer.Text %>">
   <asp:PlaceHolder runat="server">
      <script type="text/javascript">
        var cmbAddNewLang = '<%= cmbAddNewLang.ClientID%>';
    </script>
    <script type="text/javascript" language="javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/support_and_packages.js"> 
    </script>
      </asp:PlaceHolder>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalFilePath" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalFilePath.Text %>"></asp:Localize></span>
        <asp:Literal runat="server" ID="ltrFilePath" Text="" ></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalmodified" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalmodified.Text %>"></asp:Localize></span>
        <asp:Literal runat="server" ID="ltrModUtc" Text="" ></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalNewIcon" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalNewIcon.Text %>"></asp:Localize></span>
        <asp:CheckBox runat="server" ID="chkNewicon" onclick = "MutExChkList(this);"/>
    </div>
     <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalUpdateIcon" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalUpdateIcon.Text %>"></asp:Localize></span>
        <asp:CheckBox runat="server" ID="chkUpdateIcon" onclick = "MutExChkList(this);"/>
        
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalUpdateDate" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalUpdateDate.Text %>"></asp:Localize></span>
        <dx:ASPxDateEdit runat="server" ID="dateEdit"></dx:ASPxDateEdit>
    </div>
      <div class="settingrow">
        <span class="msg">
            <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label></span>
    </div>
    <div class="settingrow text-right">
        <asp:Button ID="btnSave" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,bttSaveNotification.Text%>"
            OnClick="bttSaveNotification_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerFileDescriptionText" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,pnlContainerFileDescriptionText.Text %>" >
 
    <div class="settingrow">
        <p class="bold"><asp:Localize ID="lcalFileTextDesc" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalNotificationTextDesc.Text%>"></asp:Localize></p></div>
    <div class="settingrow">
        <uc1:ResourceLocalizationItemCtrl ID="reslcNotificatonText" DefaultResourceValue=" " runat="server" />
    </div>
    <div class="settingrow">
        <p class="bold"><asp:Localize ID="lcalFileTransLangDesc" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,lcalFileTransLangDesc.Text%>"></asp:Localize></p>
    </div>
     <div class="settingrow group input-select">
            <asp:DropDownList ID="cmbAddNewLang" runat="server" DataTextField="DisplayText" DataValueField="Locale" Visible="false">
            </asp:DropDownList>
        </div>
    <div class="margin-top-15">
             <asp:Button ID="bttAddNewLang" runat="server" Text="<%$Resources:ADM_STCTRL_NotificationAdmin_DetailCtrl,bttAddNewLang.Text%>"  
                CausesValidation="false" OnClick="bttAddNewLang_Click" Visible="false" />
             </div>
</anrui:GlobalWebBoxedPanel>



