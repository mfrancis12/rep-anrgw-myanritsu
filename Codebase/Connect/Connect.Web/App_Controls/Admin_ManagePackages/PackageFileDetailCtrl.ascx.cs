﻿/*
 * Author:        kishore kumar M
 * Created Date:  17/10/2014
 * Modified Date: 
 * Modified By:   kishore kumar M
 * Purpose:       Interface to manage File attributes in JP package Section
 */

using System;
using System.Globalization;
using System.Linq;
using System.Web;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Notification;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Data;
using Anritsu.Connect.Web.App_Lib;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Office.Utils;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class PackageFileDetailCtrl : System.Web.UI.UserControl
    {
        #region Properties/Fields

        public int FileId
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.FileId], 0);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_PackageFile_DetailCtrl"; }
        }

        private String GetCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        private String EffectedArea
        {
            get { return "Export Notice"; }
        }
        #endregion


        /// <summary>
        /// loads Notification Information and Display area list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            reslcNotificatonText.ClassKey = "PackageFileDescription_" + FileId.ToString(CultureInfo.InvariantCulture).ToUpperInvariant();
            reslcNotificatonText.ResKey = "PackageFileDescription";
            if (!IsPostBack)
            {
               LoadFileDetails();
            }

        }

     
        private void LoadFileDetails()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["fid"]))
            {
                var fid = Request.QueryString["fid"];
                var pf = (Package_Files)BLL_PackageFile.GetPackageFileById(Convert.ToInt32(fid));
                if (pf != null)
                {
                    ltrFilePath.Text = pf.FilePath;
                    ltrModUtc.Text = pf.FileLastModifiedOnUTC.ToShortDateString();
                    chkNewicon.Checked = pf.ShowNewIcon.HasValue && pf.ShowNewIcon.Value;
                    chkUpdateIcon.Checked = pf.ShowUpdateIcon.HasValue && pf.ShowUpdateIcon.Value;
                    dateEdit.Text = pf.ModifiedOnUTC.HasValue ? pf.ModifiedOnUTC.Value.ToShortDateString() : "";
                }
            }
        }

        /// <summary>
        /// Bind Add New Language Section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(reslcNotificatonText.ClassKey) && (!String.IsNullOrWhiteSpace(reslcNotificatonText.ResKey)))
                InitAddNewLanguageSection();
        }

        #region Postback Events

        /// <summary>
        /// Save (update/add) Notification 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttSaveNotification_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;


            var pkgFle = new Package_Files();

            pkgFle.ModifiedOnUTC = (dateEdit.Text.Length > 0)
                ? Convert.ToDateTime(dateEdit.Text)
                : pkgFle.ModifiedOnUTC;
            pkgFle.ShowUpdateIcon = chkUpdateIcon.Checked ? true : false;
            pkgFle.ShowNewIcon = chkNewicon.Checked ? true : false;
            pkgFle.FileID = Convert.ToInt32(Request.QueryString["fid"]);


            int update = (int)BLL_PackageFile.UpdatePackageFileById(pkgFle);
            if (update > 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text =GetStaticResource("lblMsgFileSaved");
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text =GetStaticResource("lblMsgFileNotSaved");
            }

        }




        /// <summary>
        /// Can add new language
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttAddNewLang_Click(object sender, EventArgs e)
        {
            try
            {
                String newlocale = cmbAddNewLang.SelectedValue;
                Int32 contentID = Lib.Content.Res_ContentStoreBLL.Insert(reslcNotificatonText.ClassKey, reslcNotificatonText.ResKey, newlocale, "-", false);
                if (contentID > 0)
                {
                    WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&returnurl={3}", KeyDef.UrlList.SiteAdminPages.AdminContentDetails, KeyDef.QSKeys.AdminContent_ContentID, contentID, HttpUtility.UrlEncode(Request.RawUrl)));
                }
            }
            catch (SqlException ex)
            {

            }
        }
        #endregion

        #region Private methods


        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }


        /// <summary>
        /// Bind Add New Language Section
        /// </summary>
        private void InitAddNewLanguageSection()
        {

            //Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(reslcNotificatonText.ClassKey, reslcNotificatonText.ResKey);

            DataTable tbSupportedLocales = Lib.Content.Res_ContentStoreBLL.ContentSupportedLocale_SelectAll();

            var filtered = (from l in tbSupportedLocales.AsEnumerable()
                            join n in tb.AsEnumerable() on l["Locale"].ToString() equals n["Locale"].ToString()
                         into f
                            where f.Count() == 0
                            select l);
            DataTable filteredNewLocales = null;
            if (filtered != null && filtered.Count() > 0)
            {
                cmbAddNewLang.Visible = bttAddNewLang.Visible = true;
                filteredNewLocales = filtered.CopyToDataTable();
                DataView dv = filteredNewLocales.DefaultView;
                dv.Sort = "DisplayText ASC";
                cmbAddNewLang.DataSource = dv;
                cmbAddNewLang.DataBind();
            }
        }

        #endregion
    }
}