﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Package_ListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ManagePackages.Package_ListCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow text-center">
        <asp:HyperLink ID="hlAddNewForm" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,hlAddNewPackage.Text %>"
            NavigateUrl="~/SiteAdmin/PackageAdmin/PackageDetails.aspx" SkinID="blueit-left"></asp:HyperLink>
        <asp:Button ID="btnUpdateAllPackages" runat="server" Text="Update All Packages"
            OnClick="btnUpdateAllPackages_Click" /> 
		 <span class="settinglabel  error-msg my-anritsu-package-inavalid-path-error" style="width: 100% !important; margin-top: 20px;">
            <asp:Localize ID="lblInvalidPkgPathErr" runat="server" Visible="false"></asp:Localize>
         </span>
    </div>
    <div class="settingrow word-break-all">
        <dx:ASPxGridView ID="gvPackages" ClientInstanceName="gvPackages" runat="server" Theme="AnritsuDevXTheme" OnBeforePerformDataSelect="gvPackages_OnBeforePerformDataSelect" OnHtmlDataCellPrepared="gvPackages_HtmlDataCellPrepared" OnDetailRowExpandedChanged="gvPackages_DetailRowExpandedChanged"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="PackageID" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataTextColumn VisibleIndex="1" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackageName.Text %>"  FieldName="PackageName" Width="350">
                    <DataItemTemplate>
                        <a id="lnkPackageName" target="_parent" href="/siteadmin/packageadmin/packagedetails?pid=<%# Eval("PackageID")%>"><%# Eval("PackageName")%></a>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="2" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,lcalPackagePath.Text %>" Width="400"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="SystemName" VisibleIndex="2" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_DetailCtrl,SystemName %>"  Width="200"></dx:GridViewDataColumn>

                <dx:GridViewDataColumn FieldName="PackageIntact" VisibleIndex="3" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageIntact %>"  Width="50">
                    <DataItemTemplate>
                        <%#Eval("PackageIntact") %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="4" Caption="<%$ Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalUserEmail.Text %>" Settings-AllowHeaderFilter="False" >
                    <Settings AllowAutoFilter="False" />
                     <DataItemTemplate>
                       <a href="/siteadmin/packageadmin/SendEmail?page=TAU&pid=<%# Eval("PackageID")%>&pname=<%# Eval("PackageName").ToString().Replace("&","!")%>">
                           <img src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/img/mail.png"  />
                       </a>
                   </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="gvPackageFiles" runat="server" Theme="AnritsuDevXTheme" OnPageIndexChanged="gvPackageFiles_OnPageIndexChanged">
                        <Columns>
                            <dx:GridViewDataColumn FieldName="SNo" VisibleIndex="0" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,SerialNumber %>"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Key" VisibleIndex="1" Caption="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalFilePath.Text %>"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Size" VisibleIndex="2" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,ByteSize %>"></dx:GridViewDataColumn>
                            <dx:GridViewDataDateColumn FieldName="LastModified" VisibleIndex="3" Caption="<%$ Resources:ADM_STCTRL_PackageFilDetailAdmin_ListCtrl,lcalmodified.Text %>" >
                                <PropertiesDateEdit DisplayFormatString="G">
                                </PropertiesDateEdit>
                            </dx:GridViewDataDateColumn>

                        </Columns>
                    </dx:ASPxGridView>
                    <br />
                    <asp:Button ID="btnUpdate" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttUpdate.Text %>" runat="server" Visible='<%# string.Equals(Eval("PackageIntact").ToString(),"false",StringComparison.InvariantCultureIgnoreCase) %>' OnClick="btnUpdate_Click" CommandArgument='<%# Eval("PackageID") %>' />
                </DetailRow>
            </Templates>
            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>
        <%--<asp:SqlDataSource ID="sdsPackagesResults" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
            SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Package_SelectAll]"></asp:SqlDataSource>--%>
    </div>


    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_PackageAdmin_ListCtrl" />
</anrui:GlobalWebBoxedPanel>
