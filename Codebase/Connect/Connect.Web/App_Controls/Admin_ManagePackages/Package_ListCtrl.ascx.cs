﻿
/*
 * Author:        kishore kumar M
 * Created Date:  
 * Modified Date: 04/04/2014
 * Modified By:   kishore kumar M
 * Purpose:       Interface to display list of packages and option to send an email to associated users
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon.S3.Model;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.ActivityLog;
using System.Globalization;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public partial class Package_ListCtrl : Awss3SettingsCtrlBase
    {
        private string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_PackageAdmin_ListCtr"; }
        }

        private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //get the Packages, Files and S3Objects
            //DataSet dsPackageAndFiles = Lib.Package.PackageBLL.SelectPackagesWithFiles();
            //if (!dsPackageAndFiles.Tables[0].IsNullOrEmpty() && !dsPackageAndFiles.Tables[1].IsNullOrEmpty())
            //{
            //    List<S3Object> s3objects = PackageBLL.GetPackaeFileList(TauS3Settings.BasePath.Replace("/", ""), TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, null);
            //    gvPackages.DataSource = GetUpdatedPackages(dsPackageAndFiles.Tables[0], dsPackageAndFiles.Tables[1], s3objects);
                gvPackages.DataBind();
            //}
        }
        private List<Package> GetUpdatedPackages(DataTable tblPackage, DataTable tblPackageFiles, List<S3Object> s3Objects)
        {
            List<Package> lstPackages = new List<Package>();
            if (tblPackage.IsNullOrEmpty() || tblPackageFiles.IsNullOrEmpty() || s3Objects == null || s3Objects.Count == 0) return null;
            DataRow[] drPackages = ApplyManagePackageRoles(tblPackage);
            if (drPackages == null || drPackages.Count() == 0) return null;
            var tobeUpdatedPackages = new List<Package>();
            foreach (DataRow drPackage in drPackages)
            {
                //get the list of Package Files from the tblPackageFiles
                Int64 packID = ConvertUtility.ConvertToInt64(drPackage["PackageID"], 0);
                String packPath = Convert.ToString(drPackage["PackagePath"]);
                if (packID == 0) break;
                DataRow[] drPackageFiles = tblPackageFiles.Select("PackageID=" + packID);
                //get the list of Package Files from s3Objects
                IEnumerable<S3Object> lstPackageFiles = from file in s3Objects
                                                        where (file.Key.StartsWith(packPath) && (file.Size > 0) && file.Key.Replace(packPath, "").Split('/').Length == 2 && file.Key.Replace(packPath, "").Split('/')[0] == string.Empty)
                                                        select file;

                //if (!lstPackageFiles.Any())
                //{
                //    lstPackages.Add(InitPackage(drPackage, drPackageFiles, true));
                //    tobeUpdatedPackages.Add(new Package() {PackageID = packID, PackagePath = packPath});
                //}
                //else
                //{
                    //Construct the Package Objects
                    var fileIntact = CheckFileIntact(drPackageFiles, lstPackageFiles);
                    lstPackages.Add(InitPackage(drPackage, drPackageFiles, fileIntact));
                    if (fileIntact)
                        tobeUpdatedPackages.Add(new Package() { PackageID = packID, PackagePath = Convert.ToString(drPackage["PackagePath"]) });
                //}
            }
            //copy tobeUpdatedPackages to the session
            if (tobeUpdatedPackages.Count > 0)
                Session[KeyDef.SSKeys.ToBeUpdatedMBMCPackages] = tobeUpdatedPackages;
            return lstPackages;
        }

        private DataRow[] ApplyManagePackageRoles(DataTable tblPackage)
        {
            //if the user is in "adm-mngfile-engg-uk-tau" then they can only select Engg Packages
            //if (Context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) return tblPackage.Select();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) return tblPackage.Select();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU))
            {
                return tblPackage.Select("SystemName='" + Package.TauPackageTypes.EnginerringPackage + "'");
            }
            return null;
        }
        protected void gvPackages_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            //get the Packages, Files and S3Objects
            DataSet dsPackageAndFiles = Lib.Package.PackageBLL.SelectPackagesWithFiles();
            if (!dsPackageAndFiles.Tables[0].IsNullOrEmpty() && !dsPackageAndFiles.Tables[1].IsNullOrEmpty())
            {
                List<S3Object> s3objects;
                try
                {
                    s3objects = PackageBLL.GetPackaeFileList(TauS3Settings.BasePath.Replace("/", ""), TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, null);
                }
                catch (ArgumentException ex)
                {
                    if (ex.HResult == int.Parse(ConfigUtility.AppSettingGetValue("Connect.Web.IllegalCharactersExceptionHResult")))
                    {
                        lblInvalidPkgPathErr.Visible = true;
                        lblInvalidPkgPathErr.Text = GetStaticResource("lclErrorPackageInvalidChars")
                            .Replace("[[PackagePath]]", ex.Message)
                            .Replace("[[InvalidCharacters]]", ConfigUtility.AppSettingGetValue("Connect.Web.PackageInvalidChars"));
                        return;
                    }
                    else
                        throw;
                }
                gvPackages.DataSource = GetUpdatedPackages(dsPackageAndFiles.Tables[0], dsPackageAndFiles.Tables[1], s3objects);
                gvPackages.DetailRows.CollapseAllRows();
                //gvPackages.DataBind();
            }
        }
        private Package InitPackage(DataRow drPackage, DataRow[] packageFiles, bool isPackageModified)
        {
            Package objPack = new Package()
            {
                PackageID = Convert.ToInt64(drPackage["PackageID"]),
                PackageName = Convert.ToString(drPackage["PackageName"]),
                PackagePath = Convert.ToString(drPackage["PackagePath"]),
                SystemID = Convert.ToInt32(drPackage["SystemID"]),
                //ModelNumber = Convert.ToString(drPackage["PackagePath"]),
                Enabled = Convert.ToBoolean(drPackage["IsEnabled"]),
                FileList = null,//GenerateFileList(packageFiles)
                PackageIntact = isPackageModified ? "False" : "True",
                SystemName = Convert.ToString(drPackage["SystemName"])
            };
            return objPack;
        }

        private List<PackageFile> GenerateFileList(DataRow[] packageFiles)
        {
            if (packageFiles == null || packageFiles.Length == 0) return null;
            List<PackageFile> lstPackageFiles = new List<PackageFile>();
            foreach (DataRow drpackageFile in packageFiles)
            {
                lstPackageFiles.Add(new PackageFile()
                {
                    FileID = Convert.ToInt64(drpackageFile["FileID"]),
                    FilePath = Convert.ToString(drpackageFile["FilePath"]),
                    FileSize = Convert.ToInt64(drpackageFile["Size"]),
                    FileLastModified = Convert.ToDateTime(drpackageFile["LastModified"])
                });
            }
            return lstPackageFiles;
        }
        private Boolean CheckFileIntact(DataRow[] drPackageFiles, IEnumerable<S3Object> lstPackageFiles)
        {
            if (drPackageFiles == null || drPackageFiles.Length == 0 || lstPackageFiles == null || lstPackageFiles.Count() == 0) return false;
            if (lstPackageFiles.Count() != drPackageFiles.Length) return true;
            string[] formats = { "ddd, dd MMM yyyy HH:mm:ss GMT", "ddd, dd M yyyy HH:mm:ss GMT" };
            foreach (S3Object s3obj in lstPackageFiles)
            {
                //var sd=from s in lstPackageFiles.s
                DataRow drPackage = drPackageFiles.Where(dr => Convert.ToString(dr["FilePath"]) == s3obj.Key).FirstOrDefault();
                if (drPackage == null || DateTime.Compare(Convert.ToDateTime(drPackage["FileLastModifiedOnUTC"]), s3obj.LastModified.ToUniversalTime()) != 0)
                //!String.Equals(Convert.ToDateTime(drPackage["FileLastModifiedOnUTC"]).ToString("G", CultureInfo.InvariantCulture), 
                //DateTime.ParseExact(Convert.ToString(s3obj.LastModified), formats, 
                //CultureInfo.CurrentCulture, DateTimeStyles.None).ToUniversalTime().ToString("G", CultureInfo.InvariantCulture), StringComparison.InvariantCulture))
                {
                    return true;
                }
            }
            return false;
        }

        protected void gvPackages_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName != "PackageIntact") return;
            if (String.Equals(e.CellValue.ToString(), "False", StringComparison.InvariantCultureIgnoreCase))
                e.Cell.BackColor = System.Drawing.Color.Red;
        }

        protected void gvPackages_DetailRowExpandedChanged(object sender, DevExpress.Web.ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                ASPxGridView detailGrid = gvPackages.FindDetailRowTemplateControl(e.VisibleIndex, "gvPackageFiles") as ASPxGridView;
                if (detailGrid != null)
                {
                    Package objPackage = (Package)gvPackages.GetRow(e.VisibleIndex);
                    List<S3Object> lstPackageFiles = Lib.Package.PackageBLL.GetPackaeFileList(objPackage.PackagePath, TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, "/");
                    if (objPackage != null)
                    {
                        var filteredFiles = lstPackageFiles.AsEnumerable()
                        .OrderBy(x => x.Key)
                        .Select((x, iterator) => new { x.Key, SNo = iterator + 1, x.Size, LastModified = Convert.ToDateTime(x.LastModified).ToUniversalTime() });
                        detailGrid.DataSource = filteredFiles;
                        detailGrid.DataBind();
                        Button btnUpdate = gvPackages.FindDetailRowTemplateControl(e.VisibleIndex, "btnUpdate") as Button;
                        btnUpdate.Visible = String.Equals(objPackage.PackageIntact, "False", StringComparison.InvariantCultureIgnoreCase) ? true : false;
                    }
                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var btnthis = (Button)sender;
            var row = (GridViewDetailRowTemplateContainer)btnthis.NamingContainer;
            var objPackage = (Package)gvPackages.GetRow(row.VisibleIndex);
            if (objPackage != null)
            {
                UpdateS3Package(objPackage);
                WebUtility.HttpRedirect(null, Request.RawUrl);
            }
        }
        protected void btnUpdateAllPackages_Click(object sender, EventArgs e)
        {
            //take the tobeUpdatedPackages packages from the sessiom
            if (Session[KeyDef.SSKeys.ToBeUpdatedMBMCPackages] == null) return;
            var tobeUpdatedPackages = ((List<Package>)Session[KeyDef.SSKeys.ToBeUpdatedMBMCPackages]);
            if (!tobeUpdatedPackages.Any()) return;
            foreach (var pckg in tobeUpdatedPackages)
            {
                //call update method for each package
                UpdateS3Package(pckg);
            }
            //remove from the seesion
            Session.Remove(KeyDef.SSKeys.ToBeUpdatedMBMCPackages);
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }
        private void UpdateS3Package(Package objPackage)
        {
            if (objPackage == null) return;
            try
            {
                //get the updated Files
                List<S3Object> lstPackageFiles = Lib.Package.PackageBLL.GetPackaeFileList(objPackage.PackagePath, TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, "/");
                var filtered = from item in lstPackageFiles
                               select new { Key = item.Key, Size = item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                DataTable dtPackageFiles = ToFileListDataTable(filtered);
                //update Package information (Package Files Table)
                Lib.Package.PackageBLL.UpdatePackageFiles(objPackage.PackageID, LoginUtility.GetCurrentUser(true).Email, dtPackageFiles);
                //update the admin activity log
                LogAdminActivity("Package", TasksPerformed.Sync, ActivityLogNotes.PACKAGE_SYNC, getCurrentUserEmail, objPackage.PackageName);
            }
            catch (Exception ex)
            {
                //log the error details silently
                AppLog.LogError(ex, HttpContext.Current);
            }
        }

        protected void gvPackageFiles_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            long num = (long)detailGrid.GetMasterRowKeyValue();
            Package objPackage = PackageBLL.SelectByPackageID(num);
            List<S3Object> lstPackageFiles = Lib.Package.PackageBLL.GetPackaeFileList(objPackage.PackagePath, TauS3Settings.ActiveRegion, TauS3Settings.AwsConfigType, "/");
            if (objPackage != null)
            {
                var filteredFiles = lstPackageFiles.AsEnumerable()
                .OrderBy(x => x.Key)
                .Select((x, iterator) => new { x.Key, SNo = iterator + 1, x.Size, LastModified = Convert.ToDateTime(x.LastModified).ToUniversalTime() });
                detailGrid.DataSource = filteredFiles;
                detailGrid.DataBind();
            }
        }
        private DataTable ToFileListDataTable(dynamic s3Objects)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Key");
            dataTable.Columns.Add("LastModified");
            dataTable.Columns.Add("Size");
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            foreach (var n in s3Objects)
            {
                DataRow dr = dataTable.NewRow();
                dr["Key"] = n.Key;
                dr["LastModified"] = n.LastModified.ToUniversalTime();
                dr["Size"] = n.Size;
                dataTable.Rows.Add(dr);
            }
            Thread.CurrentThread.CurrentCulture = currentCulture;
            return dataTable;
        }
        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(getCurrentUserEmail, "Package", taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
        }

        private string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }
    }
}