﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Package_ListCtrl_JP.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ManagePackages.PackageListCtrljp" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,pnlContainer.Text %>">
    <asp:Literal runat="server" ID="litRichToTextArea" />
    <div class="settingrow text-center">
        <asp:HyperLink ID="hlAddNewForm" runat="server" Text="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,hlAddNewPackage.Text %>"
            NavigateUrl="~/SiteAdmin/PackageAdmin/PackageDetails_jp" SkinID="blueit-left"></asp:HyperLink>

        <asp:Button ID="btnRemovePackageCache" runat="server" Text="Clear Package Cache"
            OnClick="btnRemovePackageCache_Click" UseSubmitBehavior="false"/>
        <asp:Label ID="lblCacheClear" runat="server" EnableViewState="false"></asp:Label>

        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/prodregadmin/productsupport/search_jpsupporttypeentry" Text='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,pnlContainer.HeaderText %>' SkinID="blueit-right" runat="server"></asp:HyperLink>
    </div>


    <div class="settingrow">
        <div class="width-60">
            <div class="settingrow group">
                <label style="line-height: 23px; margin-bottom: 5px">
                    <asp:CheckBox ID="cbxSearchByPName" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageName %>" TextAlign="Left" /></label>
            </div>
            <div class="input-text">
                <asp:TextBox ID="txtSearchByPName" runat="server"></asp:TextBox>
            </div>
            <div class="settingrow group">
                <label style="line-height: 23px; margin-bottom: 5px">
                    <asp:CheckBox ID="cbxSearchByPGroup" runat="server" Text="<%$Resources:ADM_STCTRL_JPPackageAdmin_DetailCtrl,lcalPkgGroupName.Text %>" TextAlign="Left" /></label>
            </div>
            <div class="input-text">
                <asp:TextBox ID="txtSearchByPGroup" runat="server"></asp:TextBox>
            </div>

            <div class="settingrow group">
                <label style="line-height: 23px; margin-bottom: 5px">
                    <asp:CheckBox ID="cbxSearchByPPath" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl, PackagePath %>" TextAlign="Left" /></label>
            </div>
            <div class="input-text">
                <asp:TextBox ID="txtSearchByPPath" runat="server"></asp:TextBox>
            </div>

            <div class="settingrow group">
                <label style="line-height: 23px; margin-bottom: 5px">
                    <asp:CheckBox ID="cbxSearchByPType" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl, PackageType %>" TextAlign="Left" /></label>
            </div>
            <div class="input-text">
                <asp:TextBox ID="txtSearchByPType" runat="server"></asp:TextBox>
            </div>

             <div class="margin-top-15">
				<span class="my-anritsu-package-btn-search">
					<asp:Button ID="bttPackageSearch" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP, bttSearch %>" CausesValidation="false" OnClick="bttPackageSearch_Click" UseSubmitBehavior="true" />
				</span>
				<span class="settinglabel  error-msg  my-anritsu-package-inavalid-path-error" style="width: 75% !important;">
                    <asp:Localize ID="lblErrorMsg" runat="server" Visible="false" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP, lclErrorSearchFormIsEmpty %>"></asp:Localize>
                </span>
			</div>
        </div>





        <div class="settingrow word-break-all">
            <dx:ASPxGridView ID="gvPackages" ClientInstanceName="gvPackages" runat="server" Theme="AnritsuDevXTheme" OnHtmlDataCellPrepared="gvPackages_HtmlDataCellPrepared"
                OnDetailRowExpandedChanged="gvPackages_DetailRowExpandedChanged" OnBeforePerformDataSelect="gvPackages_OnBeforePerformDataSelect"
                Width="100%" AutoGenerateColumns="False" KeyFieldName="PackageID" SettingsBehavior-AutoExpandAllGroups="false"
                >
                <Columns>
                    <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="PackageGroupName" VisibleIndex="2" Caption="<%$ Resources:ADM_STCTRL_JPPackageAdmin_DetailCtrl,lcalPkgGroupName.Text%>" Width="200"></dx:GridViewDataColumn>

                    <dx:GridViewDataTextColumn VisibleIndex="1" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageName%>" FieldName="PackageName" Width="200">
                        <DataItemTemplate>
                            <a id="lnkPackageName" target="_parent" href="/siteadmin/packageadmin/packagedetails_jp?pid=<%# Eval("PackageID")%>"><%# Eval("PackageName")%></a>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="3" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackagePath%>" Width="250"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="SystemName" VisibleIndex="3" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageType%>" Width="300"></dx:GridViewDataColumn>

                    <dx:GridViewDataColumn FieldName="PackageIntact" VisibleIndex="4" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,PackageIntact%>" Width="50">
                        <DataItemTemplate>
                            <%#Eval("PackageIntact") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="5" Caption="<%$ Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalUserEmail.Text %>" Settings-AllowHeaderFilter="False">
                        <Settings AllowAutoFilter="False" />
                        <DataItemTemplate>
                            <a href="/siteadmin/packageadmin/SendEmailJP?pid=<%# Eval("PackageID")%>&pname=<%# Eval("PackageName").ToString().Replace("&","!")%>">
                                <img src="//images.cdn-anritsu.com/apps/connect/img/mail.png" />
                            </a>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>

                <SettingsDetail ShowDetailRow="True" />
                <Templates>
                    <DetailRow>
                        <dx:ASPxGridView ID="gvFiles" runat="server" Theme="AnritsuDevXTheme" KeyFieldName="FileID" OnPageIndexChanged="gvFiles_OnPageIndexChanged"
                            OnDetailRowExpandedChanged="gvFiles_DetailRowExpandedChanged"
                            OnBeforePerformDataSelect="gvFiles_OnBeforePerformDataSelect" CssClass="full-width"
                            EnableRowsCache="true"
                            OnBatchUpdate="gvFiles_BatchUpdate" OnRowUpdating="gvFiles_RowUpdating"
                            OnRowUpdated="gvFiles_RowUpdated"
                            >
                            
                            <Columns>

                                <dx:GridViewDataColumn FieldName="SNo" VisibleIndex="0" Width="20" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,SerialNumber%>"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn VisibleIndex="1" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,FilePath%>" Width="350">
                                    <DataItemTemplate>
                                        <dx:ASPxLabel runat="server" ID="lblKey" Text='<%# Eval("Key")%>' />
                                        <br />
                                        <b>Size in (bytes):</b>
                                        <dx:ASPxLabel runat="server" ID="lblSize" Text='<%# Eval("FileSize")%>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataDateColumn FieldName="LastModified" VisibleIndex="3" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,ModUTC%>">
                                    <PropertiesDateEdit DisplayFormatString="yyyy/MM/d">
                                    </PropertiesDateEdit>
                                </dx:GridViewDataDateColumn>

                            <dx:GridViewDataColumn VisibleIndex="4" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,NewIcon%>" FieldName="ShowNewIcon"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn VisibleIndex="5" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,UpdateIcon%>" FieldName="ShowUpdateIcon"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn VisibleIndex="6" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl,UpdateDate%>" FieldName="UpdateDate" />
                                <dx:GridViewDataHyperLinkColumn FieldName="FileID" VisibleIndex="7" Caption=" " PropertiesHyperLinkEdit-NavigateUrlFormatString="~/siteadmin/packageadmin/filedetails?fid={0}" PropertiesHyperLinkEdit-Text="details">
                                    <Settings AllowAutoFilter="False" />
                                </dx:GridViewDataHyperLinkColumn>

                                <%--<dx:GridViewDataColumn FieldName="ParentPackageId" VisibleIndex="8" Visible="false" Caption="" />--%>

                                <%--<dx:GridViewDataColumn FieldName="ModifiedFileData" VisibleIndex="9" Caption="" Visible="false">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox runat="server" ID="cbModifiedFileData" Text='<%# Eval("ModifiedFileData")%>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>


                                <dx:GridViewDataColumn FieldName="ModifiedMemoData" VisibleIndex= "10" Visible="false" Caption="">
                                    <DataItemTemplate>
                                        <dx:ASPxCheckBox runat="server" ID="cbModifiedMemoData" Text='<%# Eval("ModifiedMemoData")%>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>

                                <dx:GridViewDataColumn FieldName="JPMemo" VisibleIndex="11" Visible="false" Caption="" />
                                <dx:GridViewDataColumn FieldName="ENMemo" VisibleIndex="12" Visible="false" Caption="" />--%>
                            </Columns>

                              <%--<SettingsEditing Mode="Batch" BatchEditSettings-EditMode="Cell" BatchEditSettings-StartEditAction="Click" />--%>

                            <SettingsDetail ShowDetailRow="True" />
                            <Templates>
                                <DetailRow>
                                    <dx:ASPxGridView ID="gvMemos" runat="server" Theme="AnritsuDevXTheme" KeyFieldName="MemoLanguage"
                                        OnBeforePerformDataSelect="gvMemo_BeforePerformDataSelect" CssClass="full-width">
                                        <Columns>
                                            <dx:GridViewDataColumn FieldName="MemoLanguage" VisibleIndex="0" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP, MemoLanguage%> " Width="35">
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel runat="server" ID="lblLanguage" Text='<%# Eval("MemoLanguage")%>' />
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>

                                            <dx:GridViewDataColumn FieldName="MemoText" VisibleIndex="1" Caption="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP, MemoHeader%>" Width="350">
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel  runat="server" ID="lblMemoText" Text='<%# Eval("MemoText")%>' EncodeHtml="false" />
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>

                                        </Columns>
                                    </dx:ASPxGridView>
                                </DetailRow>
                            </Templates>




                        </dx:ASPxGridView>
                        </br>
                        <asp:Button ID="btnUpdate" Text="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP,StatusUpdate.Text%>" runat="server" Visible='<%# string.Equals(Eval("PackageIntact").ToString(),"false",StringComparison.InvariantCultureIgnoreCase) %>' OnClick="btnUpdate_Click" CommandArgument='<%# Eval("PackageID") %>' />
                        <%--<asp:Button ID="bttSaveGridData" Text="<%$ Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP,bttSaveGridData.Text%>" runat="server" OnClick="bttSaveGridData_Click" />--%>

                    </DetailRow>
                </Templates>
                <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" />
                <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
                <SettingsPager PageSize="30"></SettingsPager>
            </dx:ASPxGridView>
        </div>
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_PackageAdmin_ListCtrl" />
</anrui:GlobalWebBoxedPanel>
