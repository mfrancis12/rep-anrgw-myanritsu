﻿
/*
 * Author:        kishore kumar M
 * Created Date:  21/10/2014
 * Modified Date: 
 * Modified By:   kishore kumar M
 * Purpose:       Interface to display list of packages and files
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using Amazon.S3.Model;
using Anritsu.Connect.EntityFramework.ContextHelper;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.ActivityLog;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib.AppCacheFactories;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.Content;
using System.Web;
namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public partial class PackageListCtrljp : Awss3SettingsCtrlBase
    {

        #region Inner Class
        /// <summary>
        /// data class for gvMemo grid.
        /// </summary>
        private class GridMemoField
        {
            /// <summary>
            /// Memo's Language.
            /// </summary>
            /// <example>
            ///     "JP" = Japanese
            ///     "EN" = English
            /// </example>
            public String MemoLanguage { get; set; }

            /// <summary>
            /// Memo Contents.
            /// </summary>
            public String MemoText { get; set; }
        }


        /// <summary>
        /// data class for gvFiles
        /// </summary>
        private class GridFileAndMemoField
        {
            public long ParentPackageId { get; set; }
            public long FileID { get; set; }
            public String Key { get; set; }
            public int SNo { get; set; }
            public long FileSize { get; set; }
            public DateTime LastModified { get; set; }
            public bool ShowNewIcon { get; set; }
            public bool ShowUpdateIcon { get; set; }
            public DateTime? UpdateDate { get; set; }

            // flag. if user modified file datas, set true
            public bool ModifiedFileData { get; set; }
            // flag. if user modified memo datas, set true
            public bool ModifiedMemoData { get; set; }

            public String JPMemo { get; set; }
            public String ENMemo { get; set; }

            public GridFileAndMemoField(long parentPackageId, long fileId, String key, int sno, long fileSize, DateTime lastModified, bool showNewIcon, bool showUpdateIcon, DateTime? updateDate, bool modifiedFileData, bool modifiedMemoData, String jpMemo, String enMemo)
            {
                this.ParentPackageId = parentPackageId;
                this.FileID = fileId;
                this.Key = key;
                this.SNo = sno;
                this.FileSize = fileSize;
                this.LastModified = lastModified;
                this.ShowNewIcon = showNewIcon;
                this.ShowUpdateIcon = showUpdateIcon;
                this.UpdateDate = updateDate;
                this.ModifiedFileData = modifiedFileData;
                this.ModifiedMemoData = modifiedMemoData;
                this.JPMemo = jpMemo;
                this.ENMemo = enMemo;
            }

            public GridFileAndMemoField(GridFileAndMemoField f)
            {
                this.ParentPackageId = f.ParentPackageId;
                this.FileID = f.FileID;
                this.Key = f.Key;
                this.SNo = f.SNo;
                this.FileSize = f.FileSize;
                this.LastModified = f.LastModified;
                this.ShowNewIcon = f.ShowNewIcon;
                this.ShowUpdateIcon = f.ShowUpdateIcon;
                this.UpdateDate = f.UpdateDate;
                this.ModifiedFileData = f.ModifiedFileData;
                this.ModifiedMemoData = f.ModifiedMemoData;
                this.JPMemo = f.JPMemo;
                this.ENMemo = f.ENMemo;
            }



            public GridFileAndMemoField() { }
        }
        #endregion

        #region Property
        private string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_PackageAdmin_ListCtrl_JP"; }
        }

        private static String LoggedInUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        private string ClassKeyTemplate
        {
            get { return "PackageFileDescription_"; }
        }

        private string ResourceKeyTemplate
        {
            get { return "PackageFileDescription"; }
        }
        #endregion

        #region Field
        private const String AreaEffected = "Package-JP";
        #endregion

        private string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isSearched = false;
            if ((cbxSearchByPName.Checked && !String.IsNullOrEmpty(txtSearchByPName.Text)) ||
                (cbxSearchByPGroup.Checked && !String.IsNullOrEmpty(txtSearchByPGroup.Text)) ||
                (cbxSearchByPPath.Checked && !String.IsNullOrEmpty(txtSearchByPPath.Text)) ||
                (cbxSearchByPType.Checked && !String.IsNullOrEmpty(txtSearchByPType.Text))
               )
            {
                isSearched = true;
            }

            if (IsPostBack || isSearched)
            {
                return;
            }

            //bind the Packages.
            gvPackages.DataBind();
        }

        private List<Package> GetUpdatedPackages(DataTable tblPackage, DataTable tblPackageFiles, List<S3Object> s3Objects)
        {
            var lstPackages = new List<Package>();
            if (tblPackage.IsNullOrEmpty() || tblPackageFiles.IsNullOrEmpty() || s3Objects == null || s3Objects.Count == 0) return null;
            var drPackages = tblPackage.Rows;
            if (drPackages == null || drPackages.Count == 0) return null;
            foreach (DataRow drPackage in drPackages)
            {
                //get the list of Package Files from the tblPackageFiles
                var packId = ConvertUtility.ConvertToInt64(drPackage["PackageID"], 0);
                String packPath = Convert.ToString(drPackage["PackagePath"]) + "/";
                if (packId == 0) break;
                DataRow[] drPackageFiles = tblPackageFiles.Select("PackageID=" + packId);
                //get the list of Package Files from s3Objects
                var lstPackageFiles = from file in s3Objects
                                      where (file.Key.StartsWith(packPath) && (file.Size > 0))
                                      select file;
                if (lstPackageFiles == null || lstPackageFiles.Count() == 0)
                    lstPackages.Add(InitPackage(drPackage, true));
                else
                {
                    //Construct the Package Objects
                    lstPackages.Add(InitPackage(drPackage, CheckFileIntact(drPackageFiles, lstPackageFiles)));
                }

            }
            return lstPackages;
        }

        /// <summary>
        /// Get Package List. Searching by PackageName,Group,Path,Type.
        /// </summary>
        /// <param name="tblPackage"></param>
        /// <param name="tblPackageFiles"></param>
        /// <param name="s3Objects"></param>
        /// <param name="pName"></param>
        /// <param name="pGroup"></param>
        /// <param name="pPath"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        private List<Package> GetUpdatedPackages(DataTable tblPackage, DataTable tblPackageFiles, List<S3Object> s3Objects,
                                                    String pName, String pGroup, String pPath, String pType)
        {
            var packType = PackageTypes.GetPackageTypes(ModelConfigTypeInfo.CONFIGTYPE_JP);
            var packTypeTable = new Dictionary<String, String>();
            packType.ToList().ForEach(f =>
            {
                packTypeTable.Add(f.SystemID.ToString(), f.SystemName);
            });

            var lstPackages = new List<Package>();
            if (tblPackage.IsNullOrEmpty() || tblPackageFiles.IsNullOrEmpty() || s3Objects == null || s3Objects.Count == 0) return null;
            var drPackages = tblPackage.Rows;
            if (drPackages == null || drPackages.Count == 0) return null;

            // Search Keywords
            String searchKeywordPackName = pName == String.Empty ? String.Empty : pName.ToUpper();
            String searchKeywordPackGroup = pGroup == String.Empty ? String.Empty : pGroup.ToUpper();
            String searchKeywordPackPath = pPath == String.Empty ? String.Empty : pPath.ToUpper();
            String searchKeywordPackType = pType == String.Empty ? String.Empty : pType.ToUpper();

            foreach (DataRow drPackage in drPackages)
            {
                //get the list of Package Files from the tblPackageFiles
                var packId = ConvertUtility.ConvertToInt64(drPackage["PackageID"], 0);
                String packName = Convert.ToString(drPackage["PackageName"]).ToUpper();
                String packGroup = Convert.ToString(drPackage["PackageGroupName"]).ToUpper();
                String packPath = Convert.ToString(drPackage["PackagePath"]).ToUpper() + "/";
                String nonUpperPackPath = Convert.ToString(drPackage["PackagePath"]) + "/";
                String tmpPackTypeId = Convert.ToString(drPackage["SystemId"]).ToUpper();
                String packTypeId = String.Empty;
                if (packTypeTable.ContainsKey(tmpPackTypeId))
                {
                    packTypeId = packTypeTable[tmpPackTypeId].ToUpper();
                }
                else
                {
                    // Error. SystemId is not exsits.(when pigs fly)
                    return null;
                }

                // Search by Keyword.
                if ((String.IsNullOrEmpty(pName) == false && !packName.Contains(searchKeywordPackName)))
                {
                    continue;
                }

                if ((String.IsNullOrEmpty(pGroup) == false && !packGroup.Contains(searchKeywordPackGroup)))
                {
                    continue;
                }

                if ((String.IsNullOrEmpty(pPath) == false && !packPath.Contains(searchKeywordPackPath)))
                {
                    continue;
                }

                if ((String.IsNullOrEmpty(pType) == false && !packTypeId.Contains(searchKeywordPackType)))
                {
                    continue;
                }


                if (packId == 0) break;
                DataRow[] drPackageFiles = tblPackageFiles.Select("PackageID=" + packId);
                //get the list of Package Files from s3Objects
                var lstPackageFiles = from file in s3Objects
                                      where (file.Key.StartsWith(nonUpperPackPath) && (file.Size > 0))
                                      select file;
                if (lstPackageFiles == null || lstPackageFiles.Count() == 0)
                    lstPackages.Add(InitPackage(drPackage, true));
                else
                {
                    //Construct the Package Objects
                    lstPackages.Add(InitPackage(drPackage, CheckFileIntact(drPackageFiles, lstPackageFiles)));
                }

            }
            return lstPackages;
        }

        private static Package InitPackage(DataRow drPackage, bool isPackageModified)
        {
            var objPack = new Package
            {
                PackageID = Convert.ToInt64(drPackage["PackageID"]),
                PackageName = Convert.ToString(drPackage["PackageName"]),
                PackagePath = Convert.ToString(drPackage["PackagePath"]),
                SystemID = Convert.ToInt32(drPackage["SystemID"]),
                Enabled = Convert.ToBoolean(drPackage["IsEnabled"]),
                FileList = null,
                PackageIntact = isPackageModified ? "False" : "True",
                SystemName = Convert.ToString(drPackage["SystemName"]),
                PackageGroupName = Convert.ToString(drPackage["PackageGroupName"])
            };
            return objPack;
        }

        private static Boolean CheckFileIntact(DataRow[] drPackageFiles, IEnumerable<S3Object> lstPackageFiles)
        {
            var packageFiles = lstPackageFiles as IList<S3Object> ?? lstPackageFiles.ToList();
            if (drPackageFiles == null || drPackageFiles.Length == 0 || lstPackageFiles == null || !packageFiles.Any()) return false;
            var count = packageFiles.Count();
            if (count != drPackageFiles.Length) return true;
            string[] formats = { "ddd, dd MMM yyyy HH:mm:ss GMT", "ddd, dd M yyyy HH:mm:ss GMT" };
            return (from s3Obj in packageFiles
                    let drPackage = drPackageFiles.FirstOrDefault(dr => Convert.ToString(dr["FilePath"]) == s3Obj.Key)
                    where drPackage == null ||
                    (DateTime.Compare(Convert.ToDateTime(drPackage["FileLastModifiedOnUTC"]), s3Obj.LastModified.ToUniversalTime()) != 0)
                    //!String.Equals(Convert.ToDateTime(drPackage["FileLastModifiedOnUTC"]).ToString("G", CultureInfo.InvariantCulture), s3Obj.LastModified.ToUniversalTime().ToString("G", CultureInfo.InvariantCulture), StringComparison.InvariantCulture)
                    select s3Obj).Any();
        }

        protected void gvPackages_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName != "PackageIntact") return;
            if (String.Equals(e.CellValue.ToString(), "False", StringComparison.InvariantCultureIgnoreCase))
                e.Cell.BackColor = System.Drawing.Color.Red;
        }

        protected void gvPackages_DetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvFiles = gvPackages.FindDetailRowTemplateControl(e.VisibleIndex, "gvFiles") as ASPxGridView;
                if (gvFiles != null)
                {
                    gvFiles.DataBind();
                    var packageIntact = (String)gvPackages.GetRowValues(e.VisibleIndex, "PackageIntact");
                    var btnUpdate = gvPackages.FindDetailRowTemplateControl(e.VisibleIndex, "btnUpdate") as Button;
                    btnUpdate.Visible = packageIntact.Equals("false", StringComparison.InvariantCultureIgnoreCase);
                }
            }
        }

        /// <summary>
        /// gvFileのrow open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvFiles_DetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvFiles = sender as ASPxGridView;
                var gvMemo = gvFiles.FindDetailRowTemplateControl(e.VisibleIndex, "gvMemos") as ASPxGridView;
                if (gvMemo != null)
                {
                    gvMemo.DataBind();
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var btnthis = (Button)sender;
            var row = (GridViewDetailRowTemplateContainer)btnthis.NamingContainer;
            var objPackage = (Package)gvPackages.GetRow(row.VisibleIndex);
            if (objPackage != null)
            {
                //get the updated Files
                //var lstPackageFiles = GetAWSS3Packages(objPackage.PackagePath, JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
                var lstPackageFiles = PackageBLL.GetPackaeFileList(objPackage.PackagePath, JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
                CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                var filtered = from item in lstPackageFiles
                               select new { item.Key, item.Size, LastModified = Convert.ToDateTime(item.LastModified) };
                DataTable dtPackageFiles = ToFileListDataTable(filtered);
                Thread.CurrentThread.CurrentCulture = currentCulture;
                //update Package information (Package Files Table)
                PackageBLL.UpdatePackageFiles(objPackage.PackageID, LoginUtility.GetCurrentUser(true).Email, dtPackageFiles);
                //update the admin activity log
                LogAdminActivity(TasksPerformed.Sync, ActivityLogNotes.PACKAGE_SYNC, objPackage.PackageName);
                WebUtility.HttpRedirect(null, Request.RawUrl);
            }
        }

        protected void gvFiles_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            //Rebind the details grid
            detailGrid.DataBind();
        }

        protected void gvPackages_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                return;
            }

            if (!cbxSearchByPName.Checked && !cbxSearchByPGroup.Checked && !cbxSearchByPPath.Checked && !cbxSearchByPType.Checked)
            {
                // Error
                lblErrorMsg.Visible = true;
                return;
            }

            String pName = cbxSearchByPName.Checked ? txtSearchByPName.Text.Trim() : String.Empty;
            String pGroup = cbxSearchByPGroup.Checked ? txtSearchByPGroup.Text.Trim() : String.Empty;
            String pPath = cbxSearchByPPath.Checked ? txtSearchByPPath.Text.Trim() : String.Empty;
            String pType = cbxSearchByPType.Checked ? txtSearchByPType.Text.Trim() : String.Empty;

            if (pName == String.Empty && pGroup == String.Empty && pPath == String.Empty && pType == String.Empty)
            {
                lblErrorMsg.Visible = true;
                return;
            }

            // Get Pacakge and File data.
            var dsPackageAndFiles = PackageBLL.SelectPackagesWithFiles(ModelConfigTypeInfo.CONFIGTYPE_JP);
            if (!dsPackageAndFiles.Tables[0].IsNullOrEmpty() && !dsPackageAndFiles.Tables[1].IsNullOrEmpty())
            {
#if true
                //Get latest s3 objects
                var s3Objects = GetAWSS3Packages(JpS3Settings.BasePath.Replace("/", ""), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
#else
                //AEFTODO: this is dummy method for Unit Test
                var s3Objects = Dummy_GetAWSS3Packages(JpS3Settings.BasePath.Replace("/", ""), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
#endif
                //compare with local db items and set the File Intact
                gvPackages.DataSource = GetUpdatedPackages(dsPackageAndFiles.Tables[0], dsPackageAndFiles.Tables[1], s3Objects,
                                                            pName, pGroup, pPath, pType);
            }
        }

        protected void gvFiles_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var packageId = (long)detailGrid.GetMasterRowKeyValue();
            var files = PackageHelper.GetPackageFiles(packageId);
            var packagePath = ConvertUtility.ConvertNullToEmptyString(detailGrid.GetMasterRowFieldValues("PackagePath"));

            // Get Memo情報
            var enMemoDic = new Dictionary<long, string>();
            var jpMemoDic = new Dictionary<long, string>();
            files.AsEnumerable().OrderBy(x => x.FilePath).Select(s => s.FileID).ToList().ForEach(id =>
            {
                var classKey = ClassKeyTemplate + id.ToString(CultureInfo.InvariantCulture).ToUpperInvariant();
                var resKey = ResourceKeyTemplate;

                String tmpMemo = string.Empty;
                // Get English Memo Content.
                if (Res_ContentStoreBLL.GetContentStr(classKey, resKey, new CultureInfo("en"), true, out tmpMemo))
                {
                    if (!enMemoDic.ContainsKey(id))
                    {
                        enMemoDic.Add(id, HttpUtility.HtmlDecode(tmpMemo));
                    }
                }
                else
                {
                    if (!enMemoDic.ContainsKey(id))
                    {
                        enMemoDic.Add(id, string.Empty);
                    }
                }

                // Get Japanese Memo Content.
                tmpMemo = string.Empty;
                if (Res_ContentStoreBLL.GetContentStr(classKey, resKey, new CultureInfo("ja"), true, out tmpMemo))
                {
                    if (!jpMemoDic.ContainsKey(id))
                    {
                        jpMemoDic.Add(id, HttpUtility.HtmlDecode(tmpMemo));
                    }
                }
                else
                {
                    if (!jpMemoDic.ContainsKey(id))
                    {
                        jpMemoDic.Add(id, string.Empty);
                    }
                }
            });

            // Create DataSource
            var filteredFiles = files.AsEnumerable()
                   .OrderBy(x => x.FilePath)
                   .Select((x, iterator) => new
                   {
                       //ParentPackageId = packageId,
                       FileID = x.FileID,
                       Key = x.FilePath.Replace(packagePath, String.Empty),
                       SNo = iterator + 1,
                       FileSize = x.FileSize,
                       LastModified = Convert.ToDateTime(x.FileLastModifiedOnUTC).ToUniversalTime(),
                       ShowNewIcon = x.ShowNewIcon ?? false,
                       ShowUpdateIcon = x.ShowUpdateIcon ?? false,
                       UpdateDate = x.ModifiedOnUTC ?? null,
                       //ModifiedFileData = false,
                       //ModifiedMemoData = false,
                       JPMemo = jpMemoDic.ContainsKey(x.FileID) ? jpMemoDic[x.FileID] : string.Empty,
                       ENMemo = enMemoDic.ContainsKey(x.FileID) ? enMemoDic[x.FileID] : string.Empty
                   });
            detailGrid.DataSource = filteredFiles;
        }
        protected void gvMemo_BeforePerformDataSelect(object sender, EventArgs e)
        {
            // Memo Grid
            var gvMemoGrid = (ASPxGridView)sender;

            // Get Memo data from MasterGrid(gvFiles Grid).
            var JPMemo = ConvertUtility.ConvertNullToEmptyString(gvMemoGrid.GetMasterRowFieldValues("JPMemo"));
            var ENMemo = ConvertUtility.ConvertNullToEmptyString(gvMemoGrid.GetMasterRowFieldValues("ENMemo"));

            // Create DataSource
            var dataSource = new List<GridMemoField>() {
                new GridMemoField() { MemoLanguage = "EN", MemoText = ENMemo },
                new GridMemoField() { MemoLanguage = "JP", MemoText = JPMemo }
            };



            gvMemoGrid.DataSource = dataSource;
        }

        private void LogAdminActivity(String action, String taskPerformed, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            ActivityLogBLL.Insert(LoggedInUserEmail, AreaEffected, taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
        }
        private static DataTable ToFileListDataTable(dynamic s3Objects)
        {
            var dataTable = new DataTable();
            dataTable.Columns.AddRange(new[] { new DataColumn("Key"), 
                new DataColumn("LastModified"), 
                new DataColumn("Size") });
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            //Thread.CurrentThread.SetUsCulture();//.CurrentCulture = new CultureInfo("en-US");
            foreach (var n in s3Objects)
            {
                var dr = dataTable.NewRow();
                dr["Key"] = n.Key;
                dr["LastModified"] = n.LastModified.ToUniversalTime();
                dr["Size"] = n.Size;
                dataTable.Rows.Add(dr);
            }
            // Thread.CurrentThread.SetCurrentCulture(currentCulture);
            return dataTable;
        }

        private List<S3Object> GetAWSS3Packages(String prefix, String regionCfgKey, String type, String delimeter)
        {
            JapanDonloadsCacheFactory jpDLCacheobject = new JapanDonloadsCacheFactory();
            return jpDLCacheobject.GetData(prefix, regionCfgKey, type, delimeter);
        }

        //AEKTODO: delete this. dummy method for Unit Test
        private List<S3Object> Dummy_GetAWSS3Packages(String prefix, String regionCfgKey, String type, String delimeter)
        {
            JapanDonloadsCacheFactory jpDLCacheobject = new JapanDonloadsCacheFactory();
            return jpDLCacheobject.Dummy_GetData(prefix, regionCfgKey, type, delimeter);
        }


        protected void btnRemovePackageCache_Click(object sender, EventArgs e)
        {
            JapanDonloadsCacheFactory cacheObj = new JapanDonloadsCacheFactory();
            cacheObj.RemoveAll();
            lblCacheClear.Text = "Cache Cleared";
        }

        protected void bttPackageSearch_Click(object sender, EventArgs e)
        {
            if (!cbxSearchByPName.Checked && !cbxSearchByPGroup.Checked && !cbxSearchByPPath.Checked && !cbxSearchByPType.Checked)
            {
                // Error
                lblErrorMsg.Visible = true;
                return;
            }

            String pName = cbxSearchByPName.Checked ? txtSearchByPName.Text.Trim() : String.Empty;
            String pGroup = cbxSearchByPGroup.Checked ? txtSearchByPGroup.Text.Trim() : String.Empty;
            String pPath = cbxSearchByPPath.Checked ? txtSearchByPPath.Text.Trim() : String.Empty;
            String pType = cbxSearchByPType.Checked ? txtSearchByPType.Text.Trim() : String.Empty;

            // Get Pacakge and File data.
            var dsPackageAndFiles = PackageBLL.SelectPackagesWithFiles(ModelConfigTypeInfo.CONFIGTYPE_JP);
            if (!dsPackageAndFiles.Tables[0].IsNullOrEmpty() && !dsPackageAndFiles.Tables[1].IsNullOrEmpty())
            {
				List<S3Object> s3Objects;
#if true
                try
                {
                    //Get latest s3 objects
                    s3Objects = GetAWSS3Packages(JpS3Settings.BasePath.Replace("/", ""), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
                }
                catch (Exception ex)
                {
                    if (ex.HResult == int.Parse(ConfigUtility.AppSettingGetValue("Connect.Web.IllegalCharactersExceptionHResult")))
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = GetStaticResource("lclErrorPackageInvalidChars")
                            .Replace("[[PackagePath]]", ex.Message)
                            .Replace("[[InvalidCharacters]]", ConfigUtility.AppSettingGetValue("Connect.Web.PackageInvalidChars"));
                        return;
                    }
                    else
                        throw;
                }
#else
                //AEFTODO: this is dummy method for Unit Test
                var s3Objects = Dummy_GetAWSS3Packages(JpS3Settings.BasePath.Replace("/", ""), JpS3Settings.ActiveRegion, JpS3Settings.AwsConfigType, null);
#endif
                //compare with local db items and set the File Intact
                gvPackages.DataSource = GetUpdatedPackages(dsPackageAndFiles.Tables[0], dsPackageAndFiles.Tables[1], s3Objects,
                                                            pName, pGroup, pPath, pType);
                gvPackages.DataBind();
            }
        }
        #region dead code
        protected void bttSaveGridData_Click(object sender, EventArgs e)
        {
            var btnthis = (Button)sender;
            var row = (GridViewDetailRowTemplateContainer)btnthis.NamingContainer;
            var gvFiles = gvPackages.FindDetailRowTemplateControl(row.VisibleIndex, "gvFiles") as ASPxGridView;

            Enumerable.Range(0, gvFiles.VisibleRowCount).ToList().ForEach(rowNum =>
            {
                //Debug Code
                if (gvFiles.GetRow(rowNum) == null)
                    return;

                // Update Package File Detail data.(new icon, update icon, update date)
                if ((bool)gvFiles.GetRowValues(rowNum, "ModifiedFileData") == true)
                {
                    var pkgFle = new Package_Files();
                    pkgFle.ModifiedOnUTC = Convert.ToDateTime(gvFiles.GetRowValues(rowNum, "UpdateDate"));
                    pkgFle.ShowUpdateIcon = (bool?)gvFiles.GetRowValues(rowNum, "ShowUpdateIcon");
                    pkgFle.ShowNewIcon = (bool?)gvFiles.GetRowValues(rowNum, "ShowNewIcon");
                    pkgFle.FileID = Convert.ToInt32(gvFiles.GetRowValues(rowNum, "FileID"));

                    int update = (int)BLL_PackageFile.UpdatePackageFileById(pkgFle);
                    if (update > 0)
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = GetStaticResource("lblMsgFileSaved");
                    }
                    else
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = GetStaticResource("lblMsgFileNotSaved");
                        return;
                    }
                }

                // Update / Insert Package File Memo data.(English Memo, Japanaese Memo)
                if ((bool)gvFiles.GetRowValues(rowNum, "ModifiedMemoData") == true)
                {
                    var fileId = Convert.ToInt64(gvFiles.GetRowValues(rowNum, "FileID"));
                    var classKey = ClassKeyTemplate + fileId.ToString(CultureInfo.InvariantCulture).ToUpperInvariant();
                    var resKey = ResourceKeyTemplate;
                    var enMemo = HttpUtility.HtmlEncode(gvFiles.GetRowValues(rowNum, "ENMemo"));
                    var jpMemo = HttpUtility.HtmlEncode(gvFiles.GetRowValues(rowNum, "JPMemo"));
                    Res_ContentStoreBLL.InsertUpdate(classKey, resKey, "en-US", enMemo);
                    Res_ContentStoreBLL.InsertUpdate(classKey, resKey, "ja-JP", jpMemo);
                }

            });
        }

        protected void cbxShowNewIcon_CheckedChanged(object sender, EventArgs e)
        {
            return;
            // 押下されたsenderと同じRowの、別のColumnの値を変更したい。
            //((ASPxCheckBox)((CheckBox)sender).Parent.FindControl("ModifiedFileData")).Checked = true;
            CheckBox cbx = sender as CheckBox;
            if (cbx == null)
                return;

            System.Web.UI.Control obj = cbx;
            while (true)
            {
                obj = obj.Parent as System.Web.UI.Control;
                if (obj != null)
                {
                    Console.WriteLine("{0}", obj.ID);
                }
                else
                {
                    break;
                }
            }

            // find "gvFiles" Grid to getting row.VisiblityIndex
            GridViewDetailRowTemplateContainer gvFiles = null;
            FindDetailGridObject(cbx, ref gvFiles);
            if (gvFiles != null)
            {
                var modFileData = ((ASPxGridView)gvFiles.NamingContainer).FindDetailRowTemplateControl(gvFiles.VisibleIndex, "ModifiedFileData") as CheckBox;
                if (modFileData != null)
                {
                    modFileData.Checked = ((CheckBox)sender).Checked;
                }
            }



            // find DataRow.
            DevExpress.Web.Rendering.GridViewTableDataRow dataRow = null;
            FindDataRowObject(cbx, ref dataRow);
            if (dataRow != null)// && gvFiles != null)
            {
                // Set Value
                var checkBox = dataRow.FindControl("ModifiedFileDate") as CheckBox;
                checkBox.Checked = ((CheckBox)sender).Checked;

                // test
                var checkBox2 = dataRow.FindControl("ModifiedMemoDate") as CheckBox;
                checkBox2.Checked = ((CheckBox)sender).Checked;
            }

        }

        private void FindDetailGridObject(CheckBox cbx, ref GridViewDetailRowTemplateContainer detailGrid)
        {
            System.Web.UI.Control obj = cbx;
            System.Web.UI.Control castedObj = null;
            while (true)
            {
                var webControlObj = obj as System.Web.UI.Control;
                if (webControlObj != null)
                {
                    // BackUp
                    obj = webControlObj.Parent;

                    // DataRowか
                    castedObj = obj as GridViewDetailRowTemplateContainer;

                    if (castedObj != null && castedObj.ID == "gvFiles")
                    {
                        // Found DataRow. castedObj is DataRow.
                        detailGrid = (GridViewDetailRowTemplateContainer)castedObj;
                        break;
                    }
                }
                else
                {
                    return;
                }
            };
        }

        private void FindDataRowObject(CheckBox cbx, ref DevExpress.Web.Rendering.GridViewTableDataRow dataRow)
        {
            System.Web.UI.Control obj = cbx;
            System.Web.UI.Control castedObj = null;
            while (true)
            {
                var webControlObj = obj as System.Web.UI.Control;
                if (webControlObj != null)
                {
                    // BackUp
                    obj = webControlObj.Parent;

                    // DataRowか
                    castedObj = obj as DevExpress.Web.Rendering.GridViewTableDataRow;

                    if (castedObj != null)
                    {
                        // Found DataRow. castedObj is DataRow.
                        dataRow = (DevExpress.Web.Rendering.GridViewTableDataRow)castedObj;
                        break;
                    }
                }
                else
                {
                    return;
                }
            };

        }

        protected void cbxShowUpdateIcon_CheckedChanged(object sender, EventArgs e)
        {
            // 押下されたsenderと同じRowの、別のColumnの値を変更したい。
            ((ASPxCheckBox)((CheckBox)sender).Parent.FindControl("ModifiedFileData")).Checked = true;
        }

        protected void dxDateTime_DateChanged(object sender, EventArgs e)
        {
            // 押下されたsenderと同じRowの、別のColumnの値を変更したい。
            ((ASPxCheckBox)((CheckBox)sender).Parent.FindControl("ModifiedFileData")).Checked = true;
        }

        protected void txtMemo_TextChanged(object sender, EventArgs e)
        {
            // gvFilesの、現在編集中のRowの、Memo Columnに、変更した内容を反映させたい
            ((ASPxCheckBox)((CheckBox)sender).Parent.FindControl("ModifiedMemoData")).Checked = true;
        }

        protected void txtMemo_PreRender(object sender, EventArgs e)
        {
            // TextBox(Multiline Mode) initialize to Rich TextArea by tinyMCE JavaScript.
            //var txtMemo = sender as ASPxTextBox;
            //String js = string.Empty;
            //js += "<script type=\"text/javascript\" src=\"/App_ClientControls/tiny_mce/tiny_mce.js\"></script>\r\n";
            //js += "<script type=\"text/javascript\">\r\n";
            //js += "tinyMCE.init({\r\n";
            //js += "theme: \"advanced\",\r\n";
            //js += "mode: \"exact\",\r\n";
            //js += "elements: \"" + txtMemo.ClientID + "\",\r\n";
            //js += "convert_urls: false,\r\n";
            //js += "forced_root_block: \"\",\r\n";
            //js += "});\r\n";
            //js += "</script>\r\n";
            //litRichToTextArea.Text = js;
        }

        protected void gvFiles_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
        {

        }

        protected void gvFiles_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

        }

        protected void gvFiles_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
        {

        }




        protected void testSave_Click(object sender, EventArgs e)
        {
            var gvFiles = gvPackages.FindDetailRowTemplateControl(0, "gvFiles") as ASPxGridView;
            Enumerable.Range(0, gvFiles.VisibleRowCount).ToList().ForEach(rowNum =>
            {
                //Debug Code
                if (gvFiles.GetRow(rowNum) == null)
                    return;

                // Update Package File Detail data.(new icon, update icon, update date)
                if ((bool)gvFiles.GetRowValues(rowNum, "ModifiedFileData") == true)
                {
                    var pkgFle = new Package_Files();
                    pkgFle.ModifiedOnUTC = Convert.ToDateTime(gvFiles.GetRowValues(rowNum, "UpdateDate"));
                    pkgFle.ShowUpdateIcon = (bool?)gvFiles.GetRowValues(rowNum, "ShowUpdateIcon");
                    pkgFle.ShowNewIcon = (bool?)gvFiles.GetRowValues(rowNum, "ShowNewIcon");
                    pkgFle.FileID = Convert.ToInt32(gvFiles.GetRowValues(rowNum, "FileID"));

                    int update = (int)BLL_PackageFile.UpdatePackageFileById(pkgFle);
                    if (update > 0)
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = GetStaticResource("lblMsgFileSaved");
                    }
                    else
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = GetStaticResource("lblMsgFileNotSaved");
                        return;
                    }
                }

                // Update / Insert Package File Memo data.(English Memo, Japanaese Memo)
                if ((bool)gvFiles.GetRowValues(rowNum, "ModifiedMemoData") == true)
                {
                    var fileId = Convert.ToInt64(gvFiles.GetRowValues(rowNum, "FileID"));
                    var classKey = ClassKeyTemplate + fileId.ToString(CultureInfo.InvariantCulture).ToUpperInvariant();
                    var resKey = ResourceKeyTemplate;
                    var enMemo = HttpUtility.HtmlEncode(gvFiles.GetRowValues(rowNum, "ENMemo"));
                    var jpMemo = HttpUtility.HtmlEncode(gvFiles.GetRowValues(rowNum, "JPMemo"));
                    Res_ContentStoreBLL.InsertUpdate(classKey, resKey, "en-US", enMemo);
                    Res_ContentStoreBLL.InsertUpdate(classKey, resKey, "ja-JP", jpMemo);
                }

            });
        }
        #endregion

    }
}