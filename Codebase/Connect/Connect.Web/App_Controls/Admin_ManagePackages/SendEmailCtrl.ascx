﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendEmailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ManagePackages.SendEmailCtrl" %>

 <script type="text/javascript" src="../../App_ClientControls/tiny_mce/tiny_mce.js"></script>
    <style type="text/css">
       textarea{
            padding:5px 10px;
            box-sizing: border-box;
        }
         /*.email-required-msgs {
            margin-left:50px;
        }
        .email-required-msgs .expressionerror {
            margin:0;
            width:auto;
        }*/
    </style>
    <script type="text/javascript">
        tinyMCE.init({
            theme: "advanced",
            mode: "exact",
            elements: <%="'"+txtBody.ClientID+"'"%>,
            convert_urls: false,
            forced_root_block: ""
        });
    </script>

<anrui:GlobalWebBoxedPanel ID="pnlEmailInterface" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl, pnlEmailInterface.HeaderText%>'>
    <div class="settingrow group input-text required">
        <label><asp:Localize ID="lcalFrom" runat="server" Text='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl, lcalFrom.Text%>'></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="txtFrmAddErrMsg" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <p class="custom-error-message">
            <asp:Localize ID="txtFrmCstmErrMsg" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,revFromEmail.ErrorMessage %>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtFromAddress" runat="server" ValidationGroup="vgSendEmail" CssClass="email"></asp:TextBox>
        <%--<div class="email-required-msgs">
            <asp:RequiredFieldValidator id="rfvFromEmail" Display="Dynamic" runat="server" ControlToValidate="txtFromAddress"  ValidationGroup="vgSendEmail" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revFromEmail" runat="server"  Display="Dynamic"
                ControlToValidate="txtFromAddress" ValidationGroup="vgSendEmail" 
                ErrorMessage='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,revFromEmail.ErrorMessage %>' 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </div>--%>
    </div>
    <div class="settingrow group input-textarea required small-textarea">
        <label><asp:Localize ID="lcalTo" runat="server" Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,lcalTo.Text %>'></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="txtToErrMsg" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <p class="server-error-message">
            <asp:RegularExpressionValidator ID="revToAddress" runat="server"  Display="Dynamic"
                ControlToValidate="txtToAddress" ValidationGroup="vgSendEmail" 
                ErrorMessage='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,revToAddress.ErrorMessage %>' 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </p>
        <asp:TextBox ID="txtToAddress" runat="server" TextMode="MultiLine" ValidationGroup="vgSendEmail"></asp:TextBox>

        <%--<div class="email-required-msgs">
            <asp:RequiredFieldValidator id="rfvToAddress" Display="Dynamic" runat="server" ControlToValidate="txtToAddress"  ValidationGroup="vgSendEmail"  ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revToAddress" runat="server"  Display="Dynamic"
                ControlToValidate="txtToAddress" ValidationGroup="vgSendEmail" 
                ErrorMessage='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,revToAddress.ErrorMessage %>' 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </div>--%>
    </div>
    <div class="settingrow group input-textarea small-textarea">
       <label><asp:Localize ID="lcalBCC" runat="server" Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,lcalBCC.Text %>'>
       </asp:Localize></label>
        <asp:TextBox ID="txtBCCAddress" runat="server"  ReadOnly="false" TextMode="MultiLine"></asp:TextBox>
    </div>
    <div class="settingrow group input-text required">
       <label><asp:Localize ID="lcalSubject" runat="server" Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,lcalSubject.Text %>'>
       </asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="txtSubErrMsg" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtSubject" runat="server" ValidationGroup="vgSendEmail"></asp:TextBox>
        <%--<div class="email-required-msgs">
            <asp:RequiredFieldValidator id="rfvSubject" Display="Dynamic" runat="server" ControlToValidate="txtSubject"  ValidationGroup="vgSendEmail" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>--%>
    </div>
      <div class="settingrow body">
       <label><asp:Localize ID="lcalBody" runat="server" Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,lcalBody.Text %>'>
       </asp:Localize></label>
          <%--<p class="error-message">
              <asp:Localize ID="txtBdyErrMsg" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,rfvBody.ErrorMessage %>"></asp:Localize>
          </p>--%>
          <p class="server-error-message">
              <%--<div class="email-required-fields">--%>
                <asp:RequiredFieldValidator id="rfvBody" Display="Dynamic" runat="server" ControlToValidate="txtBody"  ValidationGroup="vgSendEmail" ErrorMessage='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,rfvBody.ErrorMessage %>'></asp:RequiredFieldValidator>
              <%--</div>--%>
          </p>
          <asp:TextBox ID="txtBody" runat="server" TextMode="MultiLine"  CssClass="mceEditor" ValidationGroup="vgSendEmail" Width="100%" Height="500px"></asp:TextBox>
          <%--<div class="email-required-fields">
            <asp:RequiredFieldValidator id="rfvBody" Display="Dynamic" runat="server" ControlToValidate="txtBody"  ValidationGroup="vgSendEmail" ErrorMessage='<%$Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,rfvBody.ErrorMessage %>'></asp:RequiredFieldValidator>
          </div>--%>
    </div>
   <div class="settingrow">
       <asp:Localize ID="lcalMsg" runat="server" Visible="false">
       </asp:Localize>
       
    </div>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_PackageAdmin_SendEmailCtrl" />
    </div>
</anrui:GlobalWebBoxedPanel>
  <div class="settingrow margin-top-15">
        <asp:Button ID="bttSendEmail" runat="server" ValidationGroup="vgSendEmail"  Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,bttSendEmail.Text %>' OnClick="bttSendEmail_Click" SkinID="submit-btn"/>
        
      <asp:HyperLink ID="btnCancel" runat="server" SkinID="button" NavigateUrl="~/SiteAdmin/PackageAdmin/PackageList.aspx" Text='<%$ Resources:ADM_STCTRL_PackageAdmin_SendEmailCtrl,btnCancel.Text %>'/>
    </div>