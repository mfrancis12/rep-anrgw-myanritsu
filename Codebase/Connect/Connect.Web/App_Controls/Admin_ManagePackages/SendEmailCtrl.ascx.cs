﻿/*
 * Author:        kishore kumar M
 * Created Date:  02/25/2014
 * Modified Date: 03/08/2014
 * Modified By:   kishore kumar M
 * Purpose:       An Interface to send an email to all the emails which are assocated 
 * with the packages (TAU Downloads Section)
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using Anritsu.Connect.Web.SiteAdmin.PackageAdmin;

namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public partial class SendEmailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        #region Properties

        private Int64 PackageID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.PackageId], 0); }
        }

        private String  PackageName
        {
            get { return ConvertUtility.ConvertToString(Request.QueryString[App_Lib.KeyDef.QSKeys.PName], string.Empty); }
        }

        public string ToAddress { get; set; }
        public string FromAddress { get; set; }
        public string BccAddress { get; set; }
        public string Body { get; set; }
        #endregion

        #region PageEvents

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //load email content 
                LoadToAndBccEmailAddress();
            }
        }

        /// <summary>
        /// Sending an email using EmailAPI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttSendEmail_Click(object sender, EventArgs e)
        {
            String errorMsg = String.Empty;

            //send email
            SendEmailCallRequest emailCallReq = new SendEmailCallRequest();
            var SendEmail = new SendEmail();
            String bccList =SendEmail.GetBCCList(PackageID);
            if (!String.IsNullOrEmpty(bccList))
            {
                emailCallReq.BccEmailAddresses = bccList.Split(';');
            }
            if (!String.IsNullOrEmpty(txtToAddress.Text.Trim()))
            {
                String[] tolist = { txtToAddress.Text.Trim() };
                emailCallReq.ToEmailAddresses = tolist;
            }
            emailCallReq.EmailSubject = txtSubject.Text.Trim();
            emailCallReq.FromEmailAddress = txtFromAddress.Text.Trim();
            emailCallReq.EmailBody = txtBody.Text.Trim();
            emailCallReq.IsHtmlEmailBody = true;
            BaseResponseType resp = EmailServiceManager.SendEmail(emailCallReq);
            if (resp.StatusCode == 0)
            {
                lcalMsg.Text = "Email Sent";
                lcalMsg.Visible = true;
            }

        }

        #endregion

        #region Methods

        private void LoadToAndBccEmailAddress()
        {
            //load to email address
            txtToAddress.Text = ToAddress; // ConfigUtility.AppSettingGetValue("TAU_DownloadSupport_ToEmailAddress");
            //load From address 
            txtFromAddress.Text = FromAddress;// ConfigUtility.AppSettingGetValue("TAU_DownloadSupport_FromEmailAddress");
            //load you BCC address
            txtBCCAddress.Text = BccAddress;//GetBCCList();
            //load default Body tempalte
            var pnameBody = Body.Replace("[[PackageName]]", PackageName.Replace('!', '&'));
            txtBody.Text = pnameBody;//this.GetGlobalResourceObject("~/siteadmin/packageadmin/SendEmail", "ESDSupportEmailContent.Text").ToString();
           
        }

        

        #endregion

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_PackageAdmin_SendEmailCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion
    }
}