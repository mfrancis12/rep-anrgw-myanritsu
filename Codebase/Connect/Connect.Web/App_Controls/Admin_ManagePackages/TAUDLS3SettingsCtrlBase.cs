﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ManagePackages
{
    public class Awss3SettingsCtrlBase: UserControl
    {
        public static class TauS3Settings
        {
            public static String ActiveRegion
            {
                get { return "EU"; }
            }

            public static String AwsConfigType
            {
                get { return "TAUDL"; }
            }

            public static String BasePath
            {
                get { return ConfigUtility.AppSettingGetValue(AwsConfigType + ".AWS.S3." + ActiveRegion + ".BasePath"); }
            }
        }

        public  static class JpS3Settings
        {
            public static String ActiveRegion
            {
                get { return "JP"; }
            }

            public static String AwsConfigType
            {
                get { return "JPDL"; }
            }

            public static String BasePath
            {
                get { return ConfigUtility.AppSettingGetValue(AwsConfigType + ".AWS.S3." + ActiveRegion + ".BasePath"); }
            }
        }
    }
}