﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public class BC_AdminOrg : System.Web.UI.UserControl
    {
        public Guid AccountID { get { return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID], Guid.Empty); } }
    }
}