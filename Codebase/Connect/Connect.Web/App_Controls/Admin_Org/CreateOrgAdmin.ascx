﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateOrgAdmin.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_Org.CreateOrgAdmin" %>
<%@ Register Src="~/App_Controls/AddressCtrl.ascx" TagPrefix="uc1" TagName="AddressCtrl" %>


<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_CompanyProfileWzCtrl,pnlContainer.HeaderText%>">
    <script type="text/javascript">
        // <![CDATA[
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value)
                popupControl.Show();
            else
                popupControl.Hide();
        }
        function SetImageState(value) {
            var img = document.getElementById('imgButton');
            var imgSrc = value ? 'Images/Buttons/pcButtonPress.gif' : 'Images/Buttons/pcButton.gif';
            img.src = imgSrc;
        }
        function GetPopupControl() {
            return ASPxPopupClientControl;
        }

        // ]]> 
    </script>

    <div class="settingrow">
    </div>
    <%--Team/Oranization Name--%> 
        <div class="settingrow width-60 group input-text required">
            <label>
                <asp:Localize ID="lcalAccountName" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalAccountName.Text%>"></asp:Localize>:*</label>
            <p class="error-message">
                <asp:Localize ID="lclAccntErr" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,rfvAccountName.ErrorMessage%>"></asp:Localize>
            </p>
            <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
                Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
                ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtAccountName" runat="server" MaxLength="300"></asp:TextBox>

        </div>
        <div class="detailed-label">
            <asp:Localize ID="lcalAccountInfoIntro" runat="server"></asp:Localize>
            <dx:ASPxImage ID="imgMoreInfo" runat="server"  Width="11px"
                Height="11px" Visible="false" Cursor="pointer" ToolTip="<%$Resources:STCTRL_CompanyProfileWzCtrl,imgMoreInfo.Text%>">
            </dx:ASPxImage>

            <dx:ASPxPopupControl ClientInstanceName="ASPxPopupClientControl" Width="330px" Height="200px" Theme="Aqua"
                MaxWidth="800px" MaxHeight="800px" MinHeight="150px" MinWidth="150px" ID="pcMain"
                ShowFooter="false" PopupElementID="imgMoreInfo" HeaderText=""
                runat="server" PopupHorizontalAlign="LeftSides" PopupVerticalAlign="Below" EnableHierarchyRecreation="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <asp:Panel ID="Panel1" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Localize ID="lcalAccountFullInfo" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalAccountInfoIntro.Text%>"></asp:Localize>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ClientSideEvents CloseUp="function(s, e) { SetImageState(false); }" PopUp="function(s, e) { SetImageState(true); }" />
            </dx:ASPxPopupControl>
        </div>
    <%--Team/Oranization Name End--%>
    <div class="settingrow">
    </div>
    <%--Team Owner Email Address--%>
       <div class="overflow">
        <div class="settingrow group width-60 input-text left required">
            <label>
                <asp:Localize ID="localTeamOwnerEmail" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,TeamOwner%>"></asp:Localize>*</label>
            <p class="error-message">
                <asp:Localize ID="lclTmOwnrErr" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,rfvCompanyName.ErrorMessage%>"></asp:Localize>
            </p>
            <asp:RegularExpressionValidator ID="revtxtOwnerEmail" runat="server" ValidationGroup="vgCompInfo1" Display="Dynamic"
            ControlToValidate="txtTeamOwnerEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ErrorMessage='<%$ Resources:ADM_STCTRL_SiteAdminUsers,revNewUserEmail.ErrorMessage %>'></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtTeamOwnerEmail" runat="server" MaxLength="1000"></asp:TextBox>
        </div>
        <div class="width-30 left" style="margin:31px 0 0 20px">
                <asp:LinkButton ID="lnkEditEmail" runat="server" Text="Change Email" Font-Bold="false" Visible="false" OnClick="lnkEditCompanyNameAndEmail_Click" SkinID="blueit"></asp:LinkButton>
        </div>
    </div>
    <div class="detailed-label">
        <asp:Localize ID="lcalTeamOwnerInfo" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,TeamOwnerEmail%>"></asp:Localize>
         <dx:ASPxImage ID="TeamOwnerMoreInfoimg" runat="server"  Width="11px"
                         Height="11px" Cursor="pointer" ToolTip="Team owner email address">
            </dx:ASPxImage>
          <dx:ASPxPopupControl ClientInstanceName="ASPxPopupCleintTeamOwnerInfo" Width="330px" Height="200px" Theme="Aqua"
                MaxWidth="800px" MaxHeight="800px" MinHeight="150px" MinWidth="150px" ID="ASPxPopupTeamOwnerInfo"
                ShowFooter="false" PopupElementID="TeamOwnerMoreInfoimg" HeaderText=""
                runat="server" PopupHorizontalAlign="LeftSides" PopupVerticalAlign="Below" EnableHierarchyRecreation="True">
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                        <asp:Panel ID="Panel2" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Localize ID="lcalTeamOnerDetailedInfo" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,TeamOwnerEmail%>"></asp:Localize>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <ClientSideEvents CloseUp="function(s, e) { SetImageState(false); }" PopUp="function(s, e) { SetImageState(true); }" />
            </dx:ASPxPopupControl>

    </div>
    <%--Team Owner Email Address End--%>
    <div class="settingrow">
    </div>
    <div class="settingrow" style="padding-top: 5px;">
        <asp:Button ID="bttContinue1" runat="server" Text="Continue" OnClick="bttContinue1_Click" CausesValidation="true" ValidationGroup="vgCompInfo1" SkinID="submit-btn" />
    </div>

    <div class="settingrow">
    </div>
    <asp:Panel ID="pnlAccount" runat="server" Visible="false">
            <div class="overflow">
        <div class="settingrow group width-60 input-text left required">
            <label>
                <asp:Localize ID="lcalCompanyName" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyName.Text%>"></asp:Localize>*</label>
            <p class="error-message">
                <asp:Localize ID="lclCmpNameErr" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,rfvCompanyName.ErrorMessage%>"></asp:Localize>
            </p>
            <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
                Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo1"
                ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="1000"></asp:TextBox>
        </div>
       <%-- <div class="width-30 left">
                <asp:LinkButton ID="lnkEditCompanyName" runat="server" Text="Change Name" Font-Bold="false" Visible="false" OnClick="lnkEditCompanyNameAndEmail_Click" CssClass="blueit inline-block margin-top-30 margin-left-15" ></asp:LinkButton>
        </div>--%>
    </div>
    <div class="detailed-label">
          <asp:Localize ID="lcalCompanyInfoTitle" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyInfoTitle.Text %>"></asp:Localize>
    </div>

        <div class="settingrow">
        </div>
        <div class="settingrow group width-60">
            <label>
                <asp:Localize ID="lcalCompanyNameInRuby" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyNameInRuby.Text%>"></asp:Localize></label>
            <asp:TextBox ID="txtCompanyNameInRuby" runat="server" MaxLength="1000"></asp:TextBox>
        </div>


        <div class="settingrow">
        </div>
        <div class="settingrow width-60">
            <label>
                <asp:Localize ID="lcalCompanyAddress" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyAddress.Text%>"></asp:Localize></label>
            <div>
                <uc1:AddressCtrl ID="addrCompanyAddress" runat="server" ValidationGroup="vgCompInfo"
                    IsFaxVisible="false" IsFaxRequired="false" IsPhoneVisible="false" IsPhoneRequired="false" />
            </div>
        </div>


        <div class="settingrow">
        <span class="msg">
            <asp:Literal ID="ltrCompanyBasicInfo" runat="server"></asp:Literal>
        </span>
        </div>
        <div class="settingrow">
            <asp:Button ID="bttSaveCompanyProfile" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,bttSaveCompanyProfile.Text%>" OnClientClick=""
                ValidationGroup="vgCompInfo" CausesValidation="true" OnClick="bttSaveCompanyProfile_Click" SkinID="submit-btn" />
        </div>
    </asp:Panel>
</anrui:GlobalWebBoxedPanel>


<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_CompanyProfileWzCtrl" />

