﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public partial class CreateOrgAdmin : UserControl, IStaticLocalizedCtrl
    {
        public Boolean IsMyAnritsuUser
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(ViewState["MyAnritsuUser"], false);
            }
            set
            {
                ViewState["MyAnritsuUser"] = value.ToString();
            }
        }

        public Boolean IsSSOUser
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(ViewState["SSOUser"], false);
            }
            set
            {
                ViewState["SSOUser"] = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadProfile();
        }

        protected override void OnInit(EventArgs e)
        {
            imgMoreInfo.ImageUrl= TeamOwnerMoreInfoimg.ImageUrl = ConfigKeys.GwdataCdnPath +
                                   "/images/legacy-images/apps/connect/img/smquestionmark.jpg";
            base.OnInit(e);
            bttSaveCompanyProfile.OnClientClick = "return confirm ('" + "Are you sure you want to save?" + "')";
        }

        public void LoadProfile()
        {
            ShowOrgInformation();
            SetVisibleForCompanyNameInRuby(SiteUtility.BrowserLang_IsJapanese());

        }

        private void SetVisibleForCompanyNameInRuby(Boolean isVisible)
        {
            lcalCompanyNameInRuby.Visible = isVisible;
            txtCompanyNameInRuby.Visible = isVisible;
        }

        /// <summary>
        /// Show subsstring if string is long
        /// </summary>
        private void ShowOrgInformation()
        {
            string str = GetResourceString("lcalAccountInfoIntro.Text");
            if (str.Length > 65)
            {
                lcalAccountInfoIntro.Text = str.Substring(0, 60) + "...";
                imgMoreInfo.Visible = true;
            }
            else
            {
                lcalAccountInfoIntro.Text = str;
                imgMoreInfo.Visible = false;
            }
        }

        public void SaveCompanyBasicInfo()
        {
            try
            {
                #region " validation "
                if (txtCompanyName.Text.IsNullOrEmptyString()) throw new ArgumentException(GetStaticResource("ERROR_CompanyNameRequired"));
                if (txtAccountName.Text.IsNullOrEmptyString()) throw new ArgumentException(GetStaticResource("ERROR_AccountNameRequired"));
                if (txtAccountName.Text.Equals(KeyDef.MsgKeys.DefaultOrg)) throw new ArgumentException(GetStaticResource("ERROR_UpdateAccountName"));
                #endregion

                var loggedinUser = LoginUtility.GetCurrentUserOrSignIn();
                //get SSO User
                var ssoUserData = BLLSSOUser.SelectByEmailOrUserName(txtTeamOwnerEmail.Text.Trim());
                Guid membershipId;
                if (!IsSSOUser && !IsMyAnritsuUser)
                {
                    //create new user in My Anritsu
                    var createdUser = CreateMyAnritsuUser(ssoUserData, loggedinUser);
                    membershipId = createdUser.MembershipId;
                }
                else if (IsSSOUser && !IsMyAnritsuUser)
                {
                    //migrtae user from SSO to My Anritus
                    var createdUser = CreateMyAnritsuUser(ssoUserData, loggedinUser);
                    membershipId = createdUser.MembershipId;
                }
                else
                    membershipId = addrCompanyAddress.OwnerMemberShipId;
                //create new team
                var newAccountId = BLLAcc_AccountMaster.CreateAccount(membershipId
                         , txtAccountName.Text.Trim()
                         , txtCompanyName.Text.Trim()
                         , txtCompanyNameInRuby.Text.Trim()
                         , WebUtility.GetUserIP()
                         , loggedinUser.Email
                         , loggedinUser.FullName);
                var url = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetails, KeyDef.QSKeys.AccountID, newAccountId);
                WebUtility.HttpRedirect(this, url);

            }
            catch (ArgumentException aex)
            {
                ltrCompanyBasicInfo.Text = aex.Message;
            }
        }

        private Sec_UserMembership CreateMyAnritsuUser(DataRow ssoUser, ConnectSsoUser loggedinUser)
        {
            var newUser = new Sec_UserMembership();
            if (ssoUser == null)
            {
                newUser.GWUserId = 0;
                newUser.Email = txtTeamOwnerEmail.Text.Trim();
                newUser.UserID = String.Empty;
                newUser.LastName = "n/a";
                newUser.FirstName = "n/a";
                newUser.JobTitle = String.Empty;
                newUser.PhoneNumber = addrCompanyAddress.Phone;
                newUser.FaxNumber = addrCompanyAddress.Fax;
                newUser.UserAddressCountryCode = addrCompanyAddress.UserCountry;
                newUser.MiddleName = String.Empty;
                newUser.UserStreetAddress1 = addrCompanyAddress.Address1;
                newUser.UserStreetAddress2 = addrCompanyAddress.Address2;
                newUser.UserCity = addrCompanyAddress.City;
                newUser.UserState = addrCompanyAddress.State;
                newUser.UserZipCode = addrCompanyAddress.ZipCode;
            }
            else
            {
                newUser.GWUserId = ConvertUtility.ConvertToInt32(ssoUser["GWUserId"], 0);
                newUser.Email = ConvertUtility.ConvertToString(ssoUser["EmailAddress"], String.Empty);
                newUser.UserID = ConvertUtility.ConvertToString(ssoUser["LoginName"], String.Empty);
                newUser.LastName = ConvertUtility.ConvertToString(ssoUser["FirstName"], String.Empty);
                newUser.FirstName = ConvertUtility.ConvertToString(ssoUser["LastName"], String.Empty);
                newUser.JobTitle = ConvertUtility.ConvertToString(ssoUser["JobTitle"], String.Empty);
                newUser.PhoneNumber = ConvertUtility.ConvertToString(ssoUser["PhoneNumber"], String.Empty);
                newUser.FaxNumber = ConvertUtility.ConvertToString(ssoUser["FaxNumber"], String.Empty);
                newUser.UserAddressCountryCode = ConvertUtility.ConvertToString(ssoUser["CountryCode"], String.Empty);
                newUser.MiddleName = ConvertUtility.ConvertToString(ssoUser["MiddleName"], String.Empty);
                newUser.UserStreetAddress1 = ConvertUtility.ConvertToString(ssoUser["StreetAddress1"], String.Empty);
                newUser.UserStreetAddress2 = ConvertUtility.ConvertToString(ssoUser["StreetAddress2"], String.Empty);
                newUser.UserCity = ConvertUtility.ConvertToString(ssoUser["City"], String.Empty);
                newUser.UserState = ConvertUtility.ConvertToString(ssoUser["State"], String.Empty);
                newUser.UserZipCode = ConvertUtility.ConvertToString(ssoUser["ZipCode"], String.Empty);
                newUser.JobTitle = ConvertUtility.ConvertToString(ssoUser["JobTitle"], String.Empty);
            }
            //common fields
            newUser.LastLoginDate = DateTime.MinValue;
            newUser.LastActivityDate = DateTime.MinValue;
            newUser.LastLockOutUTC = DateTime.MinValue;
            newUser.IsApproved = true;
            newUser.IsLockedOut = false;
            newUser.IsAdministrator = false;
            newUser.CreatedOnUTC = DateTime.UtcNow;
            //inser new or migrate SSO user
            return BLLSec_UserMembership.Insert(newUser);
        }


        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_CompanyProfileWzCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        public static String GetResourceString(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_CompanyProfileWzCtrl", resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion


        protected void bttSaveCompanyProfile_Click(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) return;
            if (Page.IsValid)
            {
                SaveCompanyBasicInfo();
            }
        }


        protected void bttContinue1_Click(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsValid) return;
            bttContinue1.Visible = false;
            pnlAccount.Visible = txtCompanyName.ReadOnly = txtTeamOwnerEmail.ReadOnly = lnkEditEmail.Visible = true;
            //execute Team Owner profile address login
            ProcessTeamOwnerAddress();
        }

        private void ProcessTeamOwnerAddress()
        {
            var email = txtTeamOwnerEmail.Text.Trim();
            if (string.IsNullOrEmpty(email)) throw new ApplicationException("Team Owner Email address should not be empty");
            //get user from My Anritsu 
            var myAnritsuUser = BLLSec_UserMembership.SelectByEmailAddressOrUserID(email.Trim());
            //get Complete user details along with Company Name
            var ssoUserData = BLLSSOUser.SelectByEmailOrUserName(email);
            SetCompanyName(ssoUserData != null ? Convert.ToString(ssoUserData["CompanyName"]) : null);
            if (myAnritsuUser != null)
            {
                IsMyAnritsuUser = true;
                //Extract the address object
                var address = ExtractProfileAddress(myAnritsuUser);
                //set owner
                addrCompanyAddress.OwnerMemberShipId = myAnritsuUser.MembershipId;
                //check user has address fields
                if (!address.IsNullOrEmpty())
                {
                    //-- load the address section in disabled mode admin cannot edit address details
                    addrCompanyAddress.SetAddress(address);
                }
                else //user address fields are empty
                {
                    //Get SSO user data
                    if (ssoUserData == null) return;
                    //udpate the adress from SSO and set latest address
                    addrCompanyAddress.SetAddress(addrCompanyAddress.UpdateUserAddress(ssoUserData, myAnritsuUser));
                }
            }
            else
            {
                //Get SSO user data
                //var ssoUserData = BLLSSOUser.SelectByEmailOrUserName(email);
                if(ssoUserData==null) return;
                IsSSOUser = true;
                addrCompanyAddress.SetAddress(IsSSOUser ? ExtractProfileAddress(ssoUserData) : null);
                //SetCompanyName(Convert.ToString(ssoUserData["CompanyName"]));
            }
        }

        private void SetCompanyName(string companyName)
        {
            txtCompanyName.Text = string.IsNullOrEmpty(companyName) ? string.Empty : companyName;
            txtCompanyName.ReadOnly = !string.IsNullOrEmpty(companyName);
        }

        private static Profile_Address ExtractProfileAddress(Object myAnritsuUser)
        {
            if (myAnritsuUser == null) return null;
            if (myAnritsuUser is Sec_UserMembership)
            {
                var castUser = ((Sec_UserMembership)myAnritsuUser);
                return new Profile_Address()
                {
                    Address1 = castUser.UserStreetAddress1,
                    Address2 = castUser.UserStreetAddress2,
                    CityTown = castUser.UserCity,
                    CountryCode = castUser.UserAddressCountryCode,
                    FaxNumber = castUser.FaxNumber,
                    PhoneNumber = castUser.PhoneNumber,
                    ZipPostalCode = castUser.UserZipCode,
                    StateProvince = castUser.UserState
                };
            }
            if (!(myAnritsuUser is DataRow)) return null;
            var userRow = ((DataRow) myAnritsuUser);
            return new Profile_Address()
            {
                Address1 = ConvertUtility.ConvertToString(userRow["StreetAddress1"], String.Empty),
                Address2 = ConvertUtility.ConvertToString(userRow["StreetAddress2"], String.Empty),
                CityTown = ConvertUtility.ConvertToString(userRow["City"], String.Empty),
                CountryCode = ConvertUtility.ConvertToString(userRow["CountryCode"], String.Empty),
                FaxNumber = ConvertUtility.ConvertToString(userRow["FaxNumber"], String.Empty),
                PhoneNumber = ConvertUtility.ConvertToString(userRow["PhoneNumber"], String.Empty),
                ZipPostalCode = ConvertUtility.ConvertToString(userRow["ZipCode"], String.Empty),
                StateProvince = ConvertUtility.ConvertToString(userRow["State"], String.Empty)
            };
        }

        protected void bttContinue2_Click(object sender, EventArgs e)
        {
            bttContinue1.Visible = false;
            pnlAccount.Visible = txtCompanyName.ReadOnly = txtTeamOwnerEmail.ReadOnly = true;
            //execute Team Owner profile address login
            ProcessTeamOwnerAddress();
        }

        protected void lnkEditCompanyNameAndEmail_Click(object sender, EventArgs e)
        {
            bttContinue1.Visible = true;
            lnkEditEmail.Visible =
                txtCompanyName.ReadOnly = txtTeamOwnerEmail.ReadOnly = pnlAccount.Visible = false;
        }
    }
}