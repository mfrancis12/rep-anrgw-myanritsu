﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgAdmin_DistPortalCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_Org.OrgAdmin_DistPortalCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainerUser" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,pnlContainerUser.HeaderText %>">
<div class="settingrow">
    <fieldset id="fsEnableDistPortal" runat="server">
        <legend><asp:Localize ID="lcalEnableDistPortal" runat="server" Text="<%$ Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalEnableDistPortal.Text %>"></asp:Localize></legend>
        <p><asp:Localize ID="lcalEnableDistPortalDesc" runat="server" Text="<%$ Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalEnableDistPortalDesc.Text %>"></asp:Localize></p>
        <div class="margin-top-15">
            <asp:Button ID="bttEnableDistPortal" runat="server" Text="<%$ Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,bttEnableDistPortal.Text %>" CausesValidation="false" 
                OnClick="bttEnableDistPortal_Click"/>
        </div>
    </fieldset>
</div>
<asp:Panel ID="pnlDistPortalDetials" runat="server">
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalSIS_AccessType_Public" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalSIS_AccessType_Public.Text %>" />:</span>
    <asp:CheckBox ID="cbxSIS_AccessType_Public" runat="server" Text=" " />
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalSIS_AccessType_Private" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalSIS_AccessType_Private.Text %>" />:</span>
    <asp:CheckBox ID="cbxSIS_AccessType_Private" runat="server" Text=" " />
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalDistStatus" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalDistStatus.Text %>" />:</span>
    <asp:RadioButtonList ID="rdoDistStatus" runat="server" RepeatDirection="Horizontal" DataSourceID="odsLookupDistPortalStatus" DataTextField="DisplayText" DataValueField="DistPortalStatusCode" Width="30%"></asp:RadioButtonList>
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalProdFilter" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalProdFilter.Text %>" />:</span>
    <table style="border: none; width: 60%">
            <tbody>
                <tr>
                    <td><dx:ASPxComboBox ID="dxcmbProdFilters" runat="server" TextField="FilterName" ValueField="ProdFilterID" DataSourceID="odsLookupDistPortalProdFilters" Width="350px"></dx:ASPxComboBox></td>
                    <td><asp:LinkButton ID="lbttSisProdFilterEdit" runat="server" Text="edit" Visible="false"></asp:LinkButton></td>
                    <td><asp:HyperLink ID="hlSisProdFilterManage" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_prodfilterlist" Text="manage" Font-Bold="false" SkinID="blueit"></asp:HyperLink></td>
                    <td><asp:HyperLink ID="hlSisProdFilterAddNew" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_prodfilterdetails" Text="add new" Font-Bold="false" SkinID="blueit"></asp:HyperLink></td>
                </tr>
            </tbody>
        </table>    
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalDocTypeFilter" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalDocTypeFilter.Text %>" />:</span>
    <table style="border: none; width: 60%">
        <tbody>
            <tr>
                <td><dx:ASPxComboBox ID="dxcmbDocTypeFilters" runat="server" TextField="DocTypeFilterName" ValueField="DocTypeFilterID" DataSourceID="odsLookupDistPortalDocTypeFilters" Width="350px"></dx:ASPxComboBox></td>
                <td><asp:LinkButton ID="lbttSisDocTypeFilterEdit" runat="server" Text="edit" Visible="false"></asp:LinkButton></td>
                <td><asp:HyperLink ID="hlSisDocTypeFilterManage" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_doctypefilterlist" Text="manage" Font-Bold="false" SkinID="blueit"></asp:HyperLink></td>
                <td><asp:HyperLink ID="hlSisDocTypeFilterAddNew" runat="server" NavigateUrl="/siteadmin/distportaladmin/sis_doctypefilterdetails" Text="add new" Font-Bold="false" SkinID="blueit"></asp:HyperLink></td>
            </tr>
        </tbody>
    </table>    
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalDistModifiedBy" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalDistModifiedBy.Text %>" />:</span>
    <asp:Literal ID="ltrDistModifiedBy" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal ID="ltrDistModifiedOn" runat="server"></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalDistCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,lcalDistCreatedBy.Text %>" />:</span>
    <asp:Literal ID="ltrDistCreatedBy" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal ID="ltrDistCreatedOn" runat="server"></asp:Literal>
</div>
<div class="settingrow margin-top-15">
    <asp:Button ID="bttUpdateDistPortal" runat="server" Text="<%$ Resources:ADM_STCTRL_OrgAdmin_DistPortalCtrl,bttUpdateDistPortal.Text %>" CausesValidation="false" OnClick="bttUpdateDistPortal_Click"/>
</div>
</asp:Panel>
<div class="settingrow"><p class="msg"><asp:Literal ID="ltrDistPortalMsg" runat="server"></asp:Literal></p></div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_OrgAdmin_DistPortalCtrl" EnableViewState="false" /></div>
</anrui:GlobalWebBoxedPanel>
<asp:ObjectDataSource ID="odsLookupDistPortalStatus" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_Org.ODS_OrgLookups" 
    EnablePaging="false" EnableCaching="true" 
    CacheExpirationPolicy="Sliding" CacheDuration="160" CacheKeyDependency="odsLookupDistPortalStatusCKD"
    SelectMethod="AccountConfig_DistPortal_Status_SelectAll">
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsLookupDistPortalProdFilters" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal.ODS_SISProdFilterLookup" 
    EnablePaging="false"
    SelectMethod="SelectProductFiltersWithNoFilterItem">
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsLookupDistPortalDocTypeFilters" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal.ODS_SISDocTypeFilterLookup" 
    EnablePaging="false"
    SelectMethod="SelectWithNoFilterItem">
</asp:ObjectDataSource>