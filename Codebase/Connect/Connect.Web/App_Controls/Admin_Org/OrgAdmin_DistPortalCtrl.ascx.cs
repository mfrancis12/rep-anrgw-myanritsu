﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public partial class OrgAdmin_DistPortalCtrl : BC_AdminOrg, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dxcmbProdFilters.DataBind();
                dxcmbDocTypeFilters.DataBind();
                LoadDistributorInfo();
            }
        }

        private void LoadDistributorInfo()
        {
            if (AccountID.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found.");
            Acc_AccountMaster acc = AccountUtility.Admin_AccountInfoGet(AccountID);
            if (acc == null) throw new HttpException(404, "Page not found.");

            dxcmbProdFilters.Items.FindByValue("0").Selected = true;

            AccountConfig_DistPortal config = AccountConfig_DistPortalBLL.Select(AccountID);
            if (config == null) //new
            {
                fsEnableDistPortal.Visible = true;
                pnlDistPortalDetials.Visible = false;
                return;
            }

            fsEnableDistPortal.Visible = false;
            pnlDistPortalDetials.Visible = true;
            cbxSIS_AccessType_Public.Checked = config.SIS_AccessType_Public;
            cbxSIS_AccessType_Private.Checked = config.SIS_AccessType_Private;
            //if (config.SIS_ProdFilter > 0)
                dxcmbProdFilters.Items.FindByValue(config.SIS_ProdFilter.ToString()).Selected = true;
            //if (config.SIS_DocTypeFilter > 0)
                dxcmbDocTypeFilters.Items.FindByValue(config.SIS_DocTypeFilter.ToString()).Selected = true;

            rdoDistStatus.DataBind();
            ListItem statusItem = rdoDistStatus.Items.FindByValue(config.StatusCode);

            ltrDistCreatedBy.Text = config.CreatedBy;
            ltrDistCreatedOn.Text = String.Format("( {0} )", config.CreatedOnUTC.ToString("d"));

            if (config.ModifiedBy.IsNullOrEmptyString())
            {
                ltrDistModifiedBy.Text = "n/a";
                ltrDistModifiedOn.Text = "";
            }
            else
            {
                ltrDistModifiedBy.Text = config.ModifiedBy;
                ltrDistModifiedOn.Text = String.Format("( {0} )", config.ModifiedOnUTC.ToString("d"));
            }
            statusItem.Selected = true;
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_OrgAdmin_DistPortalCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void bttEnableDistPortal_Click(object sender, EventArgs e)
        {

            if (AccountID.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found.");
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
            Acc_AccountMaster acc = AccountUtility.Admin_AccountInfoGet(AccountID);
            if (acc == null) throw new HttpException(404, "Page not found.");
            AccountConfig_DistPortal config = AccountConfig_DistPortalBLL.Select(AccountID);
            if (config != null) Response.Redirect(Request.RawUrl);


            AccountConfig_DistPortalBLL.Insert(acc.AccountID, usr.Email, 0, 0);
            LoadDistributorInfo();
        }

        protected void bttUpdateDistPortal_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
            try
            {
                ltrDistPortalMsg.Text = "";
                String statusCode = rdoDistStatus.SelectedValue;
                Boolean sisPublic = cbxSIS_AccessType_Public.Checked;
                Boolean sisPrivate = cbxSIS_AccessType_Private.Checked;
                if (sisPublic == false && sisPrivate == false)
                {
                    ltrDistPortalMsg.Text = "At least one of the SIS Access Type should be selected!";
                    return;
                }
                Int32 sisProdFilter = ConvertUtility.ConvertToInt32(dxcmbProdFilters.SelectedItem.Value, 0);
                Int32 sisDocTypeFilter = ConvertUtility.ConvertToInt32(dxcmbDocTypeFilters.SelectedItem.Value, 0);
                AccountConfig_DistPortalBLL.Update(AccountID, statusCode, sisPublic, sisPrivate, sisProdFilter, sisDocTypeFilter, usr.Email);
                ltrDistPortalMsg.Text = "Distributor Portal updated successfully.";
                LoadDistributorInfo();
            }
            catch (Exception ex)
            {
                ltrDistPortalMsg.Text = "ERROR: " + ex.Message;
            }
        }
    }
}