﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgAdmin_TAUPackageCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_Org.OrgAdmin_TAUPackageCtrl" %>


<asp:PlaceHolder runat="server">
    <link type="text/css" href="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/css/token-input.css" rel="stylesheet" />
    <script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/jquery.tokeninput.js"></script>
    <script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/suggest_packages.js"></script>
</asp:PlaceHolder>
<style type="text/css">
    .dxpc-contentWrapper table td, td.dx {
        border: none;
        padding: 3px;
    }

    td.dxic, td.dxic + td {
        border: none;
        padding: 0;
    }
</style>
<script language="javascript">
    function ConfirmOnDelete(deleteMessage) {
        if (confirm(deleteMessage) == true)
            return true;
        else
            return false;
    }
</script>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow" style="text-align: right">
        <asp:HyperLink ID="hlAddNewForm" runat="server" Text="<%$ Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,hlAddNewPackage.Text %>"
            NavigateUrl="~/siteAdmin/packageAdmin/packagedetails" SkinID="blueit"></asp:HyperLink>
    </div>
    <div class="settingrow group input-text width-60">
        <label>
            <asp:Localize ID="lcalCompName" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,lcalAssignPackage.Text%>"></asp:Localize></label>

        <div id="model">
            <asp:HiddenField ID="hfPackageID" runat="server" />
            <asp:TextBox ID="txtPackage" runat="server"></asp:TextBox>
            <span class="required-field-validator" id="token-validator">Required</span>
        </div>
    </div>
    <div class="settingrow group input-text width-60 overflow">
        <label>
            <asp:Localize ID="lcalPackageExpDate" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,lcalPackageExpDate.Text%>"></asp:Localize></label>
        <dx:ASPxDateEdit ID="expDate" runat="server"></dx:ASPxDateEdit>
    </div>
    <div class="margin-top-15">
        <asp:Button ID="bttAddPackage" runat="server" ValidationGroup="vgPackageInfo" OnClientClick="return validateTokenizer('token-validator')" Text="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,bttAddPackage.Text%>"
            OnClick="bttAddPackage_Click" SkinID="submit-btn" />
    </div>
</anrui:GlobalWebBoxedPanel>

<anrui:GlobalWebBoxedPanel ID="pnlAssignedPackages" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,pnlAssignedPackages.HeaderText %>'>
   <asp:GridView ID="gvAssignedPackages" runat="server" DataKeyNames="PackageID" EnableViewState="true" ViewStateMode="Enabled" AutoGenerateColumns="false" OnRowDataBound="gvAssignedPackages_RowDataBound"
                OnRowCommand="gvAssignedPackages_RowCommand" Width="100%" >
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources: ADM_STCTRL_OrgAdmin_TAUPackageCtrl, lblPackageName.HeaderText%>'>
                        <ItemTemplate>
                           <a id="lnkPackageName" class="blueit" target="_parent" href="/siteadmin/packageadmin/packagedetails?pid=<%# Eval("PackageID")%>"><%# Eval("PackageName")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="SystemName" HeaderText='System Name' />
                    <asp:TemplateField HeaderText="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,lcalPackageExpDate.Text%>">
                                <ItemTemplate>
                                    <dx:ASPxDateEdit ID="expDate"  runat="server" ></dx:ASPxDateEdit>
                                    <asp:HiddenField ID="hfDate" runat="server" Value='<%# Convert.ToString(Eval("ExpiryDate")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remove">
                        <ItemTemplate>
                             <asp:Button ID="bttRemovePackage" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,bttRemoveFromOrg.Text %>" OnClientClick='<%#@"return ConfirmOnRemovePackage("""+ GetStaticResource("ConfirmOnRemovePackage.Text")+@""")" %>' 
                                    CausesValidation="false" SkinID="small-button-lpa" CommandName="RemoveFromOrgCommand"
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PackageID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <EmptyDataTemplate>
                    <asp:Localize ID="lcalEmptyPackages" runat="server" Text="<%$Resources:ADM_STCTRL_OrgAdmin_TAUPackageCtrl,lcalEmptyPackages.EmptyDataText%>"></asp:Localize>
                </EmptyDataTemplate>
            </asp:GridView>
       <br />
       <div class="settingrow">
           <p>
            <span class="msg-red-small">
                <asp:Literal ID="ltrTUAPackageExpDateUpdateMsg" runat="server"></asp:Literal>
            </span>
               </p>
        <asp:Button ID="btnSave" runat="server" ValidationGroup="vgPackageInfo"  Text="Save"
            OnClick="btnSave_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>

