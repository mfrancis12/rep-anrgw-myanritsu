﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.ActivityLog;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public partial class OrgAdmin_TAUPackageCtrl : BC_AdminOrg
    {
        private Acc_AccountMaster _AccountInfo;

          private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_OrgAdmin_TAUPackageCtrl"; }
        }

        public Acc_AccountMaster AccountInfo
        {
            get
            {
                if (AccountID.IsNullOrEmptyGuid()) return null;
                if (_AccountInfo == null)
                {
                    _AccountInfo = AccountUtility.Admin_AccountInfoGet(AccountID);
                }
                return _AccountInfo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPackages();
            }
        }

        private void InitPackages()
        {

            hlAddNewForm.NavigateUrl = String.Format("~/siteAdmin/packageAdmin/packagedetails?{0}={1}"
                  , KeyDef.QSKeys.ReturnURL
                  , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}"
                      , KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsTAUPackagePermissions
                      , KeyDef.QSKeys.AccountID
                      , AccountID.ToString())));
            expDate.MinDate = DateTime.UtcNow.Date;
            BindAssignedPackages();
        }

        private void BindAssignedPackages()
        {
            DataTable dtPackages = Lib.Package.PackageBLL.SelectAllActivePackagesByAccountID(AccountID);
            if (dtPackages != null && dtPackages.Rows.Count > 0)
            {
                gvAssignedPackages.DataSource = dtPackages;
                gvAssignedPackages.DataBind();
            }
            else
                btnSave.Visible = false;
        }

        protected void bttAddPackage_Click(object sender, EventArgs e)
        {
            if (AccountID == Guid.Empty || String.IsNullOrEmpty(hfPackageID.Value)) return;
            else
            {
                //update the admin activity log
                LogActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADDACC_ATTEMPT, getCurrentUserEmail, AccountInfo.CompanyName + "->" + AccountInfo.AccountName, txtPackage.Text.Trim());
                //Add Package to Org/Account
                bool result = Lib.Package.PackageBLL.InsertPackageAssignedOrg(Convert.ToInt32(hfPackageID.Value), AccountID, LoginUtility.GetCurrentUserOrSignIn().Email, expDate.Date);
                if (result)
                {
                    //update the admin activity log
                    LogActivity("Package", TasksPerformed.Add, ActivityLogNotes.PACKAGE_ADDACC_SUCCESS, getCurrentUserEmail, AccountInfo.CompanyName + "->" + AccountInfo.AccountName, txtPackage.Text.Trim());
                    WebUtility.HttpRedirect(null, Request.RawUrl);
                }
            }
        }

        protected void gvAssignedPackages_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (DataControlRowType.DataRow == e.Row.RowType)
            {
                ASPxDateEdit expDate = e.Row.FindControl("expDate") as ASPxDateEdit;
                expDate.MinDate = DateTime.UtcNow.Date;
                DataRowView dv = (DataRowView)e.Row.DataItem;

                String expDateStr = Convert.ToString(dv["ExpiryDate"]);
                if (!String.IsNullOrEmpty(expDateStr))
                {
                    DateTime dtexpDate = DateTime.Now;
                    if (DateTime.TryParse(expDateStr, out dtexpDate))
                        expDate.Date = dtexpDate.Date;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //get the Package Expiry Date Table
            DataTable dtExpiryDates = GetExpiryDatesTable(gvAssignedPackages);

            bool resut = Lib.Package.PackageBLL.BulkUpdateOrgPackages(AccountID, dtExpiryDates);
            ltrTUAPackageExpDateUpdateMsg.Text = GetStaticResource("UpdateMsg");
        }

        protected void gvAssignedPackages_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RemoveFromOrgCommand")
            {
                GridViewRow row = (GridViewRow)(((System.Web.UI.WebControls.Button)e.CommandSource).NamingContainer);

                System.Web.UI.WebControls.Label lblPackageName = (System.Web.UI.WebControls.Label)row.FindControl("lblPackageName");
                String packageName = String.Empty;
                if (lblPackageName != null)
                    packageName = lblPackageName.Text.Trim();

                //update the admin activity log
                LogActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETEACC_ATTEMPT, getCurrentUserEmail, AccountInfo.CompanyName + "->" + AccountInfo.AccountName, packageName);
                //remove the package from the Org
                if (Lib.Package.PackageBLL.DeleteOrganizationPackageMapping(Convert.ToInt64(e.CommandArgument), AccountID))
                {
                    //update the admin activity log
                    LogActivity("Package", TasksPerformed.Delete, ActivityLogNotes.PACKAGE_DELETEACC_SUCCESS, getCurrentUserEmail, AccountInfo.CompanyName + "->" + AccountInfo.AccountName, packageName);
                    WebUtility.HttpRedirect(null, Request.RawUrl);
                }
            }
        }

        private DataTable GetExpiryDatesTable(GridView gvPackages)
        {
            if (gvPackages == null || gvPackages.Rows == null || gvPackages.Rows.Count == 0) return null;

            DataTable dtExpDates = new DataTable("Package_Org");
            dtExpDates.Columns.Add("PackageID");
            dtExpDates.Columns.Add("ExpiryDate");
            var currentCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            foreach (GridViewRow grRow in gvPackages.Rows)
            {
                DataRow drRow = dtExpDates.NewRow();
                drRow["PackageID"] = Convert.ToInt64(gvPackages.DataKeys[grRow.RowIndex].Value);
                ASPxDateEdit expDate = grRow.FindControl("expDate") as ASPxDateEdit;
                if (expDate.Date != DateTime.MinValue)
                    drRow["ExpiryDate"] = expDate.Date.ToShortDateString(); //expDate.Date.ToUniversalTime().Date;// TimeZone.CurrentTimeZone.ToUniversalTime(expDate.Date);
                else
                    drRow["ExpiryDate"] = DBNull.Value;
                dtExpDates.Rows.Add(drRow);
            }
            Thread.CurrentThread.CurrentCulture = currentCulture;
            return dtExpDates;
        }
        
        public string GetStaticResource(string resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        private void LogActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(getCurrentUserEmail, "Package", taskPerformed, action, WebUtility.GetUserIP(),ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
        }
    }
}