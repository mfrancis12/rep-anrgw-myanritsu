﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Cfg;
using DevExpress.Web;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public partial class OrgAdmin_TabsCtrl : BC_AdminOrg, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            InitTabs();
            base.OnInit(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                //InitTabs();
            }
        }

        public void InitTabs()
        {
            Acc_AccountMaster acc = AccountUtility.Admin_AccountInfoGet(AccountID);
            if (acc == null) throw new HttpException(404, "Page not found.");
            String query = String.Format("?{0}={1}", KeyDef.QSKeys.AccountID, AccountID.ToString());
            Tab tab;
            int tabIndex = 0;

            tab = new Tab(GetStaticResource("tbOrgDetails.Text"), "tbOrgDetails");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetails + query;
            tab.Enabled = true;
            dxOrgAdminTabs.Tabs.Add(tab);

            tab = new Tab(GetStaticResource("tbOrgDetailsUsers.Text"), "tbOrgDetailsUsers");
            tab.Enabled = true;
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsUsers + query;
            dxOrgAdminTabs.Tabs.Add(tab);

            tab = new Tab(GetStaticResource("tbOrgDetailsProdRegs.Text"), "tbOrgDetailsProdRegs");
            tab.Enabled = true;
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsProdRegs + query;
            dxOrgAdminTabs.Tabs.Add(tab);
            if (Context.User.IsInRole(KeyDef.UserRoles.Admin_SIS_EMEA) || Context.User.IsInRole(KeyDef.UserRoles.Admin_SIS_US) || Context.User.IsInRole(KeyDef.UserRoles.Admin_SiteAdmin))
            {
                tab = new Tab(GetStaticResource("tbOrgDetailsDistPortal.Text"), "tbOrgDetailsDistPortal");
                tab.VisibleIndex = tabIndex++;
                tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_DistPortalPages.OrgDetailsDistributorPortal + query;
                tab.Enabled = true;
                dxOrgAdminTabs.Tabs.Add(tab);
            }

            if (Context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU) || Context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU) || Context.User.IsInRole(KeyDef.UserRoles.Admin_SiteAdmin))
            {
                tab = new Tab(GetStaticResource("tbOrgDetailsTAUPackagePermissions.Text"), "tbOrgDetailsTAUPackagePermissions");
                tab.VisibleIndex = tabIndex++;
                tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsTAUPackagePermissions + query;
                tab.Enabled = true;
                dxOrgAdminTabs.Tabs.Add(tab);
            }
            bool showDeletedProdReg = ProdReg_MasterBLL.HasDeletedProductRegistrations(AccountID);
            if (showDeletedProdReg)
            {
                tab = new Tab(GetStaticResource("tbOrgDetailsDeletedProdRegs.Text"), "tbOrgDetailsDeletedProdRegs")
                {
                    Enabled = true,
                    VisibleIndex = tabIndex++,
                    NavigateUrl = KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsDeletedProdRegs + query
                };
                dxOrgAdminTabs.Tabs.Add(tab);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_OrgAdmin_TabsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}