﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgAdmin_UsersCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_Org.OrgAdmin_UsersCtrl" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v14.2, Version=14.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainerUser" runat="server" ShowHeader="true"
    HeaderText="<%$ Resources:ADM_STCTRL_SiteAdminUsers,pnlContainerUser.HeaderText %>">
    <div class="settingrow">

        <dx:ASPxGridView ID="gvOrgDetails" ClientInstanceName="gvOrgDetails" runat="server" Theme="AnritsuDevXTheme"
            OnBeforePerformDataSelect="gvOrgDetails_OnBeforePerformDataSelect" OnRowDeleting="gvOrgDetails_OnRowDeleting"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="MembershipId" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>

                <dx:GridViewDataColumn Width="10" VisibleIndex="1" >
                    <DataItemTemplate>
                        
                        <dx:ASPxButton ID="btnDelete" runat="server" SkinID="small-button-lpa" Text="Delete" class="button"  CommandArgument='<%#Eval("MembershipId") %>' CommandName="Delete" OnCommand="btnDelete_Command" Width="20px" />
                        
                    </DataItemTemplate>


                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Width="150" VisibleIndex="2">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="ASPxHyperLink1" ToolTip="user details" runat="server" Text='<%#Eval("EmailAddress") %>' NavigateUrl='<%#"~/siteadmin/orguseradmin/connectuserdetail?memid="+Eval("MembershipId") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FullName" Caption="Name" Width="150" VisibleIndex="3" />
                <dx:GridViewDataColumn FieldName="LastActivityUTC" Caption="Last Activity (UTC)" Width="50" VisibleIndex="4" />
                <dx:GridViewDataColumn FieldName="CreatedOnUTC" Caption="First Access" Width="140" VisibleIndex="5" />
                <dx:GridViewDataColumn Caption="Can Manage User" Width="70" VisibleIndex="6">
                    <DataItemTemplate>
                        <dx:ASPxCheckBox runat="server" Checked='<%#CanManageUser(Eval("Roles")) %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="IsAdministrator" Caption="Is Super Admin" Width="70" VisibleIndex="7" />

                <dx:GridViewDataColumn FieldName="MembershipId" Visible="false" />
                <dx:GridViewDataColumn FieldName="EmailAddress" Visible="false" />
            </Columns>

            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>
            
        </dx:ASPxGridView>

    </div>
    
    <div class="settingrow group input-text width-60 required">
        <label>
            <asp:Localize ID="lcalAddUser" runat="server" Text="<%$ Resources:ADM_STCTRL_SiteAdminUsers,lcalAddUser.Text %>"></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="lclEmlError" runat="server" Text="<%$ Resources:ADM_STCTRL_SiteAdminUsers,rfvNewUserEmail.ErrorMessage %>"></asp:Localize>
        </p>
        <p class="custom-error-message" style="margin: 1em 0">
            <asp:Localize ID="lclEmailErrMsg" runat="server" Text="<%$ Resources:ADM_STCTRL_SiteAdminUsers,revNewUserEmail.ErrorMessage %>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtNewUserEmail" runat="server" ValidationGroup="vgNewUser" CssClass="email" ></asp:TextBox>

    </div>
    <div class="group margin-top-15">
        <asp:Button ID="bttNewUserAdd" runat="server" Text="<%$ Resources:ADM_STCTRL_SiteAdminUsers,bttNewUserAdd.Text %>"
            ValidationGroup="vgNewUser" SkinID="submit-btn" OnClick="bttNewUserAdd_Click" />
    </div>
    
    <%--<div class="settingrow">
        <span class="settinglabelrgt-15">&nbsp;</span>
        <asp:RequiredFieldValidator ID="rfvNewUserEmail" runat="server" ValidationGroup="vgNewUser"
            ControlToValidate="txtNewUserEmail" ErrorMessage='<%$ Resources:ADM_STCTRL_SiteAdminUsers,rfvNewUserEmail.ErrorMessage %>'></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="revNewUserEmail" runat="server" ValidationGroup="vgNewUser"
            ControlToValidate="txtNewUserEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ErrorMessage='<%$ Resources:ADM_STCTRL_SiteAdminUsers,revNewUserEmail.ErrorMessage %>'></asp:RegularExpressionValidator>
    </div>--%>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_SiteAdminUsers"
            EnableViewState="false" />
    </div>
</anrui:GlobalWebBoxedPanel>
