﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;
using Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_Org
{
    public partial class OrgAdmin_UsersCtrl : BC_AdminOrg, App_Lib.UI.IStaticLocalizedCtrl
    {
        private DataTable _UserMembers;
        public DataTable UserMembers
        {
            get
            {
                if (_UserMembers == null)
                {
                    _UserMembers = Lib.BLL.BLLAcc_AccountMaster.UserMembership_SelectByAccountID_ForAdmin(AccountID);
                }
                return _UserMembers;
            }
            set
            {
                _UserMembers = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (AccountID.IsNullOrEmptyGuid())
            {
                this.Visible = false;
                return;
            }
          
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Form.DefaultButton = bttNewUserAdd.UniqueID;
            if (!IsPostBack)
            {
                gvOrgDetails.DataBind();
            }
        }

        protected void bttNewUserAdd_Click(object sender, EventArgs e)
        {
            Page.Validate("vgNewUser");
            if (!Page.IsValid) return;
            String email = txtNewUserEmail.Text.Trim();
            Guid accountID = AccountID;
            if (email.IsNullOrEmptyString() || accountID.IsNullOrEmptyGuid()) return;
            Lib.Security.Sec_UserMembership actingUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.Security.Sec_UserMembership user = Lib.BLL.BLLSec_UserMembership.AddNewUserPendingLogin(accountID
                , email
                , "n/a"
                , "n/a"
                , false
                , WebUtility.GetUserIP()
                , actingUser.Email
                , actingUser.FullName);

            Lib.Security.Sec_UserMembership usr = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
            var addnewuser = new ConnectUserAddNew();
            addnewuser.SendEmails(usr, accountID);

            UserMembers = null;//refresh

            gvOrgDetails.DataBind();
        }
        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }


        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        protected void gvOrgDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvOrgDetails.DataSource = UserMembers;

        }

        protected void gvOrgDetails_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Guid membershipId = ConvertUtility.ConvertToGuid(e.Keys["MembershipId"], Guid.Empty);
            Lib.BLL.BLLSec_UserMembership.DeleteFromAccount(membershipId, AccountID);
            _UserMembers = null;
            gvOrgDetails.DataBind();
        }

        protected bool CanManageUser(object roles)
        {
            var userRoles = Convert.ToString(roles);
            if (string.IsNullOrEmpty(userRoles)) return false;
            return userRoles.Split(',').Contains(KeyDef.UserRoles.User_OrgManager);
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Guid membershipId = ConvertUtility.ConvertToGuid(e.CommandArgument, Guid.Empty);
                Lib.BLL.BLLSec_UserMembership.DeleteFromAccount(membershipId, AccountID);
                _UserMembers = null;
                gvOrgDetails.DataBind();
            }
        }

       
    }
}