﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClearCache_ModelConfigCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ClearCache_ModelConfigCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='Clear Cache for Model Configurations'>
    <div class="settingrow" style="text-align: center;">
        <asp:Button ID="bttClearModelConfigCache" runat="server" Text="Clear Model Config Cache" OnClick="bttClearModelConfigCache_Click" />
    </div>
    <div class="settingrow" style="text-align: center;">
        <span class="msg"><asp:Literal ID="ltrClearModelConfigCacheMsg" runat="server"></asp:Literal></span>
    </div>
</anrui:GlobalWebBoxedPanel>