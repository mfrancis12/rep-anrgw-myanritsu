﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AppCacheFactories;
namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class ClearCache_ModelConfigCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bttClearModelConfigCache_Click(object sender, EventArgs e)
        {
            ClearCache();
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        public void ClearCache()
        {
            ltrClearModelConfigCacheMsg.Text = "";
            ModelConfig_CacheFactory dataCache_ModelConfig = new ModelConfig_CacheFactory();
            dataCache_ModelConfig.RemoveAll();

            Downloads_GlobalWeb_CacheFactory dataCache_GW = new Downloads_GlobalWeb_CacheFactory();
            dataCache_GW.RemoveAll();

            Downloads_JPDL_CacheFactory dataCache_JPDL = new Downloads_JPDL_CacheFactory();
            dataCache_JPDL.RemoveAll();

            ltrClearModelConfigCacheMsg.Text = "Cache cleared successfully.";
        }
    }
}