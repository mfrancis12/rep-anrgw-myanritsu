﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MasterModelDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.MasterModelDetailCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,pnlContainer.HeaderText %>'>
    <div id="divDuplicateConfigMsg" runat="server" class="settingrow" style="text-align: center;" visible="false">
        <span style="color:#f5a623;"><asp:Localize ID="lcalDuplicateConfigMsg" runat="server" Text='Please contact Global Web to make changes in the Master Model Configuration Section.'></asp:Localize></span>
    </div>
    <div id="divMasterModelCnfg" runat="server" class="settingrow">
        <fieldset id="fsConfigureMasterModel">
            <legend>
                <asp:Localize ID="lcalConfigureMasterModel" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalConfigureMasterModel.Text %>'></asp:Localize></legend>
            <div class="settingrow width-60 left">
                <div class="settingrow group">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalAllowProdReg" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalAllowProdReg.Text %>'></asp:Localize></span>
                    <asp:CheckBox ID="cbxAllowProdReg" runat="server" />
                </div>
                <div class="settingrow group">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalShowAgreement" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalShowAgreement.Text %>'></asp:Localize></span>
                    <asp:CheckBox ID="cbxShowAgreement" runat="server" />
                </div>
                 <div class="settingrow group">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalShowWarrantyInfo" runat="server" Text="Show warranty info:"></asp:Localize></span>
                    <asp:CheckBox ID="cbxShowWarrantyInfo" runat="server" />
                </div>
                <div class="settingrow group">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalReg_ValidateSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalReg_ValidateSN %>'></asp:Localize></span>
                    <asp:CheckBox ID="cbxValidateSN" runat="server" />
                </div>
                <div class="settingrow group input-text">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalSNPattern" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalSNPattern.Text %>'></asp:Localize></span>
                    <asp:TextBox ID="txtSNPattern" runat="server"></asp:TextBox>
                </div>

                <div class="settingrow group input-select">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalAddOnForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalAddOnForm.Text %>'></asp:Localize></span>
                    <asp:DropDownList ID="cmbAddOnForm" runat="server" AppendDataBoundItems="true" DataTextField="FormName"
                        DataValueField="FormID">
                        <asp:ListItem Text="- None" Value="none"></asp:ListItem>
                    </asp:DropDownList>
                </div>
               
                <div class="settingrow group">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalAddOnFormPerSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalAddOnFormPerSN.Text %>'></asp:Localize></span>
                    <asp:CheckBox ID="cbxAddOnFormPerSN" runat="server" />
                </div>
                <div class="settingrow group input-select">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalFeedbackForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalFeedbackForm.Text %>'></asp:Localize>:
                    </span>
                    
                    <asp:DropDownList ID="cmbFeedbackForm" runat="server" AppendDataBoundItems="true"
                        DataTextField="FormName" DataValueField="FormID">
                        <asp:ListItem Text="- None" Value="none"></asp:ListItem>
                    </asp:DropDownList>
                </div>
               <%-- <div class="group">
                <asp:HyperLink ID="hlEditFeedbackForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,hlEditForm.Text %>' Visible="false"
                        NavigateUrl="" CssClass="blueit"></asp:HyperLink>
                    </div>--%>
                <div class="settingrow group input-text">
                    <span class="settinglabelrgt-20">
                        <asp:Localize ID="lcalProdImageUrl" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalProdImageUrl.Text %>'></asp:Localize>:</span>
                    <asp:TextBox ID="txtProdImageUrl" runat="server" ></asp:TextBox>
                </div>
                <div class="settingrow">
                    <br />
                    <asp:Button ID="bttUpdateMaster" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,bttUpdate.Text %>'
                        OnClick="bttUpdateMaster_Click" CssClass="button" />

                </div>
                <div class="settingrow" style="text-align: center">
                    <span class="msg">
                        <asp:Literal ID="ltrMasterMsg" runat="server"></asp:Literal>
                    </span>
                </div>
            </div>
            <div class="width-30 right">
                <div class="group">
                <asp:HyperLink ID="hlEditFeedbackForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,hlEditForm.Text %>' Visible="false"
                        NavigateUrl="" SkinID="blueit"></asp:HyperLink>
                    </div>
                 <div class="group">
                <asp:HyperLink ID="hlEditAddOnForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,hlEditForm.Text %>' Visible="false"
                        NavigateUrl="" CssClass="blueit"></asp:HyperLink>
                    </div>
            </div>
        </fieldset>
    </div>
    <div id="divModelTags" runat="server" class="settingrow">
        <fieldset id="fsModelTags">
            <legend>
                <asp:Localize ID="lcalConfigureModelTags" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,pnlContainerModelTags.HeaderText %>'></asp:Localize></legend>
            <div class="settingrow">
            <p>
                <asp:Localize ID="lcalModelTagDesc" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalModelTagDesc.Text %>"></asp:Localize></p>
        
            </div>
            <div class="settingrow">
            <asp:GridView ID="gvModelTags" runat="server" EmptyDataText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,gvModelTags.EmptyDataText %>'
                OnRowCommand="gvModelTags_RowCommand" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                ShowFooter="false" Width="100%">
                <Columns>
                    <asp:BoundField DataField="TagModelNumber" HeaderText="Tag Model Number" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="bttDeleteTag" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ModelTagID") %>'
                                CommandName="DeleteTagCommand" Text="delete" CausesValidation="false" Visible='<%# IsManageMasterInfoAllowed()%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </div>
            <div id="addTAG" runat="server" class="settingrow">
                <div class="group input-text width-60">
                <label class="settinglabelplain">
                    <asp:Localize ID="lcalTagModelNumber" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalNewTagModelNumber.Text %>'></asp:Localize></label>

                <asp:TextBox ID="txtTagModelNumber" runat="server" SkinID="tbx-200" ValidationGroup="vgModelTag"></asp:TextBox>
                    </div>
                <div class="group margin-top-15">
                <asp:Button ID="bttAddTag" runat="server" Text="Add Model Tag" ValidationGroup="vgModelTag"
                    OnClick="bttAddTag_Click" />
                </div>
                <asp:RequiredFieldValidator ID="rfvTagModelNumber" runat="server" ControlToValidate="txtTagModelNumber"
                    ErrorMessage="<%$Resources:Common,ERROR_Required%>" ValidationGroup="vgModelTag"></asp:RequiredFieldValidator>
            </div>
        </fieldset>
    </div>
    <div id="divCustomText" runat="server" class="settingrow">
        <fieldset id="fsCustomText">
            <legend>
                <asp:Localize ID="lcalCustomText" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,pnlContainerCustomText.HeaderText %>'></asp:Localize></legend>
            <div class="settingrow">
            <p>
                <asp:Localize ID="lcalCustomTextDesc" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,lcalCustomTextDesc.Text %>"></asp:Localize><br />
            </p>
                <div class="settingrow">
                <asp:DataGrid ID="dgList" runat="server" ShowFooter="false" ShowHeader="false" AutoGenerateColumns="false"
                    OnItemCommand="dgList_ItemCommand" Width="100%">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Content" ItemStyle-CssClass="dgstyle1-item-lft" ItemStyle-Width="70%">
                            <ItemTemplate>
                               <%# DataBinder.Eval(Container.DataItem, "ContentSnippet") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Locale">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Locale") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-CssClass="dgstyle1-item-lft">
                            <ItemTemplate>
                                <asp:HyperLink CssClass="button" ID="hlEdit" runat="server" NavigateUrl='<%# BuildResEditUrl(DataBinder.Eval(Container.DataItem, "ContentID") ) %>'
                                    Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,hlEdit.Text %>' Visible='<%# IsManageMasterInfoAllowed()%>'></asp:HyperLink>
                                <asp:Button ID="bttDelete" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD,bttDelete.Text %>'
                                    CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ContentID") %>'
                                    Visible='<%# IsManageMasterInfoAllowed() && DataBinder.Eval(Container.DataItem, "Locale").ToString() != "en" %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                </div>
            </div>
        </fieldset>
    </div>
</anrui:GlobalWebBoxedPanel>

<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD" />
