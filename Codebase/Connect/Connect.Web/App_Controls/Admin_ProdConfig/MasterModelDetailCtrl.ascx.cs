﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class MasterModelDetailCtrl : App_Lib.UI.ModelConfigDetail_MasterCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            if (ModelNumber.IsNullOrEmptyString())
            {
                this.Visible = false;
                return;
            }

            base.OnInit(e);
            bttUpdateMaster.OnClientClick = "return confirm ('" + GetGlobalResourceObject("common", "ConfirmUpdateRegMsg") + "')";

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindForms();
                
                if (!String.IsNullOrEmpty(ModelNumber))
                {
                    BindLocalizedStrings();
                    BindModelTags();
                    LoadProductModelConfig();
                    pnlContainer.HeaderText += " - " + ModelNumber;
                }
            }
            ClearMessages();
        }

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            DeleteProductModelConfig();
        }

        protected void bttUpdateMaster_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            UpdateMasterModelConfig();
        }

        private void ClearMessages()
        {
            ltrMasterMsg.Text = String.Empty;
        }

        private void BindForms()
        {
            List<Lib.DynamicForm.Frm_Form> feedbackForms = null;
            List<Lib.DynamicForm.Frm_Form> addOnForms = null;
            Lib.BLL.BLLFrm_Form.SelectAll(out addOnForms, out feedbackForms);
            cmbAddOnForm.DataSource = addOnForms;
            cmbAddOnForm.DataBind();

            cmbFeedbackForm.DataSource = feedbackForms;
            cmbFeedbackForm.DataBind();
        }

        private void BindModelTags()
        {
            gvModelTags.DataSource = Lib.Cfg.ModelConfig_ModelTagBLL.SelectByModelNumber(ModelNumber);
            gvModelTags.DataBind();
        }

        private void LoadProductModelConfig()
        {
            Lib.Cfg.ProductConfig pcfg = PConfig;
            txtProdImageUrl.Text = pcfg.CustomImageUrl;
            cbxValidateSN.Checked = pcfg.Reg_ValidateSN;
            cbxAllowProdReg.Checked = pcfg.Reg_AllowSelfReg;
            txtSNPattern.Text = pcfg.Reg_SNPattern;
            cbxShowAgreement.Checked = pcfg.ShowSupportAgreement;
            cbxShowWarrantyInfo.Checked = pcfg.ShowWarrantyInfo;

            #region " AddOnForm "
            if (pcfg.Reg_AddOnFormID.IsNullOrEmptyGuid())
            {
                cmbAddOnForm.SelectedValue = "none";
                hlEditAddOnForm.Visible = false;
            }
            else
            {
                ListItem addOnForm = cmbAddOnForm.Items.FindByValue(pcfg.Reg_AddOnFormID.ToString());
                if (addOnForm != null)
                {
                    addOnForm.Selected = true;
                    hlEditAddOnForm.NavigateUrl = String.Format("~/siteadmin/formadmin/formdetails?frmid={0}", pcfg.Reg_AddOnFormID.ToString());
                    hlEditAddOnForm.Visible = IsManageMasterInfoAllowed();
                }
            }

            #endregion

            #region " FeedbackForm "
            if (pcfg.Reg_FeedbackFormID.IsNullOrEmptyGuid())
            {
                cmbFeedbackForm.SelectedValue = "none";
                hlEditFeedbackForm.Visible = false;
            }
            else
            {
                ListItem feedbackFormListItem = cmbFeedbackForm.Items.FindByValue(pcfg.Reg_FeedbackFormID.ToString());
                if (feedbackFormListItem != null)
                {
                    feedbackFormListItem.Selected = true;
                    hlEditFeedbackForm.NavigateUrl = String.Format("~/siteadmin/formadmin/formdetails?frmid={0}", pcfg.Reg_FeedbackFormID.ToString());
                    hlEditFeedbackForm.Visible = IsManageMasterInfoAllowed();
                }
            }
            #endregion

            cbxAddOnFormPerSN.Checked = pcfg.Reg_AddOnFormPerSN;
            bool isManageMasterInfoAllowed = IsManageMasterInfoAllowed();
            bttUpdateMaster.Visible = bttAddTag.Visible = addTAG.Visible = pnlContainer.Enabled = isManageMasterInfoAllowed;
            divDuplicateConfigMsg.Visible = !isManageMasterInfoAllowed;

        }

        private void UpdateMasterModelConfig()
        {
            try
            {
                Lib.Cfg.ProductConfig pcfg = PConfig;
                if (pcfg != null)
                {
                    pcfg.Reg_ValidateSN = cbxValidateSN.Checked;
                    pcfg.Reg_SNPattern = txtSNPattern.Text;
                    pcfg.Reg_AllowSelfReg = cbxAllowProdReg.Checked;
                    pcfg.ShowSupportAgreement = cbxShowAgreement.Checked;
                    pcfg.ShowWarrantyInfo = cbxShowWarrantyInfo.Checked;
                    //bttDelete.Visible = true;
                    // bttDelete.Visible = IsManageMasterInfoAllowed();
                    pcfg.Reg_AddOnFormID = Guid.Empty;
                    if (!cmbAddOnForm.SelectedValue.Equals("none", StringComparison.InvariantCultureIgnoreCase))
                        pcfg.Reg_AddOnFormID = ConvertUtility.ConvertToGuid(cmbAddOnForm.SelectedValue, Guid.Empty);
                    pcfg.Reg_AddOnFormPerSN = cbxAddOnFormPerSN.Checked;

                    pcfg.Reg_FeedbackFormID = Guid.Empty;
                    if (!cmbFeedbackForm.SelectedValue.Equals("none", StringComparison.InvariantCultureIgnoreCase))
                        pcfg.Reg_FeedbackFormID = ConvertUtility.ConvertToGuid(cmbFeedbackForm.SelectedValue, Guid.Empty);

                    pcfg.CustomImageUrl = txtProdImageUrl.Text.Trim();

                    if (!cbxValidateSN.Checked && String.IsNullOrEmpty(txtSNPattern.Text))
                    {
                        ltrMasterMsg.Text = GetStaticResource("MSG_SNPatternRequired");
                        return;
                    }
                    else
                    {
                        Lib.Cfg.ProductConfigBLL.InsertUpdate(pcfg);
                        ltrMasterMsg.Text = GetStaticResource("MSG_ModelConfigUpdated"); ;
                    }

                    // bttDelete.Visible = IsManageMasterInfoAllowed();

                }

            }

            catch (Exception ex)
            {
                throw ex;

            }

        }


        private void DeleteProductModelConfig()
        {
            // string mn = ModelNumber.Trim();
            //Lib.Erp.Erp_ProductConfigBLL.DeleteAll(mn);
            //String resKey = string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, mn);
            //Lib.Content.Res_ContentStoreBLL.DeleteByClassKeyResKey(KeyDef.ResClassKeys.ProductCfgClassKey, resKey);
            //ClearCache();

            //WebUtility.HttpRedirect(this, App_Lib.KeyDef.UrlList.ErpProd_ConfigList);
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig_DetailCtrl_USMMD "; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void gvModelTags_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteTagCommand")
            {
                Int32 modelTagID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.Cfg.ModelConfig_ModelTagBLL.Delete(modelTagID);
                BindModelTags();
            }
        }

        protected void bttAddTag_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            if (txtTagModelNumber.Text.Trim().IsNullOrEmptyString()) return;
            Lib.Cfg.ModelConfig_ModelTagBLL.Insert(ModelNumber, HttpUtility.HtmlEncode(txtTagModelNumber.Text.Trim().ToUpperInvariant()));
            txtTagModelNumber.Text = string.Empty;
            BindModelTags();
            
        }

        #region CustomText

        public void BindLocalizedStrings()
        {
            String customTextClassKey = KeyDef.ResClassKeys.ProductCfgClassKey;
            String customTextResKey = string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, ModelNumber.Trim());
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(customTextClassKey, customTextResKey);
            if (tb == null || tb.Rows.Count < 1)
            {
                Int32 defaultContentID = CreateDefaultContent();
                if (defaultContentID < 1)
                {
                    this.Visible = false;
                    return;
                }
                tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(customTextClassKey, customTextResKey);
            }
            dgList.DataSource = tb;
            dgList.DataBind();
        }

        private Int32 CreateDefaultContent()
        {
            return Lib.Content.Res_ContentStoreBLL.Insert(CustomTextClassKey, CustomTextResKey, "en", string.Empty, true);
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                TextBox txtLocale = (TextBox)e.Item.FindControl("txtLocale");
                TextBox txtContent = (TextBox)e.Item.FindControl("txtContent");
                CultureInfo locale = new CultureInfo(txtLocale.Text.Trim());
                Lib.Content.Res_ContentStoreBLL.Insert(CustomTextClassKey, CustomTextResKey, locale.Name, txtContent.Text.Trim(), true);
                BindLocalizedStrings();
            }

            else if (e.CommandName == "DeleteCommand")
            {
                Int32 contentID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.Content.Res_ContentStoreBLL.DeleteByContentID(contentID);
                BindLocalizedStrings();
            }
        }

        public string BuildResEditUrl(object contentIDObj)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(contentIDObj, 0);
            if (contentID < 1) return String.Empty;
            return String.Format("~/siteadmin/contentadmin/contentdetails?contentid={0}&returnurl={1}", contentID, HttpUtility.UrlEncode(Request.RawUrl));
        }
        #endregion


    }
}