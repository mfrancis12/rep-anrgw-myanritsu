﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfig.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfig" %>

<anrui:GlobalWebBoxedPanel ID="pnlConfigureContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig,pnlConfigureContainer.HeaderText %>'>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvNeedReviews" EnableRowsCache="False" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" SettingsPager-PageSize="20" AutoGenerateColumns="False" DataSourceID="sdsProdMasterModels" KeyFieldName="ModelNumber" Width="100%">
            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true" />
            <Columns>
                <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig,ModelNumber.Text%>" Width="70%" />
                <dx:GridViewDataColumn FieldName="Reg_AllowSelfReg" VisibleIndex="2" SortOrder="Descending" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig,Reg_AllowSelfReg.Text%>" Width="10%" />
                <dx:GridViewDataColumn FieldName="Reg_ValidateSN" VisibleIndex="3" SortOrder="Descending" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig,Reg_ValidateSN.Text%>" Width="10%" />

                <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%">
                    <DataItemTemplate>
                        <a id="clickElement" href="/prodregadmin/modelconfigdetail?mn=<%# HttpUtility.UrlEncode(Container.KeyValue.ToString()) %>" class="blueit-hover">
                            <asp:Localize ID="lcalDetails" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig,lcalDetails.Text %>'></asp:Localize>
                        </a>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="20"></SettingsPager>
            <Settings ShowFilterRow="True" />

        </dx:ASPxGridView>

        <asp:SqlDataSource ID="sdsProdMasterModels" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelMaster_SelectMasterModels]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>

        <div class="settingrow">
            <span class="msg" style="margin-left: 200px">
                <asp:Literal ID="ltrConfigureMsg" runat="server"></asp:Literal>
            </span>
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlAddItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig,pnlAddItemContainer.HeaderText %>'  DefaultButton="bttAddNewMN">
    <div class="settingrow width-60">
        <div class="settingrow group input-text required">
            <label>
                <asp:Localize ID="lcalAddNewMN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig,lcalAddNewMN.Text %>'></asp:Localize></label>
            <p class="error-message">
                <asp:Localize runat="server" ID="lclAddNewErrMsg" Text="<%$Resources:Common,ERROR_InvalidMNInput%>"></asp:Localize>
            </p>
            <asp:RegularExpressionValidator ID="revAddNewMN" runat="server" ControlToValidate="txtAddNewMN"
            Display="Dynamic" ValidationExpression='^[^\s%]*$' ValidationGroup="vgAdd"
            ErrorMessage="<%$Resources:Common,ERROR_InvalidMNInput%>"></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtAddNewMN" runat="server" MaxLength="100"></asp:TextBox>

            <%--<asp:RequiredFieldValidator ID="rvSn" runat="server" ControlToValidate="txtAddNewMN" ErrorMessage="<%$Resources:Common,ERROR_Required%>" SetFocusOnError="true" ValidationGroup="vgAdd" Display="Dynamic"></asp:RequiredFieldValidator>--%>
             

        </div>

        <div class="settingrow group input-checkbox">
            <p><asp:CheckBox ID="cbxAllowProdReg" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig,cbxNewProdIsPaidSupportModel.Text %>" /></p>
            <p><asp:CheckBox ID="cbxValidateSN" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig,cbxNewProdVerifyCompany.Text %>" /></p>
        </div>

        <div class="settingrow group input-text">
            <span class="settinglabel">
                <asp:Localize ID="lcalNewProdImageUrl" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig,lcalNewProdImageUrl.Text %>'></asp:Localize>:</span>
            <asp:TextBox ID="txtNewProdImageUrl" runat="server" MaxLength="300"></asp:TextBox>
        </div>

        <div class="settingrow group margin-top-15">
                <asp:Button ID="bttAddNewMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig,bttAddNewMN.Text%>" 
                    SkinID="submit-btn" ValidationGroup="vgAdd" OnClick="bttAddNewMN_Click" />
        </div>
        <div class="settingrow">
            <p class="msg" style="margin-left: 200px">
                <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>

            </p>

        </div>

    </div>

    <div class="settingrow">
        &nbsp;
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig" />


