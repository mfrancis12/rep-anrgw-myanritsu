﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class ModelConfig : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlAddItemContainer.Visible = IsAdminMode();
        }

        protected void bttAddNewMN_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            AddNewModelConfig();
        }

        private void ClearMessages()
        {
            ltrMsg.Text = String.Empty;
        }

        //<summary>
        //Checks whether user is super admin
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = user.IsAdministrator;
            return isAdminMode;
        }

        private void AddNewModelConfig()
        {
            try
            {
                String mn = HttpUtility.HtmlEncode(txtAddNewMN.Text.Trim().ToUpperInvariant());
                Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(mn);
                if (pcfg != null)
                {
                    ltrMsg.Text = String.Format(GetStaticResource("MSG_AlreadyAdded")
                        //"{0} is already in the database.<a href='{1}?{2}={3}'>Click here</a> to modify."
                        , mn
                        , App_Lib.KeyDef.UrlList.CfgModelDetail.Replace("~", "")
                        , App_Lib.KeyDef.QSKeys.ModelNumber
                        , HttpUtility.UrlEncode(mn));
                    return;
                }

                pcfg = new Lib.Cfg.ProductConfig();
                pcfg.ModelNumber = mn;
                pcfg.CustomImageUrl = HttpUtility.HtmlEncode(txtNewProdImageUrl.Text.Trim());
                pcfg.Reg_ValidateSN = cbxValidateSN.Checked;
                pcfg.Reg_AllowSelfReg = cbxAllowProdReg.Checked;
                pcfg.Reg_AddOnFormPerSN = false;
                pcfg.ShowSupportAgreement = false;
                pcfg.ShowWarrantyInfo = true;             
                Lib.Cfg.ProductConfigBLL.InsertUpdate(pcfg);
                //ltrMsg.Text = GetStaticResource("MSG_ModelConfigUpdated");


                String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.CfgModelDetail
                       , KeyDef.QSKeys.ModelNumber, HttpUtility.UrlEncode(mn));
                WebUtility.HttpRedirect(this, url);
                   
                }

            catch (Exception ex)
            {
                throw ex;

            }

        }


        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

    }
}