﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfigDetail_Item_DKCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfigDetail_Item_DKCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlModelItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,pnlModelItemContainer.HeaderText %>'>
    <div class="settingrow" style="position: relative; height: 300px">

        <div style="float: left; width: 75%; position: relative; margin-left: 20px">

            <div class="settingrow">

                <span class="msg">
                    <asp:Localize ID="lcalNeedReview" runat="server" Visible="false" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalNeedReview.Text %>'></asp:Localize></span>
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalIsPaidSupportModel" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalIsPaidSupportModel.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxIsPaidSupportModel" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalConfigStatus" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalConfigStatus.Text %>'></asp:Localize></span>
                <asp:DropDownList ID="DropDownListConfigStatus" runat="server" AppendDataBoundItems="true" DataTextField="StatusDisplayText"
                    DataValueField="ModelConfigStatusCode">
                </asp:DropDownList>
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalRegInitialStatus" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalRegInitialStatus.Text %>'></asp:Localize></span>
                <asp:DropDownList ID="DropDownListlcalRegInitialStatus" runat="server" AppendDataBoundItems="true" DataTextField="StatusDisplayText" DataValueField="StatusCode">
                </asp:DropDownList>
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalVerifyCompany" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalVerifyCompany.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxVerifyCompany" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalVerifyProductSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalVerifyProductSN.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxVerifyProductSN" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalUseUserACL" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalUseUserACL.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxUseUserACL" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalUseModelTags" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,lcalUseModelTags.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxUseModelTags" runat="server" />
            </div>
            <div class="settingrow" style="text-align: center">
                <br />
                <asp:Button ID="bttUpdate" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_DK,bttUpdate.Text %>'
                    OnClick="bttUpdate_Click" />
            </div>
            <div class="settingrow" style="text-align: center">
                <span class="msg">
                    <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
                </span>
            </div>
        </div>

        <div id="selectedListDiv" runat="server" style="float: left; width: 15%; position: relative; margin-left: 0px; display: none">
            <div>
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalSelectedValues" runat="server" Text='<%$ Resources:Common,SelectedProducts.Text %>'></asp:Localize></span>
            </div>
            <dx:ASPxListBox ID="lstbxSelectedValues" ClientInstanceName="selList" runat="server" Theme="AnritsuDevXTheme" Height="270px"
                Width="150px" />
            <div class="settinglabel-10">
                <div style="float: left;">
                    <span>
                        <asp:Localize ID="lcalSelectedCount" runat="server" Text='<%$ Resources:Common,SelectedCount.Text %>'></asp:Localize></span>
                </div>

                <div style="float: left; margin-left: 1px"><span id="selCount" runat="server" style="font-weight: bold">0</span></div>
            </div>
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_DetailCtrl_DK" />


