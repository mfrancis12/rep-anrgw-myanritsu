﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class ModelConfigDetail_Item_DKCtrl : App_Lib.UI.ModelConfigDetail_ItemCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public override String BatchChangeSessionKey { get { return "ss_SelectedReviewProds_DK"; } }
        
            protected override void OnInit(EventArgs e)
            {
                if (!Page.IsPostBack)
                {

                }
                base.OnInit(e);
                bttUpdate.OnClientClick = "return confirm ('" + GetGlobalResourceObject("common", "ConfirmUpdateRegMsg") + "')";
              
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    BindProdRegStatus();
                    BindProdConfigStatus();
                    if (!String.IsNullOrEmpty(ModelNumber))
                    {
                        LoadProductModelConfig();
                        
                        pnlModelItemContainer.HeaderText += " - " + ModelNumber;
                    }
                    else if (SelectedModels.Count > 0)
                    {
                        ShowSelectedProducts();
                    }
                    else
                        throw new HttpException(404, "Page not found.");


                }
                ClearMessages();
            }

            protected void bttUpdate_Click(object sender, EventArgs e)
            {
                if (!Page.IsValid) return;
                UpdateProductModelConfig();
            }

            private void ClearMessages()
            {
                ltrMsg.Text = String.Empty;
            }

            private void BindProdConfigStatus()
            {
                DropDownListConfigStatus.DataSource = ModelConfigStatusBLL.SelectAll();
                DropDownListConfigStatus.DataBind();
            }

            private void BindProdRegStatus()
            {
                DropDownListlcalRegInitialStatus.DataSource = Admin_ProductRegUtility.ProdReg_Item_Status_SelectAll(true);
                DropDownListlcalRegInitialStatus.DataBind();
            }

            public void ShowSelectedProducts()
            {
                int count = 0;
                foreach (String mn in SelectedModels)
                {
                    if (!String.IsNullOrEmpty(mn))
                    {
                        lstbxSelectedValues.Items.Add(mn);
                        count = count + 1;
                    }
                }
                selCount.InnerText = count.ToString();
                selectedListDiv.Style["Display"] = "Block";
                //bttDelete.Visible = false;
            }

            private void LoadProductModelConfig()
            {
                Lib.Cfg.ProductConfig pcfg = PConfig;
                
                foreach (Lib.Cfg.ModelConfig mcfg in pcfg.ModelConfigs)
                {
                    if(mcfg.ModelConfigType.Equals(Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_DENMARK))
                    {
                        lcalNeedReview.Visible = mcfg.ModelConfigStatus.Equals(ModelConfigStatus.STATUS_TOREVIEW) ? true : false;
                        cbxIsPaidSupportModel.Checked = mcfg.HasSupportExpiration;
                        cbxVerifyCompany.Checked = mcfg.Verify_Company;
                        cbxUseUserACL.Checked = mcfg.UseUserACL;
                        cbxVerifyProductSN.Checked = mcfg.Verify_Registration;
                        cbxUseModelTags.Checked = mcfg.UseModelTags;
                        DropDownListConfigStatus.SelectedValue = mcfg.ModelConfigStatus;
                        DropDownListlcalRegInitialStatus.SelectedValue = mcfg.Reg_InitialStatus;

                    }

                }
            }

            private void UpdateProductModelConfig()
            {
                try
                {
                    Lib.Cfg.ModelConfig mcfg;
                    Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
                    List<String> modelsToUpdate = SelectedModels;

                    if (!ModelNumber.IsNullOrEmptyString())
                    {
                        modelsToUpdate.Clear();
                        modelsToUpdate.Add(ModelNumber.Trim());
                    }

                    if (modelsToUpdate == null) return;
                    foreach (String mn in modelsToUpdate)
                    {
                        mcfg = Lib.Cfg.ModelConfigBLL.SelectByModelAndConfigType(mn, Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_DENMARK);
                        mcfg.HasSupportExpiration = cbxIsPaidSupportModel.Checked;
                        mcfg.Verify_Registration = cbxVerifyProductSN.Checked;
                        mcfg.UseUserACL = cbxUseUserACL.Checked;
                        mcfg.Verify_Company = cbxVerifyCompany.Checked;
                        mcfg.ModelConfigStatus = DropDownListConfigStatus.SelectedValue;
                        mcfg.Reg_InitialStatus = DropDownListlcalRegInitialStatus.SelectedValue;
                        mcfg.UseModelTags = cbxUseModelTags.Checked;
                        //bttDelete.Visible = true;
                        // bttDelete.Visible = IsManageMasterInfoAllowed();
                                         
                        // Lib.Erp.Erp_ProductConfigBLL.InsertUpdate(pcfg);
                        Lib.Cfg.ModelConfigBLL.InsertUpdate(mcfg, String.Empty, user.Email);
                    }
                    lcalNeedReview.Visible = false;
                    ltrMsg.Text = GetStaticResource("MSG_ModelConfigUpdated");
                    ClearSelectedModels();
                }

                catch (Exception ex)
                {
                    throw ex;

                }

            }
               
            public string StaticResourceClassKey
            {
                get { return "ADM_STCTRL_ProdModelConfig_DetailCtrl_DK"; }
            }

            public string GetStaticResource(string resourceKey)
            {
                return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
            }
           
    }
}