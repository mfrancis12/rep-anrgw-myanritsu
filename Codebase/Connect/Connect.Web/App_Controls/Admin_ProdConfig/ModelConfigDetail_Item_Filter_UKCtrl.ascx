﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfigDetail_Item_Filter_UKCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfigDetail_Item_Filter_UKCtrl" %>


<anrui:GlobalWebBoxedPanel ID="pnlModelItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,pnlModelItemContainer.HeaderText %>'>
    <fieldset id="fsConfigure" style="position: relative;">
        <legend>
            <asp:Localize ID="lcalFilterLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,pnlModelItemContainer.HeaderText %>'></asp:Localize></legend>
        <div id="divFilter" runat="server" class="settingrow">
            <div class="settingrow">
                <asp:GridView ID="gvFilters" runat="server" EmptyDataText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,grid.DataIsEmpty%>'
                    OnRowCommand="gvFilters_RowCommand" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                    ShowFooter="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,grid.ModelColumnTitle %>'>
                            <ItemTemplate>
                                <asp:Label ID="lblModelFilter" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ModelFilter") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,grid.FunctionColumnTitle %>'>
                            <ItemTemplate>
                                <asp:Label ID="lblFunctionFilter" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FunctionFilter") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,grid.OptionColumnTitle %>'>
                            <ItemTemplate>
                                <asp:Label ID="lblOptionFilter" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OptionFilter") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="bttDelete" runat="server" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="DeleteTagCommand" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,bttDelete.Text %>' CausesValidation="false" UseSubmitBehavior="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <div id="addFilter" runat="server" class="settingrow">
                <div class="group input-text width-60">
                    <label class="settinglabelplain">
                        <asp:Localize ID="lcalModelNumber" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,lcalModelNumber.Text%>'></asp:Localize>
                    </label>
                    <asp:TextBox ID="txtModelKeyword" runat="server" SkinID="tbx-200" onkeydown="if (window.event.keyCode==13){ return false}" />
                </div>

                <div class="group input-text width-60">
                    <label class="settinglabelplain">
                        <asp:Localize ID="lcalFunctionKeyword" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,lcalFunctionKeyword.Text%>'></asp:Localize>
                    </label>
                    <asp:TextBox ID="txtFunctionKeyword" runat="server" SkinID="tbx-200" onkeydown="if (window.event.keyCode==13){ return false}" />
                </div>

                <div class="group input-text width-60">
                    <label class="settinglabelplain">
                        <asp:Localize ID="lcalOptionKeyword" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,lcalOptionKeyword.Text%>'></asp:Localize>
                    </label>
                    <asp:TextBox ID="txtOptionKeyword" runat="server" SkinID="tbx-200" onkeydown="if (window.event.keyCode==13){ return false}" />
                </div>

                <div class="group margin-top-15">
                    <span class="msg">
                        <asp:Localize ID="lblErrorMessage" runat="server"></asp:Localize>
                    </span>
                </div>

                <div class="group margin-top-15">
                    <asp:Button ID="bttAddFilter" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter,bttAddFilter.Text%>' OnClick="bttAddFilter_Click" UseSubmitBehavior="false" />
                </div>
            </div>
        </div>
    </fieldset>
</anrui:GlobalWebBoxedPanel>

