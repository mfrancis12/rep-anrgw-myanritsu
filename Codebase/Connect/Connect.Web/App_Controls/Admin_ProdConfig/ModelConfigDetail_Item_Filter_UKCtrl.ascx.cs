﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;
using System.Text.RegularExpressions;
using Anritsu.Connect.Lib.Product;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class ModelConfigDetail_Item_Filter_UKCtrl : App_Lib.UI.ModelConfigDetail_ItemCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        #region Define
        private const int KEYWORD_LENGTH = 15;

        private const string MODEL_CONFIG_TYPE_INFO = Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD;
		#endregion

        #region Properties
        public override String BatchChangeSessionKey { get { return String.Empty; } }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig_DetailCtrl_Filter "; }
        }

        public Anritsu.Connect.Lib.Cfg.ModelConfig ModelConfig
        {
            get
            {
                ProductConfig pcfg = PConfig;
                var mcfg = pcfg.ModelConfigs.Where(w => w.ModelConfigType.Equals(MODEL_CONFIG_TYPE_INFO)).Select(s => s).FirstOrDefault();

                return mcfg;
            }
        }
        #endregion

        #region Public method & events

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(ModelNumber))
                {
                    pnlModelItemContainer.HeaderText += " - " + ModelNumber;

                    // Bind List
                    bindFilterList();
                }
                else
                    throw new HttpException(404, "Page not found.");
            }
            clearMessages();
        }



        protected void bttAddFilter_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            var modelKeyword = txtModelKeyword.Text.Trim();
            var functionKeyword = txtFunctionKeyword.Text.Trim();
            var optionKeyword = txtOptionKeyword.Text.Trim();

            // Model need inputted.
            if (modelKeyword.IsNullOrEmptyString())
            {
                lblErrorMessage.Text = GetStaticResource("lcalErroMessage.ModelKeywordIsEmpty");
                return;
            }

            // Wide-char(2bye char) is not Support
            var isWideChar = isFullWideCharacter(modelKeyword, functionKeyword, optionKeyword);
            if (isWideChar)
            {
                lblErrorMessage.Text = GetStaticResource("lcalErroMessage.FullWideCharIsNotSupport");
                return;
            }

            // Check length of the Keyword
            if (modelKeyword.Length > KEYWORD_LENGTH || functionKeyword.Length > KEYWORD_LENGTH || optionKeyword.Length > KEYWORD_LENGTH)
            {
                lblErrorMessage.Text = GetStaticResource("lcalErroMessage.KeywordLengthOver");
                return;
            }

            // is it already exist?
            bool exist = isKeywordExist(modelKeyword, functionKeyword, optionKeyword);
            if (exist)
            {
                lblErrorMessage.Text = GetStaticResource("lcalErroMessage.AlreadyExist");
                return;
            }

            // Register New Filter
            var currentUser = LoginUtility.GetCurrentUserOrSignIn();
            var result = ProductFilterFunctionBLL.Insert(ModelConfig.ModelConfigID, modelKeyword, functionKeyword, optionKeyword, currentUser.Email);
            if (result == false)
            {
                // Error. Insert Failed
                lblErrorMessage.Text = "Register new Filter failed.";
                return;
            }
            // Log
            ProductFilterFunctionBLL.InsertToAdminLog(ModelConfig.ModelConfigID, modelKeyword, functionKeyword, optionKeyword, currentUser.Email);

            // Reload GridView
            bindFilterList();

            // Init TextBox
            txtModelKeyword.Text = String.Empty;
            txtFunctionKeyword.Text = String.Empty;
            txtOptionKeyword.Text = String.Empty;
        }

        protected void gvFilters_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteTagCommand")
            {
                var rowIndex = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                var rows = gvFilters.Rows;
                // Get Label Text
                var model = ((Label)(rows[rowIndex].FindControl("lblModelFilter"))).Text;
                var function = ((Label)(rows[rowIndex].FindControl("lblFunctionFilter"))).Text;
                var option = ((Label)(rows[rowIndex].FindControl("lblOptionFilter"))).Text;

                // Delete
                ProductFilterFunctionBLL.Delete(ModelConfig.ModelConfigID, model, function, option);
                // Log
                var currentUser = LoginUtility.GetCurrentUserOrSignIn();
                ProductFilterFunctionBLL.UpdateDeleteStatusForAdminLog(ModelConfig.ModelConfigID, model, function, option, currentUser.Email);

                // Reload GridView
                bindFilterList();
            }
        }

        #endregion

        #region Private Method
        /// <summary>
        /// Bind GridView.
        /// </summary>
        private void bindFilterList()
        {
            if (ModelConfig == null)
                return;

            // Select
            var list = ProductFilterFunctionBLL.SelectByModelConfigID(ModelConfig.ModelConfigID);
            gvFilters.DataSource = list;
            gvFilters.DataBind();

        }

        /// <summary>
        /// Clear Message Label
        /// </summary>
        private void clearMessages()
        {
            lblErrorMessage.Text = String.Empty;
        }

        /// <summary>
        /// Is Keyword Full-Wide Character?
        /// </summary>
        /// <param name="modelKeyword"></param>
        /// <param name="functionKeyword"></param>
        /// <param name="optionKeyword"></param>
        /// <returns></returns>
        private bool isFullWideCharacter(string modelKeyword, string functionKeyword, string optionKeyword)
        {
            var result = new List<string>() { modelKeyword, functionKeyword, optionKeyword }.Where(keyword =>
            {
                if (keyword.IsNullOrEmptyString())
                    return false;

                var pattern = "^[\x20-\x7E]+$";
                var regex = new Regex(pattern);
                if (regex.Match(keyword).Success == false)
                {
                    return true;
                }
                return false;
            }).Any();

            return result;
        }

        /// <summary>
        /// Check the Model,Function,Option is already exist?
        /// </summary>
        /// <param name="model"></param>
        /// <param name="function"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        private bool isKeywordExist(string model, string function, string option)
        {
            // Select
            return ProductFilterFunctionBLL.SelectByFilterKeywords(ModelConfig.ModelConfigID, model, function, option).Any();
        }
        #endregion
    }
}