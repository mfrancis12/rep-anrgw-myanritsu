﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfigDetail_Item_JPCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfigDetail_Item_JPCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlModelItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,pnlModelItemContainer.HeaderText %>'>
    <div class="settingrow overflow">
        <div class="width-60 left">
            <div class="settingrow group">
                <label class="msg">
                    <asp:Localize ID="lcalNeedReview" runat="server" Visible="false" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalNeedReview.Text %>'></asp:Localize></label>
            </div>
            <div class="settingrow group">
                <label class="settinglabelrgt-20">
                    <asp:Localize ID="lcalIsPaidSupportModel" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalIsPaidSupportModel.Text %>'></asp:Localize></label>
                <asp:CheckBox ID="cbxIsPaidSupportModel" runat="server" />
            </div>
            <div class="group settingrow input-select">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalConfigStatus" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalConfigStatus.Text %>'></asp:Localize></span>
                <asp:DropDownList ID="DropDownListConfigStatus" runat="server" AppendDataBoundItems="true" DataTextField="StatusDisplayText"
                    DataValueField="ModelConfigStatusCode">
                </asp:DropDownList>
            </div>
            <div class="group settingrow input-select">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalRegInitialStatus" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalRegInitialStatus.Text %>'></asp:Localize></span>
                <asp:DropDownList ID="DropDownListlcalRegInitialStatus" runat="server" AppendDataBoundItems="true" DataTextField="StatusDisplayText" DataValueField="StatusCode">
                </asp:DropDownList>
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalVerifyCompany" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalVerifyCompany.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxVerifyCompany" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalVerifyProductSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalVerifyProductSN.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxVerifyProductSN" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalUseUserACL" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalUseUserACL.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxUseUserACL" runat="server" />
            </div>
            <div class="settingrow">
                <span class="settinglabelrgt-20">
                    <asp:Localize ID="lcalUseModelTags" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,lcalUseModelTags.Text %>'></asp:Localize></span>
                <asp:CheckBox ID="cbxUseModelTags" runat="server" />
            </div>
            <div class="settingrow">
                <br />
                <asp:Button ID="bttUpdate" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM,bttUpdate.Text %>'
                    OnClick="bttUpdate_Click" />
            </div>
            <div class="settingrow">
                <span class="msg">
                    <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
                </span>
            </div>
        </div>

        <div id="selectedListDiv" class="width-30 right" runat="server" style="display: none">
            <div style="padding:20px">
            <div class="group">
                <label>
                    <asp:Localize ID="lcalSelectedValues" runat="server" Text='<%$ Resources:Common,SelectedProducts.Text %>'></asp:Localize></label>
            </div>
            <dx:ASPxListBox ID="lstbxSelectedValues" ClientInstanceName="selList" runat="server" Theme="AnritsuDevXTheme" Width="100%" />
            <div class="settinglabel-10">
                <div style="float: left;">
                    <span>
                        <asp:Localize ID="lcalSelectedCount" runat="server" Text='<%$ Resources:Common,SelectedCount.Text %>'></asp:Localize></span>
                </div>

                <div style="float: left; margin-left: 1px"><span id="selCount" runat="server" style="font-weight: bold">0</span></div>
            </div>
            </div>
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_DetailCtrl _JPM" />

