﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfig_Item_DKCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfig_Item_DKCtrl" %>

<script type="text/javascript">
    var _selectNumber = 0;
    var _all = false;
    var _handle = true;
    function OnGridSelectionChanged(s, e) {
        if (e.isChangedOnServer == false) {
            if (e.isAllRecordsOnPage && e.isSelected)
                _selectNumber = s.GetVisibleRowsOnPage();
            else if (e.isAllRecordsOnPage && !e.isSelected)
                _selectNumber = 0;
            else if (!e.isAllRecordsOnPage && e.isSelected)
                _selectNumber++;
            else if (!e.isAllRecordsOnPage && !e.isSelected)
                _selectNumber--;

            if (_handle) {
                SelectAllCheckBox.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                _handle = false;
            }
            _handle = true;
        }
    }
    function OnGridEndCallback(s, e) {
        _selectnumber = s.cpselectedrowsonpage;
    }
</script>
<div class="settingrow">
    <fieldset id="fsConfigure" style="position: relative;">
        <legend>
            <asp:Localize ID="lcalConfigureLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DK,lcalConfigureLegend.Text %>'></asp:Localize></legend>
        <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
            <tr>
                <td style="padding-right: 4px">
                    <dx:ASPxButton ID="btnSelectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,btnSelectAll.Text%>" UseSubmitBehavior="False"
                        AutoPostBack="false">
                        <ClientSideEvents Click="function(s, e) { grid.SelectRows(); }" />
                    </dx:ASPxButton>
                </td>
                <td style="padding-right: 4px">
                    <dx:ASPxButton ID="btnUnselectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,btnUnselectAll.Text%>" UseSubmitBehavior="False"
                        AutoPostBack="false">
                        <ClientSideEvents Click="function(s, e) { grid.UnselectRows(); }" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>

        <dx:ASPxGridView ID="gvNeedReviews" EnableRowsCache="False" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" OnPageIndexChanged="gvNeedReviews_OnPageIndexChanged" SettingsPager-PageSize="20" AutoGenerateColumns="False" DataSourceID="sdsProdRegItems" KeyFieldName="ModelNumber" Width="100%">
            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true" />
            <Columns>
                <dx:GridViewCommandColumn Name="Select" ShowSelectCheckbox="True" VisibleIndex="0">
                    <HeaderTemplate>
                        <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ToolTip="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,SelectAllCheckBox.Text%>"
                            ClientSideEvents-CheckedChanged="function(s, e) { grid.SelectAllRowsOnPage(s.GetChecked()); }" ClientInstanceName="SelectAllCheckBox" OnInit="SelectAllCheckBox_Init"/>
                    </HeaderTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,ModelNumber.Text%>" />
                <dx:GridViewDataColumn FieldName="ModelConfigStatus" VisibleIndex="2" SortOrder="Descending" Settings-AllowAutoFilter="True" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,IsActive.Text%>" />
               <%-- <dx:GridViewDataCheckColumn FieldName="ModelConfigStatus" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,IsActive.Text%>">
                    <PropertiesCheckEdit DisplayTextChecked="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,Reviewed.Text%>" DisplayTextUnchecked="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,NotReviewed.Text%>"
                        UseDisplayImages="False">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>--%>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="3" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,CreatedOn.Text%>">
                    <PropertiesDateEdit DisplayFormatString="d">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn VisibleIndex="10">
                      <DataItemTemplate>
                        <a href="/prodregadmin/modelconfigdetail_dk?mn=<%# HttpUtility.UrlEncode(Container.KeyValue.ToString()) %>" 
                            class="blueit-hover">
                            Details
                        </a>
                      </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager PageSize="20"></SettingsPager>
            <Settings ShowFilterRow="True" />
            <ClientSideEvents SelectionChanged="OnGridSelectionChanged" EndCallback="OnGridEndCallback" />
        </dx:ASPxGridView>

        <asp:SqlDataSource ID="sdsProdRegItems" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectByConfigType]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>">
            <SelectParameters>
                <asp:Parameter DefaultValue="downloads-dk" Name="ModelConfigType" DbType="String" />
            </SelectParameters>
        </asp:SqlDataSource>

        <div class="settingrow" style="margin-top: 20px">
            <asp:Button ID="btnConfigure" Text='<%$ Resources:Common,btnConfigure.Text %>' runat="server"
                OnClick="btnConfigure_Click" />

        </div>

        <div class="settingrow">
             <span  class="msg" style="margin-left: 200px">
                <asp:Literal ID="ltrConfigureMsg" runat="server"></asp:Literal>
            </span>
       </div>
    </fieldset>
</div>

<div class="settingrow">
    &nbsp;
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_DK" />





