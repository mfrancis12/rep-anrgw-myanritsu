﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
   public partial class ModelConfig_Item_DKCtrl : App_Lib.UI.ModelConfigList_CtrlBase
    {
        public int ProductRowCount
        {
            get
            {
                int count = gvNeedReviews.VisibleRowCount;
                if (Session[SelectedProductCountSessionKey] != null && !String.IsNullOrEmpty(Session[SelectedProductCountSessionKey].ToString()) && gvNeedReviews.VisibleRowCount == 0)
                {
                    count = Convert.ToInt32(Session[SelectedProductCountSessionKey].ToString());
                }

                return count;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (!App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD))
                //{
                //    this.Visible = false;
                //    return;
                //}
                LoadSelectedModels();
            }
        }

        protected void SelectAllCheckBox_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            Boolean cbChecked = true;
            //get the naming container
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            Int32 start = grid.VisibleStartIndex;
            Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);
            for (int i = start; i < end; i++)
            {
                if (!grid.Selection.IsRowSelected(i))
                {
                    cbChecked = false;
                    break;
                }
            }
            chk.Checked = cbChecked;
        }

        /// <summary>
        /// Returns preselected rows
        /// </summary>
        private void LoadSelectedModels()
        {
            List<object> keys = new List<object>();

            for (int i = 0; i < ProductRowCount; i++)
            {
                keys.Add(gvNeedReviews.GetRowValues(i, "ModelNumber"));
            }

            for (int i = 0; i < keys.Count; i++)
            {
                int visibleIndex = gvNeedReviews.FindVisibleIndexByKeyValue(keys[i]);
                foreach (String mn in SelectedModels)
                {
                    if (mn.Equals(keys[i]))
                    {
                        gvNeedReviews.Selection.SelectRow(visibleIndex);
                    }
                }
            }
        }

        protected void gvNeedReviews_OnPageIndexChanged(object sender, EventArgs e)
        {
            List<object> keys = gvNeedReviews.GetSelectedFieldValues("ModelNumber");
            SetSelectedModels(keys, gvNeedReviews.VisibleRowCount);
            LoadSelectedModels();
        }

        protected void btnConfigure_Click(object sender, EventArgs e)
        {
            List<object> keys = gvNeedReviews.GetSelectedFieldValues("ModelNumber");
            Boolean configureCheck = SetBatchConfigure(keys, gvNeedReviews.VisibleRowCount);
            if (configureCheck)
            {
                WebUtility.HttpRedirect(this, KeyDef.UrlList.CfgModelDetail_DK);
            }
            else
            {
                ltrConfigureMsg.Text = GetStaticResource("SelectModelMsg");
            }
        }

        public String GetUrl(object model)
        {
            String mn = model.ToString().ToUpperInvariant();
            String url = String.Format("/prodregadmin/modelconfig_dk?mn={0}"
                         , HttpUtility.UrlEncode(mn));

            return url;
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig_DK"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion

        public override string BatchChangeSessionKey
        {
            get { return "ss_SelectedReviewProds_DK"; }
        }

        public override string SelectedProductCountSessionKey
        {
            get { return "ss_SelectedProdCountDK"; }
        }
    }
}