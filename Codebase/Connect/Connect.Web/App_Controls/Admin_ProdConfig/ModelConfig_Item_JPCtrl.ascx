﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfig_Item_JPCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfig_Item_JPCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlConfigureContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_JPM,pnlConfigureContainer.HeaderText %>'>
    <script type="text/javascript">
        var _selectNumber = 0;
        var _all = false;
        var _handle = true;
        function OnGridSelectionChanged(s, e) {
            if (e.isChangedOnServer == false) {
                if (e.isAllRecordsOnPage && e.isSelected)
                    _selectNumber = s.GetVisibleRowsOnPage();
                else if (e.isAllRecordsOnPage && !e.isSelected)
                    _selectNumber = 0;
                else if (!e.isAllRecordsOnPage && e.isSelected)
                    _selectNumber++;
                else if (!e.isAllRecordsOnPage && !e.isSelected)
                    _selectNumber--;

                if (_handle) {
                    SelectAllCheckBox.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                    _handle = false;
                }
                _handle = true;
            }
        }
        function OnGridEndCallback(s, e) {
            _selectnumber = s.cpselectedrowsonpage;
        }
    </script>
    <div class="settingrow">
        <fieldset id="fsConfigure" style="position: relative;">
            <legend>
                <asp:Localize ID="lcalConfigureLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_JPM,lcalConfigureLegend.Text %>'></asp:Localize></legend>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                <tr>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnSelectAll" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnSelectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false" CssClass="button">
                            <ClientSideEvents Click="function(s, e) { grid.SelectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnUnselectAll" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnUnselectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false" CssClass="button">
                            <ClientSideEvents Click="function(s, e) { grid.UnselectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>

            <dx:ASPxGridView ID="gvNeedReviews" EnableRowsCache="False" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" OnPageIndexChanged="gvNeedReviews_OnPageIndexChanged" SettingsPager-PageSize="20" AutoGenerateColumns="False" DataSourceID="sdsProdRegItems" KeyFieldName="ModelNumber" Width="100%">
                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true" />
                <Columns>
                    <dx:GridViewCommandColumn Name="Select" ShowSelectCheckbox="True" VisibleIndex="0">
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ToolTip="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,SelectAllCheckBox.Text%>"
                                ClientSideEvents-CheckedChanged="function(s, e) { grid.SelectAllRowsOnPage(s.GetChecked()); }" ClientInstanceName="SelectAllCheckBox" OnInit="SelectAllCheckBox_Init" />
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,ModelNumber.Text%>" />
                    <dx:GridViewDataColumn FieldName="ModelConfigStatus" SortOrder="Descending" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,IsActive.Text%>" />
                    <%--<dx:GridViewDataCheckColumn FieldName="ModelConfigStatus" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,IsActive.Text%>">
                    <PropertiesCheckEdit DisplayTextChecked="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,Reviewed.Text%>" DisplayTextUnchecked="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,NotReviewed.Text%>"
                        UseDisplayImages="False">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>--%>
                    <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="3" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,CreatedOn.Text%>" Width="20%">
                        <PropertiesDateEdit DisplayFormatString="d">
                        </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="10">
                      <DataItemTemplate>
                        <a href="/prodregadmin/modelconfigdetail_jp?mn=<%# HttpUtility.UrlEncode(Container.KeyValue.ToString()) %>" 
                            class="blueit-hover">
                            Details
                        </a>
                      </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="20"></SettingsPager>
                <Settings ShowFilterRow="True" />
                <ClientSideEvents SelectionChanged="OnGridSelectionChanged" EndCallback="OnGridEndCallback" />
            </dx:ASPxGridView>

            <asp:SqlDataSource ID="sdsProdRegItems" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectByConfigType]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>">
                <SelectParameters>
                    <asp:Parameter DefaultValue="downloads-jp" Name="ModelConfigType" DbType="String" />
                </SelectParameters>
            </asp:SqlDataSource>

            <div class="settingrow" style="margin-top: 20px">
                <asp:Button ID="btnConfigure" Text='<%$ Resources:Common,btnConfigure.Text %>' runat="server"
                    OnClick="btnConfigure_Click" />

                <asp:Button ID="btnShowPopUp" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_JPM,btnShowPopUp.Text %>' runat="server"
                    Font-Bold="true" OnClick="btnPopUp_Click" />
            </div>

            <div class="settingrow">
                <span class="msg" style="margin-left: 200px">
                    <asp:Literal ID="ltrConfigureMsg" runat="server"></asp:Literal>
                </span>
            </div>
        </fieldset>
    </div>

</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlAddItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_JPM,pnlAddItemContainer.HeaderText %>'>
    <asp:Panel ID="pnlAddItemContainerChild" runat="server" DefaultButton="bttModelSearch">
        <div class="settingrow">
            <div id="SearchDiv" class="settingrow" runat="server">
                <div class="settingrow">
                    <asp:Literal ID="ltrModelSearchText" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,ltrModelSearchText.Text%>"></asp:Literal>
                </div>
                <div class="settingrow group input-text width-60 required">
                    <label>
                        <asp:Localize ID="lcalModelSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,lcalModelSearchKeyword.Text%>"></asp:Localize>:</label>
                    <p class="error-message">
                        <asp:Localize ID="lblSrchErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                    </p>
                    <asp:TextBox ID="txtModelSearchKeyword" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="rvSn" runat="server" ControlToValidate="txtModelSearchKeyword" ErrorMessage="<%$Resources:Common,ERROR_Required%>" SetFocusOnError="true" ValidationGroup="vgSearch" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    

                </div>
                <div class="group margin-top-15">
                    <asp:Button ID="bttModelSearch" runat="server" skinID="submit-btn" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,bttModelSearch.Text%>" ValidationGroup="vgSearch" OnClick="bttModelSearch_Click" />
                </div>
               <div class="settingrow">
                    <asp:GridView ID="gvModelSearchResults" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvModelSearchResults_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="ModelNumber" HeaderText="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,gvModelSearchResults.ModelNumber%>" ItemStyle-Width="350px" ItemStyle-CssClass="gvstyle1-item-lft" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink id="hlAdd" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,gvModelSearchResults.Add%>" NavigateUrl='<%# GetUrl(DataBinder.Eval(Container.DataItem,"ModelNumber")) %>' ></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div id="addNewItemDiv" class="settingrow" runat="server" visible="false">
                <div class="settingrow">
                    <span class="margin-right-15">
                        <asp:Localize ID="lcalMasterMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,lcalMasterMN.Text%>"></asp:Localize>:</span>
                    <asp:Literal ID="ltrModelNumber" runat="server"></asp:Literal>
                </div>
                <div class="settingrow">
                    <asp:CheckBox ID="cbxNewProdIsPaidSupportModel" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxNewProdIsPaidSupportModel.Text %>" />
                    <asp:CheckBox ID="cbxNewProdVerifyCompany" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxNewProdVerifyCompany.Text %>" />
                    <asp:CheckBox ID="cbxVerifyProductSN" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxVerifyProductSN.Text %>" />
                </div>

                <div class="settingrow margin-top-15">

                    <span>
                        <asp:Button ID="bttAddNewItemMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,bttAddNewItemMN.Text%>"
                            OnClick="bttAddNewItemMN_Click" />

                    </span>
                </div>
                <div class="settingrow">
                    <p class="msg">
                        <asp:Literal ID="ltrItemMsg" runat="server"></asp:Literal>
                    </p>
                </div>
            </div>

        </div>
        <div class="settingrow">
            &nbsp;
        </div>
</asp:Panel>
</anrui:GlobalWebBoxedPanel>
<div class="settingrow">
    &nbsp;
</div>
<dx:ASPxPopupControl ID="pcRelease" runat="server" CloseAction="CloseButton" Modal="True" Theme="AnritsuDevXTheme" Width="500px" ShowLoadingPanel="true"
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcRelease"
    HeaderText="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,pcRelease.Text%>" AllowDragging="True" PopupAnimationType="None" EnableViewState="False">

    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
            <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                <PanelCollection>
                    <dx:PanelContent ID="PanelContent1" runat="server">
                        <table cellpadding="0" cellspacing="0" width="300px" style="margin-left: 100px">
                            <tr>
                                <td>
                                    <span class="settinglabel-10">
                                        <asp:Localize ID="lcalSelectedValues" runat="server" Text='<%$ Resources:Common,SelectedProducts.Text %>'></asp:Localize></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxListBox ID="lstbxSelectedValues" ClientInstanceName="selList" runat="server" Theme="AnritsuDevXTheme" Height="300px"
                                        Width="200px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="settinglabel-10">
                                        <div style="float: left;">
                                            <span>
                                                <asp:Localize ID="lcalSelectedCount" runat="server" Text='<%$ Resources:Common,SelectedCount.Text %>'></asp:Localize></span>
                                        </div>
                                        <div style="float: left; margin-left: 5px"><span id="selCount" runat="server" style="font-weight: bold">0</span></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div>
                                        <table width="80%" cellpadding="0" cellspacing="0" style="margin-left: 150px">
                                            <tr>
                                                <td align="right">
                                                    <dx:ASPxButton ID="btnRelease" Theme="AnritsuDevXTheme" runat="server" Text='<%$ Resources:Common,btnRelease.Text %>' Width="80px" OnClick="btnRelease_Click">
                                                    </dx:ASPxButton>
                                                </td>
                                                <td>
                                                    <div style="width: 8px;">
                                                    </div>
                                                </td>
                                                <td align="left">
                                                    <dx:ASPxButton ID="btnCancel" Theme="AnritsuDevXTheme" runat="server" Text='<%$ Resources:Common,btnCancel.Text %>' Width="80px" AutoPostBack="False">
                                                        <ClientSideEvents Click="function(s, e) { pcRelease.Hide(); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>

        </dx:PopupControlContentControl>
    </ContentCollection>
    <ContentStyle>
        <Paddings PaddingBottom="5px" />
    </ContentStyle>
</dx:ASPxPopupControl>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_JPM" />



