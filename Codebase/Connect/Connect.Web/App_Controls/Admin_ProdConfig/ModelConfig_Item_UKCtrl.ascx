﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModelConfig_Item_UKCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdConfig.ModelConfig_Item_UKCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlConfigureContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,pnlConfigureContainer.HeaderText %>'>
    <script type="text/javascript">
        var _selectNumber = 0;
        var _all = false;
        var _handle = true;
        function OnGridSelectionChanged(s, e) {
            if (e.isChangedOnServer == false) {
                if (e.isAllRecordsOnPage && e.isSelected)
                    _selectNumber = s.GetVisibleRowsOnPage();
                else if (e.isAllRecordsOnPage && !e.isSelected)
                    _selectNumber = 0;
                else if (!e.isAllRecordsOnPage && e.isSelected)
                    _selectNumber++;
                else if (!e.isAllRecordsOnPage && !e.isSelected)
                    _selectNumber--;

                if (_handle) {
                    SelectAllCheckBox.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                    _handle = false;
                }
                _handle = true;
            }
        }
        function OnGridEndCallback(s, e) {
            _selectnumber = s.cpselectedrowsonpage;
        }
    </script>
    <div class="settingrow">
        <fieldset id="fsConfigure" style="position: relative;">
            <legend>
                <asp:Localize ID="lcalConfigureLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_UK,lcalConfigureLegend.Text %>'></asp:Localize></legend>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                <tr>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnSelectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,btnSelectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e) { grid.SelectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnUnselectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,btnUnselectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e) { grid.UnselectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>

            <dx:ASPxGridView ID="gvNeedReviews" EnableRowsCache="False" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" OnPageIndexChanged="gvNeedReviews_OnPageIndexChanged" SettingsPager-PageSize="20" AutoGenerateColumns="False" DataSourceID="sdsProdRegItems" KeyFieldName="ModelNumber" Width="100%">
                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true" />
                <Columns>
                    <dx:GridViewCommandColumn Name="Select" ShowSelectCheckbox="True" VisibleIndex="0">
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ToolTip="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,SelectAllCheckBox.Text%>"
                                ClientSideEvents-CheckedChanged="function(s, e) { grid.SelectAllRowsOnPage(s.GetChecked()); }" ClientInstanceName="SelectAllCheckBox" OnInit="SelectAllCheckBox_Init" />
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,ModelNumber.Text%>" />
                    <dx:GridViewDataColumn FieldName="ModelConfigStatus" VisibleIndex="2" SortOrder="Descending" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,IsActive.Text%>" />
                    <%--<dx:GridViewDataCheckColumn FieldName="ModelConfigStatus" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,IsActive.Text%>">
                    <PropertiesCheckEdit DisplayTextChecked="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,Reviewed.Text%>" DisplayTextUnchecked="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,NotReviewed.Text%>"
                        UseDisplayImages="False">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>--%>
                    <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="3" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_UK,CreatedOn.Text%>" Width="20%">
                        <PropertiesDateEdit DisplayFormatString="d">
                        </PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataColumn FieldName="AssignedOwner" VisibleIndex="4" SortOrder="Descending" Settings-AllowAutoFilter="True" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_DK,AssignedOwner.Text%>" />
                    <dx:GridViewDataTextColumn VisibleIndex="10">
                      <DataItemTemplate>
                        <a href="/prodregadmin/modelconfigdetail_uk?mn=<%# HttpUtility.UrlEncode(Container.KeyValue.ToString()) %>" 
                            class="blueit-hover">
                            Details
                        </a>
                      </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="20"></SettingsPager>
                <Settings ShowFilterRow="True" />
                <ClientSideEvents SelectionChanged="OnGridSelectionChanged" EndCallback="OnGridEndCallback" />
            </dx:ASPxGridView>

            <asp:SqlDataSource ID="sdsProdRegItems" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectByConfigType]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>">
                <SelectParameters>
                    <asp:Parameter DefaultValue="downloads-uk" Name="ModelConfigType" DbType="String" />
                </SelectParameters>
            </asp:SqlDataSource>

            <div class="settingrow" style="margin-top: 20px">
                <asp:Button ID="btnConfigure" Text='<%$ Resources:Common,btnConfigure.Text %>' runat="server"
                    OnClick="btnConfigure_Click" />

            </div>

            <div class="settingrow">
                <span class="msg">
                    <asp:Literal ID="ltrConfigureMsg" runat="server"></asp:Literal>
                </span>
            </div>
        </fieldset>
    </div>

    <div class="settingrow">
        &nbsp;
    </div>
</anrui:GlobalWebBoxedPanel>

<anrui:GlobalWebBoxedPanel ID="pnlAddItemContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,pnlAddItemContainer.HeaderText %>'>
    <asp:Panel ID="pnlAddItemContainerChild" runat="server" DefaultButton="bttModelSearch">
        <div class="settingrow">
            <div id="SearchDiv" class="settingrow" runat="server">
                <div class="settingrow">
                    <asp:Literal ID="ltrModelSearchText" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,ltrModelSearchText.Text%>"></asp:Literal>
                </div>
                <div class="settingrow group input-text width-60 required">
                    <label>
                        <asp:Localize ID="lcalModelSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,lcalModelSearchKeyword.Text%>"></asp:Localize>:</label>
                    <p class="error-message">
                        <asp:Localize ID="lclSrchErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                    </p>
                    <asp:TextBox ID="txtModelSearchKeyword" runat="server"></asp:TextBox>
                    <%--<asp:RequiredFieldValidator ID="rvSn" runat="server" ControlToValidate="txtModelSearchKeyword" ErrorMessage="<%$Resources:Common,ERROR_Required%>" SetFocusOnError="true" ValidationGroup="vgSearch" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    

                </div>
                <div class="group margin-top-15">
                    <asp:Button ID="bttModelSearch" SkinID="submit-btn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,bttModelSearch.Text%>" ValidationGroup="vgSearch" OnClick="bttModelSearch_Click" />
                </div>
                <div class="settingrow">
                    <asp:GridView ID="gvModelSearchResults" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvModelSearchResults_PageIndexChanging">
                        <Columns>
                            <asp:BoundField DataField="ModelNumber" HeaderText="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,gvModelSearchResults.ModelNumber%>" ItemStyle-Width="350px" ItemStyle-CssClass="gvstyle1-item-lft" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlAdd" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,gvModelSearchResults.Add%>" NavigateUrl='<%# GetUrl(DataBinder.Eval(Container.DataItem,"ModelNumber")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div id="addNewItemDiv" class="settingrow" runat="server" visible="false">
                <div class="settingrow">
                    <span class="settinglabel">
                        <asp:Localize ID="lcalMasterMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,lcalMasterMN.Text%>"></asp:Localize>:</span>
                    <span class="settinglabelplain">
                        <asp:Literal ID="ltrModelNumber" runat="server"></asp:Literal>
                    </span>
                </div>
                <div class="settingrow">
                    <asp:CheckBox ID="cbxNewProdIsPaidSupportModel" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxNewProdIsPaidSupportModel.Text %>" />
                    <asp:CheckBox ID="cbxNewProdVerifyCompany" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxNewProdVerifyCompany.Text %>" />
                    <asp:CheckBox ID="cbxVerifyProductSN" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_USMMD,cbxVerifyProductSN.Text %>" />
                </div>

                <div class="settingrow">

                    <span>
                        <asp:Button ID="bttAddNewItemMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_USMMD,bttAddNewItemMN.Text%>" OnClick="bttAddNewItemMN_Click" />

                    </span>
                </div>
            </div>

            <div class="settingrow">
                <p class="msg" style="margin-left: 100px">
                    <asp:Literal ID="ltrItemMsg" runat="server"></asp:Literal>
                </p>
            </div>


        </div>
        <div class="settingrow">
            &nbsp;
        </div>
    </asp:Panel>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_UK" />





