﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdConfig
{
    public partial class ModelConfig_Item_UKCtrl : App_Lib.UI.ModelConfigList_CtrlBase
    {
        public int ProductRowCount
        {
            get
            {
                int count = gvNeedReviews.VisibleRowCount;
                if (Session[SelectedProductCountSessionKey] != null && !String.IsNullOrEmpty(Session[SelectedProductCountSessionKey].ToString()) && gvNeedReviews.VisibleRowCount == 0)
                {
                    count = Convert.ToInt32(Session[SelectedProductCountSessionKey].ToString());
                }

                return count;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ModelNumber.IsNullOrEmptyString())
                {

                    addNewItemDiv.Visible = false;
                    LoadSelectedModels();
                }
                else
                {
                    addNewItemDiv.Visible = true;
                    ltrModelNumber.Text = ModelNumber;
                }
            }
            ClearMessages();
        }

        protected void SelectAllCheckBox_Init(object sender, EventArgs e)
        {
            ASPxCheckBox chk = sender as ASPxCheckBox;
            Boolean cbChecked = true;
            //get the naming container
            ASPxGridView grid = (chk.NamingContainer as GridViewHeaderTemplateContainer).Grid;
            Int32 start = grid.VisibleStartIndex;
            Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
            end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);
            for (int i = start; i < end; i++)
            {
                if (!grid.Selection.IsRowSelected(i))
                {
                    cbChecked = false;
                    break;
                }
            }
            chk.Checked = cbChecked;
        }

        /// <summary>
        /// Returns preselected rows
        /// </summary>
        private void LoadSelectedModels()
        {
            List<object> keys = new List<object>();

            for (int i = 0; i < ProductRowCount; i++)
            {
                keys.Add(gvNeedReviews.GetRowValues(i, "ModelNumber"));
            }

            for (int i = 0; i < keys.Count; i++)
            {
                int visibleIndex = gvNeedReviews.FindVisibleIndexByKeyValue(keys[i]);
                foreach (String mn in SelectedModels)
                {
                    if (mn.Equals(keys[i]))
                    {
                        gvNeedReviews.Selection.SelectRow(visibleIndex);
                    }
                }
            }
        }

        protected void gvNeedReviews_OnPageIndexChanged(object sender, EventArgs e)
        {
            List<object> keys = gvNeedReviews.GetSelectedFieldValues("ModelNumber");
            SetSelectedModels(keys, gvNeedReviews.VisibleRowCount);
            LoadSelectedModels();
        }

        protected void btnConfigure_Click(object sender, EventArgs e)
        {
            List<object> keys = gvNeedReviews.GetSelectedFieldValues("ModelNumber");
            Boolean configureCheck = SetBatchConfigure(keys, gvNeedReviews.VisibleRowCount);
            if (configureCheck)
            {
                WebUtility.HttpRedirect(this, KeyDef.UrlList.CfgModelDetail_UK);
            }
            else
            {
                ltrConfigureMsg.Text = GetStaticResource("SelectModelMsg");
            }

        }

        protected void gvModelSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvModelSearchResults.PageIndex = e.NewPageIndex;
            BindMasterModel();
        }

        /// <summary>
        /// Search Master Model Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttModelSearch_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            try
            {
                BindMasterModel();
            }
            catch (Exception ex)
            {
                ltrItemMsg.Text = GetGlobalResourceObject("Common", " ErrorString") + ex.Message;
            }

        }

        /// <summary>
        /// Add new model item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttAddNewItemMN_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            try
            {
                AddNewItemConfig();
            }
            catch (Exception ex)
            {
                ltrItemMsg.Text = GetGlobalResourceObject("Common", " ErrorString") + ex.Message;
            }

        }

        private void BindMasterModel()
        {
            addNewItemDiv.Visible = false;
            gvModelSearchResults.DataSource = null;
            String keyword = txtModelSearchKeyword.Text.Trim();
            List<Lib.Cfg.ProductConfig> list = Lib.Cfg.ProductConfigBLL.FindByModelKeyword(keyword, false);
            List<Lib.Cfg.ProductConfig> newList = new List<Lib.Cfg.ProductConfig>();

            if (list.Count > 0)
            {
                foreach (Lib.Cfg.ProductConfig pcfg in list)
                {
                    Lib.Cfg.ModelConfig mcfg = Lib.Cfg.ModelConfigBLL.SelectByModelAndConfigType(pcfg.ModelNumber, Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
                    if (mcfg == null)
                    {
                        if (!newList.Contains(pcfg))
                            newList.Add(pcfg);
                    }
                }

                if (newList.Count > 0)
                {
                    gvModelSearchResults.DataSource = newList;

                }
                else
                {
                    ltrItemMsg.Text = GetStaticResource("MSG_AlreadyAdded");
                }
            }

            else
            {
                ltrItemMsg.Text = GetStaticResource("ModelNotFoundMsg");
            }
            gvModelSearchResults.DataBind();
        }

        private void ClearMessages()
        {
            ltrItemMsg.Text = String.Empty;
            ltrConfigureMsg.Text = String.Empty;
        }

        /// <summary>
        /// Add model to item config
        /// </summary>
        private void AddNewItemConfig()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            List<String> selectedModelsList = new List<String>();
            if (ltrModelNumber.Text.IsNullOrEmptyString())
            {
                ltrItemMsg.Text = GetStaticResource("ModelRequiredMsg");
                return;
            }
            String mn = ltrModelNumber.Text.Trim().ToUpperInvariant();
            Lib.Cfg.ModelConfig mcfg = Lib.Cfg.ModelConfigBLL.SelectByModelAndConfigType(ModelNumber, Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
            if (mcfg != null)
            {

                //ltrItemMsg.Text = GetStaticResource("ModelAlreadyAddedMsg");
                ltrItemMsg.Text = String.Format(GetStaticResource("MSG_AlreadyAdded")
                        , mn
                        , App_Lib.KeyDef.UrlList.CfgModelDetail_USMMD.Replace("~", "")
                        , App_Lib.KeyDef.QSKeys.ModelNumber
                        , HttpUtility.UrlEncode(mn));
                return;
            }
            Session.Remove(BatchChangeSessionKey);

            mcfg = new Lib.Cfg.ModelConfig();
            mcfg.ModelNumber = mn;
            mcfg.ModelConfigType = Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD;
            mcfg.HasSupportExpiration = cbxNewProdIsPaidSupportModel.Checked;
            mcfg.Verify_Registration = cbxVerifyProductSN.Checked;
            mcfg.UseUserACL = false;
            mcfg.Verify_Company = cbxNewProdVerifyCompany.Checked;
            mcfg.ModelConfigStatus = ModelConfigStatus.STATUS_ACTIVE;
            mcfg.Reg_InitialStatus = ProdReg_ItemStatus.active.ToString();
            mcfg.UseModelTags = false;
            if (!selectedModelsList.Contains(mn))
                selectedModelsList.Add(mn);
            Lib.Cfg.ModelConfigBLL.InsertUpdate(mcfg, String.Empty, user.Email);


            Session[BatchChangeSessionKey] = selectedModelsList;

            String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.CfgModelDetail_UK
                   , KeyDef.QSKeys.ModelNumber, HttpUtility.UrlEncode(ModelNumber));
            WebUtility.HttpRedirect(this, url);

        }

        public String GetUrl(object model)
        {
            String mn = model.ToString().ToUpperInvariant();
            String url = String.Format("/prodregadmin/modelconfig_uk?mn={0}"
                         , HttpUtility.UrlEncode(mn));

            return url;
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig_UK"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion

        public override string BatchChangeSessionKey
        {
            get { return "ss_SelectedReviewProds_UK"; }
        }

        public override string SelectedProductCountSessionKey
        {
            get { return "ss_SelectedProdCountUK"; }
        }
    }
}