﻿using System;
using System.Collections.Generic;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public class BC_ProdRegAdminBase : System.Web.UI.UserControl
    {
        public Guid ProdRegMaster_WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        public bool IsProdRegRegionalAdmin(string modelConfigType)
        {
            bool isRegionalAdmin = false;
            switch (modelConfigType)
            {
                case "downloads-us":
                    isRegionalAdmin = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_USMMD);
                    break;
                case "downloads-dk":
                    isRegionalAdmin = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_DKSW);
                    break;
                case "downloads-jp":
                    isRegionalAdmin = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPM);
                    break;
                case "downloads-jp-an":
                    isRegionalAdmin = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPAN);
                    break;
                case "downloads-uk":
                    isRegionalAdmin = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_ESD);
                    break;
                default:
                    isRegionalAdmin = false;
                    break;
            }
            return isRegionalAdmin;
        }

        public string GetProdRegAdminTypeDisplayText(string modelConfigType)
        {
            return GetGlobalResourceObject("ItemType", modelConfigType).ToString().
                Replace(GetGlobalResourceObject("ItemType", "Replacement_Text_For_ProdReg_AdminType_DisplayText").ToString(), "");
        }

        public void NotifyAdminOnProdRegDelettion(string deletedByRegionalAdmin, List<string> distributionListKeyLst, string companyName, string teamName, 
            List<string> regionalAdminsLst)
        {
            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 gwCultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            List<EM.EmailWebRef.ReplaceKeyValueType> adminEmailData = new List<EM.EmailWebRef.ReplaceKeyValueType>();

            Lib.ProductRegistration.ProdReg_Master prm = 
                Lib.ProductRegistration.ProdReg_MasterBLL.SelectByWebToken(ProdRegMaster_WebToken);
            if (prm == null) return;

            EM.EmailWebRef.ReplaceKeyValueType kv;
            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[REGIONAL_ADMIN]]",
                ReplacementValue = deletedByRegionalAdmin.ToUpperInvariant()
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[ITEMS_COUNT]]",
                ReplacementValue = regionalAdminsLst.Count.ToString()
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[REGIONAL_ADMINS]]",
                ReplacementValue = String.Join(",", regionalAdminsLst)
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[ModelNumber]]",
                ReplacementValue = prm.MasterModel.ToUpperInvariant()
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[SerialNumber]]",
                ReplacementValue = prm.MasterSerial.ToUpperInvariant()
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[EmailAddress]]",
                ReplacementValue = prm.CreatedBy
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[CompanyName]]",
                ReplacementValue = companyName
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[TeamName]]",
                ReplacementValue = teamName
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[DeletedDate]]",
                ReplacementValue = DateTime.UtcNow.ToString("MM/dd/yyyy")
            };
            adminEmailData.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType()
            {
                TemplateKey = "[[DeletedBy]]",
                ReplacementValue = user.Email
            };
            adminEmailData.Add(kv);

            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest reqDeleteRegAdminNotifyEmail =
            new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest()
            {
                EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ProdReg.Deletion.NotifyAdmin"),
                CultureGroupId = gwCultureGroupId,
                SubjectReplacementKeyValues = adminEmailData.ToArray(),
                BodyReplacementKeyValues = adminEmailData.ToArray()
            };
            foreach (string distributionListKey in distributionListKeyLst)
            {
                reqDeleteRegAdminNotifyEmail.DistributionListKey = distributionListKey;
                var respcode = EM.EmailServiceManager.SendDistributedTemplatedEmail(reqDeleteRegAdminNotifyEmail);
            }
        }

    }
}