﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_AddNewCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_AddNewCtrl" %>
<style type="text/css">
    .auto-style1 {
        color: #FF3300;
    }
</style>
<anrui:GlobalWebBoxedPanel ID="gwpnlSearchOrg" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gwpnlSearchOrg.HeaderText %>' DefaultButton="bttAccountIDSearch">
    <div class="settingrow">
        <asp:Literal ID="ltrCreateRegTitle" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,ltrCreateRegTitle%>"></asp:Literal>
    </div>
    <div class="settingrow group input-text width-60">
         <label>
          <asp:Localize ID="lcalAccountIDSearchKeywords" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalAccountIDSearchKeywords.Text%>"></asp:Localize>:</label> 
        <asp:TextBox ID="txtAccountIDSearchKeywords" runat="server" ></asp:TextBox>
        
    </div>
    <div class="margin-top-15">
        <asp:Button ID="bttAccountIDSearch" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,bttAccountIDSearch.Text%>" CausesValidation="false" OnClick="bttAccountIDSearch_Click" />
    </div>
    <div class="settingrow margin-top-15">
        <asp:GridView ID="gvAccountSearchResults" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvAccountSearchResults_PageIndexChanging" EmptyDataText="<%$ Resources:common,EmptyGridText %>" EmptyDataRowStyle-ForeColor="#f5a623" Width="100%">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="AccountID" HeaderText="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,CompanyName.Text%>" DataTextField="CompanyName"  DataNavigateUrlFormatString="/siteadmin/orguseradmin/orgdetail_users?accid={0}" ItemStyle-CssClass="gvstyle1-item-lft" />
                <asp:BoundField DataField="AccountName" HeaderText="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,AccountName.Text%>" ItemStyle-Width="280px" ItemStyle-CssClass="gvstyle1-item-lft" />
                <asp:BoundField DataField="AccountStatusCode" HeaderText="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,AccountStatusCode.Text%>" />
                <asp:HyperLinkField DataNavigateUrlFields="AccountID" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,Select.Text%>" DataNavigateUrlFormatString="/prodregadmin/manage_registration_addnew?accid={0}" />
            </Columns>
        </asp:GridView>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="gwpnlModelSearch" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gwpnlModelSearch.HeaderText %>' DefaultButton="bttModelSearch">
    <div class="settingrow">
        <asp:Literal ID="ltrModelSearchText" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,ltrModelSearchText.Text%>"></asp:Literal>
    </div>
    <div class="settingrow group input-text required width-60">
        <label>
            <asp:Localize ID="lcalModelSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalModelSearchKeyword.Text%>"></asp:Localize>:</label>
        <p class="error-message">
            <asp:Localize ID="lclMdlSrchKeyErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
        </p>
        <asp:TextBox ID="txtModelSearchKeyword" runat="server" ValidationGroup="vgMnSearchGrp"></asp:TextBox>
        <%--<span class="auto-style1">*</span>--%>
        <%--<asp:RequiredFieldValidator ID="rfvModel" runat="server" ControlToValidate="txtModelSearchKeyword" ValidationGroup="vgMnSearchGrp" Display="Dynamic" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
        
    </div>
    <div class="group margin-top-15">
        <asp:Button ID="bttModelSearch" runat="server" SkinID="submit-btn" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,bttModelSearch.Text%>" CausesValidation="true" ValidationGroup="vgMnSearchGrp" OnClick="bttModelSearch_Click" />
    </div>
    <div class="settingrow">
        <asp:GridView ID="gvModelSearchResults" runat="server" AutoGenerateColumns="false" AllowPaging="false" Width="100%">
            <Columns>
                <asp:BoundField DataField="ModelNumber" HeaderText="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gvModelSearchResults.ModelNumber%>" ItemStyle-Width="350px" ItemStyle-CssClass="gvstyle1-item-lft" />
                <asp:HyperLinkField DataNavigateUrlFields="AccountID,ModelNumber" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gvModelSearchResults.Register%>" DataNavigateUrlFormatString="/prodregadmin/manage_registration_addnew?accid={0}&mn={1}" />
            </Columns>
        </asp:GridView>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl,pnlContainer.HeaderText %>' Visible="false">
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalAccountID" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalAccountID.Text%>"></asp:Localize>:</span>
        <span style="line-height: 30px; padding: 5px"><asp:Literal ID="ltrAccount" runat="server"></asp:Literal></span>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalMasterMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalMasterMN.Text%>"></asp:Localize>:</span>
        <span style="line-height: 30px; padding: 5px"><asp:Literal ID="ltrModelNumber" runat="server"></asp:Literal></span>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalRegType" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalRegType.Text%>"></asp:Localize>:</span>
        <asp:RadioButtonList ID="rdoRegTypeList" runat="server"></asp:RadioButtonList>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalMasterSN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,lcalMasterSN.Text%>"></asp:Localize>:</span>
        <asp:TextBox ID="txtMasterSN" runat="server" SkinID="tbx-300"></asp:TextBox><span class="auto-style1">*</span>
        <asp:RequiredFieldValidator ID="rfvMasterSN" runat="server" ControlToValidate="txtMasterSN" ValidationGroup="vgProdReg_AddNew" Display="Dynamic" ErrorMessage="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,rfvMasterSN.ErrorMessage%>"></asp:RequiredFieldValidator>
    </div>
     <p class="msg">
            <asp:Label ID="ltrProdReg_AddNewMsg" runat="server"></asp:Label>

        </p>
    <div class="settingrow overflow">
       
        <div class="inline-block margin-top-15">
            <asp:Button ID="bttProdReg_AddNew" tyle="" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,bttProdReg_AddNew.Text%>" ValidationGroup="vgProdReg_AddNew" OnClick="bttProdReg_AddNew_Click" />
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_AddNewCtrl" />
 