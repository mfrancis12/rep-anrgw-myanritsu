﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;



namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public class Admin_ProdReg_ModelSearchResult
    {
        public String ModelNumber { get; set; }
        public Guid AccountID { get; set; }
    }

    public partial class ProdReg_AddNewCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected Guid AccountIDToRegister
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.AccountID], Guid.Empty);
            }
        }

        protected String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[App_Lib.KeyDef.QSKeys.ModelNumber]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AccountIDToRegister.IsNullOrEmptyGuid())
                {
                    gwpnlSearchOrg.Visible = true;
                    gwpnlModelSearch.Visible = false;
                    pnlContainer.Visible = false;
                }
                else if (ModelNumber.IsNullOrEmptyString())
                {
                    gwpnlSearchOrg.Visible = false;
                    gwpnlModelSearch.Visible = true;
                    pnlContainer.Visible = false;
                }
                else
                {
                    gwpnlSearchOrg.Visible = false;
                    gwpnlModelSearch.Visible = false;
                    pnlContainer.Visible = true;
                    InitOrgInfo();
                }
            }
        }

        private void InitOrgInfo()
        {
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(AccountIDToRegister);
            if (acc == null) Response.Redirect(App_Lib.KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_RegistrationAddNew);

            Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(ModelNumber);
            if(!pcfg.Reg_ValidateSN)
            ViewState["Reg_SNPattern"] = pcfg.Reg_SNPattern;
            if (pcfg == null)
            {
                String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_RegistrationAddNew,
                    KeyDef.QSKeys.AccountID, AccountIDToRegister.ToString());
                Response.Redirect(url);
            }
            ltrAccount.Text = String.Format("{0} [{1}] - {2}", acc.CompanyName, acc.CompanyCountryCode, acc.AccountName);
            ltrModelNumber.Text = ModelNumber;
            
            rdoRegTypeList.Items.Clear();
            foreach (Lib.Cfg.ModelConfig mcfg in pcfg.ModelConfigs)
            {
                Lib.Cfg.ModelConfigTypeInfo mcfginfo = ProductConfigUtility.ModelConfigType_GetInfo(mcfg.ModelConfigType);
                ListItem li = new ListItem(mcfginfo.ModelConfigTypeDisplayText, mcfginfo.ModelConfigType);
                rdoRegTypeList.Items.Add(li);
            }
            rdoRegTypeList.SelectedIndex = 0;
        }

        protected void bttAccountIDSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void bttProdReg_AddNew_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            try
            {
                rfvMasterSN.ErrorMessage = string.Empty;
                AddNewProductRegistrationMaster();
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == -2146232060)
                {
                    ltrProdReg_AddNewMsg.Text =  GetStaticResource("ErrorAlreadyExist");
                }
            }
        }

        private void AddNewProductRegistrationMaster()
        {
                if (!LoginUtility.IsAdminRole(CheckRoleToAdd(rdoRegTypeList.SelectedValue)))
                {
                    ltrProdReg_AddNewMsg.Text =  GetStaticResource("NoPermissionMsg");
                    return;
                }
          
                CheckRoleToAdd(rdoRegTypeList.SelectedValue);
                Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(ModelNumber);
                //if (!LoginUtility.IsProdConfigAdminRole()) throw new HttpException(404, "Page not found.");

                if (pcfg == null)
                {
                    String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_RegistrationAddNew,
                        KeyDef.QSKeys.AccountID, AccountIDToRegister.ToString());
                    Response.Redirect(url);
                }

                Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(AccountIDToRegister);
                if (acc == null) Response.Redirect(App_Lib.KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_RegistrationAddNew);

                String sn = txtMasterSN.Text.Trim();
                if (sn.IsNullOrEmptyString())
                {
                    ltrProdReg_AddNewMsg.Text =  GetStaticResource("SNRequiredMsg");
                    rfvMasterSN.IsValid = false;
                    return;
                }

                Boolean validateSN = true;
                Int32 regActiveCount = 0;
                Dictionary<String, String> addOnData = null;
                Guid webToken = Guid.Empty;
                //Check for TAU evluation SNo
                validateSN = CheckTAUModelsEvaluationSNo(rdoRegTypeList.SelectedValue, sn);
                Int32 prodReg = Lib.ProductRegistration.ProdReg_MasterBLL.Insert(acc.AccountID,
                    pcfg.ModelNumber.ToUpperInvariant(),
                    sn,
                    LoginUtility.GetCurrentUser(true).Email,
                    addOnData, validateSN, out webToken, out regActiveCount,
                    Convert.ToString(ViewState["Reg_SNPattern"]));

                if (prodReg == -2) //invalid sn
                {
                    ltrProdReg_AddNewMsg.Text =  GetStaticResource("SNInvalidMsg");
                    rfvMasterSN.IsValid = false;
                    return;
                }
                else if (prodReg < 1)
                {
                    ltrProdReg_AddNewMsg.Text =  GetStaticResource("AddRegErrorMsg");
                    rfvMasterSN.IsValid = false;
                    return;
                }
                Session.Clear();
                Response.Redirect(String.Format("{0}?{1}={2}", App_Lib.KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_Registration
                    , App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey, webToken));
          
        }

        private bool CheckTAUModelsEvaluationSNo(String modelConfigType, String serialNumber)
        {
            if(!modelConfigType.Equals("downloads-uk",StringComparison.InvariantCultureIgnoreCase))
                return true;
            else 
            {
                //Check for UK-TAU ProdReg Role
                if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_UKTAU) && serialNumber.Equals(ConfigUtility.AppSettingGetValue("Connect.Web.TAUProdReg.EvalSerialNo")))
                    return false;
            }
            return true;
        }


        private String CheckRoleToAdd(String ItemType)
        {
            String role = String.Empty;
            switch (ItemType)
            {
                case "downloads-us":
                    role = KeyDef.UserRoles.Admin_ProductReg_USMMD;
                    break;
                case "downloads-jp":
                    role = KeyDef.UserRoles.Admin_ProductReg_JPM;
                    break;
                case "downloads-jp-an":
                    role = KeyDef.UserRoles.Admin_ProductReg_JPAN;
                    break;
                case "downloads-dk":
                    role = KeyDef.UserRoles.Admin_ProductReg_DKSW;
                    break;
                case "downloads-uk":
                    role = KeyDef.UserRoles.Admin_ProductReg_ESD;
                    break;
             }
            return role;
        }

        protected void gvAccountSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAccountSearchResults.PageIndex = e.NewPageIndex;
            BindData();
        }

        private void BindData()
        {
            String keyword = txtAccountIDSearchKeywords.Text.Trim();
            List<Lib.SearchByOption> searchOptions = new List<Lib.SearchByOption>();
            searchOptions.Add(new Lib.SearchByOption("OrgName", keyword));
            searchOptions.Add(new Lib.SearchByOption("CompanyName", keyword));
            //searchOptions.Add(new Lib.SearchByOption("AccountID", keyword));

            Admin_SearchUtility.OrgSearchOptionsSet(searchOptions);
            gvAccountSearchResults.DataSource = Lib.BLL.BLLAcc_AccountMaster.SelectAllFiltered(searchOptions);
            gvAccountSearchResults.DataBind();

        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_AddNewCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void bttModelSearch_Click(object sender, EventArgs e)
        {
            String keyword = txtModelSearchKeyword.Text.Trim();
            List<Lib.Cfg.ProductConfig> list = Lib.Cfg.ProductConfigBLL.FindByModelKeyword(keyword, false);
            Admin_ProdReg_ModelSearchResult[] msr = (from pcfg in list
                                                    where pcfg.ModelConfigs != null && pcfg.ModelConfigs.Count > 0
                          select new Admin_ProdReg_ModelSearchResult() 
                          {
                              AccountID = this.AccountIDToRegister,
                              ModelNumber = pcfg.ModelNumber
                          }).ToArray();
            gvModelSearchResults.DataSource = msr;
            gvModelSearchResults.DataBind();
        }
    }
}