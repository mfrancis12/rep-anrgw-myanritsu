﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_AddNewCtrl_JP.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_AddNewCtrl_JP" %>
<style type="text/css">
    .auto-style1 {
        color: #FF3300;
    }
    .remove_field {
        /*background-color: #1F9C7D*/
    }
</style>

<anrui:GlobalWebBoxedPanel ID="gwpnlSearchKeywordInputArea" runat="server" ShowHeader="true" HeaderText='<%$ Resources:~/ProdRegAdmin/Manage_Registration_AddNew_JP,PageTitle%>' DefaultButton="bttModelSearch">
    <div class="settingrow">
        <asp:Literal ID="ltrModelSerialEmailSearchText" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclDescrMsg%>"></asp:Literal>
    </div>
    <div class="input-text">
        <!-- Model -->
        <div class="settingrow group">
            <div class="input_field_model">
                <table>
                    <tr>
                        <td Style="text-align:left;vertical-align:top">
                            <label>
                                <asp:Localize ID="lcalModelSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclModelName%>"></asp:Localize>
                            </label>
                        </td>
                        <td Style="text-align:left;vertical-align:top">
                            <span class="msg">
                                <asp:Localize ID="lclMdlSrchKeyErr" runat="server" />
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="modelArea">
                    <input name="txtModels" id="txtModels" type="text" Style=" width:300px">
                </div>
            </div>
        </div>
</div>

        <!-- Serial -->
    <div class="input-text">
        <div class="settingrow group">
            <div class="input_field_serial">
                <table>
                    <tr>
                        <td Style="text-align:left;vertical-align:top">
                            <label>
                                <asp:Localize ID="lcalSerialSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclSerial%>"></asp:Localize>
                            </label>
                        </td>
                        <td Style="text-align:left;vertical-align:top">
                            <span class="msg">
                                <asp:Localize ID="lclSralSrchKeyErr" runat="server"></asp:Localize>
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="serialArea">
                    <input name="txtSerials" id="txtSerials" type="text" Style="width:300px">
                </div>
            </div>
        </div>
        </div>

        <!-- Email -->
    <div class="input-text">
        <div class="settingrow group">
            <div class="input_field_email">
                <table>
                    <tr>
                        <td Style="text-align:left;vertical-align:top">
                            <label>
                                <asp:Localize ID="lcalEmailSearchKeyword" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclEmail%>"></asp:Localize>
                            </label>
                        </td>
                        <td Style="text-align:left;vertical-align:top">
                            <span class="msg">
                                <asp:Localize ID="lclEmailSrchKeyErr" runat="server" />
                            </span>
                        </td>
                    </tr>
                </table>

                <div class="emailArea">
                    <input name="txtEmails" id="txtEmails" type="text" Style="width:300px">
                </div>
            </div>
        </div>
        </div>

    <div class="group margin-top-15">
        <span class="msg">
            <asp:Localize ID="lclErroMessageForSearch" runat="server"></asp:Localize>
        </span>
        <asp:Button ID="bttSearch" runat="server" SkinID="submit-btn" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,bttNext%>" CausesValidation="true" ValidationGroup="vgMnSearchGrp" OnClick="bttSearch_Click" />
        <asp:Literal ID="litSearch" runat="server"></asp:Literal>
        <asp:Literal ID="litInitForm" runat="server"></asp:Literal>
        <asp:Literal ID="litAddKeyPressEvent" runat="server"></asp:Literal>
    </div>

</anrui:GlobalWebBoxedPanel>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl,pnlContainer.HeaderText %>' Visible="false">
    <div class="settingrow margin-top-15" style="overflow: auto">
        <asp:GridView ID="gvSearchResults" runat="server" AutoGenerateColumns="false" Width="1500" OnRowDataBound="gvSearchResults_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText=''>
                    <ItemTemplate>
                        <asp:CheckBox ID="cbxSelectItem" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsItemSelected") %>' Enabled='<%# DataBinder.Eval(Container.DataItem, "IsEnable") %>' />
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblModelNumber %>'>
                    <ItemTemplate>
                        <asp:Label ID="lblModelNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ModelNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblSerial %>'>
                    <ItemTemplate>
                        <asp:Label ID="lblSerialNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblEmail %>'>
                    <ItemStyle Width="200" />
                    <ItemTemplate>
                        <asp:Label ID="lblEMailAddress" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EMailAddress") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblTeam %>'>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlTeam" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblMG %>' >
                    <ItemStyle Width="70px"/>
                    <ItemTemplate>
                        <div >
                            <asp:Label runat="server" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblDateOnOff %>' Style="width:70px"></asp:Label>
                            <asp:CheckBox ID="cbxMG" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsMGChecked") %>'/>
                        </div>
                        <asp:TextBox ID="deMG" runat="server" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "MGDate") %>'></asp:TextBox>
                        <%--<dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" AllowUserInput="true" Width="100" AllowNull="true" ClientIDMode="AutoID" CssClass="date-expire" Date='<%# DataBinder.Eval(Container.DataItem, "MGDate") %>'></dx:ASPxDateEdit>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblMH %>' >
                    <ItemStyle Width="70px"/>
                    <ItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblDateOnOff %>' Style="width:70px"></dx:ASPxLabel>
                        <asp:CheckBox ID="cbxMH" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsMHChecked") %>' />
                        <br />
                        <asp:TextBox ID="deMH" runat="server" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "MHDate") %>'></asp:TextBox>
                        <%--<dx:ASPxDateEdit ID="deMH" runat="server" AllowUserInput="true" Width="100" AllowNull="true" ClientIDMode="AutoID" CssClass="date-expire" Date='<%# DataBinder.Eval(Container.DataItem, "MHDate") %>'></dx:ASPxDateEdit>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblUG %>' >
                    <ItemStyle Width="70px"/>
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblDateOnOff %>' Style="width:70px"></asp:Label>
                        <asp:CheckBox ID="cbxUG" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsUGChecked") %>' />
                        <br />
                        <asp:TextBox ID="deUG" runat="server" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "UGDate") %>'></asp:TextBox>
                        <%--<dx:ASPxDateEdit ID="deUG" runat="server" AllowUserInput="true" Width="100" AllowNull="true" ClientIDMode="AutoID" CssClass="date-expire" Date='<%# DataBinder.Eval(Container.DataItem, "UGDate") %>'></dx:ASPxDateEdit>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblUH %>' >
                    <ItemStyle Width="70px"/>
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblDateOnOff %>' Style="width:70px"></asp:Label>
                        <asp:CheckBox ID="cbxUH" runat="server"  Checked='<%# DataBinder.Eval(Container.DataItem, "IsUHChecked") %>' />
                        <br />
                        <asp:TextBox ID="deUH" runat="server" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "UHDate") %>'></asp:TextBox>
                        <%--<dx:ASPxDateEdit ID="deUH" runat="server" AllowUserInput="true" Width="100" AllowNull="true" ClientIDMode="AutoID" CssClass="date-expire" Date='<%# DataBinder.Eval(Container.DataItem, "UHDate") %>'></dx:ASPxDateEdit>--%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblMT %>'>
                    <ItemStyle Width="100px"/>
                    <ItemTemplate>
                        <asp:CheckBox ID="cbxMT" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "ModelTag") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, lblSC %>'>
                    <ItemStyle Width="70px"/>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlStatusCode" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtMemberShipId" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "MemberShipId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtProdRegItemId" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "ProdRegItemId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtProdRegId" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "ProdRegId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtStatusCode" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "StatusCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtFoundTeamName" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "FoundTeamName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtFoundTeamId" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "FoundTeamId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText='' ShowHeader="false" Visible="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="txtAccountId" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "AccountId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <div class="group margin-top-15">
            <span class="msg">
                <asp:Localize ID="lblErrorMsg" runat="server"></asp:Localize>
            </span>
            <br />
            <asp:Button ID="bttModify" runat="server" SkinID="submit-btn" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, bttModify %>' OnClick="bttModify_Click" Visible="false" UseSubmitBehavior="false"/>
            <asp:Button ID="bttDelete" runat="server" SkinID="submit-btn" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, bttDelete %>' OnClick="bttDelete_Click" Visible="false" UseSubmitBehavior="false"/>
            <asp:Button ID="bttOK" runat="server" SkinID="submit-btn" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, bttOK %>' OnClick="bttOK_Click" Visible="false" UseSubmitBehavior="false"/>
            <asp:Button ID="bttUpdate" runat="server" SkinID="submit-btn" Text='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP, bttOK %>' OnClick="bttUpdate_Click" Visible="false" UseSubmitBehavior="false"/>
        </div>

    </div>
</anrui:GlobalWebBoxedPanel>
