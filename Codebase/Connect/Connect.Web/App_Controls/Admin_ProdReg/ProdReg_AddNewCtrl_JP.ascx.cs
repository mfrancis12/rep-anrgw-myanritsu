﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Data;
using Anritsu.Connect.Lib.ActivityLog;
using System.Text.RegularExpressions;
using System.Data;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.BLL;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_AddNewCtrl_JP : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        #region Inner Class
        /// <summary>
        /// Data Class for DataSource of Grid.
        /// </summary>
        private class SearchResultGridData
        {
            public bool IsEnable { get; set; }
            public bool IsItemSelected { get; set; }
            public String ModelNumber { get; set; }
            public String SerialNumber { get; set; }
            public String EMailAddress { get; set; }
            //public List<ListItem> Team { get; set; }
            public bool IsMGChecked { get; set; }
            public String MGDate { get; set; }
            public bool IsMHChecked { get; set; }
            public String MHDate { get; set; }
            public bool IsUGChecked { get; set; }
            public String UGDate { get; set; }
            public bool IsUHChecked { get; set; }
            public String UHDate { get; set; }
            public bool ModelTag { get; set; }
            public ProdReg_ItemStatus StatusCode { get; set; }
            public Guid MemberShipId { get; set; }
            public String ProdRegItemId { get; set; }
            public String ProdRegId { get; set; }
            public String FoundTeamName { get; set; }
            public String FoundTeamId { get; set; }
            public Guid AccountID { get; set; }
        }

        /// <summary>
        /// Data class to getting grid for Register/Update/Delete Product/SupportType/ModelTag/StatusCode datas.
        /// </summary>
        private class UserModifiedGridData
        {
            public bool IsEnable { get; set; }
            public bool IsItemSelected { get; set; }
            public String ModelNumber { get; set; }
            public String SerialNumber { get; set; }
            public String EMailAddress { get; set; }
            public ListItem Team { get; set; }
            public bool IsMGChecked { get; set; }
            public String MGDate { get; set; }
            public bool IsMHChecked { get; set; }
            public String MHDate { get; set; }
            public bool IsUGChecked { get; set; }
            public String UGDate { get; set; }
            public bool IsUHChecked { get; set; }
            public String UHDate { get; set; }
            public bool ModelTag { get; set; }
            public ListItem DSStatusCode { get; set; }
            public Guid MemberShipId { get; set; }
            public String ProdRegId { get; set; }
            public String ProdRegItemId { get; set; }
            public String FoundTeamName { get; set; }
            public String FoundTeamId { get; set; }
            public Guid AccountId { get; set; }
        }


        #endregion

        #region Enum
        public enum RegisterProcessStatus
        {
            None = 0,
            InputModelSerialEmail = 1,
            InputSupportType = 2,
            ModifyModelSerialEmail = 3,
            ModifySupportType = 4,
        }
        #endregion

        #region Property
        protected Guid AccountIDToRegister
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.AccountID], Guid.Empty);
            }
        }

        protected RegisterProcessStatus ProductsModifyMode
        {
            get
            {
                return (RegisterProcessStatus)ConvertUtility.ConvertToInt32(
                                                    Request.QueryString[App_Lib.KeyDef.QSKeys.ProductsModifyMode],
                                                    (int)RegisterProcessStatus.None);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_AddNewCtrl_JP"; }
        }

        private static String LoggedInUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        #endregion

        #region Field
        private const String AreaEffected = "Manage Support Types JP";
        #endregion

        public String GetStaticResource(String resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Bind KeyPress Event.
            litAddKeyPressEvent.Text = AddKeyPressEventByJavaScript();
            lblErrorMsg.Visible = false;

            if (!IsPostBack)
            {
                if (ViewState["Status"] == null)
                {
                    ViewState["Status"] = RegisterProcessStatus.InputModelSerialEmail;
                }
            }

            if (ProductsModifyMode == RegisterProcessStatus.ModifySupportType && ProdRegMaster_WebToken != Guid.Empty)
            {
                ViewState["Status"] = RegisterProcessStatus.ModifySupportType;
                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;

                bttModify.Visible = false;
                bttOK.Visible = false;
                bttDelete.Visible = true;
                bttUpdate.Visible = true;

                if (!IsPostBack)
                {
                    // Modify SupportType/ModelTag/StatusCode mode.
                    BindProductDatasForModifySupportType();
                }
            }
            else
            {
                gwpnlSearchKeywordInputArea.Visible = true;
                pnlContainer.Visible = false;
                litInitForm.Text = ReproductionDynamicTextBox();
            }

        }

        protected void gvSearchResults_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblModel = e.Row.Cells[1].FindControl("lblModelNumber") as Label;
                var lblSerial = e.Row.Cells[2].FindControl("lblSerialNumber") as Label;
                var lblEmail = e.Row.Cells[3].FindControl("lblEMailAddress") as Label;
                var ddTeam = e.Row.Cells[4].FindControl("ddlTeam") as DropDownList;
                var ddStatus = e.Row.Cells[10].FindControl("ddlStatusCode") as DropDownList;
                var cbxModelTag = e.Row.Cells[9].FindControl("cbxMT") as CheckBox;
                var lblMemberShipId = e.Row.Cells[11].FindControl("txtMemberShipId") as Label;
                var lblProdRegItemId = e.Row.Cells[12].FindControl("txtProdRegItemId") as Label;
                var lblProdRegId = e.Row.Cells[13].FindControl("txtProdRegId") as Label;
                var txtStatusCode = e.Row.Cells[14].FindControl("txtStatusCode") as Label;
                var lblFoundTeamName = e.Row.Cells[15].FindControl("txtFoundTeamName") as Label;
                var lblFoundTeamId = e.Row.Cells[16].FindControl("txtFoundTeamId") as Label;
                var lblAccountId = e.Row.Cells[17].FindControl("txtAccountId") as Label;

                if (lblModel == null || lblSerial == null || lblEmail == null ||
                        ddTeam == null || ddStatus == null || lblMemberShipId == null ||
                            lblProdRegItemId == null || lblProdRegId == null || txtStatusCode == null ||
                            lblFoundTeamName == null || lblFoundTeamId == null || lblAccountId == null)
                    return;

                var prodRegItemId = lblProdRegItemId.Text.Trim();
                var prodRegId = lblProdRegId.Text.Trim();
                var memberShipId = lblMemberShipId.Text.Trim();
                var mode = (RegisterProcessStatus)Enum.Parse(typeof(RegisterProcessStatus), ViewState["Status"].ToString());

                // Input SupportType mode
                if (mode == RegisterProcessStatus.InputSupportType)
                {
                    // Is product already exist?
                    if (!String.IsNullOrEmpty(prodRegItemId) && !String.IsNullOrEmpty(prodRegId) && !String.IsNullOrEmpty(memberShipId))
                    {
                        // Get and Team,Status,ModelTag bind .
                        var prodRegItem = ProdReg_ItemBLL.SelectByProdRegItemId(int.Parse(lblProdRegItemId.Text));

                        // Team
                        var email = lblEmail.Text.Trim();
                        var teamDic = new Dictionary<String, Dictionary<Guid, String>>();
                        var memberShipIdDic = new Dictionary<String, Guid>();
                        CreateTeamAndMemberShipIDDictionary(email, ref teamDic, ref memberShipIdDic);
                        var teamSource = teamDic[email];
                        ddTeam.DataSource = teamSource;
                        ddTeam.DataTextField = "Value";
                        ddTeam.DataValueField = "Key";
                        ddTeam.DataBind();
                        for (int i = 0; i < teamSource.Count; i++)
                        {
                            var item = teamSource.ElementAt(i);
                            if (item.Key.ToString() == lblFoundTeamId.Text)
                            {
                                ddTeam.SelectedIndex = i;
                            }
                        }

                        // ModelTag
                        cbxModelTag.Checked = prodRegItem.InclResrc_TaggedMN;

                        // StatucCode
                        var statusCodeSource = GetStatusCodeListItem();
                        ddStatus.DataSource = statusCodeSource.ToArray();
                        ddStatus.DataTextField = "Key";
                        ddStatus.DataValueField = "Value";
                        ddStatus.DataBind();
                        for (int i = 0; i < statusCodeSource.Count; i++)
                        {
                            var item = statusCodeSource.ElementAt(i);
                            if (item.Value == prodRegItem.StatusCode.ToString())
                            {
                                ddStatus.SelectedIndex = i;
                            }
                        }
                    }
                    else
                    {
                        var email = lblEmail.Text.Trim();
                        var teamDic = new Dictionary<String, Dictionary<Guid, String>>();
                        var memberShipIdDic = new Dictionary<String, Guid>();
                        CreateTeamAndMemberShipIDDictionary(email, ref teamDic, ref memberShipIdDic);
                        ddTeam.DataSource = teamDic[email];
                        ddTeam.DataTextField = "Value";
                        ddTeam.DataValueField = "Key";
                        ddTeam.DataBind();

                        ddStatus.DataSource = GetStatusCodeListItem().ToArray();
                        ddStatus.DataTextField = "Key";
                        ddStatus.DataValueField = "Value";
                        ddStatus.DataBind();
                    }
                }
                else if (mode == RegisterProcessStatus.ModifySupportType)
                {
                    // modify SupportType mode
                    // For Team. Get Team Name.
                    var team = new Dictionary<String, String>();
                    var teamDic = new Dictionary<String, Dictionary<Guid, String>>();
                    var memberShipIdDic = new Dictionary<String, Guid>();
                    var email = lblEmail.Text;


                    CreateTeamAndMemberShipIDDictionary(email, ref teamDic, ref memberShipIdDic);
                    var accId = new Guid(lblAccountId.Text);
                    string compName = String.Empty;
                    if (teamDic.Count != 0 && teamDic[email].ContainsKey(accId))
                    {
                        compName = teamDic[email].ToList().Where(w => w.Key == accId).Select(s => s.Value).FirstOrDefault();
                    }
                    team.Add(compName, accId.ToString());

                    ddTeam.DataSource = team;
                    ddTeam.DataTextField = "Key";
                    ddTeam.DataValueField = "Value";
                    ddTeam.DataBind();

                    // bind it
                    var source = GetStatusCodeListItem();
                    ddStatus.DataSource = source.ToArray();
                    ddStatus.DataTextField = "Key";
                    ddStatus.DataValueField = "Value";
                    ddStatus.DataBind();

                    
                    // Cnahge SelectIndex by Statuc Code Value
                    for (int i = 0; i < source.Count; i++)
                    {
                        var item = source.ElementAt(i);
                        if (item.Value == txtStatusCode.Text)
                        {
                            ddStatus.SelectedIndex = i;
                        }
                    }
                }
            }
        }

        protected void bttSearch_Click(object sender, EventArgs e)
        {
            ViewState["Status"] = RegisterProcessStatus.InputSupportType;

            lclErroMessageForSearch.Visible = false;
            lclMdlSrchKeyErr.Visible = false;
            lclSralSrchKeyErr.Visible = false;
            lclEmailSrchKeyErr.Visible = false;

            if (HasPostdata())
            {
                // Verify Model/Serial/Email
                if (VerifyPostData())
                {
                    // Display Grid.
                    gwpnlSearchKeywordInputArea.Visible = false;
                    pnlContainer.Visible = true;

                    bttModify.Visible = true;
                    bttOK.Visible = true;

                    // Bind product data to Grid.
                    BindProductDatas();
                }
                else
                {
                    // Error.
                    gwpnlSearchKeywordInputArea.Visible = true;
                    pnlContainer.Visible = false;

                    // Reproduct dynamic Textbox of Model,Serial,Email.
                    litInitForm.Text = ReproductionDynamicTextBox();
                }
            }
        }

        protected void bttModify_Click(object sender, EventArgs e)
        {
            ViewState["Status"] = RegisterProcessStatus.ModifyModelSerialEmail;
            gwpnlSearchKeywordInputArea.Visible = true;
            pnlContainer.Visible = false;

            // Reproduction Dynamic Textbox.
            litInitForm.Text = ReproductionDynamicTextBox();
        }

        protected void bttOK_Click(object sender, EventArgs e)
        {
            // Register Product and SupportType
            GridViewRowCollection gridRows = gvSearchResults.Rows;
            bool updateSupportTypeResult = true, addSupportTypeResult = true, addProdResult = true;
            for (int i = 0; i < gridRows.Count; i++)
            {
                // Get current GridViewRow data.
                var row = gridRows[i];
                var rowData = GetUserModifiedGridData(row);

                if (!IsDateCorrect(rowData))
                {
                    lblErrorMsg.Text = GetStaticResource("lclInvalidSupportTypeDate");
                    lblErrorMsg.Visible = true;

                    gwpnlSearchKeywordInputArea.Visible = false;
                    pnlContainer.Visible = true;
                    bttModify.Visible = true;
                    bttOK.Visible = true;
                    return;
                }

                // Register Product
                if (!String.IsNullOrEmpty(rowData.FoundTeamName) &&
                    !String.IsNullOrEmpty(rowData.FoundTeamId) &&
                    rowData.Team.Value == rowData.FoundTeamId)
                {
                    // It is already exist.
                    // Update Product Data and Support Type
                    ProdReg_ItemBLL.Update(
                                int.Parse(rowData.ProdRegItemId),
                                rowData.ModelNumber,
                                rowData.SerialNumber,
                                ConvertStatusCodeFromListItem(rowData.DSStatusCode).ToString(),
                                rowData.ModelTag,
                                LoggedInUserEmail);
                }
                else
                {
                    // Is is new Product.
                    // Register new Product Data and Support Type
                    int errCode = 0;
                    var accountId = new Guid(rowData.Team.Value);

                    // Get ProdMaster data.
                    var prodRegMasterList = ProdReg_MasterBLL.SelectByAccMnSnToList(accountId, rowData.ModelNumber, rowData.SerialNumber);

                    // Get ProdRegItems data.
                    var prodRegitems = new List<ProdReg_Item>();
                    if (prodRegMasterList != null && prodRegMasterList.Count != 0)
                        prodRegitems = ProdReg_ItemBLL.SelectByWebToken_AsList(prodRegMasterList.FirstOrDefault().WebToken);


                    if (prodRegMasterList.Count == 0 || (prodRegMasterList.Count != 0 && prodRegitems.Count == 0))
                    {
                        // Register Product.
                        Guid webToken = Guid.Empty;
                        var tmpResult = RegisterProduct(accountId, rowData, out errCode, out webToken);
                        if (tmpResult == false && addProdResult)
                            addProdResult = false;

                        // If register sucess, update ModelTag and Status Code.
                        if (tmpResult && webToken != Guid.Empty)
                        {

                            var prodRegItems = ProdReg_ItemBLL.SelectByWebToken_AsList(webToken);
                            var JPProdItems = prodRegItems.Where(w => w.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP).Select(s => s);
                            if (JPProdItems != null && JPProdItems.Count() != 0)
                            {
                                foreach (var item in JPProdItems)
                                {
                                    ProdReg_ItemBLL.Update(item.ProdRegItemID,
                                                        rowData.ModelNumber,
                                                        rowData.SerialNumber,
                                                        ConvertStatusCodeFromListItem(rowData.DSStatusCode).ToString(),
                                                        rowData.ModelTag,
                                                        LoggedInUserEmail);
                                }
                            }
                        }
                    }
                }

                // Support Type
                var formValuesDictionary = new Dictionary<string, object>();
                if (IsExsitsSupportType(rowData, ref formValuesDictionary))
                {
                    var tmpResult = UpdateSupportTypeEntry(rowData, formValuesDictionary);

                    if (tmpResult == false && updateSupportTypeResult)
                        updateSupportTypeResult = false;
                }
                else
                {
                    var tmpResult = AddSupportTypeEntry(rowData);
                    if (tmpResult == false && addSupportTypeResult)
                        addSupportTypeResult = false;
                }

            }

            // add new product failed
            if (addProdResult == false)
            {
                lblErrorMsg.Text = GetStaticResource("lclFailRegiserProd");
                lblErrorMsg.Visible = true;
                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;
                bttModify.Visible = true;
                bttOK.Visible = true;
                return;
            }

            // update support type failed
            if (updateSupportTypeResult == false)
            {

                lblErrorMsg.Text = GetStaticResource("lclFailRegiserSupportType");
                lblErrorMsg.Visible = true;
                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;
                bttModify.Visible = true;
                bttOK.Visible = true;
                return;
            }

            // add support type failed
            if (addSupportTypeResult == false)
            {
                lblErrorMsg.Text = GetStaticResource("lclFailUpdateSupportType");
                lblErrorMsg.Visible = true;
                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;
                bttModify.Visible = true;
                bttOK.Visible = true;
                return;
            }

            // Done.
            WebUtility.HttpRedirect(null, "/siteadmin/home");
        }
        protected void bttDelete_Click(object sender, EventArgs e)
        {
            // Delete Product and SupportType
            GridViewRowCollection gridRows = gvSearchResults.Rows;
            GridViewRow row;

            for (int i = 0; i < gridRows.Count; i++)
            {
                // Get current GridViewRow data.
                row = gridRows[i];
                var rowData = GetUserModifiedGridData(row);

                if (rowData.IsItemSelected)
                {
                    // Delete - Product Item.
                    var masterProdRegItems = ProdReg_MasterBLL.SelectByAccMnSnToList(
                                new Guid(rowData.Team.Value), rowData.ModelNumber, rowData.SerialNumber);
                    foreach (var prodRegItem in masterProdRegItems)
                    {
                        Guid webToken = ProdRegMaster_WebToken;
                        var prodItemList = ProdReg_ItemBLL.SelectByWebToken_AsList(webToken);

                        if (prodItemList != null && prodItemList.Count != 0)
                        {
                            if (prodItemList.Where(w => w.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP).Select(s => s).Count() == 1)
                            {
                                // If type of ProdItem equal "downloads-jp" only, all ProdItem delete.
                                foreach (var item in prodItemList)
                                {
                                    ProdReg_ItemConfig_JPSWBLL.DeleteForJPSW(item.ProdRegItemID);
                                }

                            }
                            else
                            {
                                // If type of ProdItem have "downloads-jp" , delete selected ProdItem only.
                                if (rowData.ProdRegItemId.IsNullOrEmptyString() == false)
                                    ProdReg_ItemConfig_JPSWBLL.DeleteForJPSW(int.Parse(rowData.ProdRegItemId));
                            }

                        }

                    }
                }

            }

            ViewState["Status"] = null;
            WebUtility.HttpRedirect(null, "/siteadmin/home");
        }

        protected void bttUpdate_Click(object sender, EventArgs e)
        {
            // Update Product and SupportType
            GridViewRowCollection gridRows = gvSearchResults.Rows;
            GridViewRow row;
            bool updateResult = true;
            for (int i = 0; i < gridRows.Count; i++)
            {
                // Get current GridViewRow data.
                row = gridRows[i];
                var rowData = GetUserModifiedGridData(row);

                if (!IsDateCorrect(rowData))
                {
                    lblErrorMsg.Text = GetStaticResource("lclInvalidSupportTypeDate");
                    lblErrorMsg.Visible = true;

                    gwpnlSearchKeywordInputArea.Visible = false;
                    pnlContainer.Visible = true;
                    bttModify.Visible = false;
                    bttOK.Visible = false;
                    bttDelete.Visible = true;
                    bttUpdate.Visible = true;
                    return;
                }

                // Support Type
                var formValuesDictionary = new Dictionary<string, object>();
                if (IsExsitsSupportType(rowData, ref formValuesDictionary))
                {
                    // Update SupportType
                    var tmpResult = UpdateSupportTypeEntry(rowData, formValuesDictionary);
                    if (tmpResult == false && updateResult)
                        updateResult = false;
                }
                else
                {
                    // Insert SupportType
                    // This pattern is "EndUser Registered Products. TS admin is approve it and register SupportTypes."
                    var tmpResult = AddSupportTypeEntry(rowData);
                    if (tmpResult == false && updateResult)
                        updateResult = false;
                }

                // Update StatusCode and ModelTag
                var statusCode = ConvertStatusCodeFromListItem(rowData.DSStatusCode);
                ProdReg_ItemBLL.Update(
                                        int.Parse(rowData.ProdRegItemId),
                                        rowData.ModelNumber,
                                        rowData.SerialNumber,
                                        statusCode.ToString(),
                                        rowData.ModelTag,
                                        LoggedInUserEmail);
            }

            if (updateResult == false)
            {
                lblErrorMsg.Text = GetStaticResource("lblFailToUpdateSupportType");
                lblErrorMsg.Visible = true;
                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;
                bttModify.Visible = false;
                bttOK.Visible = false;
                bttDelete.Visible = true;
                bttUpdate.Visible = true;
                return;
            }
            WebUtility.HttpRedirect(null, "/siteadmin/home");
        }

        /// <summary>
        /// Does PostData has model,serial,email data?
        /// </summary>
        /// <returns></returns>
        private bool HasPostdata()
        {
            if (Request.Form.HasKeys())
            {
                if (Request.Form.AllKeys.Contains("txtModels") &&
                    Request.Form.AllKeys.Contains("txtSerials") &&
                    Request.Form.AllKeys.Contains("txtEmails") ||
                    (Request.Form.AllKeys.Contains("models[]") &&
                        Request.Form.AllKeys.Contains("serials[]") &&
                            Request.Form.AllKeys.Contains("emails[]")))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Verify PostData
        /// </summary>
        /// <returns></returns>
        private bool VerifyPostData()
        {
            bool ret = true;
            var br = "<br />";
            // Get inputted form value
            var models = new String[] { };
            var serials = new String[] { };
            var emails = new String[] { };
            GetFormValue(ref models, ref serials, ref emails);


            // Verify all Mode/Serial/Email are not Empty.
            if (models.Count() == 0 || serials.Count() == 0 || emails.Count() == 0)
            {
                // Error
                lclErroMessageForSearch.Text = GetStaticResource("lblErrorText_NotExists_JP") + br;
                lclErroMessageForSearch.Visible = true;
                ret = false;
                return ret;
            }

            // Verify Model and Serial is not many-to-many relationship.
            if (models.Count() != 1 && serials.Count() != 1)
            {
                lclErroMessageForSearch.Text = GetStaticResource("lblErrorText_ManyToMany_JP") + br;
                lclErroMessageForSearch.Visible = true;
                ret = false;
                return ret;
            }

            if (models.Contains("") || serials.Contains("") || emails.Contains(""))
            {
                lclErroMessageForSearch.Text = GetStaticResource("lclInputIsEmpty") + br;
                lclErroMessageForSearch.Visible = true;
                ret = false;
                return ret;
            }

            // Verify Inputted Model Data.
            var errModel = new List<string>();
            models.ToList().ForEach(m =>
            {
                if (Anritsu.Connect.Lib.Cfg.ProductConfigBLL.SelectByModelNumber(m) == null)
                {
                    errModel.Add(m);
                }
            });

            if (errModel.Count != 0)
            {
                // Show Error Message.
                string errMsg = string.Empty;
                errModel.ForEach(m =>
                {
                    errMsg += GetStaticResource("lclError");
                    errMsg += m;
                    errMsg += GetStaticResource("lclNotExsits") + br;
                });
                lclMdlSrchKeyErr.Text = errMsg;
                lclMdlSrchKeyErr.Visible = true;
                ret = false;
            }

            // Verify Inputted Serial Data.
            var errSerial = new List<string>();
            foreach (var model in models)
            {
                serials.ToList().ForEach(s =>
                   {
                       Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(model);
                       String regSNPattern = String.Empty;
                       if (pcfg != null && !pcfg.Reg_ValidateSN)
                           regSNPattern = pcfg.Reg_SNPattern;
                       bool validateSN = CheckTAUModelsEvaluationSNo("downloads-jp", s);
                       if (validateSN)
                       {
                           Boolean isInGlobalSerialNumberSystem = Data.DAProductInfo.IsValidMnSn(model, s, !validateSN);
                           Regex reg = new Regex(regSNPattern);
                           Match m = reg.Match(s);
                           if (!isInGlobalSerialNumberSystem && (!m.Success || string.IsNullOrEmpty(regSNPattern)))
                           {
                               if (!errSerial.Contains(s))
                                   errSerial.Add(s);
                           }
                       }
                   });
            };

            if (errSerial.Count != 0)
            {
                // Show Error Message.
                string errMsg = string.Empty;
                errSerial.ForEach(s =>
                {
                    errMsg += GetStaticResource("lclError");
                    errMsg += s;
                    errMsg += GetStaticResource("lclNotExsits") + br;
                });
                lclSralSrchKeyErr.Text = errMsg;
                lclSralSrchKeyErr.Visible = true;
                ret = false;
            }

            // Verify Inputted Email Data.
            var errEmail = new List<string>();
            emails.ToList().ForEach(e =>
               {
                   var option = new List<SearchByOption>();
                   option.Add(new SearchByOption("EmailAddress", e));
                   var user = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(e);
                   if (user == null)
                   {
                       errEmail.Add(e);
                   }
               });

            if (errEmail.Count != 0)
            {
                // Show Error Message.
                string errMsg = string.Empty;
                errEmail.ForEach(e =>
                {
                    errMsg += GetStaticResource("lclError");
                    errMsg += e;
                    errMsg += GetStaticResource("lclNotExsits") + br;
                });
                lclEmailSrchKeyErr.Text = errMsg;
                lclEmailSrchKeyErr.Visible = true;
                ret = false;
            }

            return ret;
        }

        private bool CheckTAUModelsEvaluationSNo(string modelConfigType, string serialNumber)
        {
            if (!modelConfigType.Equals("downloads-uk", StringComparison.InvariantCultureIgnoreCase))
                return true;
            else
            {
                //Check for UK-TAU ProdReg Role
                if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_UKTAU) && serialNumber.Equals(ConfigUtility.AppSettingGetValue("Connect.Web.TAUProdReg.EvalSerialNo")))
                    return false;
            }
            return true;
        }

        private bool IsDateCorrect(UserModifiedGridData rowData)
        {
            DateTime date;
            if (rowData.IsMGChecked && !DateTime.TryParse(rowData.MGDate, out date))
            {
                if (!String.IsNullOrEmpty(rowData.MGDate))
                    return false;
            }

            if (rowData.IsMHChecked && !DateTime.TryParse(rowData.MHDate, out date))
            {
                if (!String.IsNullOrEmpty(rowData.MHDate))
                    return false;
            }

            if (rowData.IsUGChecked && !DateTime.TryParse(rowData.UGDate, out date))
            {
                if (!String.IsNullOrEmpty(rowData.UGDate))
                    return false;
            }

            if (rowData.IsUHChecked && !DateTime.TryParse(rowData.UHDate, out date))
            {
                if (!String.IsNullOrEmpty(rowData.UHDate))
                    return false;
            }
            return true;
        }

        private void GetFormValue(ref string[] models, ref string[] serials, ref string[] emails)
        {
            // Model
            if (String.IsNullOrEmpty(Request.Form.GetValues("txtModels").FirstOrDefault()) == false)
            {
                // something inputted
                models = Request.Form.GetValues("txtModels");
            }

            if (Request.Form.AllKeys.Contains("models[]"))
            {
                models = models.Concat(Request.Form.GetValues("models[]")).ToArray();
            }

            // Serial
            if (String.IsNullOrEmpty(Request.Form.GetValues("txtSerials").FirstOrDefault()) == false)
            {
                // something inputted
                serials = Request.Form.GetValues("txtSerials");
            }

            if (Request.Form.AllKeys.Contains("serials[]"))
            {
                serials = serials.Concat(Request.Form.GetValues("serials[]")).ToArray();
            }

            // Email
            if (String.IsNullOrEmpty(Request.Form.GetValues("txtEmails").FirstOrDefault()) == false)
            {
                // something inputted
                emails = Request.Form.GetValues("txtEmails");
            }

            if (Request.Form.AllKeys.Contains("emails[]"))
            {
                emails = emails.Concat(Request.Form.GetValues("emails[]")).ToArray();
            }
        }

        /// <summary>
        /// Generate Grid Data to modifing SupportType/ModelTag/StatusCode
        /// </summary>
        private void BindProductDatasForModifySupportType()
        {
            // WebTokenから製品情報の取得(Get ProductMaster data by WebToken)
            Guid webToken = ProdRegMaster_WebToken;
            var prodItemList = ProdReg_ItemBLL.SelectByWebToken_AsList(webToken);
            var master = ProdReg_MasterBLL.SelectByWebToken(webToken);
            var displayEmailList = new List<String>();

            var gridDataList = new List<SearchResultGridData>();
            foreach (var prodItem in prodItemList)
            {
                if (prodItem.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP)
                {
                    // Nothing to do.
                }
                else
                {
                    continue;
                }

                // Get User Data belong to Team
                var userListOfTeam = BLLSec_UserMembership.SelectByAccountIdAsList(master.AccountID);
                foreach (var user in userListOfTeam)
                {
                    // Get SupportType
                    var supportTypes = BLL_SupportEntry.GetProductSupportListAsPerfectMatching(
                        prodItem.ModelNumber, prodItem.SerialNumber, user.Email) as IEnumerable<SupportTypeEntriesForJPAdmin>;

                    if (supportTypes == null)
                        continue;

                    foreach (var supportType in supportTypes)
                    {
                        // Model, Serial, Email, Team
                        var gridData = new SearchResultGridData();
                        gridData.IsEnable = true;
                        gridData.ModelNumber = prodItem.ModelNumber;
                        gridData.SerialNumber = prodItem.SerialNumber;
                        gridData.EMailAddress = user.Email;

                        // BackUp who(Email) are displayed.
                        if (!displayEmailList.Contains(user.Email))
                            displayEmailList.Add(user.Email);

                        // MemberShipId
                        var membership = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(user.Email);
                        if (membership != null)
                            gridData.MemberShipId = membership.MembershipId;

                        // Support Type
                        var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(supportType.Id)));
                        foreach (SupportEntryMapping type in mappings)
                        {
                            var dateStr = String.Empty;
                            if (type.ExpiryDate != null)
                            {
                                var tmp = (DateTime)type.ExpiryDate;
                                dateStr = tmp.ToString("yyyy/MM/dd");
                            }

                            switch (type.SupportTypeCode)
                            {
                                case "MG":
                                    gridData.IsMGChecked = true;
                                    gridData.MGDate = dateStr;
                                    break;
                                case "MH":
                                    gridData.IsMHChecked = true;
                                    gridData.MHDate = dateStr;
                                    break;
                                case "UG":
                                    gridData.IsUGChecked = true;
                                    gridData.UGDate = dateStr;
                                    break;
                                case "UH":
                                    gridData.IsUHChecked = true;
                                    gridData.UHDate = dateStr;
                                    break;
                            }
                        }
                        // ModelTag and StatusCode
                        gridData.ModelTag = prodItem.InclResrc_TaggedMN;
                        gridData.StatusCode = prodItem.StatusCode;
                        gridData.AccountID = master.AccountID;

                        // ProdRegItemId
                        gridData.ProdRegItemId = prodItem.ProdRegItemID.ToString();
                        gridDataList.Add(gridData);
                    }
                }
            }


            // If gridDataList.Count is 0, There is no SupportType(This product's Model-Serial pair is not registed to SupportType).
            // so the system show Prod data only.
            // and...
            // In Japanese : 同じチームに属した人間が、同じモデル/シリアルのペアで登録している。
            //               表示した人間のうち、登録した人間(master.CreatedBy)が含まれない場合は追加で表示する。
            // In English : same Model/Serial pair is registed by same Team ppl.
            //              if displayed user does not contain "regist master prod user", display master.CreatedBy user.
            if (gridDataList.Count == 0 || !displayEmailList.Contains(master.CreatedBy))
            {
                foreach (var prodItem in prodItemList)
                {
                    if (prodItem.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP)
                    {
                        // Nothing to do.
                    }
                    else
                    {
                        continue;
                    }

                    // Model, Serial
                    var gridData = new SearchResultGridData();
                    gridData.IsEnable = true;
                    gridData.ModelNumber = prodItem.ModelNumber;
                    gridData.SerialNumber = prodItem.SerialNumber;

                    // MemberShipId
                    var membership = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(master.CreatedBy);
                    if (membership != null)
                        gridData.MemberShipId = membership.MembershipId;

                    // There is no SupportType.
                    // so input PrdMaster's "CreatedBy" Email.
                    gridData.EMailAddress = master.CreatedBy;

                    // ModelTag and StatusCode
                    gridData.ModelTag = prodItem.InclResrc_TaggedMN;
                    gridData.StatusCode = prodItem.StatusCode;
                    gridData.AccountID = master.AccountID;

                    // ProdRegItemId
                    gridData.ProdRegItemId = prodItem.ProdRegItemID.ToString();
                    gridDataList.Add(gridData);

                    // Message - the SupportType is not registered
                    // Below message is commnet out. becouse Email = "master.CreatedBy". so TS admin can register SupportType in this page.
                    //lblErrorMsg.Text = GetStaticResource("lclSupportTypeIsNotRegistered");
                    //lblErrorMsg.Visible = true;
                    gwpnlSearchKeywordInputArea.Visible = false;
                    pnlContainer.Visible = true;
                    bttModify.Visible = false;
                    bttOK.Visible = false;
                    bttDelete.Visible = true;
                    bttUpdate.Visible = true;
                }
            }

            // There is no "downloads-jp" type ProdItem. so the system show "nothing to show" message.
            if (gridDataList.Count == 0)
            {
                // "downloads-jp"type ProdItem is not found
                lblErrorMsg.Text = GetStaticResource("lclDonwloadsJpTypeItemIsNotFound");
                lblErrorMsg.Visible = true;

                gwpnlSearchKeywordInputArea.Visible = false;
                pnlContainer.Visible = true;
                bttModify.Visible = false;
                bttOK.Visible = false;
                bttDelete.Visible = false;
                bttUpdate.Visible = false;
            }

            gvSearchResults.DataSource = gridDataList;
            gvSearchResults.DataBind();
        }

        /// <summary>
        /// Bind Prodcut Data to Grid.
        /// </summary>
        private void BindProductDatas()
        {
            gvSearchResults.DataSource = null;

            var list = new List<SearchResultGridData>();
            // Get inputted form value
            var models = new String[] { };
            var serials = new String[] { };
            var emails = new String[] { };
            GetFormValue(ref models, ref serials, ref emails);

            // Get Team List per Email
            var teamDic = new Dictionary<String, Dictionary<Guid, String>>();
            var memberShipIdDic = new Dictionary<String, Guid>();
            foreach (var email in emails)
            {
                CreateTeamAndMemberShipIDDictionary(email, ref teamDic, ref memberShipIdDic);
            }

            // Generate Bind Data.
            List<SearchResultGridData> gridDatas = new List<SearchResultGridData>();
            foreach (var model in models)
            {
                foreach (var serial in serials)
                {
                    bool isExist = false;
                    foreach (var email in emails)
                    {
                        var teamList = teamDic[email];

                        // Check this Model,Serial is already Registered to ProductMaster Table?
                        foreach (var team in teamList)
                        {
                            // Get ProductMaster data
                            var prodRegId = ProdReg_MasterBLL.SelectByAccMnSnToList(team.Key, model, serial);
                            if (prodRegId != null && prodRegId.Count != 0)
                            {
                                foreach (var itemId in prodRegId)
                                {
                                    // Search ProdRegID
                                    var prodItemList = ProdReg_ItemBLL.SelectWithFilter(itemId.ProdRegID,
                                                                    Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_JP, model, serial);
                                    if (prodItemList != null && prodItemList.Count != 0)
                                    {
                                        // found.
                                        isExist = true;
                                        foreach (var item in prodItemList)
                                        {
                                            // Already Registered Grid Data
                                            var gridRowData = new SearchResultGridData();
                                            gridRowData.IsEnable = false;
                                            gridRowData.IsItemSelected = false;
                                            gridRowData.ModelNumber = model;
                                            gridRowData.SerialNumber = serial;
                                            gridRowData.EMailAddress = email;


                                            var supportTypes = BLL_SupportEntry.GetProductSupportListAsPerfectMatching(model, serial, email) as IEnumerable<SupportTypeEntriesForJPAdmin>;
                                            if (supportTypes.FirstOrDefault() != null && supportTypes.Count() != 0)
                                            {
                                                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(supportTypes.FirstOrDefault().Id)));
                                                foreach (SupportEntryMapping type in mappings)
                                                {
                                                    var dateStr = String.Empty;
                                                    if (type.ExpiryDate != null)
                                                    {
                                                        var tmp = (DateTime)type.ExpiryDate;
                                                        dateStr = tmp.ToString("yyyy/MM/dd");
                                                    }

                                                    switch (type.SupportTypeCode)
                                                    {
                                                        case "MG":
                                                            gridRowData.IsMGChecked = true;
                                                            gridRowData.MGDate = dateStr;
                                                            break;
                                                        case "MH":
                                                            gridRowData.IsMHChecked = true;
                                                            gridRowData.MHDate = dateStr;
                                                            break;
                                                        case "UG":
                                                            gridRowData.IsUGChecked = true;
                                                            gridRowData.UGDate = dateStr;
                                                            break;
                                                        case "UH":
                                                            gridRowData.IsUHChecked = true;
                                                            gridRowData.UHDate = dateStr;
                                                            break;
                                                    }
                                                }
                                            }

                                            if (memberShipIdDic.ContainsKey(email))
                                            {
                                                gridRowData.MemberShipId = memberShipIdDic[email];
                                            }
                                            gridRowData.ProdRegItemId = item.ProdRegItemID.ToString();
                                            gridRowData.ProdRegId = item.ProdRegID.ToString();
                                            gridRowData.FoundTeamName = team.Value;
                                            gridRowData.FoundTeamId = team.Key.ToString();
                                            gridDatas.Add(gridRowData);
                                        }
                                    }
                                }
                            }
                        }

                        if (isExist == false)
                        {
                            // Grid Data
                            var gridRowData = new SearchResultGridData();
                            gridRowData.IsEnable = false;
                            gridRowData.IsItemSelected = false;
                            gridRowData.ModelNumber = model;
                            gridRowData.SerialNumber = serial;
                            gridRowData.EMailAddress = email;
                            gridRowData.ModelTag = false;
                            if (memberShipIdDic.ContainsKey(email))
                            {
                                gridRowData.MemberShipId = memberShipIdDic[email];
                            }

                            var supportTypes = BLL_SupportEntry.GetProductSupportListAsPerfectMatching(model, serial, email) as IEnumerable<SupportTypeEntriesForJPAdmin>;
                            if (supportTypes.FirstOrDefault() != null && supportTypes.Count() != 0)
                            {
                                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(supportTypes.FirstOrDefault().Id)));
                                foreach (SupportEntryMapping type in mappings)
                                {
                                    var dateStr = String.Empty;
                                    if (type.ExpiryDate != null)
                                    {
                                        var tmp = (DateTime)type.ExpiryDate;
                                        dateStr = tmp.ToString("yyyy/MM/dd");
                                    }

                                    switch (type.SupportTypeCode)
                                    {
                                        case "MG":
                                            gridRowData.IsMGChecked = true;
                                            gridRowData.MGDate = dateStr;
                                            break;
                                        case "MH":
                                            gridRowData.IsMHChecked = true;
                                            gridRowData.MHDate = dateStr;
                                            break;
                                        case "UG":
                                            gridRowData.IsUGChecked = true;
                                            gridRowData.UGDate = dateStr;
                                            break;
                                        case "UH":
                                            gridRowData.IsUHChecked = true;
                                            gridRowData.UHDate = dateStr;
                                            break;
                                    }
                                }
                            }
                            gridDatas.Add(gridRowData);
                        }

                    }
                }
            }

            // Bind
            gvSearchResults.DataSource = gridDatas;
            gvSearchResults.DataBind();
        }

        private void CreateTeamAndMemberShipIDDictionary(string email, ref Dictionary<string, Dictionary<Guid, string>> teamDic, ref Dictionary<string, Guid> memberShipIdDic)
        {
            // Get MemberShipID
            var membership = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
            if (membership != null)
            {
                // Get User Data by MembershipId
                var accountArray = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(membership.MembershipId).Select(s => new
                {
                    AccountName = s.AccountName,
                    AccountID = s.AccountID
                }).Distinct().ToArray(); ;

                if (teamDic.ContainsKey(email) == false)
                {
                    var accountDic = new Dictionary<Guid, string>();
                    accountArray.ToList().ForEach(f =>
                    {
                        accountDic.Add(f.AccountID, f.AccountName);
                    });

                    teamDic.Add(email, accountDic);
                }

                if (memberShipIdDic.ContainsKey(email) == false)
                {
                    memberShipIdDic.Add(email, membership.MembershipId);
                }
            }
        }
        /// <summary>
        /// Bind KeyPress Event and Save TextBox Value to WebStrage.
        /// </summary>
        /// <returns></returns>
        private string AddKeyPressEventByJavaScript()
        {
            string js;


            js = "<script type=\"text/javascript\">\r\n";
            js += "function addKeyPressEventByJavaScript() {\r\n";
            js += "        var modelWrapper = $(\".input_field_model\");\r\n";
            js += "        var serialWrapper = $(\".input_field_serial\");\r\n";
            js += "        var emailWrapper = $(\".input_field_email\");\r\n";
            js += "\r\n";
            js += "        $(modelWrapper).on(\"click\", \".remove_field\", function (e) {\r\n";
            js += "            e.preventDefault();\r\n";
            js += "            $(this).parent('div').remove();\r\n";
            js += "        })\r\n";
            js += "\r\n";
            js += "        $(serialWrapper).on(\"click\", \".remove_field\", function (e) {\r\n";
            js += "            e.preventDefault();\r\n";
            js += "            $(this).parent('div').remove();\r\n";
            js += "        })\r\n";
            js += "\r\n";
            js += "        $(emailWrapper).on(\"click\", \".remove_field\", function (e) {\r\n";
            js += "            e.preventDefault();\r\n";
            js += "            $(this).parent('div').remove();\r\n";
            js += "        })\r\n";
            js += "\r\n";
            js += "        $(modelWrapper).keypress(function (e) {\r\n";
            js += "\r\n";
            js += "            if (e.which == 13) {\r\n";
            js += "                e.preventDefault();\r\n";
            js += "\r\n";
            js += "                var inputModelName = $(\"#txtModels\").val();\r\n";
            js += "                var h = '<div><input type=\"text\" name=\"models[]\" value=\"' + inputModelName + '\" Style=\"width:300px;background-color:#F7F7F7\" readonly=\"readonly\"/> <a href=\"#\" class=\"remove_field\">X</a></div>';\r\n";
            js += "                $(\".modelArea\").append(h);\r\n";
            js += "\r\n";
            js += "                // Init input word\r\n";
            js += "                $(\"#txtModels\").val(\"\");\r\n";
            js += "                return false;\r\n";
            js += "            }\r\n";
            js += "        });\r\n";
            js += "\r\n";
            js += "        $(serialWrapper).keypress(function (e) {\r\n";
            js += "\r\n";
            js += "            if (e.which == 13) {\r\n";
            js += "                e.preventDefault();\r\n";
            js += "\r\n";
            js += "                var inputModelName = $(\"#txtSerials\").val();\r\n";
            js += "                var h = '<div><input type=\"text\" name=\"serials[]\" value=\"' + inputModelName + '\" Style=\"width:300px;background-color:#F7F7F7\" readonly=\"readonly\"/> <a href=\"#\" class=\"remove_field\">X</a></div>';\r\n";
            js += "                $(\".serialArea\").append(h);\r\n";
            js += "\r\n";
            js += "                $(\"#txtSerials\").val(\"\");\r\n";
            js += "                return false;\r\n";
            js += "            }\r\n";
            js += "        });\r\n";
            js += "\r\n";
            js += "        $(emailWrapper).keypress(function (e) {\r\n";
            js += "            if (e.which == 13) {\r\n";
            js += "                e.preventDefault();\r\n";
            js += "\r\n";
            js += "                var inputModelName = $(\"#txtEmails\").val();\r\n";
            js += "                var h = '<div><input type=\"text\" name=\"emails[]\" value=\"' + inputModelName + '\" Style=\"width:300px;background-color:#F7F7F7\" readonly=\"readonly\"/> <a href=\"#\" class=\"remove_field\" >X</a></div>';\r\n";
            js += "                $(\".emailArea\").append(h);\r\n";
            js += "\r\n";
            js += "                $(\"#txtEmails\").val(\"\");\r\n";
            js += "                return false;\r\n";
            js += "            }\r\n";
            js += "        });\r\n";
            js += "\r\n";

            js += "$(\"#" + bttSearch.ClientID + "\").on( 'click', function(){";
            js += "var ss = sessionStorage;";
            js += "if( ss ){";
            js += "var initModelHtml = ss.getItem( 'initModelHtml' );";
            js += "var initSerialHtml = ss.getItem( 'initSerialHtml' );";
            js += "var initEmailHtml = ss.getItem( 'initEmailHtml' );";
            js += "var lastModelHtml = $(\".modelArea\").html();";
            js += "var lastSerialHtml = $(\".serialArea\").html();";
            js += "var lastEmailHtml = $(\".emailArea\").html();";
            js += "var lastModelTextBoxValue = $(\"#txtModels\").val();\r\n";
            js += "var lastSerialTextBoxValue = $(\"#txtSerials\").val();\r\n";
            js += "var lastEmailTextBoxValue = $(\"#txtEmails\").val();\r\n";
            js += "lastModelHtml = lastModelHtml.replace( initModelHtml, \"\" );";
            js += "lastSerialHtml = lastSerialHtml.replace( initSerialHtml, \"\" );";
            js += "lastEmailHtml = lastEmailHtml.replace( initEmailHtml, \"\" );";
            js += "ss.setItem( 'lastModelHtml', lastModelHtml );";
            js += "ss.setItem( 'lastSerialHtml', lastSerialHtml );";
            js += "ss.setItem( 'lastEmailHtml', lastEmailHtml );";
            js += "ss.setItem( 'lastModelTextBoxValue', lastModelTextBoxValue );";
            js += "ss.setItem( 'lastSerialTextBoxValue', lastSerialTextBoxValue );";
            js += "ss.setItem( 'lastEmailTextBoxValue', lastEmailTextBoxValue );";
            js += "}";
            js += "});";
            js += "}\r\n";
            js += "addKeyPressEventByJavaScript();\r\n";
            js += @"</script>";

            return js;
        }

        /// <summary>
        /// Reproduction Dynamic TextBox by JavaScript
        /// </summary>
        /// <returns></returns>
        private string ReproductionDynamicTextBox()
        {
            string js;
            js = "<script type=\"text/javascript\" >\r\n";
            js += "function initEntryForm() {\r\n";
            js += "var ss = sessionStorage;\r\n";
            js += "if (ss) {\r\n";
            js += "      var initModelHtml = $(\".modelArea\").html();\r\n";
            js += "      var initSerialHtml = $(\".serialArea\").html();\r\n";
            js += "      var initEmailHtml = $(\".emailArea\").html();\r\n";
            js += "      ss.setItem('initModelHtml', initModelHtml);\r\n";
            js += "      ss.setItem('initSerialHtml', initSerialHtml);\r\n";
            js += "      ss.setItem('initEmailHtml', initEmailHtml);\r\n";
            js += "      var lastModelHtml = ss.getItem('lastModelHtml');\r\n";
            js += "      var lastSerialHtml = ss.getItem('lastSerialHtml');\r\n";
            js += "      var lastEmailHtml = ss.getItem('lastEmailHtml');\r\n";
            js += "      var lastModelTextBoxValue = ss.getItem('lastModelTextBoxValue');\r\n";
            js += "      var lastSerialTextBoxValue = ss.getItem('lastSerialTextBoxValue');\r\n";
            js += "      var lastEmailTextBoxValue = ss.getItem('lastEmailTextBoxValue');\r\n";
            js += "      if (lastModelHtml) {\r\n";
            js += "            $(\".modelArea\").append(lastModelHtml);\r\n";
            js += "      }\r\n";
            js += "      if (lastSerialHtml) {\r\n";
            js += "            $(\".serialArea\").append(lastSerialHtml);\r\n";
            js += "      }\r\n";
            js += "      if (lastEmailHtml) {\r\n";
            js += "            $(\".emailArea\").append(lastEmailHtml);\r\n";
            js += "      }\r\n";
            js += "      if (lastModelTextBoxValue) {\r\n";
            js += "                $(\"#txtModels\").val(lastModelTextBoxValue);\r\n";
            js += "      }\r\n";
            js += "      if (lastSerialTextBoxValue) {\r\n";
            js += "                $(\"#txtSerials\").val(lastSerialTextBoxValue);\r\n";
            js += "      }\r\n";
            js += "      if (lastEmailTextBoxValue) {\r\n";
            js += "                $(\"#txtEmails\").val(lastEmailTextBoxValue);\r\n";
            js += "      }\r\n";
            js += "      ss.removeItem('lastModelHtml');\r\n";
            js += "      ss.removeItem('lastSerialHtml');\r\n";
            js += "      ss.removeItem('lastEmailHtml');\r\n";
            js += "      ss.removeItem('lastModelTextBoxValue');\r\n";
            js += "      ss.removeItem('lastSerialTextBoxValue');\r\n";
            js += "      ss.removeItem('lastEmailTextBoxValue');\r\n";
            js += "}\r\n";
            js += "}\r\n";
            js += "initEntryForm();\r\n";
            js += "</script>\r\n";
            return js;
        }

        /// <summary>
        /// Get StatusCode ListItem for Modifing SupportType/ModelTag/StatusCode
        /// </summary>
        /// <param name="prodReg_ItemStatus"></param>
        /// <returns></returns>
        private List<ListItem> GetStatusCodeListItem(ProdReg_ItemStatus prodReg_ItemStatus)
        {
            List<ProdReg_Item_Status> items = Admin_ProductRegUtility.ProdReg_Item_Status_SelectAll(true);

            var listItem = new List<ListItem>();
            items.OrderBy(o => o.DisplayOrder).ToList().ForEach(f =>
            {
                var item = new ListItem();
                item.Text = f.StatusDisplayText;
                item.Value = f.StatusCode;
                item.Enabled = true;

                if (prodReg_ItemStatus.ToString() == f.StatusCode)
                {
                    item.Selected = true;
                }

                listItem.Add(item);
            });
            return listItem;
        }

        /// <summary>
        /// Create ListItems of StatucCode
        /// </summary>
        /// <returns></returns>
        private Dictionary<String, String> GetStatusCodeListItem()
        {
            List<ProdReg_Item_Status> items = Admin_ProductRegUtility.ProdReg_Item_Status_SelectAll(true);

            var listItem = new Dictionary<String, String>();
            items.OrderBy(o => o.DisplayOrder).ToList().ForEach(f =>
            {
                if (listItem.Keys.Contains(f.StatusDisplayText) == false)
                {
                    listItem.Add(f.StatusDisplayText, f.StatusCode);
                }
            });
            return listItem;
        }

        /// <summary>
        /// Get current GridData
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private UserModifiedGridData GetUserModifiedGridData(GridViewRow row)
        {
            var isSelectCheckBox = (CheckBox)row.FindControl("cbxSelectItem");
            var modelNumberLabel = (Label)row.FindControl("lblModelNumber");
            var serialNumberLabel = (Label)row.FindControl("lblSerialNumber");
            var emailLabel = (Label)row.FindControl("lblEMailAddress");
            var teamDropDownList = (DropDownList)row.FindControl("ddlTeam");
            var MGCheckBox = (CheckBox)row.FindControl("cbxMG");
            var MGDate = (TextBox)row.FindControl("deMG");
            var MHCheckBox = (CheckBox)row.FindControl("cbxMH");
            var MHDate = (TextBox)row.FindControl("deMH");
            var UGCheckBox = (CheckBox)row.FindControl("cbxUG");
            var UGDate = (TextBox)row.FindControl("deUG");
            var UHCheckBox = (CheckBox)row.FindControl("cbxUH");
            var UHDate = (TextBox)row.FindControl("deUH");
            var modelTagCheckBox = (CheckBox)row.FindControl("cbxMT");
            var memberShipIdLabel = (Label)row.FindControl("txtMemberShipId");
            var statusCodeDropDownList = (DropDownList)row.FindControl("ddlStatusCode");
            var prodRegItemIdLabel = (Label)row.FindControl("txtProdRegItemId");
            var prodRegIdLabel = (Label)row.FindControl("txtProdRegId");
            var foundTeamName = (Label)row.FindControl("txtFoundTeamName");
            var foundTeamId = (Label)row.FindControl("txtFoundTeamId");
            var accountId = (Label)row.FindControl("txtAccountId");

            var rowData = new UserModifiedGridData();
            rowData.IsEnable = false;
            rowData.IsItemSelected = isSelectCheckBox.Checked;
            rowData.ModelNumber = modelNumberLabel.Text;
            rowData.SerialNumber = serialNumberLabel.Text;
            rowData.EMailAddress = emailLabel.Text;
            rowData.Team = teamDropDownList.SelectedItem;
            rowData.IsMGChecked = MGCheckBox.Checked;
            rowData.MGDate = MGDate.Text;
            rowData.IsMHChecked = MHCheckBox.Checked;
            rowData.MHDate = MHDate.Text;
            rowData.IsUGChecked = UGCheckBox.Checked;
            rowData.UGDate = UGDate.Text;
            rowData.IsUHChecked = UHCheckBox.Checked;
            rowData.UHDate = UHDate.Text;
            rowData.ModelTag = modelTagCheckBox.Checked;
            rowData.DSStatusCode = statusCodeDropDownList.SelectedItem;

            if (!String.IsNullOrEmpty(memberShipIdLabel.Text))
            {
                var regex = new Regex(@"\S{8}-\S{4}-\S{4}-\S{4}-\S{12}");
                var tmpMemberShipId = memberShipIdLabel.Text;
                if (regex.IsMatch(tmpMemberShipId))
                {
                    rowData.MemberShipId = new Guid(regex.Match(tmpMemberShipId).Value);
                }
                else
                {
                    rowData.MemberShipId = new Guid();
                }
            }

            rowData.ProdRegItemId = prodRegItemIdLabel.Text;
            rowData.ProdRegId = prodRegIdLabel.Text;
            rowData.FoundTeamName = foundTeamName.Text;
            rowData.FoundTeamId = foundTeamId.Text;

            if (!String.IsNullOrEmpty(accountId.Text))
            {
                var regex = new Regex(@"\S{8}-\S{4}-\S{4}-\S{4}-\S{12}");
                var tmpAccId = accountId.Text;
                if (regex.IsMatch(tmpAccId))
                {
                    rowData.AccountId = new Guid(regex.Match(tmpAccId).Value);
                }
                else
                {
                    rowData.AccountId = new Guid();
                }
            }

            return rowData;
        }

        /// <summary>
        /// Get all Expire Date of SupportType from current GridData.
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetExpireDateForModifySupportType(UserModifiedGridData rowData)
        {
            var tempDic = new Dictionary<string, string>();
            if (rowData.IsMGChecked)
            {
                if (!String.IsNullOrEmpty(rowData.MGDate))
                {
                    tempDic.Add("MG", rowData.MGDate);
                }
                else
                {
                    tempDic.Add("MG", String.Empty);
                }
            }

            if (rowData.IsMHChecked)
            {
                if (!String.IsNullOrEmpty(rowData.MHDate))
                {
                    tempDic.Add("MH", rowData.MHDate);
                }
                else
                {
                    tempDic.Add("MH", String.Empty);
                }
            }

            if (rowData.IsUGChecked)
            {
                if (!String.IsNullOrEmpty(rowData.UGDate))
                {
                    tempDic.Add("UG", rowData.UGDate);
                }
                else
                {
                    tempDic.Add("UG", String.Empty);
                }
            }

            if (rowData.IsUHChecked)
            {
                if (!String.IsNullOrEmpty(rowData.UHDate))
                {
                    tempDic.Add("UH", rowData.UHDate);
                }
                else
                {
                    tempDic.Add("UH", String.Empty);
                }
            }

            return tempDic;
        }

        /// <summary>
        /// Convert StatusCode from ListItem.
        /// </summary>
        /// <param name="listItem"></param>
        /// <returns></returns>
        private ProdReg_ItemStatus ConvertStatusCodeFromListItem(ListItem listItem)
        {
            var status = (ProdReg_ItemStatus)Enum.Parse(typeof(ProdReg_ItemStatus), listItem.Value);
            return status;
        }

        /// <summary>
        /// Register the Product
        /// </summary>
        /// <param name="acc">account infomraiotn</param>
        /// <param name="serialPatternDic">serial pattern dictionary</param>
        /// <param name="rowData">grid data</param>
        /// <param name="errCode">erro code</param>
        /// <returns></returns>
        private bool RegisterProduct(Guid accountID, UserModifiedGridData rowData, out int errCode, out Guid webToken)
        {

            Dictionary<String, String> addOnData = null;
            Int32 regActiveCount = 0;

            Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(rowData.ModelNumber);
            String serialPattern = String.Empty;
            if (!pcfg.Reg_ValidateSN)
                serialPattern = pcfg.Reg_SNPattern;

            // Register Product.
            errCode = ProdReg_MasterBLL.Insert(accountID,
                                                rowData.ModelNumber.ToUpperInvariant(),
                                                rowData.SerialNumber,
                                                LoginUtility.GetCurrentUser(true).Email,
                                                addOnData,
                                                true,
                                                out webToken,
                                                out regActiveCount,
                                                serialPattern
                                                );
            if (errCode < 1)
            {
                // Error Product is exist.
                lblErrorMsg.Text = GetStaticResource("lclFailToRegisterAlreadyExist");
                lblErrorMsg.Visible = true;
                return false;
            }

            return true;
        }

        private void CreateSupportTypeUpdateData(UserModifiedGridData rowData, ref Dictionary<string, object> formValuesDictionary)
        {
            var supportTypes = BLL_SupportEntry.GetProductSupportListAsPerfectMatching(rowData.ModelNumber,
                                           rowData.SerialNumber, rowData.EMailAddress) as IEnumerable<SupportTypeEntriesForJPAdmin>;
            if (supportTypes == null || supportTypes.Count() != 1)
            {
                return;
            }
            var supportType = supportTypes.FirstOrDefault();

            formValuesDictionary.Add("Id", supportType.Id);

            // Refer:SupportType_Manager.ascx.cs - gridSupportType_OnRowUpdating function
            if (supportType.AccountId == null && supportType.MemberShipId != null)
            {
                formValuesDictionary.Add("MembershipId", supportType.MemberShipId);
                formValuesDictionary.Add("OrgId", string.Empty);

            }
            else if (supportType.AccountId != null && supportType.MemberShipId == null)
            {
                formValuesDictionary.Add("MembershipId", string.Empty);
                formValuesDictionary.Add("OrgId", supportType.AccountId);
            }

            formValuesDictionary.Add("ModelNumber", rowData.ModelNumber);
            formValuesDictionary.Add("SerialNumber", rowData.SerialNumber);
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);
            formValuesDictionary.Add("SupportType", GetExpireDateForModifySupportType(rowData));
            formValuesDictionary.Add("ModelConfigType", ModelConfigTypeInfo.CONFIGTYPE_JP);
        }

        /// <summary>
        /// Add SupportType Entry
        /// </summary>
        /// <param name="productData"></param>
        /// <returns></returns>
        private bool AddSupportTypeEntry(UserModifiedGridData rowData)
        {
            var formValuesDictionary = new Dictionary<string, object>();

            formValuesDictionary.Add("OrgId", string.Empty);
            formValuesDictionary.Add("MembershipId", rowData.MemberShipId);
            formValuesDictionary.Add("ModelNumber", rowData.ModelNumber);
            formValuesDictionary.Add("SerialNumber", rowData.SerialNumber);

            var supportTypesDictionary = new Dictionary<string, string>();
            if (rowData.IsMGChecked)
            {
                if (!String.IsNullOrEmpty(rowData.MGDate))
                    supportTypesDictionary.Add("MG", rowData.MGDate);
                else
                    supportTypesDictionary.Add("MG", String.Empty);
            }

            if (rowData.IsMHChecked)
            {
                if (!String.IsNullOrEmpty(rowData.MHDate))
                    supportTypesDictionary.Add("MH", rowData.MHDate);
                else
                    supportTypesDictionary.Add("MH", String.Empty);
            }

            if (rowData.IsUGChecked)
            {
                if (!String.IsNullOrEmpty(rowData.UGDate))
                    supportTypesDictionary.Add("UG", rowData.UGDate);
                else
                    supportTypesDictionary.Add("UG", String.Empty);
            }

            if (rowData.IsUHChecked)
            {
                if (!String.IsNullOrEmpty(rowData.UHDate))
                    supportTypesDictionary.Add("UH", rowData.UHDate);
                else
                    supportTypesDictionary.Add("UH", String.Empty);
            }

            formValuesDictionary.Add("SupportType", supportTypesDictionary);
            formValuesDictionary.Add("ModelConfigType", ModelConfigTypeInfo.CONFIGTYPE_JP);
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);

            LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_ATTEMPT, LoggedInUserEmail, rowData.ModelNumber, rowData.SerialNumber, rowData.EMailAddress);

            int result = (int)BLL_SupportEntry.AddProductSupportEntry(formValuesDictionary);
            if (result > 0)
            {
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_SUCCESS, LoggedInUserEmail, rowData.ModelNumber, rowData.SerialNumber, rowData.EMailAddress);
            }
            else
            {
                lblErrorMsg.Text = GetStaticResource("lblMsgSupportTypeSaveError");
                lblErrorMsg.Visible = true;
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_FAIL, LoggedInUserEmail, rowData.ModelNumber, rowData.SerialNumber, rowData.EMailAddress);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Update SupportType by current GridData.
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        private bool UpdateSupportTypeEntry(UserModifiedGridData rowData, Dictionary<string, object> formValuesDictionary)
        {
            // Update Attempt
            LogAdminActivity(AreaEffected,
                       TasksPerformed.Modify,
                       ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_ATTEMPT,
                       LoggedInUserEmail,
                       rowData.ModelNumber,
                       rowData.SerialNumber,
                       rowData.EMailAddress);

            // Update
            var i = (int)BLL_SupportEntry.UpdateProductSupportEntry(formValuesDictionary);
            if (i < 0)
            {
                // Update Fail
                LogAdminActivity(AreaEffected,
                       TasksPerformed.Modify,
                       ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_FAIL,
                       LoggedInUserEmail,
                       rowData.ModelNumber,
                       rowData.SerialNumber,
                       rowData.EMailAddress);
                return false;
            }

            // Update Success
            LogAdminActivity(AreaEffected,
                       TasksPerformed.Modify,
                       ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_SUCCESS,
                       LoggedInUserEmail,
                       rowData.ModelNumber,
                       rowData.SerialNumber,
                       rowData.EMailAddress);
            return true;
        }

        private bool IsExsitsSupportType(UserModifiedGridData rowData, ref Dictionary<string, object> formValuesDictionary)
        {
            var supportTypes = BLL_SupportEntry.GetProductSupportListAsPerfectMatching(rowData.ModelNumber,
                                           rowData.SerialNumber, rowData.EMailAddress) as IEnumerable<SupportTypeEntriesForJPAdmin>;
            if (supportTypes == null || supportTypes.Count() != 1)
            {
                return false;
            }

            var supportType = supportTypes.FirstOrDefault();
            formValuesDictionary.Add("Id", supportType.Id);

            // Refer:SupportType_Manager.ascx.cs - gridSupportType_OnRowUpdating function
            if (supportType.AccountId == null && supportType.MemberShipId != null)
            {
                formValuesDictionary.Add("MembershipId", supportType.MemberShipId);
                formValuesDictionary.Add("OrgId", string.Empty);

            }
            else if (supportType.AccountId != null && supportType.MemberShipId == null)
            {
                formValuesDictionary.Add("MembershipId", string.Empty);
                formValuesDictionary.Add("OrgId", supportType.AccountId);
            }

            formValuesDictionary.Add("ModelNumber", rowData.ModelNumber);
            formValuesDictionary.Add("SerialNumber", rowData.SerialNumber);
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);
            formValuesDictionary.Add("SupportType", GetExpireDateForModifySupportType(rowData));
            formValuesDictionary.Add("ModelConfigType", ModelConfigTypeInfo.CONFIGTYPE_JP);

            return true;
        }

        /// <summary>
        /// Log Admin Activity.
        /// </summary>
        /// <param name="areEffected"></param>
        /// <param name="action"></param>
        /// <param name="taskPerformed"></param>
        /// <param name="userEmail"></param>
        /// <param name="paramsArgs"></param>
        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(LoggedInUserEmail, AreaEffected, taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
        }
    }
}