﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_Deletions_LogByAccountCtrl.ascx.cs" 
    Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_Deletions_LogByAccountCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlRegProdDeleted" runat="server" ShowHeader="true" 
    HeaderText="<%$Resources:ADM_STCTRL_ProdReg_Deletions_LogByAccountCtrl,pnlRegProdDeleted.HeaderText%>">
    <div class="settingrow">
       <dx:ASPxGridView ID="dxgvProdRegDeletions" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false" 
           DataSourceID="sdsDeletedProdRegs" Width="100%">
           <Columns>
               <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1"></dx:GridViewDataColumn>
               <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="2"></dx:GridViewDataColumn>
               <dx:GridViewDataDateColumn FieldName="DeletedOnUTC" VisibleIndex="3"></dx:GridViewDataDateColumn>
               <dx:GridViewDataColumn FieldName="DeletedBy" VisibleIndex="4"></dx:GridViewDataColumn>  
               <dx:GridViewDataColumn FieldName="Admin" VisibleIndex="5"></dx:GridViewDataColumn>
           </Columns>
           <SettingsPager PageSize="500"></SettingsPager>
       </dx:ASPxGridView>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_Deletions_LogByAccountCtrl" />
<asp:SqlDataSource ID="sdsDeletedProdRegs" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Deletions_SelectByAccountID]">
    <SelectParameters>
        <asp:QueryStringParameter Name="AccountID" QueryStringField="accid" DbType="Guid" />
    </SelectParameters>
</asp:SqlDataSource>