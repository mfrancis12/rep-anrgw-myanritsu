﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_Item_ESDCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_Item_ESDCtrl" %>

<dx:ASPxGridView ID="gvItems" ClientInstanceName="gvItems" runat="server" DataSourceID="sdsProdRegItems" Theme="AnritsuDevXTheme" KeyFieldName="ProdRegItemID"
    Width="100%" EnableRowsCache="false" Settings-ShowColumnHeaders="true"
    SettingsText-CommandCancel="Cancel" SettingsText-CommandUpdate="Update" SettingsText-CommandDelete="Delete"
    SettingsText-PopupEditFormCaption="Update Item" OnInitNewRow="gvItems_InitNewRow" OnCellEditorInitialize="gvItems_CellEditorInitialize" >
    <Columns>
        <dx:GridViewDataColumn FieldName="ProdRegItemID" VisibleIndex="1" ReadOnly="true" Visible="false" />
        <dx:GridViewDataComboBoxColumn FieldName="ModelNumber" VisibleIndex="2" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ModelNumber.Text %>'>
            <PropertiesComboBox TextField="ModelNumber" ValueField="ModelNumber" DataSourceID="sdsLookupRegistratrableProducts" IncrementalFilteringMode="Contains"></PropertiesComboBox>
            <%--  <EditFormSettings Visible="False" />--%>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,SerialNumber.Text %>' />
        <dx:GridViewDataComboBoxColumn FieldName="StatusCode" VisibleIndex="10" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,StatusCode.Text %>'>
            <PropertiesComboBox TextField="StatusCode" ValueField="StatusCode" DataSourceID="sdsLookupProdRegStatus"></PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>

        <dx:GridViewDataColumn FieldName="InclResrc_TaggedMN" VisibleIndex="26" Visible="false" EditFormSettings-Visible="True" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,InclResrc_TaggedMN.Text %>' />
        <dx:GridViewDataColumn FieldName="AddedBy" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,AddedBy.Text %>' />
        <dx:GridViewDataColumn FieldName="UpdatedBy" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,UpdatedBy.Text %>' />
        <dx:GridViewDataDateColumn FieldName="ModifiedOnUTC" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ModifiedOnUTC.Text %>'>
            <PropertiesDateEdit DisplayFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,CreatedOnUTC.Text %>'>
            <PropertiesDateEdit DisplayFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>

        <dx:GridViewCommandColumn VisibleIndex="600" ButtonType="Image" Caption=" ">
          <%--<NewButton Visible="True" Image-ToolTip="Add New Item" Image-SpriteProperties-CssClass="new-icon" />--%>
            <EditButton Visible="true" Image-ToolTip="Edit Item Details" Image-SpriteProperties-CssClass="edit-icon"></EditButton>
           <%-- <DeleteButton Visible="true" Image-ToolTip="Delete Item" Image-SpriteProperties-CssClass="delete-icon"></DeleteButton>--%>
            <UpdateButton Image-ToolTip="Save Item" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/Update1.PNG"></UpdateButton>
            <CancelButton Image-ToolTip="Cancel" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/CancelHover.PNG"></CancelButton>
        </dx:GridViewCommandColumn>
    </Columns>
    <SettingsPopup>
        <EditForm Width="600" Modal="true" ShowHeader="false" />
    </SettingsPopup>
    <SettingsEditing Mode="Inline" EditFormColumnCount="1" />
    <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true"              ProcessSelectionChangedOnServer="true"  ConfirmDelete="true" />
    <%--  <SettingsDetail ShowDetailRow="true" />--%>
    <%--  <SettingsLoadingPanel Text="Loading..."></SettingsLoadingPanel>--%>

    <Styles>
        <AlternatingRow Enabled="false"></AlternatingRow>
        <Row BackColor="#FFFFFF"></Row>
        <CommandColumnItem Spacing="10" Paddings-PaddingRight="10px"></CommandColumnItem>
    </Styles>
</dx:ASPxGridView>
<asp:SqlDataSource ID="sdsProdRegItems" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_SelectByWebToken]"
    UpdateCommandType="StoredProcedure" UpdateCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForESD_Update]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForESD_Delete]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForESD_Insert]"
    OnInserting="sdsProdRegItems_Inserting" OnUpdating="sdsProdRegItems_Updating"
    OnInserted="sdsProdRegItems_Inserted" OnUpdated="sdsProdRegItems_Updated" OnDeleted="sdsProdRegItems_Deleted">
    <SelectParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
        <asp:Parameter Name="ModelConfigType" Type="String" DefaultValue="downloads-uk" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
        <asp:Parameter Name="ModelNumber" Type="String" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="StatusCode" Type="String" />
        <asp:Parameter Name="InclResrc_TaggedMN" Type="Boolean" />
        <asp:Parameter Name="UpdatedBy" Type="String" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
        <asp:Parameter Name="ModelNumber" Type="String" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="StatusCode" Type="String" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="InclResrc_TaggedMN" Type="Boolean" DefaultValue="true" />
        <asp:Parameter Name="AddedBy" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupRegistratrableProducts" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectActiveByModelConfigType]">
    <SelectParameters>
        <asp:Parameter Name="ModelConfigType" DbType="String" DefaultValue="downloads-uk" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupProdRegStatus" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_Status_SelectAll]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_Item_JPDLCtrl" />

