﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_Item_ESDCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ItemType { get; set; }

        private Guid WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback && !IsPostBack)
            {
                if (ItemType.IsNullOrEmptyString()) throw new ArgumentNullException("ItemType");

                gvItems.DataBind();
                //gvItems.DetailRows.ExpandRow(0);
                gvItems.DetailRows.ExpandAllRows();
            }

            if (!IsPostBack)
            {
                // gvItems.StartEdit(0);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_Item_JPDLCtrl"; }

        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void sdsProdRegItems_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            ProdReg_Master prm = ProdReg_MasterBLL.SelectByWebToken(WebToken);
            if (prm == null) throw new HttpException(404, "Invalid token.");
            if (e.Command.Parameters["@ModelNumber"].Value != null && e.Command.Parameters["@SerialNumber"].Value != null)
            {
                List<ProdReg_Item> itemList = ProdReg_ItemBLL.SelectWithFilter(prm.ProdRegID
                    , Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
                    , e.Command.Parameters["@ModelNumber"].Value.ToString()
                    , e.Command.Parameters["@SerialNumber"].Value.ToString());

                if (itemList.Count == 0)
                {
                    e.Command.Parameters["@AddedBy"].Value = LoginUtility.GetCurrentUserOrSignIn().Email;
                    //e.Command.Parameters["@InclResrc_TaggedMN"].Value = true;
                }
                else
                    throw new ArgumentException(GetStaticResource("MNSNExistsMsg"));
            }
            else
                throw new ArgumentException(GetStaticResource("EnterMNSNMsg"));
        }

        protected void sdsProdRegItems_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            ProdReg_Master prm = Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, false);
            Lib.ProductRegistration.ProdReg_Item prItem = Lib.ProductRegistration.ProdReg_ItemBLL.SelectByProdRegItemId(Convert.ToInt32(e.Command.Parameters["@ProdRegItemID"].Value));
            List<ProdReg_Item> itemList = Lib.ProductRegistration.ProdReg_ItemBLL.SelectWithFilter(prm.ProdRegID
                , Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
                , e.Command.Parameters["@ModelNumber"].Value.ToString()
                , e.Command.Parameters["@SerialNumber"].Value.ToString());
            if (itemList.Count > 1 || (itemList.Count == 1 && !prItem.SerialNumber.Equals(e.Command.Parameters["@SerialNumber"].Value.ToString())))
            {
                throw new ArgumentException(GetStaticResource("MNSNExistsMsg"));
            }
        }


        protected void gvItems_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["InclResrc_TaggedMN"] = true;
        }

        protected void sdsProdRegItems_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
        }

        protected void sdsProdRegItems_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
        }

        protected void sdsProdRegItems_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
        }

        protected void gvItems_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "ModelNumber" || e.Column.FieldName == "SerialNumber")
                e.Editor.ClientEnabled = false;
        }
    }
}