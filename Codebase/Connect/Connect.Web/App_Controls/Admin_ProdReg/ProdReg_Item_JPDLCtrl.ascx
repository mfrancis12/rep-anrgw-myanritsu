﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_Item_JPDLCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_Item_JPDLCtrl" %>
<div class="lcalGoToPack-wrapper">
        <a href="/prodregadmin/productsupport/jpsupporttypeentry" target="_self" class="blueit"><asp:Localize ID="lcalGoToPack" runat="server" Text='<%$ Resources:~/prodregadmin/productsupport/jpsupporttypeentry,BrowserTitle %>'> </asp:Localize></a>
    </div>
<dx:ASPxGridView ID="gvItems" ClientInstanceName="gvItems" runat="server" DataSourceID="sdsProdRegItems" Theme="AnritsuDevXTheme" KeyFieldName="ProdRegItemID"
    Width="100%" EnableRowsCache="false" Settings-ShowColumnHeaders="true" OnHtmlRowCreated="gvItems_HtmlRowCreated"
    SettingsText-CommandCancel="Cancel" SettingsText-CommandUpdate="Update" SettingsText-CommandDelete="Delete" 
    SettingsText-PopupEditFormCaption="Update Item" OnInitNewRow="gvItems_InitNewRow" OnCellEditorInitialize="gvItems_CellEditorInitialize" >
    <Columns>
        <dx:GridViewDataColumn FieldName="ProdRegItemID" VisibleIndex="1" ReadOnly="true" Visible="false" />
          <dx:GridViewDataColumn Visible="false" >
            <DataItemTemplate>
        <dx:ASPxTextBox runat="server" ID="txtStatuscode" Text='<%#Eval("StatusCode") %>' style="display:none"></dx:ASPxTextBox>
                </DataItemTemplate>
              </dx:GridViewDataColumn>
        <dx:GridViewDataComboBoxColumn FieldName="ModelNumber" VisibleIndex="2" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ModelNumber.Text %>'>
            <PropertiesComboBox TextField="ModelNumber" ValueField="ModelNumber" DataSourceID="sdsLookupRegistratrableProducts" IncrementalFilteringMode="Contains"></PropertiesComboBox>
          <%--  <EditFormSettings Visible="False" />--%>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,SerialNumber.Text %>' />
        <dx:GridViewDataComboBoxColumn FieldName="StatusCode" VisibleIndex="10" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,StatusCode.Text %>'>
            <PropertiesComboBox TextField="ContentStr" ValueField="StatusCode" DataSourceID="sdsLookupProdRegStatus"></PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <%--<dx:GridViewDataComboBoxColumn  FieldName="JPExport_ModelTarget" VisibleIndex="15" Visible="false" EditFormSettings-Visible="False" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,JPExport_ModelTarget.Text %>'>
            <PropertiesComboBox TextField="JPExportCode" ValueField="JPExportCode" DataSourceID="sdsLookupProdRegJPExportType" ></PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>--%>
       <%-- <dx:GridViewDataDateColumn FieldName="JPSupportContract" VisibleIndex="20" Visible="false" EditFormSettings-Visible="False" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,JPSupportContract.Text %>'>
            <PropertiesDateEdit DisplayFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>--%>
        <dx:GridViewDataColumn FieldName="InclResrc_TaggedMN" VisibleIndex="26" Visible="false" EditFormSettings-Visible="True" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,InclResrc_TaggedMN.Text %>' />
        <dx:GridViewDataColumn FieldName="AddedBy" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,AddedBy.Text %>' />
        <dx:GridViewDataColumn FieldName="UpdatedBy" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,UpdatedBy.Text %>' />
        <dx:GridViewDataDateColumn FieldName="ModifiedOnUTC" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ModifiedOnUTC.Text %>'>
            <PropertiesDateEdit DisplayFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,CreatedOnUTC.Text %>'>
            <PropertiesDateEdit DisplayFormatString="MM/dd/yyyy">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataColumn FieldName="UseUserACL" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,UserACLRequired.Text %>' />
        <dx:GridViewDataColumn FieldName="HasSupportExpiration" VisibleIndex="500" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" />
        <dx:GridViewCommandColumn VisibleIndex="600" ButtonType="Image">
            <%-- <NewButton Visible="True" Image-ToolTip="Add New Item" Image-SpriteProperties-CssClass="new-icon"  />--%>
            <EditButton Visible="true" Image-ToolTip="Edit Item Details" Image-SpriteProperties-CssClass="edit-icon" ></EditButton>
           <%-- <DeleteButton Visible="true" Image-ToolTip="Delete Item" Image-SpriteProperties-CssClass="delete-icon" ></DeleteButton>--%>
            <UpdateButton Image-ToolTip="Save Item" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/Update1.PNG"></UpdateButton>
            <CancelButton Image-ToolTip="Cancel" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/CancelHover.PNG"></CancelButton>
        </dx:GridViewCommandColumn>
    </Columns>
    <SettingsPopup>
        <EditForm Width="600" Modal="true" ShowHeader="false" />
    </SettingsPopup>
    <SettingsEditing Mode="Inline" EditFormColumnCount="1" />
    <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true" ProcessSelectionChangedOnServer="true" ConfirmDelete="true"  />
    <SettingsDetail ShowDetailRow="true" />
    <%--  <SettingsLoadingPanel Text="Loading..."></SettingsLoadingPanel>--%>

    <Styles>
        <AlternatingRow Enabled="false"></AlternatingRow>
        <Row BackColor="#FFFFFF"></Row>
        <CommandColumnItem Spacing="10" Paddings-PaddingRight="10px"></CommandColumnItem>
    </Styles>
    <Templates>
        <DetailRow>
            <div style="padding: 5px; text-align: left;">
                <dx:ASPxPageControl runat="server" ID="pageControl" Theme="AnritsuDevXTheme" Width="100%" EnableCallBacks="true" ShowLoadingPanel="true" >
                    <ContentStyle>
                    </ContentStyle>
                    <TabPages>
                        <dx:TabPage Text='<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ItemDetailsTab.Text%>' Visible="True">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server">
                                    <table class="templateTable">
                                        <tr>
                                            <td >
                                                <asp:Literal ID="ltrlModelTarget" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,JPExport_ModelTarget.Text%>" />:
                                             <%--   Export Control Type: Text='<%# Eval("JPSupportContract", "{0:d}") %>'--%>
                                                
                                            </td>
                                           <td colspan="3" class="value">
                                               <dx:ASPxLabel ID="lblJPExport_ModelTarget" runat="server" Text='<%# GetSupportTypes(Eval("ModelNumber"),Eval("SerialNumber"),Eval("AddedBy"))%>' />
                                                
                                               
                                           </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ltrlModelOptions" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ltrlModelOptions.Text %>" />:
                                            </td>
                                            <td class="value">
                                                <ul>
                                                    <li style="display: inline-block; list-style-type: none; padding-right: 10px;">
                                                        <dx:ASPxCheckBox ID="cbxInclTaggedMN" runat="server" Checked='<%# Eval("InclResrc_TaggedMN") %>' Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,InclResrc_TaggedMN.Text %>" ReadOnly="true" Border-BorderStyle="None"></dx:ASPxCheckBox>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td>
                                                <asp:Literal ID="ltrlApprovedOn" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ltrlApprovedOn.Text  %>" />:
                                            </td>
                                            <td class="value">
                                                <dx:ASPxLabel ID="lblApprovedOn" runat="server" Text='<%# Eval("ApprovedOnUTC", "{0:d}") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ltrlCreatedBy" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,AddedBy.Text %>" />:
                                            </td>
                                            <td class="value">
                                                <dx:ASPxLabel ID="lblAddedBy" runat="server" Text='<%# Eval("AddedBy") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal ID="ltrlUpdatedBy" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,UpdatedBy.Text %>" />:
                                            </td>

                                            <td class="value">
                                                <dx:ASPxLabel ID="lblUpdatedBy" runat="server" Text='<%# Eval("UpdatedBy") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ltrlCreatedOn" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,CreatedOnUTC.Text %>" />:
                                            </td>

                                            <td class="value">
                                                <dx:ASPxLabel ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOnUTC", "{0:d}") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal ID="ltrlModifiedOn" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdReg_Item_JPDLCtrl,ModifiedOnUTC.Text %>" />:
                                            </td>
                                            <td class="value">
                                                <dx:ASPxLabel ID="lblModifiedOn" runat="server" Text='<%# Eval("ModifiedOnUTC", "{0:d}") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                      
                    </TabPages>
                </dx:ASPxPageControl>
            </div>
        </DetailRow>
    </Templates>
</dx:ASPxGridView>
<asp:SqlDataSource ID="sdsProdRegItems" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForJPSW_SelectByWebToken]"
    UpdateCommandType="StoredProcedure" UpdateCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForJPSW_Update]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForJPSW_Delete]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForJPSW_Insert]"
    OnInserting="sdsProdRegItems_Inserting" OnUpdating="sdsProdRegItems_Updating"
    OnInserted="sdsProdRegItems_Inserted" OnUpdated="sdsProdRegItems_Updated" OnDeleted="sdsProdRegItems_Deleted">
    <SelectParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
        <asp:QueryStringParameter Name="ModelConfigType" QueryStringField="mdlcfg" Type="String"/>
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
        <asp:Parameter Name="ModelNumber" Type="String" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="StatusCode" Type="String" />
        <asp:Parameter Name="InclResrc_TaggedMN" Type="Boolean" />
        <asp:Parameter Name="UpdatedBy" Type="String" />
        <asp:Parameter Name="JPExport_ModelTarget" Type="String" />
        <asp:Parameter Name="JPSupportContract" Type="DateTime" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
        <asp:Parameter Name="ModelNumber" Type="String" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="StatusCode" Type="String" ConvertEmptyStringToNull="true" />
        <asp:Parameter Name="InclResrc_TaggedMN" Type="Boolean" DefaultValue="true" />
        <asp:Parameter Name="AddedBy" Type="String" />
        <asp:parameter name="jpexport_modeltarget" type="string" />
        <asp:parameter name="jpsupportcontract" type="datetime" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupProdRegJPExportType" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_JPExportType_SelectAll]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupProdRegStatus" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_Status_SelectAll]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupUSBMaster" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_JapanUSBDongle_SelectAll]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupRegistratrableProducts" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectActiveByModelConfigType]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>">
    <SelectParameters>
        <%--<asp:Parameter Name="ModelConfigType" DbType="String" DefaultValue="downloads-jp" />--%>
        <asp:QueryStringParameter Name="ModelConfigType" QueryStringField="mdlcfg" Type="String"/>
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupUnAssignedUserACL" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_SelectUnAssignedAccountMembers]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>">
    <SelectParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupUserACLStatus" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_Status]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<asp:SqlDataSource ID="sdsProdRegItemUSB" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_JPUSB_Select]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_JPUSB_Insert]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_JPUSB_Delete]" OnInserting="sdsProdRegItemUSB_Inserting">
    <SelectParameters>
        <asp:SessionParameter Name="ProdRegItemID" SessionField="ssProdReg_Item_JPDLCtrl_SelectedProdRegItem" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
        <asp:Parameter Name="USBNo" Type="String" />
        <asp:Parameter Name="AssignedBy" Type="String" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="JPUsbRegItemID" Type="Int32" />
    </DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsProdRegItemUserACL" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_Select]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_Insert]"
    UpdateCommandType="StoredProcedure" UpdateCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_SetApprove]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_UserACL_Delete]">
    <SelectParameters>
        <asp:SessionParameter Name="ProdRegItemID" SessionField="ssProdReg_Item_JPDLCtrl_SelectedProdRegItem" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
        <asp:Parameter Name="UserMembershipId" DbType="Guid" />
        <asp:Parameter Name="ACLStatusCode" Type="String" />
        <asp:Parameter Name="IsRegistrationSubmitter" DbType="Boolean" DefaultValue="false" />
        <asp:Parameter Name="ApprovedBy" Type="String" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="ProdRegUserACLID" Type="Int32" />
        <asp:Parameter Name="ApprovedBy" Type="String" />
    </UpdateParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProdRegUserACLID" Type="Int32" />
    </DeleteParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsProdRegItemPrivateNotes" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_PrivateNotes_Select]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_PrivateNotes_Insert]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_PrivateNotes_Delete]">
    <SelectParameters>
        <asp:SessionParameter Name="ProdRegItemID" SessionField="ssProdReg_Item_JPDLCtrl_SelectedProdRegItem" Type="Int32" />
        <asp:Parameter Name="NoteType" Type="String" DefaultValue="<% ItemType %>" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
        <asp:Parameter Name="NoteType" Type="String" DefaultValue="<% ItemType %>" />
        <asp:Parameter Name="PrivateNote" Type="String" />
        <asp:Parameter Name="NoteAddedBy" Type="String" />
    </InsertParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProdRegItemNoteID" Type="Int32" />
    </DeleteParameters>
</asp:SqlDataSource>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_Item_JPDLCtrl" />
