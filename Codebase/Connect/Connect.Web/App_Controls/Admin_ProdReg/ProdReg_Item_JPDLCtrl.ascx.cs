﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;
using Anritsu.Connect.Lib.ProductRegistration;
using EM = Anritsu.AnrCommon.EmailSDK;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_Item_JPDLCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ModelConfigType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback && !IsPostBack)
            {
                if (ModelConfigType.IsNullOrEmptyString()) throw new ArgumentNullException("ModelConfigType");

                gvItems.DataBind();
                //gvItems.DetailRows.ExpandRow(0);
                gvItems.DetailRows.ExpandAllRows();
            }

            if (!IsPostBack)
            {
                // gvItems.StartEdit(0);
            }
        }



        protected void gvItem_JPUSB_DataSelect(object sender, EventArgs e)
        {
            Session["ssProdReg_Item_JPDLCtrl_SelectedProdRegItem"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_Item_JPDLCtrl"; }

        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void sdsProdRegItems_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            ProdReg_Master prm = Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, false);
            if (prm == null) throw new HttpException(404, "Invalid token.");

            if (e.Command.Parameters["@ModelNumber"].Value != null && e.Command.Parameters["@SerialNumber"].Value != null)
            {

                List<ProdReg_Item> itemList = ProdReg_ItemBLL.SelectWithFilter(prm.ProdRegID
                    , Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_JP
                    , e.Command.Parameters["@ModelNumber"].Value.ToString()
                    , e.Command.Parameters["@SerialNumber"].Value.ToString());
                if (itemList.Count == 0)
                {
                    e.Command.Parameters["@AddedBy"].Value = LoginUtility.GetCurrentUserOrSignIn().Email;
                }
                else
                    throw new ArgumentException(GetStaticResource("MNSNExistsMsg"));
            }
            else
                throw new ArgumentException(GetStaticResource("EnterMNSNMsg"));
        }

        private Lib.ProductRegistration.ProdReg_Item prItem;
        private ProdReg_Master prm;
        protected void sdsProdRegItems_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {
            prm = Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, false);
            prItem = Lib.ProductRegistration.ProdReg_ItemBLL.SelectByProdRegItemId(Convert.ToInt32(e.Command.Parameters["@ProdRegItemID"].Value));
            List<ProdReg_Item> itemList = Lib.ProductRegistration.ProdReg_ItemBLL.SelectWithFilter(prm.ProdRegID
                , Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_JP
                , e.Command.Parameters["@ModelNumber"].Value.ToString()
                , e.Command.Parameters["@SerialNumber"].Value.ToString());
            if (itemList.Count > 1 || (itemList.Count == 1 && !prItem.SerialNumber.Equals(e.Command.Parameters["@SerialNumber"].Value.ToString())))
            {
                throw new ArgumentException(GetStaticResource("MNSNExistsMsg"));
            }
        }

        protected void sdsProdRegItemUSB_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            e.Command.Parameters["@AssignedBy"].Value = LoginUtility.GetCurrentUserOrSignIn().Email;
        }

        protected void gvItem_JPUSB_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["ProdRegItemID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gvItems_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Detail)
            {
                ASPxPageControl pageControl = gvItems.FindDetailRowTemplateControl(e.VisibleIndex, "pageControl") as ASPxPageControl;
                // pageControl.TabPages.FindByName("tabUserACL").Visible = ConvertUtility.ConvertToBoolean(e.GetValue("UseUserACL"), false);
            }
        }

        protected void gvItems_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "JPExport_ModelTarget")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.Items.Clear();
                DataTable tb = Lib.ProductRegistration.Acc_ProductReg_Item_JPExportTypeBLL.SelectAllExportType();

                foreach (DataRow r in tb.Rows)
                {
                    ListEditItem item = new ListEditItem(GetExportType(r["JPExportCode"].ToString().ToUpper()), r["JPExportCode"].ToString());
                    cmb.Items.Add(item);
                }

                ListEditItem emptyItem = new ListEditItem("None", "");
                cmb.Items.Add(emptyItem);

                if (e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
                {
                    cmb.SelectedItem.Text = GetExportType(e.Value.ToString().ToUpper());
                }
                else
                    cmb.SelectedItem = emptyItem;

            }
            if (e.Column.FieldName == "ModelNumber" || e.Column.FieldName == "SerialNumber")
                e.Editor.ClientEnabled = false;
        }

        protected void gvItem_UserACL_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["ssProdReg_Item_JPDLCtrl_SelectedProdRegItem"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void gvItem_UserACL_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["ProdRegItemID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
            e.NewValues["ApprovedBy"] = LoginUtility.GetCurrentUserOrSignIn().Email;

        }

        protected void gvItem_UserACL_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView gvItem_UserACL = (sender as ASPxGridView);

            if (!gvItem_UserACL.IsEditing || e.Column.FieldName != "UserMembershipId") return;
            int prodRegItemID = ConvertUtility.ConvertToInt32(gvItem_UserACL.GetMasterRowKeyValue(), 0);
            if (prodRegItemID < 1) return;

            sdsLookupUnAssignedUserACL.SelectParameters["ProdRegItemID"].DefaultValue = prodRegItemID.ToString();
            sdsLookupUnAssignedUserACL.Select(DataSourceSelectArguments.Empty);


        }

        protected void gvItem_UserACL_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (e.ButtonID == "gvcccbttApprove")
            {
                ASPxGridView gvItem_UserACL = (sender as ASPxGridView);
                object aclStatusCode = gvItem_UserACL.GetRowValues(e.VisibleIndex, "ACLStatusCode");
                if (aclStatusCode != null && aclStatusCode.ToString().Equals("active", StringComparison.InvariantCultureIgnoreCase))
                    e.Visible = DevExpress.Utils.DefaultBoolean.False;
                else
                    e.Visible = DevExpress.Utils.DefaultBoolean.True;

            }
        }

        protected void gvItem_UserACL_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID == "gvcccbttApprove")
            {
                ASPxGridView gvItem_UserACL = (sender as ASPxGridView);
                Int32 prodRegUserACLID = ConvertUtility.ConvertToInt32(gvItem_UserACL.GetRowValues(e.VisibleIndex, "ProdRegUserACLID"), 0);
                sdsProdRegItemUserACL.UpdateParameters["ProdRegUserACLID"].DefaultValue = prodRegUserACLID.ToString();
                sdsProdRegItemUserACL.UpdateParameters["ApprovedBy"].DefaultValue = LoginUtility.GetCurrentUserOrSignIn().Email;
                sdsProdRegItemUserACL.Update();
                gvItem_UserACL.DataBind();
            }
        }

        protected void gvItems_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["InclResrc_PrimaryMN"] = true;
            e.NewValues["InclResrc_TaggedMN"] = true;
        }

        protected void gvPrivateItemNotes_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            e.NewValues["ProdRegItemID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
            e.NewValues["NoteAddedBy"] = LoginUtility.GetCurrentUserOrSignIn().Email;

            ASPxGridView gvPrivateItemNotes = (sender as ASPxGridView);
            ASPxMemo dxMemoItemPrivateNote = gvPrivateItemNotes.FindEditFormTemplateControl("dxMemoItemPrivateNote") as ASPxMemo;
            e.NewValues["PrivateNote"] = dxMemoItemPrivateNote.Text.Trim();
        }

        /// <summary>
        /// Returns Export Type description
        ///• UG--> Paid -- Restricted & non-restricted files
        ///• MG--> Non-Paid -- Restricted & non-restricted files
        ///• UH--> Paid -- Non-Restricted files ONLY
        ///• MH--> Non-Paid -- Non-Restricted files ONLY
        /// </summary>
        /// <param name="SupportType"></param>
        /// <returns></returns>
        public String GetExportType(object supportType)
        {
            String resourceKey = ConvertUtility.ConvertNullToEmptyString(supportType).Trim();
            if (String.IsNullOrEmpty(resourceKey)) return String.Empty;
            return GetGlobalResourceObject("common", resourceKey).ToString();
        }


        public string GetSupportTypes(object model, object serial, object email)
        {
            Guid orgId = Guid.Parse(Request.QueryString["rwak"]);
            return Convert.ToString(BLL_SupportEntry.GetSupportTypesByModelSerial(Convert.ToString(model), Convert.ToString(serial), Convert.ToString(email),orgId));
        }
        /// <summary>
        /// Return Support exp date according to model type
        /// </summary>
        /// <param name="modelType"></param>
        /// <param name="supportExpDt"></param>
        /// <returns></returns>
        public String GetSupportExpDt(object isPaidModelType, object supportExpDt)
        {
            Boolean isPaidModel = ConvertUtility.ConvertToBoolean(isPaidModelType, false);
            String supportDt = ConvertUtility.ConvertNullToEmptyString(supportExpDt).Trim();
            if (!isPaidModel)
            {
                supportDt = string.Empty;
            }

            return supportDt;
        }

        public String GetUrl()
        {
            String url = String.Format("~/siteadmin/usbkeyadmin/usbaddnew?{0}={1}"
                , KeyDef.QSKeys.ReturnURL
                , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}"
                    , KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_JPSW
                    , KeyDef.QSKeys.RegisteredProductWebAccessKey
                    , ProdRegMaster_WebToken.ToString())));

            return url;
        }

        protected void sdsProdRegItems_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
        }

        protected void sdsProdRegItems_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
           
        }

        protected void sdsProdRegItems_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, true);
        }

        public void SendEmails(string newStatus)
        {
            #region " send submitted email to customer "
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 gwCultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            List<EM.EmailWebRef.ReplaceKeyValueType> emailData = Email_GetKeyValues(user,gwCultureGroupId,newStatus);
            if (emailData == null) return;

            EM.EmailWebRef.SendTemplatedEmailCallRequest reqUserSubmittedAckEmail =
                            new EM.EmailWebRef.SendTemplatedEmailCallRequest();
            reqUserSubmittedAckEmail.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegEmail.ChangeStatus");// replace with Connect.Web.ProdRegEmail.UserSubmittedAck when JP goes live
            reqUserSubmittedAckEmail.ToEmailAddresses = new string[] {prm.CreatedBy};
            //reqUserSubmittedAckEmail.CultureGroupId = prm.;
            reqUserSubmittedAckEmail.SubjectReplacementKeyValues = emailData.ToArray();
            reqUserSubmittedAckEmail.BodyReplacementKeyValues = emailData.ToArray();
          //  var response= EM.EmailServiceManager.SendTemplatedEmail(reqUserSubmittedAckEmail);
            #endregion


        }
        private List<EM.EmailWebRef.ReplaceKeyValueType> Email_GetKeyValues(App_Lib.AnrSso.ConnectSsoUser user,Int32 gwCultureGroupId,string newStatus)
        {
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;
            if (prItem != null)
            {
                #region " product information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[ModelNumber]]";
                kv.ReplacementValue = prItem.ModelNumber;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SerialNumbers]]";
                kv.ReplacementValue = prItem.SerialNumber;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[OLDSTATUS]]";
                kv.ReplacementValue = prItem.StatusCode.ToString();
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[NEWSTATUS]]";
                kv.ReplacementValue = newStatus;
                kvList.Add(kv);
                #endregion

            }

            if (user != null)
            {
                #region " user information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
                kvList.Add(kv);
                #endregion
            }

            return kvList;
        }
    }
}