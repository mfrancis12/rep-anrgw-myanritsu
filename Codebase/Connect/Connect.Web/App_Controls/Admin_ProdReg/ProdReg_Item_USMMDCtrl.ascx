﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_Item_USMMDCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_Item_USMMDCtrl" %>

<dx:ASPxGridView ID="gvItems" ClientInstanceName="gvItems" runat="server" DataSourceID="sdsProdRegItems" Theme="AnritsuDevXTheme" KeyFieldName="ProdRegItemID"
    Width="100%" EnableRowsCache="false" Settings-ShowColumnHeaders="true"
    SettingsText-CommandCancel="Cancel" SettingsText-CommandUpdate="Update" SettingsText-CommandDelete="Delete"
    SettingsText-PopupEditFormCaption="Update Item" OnInitNewRow="gvItems_InitNewRow">
    <Columns>
        <dx:GridViewDataColumn FieldName="ProdRegItemID" VisibleIndex="1" ReadOnly="true" EditFormSettings-Visible="False" Visible="false" />
        <dx:GridViewDataComboBoxColumn FieldName="ModelNumber" VisibleIndex="2" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_USMMDCtrl,gvItems.ModelNumber %>'>
            <PropertiesComboBox TextField="ModelNumber" ValueField="ModelNumber" DataSourceID="sdsLookupRegistratrableProducts" IncrementalFilteringMode="Contains"></PropertiesComboBox>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_USMMDCtrl,gvItems.SerialNumber %>' />
        <dx:GridViewDataColumn FieldName="StatusCode" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_USMMDCtrl,gvItems.StatusCode %>' EditFormSettings-Visible="False"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="InclResrc_TaggedMN" VisibleIndex="5" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_USMMDCtrl,gvItems.InclResrc_TaggedMN %>'></dx:GridViewDataColumn>
        <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="90" ReadOnly="true" Caption='<%$ Resources:ADM_STCTRL_ProdReg_Item_USMMDCtrl,gvItems.CreatedOnUTC %>'>
            <PropertiesDateEdit DisplayFormatString="d"></PropertiesDateEdit>
            <EditFormSettings Visible="False" />
        </dx:GridViewDataDateColumn>
        <%--<dx:GridViewCommandColumn VisibleIndex="100" ButtonType="Image">
          <NewButton Visible="True" Image-ToolTip="Add New Item" Image-SpriteProperties-CssClass="new-icon"  />
            <EditButton Visible="true" Image-ToolTip="Edit Item Details" Image-SpriteProperties-CssClass="edit-icon"></EditButton>
            <DeleteButton Visible="true" Image-ToolTip="Delete" Image-SpriteProperties-CssClass="delete-icon"></DeleteButton>
            <UpdateButton Image-ToolTip="Save Item" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/Update1.PNG"></UpdateButton>
            <CancelButton Image-ToolTip="Cancel" Image-Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/CancelHover.PNG"></CancelButton>
        </dx:GridViewCommandColumn>--%>
    </Columns>
    <SettingsPopup>
        <EditForm Width="600" Modal="true" ShowHeader="false" />
    </SettingsPopup>
    <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1" />
    <SettingsBehavior AllowSelectByRowClick="true" AllowFocusedRow="true" ProcessSelectionChangedOnServer="true" ConfirmDelete="true" />
    <Styles>
        <AlternatingRow Enabled="false"></AlternatingRow>
        <Row BackColor="#FFFFFF"></Row>
        <CommandColumnItem Spacing="10" Paddings-PaddingRight="10px"></CommandColumnItem>
    </Styles>
</dx:ASPxGridView>
<asp:SqlDataSource ID="sdsProdRegItems" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_SelectByWebToken]"
    DeleteCommandType="StoredProcedure" DeleteCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForUSMMD_Delete]"
    InsertCommandType="StoredProcedure" InsertCommand="[CNT].[uSP_Acc_ProductRegistered_Item_ForUSMMD_Insert]"
    OnInserting="sdsProdRegItems_Inserting">
    <SelectParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ProdRegItemID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
        <asp:Parameter Name="ModelNumber" Type="String" />
        <asp:Parameter Name="SerialNumber" Type="String" />
        <asp:Parameter Name="InclResrc_TaggedMN" Type="Boolean" DefaultValue="true" />
        <asp:Parameter Name="AddedBy" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupRegistratrableProducts" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Cfg_ModelConfig_SelectActiveByModelConfigType]">
    <SelectParameters>
        <asp:Parameter Name="ModelConfigType" DbType="String" DefaultValue="downloads-us" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sdsLookupProdRegStatus" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Item_Status_SelectAll]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_Item_USMMDCtrl" />
