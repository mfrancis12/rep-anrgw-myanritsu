﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_Item_USMMDCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ItemType { get; set; }

        private Guid WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsCallback && !IsPostBack)
            {
                if (ItemType.IsNullOrEmptyString()) throw new ArgumentNullException("ItemType");

                gvItems.DataBind();
                //gvItems.DetailRows.ExpandRow(0);
                gvItems.DetailRows.ExpandAllRows();
            }

            if (!IsPostBack)
            {
                // gvItems.StartEdit(0);
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_Item_USMMDCtrl"; }

        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void sdsProdRegItems_Inserting(object sender, SqlDataSourceCommandEventArgs e)
        {
            ProdReg_Master prm = ProdReg_MasterBLL.SelectByWebToken(WebToken);
            if (prm == null) throw new HttpException(404, "Invalid token.");
            if (e.Command.Parameters["@ModelNumber"].Value!=null && e.Command.Parameters["@SerialNumber"].Value!=null)
            {
                List<ProdReg_Item> itemList = ProdReg_ItemBLL.SelectWithFilter(prm.ProdRegID
                    , Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_US_MMD
                    , e.Command.Parameters["@ModelNumber"].Value.ToString()
                    , e.Command.Parameters["@SerialNumber"].Value.ToString());

                if (itemList.Count == 0)
                {
                    e.Command.Parameters["@AddedBy"].Value = LoginUtility.GetCurrentUserOrSignIn().Email;
                    //e.Command.Parameters["@InclResrc_TaggedMN"].Value = true;
                }
                else
                    throw new ArgumentException(GetStaticResource("MNSNExistsMsg"));
            }
            else
                throw new ArgumentException(GetStaticResource("EnterMNSNMsg"));
        }

        protected void gvItems_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            e.NewValues["InclResrc_TaggedMN"] = true;
        }

    }
}