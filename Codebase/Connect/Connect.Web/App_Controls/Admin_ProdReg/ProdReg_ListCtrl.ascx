﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_ListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_ListCtrl" %>
<dx:ASPxGridView ID="gvProdRegSearchResults" ClientInstanceName="gvProdRegSearchResults" runat="server" DataSourceID="sdsProdRegSearchResults" Theme="AnritsuDevXTheme" 
        Width="100%" AutoGenerateColumns="False" KeyFieldName="ProdRegID" SettingsBehavior-AutoExpandAllGroups="false">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="ProdRegID" VisibleIndex="0" Visible="false"></dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="WebToken" VisibleIndex="1" Visible="false"></dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="1" GroupIndex="1" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,CompanyName.Text %>'></dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="AccountName" VisibleIndex="2" GroupIndex="2" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,AccountName.Text %>'></dx:GridViewDataTextColumn>
        <dx:GridViewDataHyperLinkColumn FieldName="WebToken" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,MasterModel.Text %>' PropertiesHyperLinkEdit-NavigateUrlFormatString="~/prodregadmin/manage_registration?rwak={0}" PropertiesHyperLinkEdit-TextField="MasterModel"></dx:GridViewDataHyperLinkColumn>
        <dx:GridViewDataTextColumn FieldName="MasterSerial" VisibleIndex="5" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,MasterSerial.Text %>'></dx:GridViewDataTextColumn>
        <dx:GridViewDataColumn FieldName="AdminTypes" VisibleIndex="6" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,Admins.Text  %>'></dx:GridViewDataColumn>
        <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="500" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,CreatedOnUTC.Text %>'>
            <PropertiesDateEdit DisplayFormatString="d">
            </PropertiesDateEdit>
        </dx:GridViewDataDateColumn>

    </Columns>
    <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowFooter="true" />
    <SettingsPager PageSize="30"></SettingsPager>
  <%--  <SettingsLoadingPanel Text="Loading..."></SettingsLoadingPanel>--%>
  <%--  <SettingsText GroupContinuedOnNextPage="Continued On Next Page..." />--%>
</dx:ASPxGridView>
<asp:SqlDataSource ID="sdsProdRegSearchResults" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    OnSelecting="sdsProdRegSearchResults_Selecting"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Master_SelectWithFilter]">
    <SelectParameters>
        <asp:Parameter Name="AccountID" DbType="Guid" />
        <asp:Parameter Name="WebToken" DbType="Guid" />
        <asp:Parameter Name="ModelNumber" DbType="String" />
        <asp:Parameter Name="SerialNumber" DbType="String" />
        <asp:Parameter Name="ItemStatusCode" DbType="String" />
        <asp:Parameter Name="RegCreatedBy" DbType="String" />
        <asp:Parameter Name="UserEmail" DbType="String" />
        <asp:Parameter Name="OrgName" DbType="String" />
        <asp:Parameter Name="CompanyName" DbType="String" />
    </SelectParameters>
</asp:SqlDataSource>
