﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_ListCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //App_Lib.LinqData.LqtSql_ProdReg_MasterDataContext db = new App_Lib.LinqData.LqtSql_ProdReg_MasterDataContext();
            //Lib.ProductRegistration.Acc_ProductRegisteredBLL.AdminProdRegSearch(UIHelper.AdminProdRegSearchOptionsGet(false));
        }

        protected void ldsProdRegMaster_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            e.KeyExpression = "ProdRegID";
        }

        protected void sdsProdRegSearchResults_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            List<Lib.SearchByOption> searchOptions = Admin_SearchUtility.AdminProdRegSearchOptionsGet(false);
            if (searchOptions == null) Response.Redirect(KeyDef.UrlList.SiteAdminPages.AdminProdRegSearch);

            e.Command.Parameters["@AccountID"].Value = DBNull.Value;
            e.Command.Parameters["@WebToken"].Value = DBNull.Value;
            e.Command.Parameters["@ModelNumber"].Value = DBNull.Value;
            e.Command.Parameters["@SerialNumber"].Value = DBNull.Value;
            e.Command.Parameters["@ItemStatusCode"].Value = DBNull.Value;
            e.Command.Parameters["@RegCreatedBy"].Value = DBNull.Value;
            e.Command.Parameters["@UserEmail"].Value = DBNull.Value;
            e.Command.Parameters["@OrgName"].Value = DBNull.Value;
            e.Command.Parameters["@CompanyName"].Value = DBNull.Value;

            foreach (Lib.SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    //case "ItemType":
                    //    e.Command.Parameters["@ItemType"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                    //    break;

                    case "AccountID":
                        e.Command.Parameters["@AccountID"].Value = ConvertUtility.ConvertToGuid(sbo.SearchObject, Guid.Empty);
                        break;
                    case "WebToken":
                        e.Command.Parameters["@WebToken"].Value = ConvertUtility.ConvertToGuid(sbo.SearchObject, Guid.Empty);
                        break;
                    case "MN":
                        e.Command.Parameters["@ModelNumber"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "SN":
                        e.Command.Parameters["@SerialNumber"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "ItemStatusCode":
                        e.Command.Parameters["@ItemStatusCode"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "UserEmail":
                        e.Command.Parameters["@UserEmail"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "RegCreatedBy":
                        e.Command.Parameters["@RegCreatedBy"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "OrgName":
                        e.Command.Parameters["@OrgName"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        e.Command.Parameters["@CompanyName"].Value = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                }

            }

        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}