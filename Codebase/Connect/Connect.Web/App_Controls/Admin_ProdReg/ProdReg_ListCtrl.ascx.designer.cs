﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg {
    
    
    public partial class ProdReg_ListCtrl {
        
        /// <summary>
        /// gvProdRegSearchResults コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView gvProdRegSearchResults;
        
        /// <summary>
        /// sdsProdRegSearchResults コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource sdsProdRegSearchResults;
    }
}
