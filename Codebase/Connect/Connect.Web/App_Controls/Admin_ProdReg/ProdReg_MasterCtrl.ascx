﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_MasterCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_MasterCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_MasterCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalMasterMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalMasterMN.Text%>"></asp:Localize>:</span>
            <asp:Label ID="lblMasterMN" runat="server" Text=""></asp:Label>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalMasterSN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalMasterSN.Text%>"></asp:Localize>:</span>
            <asp:Label ID="lblMasterSN" runat="server" Text=""></asp:Label>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalCreatedBy.Text%>"></asp:Localize>:</span>
        <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label>
    </div>
    <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalCreatedOn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalCreatedOn.Text%>"></asp:Localize>:</span>
        <asp:Label ID="lblCreatedOnUTC" runat="server" Text=""></asp:Label>
    </div>
    <div class="settingrow margin-top-15">
        <p class="msg">
            <asp:Literal ID="ltrProductRegMasterMsg" runat="server"></asp:Literal>
        </p>
        <asp:Button ID="bttProdRegMaster_Delete" Visible=false runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,bttProdRegMaster_Delete.Text%>" CausesValidation="false" OnClick="bttProdRegMaster_Delete_Click" />
        <asp:Repeater ID="repDelete" Visible="false" runat="server"
            OnItemDataBound="repDelete_ItemDataBound" OnItemCommand="repDelete_ItemCommand">
            <ItemTemplate>           
                <div>                
                     <asp:button ID="btnDeleteAdmins" runat="server" style="float: left; margin-left: 10px" CausesValidation="false"
                         modelConfigType='<%#Eval("ModelConfigType") %>' accountId ='<%# Eval("AccountId") %>' 
                         prodRegItemID='<%#Eval("ProdRegItemID") %>'/>
                </div>            
            </ItemTemplate>
        </asp:Repeater>        
    </div>
    <div class="settingrow margin-top-15">
        <div style="margin-bottom:10px;font-weight:bold">
            <asp:Localize ID="lcalProdRegDataTitle" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalProdRegDataTitle.Text%>"></asp:Localize>
        </div>
        <dx:ASPxGridView ID="dxgvProdReg_Data" runat="server" ClientInstanceName="dxgvProdReg_Data" KeyFieldName="ProdRegDataID" DataSourceID="odsProdRegData" Width="100%">
            <Columns>
                <dx:GridViewDataColumn FieldName="ProdRegDataID" Visible="false"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="DataKey" Caption="Field " VisibleIndex="5" Width="30%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="DataValue" Caption="Information " VisibleIndex="20"></dx:GridViewDataColumn>
            </Columns>
        </dx:ASPxGridView>
    </div>
    <asp:HiddenField ID="hdnIsFinalDelete" runat="server" />
    <asp:HiddenField ID="hdnCompanyName" runat="server" />
    <asp:HiddenField ID="hdnTeamName" runat="server" />
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_MasterCtrl" />
</anrui:GlobalWebBoxedPanel>

<asp:SqlDataSource ID="sdsLookupRegistratrableProducts" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Erp_ProductConfig_SelectMasterModels]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>
<asp:ObjectDataSource ID="odsProdRegData" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_ProdReg.ODS_ProdReg_Data" 
    EnablePaging="false" EnableCaching="true" 
    CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdRegDataCKD"
    SelectMethod="ProdReg_Data_Select" OnSelecting="odsProdRegData_Selecting">
<SelectParameters>
    <asp:Parameter Name="WebToken" DbType="Guid"/>
</SelectParameters>
</asp:ObjectDataSource>

