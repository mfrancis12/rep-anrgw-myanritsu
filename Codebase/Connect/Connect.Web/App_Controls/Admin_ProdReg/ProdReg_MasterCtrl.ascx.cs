﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_MasterCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);            
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ProdRegMaster_WebToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Invalid token.");
                HandleODSCache();
                InitRegMasterInfo();
                ManageDeleteButtons();
            }            
        }

        private void HandleODSCache()
        {
            String obds_ProdRegDataCacheKey = ProdRegMaster_WebToken.ToString();
            if (Cache[odsProdRegData.CacheKeyDependency] == null) Cache[odsProdRegData.CacheKeyDependency] = obds_ProdRegDataCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsProdRegData.CacheKeyDependency].ToString().Equals(obds_ProdRegDataCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsProdRegData.CacheKeyDependency);
                Cache[odsProdRegData.CacheKeyDependency] = obds_ProdRegDataCacheKey;
            }
        }

        private void InitRegMasterInfo()
        {
            ProdReg_Master prm = ProdReg_MasterBLL.SelectByWebToken(ProdRegMaster_WebToken);
            if (prm == null) throw new HttpException(404, "Invalid token.");

            //dxcmbMasterModel.Value = prm.MasterModel.Trim();
            lblMasterMN.Text = prm.MasterModel.Trim();
            lblMasterSN.Text = prm.MasterSerial;
            //txtDesc.Text = prm.Description;
            lblCreatedBy.Text = prm.CreatedBy;
            lblCreatedOnUTC.Text = prm.CreatedOnUTC.ToStringFormatted();
            Lib.Account.Acc_AccountMaster account = AccountUtility.Admin_AccountInfoGet(prm.AccountID);
            hdnCompanyName.Value = account.CompanyName;
            hdnTeamName.Value = account.AccountName;
            bttProdRegMaster_Delete.Attributes.Add("accountId", account.AccountID.ToString());
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdReg_MasterCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void bttProdRegMaster_Delete_Click(object sender, EventArgs e)
        {
            Boolean isOkToDelete = ProdReg_MasterBLL.CheckIfOkToDelete(ProdRegMaster_WebToken);
            if (!isOkToDelete)
            {
                ltrProductRegMasterMsg.Text = GetStaticResource("ERROR_ItemsExist");
                return;
            }
            else
            {
                App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                ProdReg_MasterBLL.Delete(ProdRegMaster_WebToken);
                Guid accountId = Guid.Parse(bttProdRegMaster_Delete.Attributes["accountId"]);
                Response.Redirect(string.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsDeletedProdRegs, KeyDef.QSKeys.AccountID, accountId));
            }
        }

        protected void odsProdRegData_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["WebToken"] = ProdRegMaster_WebToken;
        }

        protected void ManageDeleteButtons()
        {
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageProductReg))
            {
                DataTable dtItems = ProdReg_MasterBLL.CheckItemsToDelete(ProdRegMaster_WebToken);
                if (dtItems == null || dtItems.Rows.Count < 1)
                {
                    bttProdRegMaster_Delete.Visible = true;
                    bttProdRegMaster_Delete.OnClientClick = GetFinalRegDeleteConfirmMsgAlert(hdnTeamName.Value);
                }
                else
                {
                    bttProdRegMaster_Delete.Visible = false;
                    hdnIsFinalDelete.Value = dtItems.Rows.Count == 1 ? "true" : "false";                    
                    repDelete.DataSource = dtItems;
                    repDelete.DataBind();
                    repDelete.Visible = true;
                    if (dtItems.Rows.Count > 1)
                    {
                        ltrProductRegMasterMsg.Text = GetStaticResource("MultipleProdRegInfoMsg").Replace("[[ITEMS_COUNT]]", dtItems.Rows.Count.ToString());
                    }
                }
            }
        }

        protected void repDelete_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Button btnDelete = (Button)e.Item.FindControl("btnDeleteAdmins");
            btnDelete.Text = GetStaticResource("bttProdRegMaster_Delete_By.Text") +
                GetProdRegAdminTypeDisplayText(btnDelete.Attributes["modelConfigType"]);
            if (IsProdRegRegionalAdmin(btnDelete.Attributes["modelConfigType"]))
            {
                btnDelete.Enabled = true;
                if (hdnIsFinalDelete.Value == "true")
                    btnDelete.OnClientClick = GetFinalRegDeleteConfirmMsgAlert(hdnTeamName.Value);
                else
                    btnDelete.OnClientClick = GetMultipleRegDeleteConfirmMsgAlert(hdnTeamName.Value);
            }
            else
            {
                btnDelete.Enabled = false;
            }
        }

        protected string GetFinalRegDeleteConfirmMsgAlert(string teamName)
        {
            return $"return confirm('{GetStaticResource("ConfirmDeleteRegMsg").Replace("[[TEAM_NAME]]", teamName)}')";
        }

        protected string GetMultipleRegDeleteConfirmMsgAlert(string teamName)
        {
            return $"return confirm('{GetStaticResource("ConfirmDeleteMultipleRegMsg").Replace("[[TEAM_NAME]]", teamName)}')";
        }

        protected void repDelete_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Button btnAdmins = (Button)e.Item.FindControl("btnDeleteAdmins");

            string modelConfigType = btnAdmins.Attributes["modelConfigType"];
            Guid accountId = Guid.Parse(btnAdmins.Attributes["accountId"]);
            int proRegItemId = int.Parse(btnAdmins.Attributes["prodRegItemID"]);
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            ProdReg_ItemBLL.Delete(proRegItemId, user.Email);

            //Removing registration from master upon last Admin delete
            if (hdnIsFinalDelete.Value == "true")
            {                
                ProdReg_MasterBLL.Delete(ProdRegMaster_WebToken);
                Response.Redirect(string.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsDeletedProdRegs, KeyDef.QSKeys.AccountID, accountId));
            }
            else
            {
                ltrProductRegMasterMsg.Text = GetStaticResource("prodRegDeletedMsg").Replace("[[REGIONAL_ADMIN]]", GetProdRegAdminTypeDisplayText(modelConfigType));
                NotifyAdminOnProdRegDeletion(GetProdRegAdminTypeDisplayText(modelConfigType));
            }
            ManageDeleteButtons();
        }

        protected void NotifyAdminOnProdRegDeletion(string deletedByRegionalAdmin)
        {
            List<string> regionalAdminsDistKeysLst = new List<string>();
            List<string> regionalAdminsLst = new List<string>();
            foreach (RepeaterItem ritem in repDelete.Items)
            {
                Button btnDelete = ritem.FindControl("btnDeleteAdmins") as Button;
                regionalAdminsDistKeysLst.Add(ProductConfigUtility.ModelConfigType_GetInfo(btnDelete.Attributes["modelConfigType"]).EmailDistKey_NotifyReg);
                regionalAdminsLst.Add(GetProdRegAdminTypeDisplayText(btnDelete.Attributes["modelConfigType"]));
            }
            NotifyAdminOnProdRegDelettion(deletedByRegionalAdmin, regionalAdminsDistKeysLst, hdnCompanyName.Value, hdnTeamName.Value, regionalAdminsLst);
        }
    }
}