﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_MasterCtrl_JP.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_MasterCtrl_JP" %>
<%@ Register Src="../AddressCtrl.ascx" TagName="AddressCtrl" TagPrefix="uc1" %>

<script type="text/javascript">
    function ShowCompTab() {
        $(".compArea").css("display", 'block');
        $(".divComTabTitle").css("background-color", '#F9F9F9');

        $(".summaryArea").css("display", 'none');
        $(".divSummaryTabTitle").css("background-color", '#D1D2D5');
    }

    function ShowRegistrationSummaryTab() {
        $(".compArea").css("display", 'none');
        $(".divComTabTitle").css("background-color", '#D1D2D5');

        $(".summaryArea").css("display", 'block');
        $(".divSummaryTabTitle").css("background-color", '#F9F9F9');
    }

    function InitTabChangeEvent() {
        $(".compArea").css("display", 'none');
        $(".divComTabTitle").css("background-color", '#D1D2D5');

        $(".summaryArea").css("display", 'block');
        $(".divSummaryTabTitle").css("background-color", '#F9F9F9');
    }
</script>


<div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 100px;">
    <div class="TabTittles">
        <asp:Literal runat="server" ID="litTab" />
        <table>
            <tr style="min-height:50px">
                <td Style="text-align:center;vertical-align:central;">
                    <div style="display:none;" class="divComTabTitle" style="height:50px;width:100px; border-top-left-radius: 10px; border-top-right-radius: 10px; border: 1px solid gray; background-color: #D1D2D5;">
                        <asp:HyperLink runat="server" ID="lnkCompTabTitle" NavigateUrl="javascript:ShowCompTab();" Text="<%$Resources:ProductRegAdmin_Tabs,Account%>"></asp:HyperLink>
                    </div>
                </td>
               <%-- <td Style="text-align:center;vertical-align:central">
                    <div class="divSummaryTabTitle" style="height:50px;width:100px;border-top-left-radius: 10px; border-top-right-radius: 10px; border: 1px solid gray; background-color: #F9F9F9;">
                        <asp:HyperLink runat="server" ID="lnkSummaryTabTitle" NavigateUrl="javascript:ShowRegistrationSummaryTab();" Text="<%$Resources:ProductRegAdmin_Tabs,Registration%>"></asp:HyperLink>
                    </div>
                </td>--%>
                <td style="width:100%;border-bottom: 1px solid gray;">

                </td>
            </tr>
        </table>

    </div>
    <div id="tabContent" style="min-height: 300px; margin: 10px; color: Black;">



        <div class="summaryArea">
            <anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_MasterCtrl_JP,pnlContainer.HeaderText %>'>
                <div class="settingrow">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalMasterMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalMasterMN.Text%>"></asp:Localize>:</span>
                    <asp:Label ID="lblMasterMN" runat="server" Text=""></asp:Label>
                </div>
                <div class="settingrow">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalMasterSN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalMasterSN.Text%>"></asp:Localize>:</span>
                    <asp:Label ID="lblMasterSN" runat="server" Text=""></asp:Label>
                </div>
                <div class="settingrow">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalCreatedBy" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalCreatedBy.Text%>"></asp:Localize>:</span>
                    <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label>
                </div>
                <div class="settingrow">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalCreatedOn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalCreatedOn.Text%>"></asp:Localize>:</span>
                    <asp:Label ID="lblCreatedOnUTC" runat="server" Text=""></asp:Label>
                </div>
                <div class="settingrow margin-top-15">
                    <p class="msg">
                        <asp:Literal ID="ltrProductRegMasterMsg" runat="server"></asp:Literal>
                    </p>
                    <asp:Button ID="bttProdRegMaster_Delete" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,bttProdRegMaster_Delete.Text%>" CausesValidation="false" OnClick="bttProdRegMaster_Delete_Click" />
                     <asp:Repeater ID="repDelete" Visible="false" runat="server" OnItemDataBound="repDelete_ItemDataBound" OnItemCommand="repDelete_ItemCommand">
                        <ItemTemplate>           
                            <div>                
                                <asp:button ID="btnDeleteAdmins" runat="server" style="float: left; margin-left: 10px" CausesValidation="false"
                                    modelConfigType='<%#Eval("ModelConfigType") %>' accountId ='<%# Eval("AccountId") %>' 
                                    prodRegItemID='<%#Eval("ProdRegItemID") %>'/>
                            </div>            
                        </ItemTemplate>
                     </asp:Repeater>  
                </div>
                <div class="settingrow margin-top-15">
                    <div style="margin-bottom: 10px; font-weight: bold">
                        <asp:Localize ID="lcalProdRegDataTitle" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl,lcalProdRegDataTitle.Text%>"></asp:Localize>
                    </div>
                    <dx:ASPxGridView ID="dxgvProdReg_Data" runat="server" ClientInstanceName="dxgvProdReg_Data" KeyFieldName="ProdRegDataID" DataSourceID="odsProdRegData" Width="100%">
                        <Columns>
                            <dx:GridViewDataColumn FieldName="ProdRegDataID" Visible="false"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="DataKey" Caption="Field " VisibleIndex="5" Width="30%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="DataValue" Caption="Information " VisibleIndex="20"></dx:GridViewDataColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </div>


                <div class="settingrow margin-top-15">
                    <asp:Button ID="bttModify" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_MasterCtrl_JP,bttModify.Text%>" CausesValidation="false" OnClick="bttModify_Click" Visible="false" />
                </div>


                <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_MasterCtrl" />
            </anrui:GlobalWebBoxedPanel>
            <asp:SqlDataSource ID="sdsLookupRegistratrableProducts" runat="server" SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Erp_ProductConfig_SelectMasterModels]" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"></asp:SqlDataSource>           
            <asp:ObjectDataSource ID="odsProdRegData" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_ProdReg.ODS_ProdReg_Data"
                EnablePaging="false" EnableCaching="true"
                CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdRegDataCKD"
                SelectMethod="ProdReg_Data_Select" OnSelecting="odsProdRegData_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="WebToken" DbType="Guid" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        
        <div class="compArea" style="display: none">
            <anrui:GlobalWebBoxedPanel ID="pnlCompanyInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,pnlCompanyInfo.HeaderText%>"
                Visible="false">
                <div class="width-60">

                    <div class="settingrow group input-text required">
                        <label>
                            <asp:Localize ID="lcalCompName" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompName.Text%>"></asp:Localize></label>
                        <p class="error-message">
                            <asp:Localize ID="lclCompNameErr" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                        </p>
                        <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
                            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
                            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvCompanyName" runat="server"  Display="Dynamic" ValidationGroup="vgCompInfo"
            ControlToValidate="txtCompanyName" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="settingrow required group input-text">
                        <label>
                            <asp:Localize ID="lcalAccountName" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountName.Text%>"></asp:Localize></label>
                        <p class="error-message">
                            <asp:Localize ID="lclAccntName" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                            <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
                                Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
                                ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>
                        </p>
                        <asp:TextBox ID="txtAccountName" runat="server"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvAccountName" runat="server" Display="Dynamic" ValidationGroup="vgCompInfo"
            ControlToValidate="txtAccountName" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="settingrow group input-text" id="divCompanyNameInRuby" runat="server">
                        <label>
                            <asp:Localize ID="lcalRuby" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalRuby.Text%>"></asp:Localize></label>
                        <asp:TextBox ID="txtCompanyNameInRuby" runat="server"></asp:TextBox>
                    </div>
                    <div class="settingrow">
                        <label>
                            <asp:Localize ID="lcalAddress" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAddress.Text%>"></asp:Localize></label>
                        <div>
                            <uc1:AddressCtrl ID="addrCompanyAddress" runat="server" ValidationGroup="vgCompInfo"
                                IsFaxVisible="false" IsFaxRequired="false" IsPhoneVisible="false" IsPhoneRequired="false" />
                        </div>
                    </div>
                </div>
                <div class="settingrow" id="divIsVerified" runat="server">
                    <span class="settinglabelrgt-10">
                        <asp:Localize ID="lcalIsVerified" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalIsVerified.Text%>"></asp:Localize></span>
                    <asp:Image ID="imgAccountVerifiedAndApproved" runat="server" />
                </div>
                <div class="settingrow" id="divAgreedToTerms" runat="server">
                    <span class="settinglabelrgt-10">
                        <asp:Localize ID="lcalAgreeToTerms" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAgreeToTerms.Text%>"></asp:Localize></span>
                    <asp:Literal ID="ltrAgreedToTerms" runat="server"></asp:Literal>
                </div>
                <div class="settingrow" id="divAgreeToTermsOn" runat="server">
                    <span class="settinglabelrgt-10">
                        <asp:Localize ID="lcalAgreedToTermsOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAgreedToTermsOnUTC.Text%>"></asp:Localize></span>
                    <asp:Literal ID="ltrAgreedToTermsOnUTC" runat="server"></asp:Literal>
                </div>
                <div class="settingrow" style="padding-left: 500px">
                    <span class="msg">
                        <asp:Literal ID="ltrCompanyMsg" runat="server"></asp:Literal>
                    </span>
                </div>
                <div class="settingrow">
                    <asp:Button ID="bttDeleteCompany" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttDeleteCompany.Text%>"
                        OnClick="bttDeleteCompany_Click" />
                    <asp:Button ID="bttSaveCompany" runat="server" ValidationGroup="vgCompInfo" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttSaveCompany.Text%>"
                        SkinID="submit-btn" OnClick="bttSaveCompany_Click" />
                </div>
                <div class="settingrow">
                    <fieldset>
                        <legend>
                            <asp:Localize ID="lcalAccountVerifications" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountVerifications.Text%>"></asp:Localize></legend>
                        <p>
                            <asp:Localize ID="lcalAccountVerificationDesc" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAccountVerificationDesc.Text%>"></asp:Localize>
                        </p>
                        <div class="settingrow group input-textarea width-60">
                            <label>
                                <asp:Localize ID="lcalCompanyVerificationInternalComment" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompanyVerificationInternalComment.Text%>"></asp:Localize></label>
                            <asp:TextBox ID="txtCompanyVerificationInternalComment" runat="server" TextMode="MultiLine"
                                Style="vertical-align: middle;" MaxLength="300"></asp:TextBox>
                        </div>
                        <div class="group margin-top-15">
                            <asp:Button ID="bttVerifyAndApproveAccount" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttVerifyAndApproveAccount.Text%>"
                                OnClick="bttVerifyAndApproveAccount_Click" />
                            <asp:Button ID="bttUnVerifyAccount" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttUnVerifyAccount.Text%>"
                                OnClick="bttUnVerifyAccount_Click" />
                        </div>
                        <div class="settingrow">
                            <asp:GridView ID="gvAccountVerifications" runat="server" AutoGenerateColumns="false"
                                Width="720px">
                                <Columns>
                                    <asp:BoundField DataField="ModifiedBy" ItemStyle-Width="200px" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.VerifiedBy.HeaderText%>"
                                        ItemStyle-CssClass="gvstyle1-item-lft" />
                                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.VerifiedOnUTC.HeaderText%>">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltrVerifiedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"ModifiedOnUTC")) %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.ChangedStatus.HeaderText%>">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltrStatus" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "IsVerified") ) %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="InternalComment" HeaderText="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,gvAccountVerifications.InternalComment.HeaderText%>"
                                        ItemStyle-CssClass="gvstyle1-item-lft" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div class="settingrow">
                    <fieldset>
                        <legend>
                            <asp:Localize ID="lcalAdditionalNote" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAdditionalNote.Text%>"></asp:Localize></legend>
                        <label class="">
                            <asp:Localize ID="lcalAdditionalNoteDesc" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalAdditionalNoteDesc.Text%>"></asp:Localize></label>
                        <div class="settingrow group width-60 input-textarea">
                            <label>
                                <asp:Localize ID="lcalCompanyAdditinalNotes" runat="server" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,lcalCompanyAdditinalNotes.Text%>"></asp:Localize></label>
                            <asp:TextBox ID="txtCompanyAdditinalNotes" runat="server" TextMode="MultiLine" Style="vertical-align: middle;"
                                MaxLength="4000"></asp:TextBox>&nbsp;&nbsp;
                
                        </div>
                        <div class="group input-top-15">
                            <asp:Button ID="bttAddNote" runat="server" CausesValidation="false" Text="<%$Resources:ADM_STCTRL_AccountAdmin_DetailCtrl,bttAddNote.Text%>"
                                OnClick="bttAddNote_Click" />
                        </div>
                    </fieldset>
                </div>
            </anrui:GlobalWebBoxedPanel>
            <anrui:StaticResourceEditPanel ID="StaticResourceEditPanel1" runat="server" StaticResourceClassKey="ADM_STCTRL_AccountAdmin_DetailCtrl" />
        </div>
    </div>    
    <asp:HiddenField ID="hdnIsFinalDelete" runat="server" />
    <asp:HiddenField ID="hdnCompanyName" runat="server" />
    <asp:HiddenField ID="hdnTeamName" runat="server" />
</div>
