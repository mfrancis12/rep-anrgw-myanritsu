﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.Account;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_MasterCtrl_JP : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);
            
            bttDeleteCompany.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteOrgMsg") + "')";
            bttUnVerifyAccount.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmUnverifyOrgMsg") + "')";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ProdRegMaster_WebToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Invalid token.");
                HandleODSCache();
                InitRegMasterInfo();
                BindCompanyInfo();
                ManageDeleteButtons();
            }

            litTab.Text = InitJavaScript();
        }

        private void HandleODSCache()
        {
            String obds_ProdRegDataCacheKey = ProdRegMaster_WebToken.ToString();
            if (Cache[odsProdRegData.CacheKeyDependency] == null) Cache[odsProdRegData.CacheKeyDependency] = obds_ProdRegDataCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsProdRegData.CacheKeyDependency].ToString().Equals(obds_ProdRegDataCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsProdRegData.CacheKeyDependency);
                Cache[odsProdRegData.CacheKeyDependency] = obds_ProdRegDataCacheKey;
            }

        }

        private void InitRegMasterInfo()
        {
            Lib.ProductRegistration.ProdReg_Master prm = Lib.ProductRegistration.ProdReg_MasterBLL.SelectByWebToken(ProdRegMaster_WebToken);
            if (prm == null) throw new HttpException(404, "Invalid token.");

            //dxcmbMasterModel.Value = prm.MasterModel.Trim();
            lblMasterMN.Text = prm.MasterModel.Trim();
            lblMasterSN.Text = prm.MasterSerial;
            //txtDesc.Text = prm.Description;
            lblCreatedBy.Text = prm.CreatedBy;
            lblCreatedOnUTC.Text = prm.CreatedOnUTC.ToStringFormatted();
            Lib.Account.Acc_AccountMaster account = AccountUtility.Admin_AccountInfoGet(prm.AccountID);
            hdnCompanyName.Value = account.CompanyName;
            hdnTeamName.Value = account.AccountName;
            bttProdRegMaster_Delete.Attributes.Add("accountId", account.AccountID.ToString());
        }

        public string StaticResourceClassKeyForRegistrationSummary
        {
            get { return "ADM_STCTRL_ProdReg_MasterCtrl"; }
        }

        public string GetStaticResourceForRegistrationSummary(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKeyForRegistrationSummary, resourceKey));
        }

        protected void bttProdRegMaster_Delete_Click(object sender, EventArgs e)
        {
            Boolean isOkToDelete = Lib.ProductRegistration.ProdReg_MasterBLL.CheckIfOkToDelete(ProdRegMaster_WebToken);
            if (!isOkToDelete)
            {
                ltrProductRegMasterMsg.Text = GetStaticResourceForRegistrationSummary("ERROR_ItemsExist");
                return;
            }
            else
            {
                App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                ProdReg_MasterBLL.Delete(ProdRegMaster_WebToken);
                Guid accountId = Guid.Parse(bttProdRegMaster_Delete.Attributes["accountId"]);
                Response.Redirect(string.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsDeletedProdRegs, KeyDef.QSKeys.AccountID, accountId));
            }
        }       

        protected void odsProdRegData_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["WebToken"] = ProdRegMaster_WebToken;
        }

        protected void bttModify_Click(object sender, EventArgs e)
        {
            //Redirect to Modify Page.
            String url = String.Format("{0}?{1}={2}&{3}={4}",
                               KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_RegistrationAddNew_JP,
                               KeyDef.QSKeys.RegisteredProductWebAccessKey,
                               ProdRegMaster_WebToken.ToString(),
                               KeyDef.QSKeys.ProductsModifyMode,
                               (int)ProdReg_AddNewCtrl_JP.RegisterProcessStatus.ModifySupportType
                               );
            Response.Redirect(url);
        }



        private Acc_AccountMaster _AccountInfo;
        public Guid AccountID
        {
            get { return ConvertUtility.ConvertToGuid(ViewState["vsAccountID"], Guid.Empty); }
            set { ViewState["vsAccountID"] = value; }
        }

        public Boolean AllowAddNewAccount
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState["vsAllowAddNewAccount"], true); }
            set { ViewState["vsAllowAddNewAccount"] = value; }
        }

        public Acc_AccountMaster AccountInfo
        {
            get
            {
                if (AccountID.IsNullOrEmptyGuid()) return null;
                if (_AccountInfo == null)
                {
                    _AccountInfo = AccountUtility.Admin_AccountInfoGet(AccountID);
                }
                return _AccountInfo;
            }
        }



        //<summary>
        //Checks whether user has access to modify company
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            // if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD))
            //Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageCompany);
            return isAdminMode;
        }

        public void BindCompanyInfo()
        {
            divCompanyNameInRuby.Visible = SiteUtility.BrowserLang_IsJapanese();

            var account = Lib.ProductRegistration.ProdReg_MasterBLL.SelectByWebToken(ProdRegMaster_WebToken);
            if (account != null)
                AccountID = account.AccountID;

            if (AccountID.IsNullOrEmptyGuid())
            {
                bttDeleteCompany.Visible = false;
                divAgreedToTerms.Visible = divAgreeToTermsOn.Visible = divIsVerified.Visible = false;
                bttSaveCompany.Visible = AllowAddNewAccount;
                pnlCompanyInfo.Visible = AllowAddNewAccount;
                return;
            }
            Lib.Account.Acc_AccountMaster acc = AccountInfo;
            if (acc == null) return;

            txtCompanyName.Text = acc.CompanyName;
            txtAccountName.Text = acc.AccountName;
            txtCompanyNameInRuby.Text = acc.CompanyNameInRuby;

            if (acc.IsVerifiedAndApproved)
            {
                //bttUnVerifyAccount.Visible = true;
                //bttVerifyAndApproveAccount.Visible = false;
                imgAccountVerifiedAndApproved.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/verified.gif";
            }
            else
            {
                //bttUnVerifyAccount.Visible = false;
                //bttVerifyAndApproveAccount.Visible = true;
                imgAccountVerifiedAndApproved.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/alert.png";
            }
            ltrAgreedToTerms.Text = acc.AgreedToTerms.ToString();
            ltrAgreedToTermsOnUTC.Text = "n/a";
            if (acc.AgreedToTermsOnUTC != DateTime.MinValue) ltrAgreedToTermsOnUTC.Text = GetDate(acc.AgreedToTermsOnUTC);
            if (!acc.OwnerMemberShipID.IsNullOrEmptyGuid()) addrCompanyAddress.LoadAddress(acc.OwnerMemberShipID);

            pnlCompanyInfo.Visible = true;
            txtCompanyAdditinalNotes.Text = acc.AdditinalNotes;

            bttDeleteCompany.Visible = (acc.AccountID != AccountUtility.AnritsuMasterAccountID());

            BindAccountVerifications();

            bttSaveCompany.Visible = bttDeleteCompany.Visible = bttVerifyAndApproveAccount.Visible = bttUnVerifyAccount.Visible = bttAddNote.Visible = pnlCompanyInfo.Enabled = IsAdminMode();

        }

        private void BindAccountVerifications()
        {
            gvAccountVerifications.DataSource = Lib.BLL.BLLAcc_AccountMaster.AccountVerifications_Select(AccountID);
            gvAccountVerifications.DataBind();
        }

        protected void bttVerifyAndApproveAccount_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.VerifyAccount(AccountID, admin.Email, txtCompanyVerificationInternalComment.Text.Trim());
            txtCompanyVerificationInternalComment.Text = String.Empty;
            //bttUnVerifyAccount.Visible = true;
            //bttVerifyAndApproveAccount.Visible = false;
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        protected void bttUnVerifyAccount_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.UnVerifyAccount(AccountID, admin.Email, txtCompanyVerificationInternalComment.Text.Trim());
            txtCompanyVerificationInternalComment.Text = String.Empty;
            //bttUnVerifyAccount.Visible = false;
            //bttVerifyAndApproveAccount.Visible = true;
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        protected void bttAddNote_Click(object sender, EventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser admin = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLAcc_AccountMaster.UpdateNotes(AccountID, txtCompanyAdditinalNotes.Text.Trim());
            AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
            BindCompanyInfo();
        }

        public string GetStatus(object isVerifiedObj)
        {
            Boolean isVerified = ConvertUtility.ConvertToBoolean(isVerifiedObj, false);
            if (isVerified)
                return GetStaticResource("VerifiedStatus");
            else
                return GetStaticResource("UnVerifiedStatus");
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        protected void bttSaveCompany_Click(object sender, EventArgs e)
        {
            Page.Validate("vgSaveCompany");
            if (!Page.IsValid) return;
            ltrCompanyMsg.Text = String.Empty;

            try
            {
                //Lib.Account.Profile_Address addr = addrCompanyAddress.GetAddressValue();
                //if (addr == null) throw new ArgumentException(GetStaticResource("MSG_InvalidAddress"));
                if (AccountID.IsNullOrEmptyGuid())
                {
                    App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                    Guid newAccountID = Lib.BLL.BLLAcc_AccountMaster.CreateAccount(user.MembershipId
                        , txtAccountName.Text.Trim()
                        , txtCompanyName.Text.Trim()
                        , txtCompanyNameInRuby.Text.Trim()
                        , WebUtility.GetUserIP()
                        , user.Email
                        , user.FullName);
                    WebUtility.HttpRedirectWithUpdatedQueryStrings(this, String.Format("?{0}={1}", KeyDef.QSKeys.AccountID, newAccountID.ToString()));
                }
                else
                {
                    Lib.BLL.BLLAcc_AccountMaster.Update(AccountID, txtAccountName.Text.Trim(), txtCompanyName.Text.Trim(), txtCompanyNameInRuby.Text.Trim(), false);
                    //Lib.BLL.BLLProfile_Address.Update(addr, false);
                    ltrCompanyMsg.Text = GetStaticResource("MSG_Updated");
                    AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
                }
            }
            catch (Exception ex)
            {
                ltrCompanyMsg.Text = ex.Message;
            }
        }

        protected void bttDeleteCompany_Click(object sender, EventArgs e)
        {
            if (AccountID.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");
            ltrCompanyMsg.Text = String.Empty;

            try
            {
                Lib.BLL.BLLAcc_AccountMaster.DeleteByAccountID(AccountID);
                AccountUtility.AdminOrgSearchOptionsSet(null);
                AccountUtility.Admin_AccountInfoSet(AccountID, null);//reset
                WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgSearch);
            }
            catch (Exception ex)
            {
                ltrCompanyMsg.Text = ex.Message;
            }
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_AccountAdmin_DetailCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        private string InitJavaScript()
        {
            string js = "";
            js = "<script type=\"text/javascript\" >\r\n";
            js += "InitTabChangeEvent();\r\n";
            js += "</script>\r\n";
            return js;
        }

        protected void ManageDeleteButtons()
        {
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageProductReg))
            {
                DataTable dtItems = ProdReg_MasterBLL.CheckItemsToDelete(ProdRegMaster_WebToken);
                if (dtItems == null || dtItems.Rows.Count < 1)
                {
                    bttProdRegMaster_Delete.Visible = true;
                    bttProdRegMaster_Delete.OnClientClick = GetFinalRegDeleteConfirmMsgAlert(hdnTeamName.Value);
                }
                else
                {
                    bttProdRegMaster_Delete.Visible = false;
                    hdnIsFinalDelete.Value = dtItems.Rows.Count == 1 ? "true" : "false";
                    repDelete.DataSource = dtItems;
                    repDelete.DataBind();
                    repDelete.Visible = true;
                    if (dtItems.Rows.Count > 1)
                    {
                        ltrProductRegMasterMsg.Text = GetStaticResourceForRegistrationSummary("MultipleProdRegInfoMsg").Replace("[[ITEMS_COUNT]]", dtItems.Rows.Count.ToString());
                    }
                }
            }
        }

        protected void repDelete_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Button btnDelete = (Button)e.Item.FindControl("btnDeleteAdmins");
            btnDelete.Text = GetStaticResourceForRegistrationSummary("bttProdRegMaster_Delete_By.Text")  +
                GetProdRegAdminTypeDisplayText(btnDelete.Attributes["modelConfigType"]);
            if (IsProdRegRegionalAdmin(btnDelete.Attributes["modelConfigType"]))
            {
                btnDelete.Enabled = true;
                if (hdnIsFinalDelete.Value == "true")
                    btnDelete.OnClientClick = GetFinalRegDeleteConfirmMsgAlert(hdnTeamName.Value);
                else
                    btnDelete.OnClientClick = GetMultipleRegDeleteConfirmMsgAlert(hdnTeamName.Value);
            }
            else
            {
                btnDelete.Enabled = false;
            }
        }

        protected string GetFinalRegDeleteConfirmMsgAlert(string teamName)
        {
            return $"return confirm('{GetStaticResourceForRegistrationSummary("ConfirmDeleteRegMsg").Replace("[[TEAM_NAME]]", teamName)}')";
        }

        protected string GetMultipleRegDeleteConfirmMsgAlert(string teamName)
        {
            return $"return confirm('{GetStaticResourceForRegistrationSummary("ConfirmDeleteMultipleRegMsg").Replace("[[TEAM_NAME]]", teamName)}')";
        }

        protected void repDelete_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Button btnAdmins = (Button)e.Item.FindControl("btnDeleteAdmins");

            string modelConfigType = btnAdmins.Attributes["modelConfigType"];
            Guid accountId = Guid.Parse(btnAdmins.Attributes["accountId"]);
            int proRegItemId = int.Parse(btnAdmins.Attributes["prodRegItemID"]);
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            ProdReg_ItemBLL.Delete(proRegItemId, user.Email);

            //Removing registration from master upon last Admin delete
            if (hdnIsFinalDelete.Value == "true")
            {                
                ProdReg_MasterBLL.Delete(ProdRegMaster_WebToken);
                Response.Redirect(string.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgDetailsDeletedProdRegs, KeyDef.QSKeys.AccountID, accountId));
            }
            else
            {                
                ltrProductRegMasterMsg.Text = GetStaticResourceForRegistrationSummary("prodRegDeletedMsg").Replace("[[REGIONAL_ADMIN]]", GetProdRegAdminTypeDisplayText(modelConfigType));
                NotifyAdminOnProdRegDeletion(GetProdRegAdminTypeDisplayText(modelConfigType));
            }
            ManageDeleteButtons();
        }

        protected void NotifyAdminOnProdRegDeletion(string deletedByRegionalAdmin)
        {
            List<string> regionalAdminsDistKeysLst = new List<string>();
            List<string> regionalAdminsLst = new List<string>();
            foreach (RepeaterItem ritem in repDelete.Items)
            {
                Button btnDelete = ritem.FindControl("btnDeleteAdmins") as Button;
                regionalAdminsDistKeysLst.Add(ProductConfigUtility.ModelConfigType_GetInfo(btnDelete.Attributes["modelConfigType"]).EmailDistKey_NotifyReg);
                regionalAdminsLst.Add(GetProdRegAdminTypeDisplayText(btnDelete.Attributes["modelConfigType"]));
            }
            NotifyAdminOnProdRegDelettion(deletedByRegionalAdmin, regionalAdminsDistKeysLst, hdnCompanyName.Value, hdnTeamName.Value, regionalAdminsLst);
        }
    }
}