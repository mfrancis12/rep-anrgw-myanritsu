﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_Master_ListByAccountCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_ProdReg.ProdReg_Master_ListByAccountCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlAllRegProd" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,pnlAllRegProd.HeaderText%>">
    <div class="settingrow">
       <dx:ASPxGridView ID="dxgvProdRegMasters" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false" DataSourceID="sdsProdRegs" KeyFieldName="WebToken" Width="100%"
           OnSelectionChanged="dxgvProdRegMasters_SelectionChanged">
           <Columns>
               <dx:GridViewDataColumn FieldName="MasterModel" VisibleIndex="1" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,MasterModel.Text %>'></dx:GridViewDataColumn>
               <dx:GridViewDataColumn FieldName="MasterSerial" VisibleIndex="2" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,MasterSerial.Text %>'></dx:GridViewDataColumn>
               <dx:GridViewDataColumn FieldName="AdminTypes" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,Admins.Text  %>'></dx:GridViewDataColumn>
               <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="10" Caption='<%$ Resources:ADM_STCTRL_ProdReg_ListCtrl,CreatedOnUTC.Text  %>'></dx:GridViewDataDateColumn>
                <dx:GridViewDataColumn  VisibleIndex="11">
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Details" NavigateUrl='<%#"~/prodregadmin/manage_registration?rwak="+Eval("WebToken") %>' />
                        </DataItemTemplate>
                </dx:GridViewDataColumn>
           </Columns>
           <SettingsPager PageSize="500"></SettingsPager>
       </dx:ASPxGridView>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdReg_Master_ListByAccountCtrl" />
<asp:SqlDataSource ID="sdsProdRegs" runat="server" ConnectionString="<%$ ConnectionStrings:Connect.DB.ConnStr %>"
    SelectCommandType="StoredProcedure" SelectCommand="[CNT].[uSP_Acc_ProductRegistered_Master_SelectWithFilter]"
    OnSelecting="sdsProdRegs_Selecting">
    <SelectParameters>
        <asp:QueryStringParameter Name="AccountID" QueryStringField="accid" DbType="Guid" />
        <asp:Parameter Name="WebToken" DbType="Guid" />
        <asp:Parameter Name="ModelNumber" DbType="String" />
        <asp:Parameter Name="SerialNumber" DbType="String" />
        <asp:Parameter Name="ItemStatusCode" DbType="String" />
        <asp:Parameter Name="UserEmail" DbType="String" />
        <asp:Parameter Name="RegCreatedBy" DbType="String" />
        <asp:Parameter Name="OrgName" DbType="String" />
        <asp:Parameter Name="CompanyName" DbType="String" />
    </SelectParameters>
</asp:SqlDataSource>