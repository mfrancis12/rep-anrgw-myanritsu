﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_Master_ListByAccountCtrl : BC_ProdRegAdminBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void sdsProdRegs_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@WebToken"].Value = DBNull.Value;
            e.Command.Parameters["@ModelNumber"].Value = DBNull.Value;
            e.Command.Parameters["@SerialNumber"].Value = DBNull.Value;
            e.Command.Parameters["@ItemStatusCode"].Value = DBNull.Value;
            e.Command.Parameters["@UserEmail"].Value = DBNull.Value;
            e.Command.Parameters["@RegCreatedBy"].Value = DBNull.Value;
            e.Command.Parameters["@OrgName"].Value = DBNull.Value;
            e.Command.Parameters["@CompanyName"].Value = DBNull.Value;
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        protected void dxgvProdRegMasters_SelectionChanged(object sender, EventArgs e)
        {
            Guid webToken = ConvertUtility.ConvertToGuid( dxgvProdRegMasters.GetMasterRowKeyValue(), Guid.Empty);
            Response.Redirect(String.Format("{0}?{1}={2}"
                , App_Lib.KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_Registration
                , App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey
                , webToken));
        }
    }
}