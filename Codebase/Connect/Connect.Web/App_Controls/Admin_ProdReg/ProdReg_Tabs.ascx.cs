﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.Admin_ProdReg
{
    public partial class ProdReg_Tabs : BC_ProdRegAdminBase
    {
        protected override void OnInit(EventArgs e)
        {
            InitTabs();
            base.OnInit(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                //InitTabs();
            }
        }

        private String GetTabText(String resKey)
        {
            return ConvertUtility.ConvertToString(this.GetGlobalResourceObject("ProductRegAdmin_Tabs", resKey), resKey);
        }

        public void InitTabs()
        {
            ProdReg_Master pregM = Admin_ProductRegUtility.ProdReg_MasterInfo(ProdRegMaster_WebToken, false);
            if (pregM == null)
            {
                this.Visible = false;
                return;
            }
            ProductConfig pcfg = ProductConfigUtility.SelectByModel(false, pregM.MasterModel);
            if (pcfg == null) throw new ArgumentNullException("productconfig");

            String query = String.Format("?{0}={1}", KeyDef.QSKeys.RegisteredProductWebAccessKey, ProdRegMaster_WebToken.ToString());
            Tab tab;
            int tabIndex = 0;

            foreach (Lib.Cfg.ModelConfig mcfg in pcfg.ModelConfigs)
            {
                ModelConfigTypeInfo mcfgInfo = ProductConfigUtility.ModelConfigType_GetInfo(mcfg.ModelConfigType);
                tab = new Tab(mcfgInfo.ModelConfigTypeDisplayText, String.Format("tabProdRegConfig_{0}", mcfg.ModelConfigType));
                tab.Enabled = false;
                tab.VisibleIndex = tabIndex++;
                switch (mcfg.ModelConfigType)
                {
                    case ModelConfigTypeInfo.CONFIGTYPE_US_MMD:
                        if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_USMMD))
                        {
                            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_USMMD + query;
                            tab.Enabled = true;
                        }
                        break;
                    case ModelConfigTypeInfo.CONFIGTYPE_DENMARK:
                        if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_DKSW))
                        {
                            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_DKSW + query;
                            tab.Enabled = true;
                        }
                        break;
                    case Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_JP:
                        if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPM) )
                        {
                            query = String.Format("?{0}={1}&{2}={3}", KeyDef.QSKeys.RegisteredProductWebAccessKey, ProdRegMaster_WebToken.ToString(), KeyDef.QSKeys.ModelConfigType, ModelConfigTypeInfo.CONFIGTYPE_JP);
                            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_JPSW + query;
                            tab.Enabled = true;
                        }
                        break;
                    case Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UK_ESD:
                        if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_ESD))
                        {
                            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_ESD + query;
                            tab.Enabled = true;
                        }
                        break;
                    case Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_JPAN:
                        if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPAN))
                        {
                            query = String.Format("?{0}={1}&{2}={3}", KeyDef.QSKeys.RegisteredProductWebAccessKey, ProdRegMaster_WebToken.ToString(), KeyDef.QSKeys.ModelConfigType, ModelConfigTypeInfo.CONFIGTYPE_JPAN);
                            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_JPSW + query;
                            tab.Enabled = true;
                        }
                        break;
                    case Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_UNKNOWN:
                    default:
                        continue;
                }
                dxPregDetailTabs.Tabs.Add(tab);
            }

            #region " Org Tab "
            tab = new Tab(GetTabText("Account"), "tabProdRegConfig_AccInfo");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_Account + query;
            dxPregDetailTabs.Tabs.Add(tab);
            #endregion

            #region " Registration Tab "
            tab = new Tab(GetTabText("Registration"), "tabProdRegConfig_RegSum");
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_Registration + query;
            dxPregDetailTabs.Tabs.Add(tab);
            dxPregDetailTabs.ActiveTab = tab;
            #endregion
        }

    }
}