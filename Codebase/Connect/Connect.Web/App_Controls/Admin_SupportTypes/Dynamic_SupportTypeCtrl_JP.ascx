﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Dynamic_SupportTypeCtrl_JP.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_SupportTypes.Dynamic_SupportTypeCtrl_JP" %>


    <div class="dynamic-supportType-container overflow"> 
        <div class="cblSupType1-wrapper">
            <div class="settingrow">
                <dx:ASPxCheckBox ID="cblSupType1" runat="server"   />
            </div>
        </div>
        <div class="adExpiry-wrapper">
            <div class="settingrow">
                <dx:ASPxDateEdit ID="adExpiry" runat="server" EnableViewState="True" AllowUserInput="true" ClientIDMode="AutoID" OnValueChanged="adExpiry_OnValueChanged" CssClass="date-expire"></dx:ASPxDateEdit>
            </div>
        </div>
    </div> 


