﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_Controls.Admin_SupportTypes
{
    public partial class Dynamic_SupportTypeCtrl_JP : System.Web.UI.UserControl
    {

        public bool Checked { get; set; }
        public string ExpiryDateTime { get; set; }
        public string DisplayText { get; set; }
        public string SupportValue { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

            adExpiry.MinDate = DateTime.Now.Date.AddDays(1);

            cblSupType1.Text = DisplayText;
            adExpiry.Text = ExpiryDateTime != null ? ExpiryDateTime : string.Empty;

        }
        protected void Page_Init(object sender, EventArgs e)
        {

            cblSupType1.Checked = Checked;
            adExpiry.Text = ExpiryDateTime;

        }
        protected void cblSupType1_OnCheckedChanged(object sender, EventArgs e)
        {
            if (cblSupType1.Checked)
            {
                Checked = true;
            }
            else
            {
                Checked = false;
            }
        }

        protected void adExpiry_OnValueChanged(object sender, EventArgs e)
        {
            if (adExpiry.Text.Trim().Length > 0)
            {
                ExpiryDateTime = adExpiry.Date.ToShortDateString();
            }
        }

    }
}