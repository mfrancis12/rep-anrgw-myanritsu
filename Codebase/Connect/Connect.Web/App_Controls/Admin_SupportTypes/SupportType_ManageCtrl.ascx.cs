﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.CodeParser;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.Admin_SupportTypes
{
    public partial class SupportTypeManageCtrl : System.Web.UI.UserControl
    {
        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_SupportTypeAdmin"; }
        }
        protected string GetStaticResource(string resourceKey)
        {
            var globalResourceObject = this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey);
            if (globalResourceObject != null)
                return globalResourceObject.ToString();
            else
            {
                return string.Empty;
            }

        }
        private const String AreaEffected = "Manage Support Types";
        private static String LoggedInUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Form.DefaultButton = this.btnSupType.UniqueID;
            //if (!IsPostBack)
            //{
            //    Session["update"] = Guid.NewGuid().ToString();
            //}
            Gwid.Value = AccountUtility.GetSelectedAccountID(true).ToString();
            gridSupportType.DataBind();
        }

        //protected void Page_PreRender(object obj, EventArgs e)
        //{
        //    ViewState["update"] = Session["update"];
        //}
        protected void btnSupType_OnClick(object sender, EventArgs e)
        {
            //if (!Convert.ToString(Session["update"]).Equals(Convert.ToString(ViewState["update"])))
            //{
            //    Response.Redirect(Request.RawUrl);
            //}


            lblMsg.Visible = true;
            var formValuesDictionary = new Dictionary<string, object>();
            if (ddlSelType.SelectedIndex.Equals(1))
            {
                if (LoadSelectTypes(txtEmOrg.Text).Equals("1"))
                {
                    lblMsg.Text = GetStaticResource("lblMsgInvEmail.Text");
                    return;
                }
            }
            formValuesDictionary.Add("OrgId", ddlSelType.SelectedIndex.Equals(2) ? txtOrgId.Text : string.Empty);
            formValuesDictionary.Add("MembershipId", ddlSelType.SelectedIndex.Equals(1) ? txtOrgId.Text : string.Empty);
            formValuesDictionary.Add("ModelNumber", txtModelNumber.Text.Trim().Length > 0 ? txtModelNumber.Text : string.Empty);
            formValuesDictionary.Add("SerialNumber", txtSerialNumber.Text.Trim().Length > 0 ? txtSerialNumber.Text : string.Empty);
            var supportTypesDictionary = new Dictionary<string, string>();
            var phEditCtrl = supTypeNew.FindControl("phEditControls") as PlaceHolder;
            if (phEditCtrl != null)
            {
                GetSupportValue(phEditCtrl, ref supportTypesDictionary);

            }

            if (supportTypesDictionary.Count.Equals(0))
            {
                lblMsg.Text = GetStaticResource("lblMsgSelect.Text");
                return;
            }
            formValuesDictionary.Add("SupportType", supportTypesDictionary);
            formValuesDictionary.Add("ModelConfigType", ModelConfigTypeInfo.CONFIGTYPE_JP);
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);
            LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_ATTEMPT, LoggedInUserEmail, txtModelNumber.Text.Trim(), txtSerialNumber.Text.Trim(), txtEmOrg.Text.Trim());
            bool exist = GetSupportType(formValuesDictionary);
            if (exist)
            {
                lblMsg.Text = GetStaticResource("lblMsgExist.Text"); ;
                return;
            }

            int i = (int)BLL_SupportEntry.AddProductSupportEntry(formValuesDictionary);

            if (i > 0)
            {
                lblMsg.Text = GetStaticResource("lblMsgSuccess.Text");
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_SUCCESS, LoggedInUserEmail, txtModelNumber.Text.Trim(), txtSerialNumber.Text.Trim(), txtEmOrg.Text.Trim());
                ResetControls();
                //Session["update"] = Guid.NewGuid().ToString();
                LoadData();
            }
            else
            {
                lblMsg.Text = GetStaticResource("lblMsgSaveError.Text");
                LogAdminActivity(AreaEffected, TasksPerformed.Add, ActivityLogNotes.SUPPORTTYPEENTRY_ADD_FAIL, LoggedInUserEmail, txtModelNumber.Text.Trim(), txtSerialNumber.Text.Trim(), txtEmOrg.Text.Trim());
            }
        }
        protected void gridSupportType_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var id = ConvertUtility.ConvertToInt32(e.Keys[0], 0);


            var formLayout = (ASPxFormLayout)gridSupportType.FindEditFormTemplateControl("formLayout");
            var modelNumber = formLayout.FindControl("dxlblModelNumber") as ASPxTextBox;
            if (modelNumber.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqMdlNum"));
            }
            var dxMemId = formLayout.FindControl("dxMemId") as HiddenField;
            var dxAcctId = formLayout.FindControl("dxAcctId") as HiddenField;
            var serialNumber = formLayout.FindControl("dxtxtSerialNumber") as ASPxTextBox;
            var dxtxtEmailOrg = formLayout.FindControl("dxtxtEmailOrg") as ASPxTextBox;
            if (serialNumber.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqSerNum"));
            }
            var selType = formLayout.FindControl("ddlSelTypeEdit") as ASPxComboBox;
            var emailOrg = formLayout.FindControl("dxtxtEmailOrg") as ASPxTextBox;

            if (emailOrg.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqEaOrg"));
            }
            if (selType != null && selType.SelectedIndex.Equals(1))
            {
                if (dxtxtEmailOrg != null && LoadSelectTypes(dxtxtEmailOrg.Text).Equals("1"))
                {
                    throw new Exception(GetStaticResource("ReqValidEmail"));
                }
            }
            var placeHolder = formLayout.FindControl("phEditControls") as PlaceHolder;
            var formValuesDictionary = new Dictionary<string, object>();
            formValuesDictionary.Add("Id", id);

            if (selType != null)
            {
                if (selType.SelectedIndex.Equals(1))
                    formValuesDictionary.Add("MembershipId", selType.SelectedIndex.Equals(1) ? Convert.ToString(dxMemId.Value) : string.Empty);
                else
                {
                    formValuesDictionary.Add("MembershipId", string.Empty);
                }
                if (selType.SelectedIndex.Equals(2))
                    formValuesDictionary.Add("OrgId", selType.SelectedIndex.Equals(2) ? Convert.ToString(dxAcctId.Value) : string.Empty);
                else { formValuesDictionary.Add("OrgId", string.Empty); }
            }
            formValuesDictionary.Add("ModelNumber", modelNumber != null && modelNumber.Text.Trim().Length > 0 ? modelNumber.Text : string.Empty);
            formValuesDictionary.Add("SerialNumber", serialNumber != null && serialNumber.Text.Trim().Length > 0 ? serialNumber.Text : string.Empty);
            var supportTypesDictionary = new Dictionary<string, string>();
            var fLayout = (ASPxFormLayout)gridSupportType.FindEditFormTemplateControl("formLayout");
            var sctrl = fLayout.FindControl("supTypeNew") as SupportTypes_Ctrl;
            if (sctrl != null)
            {
                var phEditCtrl = sctrl.FindControl("phEditControls") as PlaceHolder;
                if (phEditCtrl != null)
                {
                    GetSupportValue(phEditCtrl, ref supportTypesDictionary);

                }
            }
            if (supportTypesDictionary.Count.Equals(0))
            {
                throw new Exception(GetStaticResource("lblMsgSelect.Text"));

            }
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);
            formValuesDictionary.Add("SupportType", supportTypesDictionary);
            formValuesDictionary.Add("ModelConfigType",ModelConfigTypeInfo.CONFIGTYPE_JP);
            bool exist = GetSupportType(formValuesDictionary);

            if (dxtxtEmailOrg != null)
                LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_ATTEMPT, LoggedInUserEmail, modelNumber.Text.Trim(), serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
            if (exist)
            {
                throw new Exception(GetStaticResource("lblMsgExist.Text"));
            }
            var i = (int)BLL_SupportEntry.UpdateProductSupportEntry(formValuesDictionary);
            if (i > 0)
            {
                if (dxtxtEmailOrg != null)
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify,
                        ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_SUCCESS, LoggedInUserEmail, modelNumber.Text.Trim(),
                        serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
                e.Cancel = true;
                gridSupportType.CancelEdit();
                gridSupportType.DataBind();

            }
            else
            {
                if (dxtxtEmailOrg != null)
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify,
                        ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_FAIL, LoggedInUserEmail, modelNumber.Text.Trim(),
                        serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
            }
        }
        protected void gridSupportType_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var id = ConvertUtility.ConvertToInt32(e.Keys[0], 0);
            var prodsup =(ProductSupportEntity) BLL_SupportEntry.GetProductSupportEntry(id);

            LogAdminActivity(AreaEffected, TasksPerformed.Delete, ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_ATTEMPT, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
            int deleted = (Int32)BLL_SupportEntry.DeleteProductSupportEntry(id.ToString());
            e.Cancel = true;
            if (deleted > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alt",
                    "<script>alert('" + GetStaticResource("DelMsg") + "')</script>");

                LogAdminActivity(AreaEffected, TasksPerformed.Delete,
                    ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_SUCCESS, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
                LoadData();

            }
            else
            {

                LogAdminActivity(AreaEffected, TasksPerformed.Delete,
                    ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_FAIL, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
            }
        }
        protected void gridSupportType_OnPageIndexChanged(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;
            if (grid == null) return;
            var pageIndex = grid.PageIndex;
            gridSupportType.PageIndex = pageIndex;
            LoadData();
        }
        protected void UpdateButton_OnDataBinding(object sender, EventArgs e)
        {
        }
        protected void gridSupportType_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var supportList = BLL_SupportEntry.GetProductSupportList();
            gridSupportType.DataSource = null;
            gridSupportType.DataSource = supportList;
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            ResetAllControls();
        }
        #endregion
        #region private methods

        private static void GetSupportValue(PlaceHolder phEditCtrl, ref Dictionary<string, string> supportTypesDictionary)
        {
            foreach (Dynamic_SupportTypeCtrl dynSupCtrl in phEditCtrl.Controls)
            {
                var ade1 = dynSupCtrl.FindControl("adExpiry") as ASPxDateEdit;
                var cbx = dynSupCtrl.FindControl("cblSupType1") as ASPxCheckBox;
                if (cbx != null && cbx.Checked)
                {
                    supportTypesDictionary.Add(dynSupCtrl.SupportValue, ade1.Text);
                }

            }
        }
        public string CreateSupportTypes(object id)
        {
            var sBuilder = new StringBuilder();
            var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
            //var obj = (IEnumerable<object>) mappings;
            foreach (SupportEntryMapping type in mappings)
            {
                sBuilder.Append(type.SupportTypeCode);
                sBuilder.Append("-");
                if (type.ExpiryDate.HasValue)
                {
                    sBuilder.Append(type.ExpiryDate.GetValueOrDefault().ToString("MM/dd/yyyy"));
                }
                else
                {
                    sBuilder.Append(this.GetGlobalResourceObject("STCTRL_JpPrivateresources", "notset"));
                }
                sBuilder.Append(" ");
            }
            return sBuilder.ToString();
        }
        private void LoadData()
        {
            gridSupportType.DataBind();
        }
        private void ResetAllControls()
        {
            ResetControls();
            lblMsg.Text = string.Empty;
        }
        private void ResetControls()
        {
            txtModelNumber.Text = txtSerialNumber.Text = txtEmOrg.Text = string.Empty; //phControls.Controls.Clear();
            var phEditCtrl = supTypeNew.FindControl("phEditControls") as PlaceHolder;
            if (phEditCtrl != null)
            {
                foreach (Dynamic_SupportTypeCtrl dynSupCtrl in phEditCtrl.Controls)
                {
                    ASPxDateEdit ade1 = dynSupCtrl.FindControl("adExpiry") as ASPxDateEdit;
                    ASPxCheckBox cbx = dynSupCtrl.FindControl("cblSupType1") as ASPxCheckBox;
                    if (cbx != null && cbx.Checked)
                    {
                        cbx.Checked = false;
                        ade1.Text = string.Empty;
                    }
                }

            }
            ddlSelType.SelectedIndex = ddlSelType.Items.IndexOf(ddlSelType.Items.FindByValue("Select"));

        }
        public string LoadSelectTypes(object selType)
        {
            try
            {
                if (selType != null)
                {
                    var m = new MailAddress(selType.ToString());
                }

                return "0";
            }
            catch (FormatException)
            {
                return "1";
            }



        }
        public bool SupportTypes(object id, string val)
        {
            if (id != null)
            {
                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
                bool check = false;
                foreach (SupportEntryMapping type in mappings)
                {
                    if (val != null && type.SupportTypeCode.Equals(val))
                    {
                        check = true;
                    }
                }
                return check;
            }
            else
            {
                return false;

            }
        }
        public string SupportTypesDate(object id, string val)
        {
            if (id != null)
            {
                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
                string date = string.Empty;
                foreach (SupportEntryMapping type in mappings)
                {
                    if (val != null && type.SupportTypeCode.Equals(val))
                    {
                        if (type.ExpiryDate.HasValue)
                        {
                            date = type.ExpiryDate.Value.ToShortDateString();
                        }

                    }
                }
                return date;
            }
            else
            {
                return "";

            }
        }
        public bool GetSupportType(dynamic formValues)
        {
            return BLL_SupportEntry.GetSupportTypeEntry(formValues);
        }
        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(LoggedInUserEmail, AreaEffected, taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
        }
        #endregion
    }
}