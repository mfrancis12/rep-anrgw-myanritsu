﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupportType_ManageCtrl_JP.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.Admin_SupportTypes.SupportType_ManageCtrl_JP" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib.Utils" %>

<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<link href="//ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />

<link href="http://dl.cdn-anritsu.com/appfiles/css/token-input.css" rel="stylesheet" />
<script src="http://dl.cdn-anritsu.com/appfiles/js/MyAnritsu_App_JS/jquery.tokeninput.js"></script>

<script src="//code.jquery.com/ui/1.9.2/jquery-ui.js" type="text/javascript"></script>
<%@ Register Src="SupportTypes_Ctrl_JP.ascx" TagName="SupType" TagPrefix="uc1" %>

<script type="text/javascript" src="//dl.cdn-anritsu.com/appfiles/js/MyAnritsu_App_JS/support_and_packages.js"></script>

    <style>
        .bttSearchContainer {
            margin-top: 3px;
            float: right;
        }
    </style>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdReg_AddNewCtrl,gwpnlSearchOrg.HeaderText%>'>
    <asp:HiddenField runat="server" ID="Gwid" />

    <div class="width-60">
        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByEmail" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclEmail%>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByEmail" runat="server"></asp:TextBox>
        </div>

        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclModelName%>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByMN" runat="server"></asp:TextBox>
        </div>

        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchBySN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl_JP,lclSerial%>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchBySN" runat="server"></asp:TextBox>
        </div>

        <br />
            

        <div class="settingrow">
            <span class="msg" style="margin-left:100px">
                <asp:Localize ID="lblErrorMsg" runat="server" Visible="false" ></asp:Localize>
            </span>
            <div class="bttSearchContainer">
                <asp:Button ID="bttSupportTypeSearch" runat="server" Text="<%$Resources:ADM_STCTRL_PackageAdmin_ListCtrl_JP, bttSearch%>" CausesValidation="false" OnClick="bttSupportTypeSearch_Click" />
            </div>
        </div>
        <br />
</anrui:GlobalWebBoxedPanel>


<dx:ASPxGridView ID="gridSupportType" ClientInstanceName="gridSupportType" runat="server" OnBeforePerformDataSelect="gridSupportType_OnBeforePerformDataSelect" OnPageIndexChanged="gridSupportType_OnPageIndexChanged" Theme="AnritsuDevXTheme" OnRowUpdating="gridSupportType_OnRowUpdating" OnRowDeleting="gridSupportType_OnRowDeleting" KeyFieldName="Id" Width="100%" AutoGenerateColumns="False">
    <Columns>
        <dx:GridViewDataColumn FieldName="Id" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
        <dx:GridViewDataTextColumn VisibleIndex="1" Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalModelNumber.Text %>' FieldName="ModelNumber" Width="350" SortIndex="1"></dx:GridViewDataTextColumn>
        <dx:GridViewDataColumn FieldName="SerialNumber" VisibleIndex="2" Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalSerialNumber.Text %>' Width="400" SortIndex="2"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="EmailAdd_Org" VisibleIndex="3" Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,EmailAdd_Org.Text %>' Width="400" SortIndex="3"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="MemberShipId" SortIndex="3" Visible="False"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="AccountId" SortIndex="3" Visible="False"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn VisibleIndex="4" Width="50" Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalSupType.Text %>' SortIndex="4">
            <DataItemTemplate>
                <dx:ASPxLabel ID="dxlblSisProductName" runat="server" Text='<%#CreateSupportTypes( DataBinder.Eval(Container.DataItem, "Id") ) %>'></dx:ASPxLabel>

            </DataItemTemplate>
        </dx:GridViewDataColumn>
        <dx:GridViewCommandColumn VisibleIndex="100" Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,gridSupportType.Manage %>' ButtonType="Image">
            <EditButton Visible="true" Image-ToolTip="Edit Item Details" Image-SpriteProperties-CssClass="edit-icon"></EditButton>
            <DeleteButton Visible="true" Image-ToolTip="Delete Item" Image-SpriteProperties-CssClass="delete-icon"></DeleteButton>
        </dx:GridViewCommandColumn>
    </Columns>

    <Templates>
        <EditForm>
            <dx:ASPxFormLayout ID="formLayout" AllignItemCaptions="True" EncodeHtml="true" runat="server" Width="750" Height="375" Theme="AnritsuDevXTheme" MgrId='<%# Eval("Id") %>'>
                <Items>
                    <dx:LayoutItem Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalModelNumber.Text %>' RequiredMarkDisplayMode="Required">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer1" runat="server">
                                <dx:ASPxTextBox ID="dxlblModelNumber" Width="522px" ClientIDMode="Static" ClientInstanceName="txtEditModelNo" runat="server" Text='<%# Eval("ModelNumber") %>'>
                                    <ClientSideEvents KeyUp="onkeyup" ValueChanged="clearSerialTxt" />
                                    <ValidationSettings Display="Dynamic">
                                        <RequiredField IsRequired="True" ErrorText="<%$Resources:Common,ERROR_Required%>"></RequiredField>
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                                <asp:HiddenField ID="dxMemId" runat="server" Value='<%# Eval("MemberShipId") %>' />

                                <asp:HiddenField ID="dxAcctId" runat="server" Value='<%# Eval("AccountId") %>'></asp:HiddenField>

                                <asp:RequiredFieldValidator ID="rfvModelNumber" runat="server" ControlToValidate="dxlblModelNumber"
                                    ValidationGroup="vgSupType" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
                            </dx:LayoutItemNestedControlContainer>

                        </LayoutItemNestedControlCollection>

                    </dx:LayoutItem>
                    <dx:LayoutItem Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalSerialNumber.Text %>' RequiredMarkDisplayMode="Required">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer2" runat="server">
                                <dx:ASPxTextBox ID="dxtxtSerialNumber" Width="522px" runat="server" Text='<%# Eval("SerialNumber") %>'>
                                    <ClientSideEvents KeyUp="onkeyupSerialNumber" />
                                    <ValidationSettings Display="Dynamic">
                                        <RequiredField IsRequired="True" ErrorText="<%$Resources:Common,ERROR_Required%>"></RequiredField>
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalSelType.Text %>' RequiredMarkDisplayMode="Required">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer4" runat="server">
                                <dx:ASPxComboBox ID="ddlSelTypeEdit" Style="margin-left: 3px;" Width="522px" runat="server" Value='<%#LoadSelectTypes(Eval("EmailAdd_Org") )%>'>
                                    <ClientSideEvents SelectedIndexChanged="dropDownValueChanged" />
                                    <Items>
                                        <dx:ListEditItem Text="--Select--" Value="Select"></dx:ListEditItem>
                                        <dx:ListEditItem Text='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,ListEditItem.Email %>' Value="0"></dx:ListEditItem>
                                        <dx:ListEditItem Text='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,ListEditItem.Organization %>' Value="1"></dx:ListEditItem>
                                    </Items>
                                </dx:ASPxComboBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                    <dx:LayoutItem Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,EmailAdd_Org.Text %>' RequiredMarkDisplayMode="Required">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer3" runat="server">
                                <dx:ASPxTextBox ID="dxtxtEmailOrg" runat="server" Width="522px" Text='<%# Eval("EmailAdd_Org") %>'>
                                    <ClientSideEvents KeyUp="onkeyupEmailOrg" />
                                    <ValidationSettings Display="Dynamic">
                                        <RequiredField IsRequired="True" ErrorText="<%$Resources:Common,ERROR_Required%>"></RequiredField>
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption='<%$ Resources:ADM_STCTRL_SupportTypeAdmin,lcalSupType.Text %>' RequiredMarkDisplayMode="Required">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer ID="LayoutItemNestedControlContainer5" runat="server">
                                <uc1:SupType IsNew="True" PrId='<%# Eval("Id") %>' ID="supTypeNew" runat="server" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:ASPxFormLayout>

            <div class="div-wrapper">
                <div class="updateButton-wrapper">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton" runat="server" OnDataBinding="UpdateButton_OnDataBinding" Style="color: blue;" />
                </div>
                <div class="cancelButton-wrapper">
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton" runat="server" />
                </div>
            </div>
        </EditForm>
    </Templates>
    <SettingsEditing Mode="PopupEditForm" />
    <SettingsPager Mode="ShowPager" PageSize="20" Position="Bottom" AlwaysShowPager="false">
        <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
    </SettingsPager>
    <SettingsPopup>
        <EditForm Modal="True" VerticalAlign="Middle" HorizontalAlign="Center"></EditForm>
    </SettingsPopup>

    <Settings ShowTitlePanel="false" ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="true" ShowPreview="True" />
    <SettingsBehavior ConfirmDelete="true" />
</dx:ASPxGridView>
