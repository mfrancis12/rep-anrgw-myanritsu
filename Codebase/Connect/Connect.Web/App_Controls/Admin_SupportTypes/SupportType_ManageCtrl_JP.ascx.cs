﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.CodeParser;
using DevExpress.Web;
using DevExpress.Web.Data;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls.Admin_SupportTypes
{
    public partial class SupportType_ManageCtrl_JP : System.Web.UI.UserControl
    {
        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_SupportTypeAdmin"; }
        }
        protected string GetStaticResource(string resourceKey)
        {
            var globalResourceObject = this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey);
            if (globalResourceObject != null)
                return globalResourceObject.ToString();
            else
            {
                return string.Empty;
            }

        }
        private const String AreaEffected = "Manage Support Types JP";
        private static String LoggedInUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            BindData();

            if (!IsPostBack)
            {
                InitSearchOptions();
            }
        }

        private void BindData()
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.AdminSupportTypeSearchOptionsGet(false);
            if (searchOptions == null || searchOptions.Count < 1) return;

            String model = String.Empty;
            String serial = String.Empty;
            String email = String.Empty;

            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "MN":
                        model = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "SN":
                        serial = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "UserEmail":
                        email = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                }
            }

            var supportList = BLL_SupportEntry.GetProductSupportList(model, serial, email);
            gridSupportType.DataSource = supportList;
            gridSupportType.DataBind();
        }

        private void InitSearchOptions()
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.AdminSupportTypeSearchOptionsGet(false);
            if (searchOptions == null || searchOptions.Count < 1) return;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "MN":
                        cbxSearchByMN.Checked = true;
                        txtSearchByMN.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "SN":
                        cbxSearchBySN.Checked = true;
                        txtSearchBySN.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "UserEmail":
                        cbxSearchByEmail.Checked = true;
                        txtSearchByEmail.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                }
            }
        }

        /// <summary>
        /// Click the search Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bttSupportTypeSearch_Click(object sender, EventArgs e)
        {
            String model = String.Empty;
            String serial = String.Empty;
            String email = String.Empty;

            List<SearchByOption> searchOptions = Admin_SearchUtility.AdminSupportTypeSearchOptionsGet(true);
    
            if (cbxSearchByMN.Checked)
            {
                model = txtSearchByMN.Text.Trim();
                searchOptions.Add(new SearchByOption("MN", model));
            }

            if (cbxSearchByEmail.Checked)
            {
                email = txtSearchByEmail.Text.Trim();
                searchOptions.Add(new SearchByOption("UserEmail", email));
            }

            if (cbxSearchBySN.Checked)
            {
                serial = txtSearchBySN.Text.Trim();
                searchOptions.Add(new SearchByOption("SN", serial));
            }

            if (searchOptions.Count != 0)
            {
                Admin_SearchUtility.AdminSupportTypeSearchOptionsSet(searchOptions);
                WebUtility.HttpRedirect(null, "~/prodregadmin/productsupport/search_jpsupporttypeentry");
            }
            else
            {
                // Error
                lblErrorMsg.Text = GetStaticResource("MSG_SearchOptionRequired");
                lblErrorMsg.Visible = true;
                return;
            }
        }


        protected void gridSupportType_OnRowUpdating(object sender, ASPxDataUpdatingEventArgs e)
        {
            var id = ConvertUtility.ConvertToInt32(e.Keys[0], 0);

            var formLayout = (ASPxFormLayout)gridSupportType.FindEditFormTemplateControl("formLayout");
            var modelNumber = formLayout.FindControl("dxlblModelNumber") as ASPxTextBox;
            if (modelNumber.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqMdlNum"));
            }
            var dxMemId = formLayout.FindControl("dxMemId") as HiddenField;
            var dxAcctId = formLayout.FindControl("dxAcctId") as HiddenField;
            var serialNumber = formLayout.FindControl("dxtxtSerialNumber") as ASPxTextBox;
            var dxtxtEmailOrg = formLayout.FindControl("dxtxtEmailOrg") as ASPxTextBox;
            if (serialNumber.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqSerNum"));
            }
            var selType = formLayout.FindControl("ddlSelTypeEdit") as ASPxComboBox;
            var emailOrg = formLayout.FindControl("dxtxtEmailOrg") as ASPxTextBox;

            if (emailOrg.Text.Trim().Length.Equals(0))
            {
                throw new Exception(GetStaticResource("ReqEaOrg"));
            }
            if (selType != null && selType.SelectedIndex.Equals(1))
            {
                if (dxtxtEmailOrg != null && LoadSelectTypes(dxtxtEmailOrg.Text).Equals("1"))
                {
                    throw new Exception(GetStaticResource("ReqValidEmail"));
                }
            }
            var placeHolder = formLayout.FindControl("phEditControls") as PlaceHolder;
            var formValuesDictionary = new Dictionary<string, object>();
            formValuesDictionary.Add("Id", id);

            if (selType != null)
            {
                if (selType.SelectedIndex.Equals(1))
                    formValuesDictionary.Add("MembershipId", selType.SelectedIndex.Equals(1) ? Convert.ToString(dxMemId.Value) : string.Empty);
                else
                {
                    formValuesDictionary.Add("MembershipId", string.Empty);
                }
                if (selType.SelectedIndex.Equals(2))
                    formValuesDictionary.Add("OrgId", selType.SelectedIndex.Equals(2) ? Convert.ToString(dxAcctId.Value) : string.Empty);
                else { formValuesDictionary.Add("OrgId", string.Empty); }
            }
            formValuesDictionary.Add("ModelNumber", modelNumber != null && modelNumber.Text.Trim().Length > 0 ? modelNumber.Text : string.Empty);
            formValuesDictionary.Add("SerialNumber", serialNumber != null && serialNumber.Text.Trim().Length > 0 ? serialNumber.Text : string.Empty);
            var supportTypesDictionary = new Dictionary<string, string>();
            var fLayout = (ASPxFormLayout)gridSupportType.FindEditFormTemplateControl("formLayout");
            var sctrl = fLayout.FindControl("supTypeNew") as SupportTypes_Ctrl_JP;
            if (sctrl != null)
            {
                var phEditCtrl = sctrl.FindControl("phEditControls") as PlaceHolder;
                if (phEditCtrl != null)
                {
                    GetSupportValue(phEditCtrl, ref supportTypesDictionary);

                }
            }
            if (supportTypesDictionary.Count.Equals(0))
            {
                throw new Exception(GetStaticResource("lblMsgSelect.Text"));

            }
            formValuesDictionary.Add("CreatedBy", LoginUtility.GetCurrentUser(true).Email);
            formValuesDictionary.Add("SupportType", supportTypesDictionary);
            formValuesDictionary.Add("ModelConfigType",ModelConfigTypeInfo.CONFIGTYPE_JP);
            bool exist = GetSupportType(formValuesDictionary);

            if (dxtxtEmailOrg != null)
                LogAdminActivity(AreaEffected, TasksPerformed.Modify, ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_ATTEMPT, LoggedInUserEmail, modelNumber.Text.Trim(), serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
            if (exist)
            {
                throw new Exception(GetStaticResource("lblMsgExist.Text"));
            }
            var i = (int)BLL_SupportEntry.UpdateProductSupportEntry(formValuesDictionary);
            if (i > 0)
            {
                if (dxtxtEmailOrg != null)
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify,
                        ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_SUCCESS, LoggedInUserEmail, modelNumber.Text.Trim(),
                        serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
                e.Cancel = true;
                gridSupportType.CancelEdit();
                gridSupportType.DataBind();

            }
            else
            {
                if (dxtxtEmailOrg != null)
                    LogAdminActivity(AreaEffected, TasksPerformed.Modify,
                        ActivityLogNotes.SUPPORTTYPEENTRY_MODIFY_FAIL, LoggedInUserEmail, modelNumber.Text.Trim(),
                        serialNumber.Text.Trim(), dxtxtEmailOrg.Text.Trim());
            }
        }
        protected void gridSupportType_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var id = ConvertUtility.ConvertToInt32(e.Keys[0], 0);
            var prodsup =(ProductSupportEntity) BLL_SupportEntry.GetProductSupportEntry(id);

            LogAdminActivity(AreaEffected, TasksPerformed.Delete, ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_ATTEMPT, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
            int deleted = (Int32)BLL_SupportEntry.DeleteProductSupportEntry(id.ToString());
            e.Cancel = true;
            if (deleted > 0)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alt",
                    "<script>alert('" + GetStaticResource("DelMsg") + "')</script>");

                LogAdminActivity(AreaEffected, TasksPerformed.Delete,
                    ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_SUCCESS, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
                LoadData();

            }
            else
            {

                LogAdminActivity(AreaEffected, TasksPerformed.Delete,
                    ActivityLogNotes.SUPPORTTYPEENTRY_DELETE_FAIL, LoggedInUserEmail, prodsup.ModelNum, prodsup.SerialNum, prodsup.EmailOrg);
            }
        }
        protected void gridSupportType_OnPageIndexChanged(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;
            if (grid == null) return;
            var pageIndex = grid.PageIndex;
            gridSupportType.PageIndex = pageIndex;
            LoadData();
        }
        protected void UpdateButton_OnDataBinding(object sender, EventArgs e)
        {
        }
        protected void gridSupportType_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
        }

        #endregion
        #region private methods

        private static void GetSupportValue(PlaceHolder phEditCtrl, ref Dictionary<string, string> supportTypesDictionary)
        {
            foreach (Dynamic_SupportTypeCtrl_JP dynSupCtrl in phEditCtrl.Controls)
            {
                var ade1 = dynSupCtrl.FindControl("adExpiry") as ASPxDateEdit;
                var cbx = dynSupCtrl.FindControl("cblSupType1") as ASPxCheckBox;
                if (cbx != null && cbx.Checked)
                {
                    supportTypesDictionary.Add(dynSupCtrl.SupportValue, ade1.Text);
                }

            }
        }
        public string CreateSupportTypes(object id)
        {
            var sBuilder = new StringBuilder();
            var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
            //var obj = (IEnumerable<object>) mappings;
            foreach (SupportEntryMapping type in mappings)
            {
                sBuilder.Append(type.SupportTypeCode);
                sBuilder.Append("-");
                if (type.ExpiryDate.HasValue)
                {
                    sBuilder.Append(type.ExpiryDate.GetValueOrDefault().ToString("MM/dd/yyyy"));
                }
                else
                {
                    sBuilder.Append(this.GetGlobalResourceObject("STCTRL_JpPrivateresources", "notset"));
                }
                sBuilder.Append(" ");
            }
            return sBuilder.ToString();
        }
        private void LoadData()
        {
            gridSupportType.DataBind();
        }
        public string LoadSelectTypes(object selType)
        {
            try
            {
                if (selType != null)
                {
                    var m = new MailAddress(selType.ToString());
                }

                return "0";
            }
            catch (FormatException)
            {
                return "1";
            }



        }
        public bool SupportTypes(object id, string val)
        {
            if (id != null)
            {
                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
                bool check = false;
                foreach (SupportEntryMapping type in mappings)
                {
                    if (val != null && type.SupportTypeCode.Equals(val))
                    {
                        check = true;
                    }
                }
                return check;
            }
            else
            {
                return false;

            }
        }
        public string SupportTypesDate(object id, string val)
        {
            if (id != null)
            {
                var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
                string date = string.Empty;
                foreach (SupportEntryMapping type in mappings)
                {
                    if (val != null && type.SupportTypeCode.Equals(val))
                    {
                        if (type.ExpiryDate.HasValue)
                        {
                            date = type.ExpiryDate.Value.ToShortDateString();
                        }

                    }
                }
                return date;
            }
            else
            {
                return "";

            }
        }
        public bool GetSupportType(dynamic formValues)
        {
            return BLL_SupportEntry.GetSupportTypeEntry(formValues);
        }
        private void LogAdminActivity(String areEffected, String action, String taskPerformed, String userEmail, params object[] paramsArgs)
        {
            taskPerformed = String.Format(taskPerformed, paramsArgs);
            Lib.ActivityLog.ActivityLogBLL.Insert(LoggedInUserEmail, AreaEffected, taskPerformed, action, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
        }
        #endregion
    }
}