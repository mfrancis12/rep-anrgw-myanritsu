﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon.EC2.Model;
using Anritsu.Connect.EntityFramework;

namespace Anritsu.Connect.Web.App_Controls.Admin_SupportTypes
{
    public partial class SupportTypes_Ctrl_JP : System.Web.UI.UserControl
    {

        public bool IsNew { get; set; }
        public string PrId { get; set; }

        public void BindData(string PrId)
        {
            LoadSupportTypes(phEditControls, PrId, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsNew)
            {
                LoadSupportTypes(phEditControls, string.Empty, false);
            }
            else
            {
                if (!string.IsNullOrEmpty(Convert.ToString(PrId)))
                {
                    LoadSupportTypes(phEditControls, PrId, true);
                    //LoadSupportTypes(phEditControls, string.Empty, false);
                }


            }

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //this.PrId = PrId;
        }


        private void LoadSupportTypes(PlaceHolder idHolder, string id, bool update)
        {
            var sTypes = BLL_SupportEntry.GetSupportTypes() as List<SupportType>;

            if (sTypes != null)
                foreach (SupportType supType in sTypes)
                {
                    var control = (Dynamic_SupportTypeCtrl_JP)LoadControl("Dynamic_SupportTypeCtrl_JP.ascx");
                    control.ID = "sctrl_" + supType.SupportTypeCode;
                    control.SupportValue = supType.SupportTypeCode;
                    if (update)
                    {
                        var mappings = BLL_SupportEntry.GetMappings(int.Parse(Convert.ToString(id)));
                        control.Checked = (mappings.Where(i => i.SupportTypeCode.Equals(supType.SupportTypeCode) && i.SupportTypeEntryId.Equals(int.Parse(id))).ToList().Any()) ? true : false;
                        control.DisplayText = string.Format("{0}{1}", supType.SupportTypeCode, supType.Description);
                        var supportEntryMapping = mappings.FirstOrDefault(i => i.SupportTypeCode.Equals(supType.SupportTypeCode) &&
                                                                               i.SupportTypeEntryId.Equals(int.Parse(id)));
                        if (supportEntryMapping != null)
                            if (supportEntryMapping
                                .ExpiryDate != null)
                                control.ExpiryDateTime =
                                    supportEntryMapping
                                        .ExpiryDate.Value.ToShortDateString();
                    }
                    else
                    {
                        control.Checked = false;
                        control.DisplayText = string.Format("{0}{1}", supType.SupportTypeCode, supType.Description);
                    }

                    idHolder.Controls.Add(control);
                }


        }

        protected void phEditControls_DataBinding(object sender, EventArgs e)
        {
            phEditControls.Controls.Clear();

            if (!string.IsNullOrEmpty(PrId)) BindData(PrId);
        }
    }
}