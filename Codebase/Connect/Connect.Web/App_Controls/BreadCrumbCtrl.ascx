﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumbCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.BreadCrumbCtrl" %>
    <div class="breadcrumb">
       <ul>
<asp:Repeater ID="rptBreadCrumb" runat="server">
    <ItemTemplate>
        <li>
             <asp:HyperLink ID="hlBC" runat="server" CssClass='<%# GetCssClass(Eval("IsCurrentPage")) %>' EnableTheming="false" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "PageUrl") %>' Text='<%# DataBinder.Eval(Container.DataItem, "PageTitle") %>'></asp:HyperLink>
        </li>
    </ItemTemplate>
</asp:Repeater>
           </ul>  
    </div>
