﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class BreadCrumbCtrl : UserControl
    {
        public class BreadCrumbData
        {
            public string PageUrl { get; set; }
            public string PageTitle { get; set; }
            public bool IsCurrentPage { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            var breadCrumbList = GetBreadCrumbData();
            if (breadCrumbList.Count > 0) breadCrumbList[breadCrumbList.Count - 1].PageUrl = String.Empty;
            rptBreadCrumb.DataSource = breadCrumbList;
            rptBreadCrumb.DataBind();
            if (breadCrumbList.Count <= 0) return;
        }

        private List<BreadCrumbData> GetBreadCrumbData()
        {
            var url = SiteUtility.GetPageUrl().ToLowerInvariant();
            var culture = CultureInfo.CurrentUICulture;
            if (String.IsNullOrEmpty(culture.Name)) culture = new CultureInfo("en");
            var breadcrumbcache = Application["breadcrumbdata"] as Dictionary<string, List<BreadCrumbData>>;
            if (breadcrumbcache == null)
            {
                breadcrumbcache = new Dictionary<string, List<BreadCrumbData>>();
                Application["breadcrumbdata"] = breadcrumbcache;
            }
            List<BreadCrumbData> lst = null;
            var key = new StringBuilder();
          
            lock (Application["breadcrumbdata"])
            {
                key.Append(url);
                key.Append("-");
                key.Append(culture.Name);

                if (breadcrumbcache.ContainsKey(key.ToString()))
                {
                    lst = breadcrumbcache[key.ToString()];
                }
                if (lst != null) return lst;
                lst = new List<BreadCrumbData>();
                var lstSitemap = BLLSite_Sitemap.SelectForBreadCrumbByPageUrl(url);
                if (lstSitemap == null) return lst;
                foreach (var snode in lstSitemap)
                {
                    var data = new BreadCrumbData {IsCurrentPage = (snode.PageUrl == url), PageUrl = snode.PageUrl};
                    var text = GetGlobalResourceObject(snode.PageUrl, "BreadCrumbText").ToString();
                    if (String.IsNullOrEmpty(text) || text.Equals("BreadCrumbText")) text = this.GetGlobalResourceObject(snode.PageUrl, "PageTitle").ToString();
                    data.PageTitle = text;
                    lst.Add(data);
                }
                breadcrumbcache.Add(key.ToString(), lst);
                Application["breadcrumbdata"] = breadcrumbcache;
            }
            return lst;
        }

        protected String GetCssClass(object iSCurrentPage)
        {
            var curretnPage = ConvertUtility.ConvertToBoolean(iSCurrentPage, true);
            return curretnPage ? "active" : "unselectedcrumb";
        }
    }
}