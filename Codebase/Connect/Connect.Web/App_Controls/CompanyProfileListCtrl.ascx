﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyProfileListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.CompanyProfileListCtrl" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib.Utils" %>

<div class="settingrow" style="padding-bottom: 10px;">
    <asp:Localize ID="lcalAccountListIntro" runat="server" Text="<%$Resources:STCTRL_CompanyProfileListCtrl,lcalAccountListIntro.Text%>"></asp:Localize>
</div>
<div class="settingrow">
    <asp:HyperLink ID="hlAddNewOrg" runat="server" Text="<%$Resources:STCTRL_CompanyProfileListCtrl,hlAddNewOrg.Text%>" NavigateUrl="~/myaccount/companyprofile?addnew=1" SkinID="blueit"></asp:HyperLink>
</div>
<div class="settingrow">

    <div class="settingrow">
        <asp:GridView ID="gvOrg" runat="server" AutoGenerateColumns="false" DataKeyNames="AccountID" Width="100%" OnRowDataBound="gvOrg_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText='<%$ Resources:STCTRL_CompanyProfileListCtrl,dgList.Columns.CompanyName.HeaderText %>'>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkCompName" runat="server" Text='<%#Eval("CompanyName") %>' OnCommand="lnkCompName_Command" CommandArgument='<%#Eval("AccountID") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:STCTRL_CompanyProfileListCtrl,dgList.Columns.AccountName.HeaderText %>'>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkAccountName" runat="server" Text='<%#Eval("AccountName") %>' OnCommand="lnkCompName_Command" CommandArgument='<%#Eval("AccountID") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="50" Visible="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="hyplnkAccountid" ToolTip="user details" runat="server" Text='<%#Eval("AccountID") %>' Visible="false"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkActStatus" ToolTip="user details" runat="server" Text='<%#Eval("AccountStatusCode") %>' Visible="false"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
               
              <asp:TemplateField>
                    <ItemTemplate>
                        <dx:ASPxHyperLink ID="lnkEdit" runat="server" Text="Edit" NavigateUrl='<%#"~/myaccount/companyprofile?accid="+Eval("AccountID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <dx:ASPxHyperLink ID="lnkMembers" runat="server" Text='<%$Resources:STCTRL_CompanyProfileListCtrl,lcalMembers.Text%>' NavigateUrl='<%#"~/myaccount/manageusers?accid="+Eval("AccountID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>

    </div>

</div>

<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_CompanyProfileListCtrl" />
