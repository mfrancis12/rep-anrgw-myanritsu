﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class CompanyProfileListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                //gvOrg.Columns[4].Visible = gvOrg.Columns[5].Visible = LoginUtility.IsAdminMode();
            }
        }

        private void BindGrid()
        {
            gvOrg.DataSource = AccountUtility.AccountsForCurrentUserGet(false);
            gvOrg.DataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_CompanyProfileListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

       

        protected void gvOrg_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAcctStatus = (LinkButton)e.Row.FindControl("lnkActStatus");
                LinkButton lnkAcctId = (LinkButton)e.Row.FindControl("hyplnkAccountid");
                string status = lnkAcctStatus.Text;
                Guid accountID = ConvertUtility.ConvertToGuid(lnkAcctId.Text, Guid.Empty);
                if (accountID == AccountUtility.GetSelectedAccountID(false))
                {
                    e.Row.CssClass = "gvstyle1-head";
                    //cadgOrgs.Select(args.Item);
                }
            }
        }

        
        protected void lnkCompName_Command(object sender, CommandEventArgs e)
        {
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            //LinkButton lnkAcctId = (LinkButton)e.CommandArgument;

            Guid accountID = ConvertUtility.ConvertToGuid(e.CommandArgument, Guid.Empty);
            AccountUtility.SetSelectedAccountID(accountID, user);
            //UIHelper.MyRegisteredProductsCacheSet(null); //reset everytime someone selects another account
            WebUtility.HttpRedirect(null, KeyDef.UrlList.Home);
        }

    

        
    }
}