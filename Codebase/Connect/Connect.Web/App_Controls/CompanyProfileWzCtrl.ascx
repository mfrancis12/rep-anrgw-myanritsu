﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyProfileWzCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.CompanyProfileWzCtrl" %>
<%@ Register Src="AddressCtrl.ascx" TagName="AddressCtrl" TagPrefix="uc1" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_CompanyProfileWzCtrl,pnlContainer.HeaderText%>">
    <script type="text/javascript">
        // <![CDATA[
        function SetPCVisible(value) {
            var popupControl = GetPopupControl();
            if (value)
                popupControl.Show();
            else
                popupControl.Hide();
        }
        function SetImageState(value) {
            var img = document.getElementById('imgButton');
            var imgSrc = value ? 'Images/Buttons/pcButtonPress.gif' : 'Images/Buttons/pcButton.gif';
            img.src = imgSrc;
        }
        function GetPopupControl() {
            return ASPxPopupClientControl;
        }
        // ]]> 
    </script>
    <div id="divOrgUpdateMsg" runat="server" class="settingrow">

        <asp:Localize ID="lcalUpdateOrgInfo" runat="server" Visible="false" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,UpdateProfileMsg%>"></asp:Localize>
        <asp:Localize ID="lcalSaveOrgInfo" runat="server" Visible="false"></asp:Localize>
    </div>
    <div class="settingrow">
    </div>
    <%--<div class="settingrow">
        <asp:Localize ID="lcalCompanyInfoIntro" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyInfoIntro.Text%>"></asp:Localize>
    </div>--%>

 <%--   <div class="settingrow">
        <span class="settinglabel-10">
            <asp:Localize ID="lcalCompanyNameInRuby" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyNameInRuby.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCompanyNameInRuby" runat="server" SkinID="tbx-400" MaxLength="100"></asp:TextBox>
    </div>--%>

    <div class="settingrow group input-text width-60 required">
        <label>
            <asp:Localize ID="lcalAccountName" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalAccountName.Text%>"></asp:Localize>:*</label>
        <p class="error-message"><asp:RequiredFieldValidator ID="rfvAccountName" runat="server" ControlToValidate="txtAccountName" InitialValue="Please Update"
            Display="Dynamic" ValidationGroup="vgCompInfo" ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,rfvAccountName.ErrorMessage%>"></asp:RequiredFieldValidator></p>
        <asp:TextBox ID="txtAccountName" runat="server" MaxLength="50"></asp:TextBox>
        
        <asp:RegularExpressionValidator ID="revAccountName" runat="server" ControlToValidate="txtAccountName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,revAccountName.ErrorMessage%>"></asp:RegularExpressionValidator>
    </div>
    <div style="font-size:11px;">
        <asp:Localize ID="lcalAccountInfoIntro" runat="server"></asp:Localize>
        <dx:ASPxImage ID="imgMoreInfo" runat="server"  Width="11px"
            Height="11px" Visible="false" Cursor="pointer" ToolTip="<%$Resources:STCTRL_CompanyProfileWzCtrl,imgMoreInfo.Text%>">
        </dx:ASPxImage>

        <dx:ASPxPopupControl ClientInstanceName="ASPxPopupClientControl" Width="330px" Height="100px" Theme="AnritsuDevXTheme"
            MaxWidth="800px" MaxHeight="800px" MinHeight="100px" MinWidth="150px" ID="pcMain"
            ShowFooter="false" PopupElementID="imgMoreInfo" HeaderText=""
            runat="server" PopupHorizontalAlign="LeftSides" PopupVerticalAlign="Below" EnableHierarchyRecreation="True">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <asp:Panel ID="Panel1" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Localize ID="lcalAccountFullInfo" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalAccountInfoIntro.Text%>"></asp:Localize>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) { SetImageState(false); }" PopUp="function(s, e) { SetImageState(true); }" />
        </dx:ASPxPopupControl>

    </div>
    <div class="settingrow">
    </div>
        <div class="settingrow group input-text width-60 required">
        <label>
            <asp:Localize ID="lcalCompanyName" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyName.Text%>"></asp:Localize>*</label>
        <p class="error-message"><asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName"
            Display="Dynamic" ValidationGroup="vgCompInfo" ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,rfvCompanyName.ErrorMessage%>"></asp:RequiredFieldValidator></p>
        <asp:TextBox ID="txtCompanyName" runat="server" ReadOnly="True" MaxLength="100"></asp:TextBox>
        
        <asp:RegularExpressionValidator ID="revCompanyName" runat="server" ControlToValidate="txtCompanyName"
            Display="Dynamic" ValidationExpression='^[^<>%$`!?^*=;"]*$' ValidationGroup="vgCompInfo"
            ErrorMessage="<%$Resources:STCTRL_CompanyProfileWzCtrl,revCompanyName.ErrorMessage%>"></asp:RegularExpressionValidator>
    </div>
    <div style="font-size:11px;">
        <asp:Localize ID="lcalCompanyInfoTitle" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyInfoTitle.Text %>"></asp:Localize>
    </div>
    <div class="settingrow">
    </div>
  
    <div class="settingrow group input-text width-60">
        <label>
            <asp:Localize ID="lcalCompanyAddress" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalCompanyAddress.Text%>"></asp:Localize></label>
        <div>
            <uc1:AddressCtrl ID="addrCompanyAddress" runat="server" ValidationGroup="vgCompInfo"
                IsFaxVisible="false" IsFaxRequired="false" IsPhoneVisible="false" IsPhoneRequired="false" />
        </div>
    </div>
    <div class="settingrow">
    </div>
    <div class="settingrow" style="display: none">
        <span class="settinglabelplain settingrowfield">
            <asp:Localize ID="lcalUserAnritsuID" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalUserAnritsuID.Text%>" Visible="false"></asp:Localize></span>
        <span>
            <asp:Literal ID="ltrUserAnritsuID" runat="server" Visible="false"></asp:Literal>
        </span>
    </div>
    <%--<div id="divUserName" runat="server" class="settingrow">
        <span class="settinglabelplain settingrowfield">
            <asp:Localize ID="lcalUserName" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalUserName.Text%>"></asp:Localize></span>
        <span>
            <asp:Literal ID="ltrUserName" runat="server"></asp:Literal></span>
    </div>--%>
   <%-- <div class="settingrow" style="margin-left: 115px;">
        <asp:HyperLink ID="hlEditCompanyInfo" runat="server" Text="<%$ Resources:STCTRL_CompanyProfileWzCtrl,hlEditCompanyInfo.Text %>" Visible="false" ></asp:HyperLink>

    </div>--%>
    <%--<div class="settingrow">
        <br />
    </div>--%>
  <%--  <div class="settingrow company-profile-radio-container">
        <span style="margin-left: 110px;display: inline-block;">
            <asp:RadioButton ID="rdoIndividualAnritsuID" SkinID="RadioBlock" runat="server" Checked="true" GroupName="EmailTypeGroup"
                Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,rdoIndividualAnritsuID.Text%>" />
        </span>
        <br />
        <span style="margin-left: 110px;display: inline-block;">
            <asp:RadioButton ID="rdoGroupAnritsuID" SkinID="RadioBlock" runat="server" GroupName="EmailTypeGroup"
                Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,rdoGroupAnritsuID.Text%>" />
        </span>
    </div>--%>

    <%-- <asp:Panel ID="pnlHQMsg" runat="server" Visible="false">
        <div class="settingrow">
            <asp:Localize ID="lcalSuggestToAddHQInfo" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalSuggestToAddHQInfo.Text%>"></asp:Localize>
        </div>
    </asp:Panel>--%>
    <asp:HiddenField ID="hddnAccountID" runat="server" />
    <div class="settingrow">
        <span class="msg">
            <asp:Literal ID="ltrCompanyBasicInfo" runat="server"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <asp:Button ID="bttSaveCompanyProfile" runat="server" Text="<%$Resources:STCTRL_CompanyProfileWzCtrl,bttSaveCompanyProfile.Text%>"
            ValidationGroup="vgCompInfo" CausesValidation="true" OnClick="bttSaveCompanyProfile_Click" CssClass="form-submit"/>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_CompanyProfileWzCtrl" />
