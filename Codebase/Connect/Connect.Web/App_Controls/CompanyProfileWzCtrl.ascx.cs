﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class CompanyProfileWzCtrl : UserControl, IStaticLocalizedCtrl
    {
        public Guid AccountId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID],
                    ConvertUtility.ConvertToGuid(hddnAccountID.Value, Guid.Empty));
            }
            private set { hddnAccountID.Value = ConvertUtility.ConvertToString(value, String.Empty); }
        }

        public Boolean IsAdminMode { get; set; }

        public Boolean AddAsNew { get; set; }

        public String ReturnUrl
        {
            get
            {
                if (HttpContext.Current.Session != null)
                {
                    return HttpContext.Current.Session[KeyDef.QSKeys.ReturnURL].ToString();
                }
                else
                {
                    return "~/myaccount/selectcompany";
                }
                //String returnUrl = Request.QueryString[KeyDef.QSKeys.ReturnURL];
                //if (String.IsNullOrEmpty(returnUrl)) returnUrl = "~/myaccount/selectcompany";
                //return returnUrl;
            }
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_CompanyProfileWzCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion
        protected override void OnInit(EventArgs e)
        {
            imgMoreInfo.ImageUrl = ConfigKeys.GwdataCdnPath +
                                   "/images/legacy-images/apps/connect/img/smquestionmark.jpg";
        }
    private void ShowTeamInformation()
        {
            string str = GetResourceString("lcalAccountInfoIntro.Text");
            if (str.Length > 65)
            {
                lcalAccountInfoIntro.Text = str.Substring(0, 60) + "...";
                imgMoreInfo.Visible = true;
            }
            else
            {
                lcalAccountInfoIntro.Text = str;
                imgMoreInfo.Visible = false;
            }
            //set team name
            var acc = BLLAcc_AccountMaster.SelectByAccountID(AccountId);
            txtAccountName.Text = (acc == null || AddAsNew) ? string.Empty : acc.AccountName;
        }

        public void LoadCompanyProfile(Guid accountId)
        {
            AccountId = accountId;
            //load looged in user
            var currContextUser = LoginUtility.GetCurrentUserOrSignIn();
            if (accountId != Guid.Empty && !AddAsNew && (LoginUtility.IsAdminMode()))
            {
                //load Team owner profile details
                var memberShipUser = BLLSec_UserMembership.SelectByTeamOwnerId(accountId);
                if (memberShipUser != null)
                {
                    //load user profile from SSO for company name
                    var ssoUserData = BLLSSOUser.SelectByEmailOrUserName(memberShipUser.Email);
                    if (ssoUserData != null)
                    {
                        currContextUser = ConnectSsoUser.SetMyAnritsuUser(memberShipUser, ssoUserData);
                    }
                }
            }
            var acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountId);
            if (acc.InfoUpdateRequired)
            {
                lcalUpdateOrgInfo.Visible = true;
                lcalSaveOrgInfo.Visible = false;
            }

            if (currContextUser == null) return;
            ltrUserAnritsuID.Text = currContextUser.Email;
            //ltrUserName.Text = usr.LastName + "," + usr.FirstName;
            txtCompanyName.Text = currContextUser.CompanyName;
            //load Address 
            addrCompanyAddress.SetAddress(new Profile_Address()
            {
                Address1 = currContextUser.UserStreetAddress1,
                Address2 = currContextUser.UserStreetAddress2,
                CityTown = currContextUser.UserCity,
                CountryCode = currContextUser.UserAddressCountryCode,
                FaxNumber = currContextUser.FaxNumber,
                PhoneNumber = currContextUser.PhoneNumber,
                ZipPostalCode = currContextUser.UserZipCode,
                StateProvince = currContextUser.UserState
            });
            bttSaveCompanyProfile.Visible = !IsAdminMode;
            if (!acc.InfoUpdateRequired)
            {
                var accountName = "<span style='font-size:11px; font-weight:normal'>" + acc.AccountName + "</span>";
                pnlContainer.HeaderText += " - " + accountName;
                lcalUpdateOrgInfo.Visible = false;
                lcalSaveOrgInfo.Visible = true;
                lcalSaveOrgInfo.Text = GetResourceString("lcalSaveOrgInfo.Text");
                bttSaveCompanyProfile.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmSaveOrgMsg") +
                                                      "')";
            }
            else
            {
                lcalUpdateOrgInfo.Visible = true;
                lcalSaveOrgInfo.Visible = false;
            }
            ShowTeamInformation();
            lcalSaveOrgInfo.Visible = true;
            lcalSaveOrgInfo.Text = GetResourceString("AddNewOrgMsg");
            CheckAccountName();
        }

        private void CheckAccountName()
        {
            if (!txtAccountName.Text.IsNullOrEmptyString() &&
                !txtAccountName.Text.Trim().Equals("Please Update", StringComparison.InvariantCultureIgnoreCase))
                return;
            rfvAccountName.IsValid = false;
            rfvAccountName.Visible = true;
        }

        public void SaveCompanyBasicInfo()
        {
            try
            {
                //get selected account
                var acc = BLLAcc_AccountMaster.SelectByAccountID(AccountId);

                #region " validation "

                if (txtCompanyName.Text.IsNullOrEmptyString()) throw new ArgumentException(GetStaticResource("ERROR_CompanyNameRequired"));
                if (txtAccountName.Text.IsNullOrEmptyString()) throw new ArgumentException(GetStaticResource("ERROR_AccountNameRequired"));
                if (txtAccountName.Text.Equals(KeyDef.MsgKeys.DefaultOrg)) throw new ArgumentException(GetStaticResource("ERROR_UpdateAccountName"));

                #endregion

                var user = LoginUtility.GetCurrentUserOrSignIn();
                if (acc == null || AddAsNew)
                {
                    //Create new team
                    BLLAcc_AccountMaster.CreateAccount(user.MembershipId
                        , txtAccountName.Text.Trim()
                        , txtCompanyName.Text.Trim()
                        , String.Empty
                        , WebUtility.GetUserIP()
                        , user.Email
                        , user.FullName);

                    var selectedAccId = AccountUtility.GetSelectedAccountID(false);
                    if (!selectedAccId.IsNullOrEmptyGuid())
                    {
                        if (!selectedAccId.Equals(AccountId))
                        {
                            AccountUtility.SetSelectedAccountID(AccountId, user);
                        }
                    }
                }
                else
                {
                    //update Team
                    var isAccountCreateHistory = !acc.AccountName.Equals(txtAccountName.Text.Trim()) || !acc.CompanyName.Equals(txtCompanyName.Text.Trim());
                    BLLAcc_AccountMaster.Update(AccountId,
                        txtAccountName.Text.Trim(),
                        txtCompanyName.Text.Trim(),
                        String.Empty,
                        isAccountCreateHistory);

                    #region User account update notification
                    String msg;
                    if (isAccountCreateHistory)
                        BLLAcc_AccountMaster.OldAccountMaster = acc; BLLAcc_AccountMaster.UserAccUpdateNotifyAdmin(AccountId, isAccountCreateHistory, false, user, out msg);

                    if (!string.IsNullOrEmpty(msg))
                        throw new ArgumentException("Unable to send email - " + msg);
                    #endregion
                }


                #region Save user email info

                //pull this value from GW sso user profile after we move these fields to SSO edit account
                //BLLSec_UserMembership.UpdateUserEmailInfo(user.MembershipId, true);
                #endregion

                AccountUtility.GetSelectedAccount(true, false);
                WebUtility.HttpRedirect(null, ReturnUrl);

            }
            catch (ArgumentException aex)
            {
                ltrCompanyBasicInfo.Text = aex.Message;
            }
        }

        public static string GetResourceString(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_CompanyProfileWzCtrl", resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        public static string GetReadOnlyCompanyInfo(Guid accountId, Boolean includePhone, Boolean includeFax, Boolean usehtml)
        {
            Acc_AccountMaster acc = BLLAcc_AccountMaster.SelectByAccountID(accountId);
            if (acc == null) return String.Empty;
            StringBuilder sb = new StringBuilder();
            if (usehtml)
            {
                sb.AppendFormat("<div id=\"cpProfile\">");
                if (!acc.AccountName.IsNullOrEmptyString())
                {
                    sb.Append("<div class='settingrow'>");
                    sb.AppendFormat("<span class='settinglabelplain'>{0}</span>", GetResourceString("lcalAccountName.Text"));
                    sb.AppendFormat("<span >{0}</span>", acc.AccountName);
                    sb.Append("</div>");
                }
                if (!acc.CompanyName.IsNullOrEmptyString())
                {
                    sb.Append("<div class='settingrow'>");
                    sb.AppendFormat("<span class='settinglabelplain'>{0}</span>", GetResourceString("lcalCompanyName.Text"));
                    sb.AppendFormat("<span>{0}</span>", acc.CompanyName);
                    sb.Append("</div>");
                }
                if (!acc.OwnerMemberShipID.IsNullOrEmptyGuid())
                {
                    sb.Append("<div class='settingrow'>");
                    sb.AppendFormat("<table cellpadding='0' cellspacing='0'><tr><td valign='top'><span class='settinglabelplain'>{0}</span></td>", GetResourceString("lcalCompanyAddress.Text"));
                    sb.AppendFormat("<td valign='top'>{0}</td>", BLLProfile_Address.SelectAsStringByMemberShipID(acc.OwnerMemberShipID));
                    sb.Append("</tr></table></div>");
                }

                sb.AppendFormat("</div>");
            }
            else
            {
                if (!acc.CompanyName.IsNullOrEmptyString())
                {
                    sb.AppendFormat("{0}: {1}\r\n", GetResourceString("lcalCompanyName.Text"), acc.CompanyName);
                }

                if (!acc.OwnerMemberShipID.IsNullOrEmptyGuid())
                {
                    sb.AppendFormat("{0}\r\n{1}\r\n", GetResourceString("lcalCompanyAddress.Text")
                        , BLLProfile_Address.SelectAsStringByMemberShipID(acc.OwnerMemberShipID));
                }
            }
            return sb.ToString();
        }

        protected void bttSaveCompanyProfile_Click(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) return;
            if (Page.IsValid)
            {
                SaveCompanyBasicInfo();
            }
        }
    }
}