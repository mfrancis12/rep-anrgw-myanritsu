﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.CustomLink;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib;
namespace Anritsu.Connect.Web.App_Controls.EndUser_CustomLink
{
    public class BC_CLnkDisplayContainer : System.Web.UI.UserControl
    {
        public List<CustomLink_Master> GetLinkData(String displayContainerID)
        {
            if (displayContainerID.IsNullOrEmptyString())
            {
                this.Visible = false;
                return null;
            }

            String pageUrl = SiteUtility.GetPageUrl();
            Int32 sisPID = 0;
            if (pageUrl.Equals(KeyDef.UrlList.MyDistributorInfoPages.ProductSupport, StringComparison.InvariantCultureIgnoreCase)
                || pageUrl.Equals(KeyDef.UrlList.MyDistributorInfoPages.ProductSupport_ResourceLib, StringComparison.InvariantCultureIgnoreCase))
            {
                sisPID = ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.SIS_ProductID], 0);
            }
            String modelNumber = String.Empty;
            //add code later for model number


            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);

            List<CustomLink_Master> list = CustomLink_MasterBLL.SelectByDisplayContainerID(false, displayContainerID, false, false, acc, sisPID, modelNumber);
            if (list == null) return null;

            
            foreach (CustomLink_Master clm in list)
            {
                String targetUrl = clm.TargetUrl;
                if (targetUrl.Contains("[[LL-CC]]"))
                {
                    targetUrl = targetUrl.Replace("[[LL-CC]]", acc.GWCultureGroupByCountryCode);
                }
                //add model /serial later
                clm.TargetUrl = targetUrl;
            }
            return list.OrderBy(x => x.LnkOrder).ToList();
        }

        public String GetLinkDisplayText(object lnkResClass)
        {
            String resClass = ConvertUtility.ConvertNullToEmptyString(lnkResClass);
            if (resClass.IsNullOrEmptyString()) return String.Empty;
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(resClass, "Link.Text"));
        }
    }
}