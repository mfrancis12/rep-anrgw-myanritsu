﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CLnkDistplayContainer_DistPortalLeftNavCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_CustomLink.CLnkDistplayContainer_DistPortalLeftNavCtrl" %>

<div id="divCLnkDCtnrDisPtl_LeftNav">
<asp:Repeater ID="rptrClnkDistContainer_DistPortalLeftNav" runat="server">
    <ItemTemplate>
        <div class="item">
        <a href='<%# DataBinder.Eval(Container.DataItem, "TargetUrl") %>' target='<%# DataBinder.Eval(Container.DataItem, "LinkTarget") %>' title=""><img src='<%# DataBinder.Eval(Container.DataItem, "IconImageUrl") %>' alt="" /></a>
            </div>
    </ItemTemplate>
</asp:Repeater>
</div>