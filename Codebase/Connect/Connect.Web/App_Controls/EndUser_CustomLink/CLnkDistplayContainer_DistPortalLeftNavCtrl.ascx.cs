﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls.EndUser_CustomLink
{
    public partial class CLnkDistplayContainer_DistPortalLeftNavCtrl : BC_CLnkDisplayContainer, ICLnkDisplayContainer, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                BindLinks();
            }
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
        
        public string DisplayContainerID
        {
            get { return "distportal-leftnav"; }
        }

        private void BindLinks()
        {
            rptrClnkDistContainer_DistPortalLeftNav.DataSource = GetLinkData(DisplayContainerID);
            rptrClnkDistContainer_DistPortalLeftNav.DataBind();
        }
    }
}