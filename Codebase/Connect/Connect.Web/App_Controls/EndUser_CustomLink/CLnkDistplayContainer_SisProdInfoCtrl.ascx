﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CLnkDistplayContainer_SisProdInfoCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_CustomLink.CLnkDistplayContainer_SisProdInfoCtrl" %>
<div id="divCLnkDCtnrDisPtl_SisProdInfo"><ul>
<asp:Repeater ID="rptrClnkDistContainer_DistPortalSisProdInfo" runat="server">
    <ItemTemplate>
        <li><a href='<%# DataBinder.Eval(Container.DataItem, "TargetUrl") %>' target='<%# DataBinder.Eval(Container.DataItem, "LinkTarget") %>' title=""><%# GetLinkDisplayText(DataBinder.Eval(Container.DataItem, "LnkResClass")) %></a></li>
    </ItemTemplate>
</asp:Repeater>
</ul></div>