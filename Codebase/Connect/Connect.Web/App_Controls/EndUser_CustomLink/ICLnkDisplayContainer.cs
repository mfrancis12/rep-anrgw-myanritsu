﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Controls.EndUser_CustomLink
{
    public interface ICLnkDisplayContainer
    {
        String DisplayContainerID { get; }
    }
}