﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistSIS_ProdInfo_DescCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (SelectedPID < 1) throw new HttpException(404, "Page not found.");
                BindDesc();
            }
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        private void BindDesc()
        {
            Int32 sisLangID = GetSISLangID();
            PIDProductInfoDetails prodDetails = GetProductDetails();
            if (prodDetails != null && prodDetails.TextInfo != null)
            {
                ltrDesc.Text = PIDProductInfoDetailsBLL.Text_GetDescription(prodDetails.TextInfo, sisLangID);
                ltrSpec.Text = PIDProductInfoDetailsBLL.Text_GetSpec(prodDetails.TextInfo, sisLangID);
            }
            else
                this.Visible = false;
        }
    }
}