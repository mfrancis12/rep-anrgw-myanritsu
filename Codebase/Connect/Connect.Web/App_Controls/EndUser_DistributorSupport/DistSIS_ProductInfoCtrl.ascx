﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistSIS_ProductInfoCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.DistSIS_ProductInfoCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_CustomLink/CLnkDistplayContainer_SisProdInfoCtrl.ascx" TagPrefix="uc1" TagName="CLnkDistplayContainer_SisProdInfoCtrl" %>

<div class="settingrow">
    <table>
        <tbody>
            <tr>
                <td>
                    <a id="hrefProductImage" runat="server" rel='prettyPhoto'>
                            <asp:Image ID="imgProdImage" runat="server" Width="200px" CssClass="topbox1image"   AlternateText="" />
                        </a>
                </td>
                <td style="padding: 10px;">
                    <div class="settingrow"><span style="font-size: 18px;font-weight: bold;"><asp:Literal ID="ltrProductName" runat="server"></asp:Literal></span></div>
                    <div class="settingrow">
                        <span style="font-size: 14px;float:left;"><asp:Literal ID="lcalMN" runat="server" Text='<%$ Resources:STCTRL_ProductBasicInfoCtrl,lcalMN.Text %>'></asp:Literal></span>
                        <span style="font-weight: bold; font-size: 14px; float: left; width: 433px; padding-left: 5px"><asp:Literal ID="ltrModelNumber" runat="server"></asp:Literal></span></div>
                    <div id="divObsoleteProduct" runat="server" visible="false" class="settingrow"><span class="label label-important"><asp:Localize ID="lcalObsoleteProduct" runat="server" Text="<%$ Resources:STCTRL_DistSIS_ProductInfoCtrl,lcalObsoleteProduct.Text %>"></asp:Localize></span></div>
                    <div class="settingrow"><uc1:CLnkDistplayContainer_SisProdInfoCtrl runat="server" id="CLnkDistplayContainer_SisProdInfoCtrl1" /></div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_DistSIS_ProductInfoCtrl" /></div>