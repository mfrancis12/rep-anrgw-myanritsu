﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistSIS_ProductInfoCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            imgProdImage.ImageUrl = ConfigKeys.GwdataCdnPath +
                                   "/images/legacy-images/images/gwstyle/NoImage.png";
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (SelectedPID < 1) throw new HttpException(404, "Page not found.");
                BindPIDProductInfo();
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private void BindPIDProductInfo()
        {
            PIDProductInfo sisProdInfo = GetProductInfo();
            if (sisProdInfo == null) throw new HttpException(404, "Page not found.");
            if (!sisProdInfo.ProductImageUrl.IsNullOrEmptyString())
            {
                imgProdImage.ImageUrl = sisProdInfo.ProductImageUrl;
                hrefProductImage.HRef = sisProdInfo.ProductImageUrl;
            }
            
            ltrProductName.Text = sisProdInfo.ProductName;
            ltrModelNumber.Text = sisProdInfo.ModelNumber;
            divObsoleteProduct.Visible = sisProdInfo.IsObsolete;
            //Int32 sisLangID = GetSISLangID();
            //PIDProductInfoDetails prodDetails = GetProductDetails();
            //if (prodDetails != null && prodDetails.TextInfo != null)
            //{
            //    ltrProdDesc.Text = PIDProductInfoDetailsBLL.Text_GetDescription(prodDetails.TextInfo, sisLangID);
            //}

        }

        //protected string GetLinkText(Object linkID, Object resKey)
        //{
        //    String classKey = String.Format("STLNK_{0}", linkID.ToString());
        //    return this.GetGlobalResourceObject(classKey, resKey.ToString()).ToString();
        //}
    }
}