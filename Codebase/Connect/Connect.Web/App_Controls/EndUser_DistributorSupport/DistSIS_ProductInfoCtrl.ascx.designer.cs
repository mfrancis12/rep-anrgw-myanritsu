﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport {
    
    
    public partial class DistSIS_ProductInfoCtrl {
        
        /// <summary>
        /// hrefProductImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor hrefProductImage;
        
        /// <summary>
        /// imgProdImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgProdImage;
        
        /// <summary>
        /// ltrProductName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrProductName;
        
        /// <summary>
        /// lcalMN control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal lcalMN;
        
        /// <summary>
        /// ltrModelNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrModelNumber;
        
        /// <summary>
        /// divObsoleteProduct control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divObsoleteProduct;
        
        /// <summary>
        /// lcalObsoleteProduct control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalObsoleteProduct;
        
        /// <summary>
        /// CLnkDistplayContainer_SisProdInfoCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_CustomLink.CLnkDistplayContainer_SisProdInfoCtrl CLnkDistplayContainer_SisProdInfoCtrl1;
        
        /// <summary>
        /// srep control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.WebControls.StaticResourceEditPanel srep;
    }
}
