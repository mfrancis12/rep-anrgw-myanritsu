﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistSIS_ProductInfoTabsCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            InitTabs();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_DistSIS_ProductInfoTabsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private String GetTabText(String tabName)
        {
            String resKey = String.Format("{0}.Text", tabName);
            return GetStaticResource(resKey);
        }

        public void InitTabs()
        {
            Int32 sisLangID = GetSISLangID();

            PIDProductInfo sisProdInfo = GetProductInfo();
            if (sisProdInfo == null) throw new HttpException(404, "Page not found.");

            String query = String.Format("?{0}={1}", KeyDef.QSKeys.SIS_ProductID, SelectedPID);
            Tab tab;
            int tabIndex = 0;
            String tabName;

            tabName = "tabPID_ProdDesc";
            tab = new Tab(GetTabText(tabName), tabName);
            tab.Enabled = true;
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.MyDistributorInfoPages.ProductSupport + query;
            dxTabDistProdInfoTabs.Tabs.Add(tab);

            tabName = "tabPID_ResLib";
            tab = new Tab(GetTabText(tabName), tabName);
            tab.Enabled = true;
            tab.VisibleIndex = tabIndex++;
            tab.NavigateUrl = KeyDef.UrlList.MyDistributorInfoPages.ProductSupport_ResourceLib + query;
            dxTabDistProdInfoTabs.Tabs.Add(tab);



            //tabName = "tabPID_OtherInfo";
            //tab = new Tab(GetTabText(tabName), tabName);
            //tab.Enabled = true;
            //tab.VisibleIndex = tabIndex++;
            //tab.NavigateUrl = KeyDef.UrlList.MyDistributorInfoPages.ProductSupport_Others + query;
            //dxTabDistProdInfoTabs.Tabs.Add(tab);
        }
    }
}