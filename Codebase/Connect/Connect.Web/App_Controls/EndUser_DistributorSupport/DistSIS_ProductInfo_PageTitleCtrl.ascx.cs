﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistSIS_ProductInfo_PageTitleCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                PIDProductInfo sisProdInfo = GetProductInfo();
                if (sisProdInfo == null) throw new HttpException(404, "Page not found.");
                //String pageTitle = String.Format("{0} - {1}", sisProdInfo.ModelNumber, sisProdInfo.ProductName);
                String pageTitle = String.Format(" - {0}", sisProdInfo.ModelNumber);
                if (pageTitle.Length > 100)
                    pageTitle = pageTitle.Substring(0, 100) + "...";
                ltrPIDProductInfoPageTitle.Text = pageTitle;
            }
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}