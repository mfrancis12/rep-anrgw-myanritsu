﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistSIS_ResourceLibCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.DistSIS_ResourceLibCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistSIS_ResourceLib_PIDDownloadsCtrl.ascx" TagPrefix="uc1" TagName="DistSIS_ResourceLib_PIDDownloadsCtrl" %>

<div class="settingrow">
    <dx:ASPxPageControl ID="dxpcPIDCategories" runat="server" Width="100%" TabPosition="Top" Theme="AnritsuDevXTheme" RenderMode="Lightweight" TabAlign="Left">
        <TabPages>
            <dx:TabPage Text="<%$ Resources:common_pid,Marketing_Tools %>">
                <ContentCollection>
                    <dx:ContentControl ID="dxContentMarcom" runat="server">
                        <uc1:DistSIS_ResourceLib_PIDDownloadsCtrl runat="server" ID="DistSIS_ResourceLib_PIDDownloadsCtrl_Marcom" PID_DownloadCategoryCode="Marketing_Tools" />
                    </dx:ContentControl>                    
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:common_pid,Sales_Tools %>">
                <ContentCollection>
                    <dx:ContentControl ID="dxContentSales" runat="server">
                        <uc1:DistSIS_ResourceLib_PIDDownloadsCtrl runat="server" ID="DistSIS_ResourceLib_PIDDownloadsCtrl1" PID_DownloadCategoryCode="Sales_Tools" />
                    </dx:ContentControl>                    
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="<%$ Resources:common_pid,Support_Information %>">
                <ContentCollection>
                    <dx:ContentControl ID="dxContentSupport" runat="server">
                        <uc1:DistSIS_ResourceLib_PIDDownloadsCtrl runat="server" ID="DistSIS_ResourceLib_PIDDownloadsCtrl2" PID_DownloadCategoryCode="Support_Information" />
                    </dx:ContentControl>                    
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</div>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_DistSIS_ProductInfoCtrl" /></div>