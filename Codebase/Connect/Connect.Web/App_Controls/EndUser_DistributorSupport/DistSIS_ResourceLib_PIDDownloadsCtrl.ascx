﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistSIS_ResourceLib_PIDDownloadsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.DistSIS_ResourceLib_PIDDownloadsCtrl" %>
<div class="settingrow">
    <dx:ASPxGridView ID="gvPIDDoc" runat="server" ClientInstanceName="gvPIDDoc_Marcom" DataSourceID="odsPIDDoc" KeyFieldName="DocumentID" Width="100%" Theme="AnritsuDevXTheme"
        OnHtmlRowCreated="gvPIDDoc_HtmlRowCreated">
        <Columns>
            <dx:GridViewDataColumn FieldName="DocumentType" GroupIndex="0" VisibleIndex="1" Caption="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,DocumentType.Text %>" ToolTip="<%$ Resources:common,DragToGroup.Txt %>"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="DocumentName" VisibleIndex="2" Caption="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,DocumentName.Text %>" ToolTip="<%$ Resources:common,DragToGroup.Txt %>">
                <DataItemTemplate>
                    <div class="settingrow"><%# Eval("DocumentName") %></div>
                    <div class="settingrow" style="font-size:11px;">
                        <asp:Literal ID="ltrDownloadProperties" runat="server"></asp:Literal></div>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="Language" VisibleIndex="4" GroupIndex="1" ToolTip="<%$ Resources:common,DragToGroup.Txt %>"  Caption="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,LanguageID.Text %>" ></dx:GridViewDataColumn>
            <dx:GridViewDataDateColumn FieldName="ReleaseDate" ToolTip="<%$ Resources:common,DragToGroup.Txt %>" Caption="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,ReleaseDate.Text %>" VisibleIndex="5" Width="60px" CellStyle-CssClass="tablecell" CellStyle-HorizontalAlign="Center">
                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataColumn FieldName="FullDownloadURL" VisibleIndex="500" Caption=" " Width="100px" CellStyle-HorizontalAlign="Center" Settings-AllowAutoFilter="False" Settings-AllowGroup="False">
                <Settings AllowAutoFilter="False"></Settings>
                <DataItemTemplate>
                    <div class="settingrow">
                        <dx:ASPxHyperLink ID="dxhlDownload" runat="server" Text="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,dxhlDownload.Text %>" Target="_blank" NavigateUrl='<%# GeneratePIDDlUrl(Eval("FullDownloadURL")) %>'></dx:ASPxHyperLink>
                        <dx:ASPxHyperLink ID="hlDownloadYouTube" runat="server" rel='prettyPhoto' Target="_blank" NavigateUrl='<%# GeneratePIDDlUrl(Eval("FullDownloadURL")) %>' Visible="false" Text="<%$ Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,hlDownloadYouTube.Text %>"></dx:ASPxHyperLink>                        
                    </div>
                    <div class="settingrow" id="divFileSize" runat="server">
                        <span class='dlbl'>
                            <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl,ltrDownloadProperties.Text.FileSize %>'></asp:Localize>: <%# Eval("FileSize", "{0:#.00} MB") %></span>
                    </div>
                </DataItemTemplate>

                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataColumn>
        </Columns>
        <GroupSummary>
            <dx:ASPxSummaryItem FieldName="DocumentID" SummaryType="Count" DisplayFormat="{0}" />
        </GroupSummary>
        <Settings ShowGroupPanel="true" GroupFormat="{1} {2}" ShowFilterRow="true" ShowFilterRowMenu="true" />
        <SettingsBehavior AllowGroup="true" />
        <SettingsPager PageSize="100"></SettingsPager>
        <Styles GroupRow-Cursor="pointer">
            <GroupRow Cursor="pointer"></GroupRow>
        </Styles>
        <ClientSideEvents RowClick="function (s,e) { Grid_ToggleGroupRow(s, e.visibleIndex); }" />
    </dx:ASPxGridView>
    <asp:HiddenField ID="hddlPIDDoc" runat="server" />
    <asp:ObjectDataSource ID="odsPIDDoc" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS.ODS_SISDocuments" SelectMethod="SelectDocs">
        <SelectParameters>
            <asp:QueryStringParameter Name="pid" QueryStringField="pid" Type="Int32" />
            <asp:ControlParameter Name="catName" ControlID="hddlPIDDoc" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
<div class="settingrow">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl" />
</div>
