﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;
using System.IO;
using DevExpress.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistSIS_ResourceLib_PIDDownloadsCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        private String _PID_DownloadCategoryCode = String.Empty;
        public String PID_DownloadCategoryCode
        {
            get { return _PID_DownloadCategoryCode; }
            set { _PID_DownloadCategoryCode = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hddlPIDDoc.Value = _PID_DownloadCategoryCode;
                odsPIDDoc.CacheKeyDependency = this.ID + "CKD";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsCallback)
            {
                HandleODSCache();
            }
        }

        private void HandleODSCache()
        {
            String lang = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();

            String obdsCacheKey= String.Format("SISDocCache_{0}_{1}_{2}", this.ID, SelectedPID, lang);
            if (Cache[odsPIDDoc.CacheKeyDependency] == null) Cache[odsPIDDoc.CacheKeyDependency] = obdsCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsPIDDoc.CacheKeyDependency].ToString().Equals(obdsCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsPIDDoc.CacheKeyDependency);
                Cache[odsPIDDoc.CacheKeyDependency] = obdsCacheKey;
            }
        }

        protected void gvPIDDoc_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                ASPxGridView masterGrid = (ASPxGridView)sender;
                Literal ltr = masterGrid.FindRowCellTemplateControl(e.VisibleIndex
                    , masterGrid.Columns["DocumentName"] as GridViewDataColumn
                    , "ltrDownloadProperties") as Literal;


                StringBuilder sbText = new StringBuilder();
                //String fileName = ConvertUtility.ConvertNullToEmptyString(e.GetValue("FileName"));
                //if (!fileName.IsNullOrEmptyString())
                //{
                //    //if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                //    sbText.AppendFormat("<span class='dlbl'>{0}</span><span class='dltxt'>{1}</span>", 
                //        GetStaticResource("ltrDownloadProperties.Text.FileName"), fileName);
                //}

                String accessType = ConvertUtility.ConvertNullToEmptyString(e.GetValue("AccessType"));
                if (!accessType.IsNullOrEmptyString())
                {
                    if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.AppendFormat("<span class='dlbl'>{0}: </span><span class='dltxt' style='color:#f5a623 !important;'>{1}</span>",
                        GetStaticResource("ltrDownloadProperties.Text.AccessType"), accessType);
                }

                String version = ConvertUtility.ConvertNullToEmptyString(e.GetValue("Version"));
                if (!version.IsNullOrEmptyString())
                {
                    if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.AppendFormat("<span class='dlbl'>{0}: </span><span class='dltxt'>{1}</span>", GetStaticResource("ltrDownloadProperties.Text.Version"), version);
                }

                String fullDownloadUrl = ConvertUtility.ConvertNullToEmptyString(e.GetValue("FullDownloadURL"));
                if (fullDownloadUrl.Contains("youtube.com"))
                {
                    
                    ASPxHyperLink fullDownloadURLLink = masterGrid.FindRowCellTemplateControl(e.VisibleIndex, 
                        masterGrid.Columns["FullDownloadURL"] as GridViewDataColumn, "dxhlDownload") as ASPxHyperLink;
                    ASPxHyperLink dxhlDownloadYouTube = masterGrid.FindRowCellTemplateControl(e.VisibleIndex,
                        masterGrid.Columns["FullDownloadURL"] as GridViewDataColumn, "hlDownloadYouTube") as ASPxHyperLink;
                    HtmlGenericControl divFileSize = masterGrid.FindRowCellTemplateControl(e.VisibleIndex,
                        masterGrid.Columns["FullDownloadURL"] as GridViewDataColumn, "divFileSize") as HtmlGenericControl;
                    fullDownloadURLLink.Visible = divFileSize.Visible = false;
                    dxhlDownloadYouTube.Visible = true;

                }

                if (sbText.Length > 0) ltr.Text = sbText.ToString();
                else ltr.Text = "&nbsp;";
            }
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_DistSIS_ResourceLib_PIDDownloadsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

    }
}