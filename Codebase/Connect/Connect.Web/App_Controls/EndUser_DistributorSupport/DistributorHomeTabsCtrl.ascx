﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorHomeTabsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.DistributorHomeTabsCtrl" %>
<dx:ASPxTabControl ID="dxTabDistFavoriteProducts" runat="server" Theme="AnritsuDevXTheme" Width="100%" SyncSelectionMode="CurrentPath">
    <Tabs>
        <dx:Tab Name="tabPIDFavorites" Text="<%$ Resources:STCTRL_DistributorHomeTabsCtrl, tabPIDFavorites.Text %>"  NavigateUrl="~/mydistributorinfo/distproductfavorites"></dx:Tab>
        <dx:Tab Name="tabPIDProducts" Text="<%$ Resources:STCTRL_DistributorHomeTabsCtrl, tabPIDProducts.Text %>"  NavigateUrl="~/mydistributorinfo/home"></dx:Tab>
    </Tabs>
    <Paddings PaddingBottom="0" />
</dx:ASPxTabControl>