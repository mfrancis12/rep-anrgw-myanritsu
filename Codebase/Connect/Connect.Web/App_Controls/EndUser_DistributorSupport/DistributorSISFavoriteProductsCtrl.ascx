﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorSISFavoriteProductsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.DistributorSISFavoriteProductsCtrl" %>
<div class="settingrow">
    <script type="text/javascript">
        function dxgvSISProds_EndCallback(s, e) {
            if (s.cpShowDeleteConfirmBox)
                pcConfirm.Show();
        }

        function Yes_Click() {
            pcConfirm.Hide();
            dxgvSISProds.DeleteRow(dxgvSISProds.cpRowIndex);
        }

        function No_Click() {
            pcConfirm.Hide()
        }
    </script>

    <dx:ASPxGridView ID="dxgvSISProds" runat="server" ClientInstanceName="dxgvSISProds" AutoGenerateColumns="false" KeyFieldName="PID" DataSourceID="odsSISFavProds" Theme="AnritsuDevXTheme"
        OnCustomButtonCallback="dxgvSISProds_CustomButtonCallback" OnInit="dxgvSISProds_Init" OnRowDeleting="dxgvSISProds_RowDeleting" Width="98%">
        <%-- <ClientSideEvents CustomButtonClick="OnCustomButtonClick" />--%>
        <ClientSideEvents EndCallback="dxgvSISProds_EndCallback" />
        <Columns>
            <dx:GridViewDataColumn FieldName="ProductImageUrl" Caption=" " Width="50px" VisibleIndex="5">

                <DataItemTemplate>
                    <a id="hrefProductImage" rel="prettyPhoto" href='<%#GetImage(Eval("ProductImageUrl"))%>'>
                        <asp:Image ID="imgProdImage" runat="server" CssClass="imglist" Width="40px" ImageUrl='<%#GetImage(Eval("ProductImageUrl"))%>' AlternateText="" /></a>
                </DataItemTemplate>
                <Settings AllowAutoFilter="False" />
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="10" Settings-AllowSort="True" Width="150px" Caption="<% $Resources: STCTRL_SISProductListCtrl, dxgvSISProds.ModelNumber.Caption %>">
                <DataItemTemplate>
                    <a class="dxeHyperlink_AnritsuDevXTheme blueit-hover" href='<%# Eval("PID", "/mydistributorinfo/distproductsupport?pid={0}") %>'><%# Eval("ModelNumber") %></a>
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ProductName" VisibleIndex="11" Settings-AllowSort="True" Width="450px" Caption="<% $Resources: STCTRL_SISProductListCtrl, dxgvSISProds.PID.Caption %>">
                <DataItemTemplate>
                    <a class="dxeHyperlink_AnritsuDevXTheme blueit-hover" href='<%# Eval("PID", "/mydistributorinfo/distproductsupport?pid={0}") %>'><%# Eval("ProductName") %></a>
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataColumn>
            <dx:GridViewCommandColumn VisibleIndex="500" ButtonType="Image" Width="30px" Caption=" ">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="gvccbttRemoveFromFavorite" Text="remove">
                        <Image ToolTip="<% $Resources: common, removefav %>" Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/RemoveFavorite.png"  Width="20px" Height="20px" />
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>

        </Columns>
        <SettingsPager PageSize="10"></SettingsPager>
        <SettingsBehavior AllowGroup="false" AllowSort="false" />
        <Settings ShowGroupPanel="false" ShowFilterRow="true" ShowFilterRowMenu="true" />
    </dx:ASPxGridView>
    <dx:ASPxPopupControl ID="pcConfirm" runat="server" ClientInstanceName="pcConfirm" CloseAction="CloseButton"
        Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        HeaderText="<% $Resources: common, ConfirmBoxHeaderText %>">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                <table width="100%">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Localize ID="lcalDeleteRow" runat="server" Text="<% $Resources: common, ConfirmDeleteRegMsg %>"></asp:Localize>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <a href="javascript:Yes_Click()">
                                <asp:Localize ID="lcalYesMsg" runat="server" Text="<% $Resources: common, YesConfirmMsg %>"></asp:Localize>
                            </a>
                        </td>
                        <td align="center">
                            <a href="javascript:No_Click()">
                                <asp:Localize ID="lcalNoMsg" runat="server" Text="<% $Resources: common, NoConfirmMsg %>"></asp:Localize>
                            </a>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</div>
<div class="settingrow">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_DistributorSISFavoriteProductsCtrl" />
</div>
<asp:ObjectDataSource ID="odsSISFavProds" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS.ODS_SISProducts"
    SelectMethod="SelectSISFavoriteProducts" EnableCaching="false"></asp:ObjectDataSource>
