﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class DistributorSISFavoriteProductsCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsCallback)
            {
                
            }
        }

        protected void dxgvSISProds_Init(object sender, EventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            gridView.JSProperties["cpShowDeleteConfirmBox"] = false;
        }

        protected void dxgvSISProds_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            try
            {
                Int32 pid = ConvertUtility.ConvertToInt32(e.Keys[dxgvSISProds.KeyFieldName], 0);

                Guid accountID = AccountUtility.GetSelectedAccountID(true);
                Lib.PID.PIDFavoriteProductBLL.Delete(accountID, pid);
                try { Cache.Remove("odsSISFavProdsCKD"); }
                catch { }
                dxgvSISProds.DataBind();
                e.Cancel = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        protected void dxgvSISProds_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridView = sender as ASPxGridView;
            if (e.ButtonID == "gvccbttRemoveFromFavorite")
            {
                Int32 pid = ConvertUtility.ConvertToInt32(dxgvSISProds.GetRowValues(e.VisibleIndex, "PID"), 0);
                gridView.JSProperties["cpRowIndex"] = e.VisibleIndex;
                gridView.JSProperties["cpShowDeleteConfirmBox"] = true;
                              
            }
        }

        public String GetImage(object img)
        {
            if (String.IsNullOrEmpty(img.ToString())) return ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine");
            else
                return img.ToString();

        }
    }
}