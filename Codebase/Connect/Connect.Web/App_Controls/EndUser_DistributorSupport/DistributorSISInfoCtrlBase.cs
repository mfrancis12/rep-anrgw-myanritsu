﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public class DistributorSISInfoCtrlBase : System.Web.UI.UserControl
    {
        public Int32 SelectedPID { get { return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.SIS_ProductID], 0); } }

        public PIDProductInfo GetProductInfo()
        {
            ODS_SISProducts ods = new ODS_SISProducts();
            PIDProductInfo sisProdInfo = ods.SelectByPID(SelectedPID);
            return sisProdInfo;
        }

        public PIDProductInfoDetails GetProductDetails()
        {
            return PIDProductInfoDetailsBLL.SelectDetails(SelectedPID);
        }

        public Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        protected String GeneratePIDDlUrl(object FullDownloadURL)
        {
            var filepath = ConvertUtility.ConvertToString(FullDownloadURL, String.Empty);
            if (filepath.StartsWith("/"))
            {
                filepath = filepath.Substring(1).Replace(@"\", "/");
                String awsPreFix = Lib.AWS.AWSUtility_Settings.PID_Prefix_Get();                
                if (!awsPreFix.IsNullOrEmptyString()) filepath = awsPreFix + filepath;
                return string.Format("{0}?{1}={2}", KeyDef.UrlList.PIDDocumentHandler,
                    KeyDef.QSKeys.TargetURL, HttpUtility.UrlEncode(filepath));
            }
            else { return filepath; }
        }
    }
}