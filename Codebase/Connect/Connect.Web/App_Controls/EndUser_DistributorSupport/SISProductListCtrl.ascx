﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SISProductListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport.SISProductListCtrl" %>
<div class="settingrow"  id="DevXTbl">
    <dx:ASPxGridView ID="dxgvSISProds" runat="server" ClientInstanceName="dxgvSISProds" AutoGenerateColumns="false" KeyFieldName="PID" DataSourceID="odsSISProds" Theme="AnritsuDevXTheme"
        OnCustomButtonCallback="dxgvSISProds_CustomButtonCallback" Width="98%" >
        <Columns>
            <dx:GridViewDataColumn FieldName="ProductImageUrl" Caption=" " Width="50px" VisibleIndex="5">
                <DataItemTemplate>
                    <a id="hrefProductImage" rel="prettyPhoto" href='<%#GetImage(Eval("ProductImageUrl"))%>'>
                        <asp:Image ID="imgProdImage" runat="server" CssClass="imglist" Width="40px" ImageUrl='<%#GetImage(Eval("ProductImageUrl"))%>' AlternateText="" /></a>
                </DataItemTemplate>
                <Settings AllowAutoFilter="False" />
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="10" Width="150px" Settings-AllowSort="True" Caption="<% $Resources: STCTRL_SISProductListCtrl, dxgvSISProds.ModelNumber.Caption %>">
                <DataItemTemplate>
                    <a class="dxeHyperlink_AnritsuDevXTheme blueit-hover" href='<%# Eval("PID", "/mydistributorinfo/distproductsupport?pid={0}") %>'><%# Eval("ModelNumber") %></a>
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="ProductName" VisibleIndex="11" Width="450px" Settings-AllowSort="True" Caption="<% $Resources: STCTRL_SISProductListCtrl, dxgvSISProds.PID.Caption %>">
                <DataItemTemplate>
                    <a class="dxeHyperlink_AnritsuDevXTheme blueit-hover" href='<%# Eval("PID", "/mydistributorinfo/distproductsupport?pid={0}") %>'><%# Eval("ProductName") %></a>
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataColumn>
            <dx:GridViewCommandColumn VisibleIndex="500" ButtonType="Image" Width="30px" Caption=" ">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="gvccbttAddToFavorite">
                        <Image ToolTip="<% $Resources: common, addfav %>" Url="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/AddFavorite.png"   Width="20px" Height="20px" />

                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>

            </dx:GridViewCommandColumn>
        </Columns>
        <SettingsPager PageSize="15"></SettingsPager>
        <SettingsBehavior AllowGroup="false" AllowSort="false" />
        <Settings ShowGroupPanel="false" ShowFilterRow="true" ShowFilterRowMenu="true" />
    </dx:ASPxGridView>
</div>
<div class="settingrow">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_SISProductListCtrl" />
</div>
<asp:ObjectDataSource ID="odsSISProds" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS.ODS_SISProducts"
    SelectMethod="SelectAll" EnableCaching="false"></asp:ObjectDataSource>
