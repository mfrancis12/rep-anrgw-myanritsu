﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Utils;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.EndUser_DistributorSupport
{
    public partial class SISProductListCtrl : DistributorSISInfoCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsCallback)
            {
         
            }
        }

        public string StaticResourceClassKey
        {
            get { return srep.StaticResourceClassKey; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        //protected void dxgvSISProds_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        //{
        //   OnCustomButtonInitialize="dxgvSISProds_CustomButtonInitialize" 
        //     ODS_SISProducts ods = new ODS_SISProducts();
        //    // Int32 pid = ConvertUtility.ConvertToInt32(dxgvSISProds.GetRowValues(e.VisibleIndex, "PID"), 0);
        //     List<PIDProductInfo> AllSISProducts = ods.SelectAll();
         
        //     List<PIDProductInfo> SISFavoriteProducts = ods.SelectSISFavoriteProducts();

        //     foreach (PIDProductInfo ppi in SISFavoriteProducts)
        //     {
        //         foreach (PIDProductInfo allsis in AllSISProducts)
        //         {
        //             if (e.ButtonID == "gvccbttAddToFavorite" && ppi.PID.Equals(allsis.PID))
        //             {
        //                 e.Enabled = false;
        //                 AllSISProducts.Remove(allsis);
        //                 break;
        //             }
        //         }
        //     }
             
        //}

        protected void dxgvSISProds_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            if (e.ButtonID == "gvccbttAddToFavorite")
            {
                Int32 pid = ConvertUtility.ConvertToInt32(dxgvSISProds.GetRowValues(e.VisibleIndex, "PID"), 0);
                
                Guid accountID = AccountUtility.GetSelectedAccountID(true);
                Lib.PID.PIDFavoriteProductBLL.Insert(accountID, pid);
                             
                try { Cache.Remove("odsSISFavProdsCKD"); }
                catch { }
            }
            
        }


        
        public String GetImage(object img)
        {
            if (String.IsNullOrEmpty(img.ToString())) return ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine");
            else
            return img.ToString();

        }


    }
}