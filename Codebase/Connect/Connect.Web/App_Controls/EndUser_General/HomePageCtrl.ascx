﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomePageCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_General.HomePageCtrl" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib.Utils" %>
<div class="full-container">
   <div class="myanritsu-hero-banner"><span>My Anritsu</span></div>
</div>

<div class="myanritsu-section-top">
<div class="myanritsu-section-wrap">
<h2><%= GetGlobalResourceObject("~/home", "Section1Title").ToString()%></h2>
<p><%= GetGlobalResourceObject("~/home", "Section1Content").ToString()%></p>
</div>
</div>
<div class="myanritsu-section">
 <div class="myanritsu-section-twoColumn">
   <div class="myanritsu-section-column">
     <div class="myanritsu-section-column-content">
       <h3><%= GetGlobalResourceObject("~/home", "Section2Title").ToString()%></h3>
       <p><%= GetGlobalResourceObject("~/home", "Section2Content").ToString()%></p>
       <p><a href="/myproduct/regproduct-select"><%= GetGlobalResourceObject("~/myproduct/regproduct", "ButtonText").ToString()%></a></p>
     </div>
   </div>
   <div class="myanritsu-section-column">
     <div class="myanritsu-section-column-content">
       <div class="myanritsu-register-my-product-img"></div>
     </div>
   </div>
 </div>
</div>
<div class="myanritsu-section">
 <div class="myanritsu-section-twoColumn">
   <div class="myanritsu-section-column">
     <div class="myanritsu-section-column-content">
       <div class="myanritsu-support-img"></div>
     </div>
   </div>
   <div class="myanritsu-section-column">
     <div class="myanritsu-section-column-content">
       <h3><%= GetGlobalResourceObject("~/home", "Section3Title").ToString()%></h3>
       <p><%= GetGlobalResourceObject("~/home", "Section3Content").ToString()%></p>
<p><a href="/myproduct/support"><%= GetGlobalResourceObject("~/myproduct/support", "ButtonText").ToString()%></a></p>
     </div>
   </div>
 </div>
</div>
