﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeSupportResourcesCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_General.HomeSupportResourcesCtrl" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib.Utils" %>

<div class="full-container">
  <div class="support-hero-banner"><span><%= GetGlobalResourceObject("~/myproduct/support", "PageTitle").ToString().ToUpper()%></span></div>
</div>

<%--<div class="container page-height" style="padding: 0 20px">
  <div class="rightside">
    <div class="settingrow">
      <div class="container marginTop">
        <div class="myanritsu-menu">
          <ul>
            <span id="cphContentTop_HomeSupportResourcesCtrl1_dlAddOnLinks" style="display:inline-block;width:100%;">
              <span>
                <li>
                  <a href="/myproduct/home">
                    <div>
                      <div class="myanritsu-icon my-product"></div>
                      <p>My Products</p>
                    </div>
                  </a>
                </li>
              </span>
              <span>
                <li>
                  <a href="/myproduct/regproduct-select">
                    <div>
                      <div class="myanritsu-icon register-your-product"></div>
                      <p>Register Your Product</p>
                    </div>
                  </a>
                </li>
              </span>
              <span>
                <li>
                  <a href="/myaccount/selectcompany">
                    <div>
                      <div class="myanritsu-icon manage-teams"></div>
                      <p>Manage Teams</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="/contactus/web-master">
                    <div>
                      <div class="myanritsu-icon talk-to-anritsu"></div>
                      <p>Talk to Anritsu</p>
                    </div>
                  </a>
                </li>
              </span>
              <span>
                <li>
                  <a href="http://www.anritsu.com/ko-KR/Services-Support/Talk-To-Anritsu/index.aspx">
                    <div>
                      <div class="myanritsu-icon talk-to-anritsu"></div>
                      <p>Talk to Anritsu</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-activity"></div>
                      <p>Activity</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-elearning"></div>
                      <p>E-learning</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-gear"></div>
                      <p>Gear</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-repair"></div>
                      <p>Repair</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-service"></div>
                      <p>Service</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-support"></div>
                      <p>Support</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-technical-support"></div>
                      <p>Technical Support</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-training"></div>
                      <p>Training</p>
                    </div>
                  </a>
                </li>
              </span><br>
              <span>
                <li>
                  <a href="#">
                    <div>
                      <div class="myanritsu-icon myanritsu-warranty-status"></div>
                      <p>Warranty Status</p>
                    </div>
                  </a>
                </li>
              </span><br>
            </span>
          </ul>
        </div>
      </div>
    </div>
    <div class="settingrow">
    </div>
  </div>
</div>
<div>--%>
<div class="container page-height" style="padding: 0 20px">
    <div class="rightside">
    <div class="settingrow">
      <div class="container marginTop">
    <div class="myanritsu-menu">
        <ul> 
        <span id="cphContentTop_HomeSupportResourcesCtrl1_dlAddOnLinks" style="display:inline-block;width:100%;">
         
         <asp:DataList ID="dlAddOnLinks" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                RepeatLayout="Flow"  HorizontalAlign="left" Width="100%">
           <%-- <HeaderTemplate>
                <ul>
            </HeaderTemplate>--%>
            <ItemTemplate>
                 <span>
                <li>
                    <a href='<%#SetRepCalLink(Convert.ToString(Eval("TargetUrl"))) %>'>
                    <div>
                         <div class="myanritsu-icon <%# DataBinder.Eval(Container.DataItem, "LnkWrapperCssClass") %>"></div>
                      <%--  <img src='<%# DataBinder.Eval(Container.DataItem, "ImageUrl") %>' alt="My Products" width="125" height="125">--%>
                        <p><%# GetLinkText( DataBinder.Eval(Container.DataItem, "LnkID"), "LinkTitleText") %></p>
                    </div>
                </a>
                    </li>  

                 </span></ItemTemplate>
           <%-- <FooterTemplate>
                </ul></FooterTemplate>--%>
        </asp:DataList>
  </span>
       </ul>
        </div>
      </div>
    </div>
    <div class="settingrow">
    </div>
  </div>
</div>