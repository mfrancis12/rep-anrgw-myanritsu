﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_General
{
    public partial class HomeSupportResourcesCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
 

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                BindAddOnLinks();
            }
        }
       
        private void BindAddOnLinks()
        {
            if (!Page.User.Identity.IsAuthenticated)
            {
                return;
            }
            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            //Boolean hasDistributorContent = (acc.Config_DistributorPortal != null && acc.Config_DistributorPortal.StatusCode.Equals("active", StringComparison.InvariantCultureIgnoreCase));
            //divDistInfoIcon.Visible = hasDistributorContent;
            //divManageOrg.Visible = !hasDistributorContent;


            App_Lib.AppCacheFactories.LinksHomeCacheFactory dataCache = new App_Lib.AppCacheFactories.LinksHomeCacheFactory();
            DataTable tb = dataCache.GetLinks(acc.GWCultureGroupByCountryCode, acc.AccountID);
           
            foreach (DataRow dr in tb.Rows)
            {
                if (dr["LnkWrapperCssClass"] != null)
                {
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-wtystatus")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-warranty-status";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-training")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-training";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-techsupport")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-technical-support";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-support")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-support";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-service")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-service";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-repair")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-repair";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-gear")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-gear";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-elearn")
                    {
                        dr["LnkWrapperCssClass"] = "talk-to-anritsu";//"myanritsu-elearning";
                    }
                    if (dr["LnkWrapperCssClass"].ToString() == "mitm-activity")
                    {
                        dr["LnkWrapperCssClass"] = "myanritsu-activity";
                    }
                  
                }
                else
                {
                    dr["LnkWrapperCssClass"] = "myanritsu-warranty-status";
                }

            }
            DataView dv = null;
            if (tb != null)
            {
                dv = tb.DefaultView;
                dv.Sort = "LoadOrder ASC";
            }
            dlAddOnLinks.DataSource = dv;
            dlAddOnLinks.DataBind();
        }

        protected string GetLinkText(Object linkID, Object resKey)
        {
            String classKey = String.Format("STLNK_{0}", linkID.ToString());
            return this.GetGlobalResourceObject(classKey, resKey.ToString()).ToString();
        }

        public string SetRepCalLink(string link)
        {
            if (string.IsNullOrEmpty(link)) return string.Empty;
            return link.Replace("[[EmailAddress]]", LoginUtility.GetCurrentUserOrSignIn().Email);
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_HomeSupportResourcesCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}