﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftMainNavCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_General.LeftMainNavCtrl" %>
<anrui:GlobalWebBoxedPanel ID="gwpnlLeftMainNav" runat="server" HeaderText=" " ShowHeader="false">
<div class="gwsbrmenu"><ul>
<li><asp:HyperLink ID="hlMyAnritsuHome" runat="server" ToolTip="<%$ Resources:STCTRL_LeftMainNavCtrl,hlMyAnritsuHome.ToolTip %>" NavigateUrl="/home" Text="<%$ Resources:STCTRL_LeftMainNavCtrl,hlMyAnritsuHome.Text %>"></asp:HyperLink></li>
<li><asp:HyperLink ID="hlMyProducts" runat="server" ToolTip="<%$ Resources:STCTRL_LeftMainNavCtrl,hlMyProducts.ToolTip %>" NavigateUrl="~/myproduct/home" Text="<%$ Resources:STCTRL_LeftMainNavCtrl,hlMyProducts.Text %>"></asp:HyperLink></li>
<li><asp:HyperLink ID="hlRegisterProduct" runat="server" ToolTip="<%$ Resources:STCTRL_LeftMainNavCtrl,hlRegisterProduct.ToolTip %>" NavigateUrl="~/myproduct/regproduct-select" Text="<%$ Resources:STCTRL_LeftMainNavCtrl,hlRegisterProduct.Text %>"></asp:HyperLink></li>
<li><asp:HyperLink ID="hlManageAccount" runat="server" ToolTip="<%$ Resources:STCTRL_LeftMainNavCtrl,hlManageAccount.ToolTip %>" NavigateUrl="~/myaccount/companyprofile" Text="<%$ Resources:STCTRL_LeftMainNavCtrl,hlManageAccount.Text %>"></asp:HyperLink></li>
<li id="liDistInfo" runat="server" visible="false"><asp:HyperLink ID="hlDistInfo" runat="server" ToolTip="<%$ Resources:STCTRL_LeftMainNavCtrl,hlDistInfo.ToolTip %>" NavigateUrl="/mydistributorinfo/home" Text="<%$ Resources:STCTRL_LeftMainNavCtrl,hlDistInfo.Text %>"></asp:HyperLink></li>
</ul></div>
<div><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_LeftMainNavCtrl" EnableViewState="false" /></div>
</anrui:GlobalWebBoxedPanel>