﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_General
{
    public partial class LeftMainNavCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (Page.User.Identity.IsAuthenticated)
                {
                    Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
                    if (acc != null)
                    {
                        Boolean hasDistributorContent = (acc.Config_DistributorPortal != null && acc.Config_DistributorPortal.StatusCode.Equals("active", StringComparison.InvariantCultureIgnoreCase));
                        liDistInfo.Visible = hasDistributorContent;
                    }
                }
            }
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}