﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebMasterContactCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_General.WebMasterContactCtrl" %>
<asp:Panel ID="pnlWebMasterContact" runat="server" DefaultButton="bttSubmit">
    <div class="settingrow" style="padding-bottom: 10px;">
        <p>
            <asp:Localize ID="lcalContactUsIntro" runat="server" Text="<%$Resources:STCTRL_WebMasterContactCtrl,lcalContactUsIntro.Text%>"></asp:Localize>
                    </p>
    </div>
    <div class="settingrow group">
        <label>
            <asp:Localize ID="lcalComments" runat="server" Text="<%$Resources:STCTRL_WebMasterContactCtrl,lcalComments.Text%>"></asp:Localize>*</label>
        <div class="settingrow">
            <asp:TextBox runat="server" ID="txtComments" TextMode="MultiLine" MaxLength="2000" Height="150px" Width="600px" Style="font: 11px arial"></asp:TextBox>
        </div>
        <div class="settingrow">
            <asp:RequiredFieldValidator ID="rvComments" runat="server" ControlToValidate="txtComments" ErrorMessage="<%$Resources:STCTRL_WebMasterContactCtrl,vstxtComments.Text%>" SetFocusOnError="true" ValidationGroup="vgSubmit" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regExpComments" ControlToValidate="txtComments" ValidationGroup="vgSubmit"
                runat="server" ErrorMessage="<%$ Resources:STCTRL_WebMasterContactCtrl,ErrMsgCharactersLimit %>"
                ValidationExpression="^[\s\S]{0,2000}$" Display="Dynamic" />
        </div>
    </div>

    <div class="settingrow">
        <asp:Button ID="bttSubmit" runat="server" Text="<%$Resources:STCTRL_WebMasterContactCtrl,bttSubmit.Text%>" ValidationGroup="vgSubmit" OnClick="bttSubmit_Click" />
    </div>

    <div class="settingrow" style="text-align: left">
        <span class="msg">
            <asp:Literal ID="ltrMasterMsg" runat="server"></asp:Literal>
        </span>
    </div>
    <asp:HiddenField ID="hddnUrl" runat="server" />
</asp:Panel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_WebMasterContactCtrl" />
