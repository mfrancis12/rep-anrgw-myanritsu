﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Web.UI.WebControls;


namespace Anritsu.Connect.Web.App_Controls.EndUser_General
{
    public partial class WebMasterContactCtrl : System.Web.UI.UserControl
    {

        public String URL{ get;set;}
              
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
         
            try
            {
                App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                String userIP = WebUtility.GetUserIP();
                URL = hddnUrl.Value;
                Int32 cultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
                Lib.ContactUs.GW_Contact_Webmaster.SubmitWebmasterContactDataToDatabase(user.GWUserId, URL, userIP, cultureGroupID, txtComments.Text.Trim());
            }
            catch
            {
               // Response.Write(ex.ToString());
            }
            SubmitRequest();
        }

        /// <summary>
        /// Send out emails to webmaster
        /// </summary>
        private void SubmitRequest()
        {
            if (!Page.IsValid) return;
            String comments = HttpUtility.HtmlEncode(txtComments.Text);
            URL = hddnUrl.Value;
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);

            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            String userIP = WebUtility.GetUserIP();
            String geoCountryCode = String.Empty;
            AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi != null) geoCountryCode = gi.CountryCode;

            Boolean RequestSentToAdmin = false;
            Boolean emailSent = false;
            String msg = String.Empty;
            RequestSentToAdmin = Lib.ContactUs.GW_Contact_Webmaster.SubmitRequest(acc, user, userIP, geoCountryCode, URL, comments, out emailSent, out msg);

            if (!emailSent) throw new ArgumentException(GetGlobalResourceObject("common", "EmailNotSentMsg").ToString() + msg);
            if (!RequestSentToAdmin) throw new ArgumentException( GetGlobalResourceObject("common", " ErrorString").ToString() + msg);
            else
                WebUtility.HttpRedirect(null, KeyDef.UrlList.ContactUsSubmit);
               // ltrMasterMsg.Text = GetGlobalResourceObject("common", "RequestSubmittedMsg").ToString();

        }

        public void LoadData()
        {
            hddnUrl.Value = URL;
         }
    }
}