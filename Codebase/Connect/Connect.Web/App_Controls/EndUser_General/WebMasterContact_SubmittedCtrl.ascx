﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebMasterContact_SubmittedCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_General.WebMasterContact_SubmittedCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_WebMasterContact_SubmittedCtrl,pnlContainer.HeaderText %>">
   <asp:Panel ID="pnlSubmitOk" runat="server" HorizontalAlign="Center">
        <div class="settingrow">
            <asp:Localize ID="lcalThankYou" runat="server" Text="<%$Resources:common,RequestSubmittedMsg%>"></asp:Localize>

        </div>
        <%--<div class="settingrow" style="padding-top: 20px;">
            <ul class="ulhor1">
                <li style="padding: 15px;">
                    <asp:HyperLink ID="hlMore" runat="server" Text="<%$Resources:STCTRL_WebMasterContact_SubmittedCtrl,hlMore.Text%>" NavigateUrl="~/contactus/web-master" SkinID="SmallButton"></asp:HyperLink></li>
               
            </ul>
        </div>--%>
    </asp:Panel>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_WebMasterContact_SubmittedCtrl" />

