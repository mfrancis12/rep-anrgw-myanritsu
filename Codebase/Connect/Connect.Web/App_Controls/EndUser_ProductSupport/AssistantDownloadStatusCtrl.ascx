﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssistantDownloadStatusCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.AssistantDownloadStatusCtrl" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib.Utils" %>

<div class="full-container">
  <div class="support-hero-banner"><span><%= GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "PageTitle").ToString().ToUpper()%></span></div>
</div>

<div class="myanritsu-section-top">
<div class="myanritsu-section-wrap assist-download-status">
<h2><asp:Literal id="sectionTitle" runat="server"></asp:Literal>    </h2>
<p><%= GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "Section1Content").ToString()%></p>
</div>
</div>
<div> <asp:Label ID="statusmessage" style="font-size:16px"  runat="server"></asp:Label></div>


    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="assistant-status">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          
            <ContentTemplate>
                <asp:GridView  SortedAscendingHeaderStyle-CssClass="bs-caret"   SortedDescendingHeaderStyle-CssClass="caret"      runat="server" ID="datagridview" CssClass="mydatagrid" AllowPaging="True" AllowSorting="True" 
                    CellPadding="4" GridLines="Horizontal"
                   Font-Size="11pt" OnPageIndexChanging="datagridview_PageIndexChanging"  OnSorting="datagridview_Sorting" OnRowDataBound="datagridview_RowDataBound" 
                    ShowHeaderWhenEmpty="True" ViewStateMode="Enabled" AutoGenerateColumns="False">
              
                    <Columns>
                    <%--Setting fixed width to columns--%>
                 
                    <asp:BoundField  ItemStyle-Width="600px"  DataField="FilePath" SortExpression="FilePath" HeaderStyle-Width="600px" />
                    <asp:BoundField ItemStyle-Width="300px"   DataField="Status"  SortExpression="Status" HeaderStyle-Width="300px" />
                    <asp:BoundField ItemStyle-Width="300px"  DataField="DateTime" SortExpression="DateTime"  HeaderStyle-Width="300px" />
                  
                    <%--Add your other columns like this--%>
                </Columns>
                  <PagerStyle  HorizontalAlign="Center" />
                   
                   
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>