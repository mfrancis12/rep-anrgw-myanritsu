﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using Anritsu.Connect.Lib.AWS;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Drawing;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{

    public partial class AssistantDownloadStatusCtrl : DownloadResourceCtrlBase
    {
        bool EMEARegion = false;
       
       
        private DataTable fillDataGrid()
        {
            
            string filename = "";
            DataTable dtautodownload = CreateAutoDownloadStatusDataGridDataTable();
            if (Session["ModelNumber"] != null && Session["SerialNumber"] != null)
            {
                filename = Session["ModelNumber"].ToString() + "-" + Session["SerialNumber"].ToString().Substring(Session["SerialNumber"].ToString().Length - 4, 4) + ".csv";
                sectionTitle.Text = GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "Section1Title").ToString().Replace("{MN}", Session["ModelNumber"].ToString()).Replace("{SN}", Session["SerialNumber"].ToString()).ToString();
            }
            else
            {
                Response.Redirect("ProductSupport-Private.aspx");
            }
           
            string downloadstatusfilefolder = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.EU.AssistantFileStatusPath");// "DL3/DownloadStatus/";
           
            string filepath = downloadstatusfilefolder + filename.ToLower();
            string encodedurl = Uri.EscapeUriString(filepath);
            string url = AwsS3CdnUtility.GenerateTauCdnUrl(ConvertUtility.ConvertToString(encodedurl, string.Empty), 1);
           
            System.Net.WebClient client = new System.Net.WebClient();

            Stream stream = null;

            try
            {
                stream = client.OpenRead(url);


                StreamReader reader = new StreamReader(stream);
                //String content = reader.ReadToEnd();
                DataTable dt = ConvertCSVtoDataTable(reader);
              
                EMEARegion = IsEMEARegion();
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drow = dtautodownload.NewRow();
                    // drow["EmailAddress"] = dr["EmailAddress"].ToString();
                    // drow["SerialNumber"] = dr["SerialNumber"].ToString(); 
                    // drow["ModelNumber"] = dr["ModelNumber"].ToString(); 
                    drow["FilePath"] = RemoveFilePathQueryString(dr["FilePath"].ToString());
                    if (dr["Status"].ToString().ToLower() != "downloaded")
                    {
                        drow["Status"] = "Not Downloaded";
                    }
                    else
                    {
                        drow["Status"] = dr["Status"].ToString();
                    }
                    drow["DateTime"] = ChangeDateTimeBasedonUserCountry(dr["DateTime"].ToString());
                    dtautodownload.Rows.Add(drow);

                }
                

                
            }
            catch (Exception ex)
            {

                if (stream == null)
                {
                    statusmessage.Text = GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "DownloadPendingStatusMsg").ToString();// "Process not started yet";
                    return null;
                }
                else
                {
                    statusmessage.Text = ex.Message;
                }
            }
            return dtautodownload;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                datagridview.ShowHeader = true;
                datagridview.Columns[0].HeaderText = GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "Filename").ToString();
                datagridview.Columns[1].HeaderText = GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "Status").ToString();
                datagridview.Columns[2].HeaderText = GetGlobalResourceObject("~/myproduct/AssistantDownloadStatus", "DateTime").ToString();
                //datagridview.Columns[0].HeaderStyle.= "https://dl-dev.cdn-anritsu.com/appfiles/img/icons/down-arrow.png";

                //ViewState["SortDirection"] = "Asc";
                //ViewState["SortExpression"] = "FilePath";
                DataTable dt = fillDataGrid();
                //dt.DefaultView.Sort = ViewState["SortExpression"] + " " + ViewState["SortDirection"];
                datagridview.DataSource = dt;
                datagridview.DataBind();

                ViewState["dirState"] = dt;
               
               
            }
        }

        private DataTable CreateAutoDownloadStatusCSVDataTable()
        {
            DataTable dt = new DataTable();
           dt.Columns.Add("EmailAddress");
            dt.Columns.Add("SerialNumber");
            dt.Columns.Add("ModelNumber");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("Status");
            dt.Columns.Add("DateTime");

            return dt;
        }
        private DataTable CreateAutoDownloadStatusDataGridDataTable()
        {
            DataTable dt = new DataTable();
            //dt.Columns.Add("EmailAddress");
            //dt.Columns.Add("SerialNumber");
            //dt.Columns.Add("ModelNumber");
            dt.Columns.Add("FilePath");
            dt.Columns.Add("Status");
            dt.Columns.Add("DateTime");

            return dt;
        }
        private string RemoveFilePathQueryString(string FilePath)
        {
            int pos = FilePath.IndexOf("?");
            if (pos >= 0)
            {

                FilePath = FilePath.Remove(pos);
                FilePath = Uri.UnescapeDataString(FilePath);

            }

            pos= FilePath.LastIndexOf("/");
            if (pos >= 0)
            {

                FilePath = FilePath.Substring(pos+1);
                FilePath = Uri.UnescapeDataString(FilePath);

            }
            return FilePath;
        }

        private bool IsEMEARegion()
        {
            var user = LoginUtility.GetCurrentUserOrSignIn();
            bool Emearegion = false;

            if (AnrCommon.GeoLib.BLL.BLLGeoInfo.GetRegionByCountryCode(user.UserAddressCountryCode) == "EMEA")
            {
                Emearegion = true;
            }

            return Emearegion;
        }
        private string ChangeDateTimeBasedonUserCountry(string datetimevalue)
        {
            if (string.IsNullOrEmpty(datetimevalue) == false)
            {
                if (EMEARegion)
                {
                    datetimevalue = String.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Parse(datetimevalue));
                }
            }
            return datetimevalue;
        }
        public  DataTable ConvertCSVtoDataTable(StreamReader sr)
        {
            DataTable dt = CreateAutoDownloadStatusCSVDataTable();

         
            while (!sr.EndOfStream)
            {
                string[] rows = sr.ReadLine().Split(',');
                DataRow dr = dt.NewRow();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }




            return dt;
        }

        protected void downloadstatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex >= 0)
            {
                if (e.Row.Cells[1].Text.ToLower() == "downloaded")
                {
                    e.Row.Cells[1].ForeColor = Color.Green;
                }
           
            }
        }

      
        protected void datagridview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           

            datagridview.PageIndex = e.NewPageIndex;
            if (ViewState["dirState"]!=null && ViewState["SortExpression"] != null && ViewState["SortDirection"]!=null)
            {
                DataTable dt = (DataTable)ViewState["dirState"];
                dt.DefaultView.Sort = ViewState["SortExpression"] + " " + ViewState["SortDirection"];
                datagridview.DataSource = dt;
                datagridview.DataBind();
            }
            else
            {
                DataTable dtrslt = fillDataGrid();
                DataView sortedView = new DataView(dtrslt);
                datagridview.DataSource = sortedView;
                datagridview.DataBind();
            }
        }

        protected void datagridview_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = (DataTable)ViewState["dirState"];
            dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            datagridview.DataSource = dt;
            datagridview.DataBind();


        }
        private string GetSortDirection(string column)
        {
            string sortDirection = "DESC";
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "DESC"))
                    {
                        sortDirection = "ASC";
                    }
                }
            }

            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void datagridview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        LinkButton lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null )
                        {
                            if (ViewState["SortDirection"] != null)
                            {
                                lnk.CssClass = ViewState["SortDirection"].ToString() == "ASC" ? "ascending" : "descending";
                            }
                            else
                            {
                                lnk.CssClass = "ascending";
                            }
                        

                        }
                    }
                }
            }

            if (e.Row.RowIndex >= 0)
            {
                if (e.Row.Cells[1].Text.ToLower() == "downloaded")
                {
                    e.Row.Cells[1].ForeColor = Color.Green;
                    e.Row.Cells[1].Text = "Downloaded";
                }

            }
        }
    }
}