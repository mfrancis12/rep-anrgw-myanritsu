﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CalCertInfoCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.CalCertInfoCtrl" %>
<div style="width: 100%; text-align: center; padding: 10px;">
<dx:ASPxGridView ID="dxgvCalCert" ClientInstanceName="dxgvCalCert" runat="server" Width="740px" Theme="AnritsuDevXTheme">
    <Columns>
        <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$ Resources:STCTRL_CalCertInfoCtrl,ModelNumber.Text %>" VisibleIndex="1" Width="130px" Visible="false"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="SerialNumber" Caption="<%$ Resources:STCTRL_CalCertInfoCtrl,SerialNumber.Text %>" VisibleIndex="2" Width="130px" Visible="false"></dx:GridViewDataColumn>
        <dx:GridViewDataColumn FieldName="CalCertFileName" Caption="<%$ Resources:STCTRL_CalCertInfoCtrl,CalCertFileName.Text %>">
            <DataItemTemplate>
                <a href='<%# CalCertDownloadFormatString(Eval("CalCertFileName")) %>' target="_blank"><%#Eval("CalCertFileName") %></a>
            </DataItemTemplate>
        </dx:GridViewDataColumn>
        <dx:GridViewDataDateColumn FieldName="CalCertDate" Caption="<%$ Resources:STCTRL_CalCertInfoCtrl,CalCertDate.Text %>" VisibleIndex="15" Width="70px"></dx:GridViewDataDateColumn>
        <dx:GridViewDataDateColumn FieldName="CalCertDueDate" Caption="<%$ Resources:STCTRL_CalCertInfoCtrl,CalCertDueDate.Text %>" VisibleIndex="17" Width="70px"></dx:GridViewDataDateColumn>
    </Columns>
    <Styles>
        <Header HorizontalAlign="Center"></Header>
    </Styles>
</dx:ASPxGridView>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_CalCertInfoCtrl" />
</div>