﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class CalCertInfoCtrl : DownloadResourceCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CalCertDataBind();
            }
        }

        private void CalCertDataBind()
        {
            dxgvCalCert.DataSource = ProdRegInfo.CalCerts;
            dxgvCalCert.DataBind();
        }

        public String CalCertDownloadFormatString(object calCertFileName)
        {
            return String.Format("{0}?{1}=calcert&{2}={3}&{4}={5}&{6}={7}", KeyDef.UrlList.ConnectDownloadURL
                , KeyDef.QSKeys.DownloadSource
                , KeyDef.QSKeys.ModelNumber, ModelNumber
                , KeyDef.QSKeys.SerialNumber, SerialNumber
                , KeyDef.QSKeys.CalCertFileName, ConvertUtility.ConvertNullToEmptyString(calCertFileName));
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

      
    }
}