﻿using System;
using System.Web;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public class DownloadResourceCtrlBase : UserControl
    {
        private ProdReg_DataSource _prodRegDataSorce = new ProdReg_DataSource();
        public Guid WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }
        private string _activeTab = "";
        public string ActiveTab
        {
            get
            {
                var hostPage = Page as BP_ProductSupport;
                if (hostPage != null)
                {
                    _activeTab = hostPage.ActiveTab;
                }
                return _activeTab;
            }
        }

        private string _serialNumber;
        public string SerialNumber
        {
            get
            {
                //get it from the Parent Page
                var hostPage = Page as BP_ProductSupport;
                if (hostPage != null)
                {
                    _serialNumber = hostPage.SerialNumber;
                }
                if (!string.IsNullOrEmpty(_serialNumber))
                    return _serialNumber;
                var data = _prodRegDataSorce.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber);
                if (data != null && data.RegInfo != null && data.RegInfo.Count > 0)
                    _serialNumber = data.RegInfo[0].MasterSerial;
                return _serialNumber;
            }
            set { _serialNumber = value; }
        }
        public string ModelNumber
        {
            get
            {
                //MA-1395
                //return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.ModelNumber]);
                if (string.IsNullOrWhiteSpace(ConvertUtility.ConvertNullToEmptyString(Session[KeyDef.SSKeys.Selected_ModelNumber])))
                    Response.Redirect(KeyDef.UrlList.MyProducts);
                return ConvertUtility.ConvertNullToEmptyString(Session[KeyDef.SSKeys.Selected_ModelNumber]);
            }
        }

        private Acc_AccountMaster _SelectedAccount;
        public Acc_AccountMaster SelectedAccount
        {
            get
            {
                if (_SelectedAccount == null) _SelectedAccount = AccountUtility.GetSelectedAccount(false, true);
                return _SelectedAccount;
            }
        }

        private ProdReg_SessionData _ProdRegInfo;
        public ProdReg_SessionData ProdRegInfo
        {
            get
            {
                if (_ProdRegInfo == null)
                {
                    ProdReg_DataSource ds = new ProdReg_DataSource();
                    _ProdRegInfo = ds.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber, SerialNumber);
                }
                return _ProdRegInfo;
            }
        }

        private ProductConfig _ProdConfig;
        public ProductConfig ProductConfig
        {
            get
            {
                if (_ProdConfig == null)
                {
                    if (ProdRegInfo == null || ProdRegInfo.RegInfo == null) return null;
                    _ProdConfig = ProductConfigUtility.SelectByModel(false, ModelNumber);
                }
                return _ProdConfig;
            }
        }
    }
}