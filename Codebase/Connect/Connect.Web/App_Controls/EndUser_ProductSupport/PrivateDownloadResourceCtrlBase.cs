﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Web.App_Lib;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public class PrivateDownloadResourceCtrlBase : DownloadResourceCtrlBase
    {
        #region Protected Methods

        protected String GetFileName(object value)
        {
            String fileName = string.Empty;
            if (value != null)
            {
                String input = Convert.ToString(value);
                fileName = input.Substring(input.LastIndexOf('/') + 1);
            }
            return fileName;
        }

        protected string ConvertBytesAsString(object inputbytes)
        {
            long bytes = ConvertUtility.ConvertToInt64(inputbytes, 0);
            string[] suffix = { "B", "KB", "MB", "GB", "TB" };
            int i;
            double doubleBytes = bytes;

            for (i = 0; (int)(bytes / 1024) > 0; i++, bytes /= 1024)
            {
                doubleBytes = bytes / 1024.0;
            }

            return string.Format("{0:0.00} {1}", doubleBytes, suffix[i]);
        }

        protected String GenerateTAUDLURL(object filePath)
        {
            var filepath = ConvertUtility.ConvertToString(filePath, String.Empty).Replace(@"\", "/");
            return string.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.PrivateDownloadHandler, KeyDef.QSKeys.TargetURL, HttpUtility.UrlEncode(filepath), KeyDef.QSKeys.DownloadSource, ModelConfigTypeInfo.CONFIGTYPE_UK_ESD);
        }

        protected String GenerateJpDlUrl(object filePath)
        {
            var filepath = ConvertUtility.ConvertToString(filePath, String.Empty).Replace(@"\", "/");
            return string.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.PrivateDownloadHandler, KeyDef.QSKeys.TargetURL, HttpUtility.UrlEncode(filepath), KeyDef.QSKeys.DownloadSource, ModelConfigTypeInfo.CONFIGTYPE_JP);
        }
      
        public String GetVirtualName(object value)
        {
            var castValue = Convert.ToString(value);
            if (String.IsNullOrEmpty(castValue)) return String.Empty;
            if (castValue.LastIndexOf('/') > 0)
            {
                return castValue.Substring(castValue.LastIndexOf('/') + 1);
            }
            return String.Empty;
        }

        protected String GetExpiryDate(object value)
        {
            if (value != DBNull.Value)
            {
                return Convert.ToDateTime(Eval("ExpiryDate")).ToShortDateString();
            }
            else
                return Convert.ToString(GetGlobalResourceObject("STCTRL_JpPrivateresources", "notset"));
        }

        protected Boolean DoEnable()
        {
            if (Session["DisableDownloadslink"] != null)
            {
                return Convert.ToBoolean(Session["DisableDownloadslink"]);
            }
            return true;
        }

        protected void SetDownloadPaths(ASPxGridView gridView, DataTable dtSource)
        {
            if (dtSource == null || dtSource.Rows.Count == 0)
            {
                if (gridView != null)
                    gridView.DataSource = null;
                return;
            }
            var lstPaths = new List<String>();
            if (Session[KeyDef.SSKeys.PrivateDownloads] != null)
            {
                lstPaths = ((List<String>)Session[KeyDef.SSKeys.PrivateDownloads]);
            }
            if (dtSource.Rows.Count > 0)
            {
                foreach (var dr in dtSource.Rows.Cast<DataRow>().Where(dr => !lstPaths.Contains(Convert.ToString(dr["FilePath"]))))
                {
                    lstPaths.Add(ConvertUtility.ConvertToString(dr["FilePath"], string.Empty));
                }
                Session["downloadPaths"] = lstPaths;
            }
            if (gridView != null)
                gridView.DataSource = dtSource;
        }

        #endregion
    }
}