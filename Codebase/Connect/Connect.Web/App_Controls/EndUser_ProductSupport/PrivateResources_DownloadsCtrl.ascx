﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrivateResources_DownloadsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResourcesDownloadsCtrl" %>
<%@ Import Namespace="System.IO" %>
<div style="padding: 10px;">
      <asp:PlaceHolder runat="server">
        <script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/ajax-method.js"></script>
        </asp:PlaceHolder>
    <script type="text/javascript" language="javascript">

        var wsURL = "/App_Services/TAUDLClientLog.asmx/LogClientAttemptedDL";

        function SendDLInfo(filepath, filesize, email, ip, modelConfig) {
            $.ajax({
                type: "POST",
                url: wsURL,
                data: "{'FilePath':'" + filepath + "','FileSize':'" + filesize + "','EmailID':'" + email + "','ClientIP':'" + ip + "','ModelConfig':'" + modelConfig + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (msg) { },
                error: function (e) { alert("An Error Occured: " + e.statusText); }
            });
        }

        function OnRowClick(s, e) {
            var expanded = !!s.cpVisibleDetails[e.visibleIndex];
            if (expanded)
                s.CollapseDetailRow(e.visibleIndex);
            else
                s.ExpandDetailRow(e.visibleIndex);
        }
    </script>

    <dx:ASPxGridView ID="dxgvEndUserSupportPrivateDL" runat="server" CssClass="downloads-table" ClientInstanceName="dxgvEndUserSupportPrivateDL"
        OnCustomJSProperties="grid_CustomJSProperties" DataSourceID="odsProdSupportPrivateTAUDLPackages"
        Width="100%" Theme="AnritsuDevXTheme" KeyFieldName="PackageID" AutoGenerateColumns="false">
        <Settings GridLines="Horizontal" ShowColumnHeaders="false" />
        <SettingsBehavior AllowSelectByRowClick="true" />
        <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
        <SettingsPager PageSize="100"></SettingsPager>
        <Columns>
            <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="PackageName" VisibleIndex="1" ReadOnly="true" Caption=" ">
                <DataItemTemplate>
                    <div>
                        <asp:Literal ID="ltrPackageName" runat="server" Text='<%# Eval("PackageName") %>'></asp:Literal>
                    </div>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
        </Columns>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="dxgvTAUPackageFiles" ClientInstanceName="dxgvTAUPackageFiles" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                    DataSourceID="odsProdSupportPrivateTAUDLPackageFiles" OnDataBound="dxgvTAUPackageFiles_OnDataBound" KeyFieldName="FileID" Width="100%" OnBeforePerformDataSelect="dxgvTAUPackageFiles_BeforePerformDataSelect" 
                    OnHtmlRowPrepared="dxgvTAUPackageFiles_HtmlRowPrepared" >
                    <Settings GridLines="Both" ShowColumnHeaders="false" />
                    <SettingsDetail AllowOnlyOneMasterRowExpanded="true" />
                    <Columns>
                        <dx:GridViewDataColumn FieldName="FileID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="FilePath" Caption=" " VisibleIndex="1" ReadOnly="true">
                            <DataItemTemplate>
                                <%# GetFileName(Eval("FilePath")) %>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="130px" CellStyle-HorizontalAlign="Center">
                            <DataItemTemplate>
                                <div class="settingrow">
                                    <asp:HyperLink ID="hlDownload" runat="server" SkinID="blueit" Text="<%$ Resources:STCTRL_PrivateResources_TAUCtrl,dxhlDownload.Text%>" Target="_blank" NavigateUrl='<%# GenerateTAUDLURL(Eval("FilePath")) %>'></asp:HyperLink>
                                </div>
                                <div class="settingrow">
                                    <span class='dlbl'>
                                        <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PrivateResources_TAUCtrl,ltrDownloadProperties.Text.FileSizeLabel%>'></asp:Localize>: <%# ConvertBytesAsString(Eval("FileSize")) %></span>
                                </div>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <SettingsPager Mode="ShowPager" PageSize="100" Position="Bottom" AlwaysShowPager="True">
                        <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
                    </SettingsPager>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <Styles Row-Cursor="pointer"></Styles>
         <ClientSideEvents RowClick="OnRowClick" />
    </dx:ASPxGridView>
    
    
 
    

    <dx:ASPxGridView ID="gvJpPackgeGroups" CssClass="downloads-table" runat="server"  ClientInstanceName="gvJpPackgeGroups" KeyFieldName="PackageGroupName"
        Width="100%" Theme="AnritsuDevXTheme" AutoGenerateColumns="false" Border="None" OnDataBound="gvJpPackgeGroups_DataBound" DataSourceID="odsJPDLPackageGroups" OnCustomJSProperties="grid_CustomJSProperties">
        <Settings GridLines="None" ShowColumnHeaders="false" />
        <SettingsBehavior AllowSelectByRowClick="true" />
        <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
        <SettingsPager PageSize="100"></SettingsPager>
        <Columns>
            <dx:GridViewDataColumn FieldName="PackageGroupName" VisibleIndex="0" ReadOnly="true" Caption=" ">
                <DataItemTemplate>                  
                    <div >
                        <asp:Literal ID="ltrPackageGpName" runat="server" Text='<%# Eval("PackageGroupName") %>'></asp:Literal>
                        <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>' align="top" />
						<asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>'  align="top"/>
                    </div>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
        </Columns>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="gvJpPackages" ClientInstanceName="gvJpPackages" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                    KeyFieldName="PackageId" Width="98%" OnBeforePerformDataSelect="gvJpPackages_OnBeforePerformDataSelect"  OnCustomJSProperties="grid_CustomJSProperties" OnDetailRowExpandedChanged="gvJpPackages_DetailRowExpandedChanged">
                    <Settings GridLines="None" ShowColumnHeaders="false" />
                    <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="True" />

                    <Columns>
                        <dx:GridViewDataColumn FieldName="PackageId" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="1" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="HasFolders" VisibleIndex="2" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="PackageName" VisibleIndex="3" ReadOnly="true">
                            <DataItemTemplate>
                                <div class="inline-block">
                                    <%# GetVirtualName(Eval("PackagePath")) %>
                                    <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>'  align="top"/>
                                    <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>' align="top" />
                                </div>
                                <span style="float: right">
                                    <asp:PlaceHolder ID="phExpiry" Visible='<%#(Eval("ExpiryDate")!=DBNull.Value) %>' runat="server">
                                        <asp:Literal ID="ltrPackageSupportExpired" runat="server" Text='<%$Resources: STCTRL_JpPrivateresources,ltrPackageSupportExpired.Text%>'></asp:Literal>

                                        <asp:Literal ID="ltrExpiryDate" runat="server" Text='<%#GetExpiryDate(Eval("ExpiryDate")) %>' Visible="true"></asp:Literal>
                                    </asp:PlaceHolder>
                                </span>

                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="gvJpPackageSubFolders" ClientInstanceName="gvJpPackageSubFolders"  runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                                KeyFieldName="SubFolder" Width="98%" CssClass="removeborder" OnBeforePerformDataSelect="gvJpPackageSubFolders_OnBeforePerformDataSelect"  OnCustomJSProperties="grid_CustomJSProperties">
                                <Settings GridLines="None" ShowColumnHeaders="false" />
                                <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="false" />
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="1" Visible="False"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="SubFolder" VisibleIndex="2">
                                        <DataItemTemplate>
                                            <div>
                                                <%# Eval("SubFolder") %>
                                                <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>' align="top" />
                                                <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>' align="top" />
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gvJpSubFolderFiles" ClientInstanceName="gvJpSubFolderFiles" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                                            KeyFieldName="FileID" Width="98%" CssClass="seperation"  OnCustomJSProperties="grid_CustomJSProperties" OnBeforePerformDataSelect="gvJpSubFolderFiles_BeforePerformDataSelect" OnHtmlRowPrepared="gvJpSubFolderFiles_HtmlRowPrepared">
                                            <Settings GridLines="Both" ShowColumnHeaders="false" />
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="FileID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="FilePath" Caption=" " VisibleIndex="1" ReadOnly="true">
                                                    <DataItemTemplate>
                                                        <%# GetFileName(Eval("FilePath")) %>
                                                        <div>
                                                            <%# Server.HtmlDecode(Convert.ToString(Eval("Comments"))) %>
                                                        </div>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="130px" CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <div class="settingrow">
                                                            <asp:Image runat="server" Visible="False" ID="imginfo" />
                                                            <asp:HyperLink ID="hlDownload" SkinID="blueit" runat="server" Text="<%$ Resources:STCTRL_PrivateResources_TAUCtrl,dxhlDownload.Text%>" Enabled="<%#(DoEnable())%>" Target="_blank" NavigateUrl='<%# GenerateJpDlUrl(Eval("FilePath")) %>'></asp:HyperLink>
                                                        </div>
                                                        <div class="settingrow">
                                                            <span class='dlbl'>
                                                                <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PrivateResources_TAUCtrl,ltrDownloadProperties.Text.FileSizeLabel%>'></asp:Localize>: <%# ConvertBytesAsString(Eval("FileSize")) %></span>
                                                            <span class='dlbl'>
                                                                <asp:Localize ID="lcalUpdatedOn" runat="server" Visible='<%# ((Eval("ModifiedOnUTC"))!=DBNull.Value) %>' Text='<%$Resources:STCTRL_JpPrivateresources,lcalUpdatedOn.Text%>'></asp:Localize>
                                                                <%# ((Eval("ModifiedOnUTC"))!=DBNull.Value)?Convert.ToDateTime(Eval("ModifiedOnUTC")).ToShortDateString():"" %></span>
                                                        </div>
                                                    </DataItemTemplate>

                                                </dx:GridViewDataColumn>
                                            </Columns>
											<Styles Row-Cursor="pointer"></Styles>
                                            <ClientSideEvents RowClick="OnRowClick" />
                                            <SettingsPager Mode="ShowPager" PageSize="100" Position="Bottom" AlwaysShowPager="True">
                                                <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <Styles>
                                    <Cell>
                                        <Border BorderStyle="None" />
                                    </Cell>
                                    <DetailCell>
                                        <Border BorderStyle="None" />
                                    </DetailCell>
                                    <DetailButton>
                                        <Border BorderStyle="None" />
                                    </DetailButton>
                                </Styles>
                                <Styles Row-Cursor="pointer"></Styles>
                              <ClientSideEvents RowClick="OnRowClick" />
                            </dx:ASPxGridView>
                            <dx:ASPxGridView ID="gvJPPackageFiles" ClientInstanceName="gvJPPackageFiles"  runat="server" Theme="AnritsuDevXTheme"
                                AutoGenerateColumns="false" CssClass="seperation"  OnCustomJSProperties="grid_CustomJSProperties" OnBeforePerformDataSelect="gvJPPackageFiles_OnBeforePerformDataSelect" OnHtmlRowPrepared="gvJpSubFolderFiles_HtmlRowPrepared"
                                KeyFieldName="FileID" Width="98%">
                                <Settings GridLines="Both" ShowColumnHeaders="false" />
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="False" />
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="FileID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="FilePath" Caption=" " VisibleIndex="1" ReadOnly="true">
                                        <DataItemTemplate>
                                            <%# GetFileName(Eval("FilePath")) %>
                                            <br />
                                            <br />
                                            <div>
                                            <%# Server.HtmlDecode(Convert.ToString(Eval("Comments")).Trim()) %>
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="130px" CellStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <div class="settingrow">
                                                <asp:Image runat="server" Visible="False" ID="imginfo" />
                                                <asp:HyperLink ID="hlDownload" SkinID="blueit" runat="server" Text="<%$ Resources:STCTRL_PrivateResources_TAUCtrl,dxhlDownload.Text%>" Target="_blank" Enabled="<%#(DoEnable())%>" NavigateUrl='<%# GenerateJpDlUrl(Eval("FilePath")) %>'></asp:HyperLink>
                                            </div>
                                            <div class="settingrow">
                                                <span class='dlbl'>
                                                    <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PrivateResources_TAUCtrl,ltrDownloadProperties.Text.FileSizeLabel%>'></asp:Localize>: <%# ConvertBytesAsString(Eval("FileSize")) %></span>
                                                <span class='dlbl'>
                                                    <asp:Localize ID="lcalUpdatedOn" runat="server" Visible='<%# ((Eval("ModifiedOnUTC"))!=DBNull.Value) %>' Text='<%$Resources:STCTRL_JpPrivateresources,lcalUpdatedOn.Text%>'></asp:Localize>
                                                    <%# ((Eval("ModifiedOnUTC"))!=DBNull.Value)?Convert.ToDateTime(Eval("ModifiedOnUTC")).ToShortDateString():"" %></span>
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
								<Styles Row-Cursor="pointer"></Styles>
                                <ClientSideEvents RowClick="OnRowClick" />
                                <SettingsPager Mode="ShowPager" PageSize="100" Position="Bottom" AlwaysShowPager="True">
                                    <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <Styles>
                        <Cell>
                            <Border BorderStyle="None" />
                        </Cell>
                        <DetailCell>
                            <Border BorderStyle="None" />
                        </DetailCell>
                        <DetailButton>
                            <Border BorderStyle="None" />
                        </DetailButton>
                    </Styles>
					<Styles Row-Cursor="pointer"></Styles>
        			<ClientSideEvents RowClick="OnRowClick" />				
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <Styles Row-Cursor="pointer"></Styles>
        <ClientSideEvents RowClick="OnRowClick" />
    </dx:ASPxGridView>

    <asp:ObjectDataSource ID="odsJPDLPackageGroups" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource" OnSelecting="odsJPDLPackageGroups_Selecting"
        SelectMethod="SelectJpPackageGroups" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsJPDLPackageGroupsCKD">
        <SelectParameters>
            <asp:Parameter Name="modelNumber" DbType="String" />
            <asp:Parameter Name="serialNumber" DbType="String" />
            <asp:Parameter Name="membershipId" DbType="Guid" />
            <asp:Parameter Name="packageType" DbType="String" />
            <asp:Parameter Name="modelConfigType" DbType="String" />
            <asp:Parameter Name="pkgGroupName" DbType="String" />
            <asp:Parameter Name="dtTeams"/>
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="odsProdSupportPrivateTAUDLPackages" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource" OnSelecting="odsProdSupportPrivateTAUDLPackages_Selecting"
        SelectMethod="SelectTAUPackagesByAccountID" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdSupportPrivateTAUDLPackagesCKD">
        <SelectParameters>
            <asp:Parameter Name="dtTeams" />
            <asp:Parameter Name="isLicensePkg" DefaultValue="false" />
            <%--MA-1395--%>
            <%--<asp:QueryStringParameter Name="modelNumner" QueryStringField="mn" DbType="String" />--%>
            <asp:SessionParameter Name="modelNumner" DbType="String" SessionField="mn" />
            <asp:Parameter Name="serialNumber" DbType="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="odsProdSupportPrivateTAUDLPackageFiles" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource"
        SelectMethod="SelectTAUPackageFilesByPackageID" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdSupportPrivateTAUDLPackageFilesCKD">
        <SelectParameters>
            <asp:Parameter Name="PackageID" DbType="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_PrivateResources_TAUCtrl" />
</div>
