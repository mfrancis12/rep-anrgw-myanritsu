﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrivateResources_JPNonUSBCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResources_JPNonUSBCtrl" %>
<div style="padding: 10px;">
                    <div class="settingrow">
                        <p class="msg"><asp:Localize ID="lcalJPDLVerMsg" runat="server"></asp:Localize></p>
                    </div>
                    <div class="settingrow">
                        <dx:ASPxGridView ID="dxgvEndUserSupportPrivate_JPVerTBL" runat="server" DataSourceID="odsProdSupportPrivateJPVer" Border-BorderStyle="None">
                            <Columns>
                                <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="2" CellStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                <dx:GridViewDataTextColumn FieldName="Bla" VisibleIndex="3" CellStyle-ForeColor="#0BA645">
                                    <DataItemTemplate>></DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataHyperLinkColumn FieldName="VersionFileURL" VisibleIndex="4" CellStyle-ForeColor="#152DBD">
                                    <PropertiesHyperLinkEdit TextField="VersionFileName"></PropertiesHyperLinkEdit>
                                </dx:GridViewDataHyperLinkColumn>
                                <dx:GridViewDataColumn FieldName="VersionMessage" VisibleIndex="5" CellStyle-Font-Size="Smaller"></dx:GridViewDataColumn>
                            </Columns>
                            <Settings ShowColumnHeaders="false" ShowFooter="false" GridLines="None" />
                            <Styles Cell-BackColor="#EDF3F4" GroupRow-Cursor="pointer"></Styles>
                        </dx:ASPxGridView>
                    </div>
                    <dx:ASPxGridView ID="dxgvEndUserSupportPrivateDL" runat="server" ClientInstanceName="dxgvEndUserSupportPrivateDL" OnDataBound="dxgvEndUserSupportPrivateDL_DataBound"
                        Width="100%" Theme="AnritsuDevXTheme" DataSourceID="odsProdSupportPrivate" OnHtmlRowCreated="dxgvEndUserSupportPrivateDL_HtmlRowCreated">
                        <Columns>
                            <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$ Resources:STCTRL_PrivateResourcesCtrl_JPNonUSB,ModelNumber.Text %>" VisibleIndex="1" GroupIndex="1"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Version" VisibleIndex="2" GroupIndex="2" SortOrder="Descending"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="DownloadType" GroupIndex="3" Caption="<%$ Resources:STCTRL_PrivateResourcesCtrl_JPNonUSB,DownloadType.Text %>"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="Title" Caption=" " VisibleIndex="2" Width="440px">
                                <DataItemTemplate>
                                    <div class="settingrow"><%# Eval("DownloadTitle") %></div>
                                    <div class="settingrow" style="font-size:11px;"><asp:Literal ID="ltrDownloadProperties" runat="server"></asp:Literal></div>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataDateColumn FieldName="UpdatedDate" Caption="<%$ Resources:STCTRL_PrivateResourcesCtrl_JPNonUSB,UpdatedDate.Text %>" VisibleIndex="5" Width="60px" CellStyle-CssClass="tablecell" CellStyle-HorizontalAlign="Center"></dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ReleaseDate" Caption="<%$ Resources:STCTRL_PrivateResourcesCtrl_JPNonUSB,ReleaseDate.Text %>" VisibleIndex="5" Width="60px" CellStyle-CssClass="tablecell" CellStyle-HorizontalAlign="Center"></dx:GridViewDataDateColumn>
                            <dx:GridViewDataColumn FieldName="FullDownloadURL" VisibleIndex="500" Caption=" " Width="100px" CellStyle-HorizontalAlign="Center">
                                <DataItemTemplate>
                                    <div class="settingrow">
                                        <dx:ASPxHyperLink ID="dxhlDownload" runat="server" Text="<%$ Resources:STCTRL_PublicResourcesCtrl,dxhlDownload.Text %>" Target="_blank" NavigateUrl='<%# Eval("FullDownloadURL") %>'></dx:ASPxHyperLink>
                                    </div>
                                    <div class="settingrow">
                                        <span class='dlbl'><asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PublicResourcesCtrl,ltrDownloadProperties.Text.FileSizeLabel %>'></asp:Localize>: <%# Eval("SizeMB", "{0} MB") %></span>
                                    </div>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                        </Columns>
                        <SettingsPager PageSize="100"></SettingsPager>
                        <SettingsBehavior AutoExpandAllGroups="false" />
                        <Settings ShowGroupPanel="false" GroupFormat="{1}" />
                        <Styles GroupRow-Cursor="pointer"></Styles>
                        <ClientSideEvents RowClick="function (s,e) { Grid_ToggleGroupRow(s, e.visibleIndex); }" />
                    </dx:ASPxGridView>

<asp:ObjectDataSource ID="odsProdSupportPrivate" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource" 
    SelectMethod="JapanNonUSB_SelectByWebToken" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdSupportPrivateCKD">
    <SelectParameters>
        <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsProdSupportPrivateJPVer" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource" 
SelectMethod="JapanUSBandNonUSB_GetDownloadVersionTBL" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsProdSupportPrivateJPVerCKD" OnSelected="odsProdSupportPrivateJPVer_Selected">
<SelectParameters>
    <asp:QueryStringParameter Name="WebToken" QueryStringField="rwak" DbType="Guid" />
</SelectParameters>
</asp:ObjectDataSource>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_PrivateResourcesCtrl_JPNonUSB" />
</div>