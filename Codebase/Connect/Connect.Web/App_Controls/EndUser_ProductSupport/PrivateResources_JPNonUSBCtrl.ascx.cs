﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DevExpress.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class PrivateResources_JPNonUSBCtrl : DownloadResourceCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HandleODSCache();
            }
        }

        private void HandleODSCache()
        {
            Int32 gwCultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            String obds_DLCacheKey = String.Format("{0}_{1}", WebToken.ToString(), gwCultureGroupID);
            if (Cache[odsProdSupportPrivate.CacheKeyDependency] == null) Cache[odsProdSupportPrivate.CacheKeyDependency] = obds_DLCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsProdSupportPrivate.CacheKeyDependency].ToString().Equals(obds_DLCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsProdSupportPrivate.CacheKeyDependency);
                Cache[odsProdSupportPrivate.CacheKeyDependency] = obds_DLCacheKey;
            }

            String obds_VerCacheKey = String.Format("{0}", WebToken.ToString());
            if (Cache[odsProdSupportPrivateJPVer.CacheKeyDependency] == null) Cache[odsProdSupportPrivateJPVer.CacheKeyDependency] = obds_VerCacheKey;
            if (Request["refresh"] == "1" || !Cache[odsProdSupportPrivateJPVer.CacheKeyDependency].ToString().Equals(obds_VerCacheKey, StringComparison.InvariantCultureIgnoreCase))
            {
                Cache.Remove(odsProdSupportPrivateJPVer.CacheKeyDependency);
                Cache[odsProdSupportPrivateJPVer.CacheKeyDependency] = obds_VerCacheKey;
            }
        }

        public String GetVersionTBLURL(Object model, Object verFileName)
        {
            return DownloadUtility.GetMyAnritsuDownloadURL(WebToken, model.ToString().Trim(), "0", "dlv");
        }

        public String GetVersionTBLMessage(Object verMessage)
        {
            if (verMessage == null) return String.Empty;
            String str = verMessage.ToString();
            if (str.IsNullOrEmptyString()) return String.Empty;
            return String.Format("<span class='text-normal'>(Note: {0})</span>", str);
        }


        public string StaticResourceClassKey
        {
            get { return "STCTRL_PrivateResourcesCtrl_JPNonUSB"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void dxgvEndUserSupportPrivateDL_DataBound(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsCallback)
            {
                ASPxGridView grid = sender as ASPxGridView;
                for (int i = 0; i < grid.VisibleRowCount; i++)
                {
                    if (grid.IsGroupRow(i) && (grid.GetRowLevel(i) == 0))
                    {
                        grid.ExpandRow(i);

                    }
                }
            }

        }

        protected void dxgvEndUserSupportPrivateDL_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                ASPxGridView masterGrid = (ASPxGridView)sender;
                Literal ltr = masterGrid.FindRowCellTemplateControl(e.VisibleIndex
                    , masterGrid.Columns["Title"] as GridViewDataColumn
                    , "ltrDownloadProperties") as Literal;


                StringBuilder sbText = new StringBuilder();

                String version = ConvertUtility.ConvertNullToEmptyString(e.GetValue("Version"));
                if (!version.IsNullOrEmptyString())
                {
                    //if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.AppendFormat("<span class='dlbl'>{0}</span><span class='dltxt'>{1}</span>", GetStaticResource("ltrDownloadProperties.Text.VersionLabel"), version);
                }

                Boolean newUpdate = ConvertUtility.ConvertToBoolean(e.GetValue("ShowNewFlag"), false);
                if (newUpdate)
                {
                    if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.Append("<span style='font-size: 11px; font-weight: bold; color: #FF7A00;'>new</span>");
                }

                String commentText = ConvertUtility.ConvertNullToEmptyString(e.GetValue("CommentText"));
                if (!String.IsNullOrEmpty(commentText))
                {
                    if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.AppendFormat("<p class='dltxt'>{0}</p>", HttpUtility.HtmlEncode(commentText));
                }

                if (sbText.Length > 0) ltr.Text = sbText.ToString();
                else ltr.Text = "&nbsp;";
            }

        }

        protected void odsProdSupportPrivateJPVer_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            DataTable tb = e.ReturnValue as DataTable;
            if (tb == null || tb.Rows.Count < 1)
            {
                lcalJPDLVerMsg.Visible = false;
                dxgvEndUserSupportPrivate_JPVerTBL.Visible = false;
            }
        }
    }

}