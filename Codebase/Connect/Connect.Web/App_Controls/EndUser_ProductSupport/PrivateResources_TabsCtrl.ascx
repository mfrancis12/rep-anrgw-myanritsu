﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrivateResources_TabsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResources_TabsCtrl" %>
<dx:ASPxTabControl ID="dxTabPrvLevel2" runat="server" Width="100%" Theme="AnritsuDevXTheme" TabAlign="Left" ActiveTabIndex="0">
    <Tabs>
        <dx:Tab Name="tbPrivateDownloads" Visible="true" Text="<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbPrivateTAUDownloads.Text %>" ToolTip="<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbPrivateTAUDownloads.Tooltip %>"  ></dx:Tab>
        <dx:Tab Name="tbLicences" Visible="true" Text= "<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbLicences.Text %>" ToolTip="<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbLicences.Tooltip %>" ></dx:Tab>
        <dx:Tab Name="tbVersionMatchingtbl" Visible="true" Text= "<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbVersionMatchingtbl.Text %>" ToolTip="<%$ Resources:STCTRL_PrivateResourcesTabsCtrl,tbVersionMatchingtbl.Tooltip %>" ></dx:Tab>
    </Tabs>
    <Paddings PaddingBottom="0" />
</dx:ASPxTabControl>