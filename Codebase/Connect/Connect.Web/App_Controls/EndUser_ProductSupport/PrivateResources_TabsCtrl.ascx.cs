﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Lib.Cfg;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class PrivateResources_TabsCtrl : DownloadResourceCtrlBase
    {
        public DevExpress.Web.ASPxTabControl ProductSupportTabs { get { return dxTabPrvLevel2; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !Page.IsCallback)
            {
                InitTabs();
            }
        }

        public void InitTabs()
        {
            if (Page.IsCallback) return;
            var tabCtrl = dxTabPrvLevel2;
            if (tabCtrl == null) return;
            var tbPrivateDownloads = tabCtrl.Tabs.FindByName("tbPrivateDownloads");
            Tab tbLicences = tabCtrl.Tabs.FindByName("tbLicences");
            var tbVersionMatching = tabCtrl.Tabs.FindByName("tbVersionMatchingtbl");
            var privSrc = new Downloads_PrivateDataSource();
            var ds = new ProdReg_DataSource();
            var prodRegInfo = ds.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber, SerialNumber);
            var hasTauDownloadPackages = privSrc.TAUDownloads_HasData(ModelNumber, AccountUtility.GenerateUserTeamIds(), prodRegInfo, SerialNumber);

            bool hasJpVersionMatchingPackages, hasJpLicensePackages, hasJpDownloadPackages;

            privSrc.JpDownloadsHasPackages(ModelNumber, SerialNumber, out hasJpDownloadPackages, out hasJpLicensePackages,
                out hasJpVersionMatchingPackages);

            //Default Tab
            tbPrivateDownloads.Visible = true;
            tbLicences.Visible = (hasTauDownloadPackages || hasJpLicensePackages);
            tbVersionMatching.Visible = hasJpVersionMatchingPackages;

            //MA-1395, removing modelnumber & serialnumber from querystring
            tbPrivateDownloads.NavigateUrl = String.Format("{0}?act=1",
                   KeyDef.UrlList.ProductSupportPrivateDownloads);
            tbLicences.NavigateUrl = String.Format("{0}?act=1",
                  KeyDef.UrlList.ProductSupportPrivateLicense);
            tbVersionMatching.NavigateUrl = String.Format("{0}?act=1",
                 KeyDef.UrlList.ProductSupportPrivateVersion);

            //tabCtrl.ActiveTab = tbPrivateDownloads;
        }
    }
}