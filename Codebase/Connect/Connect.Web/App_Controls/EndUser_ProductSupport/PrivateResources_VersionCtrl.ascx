﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrivateResources_VersionCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResourcesVersionCtrl" %>
<div style="padding: 10px;">
 <script type="text/javascript" language="javascript">
     function OnRowClick(s, e) {
         var expanded = !!s.cpVisibleDetails[e.visibleIndex];
         if (expanded)
             s.CollapseDetailRow(e.visibleIndex);
         else
             s.ExpandDetailRow(e.visibleIndex);
     }
    </script>
    <dx:ASPxGridView ID="gvJpPackgeGroups" CssClass="version-table" runat="server" DataSourceID="odsJPDLPackageGroups" ClientInstanceName="gvJpPackgeGroups" KeyFieldName="PackageGroupName"
        Width="100%" Theme="AnritsuDevXTheme" AutoGenerateColumns="false" OnCustomJSProperties="grid_CustomJSProperties">
        <Settings GridLines="Horizontal" ShowColumnHeaders="false" />
        <SettingsBehavior AllowSelectByRowClick="true" />
        <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
        <SettingsPager PageSize="100"></SettingsPager>
        <Columns>
            <dx:GridViewDataColumn FieldName="PackageGroupName" VisibleIndex="0" ReadOnly="true" Caption=" ">
                <DataItemTemplate>
                    <div >
                        <asp:Literal ID="ltrPackageGpName" runat="server" Text='<%# Eval("PackageGroupName") %>'></asp:Literal>
                        <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>' align="top" />
                        <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>' align="top" />
                    </div>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
        </Columns>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="gvJpPackages" ClientInstanceName="gvJpPackages" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                    KeyFieldName="PackageId" Width="100%" OnBeforePerformDataSelect="gvJpPackages_OnBeforePerformDataSelect" OnDetailRowExpandedChanged="gvJpPackages_DetailRowExpandedChanged" OnCustomJSProperties="grid_CustomJSProperties">
                    <Settings GridLines="Both" ShowColumnHeaders="false" />
                    <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
                    <Columns>
                        <dx:GridViewDataColumn FieldName="PackageId" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="1" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="HasFolders" VisibleIndex="2" Visible="false"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn FieldName="PackageName" Caption=" " VisibleIndex="3" ReadOnly="true">
                            <DataItemTemplate>
                                <div class="inline-block">
                                    <span>
                                        <%# GetVirtualName(Eval("PackagePath")) %>
                                    </span>
                                    <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>' align="top" />
                                    <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>' align="top" />
                                </div>
                                <span style="float: right">
                                    <asp:PlaceHolder ID="phExpiry" Visible='<%#(Eval("ExpiryDate")!=DBNull.Value) %>' runat="server">
                                        <asp:Literal ID="ltrPackageSupportExpired" runat="server" Text='<%$Resources: STCTRL_JpPrivateresources,ltrPackageSupportExpired.Text%>'></asp:Literal>
                                        <asp:Literal ID="ltrExpiryDate" runat="server" Text='<%#GetExpiryDate(Eval("ExpiryDate")) %>'></asp:Literal>
                                    </asp:PlaceHolder>
                                </span>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="gvJpPackageSubFolders" ClientInstanceName="gvJpPackageSubFolders" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                                KeyFieldName="SubFolder" Width="100%" OnBeforePerformDataSelect="gvJpPackageSubFolders_OnBeforePerformDataSelect" OnCustomJSProperties="grid_CustomJSProperties">
                                <Settings GridLines="Both" ShowColumnHeaders="false" />
                                <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="PackagePath" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="PackageID" VisibleIndex="1" Visible="False"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="SubFolder" VisibleIndex="2">
                                        <DataItemTemplate>
                                            <div >
                                                <%# Eval("SubFolder") %>
                                                <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWNEWICON")) %>' ID="imgNewIcon" ImageUrl='<%#ShowImageUrl("New") %>' align="top" />
                                                <asp:Image runat="server" Visible='<%#ShowIcon(Eval("SHOWUPDATEICON")) %>' ID="imgUpdateIcon" ImageUrl='<%#ShowImageUrl("Update") %>' align="top" />
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gvJpSubFolderFiles" ClientInstanceName="gvJpSubFolderFiles" runat="server" Theme="AnritsuDevXTheme" AutoGenerateColumns="false"
                                            KeyFieldName="FileID" Width="100%" OnBeforePerformDataSelect="gvJpSubFolderFiles_BeforePerformDataSelect" OnHtmlRowPrepared="gvJpSubFolderFiles_HtmlRowPrepared" OnCustomJSProperties="grid_CustomJSProperties">
                                            <Settings GridLines="Both" ShowColumnHeaders="false" />
                                            <SettingsDetail AllowOnlyOneMasterRowExpanded="false" />
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="FileID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="FilePath" Caption=" " VisibleIndex="1" ReadOnly="true">
                                                    <DataItemTemplate>
                                                        <%# GetFileName(Eval("FilePath")) %>
                                                        <br />
                                                        <br />
                                                        <div>
                                                            <%# Server.HtmlDecode(Convert.ToString(Eval("Comments")).Trim()) %>
                                                        </div>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="130px" CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <div class="settingrow">
                                                            <asp:Image runat="server" Visible="False" ID="imginfo" />
                                                            <asp:HyperLink ID="hlDownload" SkinID="blueit" runat="server" Text="<%$ Resources:STCTRL_PrivateResources_TAUCtrl,dxhlDownload.Text%>" Enabled="<%#(DoEnable())%>" Target="_blank" NavigateUrl='<%# GenerateJpDlUrl(Eval("FilePath")) %>'></asp:HyperLink>
                                                        </div>
                                                        <div class="settingrow">
                                                            <span class='dlbl'>
                                                                <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PrivateResources_TAUCtrl,ltrDownloadProperties.Text.FileSizeLabel%>'></asp:Localize>: <%# ConvertBytesAsString(Eval("FileSize")) %></span>
                                                            <span class='dlbl'>
                                                                <asp:Localize ID="lcalUpdatedOn" runat="server" Visible='<%# ((Eval("ModifiedOnUTC"))!=DBNull.Value) %>' Text='<%$Resources:STCTRL_JpPrivateresources,lcalUpdatedOn.Text%>'></asp:Localize>: <%# ((Eval("ModifiedOnUTC"))!=DBNull.Value)?Convert.ToDateTime(Eval("ModifiedOnUTC")).ToShortDateString():"" %></span>
                                                        </div>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                             <Styles Row-Cursor="pointer"></Styles>
                                            <ClientSideEvents RowClick="OnRowClick" />
                                            <SettingsPager Mode="ShowPager" PageSize="100" Position="Bottom" AlwaysShowPager="True">
                                                <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <Styles>
                                    <Cell>
                                        <Border BorderStyle="None" />
                                    </Cell>
                                    <DetailCell>
                                        <Border BorderStyle="None" />
                                    </DetailCell>
                                    <DetailButton>
                                        <Border BorderStyle="None" />
                                    </DetailButton>
                                </Styles>
                                 <Styles Row-Cursor="pointer"></Styles>
                                <ClientSideEvents RowClick="OnRowClick" />
                            </dx:ASPxGridView>
                            <dx:ASPxGridView ID="gvJPPackageFiles" ClientInstanceName="gvJPPackageFiles" runat="server" Theme="AnritsuDevXTheme"
                                AutoGenerateColumns="false" OnBeforePerformDataSelect="gvJPPackageFiles_OnBeforePerformDataSelect" OnHtmlRowPrepared="gvJpSubFolderFiles_HtmlRowPrepared" OnCustomJSProperties="grid_CustomJSProperties"
                                KeyFieldName="FileID" Width="100%">
                                <Settings GridLines="Both" ShowColumnHeaders="false" />
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="true" />
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="FileID" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="FilePath" Caption=" " VisibleIndex="1" ReadOnly="true">
                                        <DataItemTemplate>
                                            <%# GetFileName(Eval("FilePath")) %>
                                            <br />
                                            <br />
                                            <div>
                                                <%# Server.HtmlDecode(Convert.ToString(Eval("Comments")).Trim()) %>
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="130px" CellStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <div class="settingrow">
                                                <asp:Image runat="server" Visible="False" ID="imginfo" />
                                                <asp:HyperLink ID="hlDownload" SkinID="blueit" runat="server" Text="<%$ Resources:STCTRL_PrivateResources_TAUCtrl,dxhlDownload.Text%>" Enabled="<%#(DoEnable())%>" Target="_blank" NavigateUrl='<%# GenerateJpDlUrl(Eval("FilePath")) %>'></asp:HyperLink>
                                            </div>
                                            <div class="settingrow">
                                                <span class='dlbl'>
                                                    <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PrivateResources_TAUCtrl,ltrDownloadProperties.Text.FileSizeLabel%>'></asp:Localize>: <%# ConvertBytesAsString(Eval("FileSize")) %></span>
                                                <span class='dlbl'>
                                                    <asp:Localize ID="lcalUpdatedOn" runat="server" Visible='<%# ((Eval("ModifiedOnUTC"))!=DBNull.Value) %>' Text='<%$Resources:STCTRL_JpPrivateresources,lcalUpdatedOn.Text%>'></asp:Localize>: <%# ((Eval("ModifiedOnUTC"))!=DBNull.Value)?Convert.ToDateTime(Eval("ModifiedOnUTC")).ToShortDateString():"" %></span>
                                            </div>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                 <Styles Row-Cursor="pointer"></Styles>
                                <ClientSideEvents RowClick="OnRowClick" />
                                <SettingsPager Mode="ShowPager" PageSize="100" Position="Bottom" AlwaysShowPager="True">
                                    <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True"></PageSizeItemSettings>
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <Styles>
                        <Cell>
                            <Border BorderStyle="None" />
                        </Cell>
                        <DetailCell>
                            <Border BorderStyle="None" />
                        </DetailCell>
                        <DetailButton>
                            <Border BorderStyle="None" />
                        </DetailButton>
                    </Styles>
                    <Styles Row-Cursor="pointer"></Styles>
                    <ClientSideEvents RowClick="OnRowClick" />
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <Styles Row-Cursor="pointer"></Styles>
        <ClientSideEvents RowClick="OnRowClick" />
    </dx:ASPxGridView>

    <asp:ObjectDataSource ID="odsJPDLPackageGroups" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateDataSource" OnSelecting="odsJPDLPackageGroups_Selecting"
        SelectMethod="SelectJpPackageGroups" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30" CacheKeyDependency="odsJPDLPackageGroupsCKD">
        <SelectParameters>
            <asp:Parameter Name="modelNumber" DbType="String" />
            <asp:Parameter Name="serialNumber" DbType="String" />
            <asp:Parameter Name="membershipId" DbType="Guid" />
            <asp:Parameter Name="packageType" DbType="String" />
            <asp:Parameter Name="modelConfigType" DbType="String" />
            <asp:Parameter Name="pkgGroupName" DbType="String" />
            <asp:Parameter Name="dtTeams" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_PrivateResources_TAULicenseCtrl" />

</div>
