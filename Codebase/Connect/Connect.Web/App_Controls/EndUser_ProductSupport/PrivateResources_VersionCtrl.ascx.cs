﻿/*
 * Author:        kishore kumar M
 * Created Date:  
 * Modified Date: 04/03/2014
 * Modified By:   kishore kumar M
 * Purpose:       List out License packages
 */

using System;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;
using System.Collections;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class PrivateResourcesVersionCtrl : PrivateDownloadResourceCtrlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) return;
            gvJpPackgeGroups.DataBind();
        }
        #region ObjectDataSource Methods
        protected void odsJPDLPackageGroups_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["modelNumber"] = ModelNumber;
            e.InputParameters["serialNumber"] = SerialNumber;
            e.InputParameters["membershipId"] = LoginUtility.GetCurrentUser(true).MembershipId;
            e.InputParameters["packageType"] = PackageType.VersionMatching;
            e.InputParameters["modelConfigType"] = ModelConfigTypeInfo.CONFIGTYPE_JP;
            e.InputParameters["pkgGroupName"] = "";
            e.InputParameters["dtTeams"] = AccountUtility.GenerateUserTeamIds();
        }

        #endregion

        #region ASPX Gridview Events

        protected void gvJpSubFolderFiles_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                var hlDownload = ((ASPxGridView)sender).FindRowCellTemplateControl(e.VisibleIndex, null, "hlDownload") as HyperLink;
                var img = ((ASPxGridView)sender).FindRowCellTemplateControl(e.VisibleIndex, null, "imginfo") as Image;
                if (hlDownload != null)
                {
                    if (Session["DisableDownloadslink"] != null && !Convert.ToBoolean(Session["DisableDownloadslink"]))
                    {
                        hlDownload.Style.Add("color", "#CCC");
                        hlDownload.Style.Add("cursor", "default !important");
                        hlDownload.Style.Add("text-decoration", "none");
                    }
                    else
                    {
                        var objFilepath = (String)e.GetValue("FilePath");
                        var objFileSize = e.GetValue("FileSize").ToString();
                        hlDownload.Attributes.Add("onclick", "SendDLInfo('" + objFilepath + "','" + objFileSize + "','" + LoginUtility.GetCurrentUser(true).Email + "','" + WebUtility.GetUserIP() + "','" + ModelConfigTypeInfo.CONFIGTYPE_JP + "')");
                    }
                }
                if (img != null)
                {
                    if (e.GetValue("ModifiedOnUTC") != DBNull.Value)
                    {
                        DateTime updateDate = Convert.ToDateTime(e.GetValue("ModifiedOnUTC"));
                        if (updateDate.AddDays(14) >= DateTime.Now)
                        {
                            if ((e.GetValue("ShowNewIcon") != DBNull.Value) && (bool)e.GetValue("ShowNewIcon"))
                                img.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/New.png";
                            else if (e.GetValue("ShowUpdateIcon") != DBNull.Value && (bool)e.GetValue("ShowUpdateIcon"))
                                img.ImageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/Updated.png";
                            img.Visible = true;
                            img.CssClass = "block";
                        }
                    }
                }
            }
        }
        protected void grid_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            Hashtable table = new Hashtable(grid.DetailRows.VisibleCount);
            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                if (grid.DetailRows.IsVisible(i))
                    table[i] = true;
            }
            e.Properties["cpVisibleDetails"] = table;
        }
        protected void gvJpPackages_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var packageGrpName = (String)detailGrid.GetMasterRowKeyValue();

            var jpPkgList = new Downloads_PrivateDataSource().SelectJpPackageGroups(ModelNumber,
               SerialNumber, LoginUtility.GetCurrentUser(true).MembershipId, PackageType.VersionMatching,
                ModelConfigTypeInfo.CONFIGTYPE_JP, packageGrpName, AccountUtility.GenerateUserTeamIds(true));
            detailGrid.DataSource = jpPkgList;
        }

        protected void gvJpSubFolderFiles_BeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var pkgId = (long)detailGrid.GetMasterRowFieldValues("PackageID");
            var subFolderName = (String)detailGrid.GetMasterRowKeyValue();
            var pkgPath = (String)detailGrid.GetMasterRowFieldValues("PackagePath");
            var versionMatchingtbl = PackageBLL.SelectJpPackageSubFolderFiles(pkgId, subFolderName, pkgPath, SiteUtility.BrowserLang_GetFromApp());
            SetDownloadPaths(detailGrid, versionMatchingtbl);
        }

        protected void gvJPPackageFiles_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var pkgId = (long)detailGrid.GetMasterRowKeyValue();
            var versionMatchingtbl = PackageBLL.SelectJpPackageSubFolderFiles(pkgId, "", "", SiteUtility.BrowserLang_GetFromApp());
            SetDownloadPaths(detailGrid, versionMatchingtbl);
        }

        protected void gvJpPackageSubFolders_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var packageId = (long)detailGrid.GetMasterRowKeyValue();

            var jpPkgSubFolder = PackageBLL.SelectJpPackageSubFoldersByPackageId(packageId);
            detailGrid.DataSource = jpPkgSubFolder;
        }

        protected void gvJpPackages_DetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                //create a seesion to store exp date 
                var expate = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "ExpiryDate");
                //Add 31 hours to support PST time zone
                if (expate != DBNull.Value && Convert.ToDateTime(expate).AddHours(31) <= DateTime.UtcNow)
                    Session["DisableDownloadslink"] = false;
                else
                    Session["DisableDownloadslink"] = true;
                var gvJpPackageFiles = ((ASPxGridView)sender).FindDetailRowTemplateControl(e.VisibleIndex, "gvJPPackageFiles") as ASPxGridView;
                var gvJpPackageSubFolders = ((ASPxGridView)sender).FindDetailRowTemplateControl(e.VisibleIndex, "gvJpPackageSubFolders") as ASPxGridView;
                var hasFolders = (int)((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "HasFolders");
                if (gvJpPackageFiles != null && gvJpPackageSubFolders != null)
                {
                    gvJpPackageFiles.Visible = !(hasFolders > 0);
                    gvJpPackageSubFolders.Visible = (hasFolders > 0);
                }
            }
        }
        #endregion

        public bool ShowIcon(object showIcon)
        {
            bool show = string.IsNullOrEmpty(Convert.ToString(showIcon)) ? false : true;
            return show;
        }
        public string ShowImageUrl(string Imgtype)
        {
            if (Imgtype.Equals("New"))
            {
                return ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/New.png";
            }
            else if (Imgtype.Equals("Update")) { return ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/Updated.png"; }
            return string.Empty;
        }
    }
}