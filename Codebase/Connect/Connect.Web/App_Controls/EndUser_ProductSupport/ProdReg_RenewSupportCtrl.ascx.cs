﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.ProductRegistration;
namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class ProdReg_RenewSupportCtrl : DownloadResourceCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        public int PrId
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ProdRegItemID], 0);
            }
        }

        public int EId
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.EntryId], 0);
            }
        }

        public string SupportTypecode
        {
            get
            {
                return ConvertUtility.ConvertToString(Request.QueryString[KeyDef.QSKeys.SupportTypeCode], string.Empty);
            }
        }
        private Boolean RequestSentToAdmin
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState["vs_PaidSupportRenewSent"], false); }
            set { ViewState["vs_PaidSupportRenewSent"] = value; }
        }

        private Int32 ProdRegItemID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ProdRegItemID].ConvertNullToEmptyString(), 0);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            ProdReg_SessionData data = ProdRegInfo;
            ProductConfig pcfg = ProductConfig;

            if (!Page.IsPostBack)
            {
                InitProductInfo(data, pcfg);
            }
            LoadAddOnFormForRenew(data, pcfg);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    InitProductInfo();
            //}
        }

        private void InitProductInfo(ProdReg_SessionData data, ProductConfig pcfg)
        {
            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            var renewItem = from i in data.RegItems
                            where i.ProdRegItemID == ProdRegItemID
                            select i;

            ProdReg_Item itemToRenew = renewItem.Any() ? renewItem.First() : null;
            //if (data == null || acc == null || user == null || data.RegItems == null || data.RegItems.Count < 1 || data.RegInfo == null || pcfg == null)
            //{
            //    throw new HttpException(404, "Page not found.");
            //}
            pnlCompanyInfo.HeaderText += String.Format("&nbsp;&nbsp;&nbsp;&nbsp;<a class='smalllink' href='/myaccount/companyprofile?{0}={1}' >{2}</a>"
               , KeyDef.QSKeys.ReturnURL
               , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}&{3}={4}"
                 , KeyDef.UrlList.ProductSupportRenew
                 , KeyDef.QSKeys.RegisteredProductWebAccessKey
                 , WebToken.ToString()
                 , KeyDef.QSKeys.ProdRegItemID
                                , PrId))
               , this.GetGlobalResourceObject("STCTRL_ProdReg_RenewSupportCtrl", "hlEditCompanyInfo.Text")
               );

            //Fix:  MA-1377 Remove Google translate api
            
            ltrUserFullName.Text = user.LastName + "," + user.FirstName;

            ltrCompanyInfo.Text = CompanyProfileWzCtrl.GetReadOnlyCompanyInfo(acc.AccountID, false, false, true);

            // ltrUserFullName.Text = user.FullName;
            ltrUserEmail.Text = user.Email;

            ltrMN.Text = pcfg.ModelNumber;
            ltrMN.Text += String.Format(" [{0}]", itemToRenew != null ? itemToRenew.ModelNumber : string.Empty);
            ltrSN.Text = itemToRenew != null ? itemToRenew.SerialNumber : string.Empty;
            if (itemToRenew != null && itemToRenew.Config_JPSW != null && itemToRenew.Config_JPSW.JPSupportContract != DateTime.MinValue)
            {
                ltrSupportExpiredOn.Text = itemToRenew.Config_JPSW.JPSupportContract.ToString("d");
                divPaidSupportExp.Visible = true;
            }
            else
                divPaidSupportExp.Visible = false;

            //LoadAddOnFormForRenew(prdCfg, prodReg);

        }

        private void LoadAddOnFormForRenew(ProdReg_SessionData data, ProductConfig pcfg)
        {
            //if (data == null || data.RegItems == null || data.RegItems.Count < 1 || data.RegInfo.Count < 1  || data.RegInfo == null || pcfg == null || pcfg.Reg_AddOnFormID.IsNullOrEmptyGuid())
            //{
            //    return;
            //}
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            Guid addOnFormID = pcfg.Reg_AddOnFormID;
            Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(addOnFormID);
            if (formData == null || formData.FormID.IsNullOrEmptyGuid()) return;

            pnlRenewAddOnForm.HeaderText = GetStaticResource("RenewHeaderInfo");
            //this.GetGlobalResourceObject(String.Format("FRM_{0}", prdCfg.AddOnFormID.ToString().ToUpperInvariant()), "HeaderText").ToString();
            App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phRenewAddOnForm, formData, user);
            DataTable tbAddOnData = ProdReg_DataBLL.SelectRegisterDataByProdRegID(data.RegInfo[0].ProdRegID);
            if (tbAddOnData != null)
            {
                App_Features.DynamicForm.UILib.DynamicFormUtility.SetFormData(phRenewAddOnForm, tbAddOnData);
                pnlRenewAddOnForm.Visible = true;
            }
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            SubmitRenewRequest();
        }

        private void SubmitRenewRequest()
        {
            if (!Page.IsValid) return;

            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            ProdReg_SessionData data = ProdRegInfo;
            ProductConfig pcfg = ProductConfig;

            if (data == null || pcfg == null)
                throw new HttpException(404, "Page not found.");

            var renewItem = from i in data.RegItems
                            where i.ProdRegItemID == ProdRegItemID
                            select i;
            ProdReg_Item itemToRenew = renewItem.First();

            if (itemToRenew == null)
                throw new HttpException(404, "Page not found.");

            List<KeyValuePair<String, String>> addOnFormData = CaptureAddOnFormData();
            //MA-1395
            //String mnToRenew = Request.QueryString[KeyDef.QSKeys.ModelNumber].ConvertNullToEmptyString().Trim();
            //String mnToRenew = ConvertUtility.ConvertNullToEmptyString(Session[KeyDef.SSKeys.Selected_ModelNumber]).Trim();
            //if (string.IsNullOrWhiteSpace(mnToRenew))
            //    Response.Redirect(KeyDef.UrlList.MyProducts);
            String userIP = WebUtility.GetUserIP();
            String geoCountryCode = String.Empty;
            AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi != null) geoCountryCode = gi.CountryCode;

            if (!RequestSentToAdmin)
            {

                Boolean emailSent = false;
                String msg = String.Empty;
                if(data.RegInfo!=null && data.RegInfo[0]!=null)
                RequestSentToAdmin = EndUser_ProductRegUtility.EndUser_SubmitSupportRenewal(user, acc, data.RegInfo[0], itemToRenew, pcfg, addOnFormData
                                           , SiteUtility.BrowserLang_GetGlobalWebCultureGroupId(), out emailSent, out msg);
                if (RequestSentToAdmin)
                {
                    ProdReg_DataBLL.RenewSubmit(EId, SupportTypecode);
                }

                if (!emailSent) throw new ArgumentException("Unable to send email - " + msg);
                if (!RequestSentToAdmin) throw new ArgumentException("ERROR: " + msg);
            }

            if (RequestSentToAdmin)
            {
                String url = String.Format("{0}", KeyDef.UrlList.ProductSupportRenewAck);
                WebUtility.HttpRedirect(this, url);
            }
        }

        private List<KeyValuePair<String, String>> CaptureAddOnFormData()
        {
            #region " capture data "

            List<KeyValuePair<string, string>> formData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(phRenewAddOnForm);
            if (formData != null)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (KeyValuePair<string, string> kv in formData)
                {
                    if (kv.Key.IsNullOrEmptyString()) continue;
                    if (!data.ContainsKey(kv.Key.Trim()))
                    {
                        data.Add(kv.Key.Trim(), kv.Value.Trim());
                    }
                }
            }

            return formData;
            #endregion
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_RenewSupportCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey) as String;
        }


    }
}