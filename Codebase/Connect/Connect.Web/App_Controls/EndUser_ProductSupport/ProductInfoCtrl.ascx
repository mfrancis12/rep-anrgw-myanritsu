﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductInfoCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductInfoCtrl" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<div class="settingrow my-anristu my-anristu-support">
    <div id="dialogBlock" style="display:none">
<div id="container">
    <h3><asp:Literal ID="h3title" runat="server" Text="<%$ Resources:STCTRL_ProdSupport_BasicInfo,dlgAssistantMsg.Title%>" /> </h3>
    <p><asp:Literal ID="pmsg" runat="server" Text="<%$ Resources:STCTRL_ProdSupport_BasicInfo,dlgAssistantMsg.Text%>" /></p>
        
<a id="closeDialogBtn">×</a>
</div>
</div>
    <table class="topbox" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tbody>
            <tr>
                <td class="topbox1" rowspan="2" style="width:200px;">
                    <div class="topbox1header">
                        <img alt="" src="<%=ConfigKeys.GwdataCdnPath %>/images/legacy-images/images/wdwhdr03l.gif" />

                    </div>
                    <div class="topbox1body">
                        <a id="hrefProductImage" runat="server" rel="prettyPhoto">
                        <asp:Image ID="imgProdImage" runat="server" CssClass="topbox1image"
                             AlternateText="" />
                        </a>
                        <p>
                            <asp:HyperLink ID="hlProdInfo" runat="server" Target="_blank"
                                EnableTheming="false" Text='<%$ Resources:STCTRL_ProdSupport_BasicInfo,hlProdInfo.Text %>'
                                NavigateUrl="https://www.anritsu.com" Visible="false"></asp:HyperLink></p>
                    </div>
                </td>
                <td class="topbox1titletext" colspan="3">
                    <div>
                        <asp:Literal ID="ltrProductName" runat="server"></asp:Literal>
                    </div>
                </td>
                <td class="topbox1titleright"><div></div></td>
            </tr>
            <tr>
                <td class="topbox1text2" colspan="3">
                    <div class="settingrow">
                        <span class="custom-select-lable"><asp:Localize ID="lcalSN1" runat="server" Text='<%$ Resources:STCTRL_ProdSupport_BasicInfo,lcalSN.Text %>'></asp:Localize></span>
                         <asp:DropDownList runat="server" ID="ddlSerialNumbers" AutoPostBack="True"  CssClass="custom-select" EnableTheming="false" />
                    
                       <span style="padding-left:20px;position: relative;"> <asp:Button Visible="false"  runat="server" style="padding-top: 5px;padding-bottom:5px;padding-right: 15px;padding-left: 15px;" ToolTip=""  ID="Startdownload" Text='<%$ Resources:STCTRL_ProdSupport_BasicInfo,lcaldwlassistant %>' OnClick="Startdownload_Click" ></asp:Button>
                           <span Visible="false" id="WhatIsThis" runat="server"><span><asp:Literal ID="assistantooltip" runat="server" Text="<%$ Resources:STCTRL_ProdSupport_BasicInfo,lcaldwlassistanttooltip%>" /></span> </span></span>                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                            <span style="padding-left:20px;">
                                <asp:HyperLink  runat="server" Visible="false" ID="Assistantdownloadstatus" Target="_self" Text='<%$ Resources:STCTRL_ProdSupport_BasicInfo,lcaldwlassistantstatus %>'  SkinID="blueit" NavigateUrl="/myproduct/AssistantDownloadStatus"></asp:HyperLink></span>
                        
                    </div>
                    <div class="settingrow">
                        <span class="settinglabelplain">
                            <asp:Localize ID="lcalStatus" Visible="true" runat="server" Text='<%$Resources: STCTRL_ProdSupport_BasicInfo,lcalStatus.Text%>'></asp:Localize></span>
                        
                        <span class="settinglabelplain-active"><asp:Literal ID="ltrStatus" runat="server"></asp:Literal></span>
                         <span style="padding-left:100px;display:inline-block"  class="server-error-message"><asp:Literal Visible="false"   ID="ltrdownloadstatus" runat="server"></asp:Literal></span>
                    </div>
                    <div class="settingrow">
                        <asp:DataList ID="dlProdSupportLinks" runat="server" RepeatColumns="5" RepeatDirection="Horizontal"
                            RepeatLayout="Table" CellPadding="10" CellSpacing="5">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlProdSupportLink" runat="server" Target='<%# DataBinder.Eval(Container.DataItem, "LnkTarget") %>'
                                    Text='<%# GetLinkText( DataBinder.Eval(Container.DataItem, "LnkID"), "Text") %>'
                                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "TargetUrl") %>' SkinID="blueit"></asp:HyperLink></ItemTemplate>
                            <SeparatorTemplate>
                                &nbsp;&nbsp;&nbsp;&nbsp;</SeparatorTemplate>
                        </asp:DataList>
                    </div>
                    <div id="txtContent" runat="server" class="settingCustomText" style="background-color: #F5F5F5!Important">
                    </div>
                    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdSupport_BasicInfo" />
                </td>
                <td class="imagecellright">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="topbox1footerleft">
                    <img alt="" src="<%=ConfigKeys.GwdataCdnPath %>/images/legacy-images/images/wdwftrl.gif" />
                </td>
                <td class="topbox1footercenter" colspan="3">
                    &nbsp;
                </td>
                <td class="topbox1footerright">
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script  type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/bootstrap-select.js"></script>
<script>
    $(document).ready(function () {
        $(".my-anristu-support").parents(".rightContent").addClass("myanritsu-support-wrapper");
        $('.custom-select').selectpicker('refresh');
        $(document).on("click", ".bootstrap-select > .dropdown-toggle", function (e) {
            e.stopPropagation();
            $(this).parent(".bootstrap-select").find(".dropdown-menu").toggle();
        });
        $(document).click(function () {
            $(".dropdown-menu").hide();
        });
    });
    </script>