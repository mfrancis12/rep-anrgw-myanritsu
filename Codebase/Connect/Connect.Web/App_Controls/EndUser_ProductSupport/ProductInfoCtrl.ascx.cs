﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.Content;
using Anritsu.Connect.Lib.CustomLink;
using Anritsu.Connect.Lib.Product;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Package;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Anritsu.Connect.Lib.AWS;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class ProductInfoCtrl : DownloadResourceCtrlBase, IStaticLocalizedCtrl
    {
        string accCultureGroup = "en-US";
        int cultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
        int accCultureGroupId = 1;

        private bool ShowAgreement
        {
            get
            {
                if (Session != null && Session[KeyDef.SSKeys.PurchasedSupport_AgreementNotice] != null) return false;
                else return true;
            }
        }
        private void ProcessAutoDownload2()
        {
            //TAU Download Files
            var user = LoginUtility.GetCurrentUserOrSignIn();
            Downloads_PrivateDataSource privatedatasource = new Downloads_PrivateDataSource();
            DataTable dttaupkglist= privatedatasource.SelectTAUPackagesByAccountID(AccountUtility.GenerateUserTeamIds(), ModelNumber, false, SerialNumber);
            DataTable dttaupkgfiles = new DataTable();
            DataTable dtautodownload = CreateAutoDownloadCSVDataTable();
            foreach (DataRow pkgidrow in dttaupkglist.Rows)
            {
                dttaupkgfiles.Merge(privatedatasource.SelectTAUPackageFilesByPackageID(long.Parse(pkgidrow["PackageID"].ToString())));
            }

            foreach(DataRow plkidrow in dttaupkgfiles.Rows)
            {
                DataRow drow = dtautodownload.NewRow();
                drow["EmailAddress"] = user.Email;
                drow["SerialNumber"] = SerialNumber;
                drow["ModelNumber"] = ModelNumber;
                string encodedurl = Uri.EscapeUriString(plkidrow["FilePath"].ToString());
                plkidrow["FilePath"] = AwsS3CdnUtility.GenerateTauCdnUrl(ConvertUtility.ConvertToString(encodedurl, string.Empty), 200000); 
                drow["FilePath"] = plkidrow["FilePath"].ToString();
                dtautodownload.Rows.Add(drow);
            }

            //JP Package Files
            DataTable dtjppkglist = new Downloads_PrivateDataSource().SelectJpPackageGroups(ModelNumber,
               SerialNumber, LoginUtility.GetCurrentUser(true).MembershipId, PackageType.Download,
               ModelConfigTypeInfo.CONFIGTYPE_JP, "", AccountUtility.GenerateUserTeamIds(true));

            if (dtjppkglist != null && dtjppkglist.Rows.Count > 0)
            {
                DataTable dtjppkgfiles = new DataTable();

                foreach (DataRow pkgidrow in dtjppkglist.Rows)
                {
                    dtjppkgfiles.Merge(privatedatasource.SelectTAUPackageFilesByPackageID(long.Parse(pkgidrow["PackageID"].ToString())));
                }

                foreach (DataRow plkidrow in dtjppkgfiles.Rows)
                {
                    DataRow drow = dtautodownload.NewRow();
                    drow["EmailAddress"] = user.Email;
                    drow["SerialNumber"] = SerialNumber;
                    drow["ModelNumber"] = ModelNumber;
                    string encodedurl = Uri.EscapeUriString(plkidrow["FilePath"].ToString());
                    plkidrow["FilePath"] = AwsS3CdnUtility.GenerateJpCdnUrl(ConvertUtility.ConvertToString(encodedurl, string.Empty), 200000);
                    drow["FilePath"] = plkidrow["FilePath"].ToString();
                    dtautodownload.Rows.Add(drow);
                }
            }


            if (dtautodownload != null && dtautodownload.Rows.Count > 0)
            {

                byte[] bytes = Encoding.ASCII.GetBytes(ConvertDataTableToCSV(dtautodownload).ToString());
                AWSUtility_S3 s3 = new AWSUtility_S3("");
                string filename = ModelNumber + "-" + SerialNumber.Substring(SerialNumber.Length - 4, 4) + ".csv";
                filename = "ToAssistant/" + filename;
                s3.AWS_S3_UploadFile(bytes, filename, filename, "csv");
            }
        }

        private DataTable CreateAutoDownloadCSVDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Email Address");
            dt.Columns.Add("SerialNumber");
            dt.Columns.Add("ModelNumber");
            dt.Columns.Add("FilePath");

            return dt;
        }
        public void DisplayDownloadButton()
        {

            if (ShowAgreement == false)
            {
                if (ActiveTab != "")
                {
                    //Startdownload Button
                    Startdownload.Visible = Assistantdownloadstatus.Visible = false;
                    if (PackageBLL.ShowAutoDownloadInfoByModelNumber(ModelNumber))
                    {

                        var privSrc = new Downloads_PrivateDataSource();
                        var ds = new ProdReg_DataSource();
                        var prodRegInfo = ds.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber, SerialNumber);
                        var hasTauDownloadPackages = privSrc.TAUDownloads_HasData(ModelNumber, AccountUtility.GenerateUserTeamIds(), prodRegInfo, SerialNumber);

                        bool hasJpVersionMatchingPackages, hasJpLicensePackages, hasJpDownloadPackages;

                        privSrc.JpDownloadsHasPackages(ModelNumber, SerialNumber, out hasJpDownloadPackages, out hasJpLicensePackages,
                            out hasJpVersionMatchingPackages);

                        ltrdownloadstatus.Visible = false;
                        WhatIsThis.Visible = Startdownload.Visible = Assistantdownloadstatus.Visible = (hasTauDownloadPackages || hasJpLicensePackages || hasJpDownloadPackages);
                    }
                }
            }
            
           
        }
        private void ProcessAutoDownload()
        {
            DataTable dt = new DataTable();
            DataTable jpdownloadsdt = null;
            var user = LoginUtility.GetCurrentUserOrSignIn();
            jpdownloadsdt = PackageBLL.GetAutoDownloadPackageFiles(ModelNumber, SerialNumber, user.Email, "downloads-jp", AccountUtility.GenerateUserTeamIds());
            if (jpdownloadsdt != null && jpdownloadsdt.Rows.Count > 0)
            {
                foreach (DataRow dr in jpdownloadsdt.Rows)
                {
                    if (dr["FilePath"] != null && dr["FilePath"].ToString() != "")
                    {
                        string encodedurl = Uri.EscapeUriString(dr["FilePath"].ToString());
                        dr["FilePath"] = AwsS3CdnUtility.GenerateJpCdnUrl(ConvertUtility.ConvertToString(encodedurl, string.Empty), 7); 
                    }
                }
                dt.Merge(jpdownloadsdt);
            }
            DataTable ukdownloadsdt = null;
            ukdownloadsdt = PackageBLL.GetAutoDownloadPackageFiles(ModelNumber, SerialNumber, user.Email, "downloads-uk", AccountUtility.GenerateUserTeamIds());

            if (ukdownloadsdt != null && ukdownloadsdt.Rows.Count > 0)
            {
                foreach (DataRow dr in ukdownloadsdt.Rows)
                {
                    if (dr["FilePath"] != null && dr["FilePath"].ToString() != "")
                    {
                        string encodedurl = Uri.EscapeUriString(dr["FilePath"].ToString());
                        
                        //string encodedurl = System.Web.HttpUtility.UrlEncode(dr["FilePath"].ToString());
                        // string encodedurl = System.Net.WebUtility.UrlEncode(dr["FilePath"].ToString());
                        dr["FilePath"] = AwsS3CdnUtility.GenerateTauCdnUrl(ConvertUtility.ConvertToString(encodedurl,string.Empty), 7);
                    }
                }
                dt.Merge(ukdownloadsdt);
            }

            if (dt != null && dt.Rows.Count > 0)
            {

                byte[] bytes = Encoding.ASCII.GetBytes(ConvertDataTableToCSV(dt).ToString());
                AWSUtility_S3 s3 = new AWSUtility_S3("");
                string filename = ModelNumber + "-" + SerialNumber.Substring(SerialNumber.Length - 4, 4) + ".csv";
             
                s3.AWS_S3_UploadFile(bytes, filename, filename, "csv");
                ltrdownloadstatus.Visible = true;
                string downloadbuttonText = GetGlobalResourceObject("STCTRL_ProdSupport_BasicInfo", "lcaldwlassistant").ToString();// STCTRL_ProdSupport_BasicInfo,lcaldwlassistant
                ltrdownloadstatus.Text = GetGlobalResourceObject("STCTRL_ProdSupport_BasicInfo", "lcaldwlassistantstatusmsg").ToString().Replace("{MN}",ModelNumber).Replace("{SN}",SerialNumber).Replace("{DABUTTON}", downloadbuttonText);
            }
            else
            {
                throw new Exception("There are Errors on System!");

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            BLLIso_Country.GetCultureGroupInfoByCountry(SelectedAccount.CompanyCountryCode, out accCultureGroupId, out accCultureGroup);
            
            // if (!Page.IsPostBack)
            //{
            BindProductInfo();
            //}
            Session["ModelNumber"] = ModelNumber;
            Session["SerialNumber"] = SerialNumber;
           

           
             DisplayDownloadButton();
            
            //
            
            Bind_Links(SelectedAccount, ddlSerialNumbers.SelectedValue, accCultureGroup);
           
            //hold the dropdown list client Id in context
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[KeyDef.SSKeys.ddlSerialNumberId] == null)
                HttpContext.Current.Session[KeyDef.SSKeys.ddlSerialNumberId] = ddlSerialNumbers.UniqueID;
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProductInfoCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        private void BindProductInfo()
        {
            var user = LoginUtility.GetCurrentUserOrSignIn();

            if (ProdRegInfo == null || ProductConfig == null || SelectedAccount == null || user == null)
            {
                Visible = false;
                return;
            }
            Bind_ProductBasicInfo(ProdRegInfo, ProductConfig, cultureGroupID);
            //TODO
            //Bind_JPDownloadInfo(acc, data, pcfg);
            Bind_CustomText();
        }

        private void Bind_ProductBasicInfo(
            ProdReg_SessionData data,
            ProductConfig pcfg,
            int cultureGroupId)
        {
            var mnUpperCase = ModelNumber.ToUpperInvariant();
            var productName = mnUpperCase;
            imgProdImage.AlternateText = mnUpperCase;

            //get the list of serial numbers registred in all assigned teams
            if (!IsPostBack)
            {
                ddlSerialNumbers.DataSource = data.RegInfo;
                ddlSerialNumbers.DataTextField = ddlSerialNumbers.DataValueField = "MasterSerial";
                ddlSerialNumbers.DataBind();
                //MA-1395
                ddlSerialNumbers.SelectedValue = SerialNumber;
            }
            if (ddlSerialNumbers.SelectedValue != null)
            {
                //set the registration status
                var activeStatusItem =
                    data.RegItems.FirstOrDefault(reg => reg.StatusCode == ProdReg_ItemStatus.active && reg.SerialNumber == ddlSerialNumbers.SelectedValue);
                ltrStatus.Text = ConvertUtility.ConvertToString(activeStatusItem != null ?
                    GetGlobalResourceObject("common", ProdReg_ItemStatus.active.ToString()) :
                    GetGlobalResourceObject("common", ProdReg_ItemStatus.submitted.ToString()), string.Empty);
                lcalStatus.Visible = ltrStatus.Text.Length > 0;
            }

            var pBasicInfoForCustomer = ProductInfoBLL.GetProductInfo(cultureGroupId, pcfg, SerialNumber);
            var prodInfoNotFoundForCustomerRegion = (pBasicInfoForCustomer == null);
            var productImageUrl = ProductInfo.ProductDefaultImageUrl;
            if (pBasicInfoForCustomer != null)
            {
                productName = pBasicInfoForCustomer.ProductName + "&nbsp;&nbsp;(" + mnUpperCase + ")";

                if (!pBasicInfoForCustomer.ProductImageURL.IsNullOrEmptyString()) productImageUrl = pBasicInfoForCustomer.ProductImageURL;
                else if (!pBasicInfoForCustomer.ProductSmallImageURL.IsNullOrEmptyString()) productImageUrl = pBasicInfoForCustomer.ProductSmallImageURL;

                if (!pBasicInfoForCustomer.ProductPageURL.IsNullOrEmptyString() && !prodInfoNotFoundForCustomerRegion)
                {
                    hlProdInfo.NavigateUrl = pBasicInfoForCustomer.ProductPageURL;
                    hlProdInfo.Visible = true;
                }
            }

            if (!string.IsNullOrEmpty(pcfg.CustomImageUrl)) productImageUrl = pcfg.CustomImageUrl;

            imgProdImage.ImageUrl = productImageUrl;
            hrefProductImage.HRef = productImageUrl;

            ltrProductName.Text = productName;
            var pg = (BP_MyAnritsu)this.Page;
            var pageTitle = string.Format("{0} - {1}", pg.GetPageTitleFromResource(), mnUpperCase);
            pg.SetPageTitle(pageTitle);
        }

        private void Bind_Links(
            Acc_AccountMaster acc,
           string SerialNumber,
            string accCultureGroup)
        {

            var mnUpperCase = ModelNumber.ToUpperInvariant();
            #region " links "
            var pageUrl = KeyDef.UrlList.ProductSupport;

            var tbLinks = CustomLinkDisplayBLL.Select(pageUrl,
                "PRDSUPRT",
                accCultureGroup,
                mnUpperCase,
                acc.AccountID);
            DataView dvLinks = null;
            if (tbLinks != null)
            {
                foreach (DataRow r in tbLinks.Rows)
                {
                    string targetUrl = r["TargetUrl"].ToString().Replace("[[MN]]", mnUpperCase)
                        .Replace("[[SN]]", SerialNumber)
                        .Replace("[[LLCC]]", accCultureGroup);
                    r["TargetUrl"] = targetUrl;
                }
                tbLinks.AcceptChanges();
                dvLinks = tbLinks.DefaultView;
                dvLinks.Sort = "LoadOrder ASC";
            }
            dlProdSupportLinks.DataSource = dvLinks;
            dlProdSupportLinks.DataBind();
            #endregion
        }
        protected override void OnInit(EventArgs e)
        {
            imgProdImage.ImageUrl = ConfigKeys.GwdataCdnPath +
                                   "/images/legacy-images/images/gwstyle/NoImage.png";
        }
        private void Bind_JPDownloadInfo(Acc_AccountMaster acc,
            ProdReg_SessionData data,
            ProductConfig pcfg)
        {
            if (acc == null || data == null || data.RegInfo == null || pcfg == null) return;
            foreach (ProdReg_Item prItem in data.RegItems)
            {
                if (prItem.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP)
                {
                    ltrStatus.Text = ConvertUtility.ConvertToString(GetGlobalResourceObject("common", prItem.StatusCode.ToString()), String.Empty);
                    lcalStatus.Visible = ltrStatus.Text.Length > 0;
                    return;
                }
                else
                {
                    ltrStatus.Text = ConvertUtility.ConvertToString(GetGlobalResourceObject("common", prItem.StatusCode.ToString()), String.Empty);
                    lcalStatus.Visible = ltrStatus.Text.Length > 0;
                }
            }
        }


        private void Bind_CustomText()
        {
            #region custom text

            var culture = CultureInfo.CurrentUICulture;
            if (String.IsNullOrEmpty(culture.Name)) culture = new CultureInfo("en");

            var resKey = string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, ModelNumber);
            Res_ContentStore content = Res_ContentStoreBLL.Select(KeyDef.ResClassKeys.ProductCfgClassKey, resKey, culture);

            txtContent.InnerHtml = content == null ? String.Empty : HttpUtility.HtmlDecode(content.ContentStr);
            txtContent.Visible = txtContent.InnerHtml == String.Empty ? false : true;


            #endregion
        }

        protected string GetLinkText(Object linkID, Object resKey)
        {
            String classKey = String.Format("STLNK_{0}", linkID.ToString());
            return this.GetGlobalResourceObject(classKey, resKey.ToString()).ToString();
        }

        private StringBuilder ConvertDataTableToCSV(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            //IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
            //                                  Select(column => column.ColumnName);
            //sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }
            return sb;
            //File.WriteAllText(@"C:\Users\am-c0959\Downloads\AutodownloadCSV\test.csv", sb.ToString());
        }

        protected void Startdownload_Click(object sender, EventArgs e)
        {
            try
            {
               // ProcessAutoDownload2();
               ProcessAutoDownload();
               

            }
            catch(Exception ex)
            {
                ltrdownloadstatus.Visible = true;
                ltrdownloadstatus.Text = "There are errors on system! Please contact System administrator" + ex.Message;
            }
        }

     
        protected void Assistantdownloadstatus_Click(object sender, EventArgs e)
        {
            try
            {
              

                Response.Redirect("AssistantDownloadStatus.aspx", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}