﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSupportTabCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductSupportTabCtrl" %>
<dx:ASPxTabControl ID="dxTabLevel1" runat="server" Width="100%" Theme="AnritsuDevXTheme" TabAlign="Left" ActiveTabIndex="0" >
    <Tabs >
        <dx:Tab Name="tbPublic" Visible="true" Text="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbPublic.Text %>"  ></dx:Tab>        
        <dx:Tab Name="tbPrivate" Visible="true" Text="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbPrivate.Text %>" ToolTip="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbPrivateTooltip.Text %>"  ></dx:Tab>        
        <dx:Tab Name="tbWarranty" Visible="false" Text="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbWarranty.Text %>" ToolTip="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbWarrantyTooltip.Text %>"  ></dx:Tab>
        <dx:Tab Name="tbCalCert" Visible="false" Text="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbCalCert.Text %>" ToolTip="<%$ Resources:STCTRL_ProductSupportTabCtrl,tbCalCertTooltip.Text %>"  ></dx:Tab>
    </Tabs>
    <Paddings PaddingBottom="0" />    
</dx:ASPxTabControl>