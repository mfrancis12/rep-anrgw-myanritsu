﻿using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class ProductSupportTabCtrl : DownloadResourceCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public DevExpress.Web.ASPxTabControl ProductSupportTabs { get { return dxTabLevel1; } }
        public String ActTab
        {
            get
            {
                return ConvertUtility.ConvertToString(Request.QueryString[KeyDef.QSKeys.ActTab], String.Empty);
            }
        }
        public String ResTab
        {
            get
            {
                return ConvertUtility.ConvertToString(Request.QueryString["resource"], String.Empty);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (ActTab.Equals("1"))
            { 
                Tab tbPrivate = dxTabLevel1.Tabs.FindByName("tbPrivate");
                if (tbPrivate.Visible)
                {
                    dxTabLevel1.ActiveTab = tbPrivate;
                }
             //   Response.Redirect(string.Format("{0}&act={1}", tbPrivate.NavigateUrl, "pvt"));
            }
            if (ResTab.Equals("1"))
            {
                Tab tbpublic = dxTabLevel1.Tabs.FindByName("tbPublic");
                if (tbpublic.Visible)
                {
                    dxTabLevel1.ActiveTab = tbpublic;
                }
                //   Response.Redirect(string.Format("{0}&act={1}", tbPrivate.NavigateUrl, "pvt"));
            }

           
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}