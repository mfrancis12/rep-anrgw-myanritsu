﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PublicResourcesCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PublicResourcesCtrl" %>
<div style="padding: 10px;">
    <script type="text/javascript" language="javascript">
        function OnClick(rowIndex, grid) {
            if (grid.IsGroupRowExpanded(rowIndex))
                grid.CollapseRow(rowIndex);
            else
                grid.ExpandRow(rowIndex);
        }
    </script>
    <div class="settingrow" id="lcalMessageDiv" runat="server" style="display: none">
        <p style="color: #777; text-align: center; font-size: 12px; border: 1px solid #446D8D; padding: 20px 0; background: #FFF;">
            <asp:Localize ID="lcalMessage" runat="server" Text="<%$ Resources:common,NoDataMsg%>"></asp:Localize>
        </p>
    </div>
    <dx:ASPxPageControl ID="dxpcEndUserSupportPublic" runat="server" EnableCallBacks="true" Width="100%" Theme="AnritsuDevXTheme" RenderMode="Lightweight" TabAlign="Left">
        <TabPages>
            <dx:TabPage Name="tabSW" Text="<%$ Resources:STCTRL_PublicResourcesCtrl,tabSW.Text %>">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <dx:ASPxGridView ID="dxgvEndUserSupportPublicSW" runat="server" ClientInstanceName="dxgvEndUserSupportPublicSW"
                            DataSourceID="odsProdSupportPubResc" KeyFieldName="DownloadID" OnBeforePerformDataSelect="dxgvEndUserSupportPublicSW_BeforePerformDataSelect"
                            Width="100%" Theme="AnritsuDevXTheme" OnHtmlRowCreated="dxgvEndUserSupportPublicSW_HtmlRowCreated">
                            <Settings ShowGroupPanel="false" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <%--   <SettingsBehavior AutoExpandAllGroups="false" />--%>
                            <Columns>
                                <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$ Resources:STCTRL_PublicResourcesCtrl,ModelNumber.Text %>" VisibleIndex="1" GroupIndex="1"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Title" Caption=" " VisibleIndex="2" Width="440px">
                                    <DataItemTemplate>
                                        <div class="settingrow"><%# Eval("Title") %></div>
                                        <div class="settingrow" style="font-size: 11px;">
                                            <asp:Literal ID="ltrDownloadProperties" runat="server"></asp:Literal>
                                        </div>

                                        <div class="overflow link-details-wrapper">
                                            <span class="left detailed-copy link-details">
                                                <asp:Localize ID="lcalReleaseDateDrivers" runat="server" Text='<%$ Resources:STCTRL_PublicResourcesCtrl,ReleaseDateUTC.Text %>'></asp:Localize>: <%# Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString()  %></span>

                                        </div>


                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                                <%-- <dx:GridViewDataDateColumn FieldName="ReleaseDate" Caption="<%$ Resources:STCTRL_PublicResourcesCtrl,ReleaseDateUTC.Text %>" VisibleIndex="5" Width="60px" CellStyle-CssClass="tablecell" CellStyle-HorizontalAlign="center">
                                </dx:GridViewDataDateColumn>--%>
                                <dx:GridViewDataColumn FieldName="FullDownloadURL" VisibleIndex="500" Caption=" " Width="100px" CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                        <div class="settingrow">
                                            <dx:ASPxHyperLink ID="dxhlDownload" SkinID="blueit" runat="server" Text="<%$ Resources:STCTRL_PublicResourcesCtrl,dxhlDownload.Text %>" Target="_blank" NavigateUrl='<%# Eval("DownloadPath") %>'></dx:ASPxHyperLink>
                                        </div>
                                        <div class="overflow link-details-wrapper">

                                            <span class="detailed-copy" runat="server" visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?true:false %>'>
                                                <asp:Localize ID="lcalFileSizeDrivers" runat="server" Text='<%$Resources:STCTRL_PublicResourcesCtrl,ltrDownloadProperties.Text.FileSizeLabel %>' Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?true:false %>'></asp:Localize><asp:Localize ID="Localize2" runat="server" Text=":" Visible='<%# string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?false:true %>'></asp:Localize><%# Eval("Size") %></span>
                                        </div>
                                        <%--<div class="settingrow">
                                            <span class='dlbl'>
                                                <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PublicResourcesCtrl,ltrDownloadProperties.Text.FileSizeLabel %>'></asp:Localize>: <%# Eval("Size") %></span>
                                        </div>--%>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                            </Columns>
                            <Templates>
                                <GroupRowContent>
                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text='<%# Container.GroupText %>' Cursor="pointer"
                                        OnLoad="ASPxLabel1_Load">
                                    </dx:ASPxLabel>
                                </GroupRowContent>
                            </Templates>
                            <Styles Row-Cursor="pointer"></Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabDoc" Text="<%$ Resources:STCTRL_PublicResourcesCtrl,tabDoc.Text %>">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server">
                        <dx:ASPxGridView ID="dxgvEndUserSupportPublicDoc" runat="server" ClientInstanceName="dxgvEndUserSupportPublicDoc"
                            DataSourceID="odsProdSupportPubResc" KeyFieldName="DownloadID" OnBeforePerformDataSelect="dxgvEndUserSupportPublicDoc_BeforePerformDataSelect"
                            Width="100%" Theme="AnritsuDevXTheme" OnHtmlRowCreated="dxgvEndUserSupportPublicDoc_HtmlRowCreated">
                            <Settings ShowGroupPanel="false" />
                            <SettingsPager PageSize="100"></SettingsPager>
                            <Columns>
                                <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$ Resources:STCTRL_PublicResourcesCtrl,ModelNumber.Text %>" VisibleIndex="1" GroupIndex="1"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="CategoryType" Caption="<%$ Resources:STCTRL_PublicResourcesCtrl,CategoryName.Text %>" VisibleIndex="2" GroupIndex="2"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn FieldName="Title" Caption=" " VisibleIndex="3" Width="440px">
                                    <DataItemTemplate>
                                        <div class="settingrow" style="text-align: left"><%# Eval("Title") %></div>

                                        <%--   <div class="settingrow" style="font-size:11px;">
                                            <asp:Literal ID="ltrDownloadProperties" runat="server"></asp:Literal>
                                        </div>--%>
                                        <div class="overflow link-details-wrapper">
                                            <span class="left detailed-copy link-details">
                                                <asp:Localize ID="lcalReleaseDate" runat="server" Text='<%$ Resources:STCTRL_PublicResourcesCtrl,ReleaseDateUTC.Text %>'></asp:Localize>: <%#Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString() %></span>
                                            <span class="left detailed-copy link-details" runat="server" visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("version")))?true:false %>'>
                                                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:STCTRL_PrivateResourcesCtrl,ltrDownloadProperties.Text.VersionLabel %>' Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("version")))?true:false %>'></asp:Localize><asp:Localize ID="Localize3" runat="server" Text=":" Visible='<%# string.IsNullOrEmpty(Convert.ToString(Eval("version")))?false:true %>'></asp:Localize>
                                                <%# Eval("version") %></span>
                                        </div>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                                <%--<dx:GridViewDataDateColumn FieldName="ReleaseDateUTC" Caption="<%$ Resources:STCTRL_PublicResourcesCtrl,ReleaseDateUTC.Text %>" VisibleIndex="5" Width="60px" CellStyle-CssClass="tablecell" CellStyle-HorizontalAlign="center">

                                </dx:GridViewDataDateColumn>--%>
                                <dx:GridViewDataColumn FieldName="FilePath" VisibleIndex="500" Caption=" " Width="100px" CellStyle-HorizontalAlign="Center">
                                    <DataItemTemplate>
                                        <div class="settingrow">
                                            <dx:ASPxHyperLink ID="dxhlDownload" SkinID="blueit" runat="server" test="test1" Text="<%$ Resources:STCTRL_PublicResourcesCtrl,dxhlDownload.Text %>" Target="_blank" NavigateUrl='<%# Eval("DownloadPath") %>'></dx:ASPxHyperLink>
                                        </div>
                                        <div class="overflow link-details-wrapper">
                                            <span class="detailed-copy" runat="server" visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?true:false %>'>
                                                <asp:Localize ID="lcalFileSize" runat="server" Text='<%$ Resources:STCTRL_PublicResourcesCtrl,ltrDownloadProperties.Text.FileSizeLabel %>' Visible='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?true:false %>'></asp:Localize><asp:Localize ID="Localize2" runat="server" Text=":" Visible='<%# string.IsNullOrEmpty(Convert.ToString(Eval("Size")))?false:true %>'></asp:Localize><%# Eval("Size") %></span>
                                        </div>
                                        <%--<div class="settingrow">
                                            <span class='dlbl'>
                                                <asp:Localize ID="lcalFileSize" runat="server" Text='<%$Resources:STCTRL_PublicResourcesCtrl,ltrDownloadProperties.Text.FileSizeLabel %>'></asp:Localize>: <%# Eval("SizeKB") %></span>
                                        </div>--%>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>
                            </Columns>
                            <Settings GroupFormat="{1}" />
                             <Templates>
                                <GroupRowContent>
                                    <dx:ASPxLabel ID="ASPxLabelDoc" runat="server" Text='<%# Container.GroupText %>' Cursor="pointer"
                                        OnLoad="ASPxLabel1_Load">
                                    </dx:ASPxLabel>
                                </GroupRowContent>
                            </Templates>
                            <Styles Row-Cursor="pointer"></Styles>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>

    <asp:ObjectDataSource ID="odsProdSupportPubResc" runat="server" TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PublicDataSource"
        SelectMethod="SelectByModelNumber" EnablePaging="false" EnableCaching="true" CacheExpirationPolicy="Sliding" CacheDuration="30">
        <SelectParameters>
            <%--MA-1395--%>
            <%--<asp:QueryStringParameter Name="modelNumber" QueryStringField="mn" DbType="String" />--%>
            <asp:SessionParameter Name="modelNumber" DbType="String" SessionField="mn" />
            <asp:SessionParameter Name="DocType" DbType="String" SessionField="odsEndUserSupportPublicDownloads_FilterMode" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_PublicResourcesCtrl" />
</div>
