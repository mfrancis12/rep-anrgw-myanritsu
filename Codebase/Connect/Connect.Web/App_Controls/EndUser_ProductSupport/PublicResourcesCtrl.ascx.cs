﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using DevExpress.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class PublicResourcesCtrl : DownloadResourceCtrlBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Cache[odsEndUserSupportPublic.CacheKeyDependency] = ProdRegWebAccessKey.ToString();
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                //dxgvEndUserSupportPublicDoc.ExpandRow(0);

                App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PublicDataSource ds =
                    new App_Lib.ObjectDataSources.EndUser_ProductSupport.Downloads_PublicDataSource();

                dxpcEndUserSupportPublic.TabPages.FindByName("tabSW").Visible = ds.HasDataForSoftwareDownloads(ModelNumber);
                dxpcEndUserSupportPublic.TabPages.FindByName("tabDoc").Visible = ds.HasDataForDocumentations(ModelNumber);

                if (dxpcEndUserSupportPublic.TabPages.FindByName("tabSW").Visible == false && dxpcEndUserSupportPublic.TabPages.FindByName("tabDoc").Visible == false)
                {
                    lcalMessageDiv.Style.Add("display", "block");
                }
            }

        }

        protected void ASPxLabel1_Load(object sender, EventArgs e)
        {
            ASPxLabel l = sender as ASPxLabel;
            GridViewGroupRowTemplateContainer c = l.NamingContainer as GridViewGroupRowTemplateContainer;
            if (l.ID.Equals("ASPxLabel1"))
                l.ClientSideEvents.Click = String.Format("function (s, e) {{ OnClick({0},{1}); }}", c.VisibleIndex, dxgvEndUserSupportPublicSW.ClientInstanceName);
            else
                l.ClientSideEvents.Click = String.Format("function (s, e) {{ OnClick({0},{1}); }}", c.VisibleIndex, dxgvEndUserSupportPublicDoc.ClientInstanceName);
        }

        protected void dxgvEndUserSupportPublic_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                ASPxGridView masterGrid = (ASPxGridView)sender;
                Literal ltr = masterGrid.FindRowCellTemplateControl(e.VisibleIndex
                    , masterGrid.Columns["ItemText"] as GridViewDataColumn
                    , "ltrProdRegItemText") as Literal;

                String model = ConvertUtility.ConvertNullToEmptyString(e.GetValue("ModelNumber"));
                String statusCode = ConvertUtility.ConvertNullToEmptyString(e.GetValue("StatusCode"));

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Model: <strong>{0}</strong>", model);

                //Boolean cfg_UserACLRequired = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("UserACLRequired"), false);
                //Boolean cfg_VerifyCompany = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("CompanyVerificationRequired"), false);
                //Boolean cfg_VerifyProductSN = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("RegistrationVerificationRequired"), false);
                //Boolean cfg_IsPaidSupportModel = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("IsPaidSupportModel"), false);
                //Boolean companyVerificationRequirementOK = !cfg_VerifyCompany || ((cfg_VerifyCompany && SelectedAccount.IsVerifiedAndApproved));

                ltr.Text = sb.ToString();
            }
        }

        protected void odsEndUserSupportPublicDownloads_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["GwCultureGroupID"] = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
        }

        protected void dxgvEndUserSupportPublicDoc_BeforePerformDataSelect(object sender, EventArgs e)
        {
            //ASPxGridView masterGrid = (ASPxGridView)sender;

            //String itemModelNumber = ConvertUtility.ConvertNullToEmptyString(masterGrid.GetMasterRowFieldValues("ModelNumber"));
            //String modelTags = ConvertUtility.ConvertNullToEmptyString(masterGrid.GetMasterRowFieldValues("ModelTags"));
            //Boolean includeTaggedModels = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("IncludeTaggedModels"), true);

            //Session["odsEndUserSupportPublicDownloads_ModelNumber"] = itemModelNumber;
            //Session["odsEndUserSupportPublicDownloads_TaggedModels"] = modelTags.IsNullOrEmptyString() || !includeTaggedModels ? String.Empty : modelTags;
            Session["odsEndUserSupportPublicDownloads_FilterMode"] = "pub-doc";

        }

        protected void dxgvEndUserSupportPublicSW_BeforePerformDataSelect(object sender, EventArgs e)
        {
            // ASPxGridView masterGrid = (ASPxGridView)sender;

            //String itemModelNumber = ConvertUtility.ConvertNullToEmptyString(masterGrid.GetMasterRowFieldValues("ModelNumber"));
            //String modelTags = ConvertUtility.ConvertNullToEmptyString(masterGrid.GetMasterRowFieldValues("ModelTags"));
            //Boolean includeTaggedModels = ConvertUtility.ConvertToBoolean(masterGrid.GetMasterRowFieldValues("IncludeTaggedModels"), true);
            //Session["odsEndUserSupportPublicDownloads_ModelNumber"] = itemModelNumber;
            //Session["odsEndUserSupportPublicDownloads_TaggedModels"] = modelTags.IsNullOrEmptyString() || !includeTaggedModels ? String.Empty : modelTags;
            Session["odsEndUserSupportPublicDownloads_FilterMode"] = "pub-sw";

        }

        protected void dxgvEndUserSupportPublicSW_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                ASPxGridView masterGrid = (ASPxGridView)sender;
                //Literal ltr = masterGrid.FindRowCellTemplateControl(e.VisibleIndex
                //    , masterGrid.Columns["Title"] as GridViewDataColumn
                //    , "ltrDownloadProperties") as Literal;


                StringBuilder sbText = new StringBuilder();
                String version = ConvertUtility.ConvertNullToEmptyString(e.GetValue("Version"));
                if (!version.IsNullOrEmptyString())
                {
                    //if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    //sbText.AppendFormat("<span class='dlbl'>{0}</span><span class='dltxt'>{1}</span>", GetStaticResource("ltrDownloadProperties.Text.VersionLabel"), version);
                }

                //if (sbText.Length > 0) ltr.Text = sbText.ToString();
                //else ltr.Text = "&nbsp;";

            }

        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_PublicResourcesCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }

        protected void dxgvEndUserSupportPublicDoc_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                ASPxGridView masterGrid = (ASPxGridView)sender;
                Literal ltr = masterGrid.FindRowCellTemplateControl(e.VisibleIndex
                    , masterGrid.Columns["Title"] as GridViewDataColumn
                    , "ltrDownloadProperties") as Literal;

                StringBuilder sbText = new StringBuilder();
                String version = ConvertUtility.ConvertNullToEmptyString(e.GetValue("Version"));
                if (!version.IsNullOrEmptyString())
                {
                    //if (sbText.Length > 0) sbText.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
                    sbText.AppendFormat("<span class='dlbl'>{0}</span>&nbsp;&nbsp;<span class='dltxt'>{1}</span>", GetStaticResource("ltrDownloadProperties.Text.VersionLabel"), version);
                }

                //if (sbText.Length > 0) ltr.Text = sbText.ToString();
                //else ltr.Text = "&nbsp;";

            }
        }
    }
}