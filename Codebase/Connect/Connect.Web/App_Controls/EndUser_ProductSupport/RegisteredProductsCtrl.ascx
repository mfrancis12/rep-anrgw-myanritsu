﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisteredProductsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.RegisteredProductsCtrl" %>
<div ng-app="myAnristuApp">
   <script type="text/javascript">
       var productList = <%=MyProductsList%>,
           mn = "";
       $(document).on('click', '.my-anristu .update-results .table-row div.link a', function () {   
           mn = $(this).attr('data-id');          
           document.getElementById("hdnModelNumber").value = mn;
           document.getElementById("btnRedirect").click();
       });

    </script>
    <asp:HiddenField ID="hdnModelNumber" runat="server" ClientIDMode="Static" />
    <asp:Button runat="server" ID="btnRedirect" Text="" style="display:none;" OnClick="btnRedirect_Click" ClientIDMode="Static" />
<div class="ng-cloak" ng-controller="mycontroller">
    <div ng-show="loading" class="loading">
        <div class="sk-fading-circle">
          <div class="sk-circle1 sk-circle"></div>
          <div class="sk-circle2 sk-circle"></div>
          <div class="sk-circle3 sk-circle"></div>
          <div class="sk-circle4 sk-circle"></div>
          <div class="sk-circle5 sk-circle"></div>
          <div class="sk-circle6 sk-circle"></div>
          <div class="sk-circle7 sk-circle"></div>
          <div class="sk-circle8 sk-circle"></div>
          <div class="sk-circle9 sk-circle"></div>
          <div class="sk-circle10 sk-circle"></div>
          <div class="sk-circle11 sk-circle"></div>
          <div class="sk-circle12 sk-circle"></div>
        </div>
      </div>
    
    
	<div class="my-anristu my-product">
		<!-- My Anristu Product Title -->
            <div ng-hide="modelDataEmpty">
        	    <h4 class="filter-txt">
                    <asp:Localize runat="server" ID="lcalFilterBy" Text='<%$ Resources:Common,FilterBy.Text%>'></asp:Localize>
        	    </h4>
			    <!-- My Anristu Product Filter -->
			    <div class="my-anristu-filter">
				    <!-- My Anristu Product Select option group -->
				    <div class="select-option-wrapper">
						    <select ng-model="modelData" ng-options="i as i.ModelFilter for i in modelFilterList track by i.ModelFilter" ng-change="getFunction()" class="selectpicker custom-select select-model-filter">
							    <option value=""> <asp:Localize runat="server" ID="lcalSystemModel" Text='<%$ Resources:Common,SelectSystemModel.Text%>'></asp:Localize></option>
						    </select>
						    <select ng-disabled="functionFilters" ng-model="functionData" ng-options="i as i.FunctionFilter for i in functionFilterList track by i.FunctionFilter" ng-change="getOption()" class="selectpicker custom-select select-function-filter">
							    <option value=""> <asp:Localize runat="server" ID="lcalFunction" Text='<%$ Resources:Common,SelectFunction.Text%>'></asp:Localize></option>
						    </select>
						    <select ng-disabled="optionFilters" ng-model="optionData" ng-options="i as i.OptionFilter for i in optionFilterList track by i.OptionFilter" ng-change="getOptionResults()" class="selectpicker custom-select select-option-filter">
							    <option value=""><asp:Localize runat="server" ID="lcalOption" Text='<%$ Resources:Common,SelectOption.Text%>'></asp:Localize></option>
						    </select>
				    </div>
				    <div class="btn-panel-wrapper">
					    <button class="btn-filter btn-update-results" ng-disabled="!modelData.ModelFilter" ng-click="showResult($event)"><asp:Localize runat="server" ID="lcalUpdate" Text='<%$ Resources:Common,UpdateResult.Text%>'></asp:Localize></button>
					    <button class="btn-filter btn-clear-filters"  ng-disabled="!modelData.ModelFilter" ng-click="clearFilters($event)"><asp:Localize runat="server" ID="lcalClear" Text='<%$ Resources:Common,Clear.Text%>'></asp:Localize></button>
				    </div>
			    </div>
            </div>
			<!-- My Anristu Product Filter -->
			<div class="my-anristu-breadcrum" ng-hide="modelDataEmpty">
				<span class="select-filter-txt"><asp:Localize runat="server" ID="lcalSelectedFilter" Text='<%$ Resources:Common,SelectedFilter.Text%>'></asp:Localize> :</span>
				<strong class="view-system-model"></strong>
                <strong class="view-function"></strong>
                <strong class="view-option"></strong>
				
			</div>
			<div class="grid-results" ng-hide="errorMessage">
				<div class="update-results" >
					<div class="table-head" ng-hide="productTitle">
						<asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SupportedModelCtrl,ProductName.Text%>"></asp:Localize>
					</div>
					<div  ng-repeat="data in resultData" class="table-row">
						<div class="img">
							<img ng-src="{{data.ProductSmallImageUrl}}" title="Image" alt="Image">
						</div>
						<div class="link">
                            <%--MA-1395--%>
                            <a style="cursor:pointer" data-id="{{data.MasterModel}}">{{data.ProductModelAndName}}</a>                            
						</div>
					</div>
				</div>                
				<div class="pagination-wrapper" ng-hide="paginationEle || paginationLinks">
				<div class="item-results"> 
					<strong>{{((currentPage-1)*itemsPerPage)+1}}</strong> &#45;
					<strong>
		                <span ng-hide="(currentPage*itemsPerPage) > totalItems">{{currentPage*itemsPerPage}}</span>
		                <span ng-hide="((currentPage*itemsPerPage) <= totalItems) || (itemsPerPage >= totalItems)">{{totalItems}}</span>
                    </strong>
					<strong ng-if = "totalItems < itemsPerPage">{{totalItems}}</strong> <asp:Localize runat="server" ID="lcalOfAbout" Text='<%$ Resources:Common,ofabout%>'></asp:Localize> 
					<strong>{{tableData.length}}</strong> 
                    <asp:Localize runat="server" ID="Localize2" Text='<%$ Resources:Common,Results_Text%>'></asp:Localize> 
				</div>
					<pagination ng-hide="itemsPerPage >= tableData.length || paginationLinks" total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage" ng-change="pageChanged($event)" max-size="maxSize"></pagination>
				</div>
			</div>
			<div class="resonse-message" ng-show="errorMessage">
				<div class="resonse-message-container" ng-bind-html="errorMessage"></div>
			</div>
        </div>
	</div>
</div>

<script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/bootstrap-select.js"></script>
<script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/angular.min.js"></script>
<script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/ui-bootstrap-tpls-0.11.0.js"></script>
<script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/angular-sanitize.js"></script>
<script type="text/javascript" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/js/MyAnritsu_App_JS/myanritsu.js"></script>
