﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using DevExpress.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using System.Collections;
using System.Net;
using Newtonsoft.Json;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class RegisteredProductsCtrl : System.Web.UI.UserControl
    {
        readonly ProdReg_DataSource _datasrcProd = new ProdReg_DataSource();
        public string MyProductsList { get; set; }

        private void Page_Load(Object sender, EventArgs e)
        {
            //get the TeamIds associated with the logged in user
            var clientStatusCode = HttpStatusCode.OK;
            var registrations = _datasrcProd.GetMasterRegistrationsBasic(AccountUtility.GenerateUserTeamIds(), ref clientStatusCode, "", "", "");
            string jsonString;
            //get the resource string from DB
            if (registrations == null || registrations.Count < 1)
            {
                var resValue = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_MyRegisteredProductsCtrl", "EmptyData.Text"));
                jsonString = JsonConvert.SerializeObject(new ErrorInfo() { ErrorMessage = resValue, StatusCode = 200 });
            }
            else
                jsonString = JsonConvert.SerializeObject(registrations);
            MyProductsList = jsonString;
        }

        //MA-1395
        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session[KeyDef.SSKeys.Selected_ModelNumber] = hdnModelNumber.Value;
            HttpContext.Current.Session[KeyDef.SSKeys.Selected_SerialNumber] = null;
            Response.Redirect(KeyDef.UrlList.ProductSupportPrivate);
        }
    }
}