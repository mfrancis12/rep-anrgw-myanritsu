﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisteredProductsNavTreeCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.RegisteredProductsNavTreeCtrl" %>
<!-- active only -->

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_MyProductsTreeCtrl,pnlContainer.HeaderText %>" visible="false">
    <div class="settingrow">
        <dx:ASPxTreeView ID="TreeView1" runat="server" AllowSelectNode="true" SettingsLoadingPanel-ShowImage="true" ShowTreeLines="true" EnableAnimation="true" ShowExpandButtons="true" EnableHotTrack="true" Styles-Node-ImageSpacing="0"
             Styles-NodeText-Spacing="0">
             
        </dx:ASPxTreeView>
    </div>

    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_MyProductsTreeCtrl" />
</anrui:GlobalWebBoxedPanel>

