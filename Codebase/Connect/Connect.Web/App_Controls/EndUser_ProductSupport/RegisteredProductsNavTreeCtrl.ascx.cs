﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;


namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class RegisteredProductsNavTreeCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) BuildTopLevel();
        }

        #region BuildTree
        private void BuildTopLevel()
        {
            //DataTable tbMyRegProds = UIHelper.MyRegisteredProductsCacheGet(); Needs to be changed
            DataTable tbMyRegProds = Lib.ProductRegistration.ProdReg_ItemBLL.SelectAllByAccountID(AccountUtility.GetSelectedAccountID(true)
                       , LoginUtility.GetCurrentUserOrSignIn().MembershipId);

          
            if (tbMyRegProds == null)
            {
                this.Visible = false;
                return;
            }
            var distinctMNs = (from n in tbMyRegProds.AsEnumerable() select n.Field<String>("ModelNumber")).Distinct();
            String imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/ModelNo.png";
            
            foreach (var mn in distinctMNs)
            {
                DevExpress.Web.TreeViewNode newNode = CreateNode(mn, imageUrl, "", false);
                TreeView1.Nodes.Add(newNode);
                PopulateSubTree(mn, newNode);
            }

        }

        private void PopulateSubTree(String mn, DevExpress.Web.TreeViewNode node)
        {

            String imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/apps/connect/img/serialNo.png";
            // changed to new icons for model and serial numbers
            

           // DataTable tbMyRegProds = UIHelper.MyRegisteredProductsCacheGet(); Needs to be changed
            DataTable tbMyRegProds = Lib.ProductRegistration.ProdReg_ItemBLL.SelectAllByAccountID(AccountUtility.GetSelectedAccountID(true)
                      , LoginUtility.GetCurrentUserOrSignIn().MembershipId);

            int activeSNCount = 0;

            if (tbMyRegProds != null)
            {
                var serials = (from r in tbMyRegProds.AsEnumerable()
                               where r.Field<String>("ModelNumber").Equals(mn, StringComparison.InvariantCultureIgnoreCase)
                               select r);

                foreach (DataRow snR in serials)
                {
                    String statusCode = snR["StatusCode"].ToString();
                    if (statusCode != "active") continue;

                    //Boolean isPaidSupportModel = ConvertUtility.ConvertToBoolean(snR["IsPaidSupportModel"], false);
                    //DateTime jpSupportExpireOn = ConvertUtility.ConvertToDateTime(snR["JPSupportExpireOn"], DateTime.MinValue);
                    Boolean userACLRequired = ConvertUtility.ConvertToBoolean(snR["UseUserACL"], false);
                    Boolean userHasAccess = ConvertUtility.ConvertToBoolean(snR["UserHasAccess"], false);
                   // if (isPaidSupportModel && jpSupportExpireOn < DateTime.UtcNow) continue;
                    if (userACLRequired && !userHasAccess) continue;

                    String webAccessKey = snR["WebToken"].ToString();

                    String navigateUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.ProductSupport, KeyDef.QSKeys.RegisteredProductWebAccessKey, webAccessKey);
                    DevExpress.Web.TreeViewNode childNode = CreateNode(snR["SerialNumber"].ToString().StartsWith("serial n/a") ? "n/a" : snR["SerialNumber"].ToString(), imageUrl, navigateUrl, true);
                    node.Nodes.Add(childNode);
                    activeSNCount++;
                    if (webAccessKey.Equals(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], StringComparison.InvariantCultureIgnoreCase))
                    {
                        TreeView1.SelectedNode = childNode;
                        node.Expanded = true;
                    }
                }

            }
            if (activeSNCount < 1) TreeView1.Nodes.Remove(node);
        }

        private DevExpress.Web.TreeViewNode CreateNode(string text, string imageurl, String navigateUrl, bool expanded)
        {
            DevExpress.Web.TreeViewNode node = new DevExpress.Web.TreeViewNode();
            node.Text = text;
    
            if (!String.IsNullOrEmpty(imageurl)) node.Image.Url = imageurl;
            node.Expanded = expanded;
            if (!String.IsNullOrEmpty(navigateUrl))
                node.NavigateUrl = navigateUrl;
            return node;
        }

        #endregion

        #region ResourceKey

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}