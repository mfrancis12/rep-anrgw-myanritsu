﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisteredProductsTabsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.RegisteredProductsTabsCtrl" %>
<dx:ASPxTabControl ID="tabMyRegProducts" runat="server" Theme="AnritsuDevXTheme" Width="100%">
    <Tabs>
        <dx:Tab Name="tabNonDongle" Text="<%$ Resources:STCTRL_RegProdsTab,prodRegsTab.Text %>"  NavigateUrl="~/myproduct/home"></dx:Tab>
        <dx:Tab Name="tabDongle" Text="<%$ Resources:STCTRL_RegProdsTab,usbProdRegsTab.Text %>"  NavigateUrl="~/myproduct/home-usb" Visible="false" ClientVisible="false"></dx:Tab>
    </Tabs>
    <Paddings PaddingBottom="0" />
</dx:ASPxTabControl>