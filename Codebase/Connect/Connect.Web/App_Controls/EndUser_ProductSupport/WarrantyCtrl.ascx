﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WarrantyCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.WarrantyCtrl" %>
<div style="width: 100%; text-align: center; padding: 10px;">
    <dx:ASPxGridView ID="dxgvEndUserSupportWty" ClientInstanceName="dxgvEndUserSupportWty" runat="server" DataSourceID="odsEndUserSupportWty" Width="740px" Theme="AnritsuDevXTheme">
        <Columns>
            <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$ Resources:STCTRL_WarrantyCtrl,ModelNumber.Text %>" VisibleIndex="1" Width="130px"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="SerialNumber" Caption="<%$ Resources:STCTRL_WarrantyCtrl,SerialNumber.Text %>" VisibleIndex="2" Width="130px"></dx:GridViewDataColumn>
            <dx:GridViewDataColumn FieldName="WarrantyDesc" Caption="<%$ Resources:STCTRL_WarrantyCtrl,WarrantyDesc.Text %>" VisibleIndex="10"></dx:GridViewDataColumn>
            <dx:GridViewDataDateColumn FieldName="WarrantyExpDate" VisibleIndex="15" Width="70px" Caption="<%$ Resources:STCTRL_WarrantyCtrl,WarrantyExpDate.Text %>"></dx:GridViewDataDateColumn>
        </Columns>
        <Styles>
            <Header HorizontalAlign="Center"></Header>
        </Styles>
    </dx:ASPxGridView>
    <asp:ObjectDataSource ID="odsEndUserSupportWty" runat="server"
        TypeName="Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport.Warranty_DataSource" SelectMethod="Select" OnSelecting="odsEndUserSupportWty_OnSelecting"
        EnablePaging="false" EnableCaching="true">
        <SelectParameters>
             <%--MA-1395--%>
             <%--<asp:QueryStringParameter Name="modelNumber" QueryStringField="mn" DbType="String" />--%>
            <asp:SessionParameter Name="modelNumber" DbType="String" SessionField="mn" />
            <%--<asp:Parameter Name="serialNumber" DbType="String" />--%>
            <asp:SessionParameter Name="serialNumber" DbType="String" SessionField="sn" />
            <asp:Parameter Name="dtTeams" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
