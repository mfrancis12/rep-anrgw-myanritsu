﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport
{
    public partial class WarrantyCtrl : DownloadResourceCtrlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void odsEndUserSupportWty_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["dtTeams"] = AccountUtility.GenerateUserTeamIds();
            e.InputParameters["serialNumber"] = SerialNumber;
        }
    }
}