﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_FeedbackCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.ProdReg_FeedbackCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_FeedbackCtrl,pnlContainer.Text %>">
<div class="settingrow"><asp:Localize ID="lcalFeedbackDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_FeedbackCtrl,lcalFeedbackDesc.Text %>"></asp:Localize></div>
<div class="settingrow">
<asp:PlaceHolder ID="phFeedbackForm" runat="server" EnableViewState="true"></asp:PlaceHolder>
</div>
<div class="settingrow">
    <asp:Button ID="bttSubmit" runat="server" Text="<%$ Resources:STCTRL_ProdReg_FeedbackCtrl,bttSubmit.Text %>" onclick="bttSubmit_Click" />
</div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_FeedbackCtrl" />