﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.AnrCommon.GeoLib;

namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class ProdReg_FeedbackCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid ProdRegCartWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.ProdRegCartWebAccessKey], Guid.Empty);
            }
        }

        public PlaceHolder FormPlaceHolder
        {
            get
            {
                return phFeedbackForm;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitFields();
            }
            LoadForm();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void InitFields()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);

            Guid accountID = AccountUtility.GetSelectedAccountID(true);

            if (!cartg.AccountID.Equals(accountID)) throw new HttpException(404, "Page not found.");

            String userEmail = LoginUtility.GetCurrentUserOrSignIn().Email;
            if (!cartg.LatestUserEmail.Equals(userEmail))
            {
                //cart was created by other user in the same account
                Lib.ProductRegistration.Acc_ProductRegCartBLL.UpdateUser(cartg.CartWebAccessKey, userEmail);
            }
            //add on form not completed yet.
            if (!prdCfg.Reg_AddOnFormID.IsNullOrEmptyGuid() && !cartg.IsAddOnFormCompletedForAllItems)
            {
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Other
                   , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey));
            }

            if (prdCfg.Reg_FeedbackFormID.IsNullOrEmptyGuid())
            {
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Submit
                  , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey));
            }


        }

        public void LoadForm()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);


            if (prdCfg.Reg_FeedbackFormID.IsNullOrEmptyGuid()) return;

            pnlContainer.HeaderText = cartg.ModelNumber + " " + this.GetGlobalResourceObject(
                String.Format("FRM_{0}", prdCfg.Reg_FeedbackFormID.ToString().ToUpperInvariant()), "HeaderText").ToString();

           
            App_Lib.AnrSso.ConnectSsoUser loggedInUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.Reg_FeedbackFormID);
            App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, FormPlaceHolder, formData, loggedInUser);
            //pnlSNAddOnForm.Visible = true;
            //this.Controls.Add(phForm);


        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            CaptureFeedback();
        }

        public void CaptureFeedback()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);

            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();

            #region " capture data "
            Dictionary<string, string> data = new Dictionary<string, string>();

            #region " common feedback data "
            data.Add("ModelNumber", cartg.ModelNumber.Trim());
            data.Add("SerialNumbers", cartg.SerialNumberDelimitedString);
            data.Add("CompanyName", HttpUtility.HtmlEncode(acc.CompanyName.Trim()));
            if (!acc.OwnerMemberShipID.IsNullOrEmptyGuid())
            {
                Lib.Account.Profile_Address companyAddr = Lib.BLL.BLLProfile_Address.SelectByMemberShipID(acc.OwnerMemberShipID);
                if (companyAddr != null)
                {
                    data.Add("CompanyAddress1", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(companyAddr.Address1).Trim()));
                    data.Add("CompanyAddress2", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(companyAddr.Address2).Trim()));
                    data.Add("CompanyCityTown", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(companyAddr.CityTown).Trim()));
                    data.Add("CompanyZipPostalCode", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(companyAddr.ZipPostalCode).Trim()));
                    data.Add("CompanyCountryCode", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(companyAddr.CountryCode).Trim()));
                }

            }
            data.Add("UserEmail", HttpUtility.HtmlEncode(usr.Email.Trim()));
            data.Add("UserFirstName", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(usr.FirstName).Trim()));
            data.Add("UserLastName", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(usr.LastName).Trim()));
            data.Add("UserPhone", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(usr.PhoneNumber).Trim()));
            data.Add("SubmittedFromIP", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(WebUtility.GetUserIP())));
            GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi == null) data.Add("SubmittedFromCountryCode", String.Empty);
            else data.Add("SubmittedFromCountryCode", HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(gi.CountryCode).Trim()));
            #endregion

            List<KeyValuePair<string, string>> formData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(FormPlaceHolder);
            if (formData != null)
            {
                foreach (KeyValuePair<string, string> kv in formData)
                {
                    if (kv.Key.IsNullOrEmptyString()) continue;
                    if (data.ContainsKey(kv.Key.Trim())) continue;
                    data.Add(kv.Key.Trim(), ConvertUtility.ConvertNullToEmptyString(kv.Value).Trim());
                }
            }
            #endregion

            Lib.DynamicForm.Frm_Form formInfo = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.Reg_FeedbackFormID);
            int formSubmitID = 0;
            Guid formSubmitKey = Guid.Empty;
            Lib.BLL.BLLFrm_FormSubmitMaster.Insert(prdCfg.Reg_FeedbackFormID
                , formInfo.FormName
                , usr.Email
                , acc.AccountID
                , data
                , out formSubmitID, out formSubmitKey);

            String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Submit
                            , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey);
            WebUtility.HttpRedirect(this, url);
        }


        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_FeedbackCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}