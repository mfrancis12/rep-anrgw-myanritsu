﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_InProgressListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.ProdReg_InProgressListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,pnlContainer.HeaderText %>">
    <div class="settingrow">
        <asp:Localize ID="lcalRegInProgressDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,lcalRegInProgressDesc.Text %>"></asp:Localize></div>
    <div class="settingrow">
        <asp:CheckBox ID="cbxShowAll" runat="server" Text="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,cbxShowAll.Text %>"
            OnCheckedChanged="cbxShowAll_CheckedChanged" AutoPostBack="true" /></div>
    <div class="settingrow">
            <div style="width: 100%; height: 150px; overflow-x: visible; overflow-y: scroll">
                <asp:GridView ID="gvInProgressList" runat="server" AutoGenerateColumns="false" EmptyDataText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,gvInProgressList.EmptyDataText %>"
                    ShowFooter="false" ShowHeaderWhenEmpty="false" OnRowCommand="gvInProgressList_RowCommand" OnRowDataBound="gvInProgressList_RowDataBound" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="ModelNumber" HeaderText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,ModelNoField %>"
                            ItemStyle-Font-Bold="true" />
                        <asp:BoundField DataField="LatestUserEmail" HeaderText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,LastUserField %>" />
                        <%-- <asp:BoundField DataField="CreatedOnUTC" HeaderText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,CreatedOnField %>"
                            DataFormatString="{0:MMM dd, yyyy}" />--%>
                        <asp:TemplateField HeaderText="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,CreatedOnField %>">
                            <ItemTemplate>
                                <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"CreatedOnUTC")) %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:HyperLinkField ControlStyle-CssClass="blueit" Text="<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,hlfContinue.Text %>"
                            DataNavigateUrlFields="CartWebAccessKey" DataNavigateUrlFormatString="/myproduct/regproduct-serialnumbers?crtwak={0}" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbttDelete" runat="server" Text='<%$ Resources:STCTRL_ProdReg_InProgressListCtrl,lbttDelete.Text %>'
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CartWebAccessKey") %>'
                                    CommandName="DeleteCommand" skinid="blueit" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_InProgressListCtrl" />
