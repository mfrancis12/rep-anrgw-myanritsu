﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class ProdReg_InProgressListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cbxShowAll.Checked = ShowAllRegistrationInProgressGet();
                BindInProgressRegistrations();
            }
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_InProgressListCtrl"; }

        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        private void BindInProgressRegistrations()
        {
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
            DataTable tb = Lib.ProductRegistration.Acc_ProductRegCartBLL.SelectByAccountID(AccountUtility.GetSelectedAccountID(true));
            DataView dv = tb.DefaultView;

            if (!cbxShowAll.Checked)
            {
                dv.RowFilter = String.Format("LatestUserEmail = '{0}'", usr.Email);
            }

            if (dv.Count > 0)
            {
                gvInProgressList.DataSource = dv;
                gvInProgressList.DataBind();
            }
            else
                this.Visible = false;
        }

        protected void gvInProgressList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteCommand")
            {
                Guid webAccessKey = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                Lib.ProductRegistration.Acc_ProductRegCartBLL.DeleteCart(webAccessKey);
                BindInProgressRegistrations();
            }
        }

        protected void gvInProgressList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // reference the Delete LinkButton
                LinkButton lbttDelete = e.Row.FindControl("lbttDelete") as LinkButton;
                lbttDelete.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteRegMsg") + "')";

            }
        }

        protected void cbxShowAll_CheckedChanged(object sender, EventArgs e)
        {
            ShowAllRegistrationInProgressSet(cbxShowAll.Checked);
            BindInProgressRegistrations();
        }

        private Boolean ShowAllRegistrationInProgressGet()
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return false;
            return ConvertUtility.ConvertToBoolean(
                HttpContext.Current.Session[KeyDef.SSKeys.ShowAllRegistrationInProgress], false);
        }

        private void ShowAllRegistrationInProgressSet(Boolean showAllRegistrationInProgress)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.ShowAllRegistrationInProgress] = showAllRegistrationInProgress;
        }
    }
}