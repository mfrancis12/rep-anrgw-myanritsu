﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_OtherCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.ProdReg_OtherCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText=" ">
<div class="settingrow"><asp:Localize ID="lcalOtherInfoDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_OtherCtrl,lcalOtherInfoDesc.Text %>"></asp:Localize></div>
<div class="settingrow">
    <span class="settinglabelplain"><asp:Localize ID="lcalMN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_OtherCtrl,lcalMN.Text %>"></asp:Localize>: </span>
    <asp:Literal ID="ltrMN" runat="server" Text=""></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabelplain"><asp:Localize ID="lcalSN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_OtherCtrl,lcalSN.Text %>"></asp:Localize>: </span>
    <asp:Literal ID="ltrSN" runat="server" Text=""></asp:Literal>
</div>
<div class="settingrow">
<asp:PlaceHolder ID="phSNAddOnForm" runat="server"></asp:PlaceHolder>
</div>
<div class="settingrow" style="text-align: right;">
    <asp:Button ID="bttSubmit" runat="server" Text="<%$ Resources:STCTRL_ProdReg_OtherCtrl,bttSubmit.Text.Submit %>" onclick="bttSubmit_Click" />
</div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_OtherCtrl" />