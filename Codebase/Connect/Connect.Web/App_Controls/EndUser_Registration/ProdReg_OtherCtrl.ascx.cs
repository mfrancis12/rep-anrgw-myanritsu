﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ProductRegistration;
namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class ProdReg_OtherCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid ProdRegCartWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.ProdRegCartWebAccessKey], Guid.Empty);
            }
        }

        public Int32 CartItemID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ProdRegCartItemID], 0);
            }
        }

        public PlaceHolder FormPlaceHolder
        {
            get
            {
                return phSNAddOnForm;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckProductReg();
            }
            LoadForm();
        }

        private void CheckProductReg()
        {
            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            if (!cartg.AccountID.Equals(accountID)) throw new HttpException(404, "Page not found.");

            String userEmail = LoginUtility.GetCurrentUserOrSignIn().Email;
            if (!cartg.LatestUserEmail.Equals(userEmail))
            {
                //cart was created by other user in the same account
                Lib.ProductRegistration.Acc_ProductRegCartBLL.UpdateUser(cartg.CartWebAccessKey, userEmail);
                EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);
            }
            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) throw new HttpException(404, "Page not found."); ;

            if (CartItemID > 0)
            {
                if (!Lib.ProductRegistration.Acc_ProductRegCartItemBLL.IsContainInItem(cartg.CartItems, CartItemID)) //someone hacking
                    throw new HttpException(404, "Page not found.");
                Lib.ProductRegistration.Acc_ProductRegCartItem item =
                    Lib.ProductRegistration.Acc_ProductRegCartItemBLL.FindItemByCartItemID(cartg.CartItems, CartItemID);
                if (item == null) throw new HttpException(404, "Page not found.");
                else if (item.IsAddOnFormCompleted) WebUtility.HttpRedirect(null, GetNextUrl(prdCfg, cartg));
            }


            if (prdCfg.Reg_AddOnFormID.IsNullOrEmptyGuid()
                || cartg.IsAddOnFormCompletedForAllItems
                || (CartItemID < 1 && prdCfg.Reg_AddOnFormPerSN)
                )
            {
                WebUtility.HttpRedirect(null, GetNextUrl(prdCfg, cartg));
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadForm()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);
            ltrMN.Text = cartg.ModelNumber;
            if (prdCfg.Reg_AddOnFormPerSN)
            {
                bttSubmit.Text = GetStaticResource("bttSubmit.Text.Next");
                pnlContainer.HeaderText = this.GetGlobalResourceObject(String.Format("FRM_{0}", prdCfg.Reg_AddOnFormID.ToString().ToUpperInvariant()), "HeaderText").ToString();
                Lib.ProductRegistration.Acc_ProductRegCartItem snItem = Lib.ProductRegistration.Acc_ProductRegCartItemBLL.FindItemByCartItemID(cartg.CartItems, CartItemID);
                if (snItem != null) ltrSN.Text = snItem.SerialNumber;
            }
            else
            {
                bttSubmit.Text = GetStaticResource("bttSubmit.Text.Submit");
                pnlContainer.HeaderText = this.GetGlobalResourceObject(String.Format("FRM_{0}", prdCfg.Reg_AddOnFormID.ToString().ToUpperInvariant()), "HeaderText").ToString();
                ltrSN.Text = cartg.SerialNumberDelimitedString;
            }

            App_Lib.AnrSso.ConnectSsoUser loggedInUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.Reg_AddOnFormID);
            App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phSNAddOnForm, formData, loggedInUser);
            //pnlSNAddOnForm.Visible = true;
            //this.Controls.Add(phForm);


        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            CaptureAndMoveToNextItem();
        }

        public void CaptureAndMoveToNextItem()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");
            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);

            #region " capture data "
            //capture data for the SN(s) here if valid
            List<KeyValuePair<string, string>> formData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(phSNAddOnForm);
            if (formData != null)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (KeyValuePair<string, string> kv in formData)
                {
                    if (kv.Key.IsNullOrEmptyString()) continue;
                    if (!data.ContainsKey(kv.Key.Trim()))
                    {
                        data.Add(kv.Key.Trim(), kv.Value.Trim());
                    }
                }

                if (prdCfg.Reg_AddOnFormPerSN)
                {
                    Lib.ProductRegistration.Acc_ProductRegCartItemDataBLL.InsertUpdate(CartItemID, data);
                }
                else
                {
                    foreach (Lib.ProductRegistration.Acc_ProductRegCartItem item in cartg.CartItems)
                    {
                        Lib.ProductRegistration.Acc_ProductRegCartItemDataBLL.InsertUpdate(item.CartItemID, data);
                    }
                }
                
            }
            #endregion

            if (prdCfg.Reg_AddOnFormPerSN)
            {
                Lib.ProductRegistration.Acc_ProductRegCartItemBLL.UpdateAddOnFormStatus(CartItemID, true);
            }
            else
            {
                foreach (Lib.ProductRegistration.Acc_ProductRegCartItem item in cartg.CartItems)
                    Lib.ProductRegistration.Acc_ProductRegCartItemBLL.UpdateAddOnFormStatus(item.CartItemID, true);
            }
            
            cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true); //refresh data
            WebUtility.HttpRedirect(null, GetNextUrl(prdCfg, cartg));
        }

        private String GetNextUrl(Lib.Cfg.ProductConfig prdCfg, Lib.ProductRegistration.Acc_ProductRegCart cart)
        {
            if (cart == null || prdCfg == null) return KeyDef.UrlList.MyProducts;

            String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Submit
                   , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
                   );

            if (!prdCfg.Reg_AddOnFormID.IsNullOrEmptyGuid() && !cart.IsAddOnFormCompletedForAllItems)
            {
                if (prdCfg.Reg_AddOnFormPerSN)
                {
                    Lib.ProductRegistration.Acc_ProductRegCartItem nextItem = cart.NextItemForAddOnForm;
                    if (nextItem != null)
                    {
                        url = String.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.RegProd_Other
                           , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
                           , KeyDef.QSKeys.ProdRegCartItemID, nextItem.CartItemID
                           );
                    }
                }
                else
                {
                    url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Other
                           , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
                           );
                }
            }
            else //addon form is done
            {
                if (!prdCfg.Reg_FeedbackFormID.IsNullOrEmptyGuid())
                {
                    url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Feedback
                               , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
                               );
                }
            }
            return url;
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_OtherCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey) as String;
        }

        #endregion
    }
}