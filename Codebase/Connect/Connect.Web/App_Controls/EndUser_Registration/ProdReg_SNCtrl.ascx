﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_SNCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.ProdReg_SNCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_SNCtrl,pnlContainer.HeaderText %>">
    <asp:Panel ID="pnlSupportedModelSearch" runat="server" Style="position: relative">
        <table border="0" cellpadding="2" cellspacing="2" width="100%">
            <tr>
                <td valign="top" width="50%">
                    <div style="width: 90%; min-height: 200px;">
                        <div class="settingrow" style="margin-left: 20px;">
                            <asp:Localize ID="lcalSNDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalSNDesc.Text %>"></asp:Localize>
                            <br />
                        </div>
                        <div class="settingrow" style="margin-left: 20px;">
                            <div class="settingrow group input-text required">
                                <label>
                                    <asp:Localize ID="lcalEnterOneSN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalEnterOneSN.Text %>"></asp:Localize>:</label>
                                <p class="error-message">
                                    <asp:Localize ID="lclErrorMsg" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
                                </p>
                                <asp:TextBox ID="txtNewSN" runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rvSn" runat="server" ControlToValidate="txtNewSN" Visible="False" Enabled="False" ErrorMessage="<%$Resources:Common,ERROR_Required%>" SetFocusOnError="true" ValidationGroup="vgAddOneSN" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                            <div style="font-size:11px;">
                                <span>
                                    <asp:Localize ID="lcalCompanyInfoTitle" runat="server" Text="<%$Resources:STCTRL_ProdReg_SNCtrl,txtMNSearch.HelpText%>"></asp:Localize>
                                </span>
                            </div>
                            <div class="margin-top-15">
                            <asp:Button ID="bttAddOneSN" runat="server" Text="<%$Resources:STCTRL_ProdReg_SNCtrl,bttAddOneSN.Text%>" ValidationGroup="vgAddOneSN"  OnClick="bttAddOneSN_Click" />
                                </div>
                            <br />

                            <span class="msg">
                                <asp:Literal ID="ltrAddOneSNMsg" runat="server"></asp:Literal>
                            </span>
                        </div>
                        <div id="divCheckSNWarning" runat="server" visible="false" class="SnWarningMsg">
                            <div style="height: 5px">
                            </div>
                            <span class="msg">
                                <asp:Localize ID="lcalCheckSNWarning" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalCheckSNWarning.HeaderText %>"></asp:Localize></span><br />
                            <span>
                                <asp:Localize ID="lcalContactSupportMsg" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalSNSupportMsg.Text %>"></asp:Localize></span>
                            <div style="height: 5px">
                            </div>
                        </div>
                        <div class="settingrow" style="overflow: auto;">
                            <div id="divExistingSNWarning" runat="server" visible="false" class="SnWarningMsg">
                                <span class="msg">
                                    <asp:Localize ID="lcalExistingSNWarning" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalExistingSNWarning.Text %>"></asp:Localize>&nbsp;-</span>
                                <span>
                                    <asp:Literal ID="ltrExistingSN" runat="server"></asp:Literal></span>
                            </div>
                        </div>
                        <div class="settingrow" style="margin-left: 15px; overflow: hidden">
                            <span class="settinglabelplain">
                                <asp:Localize ID="lcalSN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalSN.Text %>"></asp:Localize>
                            </span>(
                            <asp:Literal ID="ltrTotalSN" runat="server" Text="0"></asp:Literal>
                            ) <span class="settinglabelplain right text-right">
                                <asp:LinkButton ID="lbttRemoveAllSN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lbttRemoveAllSN.Text %>" OnClick="lbttRemoveAllSN_Click" Visible="false" CausesValidation="false" CssClass="blueit"></asp:LinkButton></span>
                        </div>
                        <div class="settingrow" style="overflow: auto; margin-left: 15px; max-height: 300px;">
                            <asp:GridView ID="gvSerials" runat="server" ShowFooter="false" ShowHeader="false"
                                AutoGenerateColumns="false" OnRowCommand="gvSerials_RowCommand" EmptyDataText="<%$ Resources:STCTRL_ProdReg_SNCtrl,gvSerials.EmptyDataText %>"
                                HorizontalAlign="Left" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Serial Number" ItemStyle-CssClass="gvstyle1-item-lft">
                                        <ItemTemplate>
                                            <span class="normal-text">
                                                <%# DataBinder.Eval(Container.DataItem, "SerialNumber") %></span>&nbsp; <span>
                                                    <asp:Image ID="imgSNNotFoundStatus" runat="server"
                                                        ToolTip="Serial number not found in our database" ImageUrl="//dl.cdn-anritsu.com/appfiles/img/icons/red-x.ico"
                                                        Width="12px" Height="12px" Visible='<%# DataBinder.Eval(Container.DataItem, "SerialCheckStatus").ToString() == "0"  %>' />
                                                    <asp:Image ID="imgSNFoundStatus" runat="server" ToolTip="Serial number found in our database"
                                                        ImageUrl="//dl.cdn-anritsu.com/appfiles/img/icons/green_check.png" Width="12px"
                                                        Height="12px" Visible='<%# DataBinder.Eval(Container.DataItem, "SerialCheckStatus").ToString() == "1"  %>' /></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="15px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbttRemoveSN" runat="server" ToolTip="<%$ Resources:STCTRL_ProdReg_SNCtrl,lbttRemoveSN.Text %>"
                                                CommandName="RemoveSNCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CartItemID") %>'
                                                Font-Bold="false">
                                                <asp:Image ID="imgRemoveSN" Style="vertical-align: bottom" runat="server" ImageUrl="//dl.cdn-anritsu.com/appfiles/img/icons/trash-can.gif"
                                                    Width="16px" Height="16px" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="settingrow" style="float: right;">
                        </div>
                    </div>
                </td>
                <td style="width: 5px;">&nbsp;
                </td>
                <td valign="top" class="companyinfo">
                    <fieldset style="width: 93%;min-height: 200px;">
                            
                        <legend><asp:Localize ID="lcalVerifyCompanyInfoDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalVerifyCompanyInfoDesc.Text %>"></asp:Localize></legend>
                        <p><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalVerifyCmpnyDescMore.Text %>"></asp:Localize></p>
                        <div class="settingrow" style="text-align:right">
                            <asp:HyperLink CssClass="blueit" ID="hlEditCompanyInfo" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,hlEditCompanyInfo.Text %>"
                                NavigateUrl="~/myaccount/companyprofile" SkinID="blueit"></asp:HyperLink>
                        </div>
                       <%-- <div class="settingrow">
                            <span class="msg">
                                <asp:Literal ID="ltrUpdateProfileMsg" runat="server" Visible="false" Text="<%$Resources:STCTRL_ProdReg_SNCtrl,ltrUpdateProfileMsg.Text%>"></asp:Literal>
                            </span>
                        </div>--%>
                        <div class="settingrow">
                            <span class="settinglabelplain">
                                <asp:Localize ID="lcalAnritsuID" runat="server" Text="Anritsu ID "></asp:Localize>:</span>
                            <asp:Literal ID="ltrAnritsuID" runat="server"></asp:Literal>
                        </div>
                        <div class="settingrow">
                            <span class="settinglabelplain">
                                <asp:Localize ID="lcalUserFullName" runat="server" Text="<%$Resources:STCTRL_ProdReg_SNCtrl,lcalUserFullName.Text%>"></asp:Localize>:</span>
                            <span>
                                <asp:Literal ID="ltrUserFullName" runat="server"></asp:Literal>
                            </span>
                        </div>
                        <div class="settingrow">
                            <asp:Literal ID="ltrCompanyBasicInfo" runat="server"></asp:Literal>
                        </div>
                        </fieldset>
                        <fieldset id="fsBulkUpload" runat="server" style="width: 93%;" class="sel.ui-button-small">
                            <legend>
                                <asp:Localize ID="lcalSNImportTitle" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalSNImportTitle.Text %>"></asp:Localize></legend>
                            <div class="settingrow">
                                <div class="sel">
                                    <asp:FileUpload ID="fluSnBulkUpload" runat="server" />
                                    <asp:Button ID="bttSNImport" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,bttSNImport.Text %>"
                                         OnClick="bttSNImport_Click" />
                                </div>
                                <div class="settingrow">
                                    <span class="msg">
                                        <asp:Literal ID="ltrUploadMsg" runat="server" Text=""></asp:Literal></span>
                                </div>
                            </div>
                            <label>
                                <asp:Localize ID="lcalSNImportDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,lcalSNImportDesc.Text %>"></asp:Localize></label>
                        </fieldset>
                </td>
            </tr>
        </table>
        <div class="settingrow" style="text-align: right; margin-right: 25px;">
            <span class="msg">
                <asp:Literal ID="ltrMsg" runat="server" Text=""></asp:Literal></span>
        </div>
        <div class="settingrow" style="text-align: right;">
            <asp:Button ID="bttSubmit" runat="server" Text="<%$ Resources:STCTRL_ProdReg_SNCtrl,bttSubmit.Text %>"
                OnClick="bttSubmit_Click" />
        </div>
    </asp:Panel>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_SNCtrl" />
</anrui:GlobalWebBoxedPanel>
