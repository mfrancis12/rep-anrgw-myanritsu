﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.GoogleAPIs.GoogleTranslateAPI;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class ProdReg_SNCtrl : UserControl, IStaticLocalizedCtrl
    {

        public Guid ProdRegCartWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.ProdRegCartWebAccessKey], Guid.Empty);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ValidateCart();
            }
            base.OnInit(e);

            if (!Page.IsPostBack)
            {
                String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
                WebUtility.GenerateProcessingScript(bttAddOneSN, "onclick", processingText);
                WebUtility.GenerateProcessingScript(bttSubmit, "onclick", processingText);
                WebUtility.GenerateProcessingScript(bttSNImport, "onclick", processingText);
            }

            lbttRemoveAllSN.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteMsg") + "?')";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitProductReg();
                BindSerialNumberGrid();
            }
        }

        private void InitProductReg()
        {
            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);

            if (!prdCfg.Reg_AddOnFormID.IsNullOrEmptyGuid() || !prdCfg.Reg_FeedbackFormID.IsNullOrEmptyGuid())
            {
                bttSubmit.Text = GetStaticResource("bttSubmit.Text.Continue");
            }

            lcalSNDesc.Text = lcalSNDesc.Text.Replace("{cntMN}", cartg.ModelNumber);
            ltrCompanyBasicInfo.Text = CompanyProfileWzCtrl.GetReadOnlyCompanyInfo(cartg.AccountID, true, true, true);

            ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Sec_UserMembership memUser = BLLSec_UserMembership.SelectByMembershipId(user.MembershipId);
            ltrAnritsuID.Text = LoginUtility.GetCurrentUserOrSignIn().Email;
            //if (memUser.IsIndividualEmailAddress)
            //    lcalAnritsuID.Text += GetStaticResource("IndividualEmailMsg");
            //else
            //    lcalAnritsuID.Text += GetStaticResource("GroupEmailMsg");
            ltrUserFullName.Text = user.LastName + "," + user.FirstName;

            hlEditCompanyInfo.NavigateUrl = String.Format("{0}?{1}={2}"
                , ConfigUtility.AppSettingGetValue("AnrSso.Idp.EditAccount")
                , KeyDef.QSKeys.ReturnURL
                , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}"
                    , KeyDef.UrlList.RegProd_SerialNumbers.Replace("~","http://"+Request.Url.Host)
                    , KeyDef.QSKeys.ProdRegCartWebAccessKey
                    , ProdRegCartWebAccessKey.ToString())));
        }

        private void BindSerialNumberGrid()
        {
            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg == null) throw new HttpException(404, "Page not found.");

            gvSerials.DataSource = cartg.CartItems;
            gvSerials.DataBind();
            lbttRemoveAllSN.Visible = (cartg.CartItems != null && cartg.CartItems.Count > 1);
            ltrTotalSN.Text = cartg.CartItems == null ? "0" : cartg.CartItems.Count.ToString();
            bool badSNFound = Acc_ProductRegCartItemBLL.CheckIfThereIsSerialNumberNotFound(cartg.CartItems);
            divCheckSNWarning.Visible = badSNFound;
            // bttSubmit.Visible = (!badSNFound && cartg.CartItems != null && cartg.CartItems.Count > 0 && !ltrUpdateProfileMsg.Visible);
            bttSubmit.Visible = (!badSNFound && cartg.CartItems != null && cartg.CartItems.Count > 0);
        }

        private void ValidateCart()
        {
            if (ProdRegCartWebAccessKey.IsNullOrEmptyGuid()) //minimum requirement
                WebUtility.HttpRedirect(null, KeyDef.UrlList.RegProd_SelectModel);

            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            String userEmail = LoginUtility.GetCurrentUserOrSignIn().Email;

            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);

            if (cartg == null) //invalid cart or someone hacking
                WebUtility.HttpRedirect(null, KeyDef.UrlList.RegProd_SelectModel);
            else
            {
                if (!cartg.AccountID.Equals(accountID)) throw new HttpException(404, "Page not found.");//hacking other account
                if (!cartg.LatestUserEmail.Equals(userEmail))
                {
                    //cart was created by other user in the same account
                    Acc_ProductRegCartBLL.UpdateUser(cartg.CartWebAccessKey, userEmail);
                    EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);//refresh cache
                }
            }
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_SNCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
            if (cartg.CartItems == null || cartg.CartItems.Count < 1)
            {
                ltrMsg.Text = GetStaticResource("ERROR_AtLeastOneSNRequired");
                return;
            }

            //Submit();
            String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Other
                , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey);
            WebUtility.HttpRedirect(this, url);
        }

        protected void bttAddOneSN_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);
                //redirect to regproduct-serialnumbers if no cart found . this can be happend by clicking back button or book marking the urls with "crtwak" as qs parameter.
                if (cartg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SerialNumbers);
                ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);

                ltrAddOneSNMsg.Text = string.Empty;
                try
                {
                    Guid accountID = AccountUtility.GetSelectedAccountID(true);
                    String sn = txtNewSN.Text.Trim();
                    if (ProdReg_MasterBLL.IsExistingMasterRegistration(accountID, cartg.ModelNumber, sn))
                        throw new ArgumentException(GetStaticResource("ERROR_MNSNEXISTS"));
                    bool validSN = false; ;
                    if (!string.IsNullOrEmpty(prdCfg.Reg_SNPattern)&&!prdCfg.Reg_ValidateSN)
                    {
                        Regex reg = new Regex(prdCfg.Reg_SNPattern);
                        Match m = reg.Match(txtNewSN.Text.Trim());
                        validSN = m.Success;
                        if (!m.Success) throw new ArgumentException(GetStaticResource("ERROR_MNSNPatternMismatch"));
                    }
                    if (cartg.CartItems.Count > 0)
                    {
                        foreach (Acc_ProductRegCartItem newsn in cartg.CartItems)
                        {
                            if (newsn.SerialNumber.ToLower().Equals(txtNewSN.Text.ToLower().Trim())) throw new ArgumentException(GetStaticResource("ERROR_SNExistsInList"));
                        }
                    }

                    
                    Acc_ProductRegCartItemBLL.AddItem(ProdRegCartWebAccessKey, cartg.ModelNumber, txtNewSN.Text.Trim(), validSN?!prdCfg.Reg_ValidateSN:validSN);
                    EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true); //refresh
                    BindSerialNumberGrid();
                    txtNewSN.Text = String.Empty;
                    //WebUtility.HttpRedirect(null, Request.RawUrl);

                }
                catch (ArgumentException aex)
                {
                    ltrAddOneSNMsg.Text = aex.Message;
                }

            }
        }

        protected void gvSerials_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RemoveSNCommand")
            {
                Int32 cartItemID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Acc_ProductRegCartItemBLL.Delete(cartItemID);
                EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);
                BindSerialNumberGrid();
                //WebUtility.HttpRedirect(null, Request.RawUrl);
            }
        }

        protected void lbttRemoveAllSN_Click(object sender, EventArgs e)
        {
            Acc_ProductRegCartItemBLL.DeleteAllByCartWebAccessKey(ProdRegCartWebAccessKey);
            EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }

        protected void bttSNImport_Click(object sender, EventArgs e)
        {
            DoBulkUpload();
        }

        private void DoBulkUpload()
        {
            ltrUploadMsg.Text = String.Empty;
            //Lib.ProductRegistration.Acc_ProductRegCartItemBLL.DeleteAllByCartWebAccessKey(ProdRegCartWebAccessKey);
            //EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);
            try
            {
                if (!fluSnBulkUpload.HasFile) throw new ArgumentException(GetStaticResource("NoFileMsg"));
                //"Unable to upload the file.");
                String uploadedFileExt = Path.GetExtension(fluSnBulkUpload.FileName);

                List<String> allowedExtentions = new List<string>() { ".csv", ".xls", ".xlsx" };
                if (!allowedExtentions.Contains(uploadedFileExt.ToLowerInvariant())) throw new ArgumentException(GetStaticResource("InvalidFileMsg"));
                //"Invalid file format.  Only .csv,.xls,.xlsx file can be used.");
                String tempFolder = Server.MapPath(ConfigUtility.GetStringProperty("Connect.Web.BulkSN_TempFolder", "~/app_data/uploadstmp"));
                String tmpFileName = Path.Combine(tempFolder, Guid.NewGuid().ToString() + uploadedFileExt);
                fluSnBulkUpload.SaveAs(tmpFileName);

                DataSet ds = ConvertUtility.ConvertCsvToDataSet(tmpFileName);
                if (ds == null || ds.Tables == null || ds.Tables.Count < 1 || ds.Tables[0].Columns.Count < 1) throw new ArgumentException(GetStaticResource("InvalidFormatMsg"));
                //"Invalid file format.");
                if (ds.Tables[0].Rows.Count < 1) throw new ArgumentException(GetStaticResource("SNNotFoundMsg"));
                //"Serial numbers not found.");

                Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, false);

                Guid accountID = AccountUtility.GetSelectedAccountID(true);

                int totalInFile = ds.Tables[0].Rows.Count;
                int maximumSNLimit = ConfigUtility.GetIntProperty("Connect.Web.BulkSN_Max", 300);
                List<String> serials = new List<string>();
                List<String> serialsFinalList = new List<string>();
                List<String> existingSerials = new List<string>();
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    if (serials.Count > maximumSNLimit - 1) break;
                    String sn = ConvertUtility.ConvertNullToEmptyString(r[0]).Trim();
                    if (sn.Length < 3 || sn.Length > 20) continue;
                    //Validate serial numbers by checking html/sql injection characters
                    if (!ContainsSpecailChars(sn)) continue;
                    if (serials.Contains(sn)) continue;

                    if (ProdReg_MasterBLL.IsExistingMasterRegistration(accountID, cartg.ModelNumber, sn))
                    {
                        existingSerials.Add(sn); continue;
                    }
                    serials.Add(sn);
                }

                serialsFinalList.AddRange(serials);

                if (cartg.CartItems.Count > 0)
                {
                    foreach (Acc_ProductRegCartItem newsn in cartg.CartItems)
                    {
                        foreach (String sn in serials)
                        {
                            if (newsn.SerialNumber.Trim().Equals(sn.Trim()))
                                serialsFinalList.Remove(sn);
                        }
                    }
                }
                ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);

                int totalImported = Acc_ProductRegCartItemBLL.AddItem(ProdRegCartWebAccessKey, cartg.ModelNumber, serialsFinalList, !prdCfg.Reg_ValidateSN);
                EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);
                ltrUploadMsg.Text = String.Format("{0}" + GetStaticResource("SNImportedMsg") + "{1}" + GetStaticResource("SNCountMsg"), totalImported, totalInFile);
                //String.Format("{0} of {1} serial numbers imported successfully.", totalImported, totalInFile);
                if (totalImported > 0) BindSerialNumberGrid();
                if (existingSerials.Count > 0)
                {
                    StringBuilder excludedsn = new StringBuilder();
                    char[] characters = new char[] { ' ', ',' };
                    foreach (String sn in existingSerials)
                    {
                        excludedsn.Append(sn + ", ");
                    }
                    divExistingSNWarning.Visible = true;
                    ltrExistingSN.Text = excludedsn.ToString().TrimEnd(characters);
                }

            }
            catch (ArgumentException aex)
            {
                ltrUploadMsg.Text = aex.Message;
            }
            catch (Exception ex)
            {
                ltrUploadMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
            }
        }

        private static bool ContainsSpecailChars(string sn)
        {
            // Create the regular expression
            var pattern = @"^[a-zA-Z0-9]+$";
            var regex = new Regex(pattern);
            // Compare a string against the regular expression
            return regex.IsMatch(sn);
        }
    }
}