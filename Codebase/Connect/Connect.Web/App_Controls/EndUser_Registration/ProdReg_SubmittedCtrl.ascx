﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_SubmittedCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.ProdReg_SubmittedCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_SubmittedCtrl,pnlContainer.HeaderText %>">
    <asp:Panel ID="pnlSubmitErr" runat="server" HorizontalAlign="Center" Visible="false">
        <div class="settingrow">
            <p class="msg">
                <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
            </p>
        </div>
        <div class="settingrow" style="padding-top: 10px;">
            <ul class="ulhor1">
                <li style="padding: 15px;">
                    <asp:HyperLink ID="hlBackToUrl" runat="server" Text="Go Back" SkinID="button"></asp:HyperLink></li>
            </ul>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSubmitOk" runat="server" HorizontalAlign="Center" Visible="false">
        <div class="settingrow">
            <asp:Localize ID="lcalThankYou" runat="server" Text="<%$Resources:STCTRL_ProdReg_SubmittedCtrl,lcalThankYou.Text%>"></asp:Localize>

        </div>
        <div class="settingrow" style="padding-top: 20px;">
            <ul class="ulhor1">
                <li style="padding: 15px;">
                    <asp:HyperLink ID="hlRegisterMore" runat="server" Text="<%$Resources:STCTRL_ProdReg_SubmittedCtrl,hlRegisterMore.Text%>" NavigateUrl="~/myproduct/regproduct-select" SkinID="button" ></asp:HyperLink></li>
                <li style="padding: 15px;">
                    <asp:HyperLink ID="hlMyProducts" runat="server" Text="<%$Resources:STCTRL_ProdReg_SubmittedCtrl,hlMyProducts.Text%>" NavigateUrl="~/myproduct/home" SkinID="button" ></asp:HyperLink></li>
            </ul>
        </div>
    </asp:Panel>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_SubmittedCtrl" />
