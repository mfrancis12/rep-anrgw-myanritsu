﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class ProdReg_SubmittedCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid ProdRegCartWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.ProdRegCartWebAccessKey], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if (Session["SS_KeySurveyUrl"] != null)
                //{
                //    String surveyUrl = Session["SS_KeySurveyUrl"].ToString();
                //    if(!surveyUrl.IsNullOrEmptyString())
                //    {
                //        hlFeedBack.NavigateUrl = surveyUrl;
                //        hlFeedBack.Visible = true;
                //    }
                //}
                ProcessProductRegCart();
            }
        }

        private void ProcessProductRegCart()
        {
            Lib.ProductRegistration.Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(ProdRegCartWebAccessKey, true);

            if (cartg == null) //invalid cart or processed already
            {
                WebUtility.HttpRedirect(null, KeyDef.UrlList.MyProducts);
            }
            Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (prdCfg == null) WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);

            String errNextUrl = String.Empty;
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            try
            {
                if (cartg.CartItems == null || cartg.CartItems.Count < 1)
                {
                    errNextUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_SerialNumbers
                        , KeyDef.QSKeys.ProdRegCartWebAccessKey, HttpUtility.UrlEncode(ProdRegCartWebAccessKey.ToString()));
                    throw new ArgumentException(GetStaticResource("SnRequiredMsg"));
                }
                if (!prdCfg.Reg_AddOnFormID.IsNullOrEmptyGuid() && !cartg.IsAddOnFormCompletedForAllItems)
                {
                    errNextUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Other
                        , KeyDef.QSKeys.ProdRegCartWebAccessKey, HttpUtility.UrlEncode(ProdRegCartWebAccessKey.ToString()));
                    throw new ArgumentException(GetStaticResource("FormIncompleteMsg"));

                }

                Boolean atleastOneAutoApproved = false;
                EndUser_ProductRegUtility.EndUser_SubmitRegistration(cartg.CartWebAccessKey, out atleastOneAutoApproved);

                lcalThankYou.Text = lcalThankYou.Text.Replace("{cntMN}", cartg.ModelNumber);
                pnlSubmitErr.Visible = false;
                pnlSubmitOk.Visible = true;
                Session.Clear();
             
            }
            catch (Exception ex)
            {
                ltrMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
                pnlSubmitErr.Visible = true;
                pnlSubmitOk.Visible = false;
                if (errNextUrl.IsNullOrEmptyString())
                {
                    errNextUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_SerialNumbers
                         , KeyDef.QSKeys.ProdRegCartWebAccessKey, HttpUtility.UrlEncode(ProdRegCartWebAccessKey.ToString()));
                }
                hlBackToUrl.NavigateUrl = errNextUrl;

            }
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_SubmittedCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }
    }
}