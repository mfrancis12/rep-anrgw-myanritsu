﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectModelForRegCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.EndUser_Registration.SelectModelForRegCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_SupportedModelCtrl,pnlContainer.HeaderText %>">
    <asp:Panel ID="pnlSupportedModelSearch" runat="server" DefaultButton="bttMNSearch">
        <div class="settingrow">
            <asp:Localize ID="lcalModelSearchDesc" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,lcalModelSearchDesc.Text%>"></asp:Localize>
        </div>
        <div class="settingrow">
            <div class="width-60 group input-text required">
            <p class="error-message">
                <asp:Localize ID="lclErrorMessage" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize>
            </p>
            <asp:TextBox ID="txtMNSearch" runat="server" ></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="rvMnSearch" runat="server" ControlToValidate="txtMNSearch" ErrorMessage="<%$Resources:Common,ERROR_Required%>" SetFocusOnError="true" ValidationGroup="vgSelectMN" Display="Dynamic"></asp:RequiredFieldValidator>--%>
            </div>

            <div style="font-size: 0.92em;">
                <span>
                    <asp:Localize ID="lcalCompanyInfoTitle" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,txtMNSearch.HelpText%>"></asp:Localize>
                </span>
            </div>

            <div class="margin-top-15">
            <asp:Button ID="bttMNSearch" runat="server" class="button form-submit" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,bttMNSearch.Text%>" ValidationGroup="vgSelectMN" OnClick="bttMNSearch_Click" CssClass="" />
                </div>
        </div>
        
        <div class="settingrow">
            <br />
            <asp:Localize ID="lcalErrorMsg" runat="server" Visible="false"></asp:Localize>
        </div>
        <div class="settingrow">
            <dx:ASPxGridView ID="dxgvModelSearchResults" runat="server" AutoGenerateColumns="false" Width="100%" KeyFieldName="ModelNumber" Visible="false" SettingsLoadingPanel-Mode="ShowAsPopup" SettingsBehavior-AllowSort="true">
                <Columns>
                    <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Settings-AllowSort="True" Caption="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.ColMN%>"></dx:GridViewDataColumn>
                 <%--   <dx:GridViewDataColumn FieldName="ProductName" VisibleIndex="2" Settings-AllowSort="True" Caption="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,ProductName.Text%>">
                        <DataItemTemplate>
                            <span>
                                <asp:Literal ID="ltrProductName" runat="server" Text='<%# FindProductName(Eval("ModelNumber"))  %>'></asp:Literal></span>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>--%>
                   <%-- <dx:GridViewDataHyperLinkColumn FieldName="ModelNumber" VisibleIndex="3" Caption=" " Settings-AllowSort="False">
                        <PropertiesHyperLinkEdit Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.TextRegister%>" NavigateUrlFormatString="/myproduct/regproduct-serialnumbers?mn={0}"></PropertiesHyperLinkEdit>
                    </dx:GridViewDataHyperLinkColumn>--%>
                      <dx:GridViewDataColumn VisibleIndex="3" Caption=" " Settings-AllowSort="False">
                        <DataItemTemplate>
                            <asp:LinkButton ID="lnkRegister" runat="server" CommandArgument='<%# Eval("ModelNumber") %>' Text='<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.TextRegister%>' OnClick="lnkRegister_Click" SkinID="blueit" ></asp:LinkButton>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <%--  <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="3" Caption=" " Settings-AllowSort="False">
                        <DataItemTemplate>
                            <div class="settingrow">
                                <dx:ASPxHyperLink ID="dxhlRegister" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.TextRegister%>" Target="_blank" NavigateUrl='<%# FindRegisterUrl(Eval("ModelNumber"))  %>'></dx:ASPxHyperLink>
                            </div>
                        </DataItemTemplate>
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataColumn>--%>
                </Columns>
                <SettingsPager PageSize="90000000" ShowEmptyDataRows="false"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </asp:Panel>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_SupportedModelCtrl" />
</anrui:GlobalWebBoxedPanel>
