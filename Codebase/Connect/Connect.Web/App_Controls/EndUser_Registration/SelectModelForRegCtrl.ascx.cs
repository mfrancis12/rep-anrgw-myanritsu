﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls.EndUser_Registration
{
    public partial class SelectModelForRegCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
                WebUtility.GenerateProcessingScript(bttMNSearch, "onclick", processingText);
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    dxgvModelSearchResults.DataBind();
            //}
            //else 
            if (!String.IsNullOrEmpty(txtMNSearch.Text))
            {
                DoSearch();
            }

        }

        //protected void bttMNSearch_Click(object sender, EventArgs e)
        //{
        //}

        private void DoSearch()
        {
            String modelNumberSearch = txtMNSearch.Text.ConvertNullToEmptyString().Trim();
            dxgvModelSearchResults.DataSource = null;
            lcalErrorMsg.Text = GetStaticResource("lcalErrorMsg.Text");
            lcalErrorMsg.Visible = true;

            if (!modelNumberSearch.Contains("%") && !modelNumberSearch.Contains("--"))
            {
                List<Lib.Cfg.ProductConfig> dataFound = Lib.Cfg.ProductConfigBLL.FindAllActiveByModelKeyword(modelNumberSearch, true);
                if (dataFound != null && dataFound.Count > 0)
                {
                    dxgvModelSearchResults.DataSource = dataFound;
                    lcalErrorMsg.Visible = false;
                }
            }

            dxgvModelSearchResults.DataBind();
            dxgvModelSearchResults.Visible = true;

            ////App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory dataCache
            ////    = new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            ////DataTable tb = dataCache.GetActiveModels();
            ////DataView dv = null;

            ////if (tb != null && tb.Rows.Count > 0 && !modelNumberSearch.IsNullOrEmptyString())
            ////{
            ////    dv = tb.DefaultView;
            ////    if (modelNumberSearch.IsNullOrEmptyString())
            ////        dv.RowFilter = String.Empty;
            ////    else
            ////        dv.RowFilter = String.Format("ModelNumber LIKE '%{0}%'", modelNumberSearch);
            ////}

            ////if (dv == null)
            ////{
            ////    dxgvModelSearchResults.Visible = false;
            ////}
            ////else
            ////{
            ////    dxgvModelSearchResults.DataSource = dv;
            ////    dxgvModelSearchResults.DataBind();
            ////    dxgvModelSearchResults.Visible = true;
            ////}
        }

        protected String FindProductName(Object modelNumber)
        {
            Int32 cultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            String mn = ConvertUtility.ConvertNullToEmptyString(modelNumber).Trim();
            Lib.Product.ProductInfo pBasicInfoForCustomer = Lib.Product.ProductInfoBLL.SelectByModelNumber(mn
                , "n/a"
                , cultureGroupID);

            if (pBasicInfoForCustomer == null)
            {
                pBasicInfoForCustomer = Lib.Product.ProductInfoBLL.SelectByModelNumber(mn.Trim()
                , "n/a"
                , 1);//get it from en-US
            }

            if (pBasicInfoForCustomer == null) return String.Empty;
            return pBasicInfoForCustomer.ProductName;
        }

        #region Use this to redirect to different link to register
        //protected String FindRegisterUrl(Object modelNumber)
        //{
        //    String mn = ConvertUtility.ConvertNullToEmptyString(modelNumber).Trim();
        //    String Url = "https://www1.anritsu.co.jp/Download2/MService";
        //    Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(mn);
        //    if (pcfg != null)
        //    {
        //        List<ModelConfig> list = pcfg.ModelConfigs;
        //        foreach (ModelConfig listitem in list)
        //        {
        //            if (listitem.ModelConfigType.Contains(Lib.Cfg.ModelConfigTypeInfo.CONFIGTYPE_US_MMD))
        //            {
        //                Url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_SerialNumbers, KeyDef.QSKeys.ModelNumber, mn);
        //                break;
        //            }

        //        }

        //    }
        //    return Url;

        //}
        #endregion

        protected void bttMNSearch_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DoSearch();
            }
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_SupportedModelCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void lnkRegister_Click(object sender, EventArgs e)
        {
            LinkButton lnkbtnRegister = (LinkButton)sender;
            if (String.IsNullOrEmpty(lnkbtnRegister.CommandArgument))
                WebUtility.HttpRedirect(null, KeyDef.UrlList.RegProd_SelectModel);
            else
                SecurityCheckAndCreateCart(lnkbtnRegister.CommandArgument);
            
        }

        private void SecurityCheckAndCreateCart(String model)
        {
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            String userEmail = LoginUtility.GetCurrentUserOrSignIn().Email;
            if (model.IsNullOrEmptyString())
                WebUtility.HttpRedirect(null, KeyDef.UrlList.RegProd_SelectModel);
            else
            {
                #region " create new product registration cart "

                Lib.Cfg.ProductConfig prdCfg = ProductConfigUtility.SelectByModel(true, model);

                if (prdCfg == null)
                    WebUtility.HttpRedirect(this, KeyDef.UrlList.RegProd_SelectModel);//not supported
                else
                {
                    Guid webAccessKey = Lib.ProductRegistration.Acc_ProductRegCartBLL.CreateNewCart(accountID, userEmail, model.Trim());
                    String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_SerialNumbers
                        , KeyDef.QSKeys.ProdRegCartWebAccessKey, webAccessKey);
                    WebUtility.HttpRedirect(null, url.ToLowerInvariant());
                }
                #endregion
            }
        }
    }
}
