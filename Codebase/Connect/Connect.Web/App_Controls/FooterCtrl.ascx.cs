﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using ApiSdk;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FooterCtrl : UserControl
    {
        public FooterCtrl()
        {
            FooterCachePrefix = "FooterData";
        }

        private String FooterCachePrefix { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //pull Culture code from the session
            var gwCultureCode = Thread.CurrentThread.CurrentCulture.Name;
            if (String.IsNullOrEmpty(gwCultureCode)) gwCultureCode = "en-US";
            //load footer
            ltrFooter.Text = GetFooterHtml(Convert.ToString(gwCultureCode));
        }
        protected string GetFooterHtml(String gwCultureCode)
        {
            String footerContent = null;
            var apiUri =
                new Uri(ConfigurationManager.AppSettings["Connect.Web.GWSiteCoreAPI.FooterUrl"].Replace("[[CULTURE_CODE]]",
                    gwCultureCode));
            var apiParams = new Dictionary<string, string>();

            var rsClass = new ResponseClass();
            var response = rsClass.CallMethod<String>(apiUri, apiParams);
            footerContent = response.StatusCode == HttpStatusCode.OK
                ? Convert.ToString(response.Content) : string.Empty;
            return footerContent;
        }
    }
}