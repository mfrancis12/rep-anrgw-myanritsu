﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormAdmin_DetailCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.FormAdmin_DetailCtrl" %>

<%@ Register Src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" TagName="ResourceLocalizationItemCtrl"
    TagPrefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_DetailCtrl,pnlContainer.HeaderText %>'>
    <script type="text/javascript" language="javascript">
        function ConfirmOnDelete(deleteMessage) {
            if (confirm(deleteMessage) == true)
                return true;
            else
                return false;
        }
    </script>
    <div class="settingrow group input-text width-70 required">
        <label>
            <asp:Localize ID="lcalFormInternalName" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,lcalFormInternalName.Text%>"></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="lclErrMsg" runat="server" Text="<%$Resources:Common,ERROR_Required%>"></asp:Localize></p>
        <asp:TextBox ID="txtFormInternalName" runat="server" ValidationGroup="vgFormInfo"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvFormInternalName" runat="server" ControlToValidate="txtFormInternalName"
            ValidationGroup="vgFormInfo" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>--%>
    </div>
    <div class="settingrow group input-radio width-70">
        <label>
            <asp:Localize ID="lcalFormType" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,lcalFormType.Text%>"></asp:Localize>:
        </label>
        <asp:RadioButton ID="rdoFormTypeProdReg" runat="server" Checked="true" GroupName="FormTypeGroup"
            Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,rdoFormTypeProdReg.Text%>" />
        <span>&nbsp;</span>
        <asp:RadioButton ID="rdoFormTypeFeedback" runat="server" GroupName="FormTypeGroup"
            Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,rdoFormTypeFeedback.Text%>" />
    </div>
    <div class="settingrow">
        <asp:Button ID="bttSaveForm" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,bttSaveForm.Text%>"
            OnClick="bttSaveForm_Click" ValidationGroup="vgFormInfo" SkinID="submit-btn" />
        <asp:Button ID="bttDeleteForm" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,bttDeleteForm.Text%>"
            OnClick="bttDeleteForm_Click" Visible="false" />
    </div>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvModels" ClientInstanceName="gvModels" runat="server" Theme="AnritsuDevXTheme" OnBeforePerformDataSelect="gvModels_BeforePerformDataSelect"
            Width="100%" AutoGenerateColumns="false" KeyFieldName="ModelNumber" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="ModelNumber" Caption="Model Number" VisibleIndex="0" Width="150"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="AddOnFormName" Caption="Add-On Form Name" VisibleIndex="1" Width="150"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FeedbackFormName" VisibleIndex="2" Caption="Feedback Form Name" Width="350"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn VisibleIndex="3">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="details" NavigateUrl='<%#"/prodregadmin/modelconfigdetail?mn="+Eval("ModelNumber") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>

            </Columns>
          
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>


    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerFormHeaderText" runat="server" ShowHeader="true"
    HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_DetailCtrl,pnlContainerFormHeaderText.HeaderText %>'>
    <div class="settingrow">
        <asp:Localize ID="lcalFormHeaderTextDesc" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,lcalFormHeaderTextDesc.Text%>"></asp:Localize>
    </div>
    <div class="settingrow">
        <uc1:ResourceLocalizationItemCtrl ID="reslcFormHeaderText" runat="server" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerFieldsets" runat="server" ShowHeader="true"
    HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_DetailCtrl,pnlContainerFieldsets.HeaderText %>'>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvFieldsets" ClientInstanceName="gvFieldsets" runat="server" Theme="AnritsuDevXTheme" OnBeforePerformDataSelect="gvFieldsets_BeforePerformDataSelect"
            Width="100%" AutoGenerateColumns="False" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="OrderNum" Caption="Order" VisibleIndex="0" Width="150"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn VisibleIndex="1" Caption="Fieldsets">
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lcalFSName" runat="server" Text='<%# GetFieldsetLegend( Eval("FormID"), Eval("FieldsetID") )  %>'></dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn VisibleIndex="2">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="details" NavigateUrl='<%#"/siteadmin/formadmin/fieldsetdetails?frmid="+Eval("FormID")+"&fsid="+Eval("FieldsetID") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
          
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>

    </div>
    <div class="settingrow overflow width-60">
        <div class="group input-text required">
        <label>
            <asp:Localize ID="lcalNewFSTitleInEng" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,lcalNewFSTitleInEng.Text%>"></asp:Localize></label>
            <p class="error-message">
                <asp:Localize ID="lclNewTitlErr" runat="server" Text="*"></asp:Localize>
            </p>
            <asp:TextBox ID="txtNewFsTitleName" runat="server" ValidationGroup="vgNewFS"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="rfvNewFsTitleName" runat="server" ControlToValidate="txtNewFsTitleName"
            ValidationGroup="vgNewFS" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
        </div>
        <div class="group input-text">
            <label>
                <asp:Localize ID="lcalNewFSOrder" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,lcalNewFSOrder.Text%>"></asp:Localize></label>
            
                <asp:RangeValidator ID="rvNewFsOrder" runat="server" ControlToValidate="txtNewFSOrder"
                ValidationGroup="vgNewFS" Type="Integer" MinimumValue="0" MaximumValue="9999"
                ErrorMessage="*"></asp:RangeValidator>
            <asp:TextBox ID="txtNewFSOrder" runat="server" ValidationGroup="vgNewFS"
                Text="10"></asp:TextBox>
        </div>
        <div class="margin-top-15">
            <asp:Button ID="bttAddNewFS" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_DetailCtrl,bttAddNewFS.Text%>"
            OnClick="bttAddNewFS_Click" ValidationGroup="vgNewFS" SkinID="submit-btn" />
        </div>
        
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_DetailCtrl" />
