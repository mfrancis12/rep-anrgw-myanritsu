﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FormAdmin_DetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid FormID 
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.FormID], Guid.Empty);
            }
        }

        private Boolean LockDelete
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState["vsLockDeleteForm"], false); }
            set { ViewState["vsLockDeleteForm"] = value; }
        }

       

        protected void Page_Load(object sender, EventArgs e)
        {
            reslcFormHeaderText.ClassKey = "FRM_" + FormID.ToString().ToUpperInvariant();
            reslcFormHeaderText.ResKey = "HeaderText";
            bttDeleteForm.Attributes.Add("onclick", "return ConfirmOnDelete(" + GetStaticResource("ConfirmDeleteMsg") + ")");

            if (!Page.IsPostBack)
            {
                BindFormInfo();
            }
        }

        private void BindFormInfo()
        {
            Lib.DynamicForm.Frm_Form formInfo = Lib.BLL.BLLFrm_Form.SelectByFormID(FormID);
            if (formInfo == null)
            {
                pnlContainerFieldsets.Visible = false;
                pnlContainerFormHeaderText.Visible = false;
                return;
            }
            LockDelete = formInfo.LockDelete;
            txtFormInternalName.Text = formInfo.FormName;
            rdoFormTypeFeedback.Checked = formInfo.IsFeedbackForm;
         
            gvFieldsets.DataBind();

            cadgModels_buildTopLevel();
           
            //bttDeleteForm.Visible = !formInfo.LockDelete;
        }

        #region " fieldsets "
        private void buildTopLevel()
        {
            gvFieldsets.DataSource = Lib.BLL.BLLFrm_FormFieldset.SelectByFormID(FormID);
        }      
        #endregion

 
        private void cadgModels_buildTopLevel()
        {
            gvModels.DataBind();
            
        }


        public String GetFieldsetLegend(object formID, object fieldsetID)
        {
            Int32 fsID = ConvertUtility.ConvertToInt32(fieldsetID, 0);
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(ConvertUtility.ConvertToGuid(formID, Guid.Empty));
            String resKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(fsID) + "Legend";
            return GetGlobalResourceObject(classKey, resKey).ToString();
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_FormAdmin_DetailCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void bttSaveForm_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            if (FormID.IsNullOrEmptyGuid())
            {
                String formType = "product-registration";
                if (rdoFormTypeFeedback.Checked) formType = "feedback";
                Guid formID = Lib.BLL.BLLFrm_Form.Insert(txtFormInternalName.Text.Trim(), formType);
                if (!formID.IsNullOrEmptyGuid())
                {
                    WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}"
                        , App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_Details
                        , App_Lib.KeyDef.QSKeys.FormID, formID.ToString()));
                }
            }
            else
            {
                Lib.BLL.BLLFrm_Form.Update(FormID, txtFormInternalName.Text.Trim());
            }
        }

        protected void bttDeleteForm_Click(object sender, EventArgs e)
        {
            Lib.BLL.BLLFrm_Form.Delete(FormID);
            WebUtility.HttpRedirect(null, App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_List);
        }

        protected void bttAddNewFS_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            String newFSTitleInEnglish = txtNewFsTitleName.Text.Trim();
            if (newFSTitleInEnglish.IsNullOrEmptyString()) return;
            Int32 orderNum = ConvertUtility.ConvertToInt32(txtNewFSOrder.Text.Trim(), 10);

            Int32 newFieldsetID = Lib.BLL.BLLFrm_FormFieldset.Insert(FormID, orderNum, false, newFSTitleInEnglish);
            WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&{3}={4}"
                       , App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetDetails
                       , App_Lib.KeyDef.QSKeys.FormID, FormID.ToString()
                       , App_Lib.KeyDef.QSKeys.FieldsetID, newFieldsetID)
                       );
        }

        protected void gvModels_BeforePerformDataSelect(object sender, EventArgs e)
        {
            DataTable tb = Lib.BLL.BLLFrm_Form.SelectModelsByFormID(FormID);
            gvModels.DataSource=tb;
            bttDeleteForm.Visible = (tb != null && tb.Rows.Count < 1) && !LockDelete;

        }

        protected void gvFieldsets_BeforePerformDataSelect(object sender, EventArgs e)
        {
            buildTopLevel();
            
        }
    }
}