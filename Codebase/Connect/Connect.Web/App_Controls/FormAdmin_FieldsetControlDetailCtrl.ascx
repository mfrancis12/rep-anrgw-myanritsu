﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormAdmin_FieldsetControlDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.FormAdmin_FieldsetControlDetailCtrl" %>
<%@ Register src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" tagname="ResourceLocalizationItemCtrl" tagprefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
        <span class="settinglabel-15"><asp:Localize ID="lcalFsControlName" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalFsControlName.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtFsControlName" runat="server" SkinID="tbx-200"></asp:TextBox>
    </div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalFsControlOrder" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalFsControlOrder.Text%>"></asp:Localize></span>
    <asp:TextBox ID="txtFsControlOrder" runat="server" SkinID="tbx-100" Text="10"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalIsRequiredField" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalIsRequiredField.Text%>"></asp:Localize></span>
    <asp:CheckBox ID="cbxIsRequiredField" runat="server" />
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalFsControlWidth" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalFsControlWidth.Text%>"></asp:Localize></span>
    <asp:TextBox ID="txtFsControlWidth" runat="server" SkinID="tbx-100" Text=""></asp:TextBox>&nbsp;<span class="tiptext">example: 200px</span>
</div>
<div class="settingrow">
    <span class="settinglabel-15"><asp:Localize ID="lcalFsControlHeight" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalFsControlHeight.Text%>"></asp:Localize></span>
    <asp:TextBox ID="txtFsControlHeight" runat="server" SkinID="tbx-100" Text=""></asp:TextBox>
</div>
<div id="divFsControlDS" class="settingrow" runat="server" visible="false">
    <span class="settinglabel-15"><asp:Localize ID="lcalFsControlDataSource" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,lcalFsControlDataSource.Text%>"></asp:Localize></span>
    <asp:DropDownList ID="cmbCtrlEdt_DS" runat="server" DataTextField="DataSourceName" DataValueField="DataSourceID"></asp:DropDownList>
        <asp:HyperLink ID="hlManageDS" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,hlManageDS.Text%>" NavigateUrl="~/App_Features/DynamicForm/FormAdmin/DataSourceEdit" Target="_blank"></asp:HyperLink>
</div>
<div class="settingrow" style="text-align: right;">
    <asp:Button ID="bttSave" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,bttSave.Text%>" onclick="bttSave_Click" />
    <asp:Button ID="bttDelete" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,bttDelete.Text%>" onclick="bttDelete_Click" />
</div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerDisplayText" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,pnlContainerDisplayText.HeaderText%>">
<center><div class="settingrow"><uc1:ResourceLocalizationItemCtrl ID="resLcItemFsControlDisplayText" runat="server" /></div></center>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerReportText" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl,pnlContainerReportText.HeaderText%>">
<center><div class="settingrow"><uc1:ResourceLocalizationItemCtrl ID="resLcItemFsControlReportText" runat="server" /></div></center>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FieldsetControlDetailCtrl" />