﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FormAdmin_FieldsetControlDetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 FieldsetControlID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.FieldsetControlID], 0); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cmbCtrlEdt_DS.DataSource = Lib.BLL.BLLFrm_FormControlDataSource.SelectAll();
                cmbCtrlEdt_DS.DataBind();

                InitFieldsetControl();
            }
        }

        private void InitFieldsetControl()
        {
            Lib.DynamicForm.Frm_FormFieldsetControl fsctrl = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FieldsetControlID);
            if (fsctrl == null)
            {
                this.Visible = false;
                return;
            }

            txtFsControlName.Text = fsctrl.FieldName;
            txtFsControlOrder.Text = fsctrl.ControlOrder.ToString();
            txtFsControlWidth.Text = "";
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ptyWidth = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(fsctrl.Properties
                , App_Features.DynamicForm.UILib.FormControlPropertyKeys.CtrlWidth);
            if (ptyWidth != null) txtFsControlWidth.Text = ptyWidth.PropertyValue;

            txtFsControlHeight.Text = "";
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ptyHeight = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(fsctrl.Properties
                , App_Features.DynamicForm.UILib.FormControlPropertyKeys.CtrlHeight);
            if (ptyHeight != null) txtFsControlHeight.Text = ptyHeight.PropertyValue;

            divFsControlDS.Visible = fsctrl.ControlInfo.IsDataSourceRequired;
            if (fsctrl.ControlInfo.IsDataSourceRequired && fsctrl.DataSourceID > 0)
            {
                ListItem itemDS = cmbCtrlEdt_DS.Items.FindByValue(fsctrl.DataSourceID.ToString());
                if (itemDS != null) itemDS.Selected = true;
            }

            Lib.DynamicForm.Frm_FormFieldset frmfs = Lib.BLL.BLLFrm_FormFieldset.SelectByFieldsetID(fsctrl.FieldsetID);
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(frmfs.FormID);
            resLcItemFsControlDisplayText.ClassKey = classKey;
            resLcItemFsControlDisplayText.ResKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(fsctrl.FieldsetID, fsctrl.FwscID, App_Features.DynamicForm.UILib.FormControlPropertyKeys.DisplayText);

            resLcItemFsControlReportText.ClassKey = classKey;
            resLcItemFsControlReportText.ResKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(fsctrl.FieldsetID, fsctrl.FwscID, App_Features.DynamicForm.UILib.FormControlPropertyKeys.ReportText);

            //if (fsctrl.ControlInfo.ControlID == 5)//textbox
            //{
            //    divTextBoxMode.Visible = true;

            //    Lib.DynamicForm.Frm_FormFieldsetControlProperty textBoxMode = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(fsctrl.Properties
            //    , App_Features.DynamicForm.UILib.FormControlPropertyKeys.TextBoxMode);

            //    if(textBoxMode != null) rdoMultiLine.Checked = textBoxMode.PropertyValue.Equals("MultiLine");
            //}

            pnlContainerDisplayText.Visible = true;
            pnlContainerReportText.Visible = true;
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            SaveCtrl();
        }

        private void SaveCtrl()
        {
            Lib.DynamicForm.Frm_FormFieldsetControl fsctrl = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FieldsetControlID);
            if (fsctrl == null)
            {
                this.Visible = false;
                return;
            }

            Int32 ctrlOrder = ConvertUtility.ConvertToInt32(txtFsControlOrder.Text.Trim(), 10);
            Int32 dataSourceID = 0;
            if (fsctrl.ControlInfo.IsDataSourceRequired) dataSourceID = ConvertUtility.ConvertToInt32(cmbCtrlEdt_DS.SelectedValue, 0);
            String internalName = txtFsControlName.Text.Trim().Replace(" ", "-");
            Lib.BLL.BLLFrm_FormFieldsetControl.Update(fsctrl.FwscID, ctrlOrder, cbxIsRequiredField.Checked, dataSourceID, txtFsControlName.Text.Trim());
            Lib.BLL.BLLFrm_FormFieldsetControlProperty.InsertUpdate(fsctrl.FwscID, App_Features.DynamicForm.UILib.FormControlPropertyKeys.CtrlWidth, txtFsControlWidth.Text);
            Lib.BLL.BLLFrm_FormFieldsetControlProperty.InsertUpdate(fsctrl.FwscID, App_Features.DynamicForm.UILib.FormControlPropertyKeys.CtrlHeight, txtFsControlHeight.Text);
            //if (fsctrl.ControlInfo.ControlID == 5)//textbox
            //{
            //    Lib.BLL.BLLFrm_FormFieldsetControlProperty.InsertUpdate(fsctrl.FwscID, App_Features.DynamicForm.UILib.FormControlPropertyKeys.TextBoxMode
            //        , rdoMultiLine.Checked ? "MultiLine" : "SingleLine");
                
            //}

        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            int fieldsetID = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FieldsetControlID).FieldsetID;
            Guid formID = Lib.BLL.BLLFrm_FormFieldset.SelectByFieldsetID(fieldsetID).FormID;
            Lib.BLL.BLLFrm_FormFieldsetControl.Delete(FieldsetControlID);
            WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetDetails
                   , KeyDef.QSKeys.FormID, formID
                   , KeyDef.QSKeys.FieldsetID, fieldsetID
                   ));
        }
    }
}