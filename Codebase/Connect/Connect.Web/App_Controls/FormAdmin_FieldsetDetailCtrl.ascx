﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormAdmin_FieldsetDetailCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.FormAdmin_FieldsetDetailCtrl" %>
<%@ Register Src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" TagName="ResourceLocalizationItemCtrl" TagPrefix="uc1" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,pnlContainer.HeaderText%>">
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalOrderNum" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalOrderNum.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtOrderNum" runat="server" SkinID="tbx-100" Text="10"></asp:TextBox>
    </div>
    <div id="divNewFieldsetLegendInEnglish" class="settingrow" runat="server" visible="false">
        <span class="settinglabel">
            <asp:Localize ID="lcalNewFieldsetLegendInEnglish" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewFieldsetLegendInEnglish.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtNewFieldsetLegendInEnglish" runat="server" SkinID="tbx-200" ValidationGroup="vgNewFieldset"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvNewFieldsetLegendInEnglish" runat="server" ControlToValidate="txtNewFieldsetLegendInEnglish" ValidationGroup="vgNewFieldset" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
    </div>
    <div class="settingrow" style="text-align: right;">
        <asp:Button ID="bttSave" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,bttSave.Text%>" OnClick="bttSave_Click" />
        <asp:Button ID="bttDelete" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,bttDelete.Text%>" OnClick="bttDelete_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerFieldsetLegend" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,pnlContainerFieldsetLegend.HeaderText %>' Visible="false">
    <div class="settingrow">
        <center><uc1:ResourceLocalizationItemCtrl ID="resLcItemFieldsetLegend" runat="server" /></center>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerControls" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,pnlContainerControls.HeaderText %>' Visible="false">
    <div class="settingrow">

        <dx:ASPxGridView ID="gvControls" ClientInstanceName="gvControls" runat="server" Theme="AnritsuDevXTheme" OnBeforePerformDataSelect="gvControls_BeforePerformDataSelect"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="FwscID" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="ControlOrder" Caption="Control Order" VisibleIndex="0" Width="80"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="ControlInfo.ControlName" Caption="Control" VisibleIndex="1" Width="150"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FieldName" VisibleIndex="3" Caption="Field name (internal)" Width="350"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn VisibleIndex="3">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="lnkFieldDetails" runat="server" Text="field details" NavigateUrl='<%#"/siteadmin/formadmin/fieldsetcontroldetails?frmid="+Eval("FormID")+"&fsid="+Eval("FieldsetID")+"&fwscid="+Eval("FwscID") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FwscID" VisibleIndex="5" Visible="false"  Caption="Is Super Admin" Width="50" />
                <dx:GridViewDataColumn FieldName="FieldsetID" VisibleIndex="6" Visible="false"  Caption="Is Super Admin" Width="50" />
                <dx:GridViewDataColumn FieldName="FormID" VisibleIndex="7" Visible="false"  Caption="Is Super Admin" Width="50" />
            </Columns>

            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>

    </div>
    <div class="settingrow">
        <fieldset>
            <legend>
                <asp:Localize ID="lcalNewControlTitle" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewControlTitle.Text%>"></asp:Localize></legend>
            <div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalNewControlControlType" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewControlControlType.Text%>"></asp:Localize></span>
                <asp:DropDownList ID="cmbNewControlControlType" runat="server" DataTextField="ControlName" DataValueField="ControlID"></asp:DropDownList>
            </div>
            <div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalNewControlFieldName" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewFieldName.Text%>"></asp:Localize></span>
                <asp:TextBox ID="txtNewControlFieldName" runat="server" SkinID="tbx-200" ValidationGroup="vgNewControl"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNewControlFieldName" runat="server" ControlToValidate="txtNewControlFieldName" ValidationGroup="vgNewControl" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
            </div>
            <div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalNewControlOrder" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewControlOrder.Text%>"></asp:Localize></span>
                <asp:TextBox ID="txtNewControlOrder" runat="server" SkinID="tbx-200" ValidationGroup="vgNewControl"></asp:TextBox>
                <asp:RangeValidator ID="rvNewControlOrder" runat="server" ControlToValidate="txtNewControlOrder" ValidationGroup="vgNewControl" Type="Integer" MinimumValue="0" MaximumValue="9999" ErrorMessage="invalid"></asp:RangeValidator>
                <asp:CheckBox ID="cbxNewControlIsRequired" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,cbxNewControlIsRequired.Text%>" />
            </div>
            <div class="settingrow">
                <span class="settinglabel">
                    <asp:Localize ID="lcalNewControlDisplayTextInEng" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,lcalNewControlDisplayTextInEng.Text%>"></asp:Localize></span>
                <asp:TextBox ID="txtNewControlDisplayTextInEng" runat="server" SkinID="tbx-200" ValidationGroup="vgNewControl"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNewControlDisplayTextInEng" runat="server" ControlToValidate="txtNewControlDisplayTextInEng" ValidationGroup="vgNewControl" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
            </div>
            <div class="settingrow" style="text-align: right;">
                <asp:Button ID="bttAddNewControl" runat="server" SkinID="SmallButton" Text="<%$Resources:ADM_STCTRL_FormAdmin_FieldsetDetailCtrl,bttAddNewControl.Text%>" OnClick="bttAddNewControl_Click" ValidationGroup="vgNewControl" />
            </div>
        </fieldset>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FieldsetDetailCtrl" />
