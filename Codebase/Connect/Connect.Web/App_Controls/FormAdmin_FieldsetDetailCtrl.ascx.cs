﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FormAdmin_FieldsetDetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 FieldsetID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.FieldsetID], 0);  }
        }

        public Guid FormID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.FormID], Guid.Empty);
            }
        }

        public String FieldsetClassKey { get { return Lib.BLL.BLLFrm_Form.ResClassKey(FormID); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (FormID.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found.");
            if (!IsPostBack)
            {
                InitFieldset();
            }
            resLcItemFieldsetLegend.ClassKey = FieldsetClassKey;
            resLcItemFieldsetLegend.ResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(FieldsetID) + "Legend";

        }

        private void InitFieldset()
        {
            Lib.DynamicForm.Frm_FormFieldset fs = Lib.BLL.BLLFrm_FormFieldset.SelectByFieldsetID(FieldsetID);
            if (fs == null) 
            {
                divNewFieldsetLegendInEnglish.Visible = true;
                return;
            }
            if (fs.FormID != FormID) throw new HttpException(404, "Page not found.");//hack
            txtOrderNum.Text = fs.OrderNum.ToString();
                       gvControls.DataBind();
            cmbNewControlControlType.DataSource = Lib.BLL.BLLFrm_FormControl.FormControls;
            cmbNewControlControlType.DataBind();

            pnlContainerFieldsetLegend.Visible = true;
            pnlContainerControls.Visible = true;
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            Int32 orderNum = ConvertUtility.ConvertToInt32(txtOrderNum.Text.Trim(), 5);
            Boolean isInternal = false;
            if (FieldsetID < 1)//new
            {
                Page.Validate("vgNewFieldset");
                if (!Page.IsValid) return;
                Int32 newFieldsetID = Lib.BLL.BLLFrm_FormFieldset.Insert(FormID, orderNum, isInternal, txtNewFieldsetLegendInEnglish.Text.Trim());
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&{3}={4}"
                    , App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetDetails
                    , App_Lib.KeyDef.QSKeys.FormID, FormID.ToString()
                    , App_Lib.KeyDef.QSKeys.FieldsetID, newFieldsetID));
            }
            else
            {
                if (!Page.IsValid) return;
                Lib.BLL.BLLFrm_FormFieldset.Update(FieldsetID, orderNum, isInternal);
            }


        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            Lib.BLL.BLLFrm_FormFieldset.DeleteByFieldsetID(FieldsetID);
            WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}"
                   , App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_Details
                   , App_Lib.KeyDef.QSKeys.FormID, FormID.ToString()));
        }

        protected void bttAddNewControl_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            Lib.DynamicForm.Frm_FormFieldset fs = Lib.BLL.BLLFrm_FormFieldset.SelectByFieldsetID(FieldsetID);
            Int32 controlID = ConvertUtility.ConvertToInt32(cmbNewControlControlType.SelectedValue, 0);
            if (controlID < 1) throw new ArgumentNullException("controlID");
            String fieldName = txtNewControlFieldName.Text.Trim().Replace(" ", "-");
            Int32 orderNum = ConvertUtility.ConvertToInt32(txtNewControlOrder.Text.Trim(), 10);
            Int32 fwscID = Lib.BLL.BLLFrm_FormFieldsetControl.Insert(FieldsetID, controlID, orderNum, cbxNewControlIsRequired.Checked
                , fs.FormID
                , txtNewControlDisplayTextInEng.Text.Trim()
                , fieldName
                , fieldName);

            WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}&{3}={4}&{5}={6}"
                   , App_Lib.KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetControlDetails
                   , App_Lib.KeyDef.QSKeys.FormID, fs.FormID.ToString()
                   , App_Lib.KeyDef.QSKeys.FieldsetID, fs.FieldsetID
                   , App_Lib.KeyDef.QSKeys.FieldsetControlID, fwscID
                   ));
        }

        protected void gvControls_BeforePerformDataSelect(object sender, EventArgs e)
        {
            gvControls.DataSource = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFieldsetID(FieldsetID);

        }
    }
}