﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormAdmin_ListCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.FormAdmin_ListCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_FormAdmin_ListCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow">
        <asp:HyperLink ID="hlAddNewForm" runat="server" Text="<%$ Resources:ADM_STCTRL_FormAdmin_ListCtrl,hlAddNewForm.Text %>"
            NavigateUrl="~/siteadmin/formadmin/formdetails" SkinID="blueit"></asp:HyperLink>
    </div>
    <div class="settingrow">

        <dx:ASPxGridView ID="gvForms" ClientInstanceName="gvForms" runat="server" OnBeforePerformDataSelect="gvForms_OnBeforePerformDataSelect" Theme="AnritsuDevXTheme"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="FormID" OnDetailRowExpandedChanged="gvForms_OnDetailRowExpandedChanged" SettingsBehavior-AutoExpandAllGroups="false"  >
            <Columns>

                <dx:GridViewDataTextColumn FieldName="FormName" VisibleIndex="1" Caption="Form Internal Name"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="FormType" VisibleIndex="2" Caption="Form Type"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="FormID" Caption="FormID" VisibleIndex="3"/>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="4" Caption="Created On (UTC)">
                    <PropertiesDateEdit DisplayFormatString="d">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataColumn Caption="Form Details" VisibleIndex="5">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Form Details" NavigateUrl='<%#"~/siteadmin/formadmin/formdetails?frmid="+Eval("FormID") %>' />
                    </DataItemTemplate>

                </dx:GridViewDataColumn>
            </Columns>
           
            <SettingsDetail ShowDetailRow="True" />
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="gvFormDetails" ClientInstanceName="gvFormDetails" runat="server" OnBeforePerformDataSelect="gvFormDetails_OnBeforePerformDataSelect" Theme="AnritsuDevXTheme"
                        Width="100%" AutoGenerateColumns="False" KeyFieldName="FieldsetID" SettingsBehavior-AutoExpandAllGroups="false">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="OrderNum" Caption="Order" Width="50"  VisibleIndex="6" />
                            <dx:GridViewDataColumn FieldName="FormID" Caption="Fieldsets"  VisibleIndex="7" >
                                  <DataItemTemplate>
                        <%# GetFieldsetLegend(Eval("FormID"),Eval("FieldsetID")) %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                            <dx:GridViewDataTextColumn FieldName="FormID" Visible="false" />
                            <dx:GridViewDataTextColumn FieldName="FieldsetID" Visible="false" />
                        </Columns>
                    </dx:ASPxGridView>
                </DetailRow>
            </Templates>
             <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>
        
        
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_ListCtrl" />
</anrui:GlobalWebBoxedPanel>
