﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FormAdmin_ListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                gvForms.DataBind();
            }
        }

        public String GetFieldsetLegend(object formID, object fieldsetID)
        {
            Int32 fsID = ConvertUtility.ConvertToInt32(fieldsetID, 0);
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(ConvertUtility.ConvertToGuid(formID, Guid.Empty));
            String resKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(fsID) + "Legend";
            return GetGlobalResourceObject(classKey, resKey).ToString();
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_FormAdmin_ListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void gvForms_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvForms.DataSource = Lib.BLL.BLLFrm_Form.SelectAllAsTable();
            
        }

        protected void gvFormDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var formID = (Guid)detailGrid.GetMasterRowKeyValue();
            var details = Data.DAFrm_FormFieldset.SelectByFormID(formID); 
            detailGrid.DataSource = details;
        }

        protected void gvForms_OnDetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvFormDetails = gvForms.FindDetailRowTemplateControl(e.VisibleIndex, "gvFormDetails") as ASPxGridView;
                if (gvFormDetails != null)
                {
                    gvFormDetails.DataBind();
                }
            }
        }
    }
}