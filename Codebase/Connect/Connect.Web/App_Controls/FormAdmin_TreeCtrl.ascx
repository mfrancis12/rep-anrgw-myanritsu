﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormAdmin_TreeCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.FormAdmin_TreeCtrl" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_FormAdmin_TreeCtrl,pnlContainer.HeaderText %>">
<div class="settingrow">
    <dx:ASPxTreeView id="dxTv" runat="server" Width="182" Border-BorderStyle="None" >

    </dx:ASPxTreeView>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_TreeCtrl" />