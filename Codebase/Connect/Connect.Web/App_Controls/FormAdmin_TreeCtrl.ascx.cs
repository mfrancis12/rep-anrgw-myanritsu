﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class FormAdmin_TreeCtrl : System.Web.UI.UserControl
    {
        public Int32 FieldsetID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.FieldsetID], 0); }
        }

        public Int32 FieldsetControlID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.FieldsetControlID], 0); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) BuildTopLevel();
        }

        private void BuildTopLevel()
        {
            String imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/controls/componentart/icons/root.gif";
            String navigateUrl = KeyDef.UrlList.SiteAdminPages.FormAdmin_List;
            DevExpress.Web.TreeViewNode rootNode = CreateNode("Forms", imageUrl, navigateUrl, false);
            dxTv.Nodes.Add(rootNode);
            List<Lib.DynamicForm.Frm_Form> forms = Lib.BLL.BLLFrm_Form.SelectAll();
            imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/controls/componentart/icons/ball_glass_greenS.gif";
            foreach (Lib.DynamicForm.Frm_Form frm in forms)
            {
                navigateUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.FormAdmin_Details
                    , KeyDef.QSKeys.FormID, frm.FormID.ToString()
                    );
                DevExpress.Web.TreeViewNode newNode = CreateNode(frm.FormName, imageUrl, navigateUrl, false);
                rootNode.Nodes.Add(newNode);
                PopulateFieldsets(frm.FormID, newNode);
            }

        }

        private void PopulateFieldsets(Guid formID, DevExpress.Web.TreeViewNode node)
        {
            String imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/controls/componentart/icons/file.gif";
            if (formID.IsNullOrEmptyGuid()) return;
            List<Lib.DynamicForm.Frm_FormFieldset> fieldsets = Lib.BLL.BLLFrm_FormFieldset.SelectByFormID(formID);
            if (fieldsets == null) return;
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(formID);
            foreach (Lib.DynamicForm.Frm_FormFieldset fs in fieldsets)
            {
                String resKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(fs.FieldsetID) + "Legend";
                String fsName = this.GetGlobalResourceObject(classKey, resKey).ToString();
                String navigateUrl = String.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetDetails
                    , KeyDef.QSKeys.FormID, formID
                    , KeyDef.QSKeys.FieldsetID, fs.FieldsetID
                    );
                DevExpress.Web.TreeViewNode childNode = CreateNode(fsName, imageUrl, navigateUrl, true);
                node.Nodes.Add(childNode);
                if (Request.Path.Equals(KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetDetails.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase)
                    && FieldsetID > 0 && FieldsetID == fs.FieldsetID)
                {
                    dxTv.SelectedNode = childNode;
                    node.Expanded = true;
                }
                PopulateFields(formID, fs.FieldsetID, childNode);
            }
        }

        private void PopulateFields(Guid formID, Int32 fieldsetID, DevExpress.Web.TreeViewNode node)
        {
            String imageUrl = ConfigUtility.AppSettingGetValue("GwdataCdnPath") + "/images/legacy-images/controls/componentart/icons/file.gif";
            if (fieldsetID < 1) return;
            List<Lib.DynamicForm.Frm_FormFieldsetControl> controls = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFieldsetID(fieldsetID);
            if (controls == null) return;
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(formID);
            foreach (Lib.DynamicForm.Frm_FormFieldsetControl ctrl in controls)
            {
                String navigateUrl = String.Format("{0}?{1}={2}&{3}={4}&{5}={6}", KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetControlDetails
                    , KeyDef.QSKeys.FormID, formID
                    , KeyDef.QSKeys.FieldsetID, ctrl.FieldsetID
                    , KeyDef.QSKeys.FieldsetControlID, ctrl.FwscID
                    );
                DevExpress.Web.TreeViewNode childNode = CreateNode(ctrl.FieldName, imageUrl, navigateUrl, true);
                node.Nodes.Add(childNode);

                if (Request.Path.Equals(KeyDef.UrlList.SiteAdminPages.FormAdmin_FieldsetControlDetails.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase)
                    && FieldsetControlID > 0 && FieldsetControlID == ctrl.FwscID)
                {
                    dxTv.SelectedNode = childNode;
                    node.Expanded = true;
                }
            }
        }

        private DevExpress.Web.TreeViewNode CreateNode(string text, string imageurl, String navigateUrl, bool expanded)
        {
            DevExpress.Web.TreeViewNode node = new DevExpress.Web.TreeViewNode();
            node.Text = text;
            if (!String.IsNullOrEmpty(imageurl)) node.Image.Url = imageurl;
            node.Expanded = expanded;
            if (!String.IsNullOrEmpty(navigateUrl))
                node.NavigateUrl = navigateUrl;
            return node;
        }

    }
}