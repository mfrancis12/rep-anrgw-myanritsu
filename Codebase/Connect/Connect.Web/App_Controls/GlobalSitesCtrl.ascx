﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalSitesCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.GlobalSitesCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlGlobalSites" runat="server" ShowHeader="true" HeaderText=" ">
<div class="gwsbrmenu">
<ul>
<li><a title="Anritsu Japan" href="http://www.anritsu.com/ja-JP/home.aspx" target="_blank">Anritsu Japan</a></li>
<li><a title="Anritsu America" href="http://www.anritsu.com/en-US/home.aspx" target="_blank">Anritsu America</a></li>
<li><a title="Anritsu EMEA" href="http://www.anritsu.com/en-GB/home.aspx" target="_blank">Anritsu EMEA</a></li>
<li><a title="Anritsu Asia/Pacific" href="http://www.anritsu.com/en-AU/home.aspx" target="_blank">Anritsu Asia/Pacific</a></li>
<li><a title="Anritsu Taiwan" href="http://www.anritsu.com/zh-TW/home.aspx" target="_blank">Anritsu Taiwan</a></li>
<li><a title="Anritsu China" href="http://www.anritsu.com/zh-CN/home.aspx" target="_blank">Anritsu China</a></li>
</ul>
</div>
</anrui:GlobalWebBoxedPanel>