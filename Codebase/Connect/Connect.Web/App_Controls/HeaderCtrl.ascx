﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.HeaderCtrl" %>

<%--<div class="header small text-center">
        <a id="hlHome" runat="server" href="/home" class="logo desktop">
                <img id="imgAnrLogo" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/img/icons/logo-normal.png" alt="anritsu">
                <h1>anritsu</h1>
            </a>
    <a href="/home" style="margin: 8px 0 0 50px; display:inline-block">
        <img id="imgMyAnrLogo" src="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/appfiles/img/icons/myanritsulogo.png" alt="MyAnritsu">
    </a>
    <div class="nav">
        <div class="desktop">
               <span class="margin-right-15" style="vertical-align: middle"><asp:Literal ID="ltrLoginName" runat="server" Visible="false"></asp:Literal></span>
            <asp:HyperLink ID="hlSignOut" runat="server" Visible="false" NavigateUrl="~/spsignout"
                    Text="<%$ Resources:STCTRL_HeaderCtrl,hlSignOut.Text %>"></asp:HyperLink>
        </div>
    </div>
</div>--%>
<div class="header-area">

    <header id="global-header">
      <h1 class="header__logo">
        <a id="hlHome" runat="server" href="/home/">Anritsu Home | Anritsu America</a>
      </h1>
      <button class="header__menu-trigger">
        <span class="menu-trigger__hamburger"></span>
        <span class="menu-trigger__label">Menu</span>
      </button>
      <ul class="util-links">
        <li class="util__my-anritsu--is-active" title="My Anritsu">
          <button class="util__my-anritsu__btn">
            <span>My Anritsu</span>
          </button>
          <p class="util__my-anritsu__welcome"><% =GetCurrentUserName() %> (<a id="divManageOrg"  runat="server" class  ="blueit-hover" href="myaccount/selectcompany">Manage-Team</a>)</p>
          <div class="util__my-anritsu__wrapper">
            <p class="util__my-anritsu__title">My Anritsu</p>
            <button class="util__my-anritsu__close"><span class="btn--close"></span></button>
           <ul class="util__my-anritsu__list--active">
               <li class="util__my-anritsu__list__item">
                <a href="#" id="lnkmyaccount" runat="server" class="util__my-anritsu__list__item--myaccount"><span><%= GetGlobalResourceObject("STCTRL_HeaderCtrl", "hlMyAccount.Text").ToString()%></span></a>
              </li>
              <li class="util__my-anritsu__list__item">
                <a id="hlSignOut" runat="server" href="/spsignout" class="util__my-anritsu__list__item--logout"><span><%= GetGlobalResourceObject("STCTRL_HeaderCtrl", "hlSignOut.Text").ToString()%></span></a>
              </li>
            </ul>
          </div>
        </li>
        <li class="util__contact-us" title=<%= GetGlobalResourceObject("STCTRL_MyProductsCtrl", "hlSupport.Text.ContactUs").ToString()%>>
          <a href="/contactus/web-master" class=util__contact-us__btn>
            <span><%= GetGlobalResourceObject("STCTRL_MyProductsCtrl", "hlSupport.Text.ContactUs").ToString()%></span>
          </a>
        </li>
       
      </ul>
      <nav class="global-nav">
        <ul class="global-menu">
          <li class="global-menu__item">
            <button type="button" onclick= "window.open('/myproduct/home','_self')"><%= GetGlobalResourceObject("~/myproduct/home", "PageTitle").ToString().ToUpper()%></button>
          </li>
          <li class="global-menu__item">
            <button type="button" onclick= "window.open('/myproduct/regproduct-select','_self')" ><%= GetGlobalResourceObject("~/myproduct/regproduct", "TopMenuTitle").ToString().ToUpper()%></button>
          </li>
          <li class="global-menu__item">
            <button type="button"  onclick= "window.open('/myproduct/support','_self')"><%= GetGlobalResourceObject("~/myproduct/support", "PageTitle").ToString().ToUpper()%></button>
          </li>
          <li id="divDistInfoIcon" runat="server" class="global-menu__item">
            <button type="button"><a href="http://distributor.eu.anritsu.com" ><%= GetGlobalResourceObject("~/mydistributorinfo/home", "PageTitle").ToString().ToUpper()%></a></button>
          </li>
            <anrui:SiteAdminEditPagePanel ID="saepnlEditPage" runat="server" />
        </ul>
      </nav>
    </header>
  
  </div>
        
       