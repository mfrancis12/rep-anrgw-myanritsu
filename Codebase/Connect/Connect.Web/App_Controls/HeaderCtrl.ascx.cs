﻿using System;
using System.Web.UI;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using System.Web;
using System.Globalization;
using Anritsu.Connect.Web.App_Lib;
using System.Threading;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class HeaderCtrl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (!Page.User.Identity.IsAuthenticated) return;
            var acc = BLLAcc_AccountMaster.SelectByAccountID(AccountUtility.GetSelectedAccountID(false));
            if (acc == null) return;
            divManageOrg.HRef = "~/myaccount/selectcompany";
            divManageOrg.Title = GetGlobalResourceObject("~/myaccount/selectcompany", "PageTitle").ToString();
            divManageOrg.InnerText= HttpUtility.HtmlEncode(acc.AccountName);
            Boolean hasDistributorContent = (acc.Config_DistributorPortal != null && acc.Config_DistributorPortal.StatusCode.Equals("active", StringComparison.InvariantCultureIgnoreCase));
            divDistInfoIcon.Visible = hasDistributorContent;
           
            var qaParam = Convert.ToString(HttpContext.Current.Request[KeyDef.QSKeys.SiteLocale]);
            if (string.IsNullOrEmpty(qaParam))
            {
                qaParam = Thread.CurrentThread.CurrentCulture.Name;
            }
          
            var langUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
            lnkmyaccount.HRef = ConfigUtility.AppSettingGetValue("AnrSso.Idp.MyAccount");
            var lang = langUri.Query.Contains("?") ? langUri.Query.Replace('?', '&') : !string.IsNullOrEmpty(langUri.Query) ? string.Format("&{0}", langUri.Query) : string.Empty;
            if (!string.IsNullOrEmpty(lang))
            {
            lnkmyaccount.HRef = String.Format(ConfigUtility.AppSettingGetValue("AnrSso.Idp.MyAccount") + "?{0}={1}{2}"
                    , KeyDef.QSKeys.ReturnURL
                    , HttpUtility.UrlEncode(String.Format("{0}"
                    , HttpContext.Current.Request.Url.AbsoluteUri)), lang);
            }
            else
            {
            lnkmyaccount.HRef = String.Format(ConfigUtility.AppSettingGetValue("AnrSso.Idp.MyAccount") + "?{0}={1}"
                    , KeyDef.QSKeys.ReturnURL
                    , HttpUtility.UrlEncode(String.Format("{0}"
                    , HttpContext.Current.Request.Url.AbsoluteUri)));
            }
            
            hlHome.HRef = ConfigUtility.AppSettingGetValue("Connect.GlobalWebBaseUrl").Replace("[[LL-CC]]", qaParam);
          
            hlSignOut.HRef = "~/spsignout?lang=" + Request.QueryString[App_Lib.KeyDef.QSKeys.SiteLocale];
        }

        public string GetCurrentUserName()
        {
            string username = "My Anritsu";
            try
            {
                string lang = "AnrSso.GWIdp.LoginGreeting_EN-EN";
                username = ConfigUtility.AppSettingGetValue(lang);


                if (((Page.User.Identity.IsAuthenticated)))
                {
                    String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
                    lang = "AnrSso.GWIdp.LoginGreeting_" + browserLangFromApp.ToUpper();
                    username = ConfigUtility.AppSettingGetValue(lang);
                    username = username.Replace("[FIRSTNAME]", LoginUtility.GetCurrentUser(false).FirstName).Replace("[LASTNAME]", LoginUtility.GetCurrentUser(false).LastName);
                    if(username =="")
                        username = "Hello "+LoginUtility.GetCurrentUser(false).FirstName;
                }
            }
            catch
            {
                username = "My Anritsu";
            }

            return username;
        }

       
    }
}