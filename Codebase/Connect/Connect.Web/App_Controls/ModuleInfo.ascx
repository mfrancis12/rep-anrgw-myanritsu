﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleInfo.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ModuleInfo" %>
<anrui:GlobalWebBoxedPanel ID="pnlModuleInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ModuleInfo,pnlModuleInfo.HeaderText%>">
<div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalModule_ModuleID" runat="server" Text="<%$Resources:STCTRL_ModuleInfo,lcalModule_ModuleID.Text%>"></asp:Localize></span>
            <asp:Literal ID="ltrModule_ModuleID" runat="server" Text="new"></asp:Literal>
        </div>
    <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalModule_Feature" runat="server" Text="<%$Resources:STCTRL_ModuleInfo,lcalModule_Feature.Text%>"></asp:Localize></span>
            <asp:DropDownList ID="cmbModules" runat="server" DataTextField="FeatureName" DataValueField="FeatureID" Width="300px"></asp:DropDownList>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalModule_ModuleName" runat="server" Text="<%$Resources:STCTRL_ModuleInfo,lcalModule_ModuleName.Text%>"></asp:Localize></span>
            <asp:TextBox ID="txtModule_ModuleName" runat="server" SkinID="tbx-500"></asp:TextBox>
        </div>
         <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalModule_Others" runat="server" Text="<%$Resources:STCTRL_ModuleInfo,lcalModule_Others.Text%>"></asp:Localize></span>
            <table>
                <tbody>
                    <tr><td><asp:CheckBox ID="cbxShowHeader" runat="server" Text="Show Header" /></td>
                    <td><asp:CheckBox ID="cbxNoBorder" runat="server" Text="No Border" /></td>
                    <td><asp:CheckBox ID="cbxIsPublic" runat="server" Text="Is Public" /></td>
                    <td><asp:CheckBox ID="cbxHideFromLoggedInUser" runat="server" Text="Hide from logged in user" /></td></tr>
                </tbody>
            </table>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10">&nbsp;</span>
            <asp:Button ID="bttModuleSave" runat="server"  ValidationGroup="vgModuleDetails"
                Text="<%$Resources:STCTRL_ModuleInfo,bttModuleSave.Text%>" SkinID="SmallButton" 
                onclick="bttModuleSave_Click" />
        </div>
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ModuleInfo" />
</anrui:GlobalWebBoxedPanel>