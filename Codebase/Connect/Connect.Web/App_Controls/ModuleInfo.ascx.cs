﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ModuleInfo : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.ModuleID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitFields();

            }
        }

        private void InitFields()
        {
            cmbModules.DataSource = Lib.BLL.BLLSite_Feature.SelectAll();
            cmbModules.DataBind();

            LoadModuleDetails();
        }

        private void ResetFields()
        {
            ltrModule_ModuleID.Text = ModuleID.ToString();
            cmbModules.SelectedIndex = 0;
            txtModule_ModuleName.Text = "";
            cbxHideFromLoggedInUser.Checked = cbxIsPublic.Checked = cbxNoBorder.Checked = cbxShowHeader.Checked = false;
        }

        private void LoadModuleDetails()
        {
            ResetFields();
            ltrModule_ModuleID.Text = ModuleID.ToString();
            if (ModuleID < 1) return;
            Lib.UI.Site_Module m = Lib.BLL.BLLSite_Module.SelectByModuleID(ModuleID);
            if (m == null) return;
            cmbModules.SelectedValue = m.FeatureID.ToString();
            txtModule_ModuleName.Text = m.ModuleName;
            cbxShowHeader.Checked = m.ShowHeader;
            cbxNoBorder.Checked = m.NoBorder;
            cbxIsPublic.Checked = m.IsPublic;
            cbxHideFromLoggedInUser.Checked = m.HideFromLoggedInUser;
        }

        protected void bttModuleSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            Int32 moduleID = ConvertUtility.ConvertToInt32(ltrModule_ModuleID.Text, 0);
            Lib.UI.Site_Module m = Lib.BLL.BLLSite_Module.SelectByModuleID(moduleID);
            Boolean isNew = (m == null);
            if (m == null)
            {
                Int32 newModuleID = Lib.BLL.BLLSite_Module.Insert(ConvertUtility.ConvertToInt32(cmbModules.SelectedValue, 10)
                 , txtModule_ModuleName.Text
                 , cbxShowHeader.Checked
                 , cbxNoBorder.Checked
                 , cbxIsPublic.Checked
                 , cbxHideFromLoggedInUser.Checked);
                WebUtility.HttpRedirect(null, String.Format("editmodule?{0}={1}", App_Lib.KeyDef.QSKeys.ModuleID, newModuleID));
            }
            else
            {
                Lib.BLL.BLLSite_Module.Update(moduleID
                 , ConvertUtility.ConvertToInt32(cmbModules.SelectedValue, 10)
                 , txtModule_ModuleName.Text
                 , cbxShowHeader.Checked
                 , cbxNoBorder.Checked
                 , cbxIsPublic.Checked
                 , cbxHideFromLoggedInUser.Checked);
            }
            //ResetFields();
            //ModuleDataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ModuleInfo"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion
    }
}