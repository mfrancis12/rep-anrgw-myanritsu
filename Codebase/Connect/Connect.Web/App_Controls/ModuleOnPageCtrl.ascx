﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleOnPageCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ModuleOnPageCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlModuleOnPage" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ModuleOnPageCtrl,pnlModuleOnPage.HeaderText%>">
<fieldset>
    <legend>Add Module To Page</legend>
    <div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalModulePage_PageUrl" runat="server" Text="<%$Resources:STCTRL_ModuleOnPageCtrl,lcalModulePage_PageUrl.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtModulePage_PageUrl" runat="server" SkinID="tbx-500" Text="~/home"></asp:TextBox>
    </div>
    <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalModulePage_Cph" runat="server" Text="<%$Resources:STCTRL_ModuleOnPageCtrl,lcalModulePage_Cph.Text%>"></asp:Localize></span>
            <asp:DropDownList ID="cmbModulePageCPH" runat="server">
                <asp:ListItem Text="cphContentTop" Value="cphContentTop" Selected="True"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Top" Value="cphLeft_Top"></asp:ListItem>
                <asp:ListItem Text="cphContentBottom" Value="cphContentBottom"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterLeft" Value="cphContentCenterLeft"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterRight" Value="cphContentCenterRight"></asp:ListItem>
            </asp:DropDownList>
        </div>
    <div class="settingrow">
            <span class="settinglabel-10">&nbsp;</span>
            <asp:Button ID="bttModulePageAdd" runat="server" ValidationGroup="vgModuleDetails"
                Text="<%$Resources:STCTRL_ModuleOnPageCtrl,bttModulePageAdd.Text%>" SkinID="SmallButton" 
                onclick="bttModulePageAdd_Click" />
        </div>
        <label class="failureNotification"><asp:Literal ID="ltrAddModuleToPageMsg" runat="server"></asp:Literal></label>
</fieldset>
<fieldset>
    <legend>Module on Pages</legend>
     <center>
    <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" onitemcommand="dgList_ItemCommand" OnItemDataBound="dgList_ItemDataBound" ShowFooter="true">
        <Columns>
            <asp:BoundColumn DataField="PageUrl" HeaderText="PageUrl" ItemStyle-Width="300px" ItemStyle-CssClass="dgstyle1-item-lft"></asp:BoundColumn>
             <asp:TemplateColumn HeaderText="PlaceHolder">
                <ItemTemplate>
                    <asp:DropDownList ID="cmbModulePageItemCph" runat="server">
                <asp:ListItem Text="cphContentTop" Value="cphContentTop" Selected="True"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Top" Value="cphLeft_Top"></asp:ListItem>
                <asp:ListItem Text="cphContentBottom" Value="cphContentBottom"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterLeft" Value="cphContentCenterLeft"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterRight" Value="cphContentCenterRight"></asp:ListItem>
            </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Order">
                <ItemTemplate>
                    <asp:TextBox ID="txtModulePageLoadOrder" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LoadOrder") %>' SkinID="tbx-20"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="CreatedOnUTC" HeaderText="CreatedOnUTC" DataFormatString="{0:d}"></asp:BoundColumn>
             <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="bttModulePageUpdate" runat="server" Text="update" SkinID="SmallButton" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PageRefID") %>'  ValidationGroup=""/>
                    <asp:Button ID="bttModulePageDelete" runat="server" Text="delete" SkinID="SmallButton" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PageRefID") %>'  ValidationGroup=""/>
                </ItemTemplate>
                <FooterTemplate>
                    
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    </center>
</fieldset>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ModuleOnPageCtrl" />
</anrui:GlobalWebBoxedPanel>