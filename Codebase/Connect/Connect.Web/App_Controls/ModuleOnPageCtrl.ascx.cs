﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ModuleOnPageCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.ModuleID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ModuleOnPagesDataBind();
            }
        }

        protected void bttModulePageAdd_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid || ModuleID < 1) return;
            ltrAddModuleToPageMsg.Text = "";
            if (!txtModulePage_PageUrl.Text.StartsWith("~/"))
            {
                ltrAddModuleToPageMsg.Text = "Invalid page url.";
                return;
            }
            String pageUrl = txtModulePage_PageUrl.Text.Trim();
            String cph = cmbModulePageCPH.SelectedValue;
            Lib.BLL.BLLSite_ModulePage.Insert(ModuleID, pageUrl, cph);
            txtModulePage_PageUrl.Text = "";
            cmbModulePageCPH.SelectedIndex = 0;
            ModuleOnPagesDataBind();
        }

        private void ModuleOnPagesDataBind()
        {
            List<Lib.UI.Site_ModulePage> lst = Lib.BLL.BLLSite_ModulePage.SelectByModuleID(ModuleID);
            dgList.DataSource = lst;
            dgList.DataBind();
            dgList.Visible = (lst != null && lst.Count > 0);
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "UpdateCommand")
            {
                if (ModuleID < 1) return;
                Int32 pageRefID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                TextBox txtModulePageLoadOrder = e.Item.FindControl("txtModulePageLoadOrder") as TextBox;
                Int32 loadOrder = ConvertUtility.ConvertToInt32(txtModulePageLoadOrder.Text, 1);
                DropDownList cmbModulePageItemCph = e.Item.FindControl("cmbModulePageItemCph") as DropDownList;
                Lib.BLL.BLLSite_ModulePage.Update(pageRefID, loadOrder, cmbModulePageItemCph.SelectedValue);
                ModuleOnPagesDataBind();
            }
            else if (e.CommandName == "DeleteCommand")
            {
                if (ModuleID < 1) return;
                Int32 pageRefID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLSite_ModulePage.Delete(pageRefID);
                ModuleOnPagesDataBind();
            }
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList cmbModulePageItemCph = e.Item.FindControl("cmbModulePageItemCph") as DropDownList;
                cmbModulePageItemCph.SelectedValue = (DataBinder.Eval(e.Item.DataItem, "PlaceHolder")).ToString();
            }
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ModuleOnPageCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion
    }
}