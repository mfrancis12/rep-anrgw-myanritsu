﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleSettingCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ModuleSettingCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlModuleSettings" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ModuleSettingCtrl,pnlModuleSettings.HeaderText%>">
<div class="settingrow">
<asp:DataGrid ID="dgModuleSettings" runat="server" AutoGenerateColumns="false" 
                Width="100%" ShowFooter="true" onitemcommand="dgModuleSettings_ItemCommand">
                <Columns>
                    <asp:TemplateColumn HeaderText="Setting Key">
                        <ItemTemplate>
                            <asp:TextBox ID="txtModuleSettingKey" runat="server" SkinID="tbx-100" Text='<%# DataBinder.Eval(Container.DataItem, "SettingKey") %>'></asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtModuleSettingKeyNew" runat="server" SkinID="tbx-100"></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Setting Value" ItemStyle-CssClass="dgstyle1-item-lft" FooterStyle-CssClass="dgstyle1-item-lft">
                        <ItemTemplate>
                            <asp:TextBox ID="txtModuleSettingValue" runat="server" SkinID="tbx-400" Text='<%# DataBinder.Eval(Container.DataItem, "SettingValue") %>'></asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtModuleSettingValueNew" runat="server" SkinID="tbx-400" ></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn ItemStyle-CssClass="dgstyle1-item-lft" FooterStyle-CssClass="dgstyle1-item-lft">
                        <ItemTemplate>
                            <asp:Button ID="bttUpdate" runat="server" Text="update" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SettingKey") %>' />
                            <asp:Button ID="bttDelete" runat="server" Text="delete" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ModuleSettingID") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="bttAdd" runat="server" Text="add" CommandName="AddCommand" />
                        </FooterTemplate>
                    </asp:TemplateColumn>

                </Columns>                
            </asp:DataGrid>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ModuleSettingCtrl" />
</anrui:GlobalWebBoxedPanel>