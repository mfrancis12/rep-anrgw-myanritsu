﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ModuleSettingCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ModuleID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindModuleSettings();
            }
        }

        private void BindModuleSettings()
        {
            List<Lib.UI.Site_ModuleSetting> settings = Lib.BLL.BLLSite_ModuleSetting.SelectByModuleID(ModuleID);
            dgModuleSettings.DataSource = settings;
            dgModuleSettings.DataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ModuleSettingCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion

        protected void dgModuleSettings_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                TextBox txtModuleSettingKeyNew = (TextBox)e.Item.FindControl("txtModuleSettingKeyNew");
                TextBox txtModuleSettingValueNew = (TextBox)e.Item.FindControl("txtModuleSettingValueNew");
                if (String.IsNullOrEmpty(txtModuleSettingKeyNew.Text)) return;
                Lib.BLL.BLLSite_ModuleSetting.Save(ModuleID, txtModuleSettingKeyNew.Text.Trim(), HttpUtility.HtmlEncode(txtModuleSettingValueNew.Text.Trim()));
                BindModuleSettings();
            }
            else if (e.CommandName == "DeleteCommand")
            {
                Int32 moduleSettingID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLSite_ModuleSetting.DeleteByModuleSettingID(moduleSettingID);
                BindModuleSettings();
            }
            else if (e.CommandName == "UpdateCommand")
            {
                TextBox txtModuleSettingKey = (TextBox)e.Item.FindControl("txtModuleSettingKey");
                TextBox txtModuleSettingValue = (TextBox)e.Item.FindControl("txtModuleSettingValue");
                if (String.IsNullOrEmpty(txtModuleSettingKey.Text)) return;
                Lib.BLL.BLLSite_ModuleSetting.Save(ModuleID, txtModuleSettingKey.Text.Trim(),  HttpUtility.HtmlEncode(txtModuleSettingValue.Text.Trim()));
            }
        }
    }
}