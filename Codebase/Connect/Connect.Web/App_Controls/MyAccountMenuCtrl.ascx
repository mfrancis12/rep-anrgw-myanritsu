﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyAccountMenuCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.MyAccountMenuCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="false">
<div class="gwsbrmenu">
<ul>
<li><asp:HyperLink ID="hlManageUsers" runat="server" NavigateUrl="~/myaccount/manageusers" Text="<%$Resources:STCTRL_MyAccountMenuCtrl,hlManageUsers.Text%>"></asp:HyperLink></li>
<%--<li><asp:HyperLink ID="hlManageOrg" runat="server" NavigateUrl="~/myaccount/companyprofile" Text="<%$Resources:STCTRL_MyAccountMenuCtrl,hlManageOrg.Text%>"></asp:HyperLink></li>--%>
<li><asp:HyperLink ID="hlAddNew" runat="server" NavigateUrl="~/myaccount/companyprofile?addnew=1" Text="<%$Resources:STCTRL_MyAccountMenuCtrl,hlAddNew.Text%>"></asp:HyperLink></li>
<li><asp:HyperLink ID="hlMyOrganizations" runat="server" NavigateUrl="~/myaccount/selectcompany" Text="<%$Resources:STCTRL_MyAccountMenuCtrl,hlMyOrganizations.Text%>"></asp:HyperLink></li>
</ul>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_MyAccountMenuCtrl" />
</anrui:GlobalWebBoxedPanel>