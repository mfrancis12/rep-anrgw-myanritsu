﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class MyAccountMenuCtrl : System.Web.UI.UserControl
    {
        public Boolean AddAsNew
        {
            get
            {
                return Request.QueryString[App_Lib.KeyDef.QSKeys.AddNewOrg] == "1";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AddAsNew)
                {
                    hlManageUsers.Visible = false;
                    hlAddNew.Visible = false;
                    //hlManageOrg.Visible = false;
                    //hlMyOrganizations.Visible = false;
                }
                else if (SiteUtility.GetPageUrl().Equals(App_Lib.KeyDef.UrlList.ManageOrg))
                {
                   // hlManageOrg.Visible = false;
                }
            }
        }
    }
}