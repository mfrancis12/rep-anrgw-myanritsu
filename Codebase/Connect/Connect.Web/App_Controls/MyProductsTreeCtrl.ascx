﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyProductsTreeCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.MyProductsTreeCtrl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_MyProductsTreeCtrl,pnlContainer.HeaderText %>">
    <div class="settingrow">
<ComponentArt:TreeView id="TreeView1" Width="175" BorderStyle="None"
            AutoTheming="true" ClientIDMode="AutoID" ShowLines="true" LineImagesFolderUrl="static.cdn-anritsu.com/apps/connect/img"
            EnableViewState="true"
            runat="server" >
          </ComponentArt:TreeView>
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_MyProductsTreeCtrl" />
</anrui:GlobalWebBoxedPanel>