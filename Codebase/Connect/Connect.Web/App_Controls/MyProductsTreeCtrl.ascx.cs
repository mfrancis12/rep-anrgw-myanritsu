﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class MyProductsTreeCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) BuildTopLevel();
        }

        
        private void BuildTopLevel()
        {
            DataTable tbMyRegProds = UIHelper.MyRegisteredProductsCacheGet();
            if (tbMyRegProds == null)
            {
                this.Visible = false;
                return;
            }
            var distinctMNs = (from n in tbMyRegProds.AsEnumerable() select n.Field<String>("ModelNumber")).Distinct();
            String imageUrl = "https://static.cdn-anritsu.com/apps/connect/img/ModelNo.png";
            // changed to new icons for model and serial numbers
            //"https://static.cdn-anritsu.com/controls/componentart/icons/folders.gif";
            foreach (var mn in distinctMNs)
            {
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(mn, imageUrl, "", false);
                TreeView1.Nodes.Add(newNode);
                PopulateSubTree(mn, newNode);
            }
           
        }

        private void PopulateSubTree(String mn, ComponentArt.Web.UI.TreeViewNode node)
        {

            String imageUrl = "https://static.cdn-anritsu.com/apps/connect/img/serialNo.png";
            // changed to new icons for model and serial numbers
            //"https://static.cdn-anritsu.com/controls/componentart/icons/file.gif";
            DataTable tbMyRegProds = UIHelper.MyRegisteredProductsCacheGet();
            int activeSNCount = 0;

            if (tbMyRegProds != null)
            {
                //TreeView1.Nodes.Remove(node);
                var serials = (from r in tbMyRegProds.AsEnumerable() 
                               where r.Field<String>("ModelNumber").Equals(mn, StringComparison.InvariantCultureIgnoreCase)
                               select r);

                foreach (DataRow snR in serials)
                {
                    String statusCode = snR["StatusCode"].ToString();
                    if (statusCode != "active") continue;

                    Boolean isPaidSupportModel = ConvertUtility.ConvertToBoolean(snR["IsPaidSupportModel"], false);
                    DateTime jpSupportExpireOn = ConvertUtility.ConvertToDateTime(snR["JPSupportExpireOn"], DateTime.MinValue);
                    Boolean userACLRequired = ConvertUtility.ConvertToBoolean(snR["UserACLRequired"], false);
                    Boolean userHasAccess = ConvertUtility.ConvertToBoolean(snR["UserHasAccess"], false);
                    if (isPaidSupportModel && jpSupportExpireOn < DateTime.UtcNow) continue;  
                    if (userACLRequired && !userHasAccess) continue;

                    String webAccessKey = snR["WebAccessKey"].ToString();

                    String navigateUrl = String.Format("{0}?tab=tab02&{1}={2}", KeyDef.UrlList.ProductSupport, KeyDef.QSKeys.RegisteredProductWebAccessKey, webAccessKey);
                    ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(snR["SerialNumber"].ToString().StartsWith("serial n/a") ? "n/a" : snR["SerialNumber"].ToString(), imageUrl, navigateUrl, true);
                    node.Nodes.Add(childNode);
                    activeSNCount++;
                    if (webAccessKey.Equals(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], StringComparison.InvariantCultureIgnoreCase))
                    {
                        TreeView1.SelectedNode = childNode;
                        node.ExpandAll();
                    }
                }

            }
            if (activeSNCount < 1) TreeView1.Nodes.Remove(node);            
        }

        private ComponentArt.Web.UI.TreeViewNode CreateNode(string text, string imageurl, String navigateUrl, bool expanded)
        {
            ComponentArt.Web.UI.TreeViewNode node = new ComponentArt.Web.UI.TreeViewNode();
            node.Text = text;  
            if(!String.IsNullOrEmpty(imageurl)) node.ImageUrl = imageurl;
            node.Expanded = expanded;
            if (!String.IsNullOrEmpty(navigateUrl))
                node.NavigateUrl = navigateUrl;
            return node;
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}