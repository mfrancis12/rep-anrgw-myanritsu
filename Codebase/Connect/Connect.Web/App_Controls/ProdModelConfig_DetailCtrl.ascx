﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdModelConfig_DetailCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdModelConfig_DetailCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow" style="text-align: right">
        <asp:Button ID="bttRegNotRequired" SkinID="GreyButton" runat="server" Visible="false" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttRegNotRequired.Text %>'
            OnClick="bttRegNotRequired_Click" />
               <asp:Button ID="bttAllowRegistration" runat="server" Visible="false" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttAllowRegistration.Text %>'
            OnClick="bttAllowRegistration_Click" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalProductOwnerRegion" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalProductOwnerRegion.Text %>'></asp:Localize></span>
        <asp:DropDownList ID="cmbProductOwnerRegion" runat="server">
            <asp:ListItem Text="US" Value="US"></asp:ListItem>
            <asp:ListItem Text="JP" Value="JP"></asp:ListItem>
        </asp:DropDownList>
        <span class="msg">
            <asp:Localize ID="lcalNeedReview" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalNeedReview.Text %>'></asp:Localize></span>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalIsPaidSupportModel" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalIsPaidSupportModel.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxIsPaidSupportModel" runat="server" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalVerifyCompany" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalVerifyCompany.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxVerifyCompany" runat="server" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalVerifyProductSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalVerifyProductSN.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxVerifyProductSN" runat="server" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalSkipSerialNoChk" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalSkipSerialNoChk.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxSkipSerialNumber" runat="server" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalUseUserACL" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalUseUserACL.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxUseUserACL" runat="server" />
    </div>
    <%--  <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalDoNotAllowProdReg" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalDoNotAllowProdReg.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxDoNotAllowProdReg" runat="server" />
    </div>--%>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalAddOnForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalAddOnForm.Text %>'></asp:Localize></span>
        <asp:DropDownList ID="cmbAddOnForm" runat="server" AppendDataBoundItems="true" DataTextField="FormName"
            DataValueField="FormID">
            <asp:ListItem Text="- None" Value="none"></asp:ListItem>
        </asp:DropDownList>
        <asp:HyperLink ID="hlEditAddOnForm" runat="server" Text="edit form" Visible="false"
            NavigateUrl=""></asp:HyperLink>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalAddOnFormPerSN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalAddOnFormPerSN.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxAddOnFormPerSN" runat="server" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalFeedbackForm" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalFeedbackForm.Text %>'></asp:Localize>:
        </span>
        <asp:DropDownList ID="cmbFeedbackForm" runat="server" AppendDataBoundItems="true"
            DataTextField="FormName" DataValueField="FormID">
            <asp:ListItem Text="- None" Value="none"></asp:ListItem>
        </asp:DropDownList>
        <asp:HyperLink ID="hlEditFeedbackForm" runat="server" Text="edit form" Visible="false"
            NavigateUrl=""></asp:HyperLink>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-20">
            <asp:Localize ID="lcalProdImageUrl" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalProdImageUrl.Text %>'></asp:Localize></span>
        <asp:TextBox ID="txtProdImageUrl" runat="server" SkinID="tbx-400"></asp:TextBox>
    </div>
       <div class="settingrow" style="text-align: right">
        <span class="msg">
            <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="bttUpdate" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttUpdate.Text %>'
            OnClick="bttUpdate_Click" />
        <asp:Button ID="bttDelete" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttDelete.Text %>'
            OnClick="bttDelete_Click" Visible="false" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerModelTags" runat="server" ShowHeader="true"
    HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,pnlContainerModelTags.HeaderText %>'>
    <div class="settingrow">
        <center>
            <p>
                <asp:Localize ID="lcalModelTagDesc" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalModelTagDesc.Text %>"></asp:Localize></p>
        </center>
    </div>
    <div class="settingrow">
        <center>
            <asp:GridView ID="gvModelTags" runat="server" EmptyDataText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,gvModelTags.EmptyDataText %>'
                OnRowCommand="gvModelTags_RowCommand" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                ShowFooter="false">
                <Columns>
                    <asp:BoundField DataField="TagModelNumber" HeaderText="Tag Model Number" ItemStyle-Width="450px" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="bttDeleteTag" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ModelTagID") %>'
                                CommandName="DeleteTagCommand" Text="delete" CausesValidation="false" SkinID="SmallButton" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </center>
    </div>
    <div class="settingrow" style="text-align: center;">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalTagModelNumber" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalNewTagModelNumber.Text %>'></asp:Localize></span>
        <asp:TextBox ID="txtTagModelNumber" runat="server" SkinID="tbx-200" ValidationGroup="vgModelTag"></asp:TextBox>
        <asp:Button ID="bttAddTag" runat="server" Text="Add Model Tag" ValidationGroup="vgModelTag"
            SkinID="SmallButton" OnClick="bttAddTag_Click" />
        <asp:RequiredFieldValidator ID="rfvTagModelNumber" runat="server" ControlToValidate="txtTagModelNumber"
            ErrorMessage="<%$Resources:Common,ERROR_Required%>" ValidationGroup="vgModelTag"></asp:RequiredFieldValidator>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlContainerCustomText" runat="server" ShowHeader="true"
    HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,pnlContainerCustomText.HeaderText %>'>
    <div class="settingrow">
        <center>
            <p>
                <asp:Localize ID="lcalCustomTextDesc" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,lcalCustomTextDesc.Text %>"></asp:Localize><br />
            </p>
        </center>
        <div class="settingrow">
            <center>
                <asp:DataGrid ID="dgList" runat="server" ShowFooter="false" ShowHeader="false" AutoGenerateColumns="false"
                    OnItemCommand="dgList_ItemCommand" Width="90%">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Content" ItemStyle-CssClass="dgstyle1-item-lft" ItemStyle-Width="70%">
                            <ItemTemplate>
                                <%# HttpUtility.HtmlDecode(DataBinder.Eval(Container.DataItem, "CustomContentSnippet").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Locale">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Locale") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-CssClass="dgstyle1-item-lft" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server" SkinID="SmallButton" NavigateUrl='<%# BuildResEditUrl(DataBinder.Eval(Container.DataItem, "ContentID") ) %>'
                                    Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,hlEdit.Text %>'></asp:HyperLink>
                                <asp:Button ID="bttDelete" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_DetailCtrl,bttDelete.Text %>'
                                    SkinID="SmallButton" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ContentID") %>'
                                    Visible='<%# DataBinder.Eval(Container.DataItem, "Locale").ToString() != "en" %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </center>
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_DetailCtrl" />
