﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using ComponentArt.Web.UI;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdModelConfig_DetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString["mn"]).Trim().ToUpperInvariant();
            }
        }

        private Lib.Erp.Erp_ProductConfig_TBD PConfig
        {
            get
            {
                App_Lib.AppCacheFactories.ProductConfigCacheFactory dataCache = new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
                Lib.Erp.Erp_ProductConfig_TBD pcfg = dataCache.GetProductConfigInfo(ModelNumber);
                if (pcfg == null)
                {
                    WebUtility.HttpRedirect(null, App_Lib.KeyDef.UrlList.ErpProd_ConfigList);
                }
                return pcfg;
            }
        }

        private String CustomTextClassKey { get { return KeyDef.ResClassKeys.ProductCfgClassKey; } }
        private String CustomTextResKey { get { return string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, ModelNumber.Trim()); } }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);
            bttRegNotRequired.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmRegNotReqMsg") + "')";
            bttAllowRegistration.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmAllowRegMsg") + "')";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLocalizedStrings();
                InitControls();
                LoadProductModelConfig();
                pnlContainer.HeaderText += " " + ModelNumber;
            }
            ClearMessages();
        }

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            DeleteProductModelConfig();
        }

        protected void bttUpdate_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            UpdateProductModelConfig(true);
        }

        protected void bttRegNotRequired_Click(object sender, EventArgs e)
        {
            UpdateProductModelConfig(false);
            bttRegNotRequired.Visible = false;
            bttAllowRegistration.Visible = true;
        }

        protected void bttAllowRegistration_Click(object sender, EventArgs e)
        {
            UpdateProductModelConfig(true);
            bttRegNotRequired.Visible = true;
            bttAllowRegistration.Visible = false;
        }

        private void ClearMessages()
        {
            ltrMsg.Text = String.Empty;
        }

        private void InitControls()
        {
            List<Lib.DynamicForm.Frm_Form> feedbackForms = null;
            List<Lib.DynamicForm.Frm_Form> addOnForms = null;
            Lib.BLL.BLLFrm_Form.SelectAll(out addOnForms, out feedbackForms);
            cmbAddOnForm.DataSource = addOnForms;
            cmbAddOnForm.DataBind();

            cmbFeedbackForm.DataSource = feedbackForms;
            cmbFeedbackForm.DataBind();

            BindModelTags();
        }

        private void BindModelTags()
        {
            gvModelTags.DataSource = Lib.Erp.Erp_ProductConfig_ModelTagBLL.SelectByModelNumber(ModelNumber);
            gvModelTags.DataBind();
        }

        /// <summary>
        /// Checks whether user is super admin
        /// </summary>
        /// <returns></returns>
        public static Boolean IsAdminMode()
        {
            Lib.Security.Sec_UserMembership user = UIHelper.GetCurrentUserOrSignIn();
            Boolean isAdminMode = user.IsAdministrator;
            return isAdminMode;
        }

        private void LoadProductModelConfig()
        {
            Lib.Erp.Erp_ProductConfig_TBD pcfg = PConfig;
            cmbProductOwnerRegion.SelectedValue = pcfg.ProductOwnerRegion.ToUpperInvariant();
            cbxIsPaidSupportModel.Checked = pcfg.IsPaidSupportModel;
            cbxVerifyCompany.Checked = pcfg.VerifyCompany;
            cbxUseUserACL.Checked = pcfg.UserACLRequired;
            cbxVerifyProductSN.Checked = pcfg.VerifyProductSN;
            cbxSkipSerialNumber.Checked = pcfg.SkipSerialNumberValidation;
            txtProdImageUrl.Text = pcfg.ImageUrl;
            bttDelete.Visible = IsAdminMode();
            #region AllowRegistration

            if (pcfg.IsVisibleToUser)
            {
                bttRegNotRequired.Visible = true;
            }
            else
            {
                bttAllowRegistration.Visible = true;
            }

            #endregion

            #region " AddOnForm "
            if (pcfg.AddOnFormID.IsNullOrEmptyGuid())
            {
                cmbAddOnForm.SelectedValue = "none";
                hlEditAddOnForm.Visible = false;
            }
            else
            {
                ListItem addOnForm = cmbAddOnForm.Items.FindByValue(pcfg.AddOnFormID.ToString());
                if (addOnForm != null)
                {
                    addOnForm.Selected = true;

                    hlEditAddOnForm.NavigateUrl = String.Format("~/siteadmin/formadmin/formdetails?frmid={0}", pcfg.AddOnFormID.ToString());
                    hlEditAddOnForm.Visible = true;
                }
            }
            cbxAddOnFormPerSN.Checked = pcfg.AddOnFormPerSN;
            #endregion

            #region " FeedbackForm "
            if (pcfg.FeedbackFormID.IsNullOrEmptyGuid())
            {
                cmbFeedbackForm.SelectedValue = "none";
                hlEditFeedbackForm.Visible = false;
            }
            else
            {
                ListItem feedbackFormListItem = cmbFeedbackForm.Items.FindByValue(pcfg.FeedbackFormID.ToString());
                if (feedbackFormListItem != null)
                {
                    feedbackFormListItem.Selected = true;
                    hlEditFeedbackForm.NavigateUrl = String.Format("~/siteadmin/formadmin/formdetails?frmid={0}", pcfg.FeedbackFormID.ToString());
                    hlEditFeedbackForm.Visible = true;
                }
            }
            #endregion

            lcalNeedReview.Visible = !pcfg.IsActive;
        }

        private void UpdateProductModelConfig(Boolean IsVisibleToUser)
        {
            try
            {
                Lib.Erp.Erp_ProductConfig_TBD pcfg = PConfig;

                if (pcfg == null)
                {
                    this.Visible = false;
                    return;
                }
                pcfg.ProductOwnerRegion = cmbProductOwnerRegion.SelectedValue.ToUpperInvariant();
                pcfg.IsPaidSupportModel = cbxIsPaidSupportModel.Checked;
                pcfg.VerifyCompany = cbxVerifyCompany.Checked;
                pcfg.VerifyProductSN = cbxVerifyProductSN.Checked;
                pcfg.SkipSerialNumberValidation = cbxSkipSerialNumber.Checked;
                pcfg.IsVisibleToUser = IsVisibleToUser;
                pcfg.UserACLRequired = cbxUseUserACL.Checked;
                bttDelete.Visible = IsAdminMode();
                pcfg.AddOnFormID = Guid.Empty;
                if (!cmbAddOnForm.SelectedValue.Equals("none", StringComparison.InvariantCultureIgnoreCase))
                    pcfg.AddOnFormID = ConvertUtility.ConvertToGuid(cmbAddOnForm.SelectedValue, Guid.Empty);
                pcfg.AddOnFormPerSN = cbxAddOnFormPerSN.Checked;

                pcfg.FeedbackFormID = Guid.Empty;
                if (!cmbFeedbackForm.SelectedValue.Equals("none", StringComparison.InvariantCultureIgnoreCase))
                    pcfg.FeedbackFormID = ConvertUtility.ConvertToGuid(cmbFeedbackForm.SelectedValue, Guid.Empty);

                pcfg.ImageUrl = txtProdImageUrl.Text.Trim();
                pcfg.IsActive = true;
                Lib.Erp.Erp_ProductConfigBLL.InsertUpdate(pcfg);

                ClearCache();
                ltrMsg.Text = GetStaticResource("MSG_ModelConfigUpdated");
            }

            catch (Exception ex)
            {
                throw ex;
                //return string.Empty;
            }
            // WebUtility.HttpRedirect(this, Request.RawUrl);
        }

        private void DeleteProductModelConfig()
        {
            string mn = ModelNumber.Trim();
            Lib.Erp.Erp_ProductConfigBLL.DeleteAll(mn);
            String resKey = string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, mn);
            Lib.Content.Res_ContentStoreBLL.DeleteByClassKeyResKey(KeyDef.ResClassKeys.ProductCfgClassKey, resKey);
            ClearCache();

            WebUtility.HttpRedirect(this, App_Lib.KeyDef.UrlList.ErpProd_ConfigList);
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_ProdModelConfig_DetailCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        protected void gvModelTags_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteTagCommand")
            {
                Int32 modelTagID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.Erp.Erp_ProductConfig_ModelTagBLL.Delete(modelTagID);
                ClearCache();
                BindModelTags();
            }
        }

        protected void bttAddTag_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            if (txtTagModelNumber.Text.Trim().IsNullOrEmptyString()) return;
            Lib.Erp.Erp_ProductConfig_ModelTagBLL.Insert(ModelNumber, HttpUtility.HtmlEncode(txtTagModelNumber.Text.Trim().ToUpperInvariant()));

            ClearCache();

            BindModelTags();
        }

        private void ClearCache()
        {
            App_Lib.AppCacheFactories.ProductConfigCacheFactory prodCfgDataCache =
                new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
            prodCfgDataCache.RemoveFromCache(ModelNumber.Trim());

            App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory dataCache =
                new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            dataCache.TouchCacheFile();

            App_Lib.AppCacheFactories.DownloadCacheFactory dlDataCache =
                new App_Lib.AppCacheFactories.DownloadCacheFactory();
            dlDataCache.TouchCacheFile();

            App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory dlInActiveDataCache
               = new App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory();
            dlInActiveDataCache.TouchCacheFile();
        }

        #region CustomText

        public void BindLocalizedStrings()
        {
            String customTextClassKey = KeyDef.ResClassKeys.ProductCfgClassKey;
            String customTextResKey = string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, ModelNumber.Trim());
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(customTextClassKey, customTextResKey);
            if (tb == null || tb.Rows.Count < 1)
            {
                Int32 defaultContentID = CreateDefaultContent();
                if (defaultContentID < 1)
                {
                    this.Visible = false;
                    return;
                }
                tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(customTextClassKey, customTextResKey);
            }
            dgList.DataSource = tb;
            dgList.DataBind();
        }

        private Int32 CreateDefaultContent()
        {
            return Lib.Content.Res_ContentStoreBLL.Insert(CustomTextClassKey, CustomTextResKey, "en", string.Empty, true);
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                TextBox txtLocale = (TextBox)e.Item.FindControl("txtLocale");
                TextBox txtContent = (TextBox)e.Item.FindControl("txtContent");
                CultureInfo locale = new CultureInfo(txtLocale.Text.Trim());
                Lib.Content.Res_ContentStoreBLL.Insert(CustomTextClassKey, CustomTextResKey, locale.Name, txtContent.Text.Trim(), true);
                BindLocalizedStrings();
            }

            else if (e.CommandName == "DeleteCommand")
            {
                Int32 contentID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.Content.Res_ContentStoreBLL.DeleteByContentID(contentID);
                BindLocalizedStrings();
            }
        }

        public string BuildResEditUrl(object contentIDObj)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(contentIDObj, 0);
            if (contentID < 1) return String.Empty;
            return String.Format("~/siteadmin/contentadmin/contentdetails?contentid={0}&returnurl={1}", contentID, HttpUtility.UrlEncode(Request.RawUrl));
        }
        #endregion

    }
}