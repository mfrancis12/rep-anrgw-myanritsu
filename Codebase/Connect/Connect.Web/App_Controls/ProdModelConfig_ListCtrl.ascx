﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdModelConfig_ListCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdModelConfig_ListCtrl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow">
        <ComponentArt:DataGrid ID="cadgProductCfg" AutoTheming="true" ClientIDMode="AutoID"
            PagerStyle="Numbered" AllowColumnResizing="true" EnableViewState="false" RunningMode="Callback"
            ShowFooter="true" PageSize="15" AllowPaging="true" Width="100%" runat="server">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="ModelNumber">
                    <Columns>
                        <ComponentArt:GridColumn DataField="ModelNumber" HeadingText="Model Number" Width="100" />
                        <ComponentArt:GridColumn DataField="IsActive" HeadingText="Active" Width="50" />
                        <ComponentArt:GridColumn DataField="ProductOwnerRegion" HeadingText="Owner" Width="30" />
                        <ComponentArt:GridColumn DataField="IsPaidSupportModel" HeadingText="Paid Support"
                            Width="80" />
                        <ComponentArt:GridColumn DataField="VerifyCompany" HeadingText="Verify Company" Width="80" />
                        <ComponentArt:GridColumn DataField="VerifyProductSN" HeadingText="Verify ProductSN"
                            Width="80" />
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstDetails" Width="50" AllowSorting="False" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="gstDetails">
                    <Template>
                        <asp:HyperLink ID="hlDetails" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cadgProductCfg.Text_Details%>"
                            NavigateUrl='<%# "~/prodregadmin/productmodelconfigdetail?mn=" + Container.DataItem["ModelNumber"] %>'></asp:HyperLink>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:DataGrid>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalModelSearchDesc" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalModelSearchDesc.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtModelSearch" runat="server"></asp:TextBox>
        <asp:Button ID="bttModelSearch" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalFind.Text%>"
            SkinID="SmallButton" OnClick="bttModelSearch_Click" />
    </div>
    <div class="settingrow">
        &nbsp;</div>
    <div class="settingrow">
        <fieldset id="fsNew" style="position: relative;">
            <legend>
                <asp:Localize ID="lcalAddNewLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalAddNewLegend.Text %>'></asp:Localize></legend>
            <div class="settingrow">
                <span class="settinglabel">
                    <asp:Localize ID="lcalAddNewMN" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalAddNewMN.Text %>'></asp:Localize></span>
                <asp:TextBox ID="txtAddNewMN" runat="server"></asp:TextBox>
            </div>
            <div class="settingrow">
                <span class="settinglabel">
                    <asp:Localize ID="lcalProductOwnerRegion" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalProductOwnerRegion.Text %>'></asp:Localize>
                    <asp:RadioButton ID="rdoNewProdJP" runat="server" Style="font-weight: normal" GroupName="NewProductOwnerGroup"
                        Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,rdoNewProdJP.Text %>"
                        Checked="true" />
                    <asp:RadioButton ID="rdoNewProdUS" runat="server" Style="font-weight: normal" GroupName="NewProductOwnerGroup"
                        Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,rdoNewProdUS.Text %>" /></span>
            </div>
            <div class="settingrow">
                <asp:CheckBox ID="cbxNewProdIsPaidSupportModel" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cbxNewProdIsPaidSupportModel.Text %>" />
                <asp:CheckBox ID="cbxNewProdVerifyCompany" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cbxNewProdVerifyCompany.Text %>" />
                <asp:CheckBox ID="cbxNewProdVerifyProduct" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cbxNewProdVerifyProduct.Text %>" />
                <asp:CheckBox ID="cbxNewProdSkipSerialNoChk" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cbxNewProdSkipSerialNoChk.Text %>" />
            </div>
            <div class="settingrow">
                <span class="settinglabel">
                    <asp:Localize ID="lcalNewProdImageUrl" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalNewProdImageUrl.Text %>'></asp:Localize>:</span>
                <asp:TextBox ID="txtNewProdImageUrl" runat="server"></asp:TextBox>
            </div>
            <div class="settingrow">
                <span>
                    <asp:Button ID="bttAddNewMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,bttAddNewMN.Text%>"
                        SkinID="SmallButton" OnClick="bttAddNewMN_Click" />
                </span>
                <label>
                    <p class="msg">
                        <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
                    </p>
                </label>
            </div>
        </fieldset>
    </div>
    <div class="settingrow">
        &nbsp;</div>
    <div class="settingrow">
        <fieldset id="fsNeedReview" runat="server">
            <legend>
                <asp:Localize ID="lcalNeedReviewLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalNeedReviewLegend.Text %>'></asp:Localize></legend>
            <center>
                <label class="msg">
                    <asp:Localize ID="lcalNeedReviewLabel" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalNeedReviewLabel.Text %>'></asp:Localize></label>
                <div class="settingrow">
                    <ComponentArt:DataGrid ID="cdgNeedReview" AutoTheming="true" ClientIDMode="AutoID"
                        AllowColumnResizing="true" GroupBy="ProductOwnerRegion" PreExpandOnGroup="false"
                        EnableViewState="false" RunningMode="Callback" ShowFooter="false" AllowPaging="false"
                        Width="100%" runat="server">
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="ModelNumber">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="ProductOwnerRegion" HeadingText="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.ProductOwnerRegion.HeadingText%>"
                                        Width="100" />
                                    <ComponentArt:GridColumn DataField="ModelNumber" HeadingText="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.ModelNumber.HeadingText%>"
                                        Width="180" />
                                    <ComponentArt:GridColumn DataField="IsActive" HeadingText="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.IsActive.HeadingText%>"
                                        Width="50" />
                                    <%-- <ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.CreatedOnUTC.HeadingText%>"
                                        FormatString="MMM dd, yyyy" Width="120" />--%>
                                    <ComponentArt:GridColumn DataCellServerTemplateId="gstCreatedOnUTC" Width="120" HeadingText="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.CreatedOnUTC.HeadingText%>" />
                                    <ComponentArt:GridColumn DataCellServerTemplateId="gstNeedReviewDetails" AllowSorting="False" />
                                    <ComponentArt:GridColumn DataField="CreatedOnUTC" Visible="false" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ServerTemplates>
                            <ComponentArt:GridServerTemplate ID="gstNeedReviewDetails">
                                <Template>
                                    <asp:HyperLink ID="hlNeedReviewDetails" runat="server" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,cdgNeedReview.hlNeedReviewDetails.Text%>"
                                        NavigateUrl='<%# "~/prodregadmin/productmodelconfigdetail?mn=" + Container.DataItem["ModelNumber"] %>'></asp:HyperLink>
                                </Template>
                            </ComponentArt:GridServerTemplate>
                            <ComponentArt:GridServerTemplate ID="gstCreatedOnUTC">
                                <Template>
                                    <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["CreatedOnUTC"]) %>'></asp:Literal>
                                </Template>
                            </ComponentArt:GridServerTemplate>
                        </ServerTemplates>
                    </ComponentArt:DataGrid>
                </div>
            </center>
        </fieldset>
    </div>
    <div class="settingrow">
        &nbsp;</div>
    <div class="settingrow">
        <fieldset style="position: relative;">
            <legend>
                <asp:Localize ID="lcalClearCache" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,lcalClearCache.Text %>'></asp:Localize></legend>
            <center>
                <asp:Button ID="bttClearCacheActiveModels" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,bttClearCacheActiveModels.Text %>"
                    CausesValidation="false" OnClick="bttClearCacheActiveModels_Click" />
                <asp:Button ID="bttClearCacheInActiveModels" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,bttClearCacheInActiveModels.Text %>"
                    CausesValidation="false" OnClick="bttClearCacheInActiveModels_Click" />
                <asp:Button ID="bttClearCacheAllProdCfg" runat="server" Text="<%$ Resources:ADM_STCTRL_ProdModelConfig_ListCtrl,bttClearCacheAllProdCfg.Text %>"
                    CausesValidation="false" OnClick="bttClearCacheAllProdCfg_Click" />
            </center>
        </fieldset>
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdModelConfig_ListCtrl" />
</anrui:GlobalWebBoxedPanel>
