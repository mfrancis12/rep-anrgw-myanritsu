﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using ComponentArt.Web.UI;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdModelConfig_ListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.ModelNumber]).Trim().ToUpperInvariant();
            }
        }

        public String ProductOwnerRegion
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.ProductOwnerRegion]).Trim().ToUpperInvariant();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            cadgProductCfg.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            cadgProductCfg.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            cadgProductCfg.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            cadgProductCfg.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            cadgProductCfg.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(OnFilter);
            //cadgRegProducts.GroupCommand += new ComponentArt.Web.UI.Grid.GroupCommandEventHandler(OnGroup);

            cdgNeedReview.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(cdgNeedReview_OnNeedRebind);
            cdgNeedReview.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(cdgNeedReview_OnNeedDataSource);
            cdgNeedReview.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(cdgNeedReview_OnPageChanged);
            cdgNeedReview.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(cdgNeedReview_OnSort);
            cdgNeedReview.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(cdgNeedReview_OnFilter);
            cdgNeedReview.GroupCommand += new ComponentArt.Web.UI.Grid.GroupCommandEventHandler(cdgNeedReview_OnGroup);

            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cadgProductCfg_GetGridDataSource();
                cadgProductCfg.DataBind();

                //if (cadgRegProducts.Items != null && cadgRegProducts.Items.Count > 0)
                //{
                //   cadgRegProducts.Expand(cadgRegProducts.Items[0]);
                //}
                cdgNeedReview_GetGridDataSource();
                cdgNeedReview.DataBind();
                txtModelSearch.Text = ModelNumber;
            }
        }

        #region " cadgProductCfg "
        private void cadgProductCfg_GetGridDataSource()
        {
            //cadgProductCfg.DataSource = Lib.Erp.Erp_ProductConfigBLL.SelectWithFilter(ModelNumber, ProductOwnerRegion, true);

            App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory dataCache
               = new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            DataTable tb = dataCache.GetActiveModels();
            DataView dv = null;
            if (tb != null)
            {
                dv = tb.DefaultView;
                string filter = String.Empty;
                if (!ProductOwnerRegion.IsNullOrEmptyString())
                {
                    filter = String.Format("ProductOwnerRegion = '{0}'", ProductOwnerRegion.Trim());
                }

                if (!ModelNumber.IsNullOrEmptyString())
                {
                    filter += String.Format("{0}ModelNumber LIKE '{1}%'"
                        , filter.IsNullOrEmptyString() ? String.Empty : " AND "
                        , ModelNumber.Trim());
                }

                dv.RowFilter = filter;
            }
            cadgProductCfg.DataSource = dv;
        }

        public void OnNeedRebind(object sender, EventArgs oArgs)
        {
            //System.Threading.Thread.Sleep(200);
            cadgProductCfg.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs oArgs)
        {
            cadgProductCfg_GetGridDataSource();
        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cadgProductCfg.CurrentPageIndex = oArgs.NewIndex;
        }

        public void OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cadgProductCfg.Filter = oArgs.FilterExpression;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cadgProductCfg.Sort = oArgs.SortExpression;
        } 
        #endregion

        #region " cdgNeedReview "
        private void cdgNeedReview_GetGridDataSource()
        {
            App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory dataCache
               = new App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory();
            DataTable tb = dataCache.GetInActiveModels();
            DataView dv = null;
            if (tb != null)
            {
                dv = tb.DefaultView;
                string filter = String.Empty;
                if (!ProductOwnerRegion.IsNullOrEmptyString())
                {
                    filter = String.Format("ProductOwnerRegion = '{0}'", ProductOwnerRegion.Trim());
                }
                dv.RowFilter = filter;
            }
            cdgNeedReview.DataSource = dv;
            fsNeedReview.Visible = (tb != null && tb.Rows.Count > 0);
        }

        public void cdgNeedReview_OnNeedRebind(object sender, EventArgs oArgs)
        {
            cdgNeedReview.DataBind();
        }

        public void cdgNeedReview_OnNeedDataSource(object sender, EventArgs oArgs)
        {
            cdgNeedReview_GetGridDataSource();
        }

        public void cdgNeedReview_OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cdgNeedReview.CurrentPageIndex = oArgs.NewIndex;
        }

        public void cdgNeedReview_OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cdgNeedReview.Filter = oArgs.FilterExpression;
        }

        public void cdgNeedReview_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cdgNeedReview.Sort = oArgs.SortExpression;
        }

        public void cdgNeedReview_OnGroup(object sender, ComponentArt.Web.UI.GridGroupCommandEventArgs oArgs)
        {
            cdgNeedReview.GroupBy = oArgs.GroupExpression;
        }
        #endregion


        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = UIHelper.GetBrowserLang();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        protected void bttModelSearch_Click(object sender, EventArgs e)
        {
            String url = KeyDef.UrlList.ErpProd_ConfigList + "?";

            if (!ProductOwnerRegion.IsNullOrEmptyString()) url += String.Format("{0}={1}&", KeyDef.QSKeys.ProductOwnerRegion, ProductOwnerRegion.Trim());
            if (!txtModelSearch.Text.IsNullOrEmptyString()) url += String.Format("{0}={1}", KeyDef.QSKeys.ModelNumber, txtModelSearch.Text.Trim());
           
            WebUtility.HttpRedirect(null, url);
        }

        protected void bttAddNewMN_Click(object sender, EventArgs e)
        {
            if (txtAddNewMN.Text.IsNullOrEmptyString())
            {
                ltrMsg.Text = "Model number is required.";
                return;
            }
            String mn = txtAddNewMN.Text.Trim().ToUpperInvariant();
            Lib.Erp.Erp_ProductConfig_TBD pcfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(mn);
            if (pcfg != null)
            {
                ltrMsg.Text = String.Format("{0} is already in the database.  <a href='{1}?{2}={3}'>click here</a> to modify."
                    , mn
                    , App_Lib.KeyDef.UrlList.ErpProd_ConfigDetail.Replace("~", "")
                    , App_Lib.KeyDef.QSKeys.ModelNumber
                    , HttpUtility.UrlEncode(mn));
                return;
            }

            pcfg = new Lib.Erp.Erp_ProductConfig_TBD();
            pcfg.ModelNumber = txtAddNewMN.Text.Trim().ToUpperInvariant();
            pcfg.ProductOwnerRegion = rdoNewProdJP.Checked ? "JP" : "US";
            pcfg.VerifyCompany = cbxNewProdVerifyCompany.Checked;
            pcfg.VerifyProductSN = cbxNewProdVerifyProduct.Checked;
            pcfg.IsPaidSupportModel = cbxNewProdIsPaidSupportModel.Checked;
            pcfg.SkipSerialNumberValidation = cbxNewProdSkipSerialNoChk.Checked;
            pcfg.ImageUrl = txtNewProdImageUrl.Text.Trim();
            Lib.Erp.Erp_ProductConfigBLL.InsertUpdate(pcfg);

            App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory dataCache =
                new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            dataCache.TouchCacheFile();

            WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}"
                , App_Lib.KeyDef.UrlList.ErpProd_ConfigDetail
                , App_Lib.KeyDef.QSKeys.ModelNumber
                , HttpUtility.UrlEncode(mn)));
        }

        protected void bttClearCacheActiveModels_Click(object sender, EventArgs e)
        {
            App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory activeMNDataCache = new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            activeMNDataCache.TouchCacheFile();
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }

        protected void bttClearCacheAllProdCfg_Click(object sender, EventArgs e)
        {
            App_Lib.AppCacheFactories.ProductConfigCacheFactory prodCfgDataCache = new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
            prodCfgDataCache.TouchCacheFile();
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }

        protected void bttClearCacheInActiveModels_Click(object sender, EventArgs e)
        {
            App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory dataCache
             = new App_Lib.AppCacheFactories.ProductConfigInActiveModelCacheFactory();
            dataCache.TouchCacheFile();
             WebUtility.HttpRedirect(null, Request.RawUrl);
        }
    }
}