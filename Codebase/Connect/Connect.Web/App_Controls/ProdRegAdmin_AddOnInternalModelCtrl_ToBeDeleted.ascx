﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_AddOnInternalModelCtrl_ToBeDeleted.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_AddOnInternalModelCtrl_ToBeDeleted" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register Src="~/App_Controls/ProdRegAdmin_JapanUSBKeyCtrl.ascx" TagName="ProdRegAdmin_JapanUSBKeyCtrl"
    TagPrefix="uc1" %>
<div class="settingrow">
    <uc1:ProdRegAdmin_JapanUSBKeyCtrl ID="ProdRegAdmin_JapanUSBKeyCtrl1" runat="server" />
</div>
<anrui:GlobalWebBoxedPanel ID="pnlAddOnInternalModel" runat="server" ShowHeader="true"
    HeaderText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,pnlAddOnInternalModel.HeaderText%>">
    <div class="settingrow">
        <p>
            <asp:Localize ID="lcalAddOnDownloads" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,lcalAddOnDownloads.Text%>"></asp:Localize></p>
        <ComponentArt:DataGrid ID="cadgAddOnDownloads" AutoTheming="true" ClientIDMode="AutoID"
            PagerStyle="Numbered" AllowColumnResizing="true" AllowPaging="false" EnableViewState="true"
            AutoCallBackOnDelete="true" RunningMode="Callback" ShowFooter="false" Width="100%"
            runat="server">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="IntMnTagID">
                    <Columns>
                        <ComponentArt:GridColumn DataCellClientTemplateId="ctIntMnTagRemove" Width="20" Align="Center"
                            HeadingText=" " DataField="IntMnTagID" />
                        <ComponentArt:GridColumn DataField="IntModelNumber" Width="250" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_IntModelNumber%>" />
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstExpireOnUTC" Width="100" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_ExpOnUTC%>" />
                        <%--   <ComponentArt:GridColumn DataField="ExpireOnUTC" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_ExpOnUTC%>"
                            Width="100" FormatString="MMM dd, yyyy" />--%>
                        <ComponentArt:GridColumn DataField="CreatedBy" Width="150" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_CreatedBy%>" />
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstCreatedOnUTC" Width="80" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_CreatedOnUTC%>" />
                        <%--  <ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,cadgAddOnDownloads.HD_CreatedOnUTC%>"
                            Width="80" FormatString="MMM dd, yyyy" />--%>
                        <ComponentArt:GridColumn DataField="CreatedOnUTC" Visible="false" />
                        <ComponentArt:GridColumn DataField="ExpireOnUTC" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ctIntMnTagRemove">
                    <a title='remove' href="javascript:cadgAddOnDownloads_DeleteRow('## DataItem.ClientId ##')">
                        <span class='deleteicon'></span></a>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="gstExpireOnUTC">
                    <Template>
                        <asp:Literal ID="ltrExpireOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["ExpireOnUTC"]) %>'></asp:Literal>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="gstCreatedOnUTC">
                    <Template>
                        <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["CreatedOnUTC"]) %>'></asp:Literal>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:DataGrid>
    </div>
    <fieldset>
        <legend>
            <asp:Localize ID="lcalAddOnInternalMNAddNewLegend" runat="server" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,lcalAddOnInternalMNAddNewLegend.Text%>'></asp:Localize></legend>
        <label>
            <asp:Localize ID="lcalAddOnInternalMNAddNewDesc" runat="server" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,lcalAddOnInternalMNAddNewDesc.Text%>'></asp:Localize></label>
        <div class="settingrow">
            <span class="settinglabel-15">
                <asp:Localize ID="lcalAddOnDownloadsNew" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,lcalAddOnDownloadsNew.Text%>"></asp:Localize></span>
            <asp:DropDownList ID="cmbInternalMN" runat="server" DataTextField="IntModelNumber"
                DataValueField="IntModelNumber">
            </asp:DropDownList>
        </div>
        <div class="settingrow">
            <span class="settinglabel-15">
                <asp:Localize ID="lcalAddOnExp" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,lcalAddOnExp.Text%>"></asp:Localize></span>
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td>
                        <ComponentArt:Calendar ID="caAddOnExpireCalendarPicker" ControlType="Picker" PickerFormat="Custom"
                            PickerCustomFormat="MMM dd yyyy" PickerCssClass="picker" runat="server">
                            <ClientEvents>
                                <SelectionChanged EventHandler="caAddOnExpireCalendarPicker_onSelectionChanged" />
                            </ClientEvents>
                        </ComponentArt:Calendar>
                    </td>
                    <td style="font-size: 10px;">
                        &nbsp;
                    </td>
                    <td>
                        <img id="calendar_button" alt="" onclick="caAddOnExpireCalendar.setSelectedDate(caAddOnExpireCalendarPicker.getSelectedDate());caAddOnExpireCalendar.show();"
                            class="calendar_button" src="//static.cdn-anritsu.com/controls/componentart/icons/btn_calendar.gif"
                            width="25" height="22" />
                    </td>
                    <td>
                        <a href='javascript:caAddOnExpireCalendarPicker.ClearSelectedDate();caAddOnExpireCalendar.ClearSelectedDate();'>
                            clear date</a>
                    </td>
                </tr>
            </table>
            <ComponentArt:Calendar runat="server" ID="caAddOnExpireCalendar" PopUp="Custom" PopUpExpandControlId="calendar_button"
                AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false"
                ControlType="Calendar" AutoTheming="true" Width="170" Height="100">
                <ClientEvents>
                    <SelectionChanged EventHandler="caAddOnExpireCalendar_onSelectionChanged" />
                </ClientEvents>
            </ComponentArt:Calendar>
        </div>
        <div class="settingrow">
            <span class="msg">
                <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
            </span>
        </div>
        <div class="settingrow">
            <span class="settinglabel-15">&nbsp;</span>
            <asp:Button ID="bttAddOnInternalMNAddNew" runat="server" CausesValidation="false"
                Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl,bttAddOnInternalMNAddNew.Text%>"
                SkinID="SmallButton" OnClick="bttAddOnInternalMNAddNew_Click" />
        </div>
    </fieldset>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl" />
