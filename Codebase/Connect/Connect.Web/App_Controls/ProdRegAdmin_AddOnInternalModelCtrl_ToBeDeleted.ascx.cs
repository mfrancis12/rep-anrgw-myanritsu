﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    //public partial class ProdRegAdmin_AddOnInternalModelCtrl_ToBeDeleted : CustomControlBase.ProdRegAdmin_DetailBase, App_Lib.UI.IStaticLocalizedCtrl
    //{
    //    protected override void OnInit(EventArgs e)
    //    {

    //        cadgAddOnDownloads.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(cadgAddOnDownloads_OnNeedRebind);
    //        cadgAddOnDownloads.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(cadgAddOnDownloads_OnNeedDataSource);
    //        cadgAddOnDownloads.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(cadgAddOnDownloads_OnSort);
    //        cadgAddOnDownloads.DeleteCommand += new ComponentArt.Web.UI.Grid.GridItemEventHandler(cadgAddOnDownloads_DeleteCommand);
    //        base.OnInit(e);

    //        if (!IsPostBack)
    //        {
    //            String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
    //            WebUtility.GenerateProcessingScript(bttAddOnInternalMNAddNew, "onclick", processingText);
    //        }
    //    }

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        if (!IsPostBack)
    //        {
    //            if (ProductReg == null)
    //            {
    //                this.Visible = false;
    //                return;
    //            }

    //            cmbInternalMN.DataSource = Lib.Erp.IntModelMasterBLL.SelectAll();
    //            cmbInternalMN.DataBind();

    //            AddOnInternalModelDataBind();
    //            // ProdRegAdmin_JapanUSBKeyCtrl1.ClearControls();
    //        }
    //    }

    //    private void AddOnInternalModelDataBind()
    //    {
    //        cadgAddOnDownloads_GetDataSource();
    //        cadgAddOnDownloads.DataBind();
    //    }

    //    #region " cadgAddOnDownloads "
    //    private void cadgAddOnDownloads_GetDataSource()
    //    {
    //        Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;
    //        if (prodReg == null) return;

    //        DataTable tbAddOn = new DataTable();
    //        DataColumn col = new DataColumn("IntMnTagID", typeof(Int32));
    //        tbAddOn.Columns.Add(col);
    //        col = new DataColumn("IntModelNumber", typeof(String));
    //        tbAddOn.Columns.Add(col);
    //        col = new DataColumn("ExpireOnUTC", typeof(DateTime));
    //        tbAddOn.Columns.Add(col);
    //        col = new DataColumn("CreatedBy", typeof(String));
    //        tbAddOn.Columns.Add(col);
    //        col = new DataColumn("CreatedOnUTC", typeof(DateTime));
    //        tbAddOn.Columns.Add(col);

    //        col = new DataColumn("IsExpired", typeof(Boolean));
    //        col.DefaultValue = false;
    //        tbAddOn.Columns.Add(col);
    //        tbAddOn.AcceptChanges();

    //        if (prodReg.InternalModels != null)
    //        {
    //            foreach (Lib.Erp.ProdReg_IntModel actAddOn in prodReg.InternalModels)
    //            {
    //                DataRow r = tbAddOn.NewRow();
    //                r["IntMnTagID"] = actAddOn.IntMnTagID;
    //                r["IntModelNumber"] = actAddOn.IntModelNumber;
    //                if (actAddOn.ExpireOnUTC != DateTime.MinValue)
    //                {
    //                    r["ExpireOnUTC"] = actAddOn.ExpireOnUTC;
    //                }
    //                r["IsExpired"] = false;
    //                r["CreatedBy"] = actAddOn.CreatedBy;
    //                r["CreatedOnUTC"] = actAddOn.CreatedOnUTC;
    //                tbAddOn.Rows.Add(r);
    //            }
    //        }

    //        if (prodReg.InternalModelsExpired != null)
    //        {
    //            foreach (Lib.Erp.ProdReg_IntModel expAddOn in prodReg.InternalModelsExpired)
    //            {
    //                DataRow r = tbAddOn.NewRow();
    //                r["IntMnTagID"] = expAddOn.IntMnTagID;
    //                r["IntModelNumber"] = expAddOn.IntModelNumber;
    //                if (expAddOn.ExpireOnUTC != DateTime.MinValue)
    //                {
    //                    r["ExpireOnUTC"] = expAddOn.ExpireOnUTC;
    //                }
    //                r["IsExpired"] = true;
    //                r["CreatedBy"] = expAddOn.CreatedBy;
    //                r["CreatedOnUTC"] = expAddOn.CreatedOnUTC;
    //                tbAddOn.Rows.Add(r);
    //            }
    //        }

    //        tbAddOn.AcceptChanges();
    //        cadgAddOnDownloads.DataSource = tbAddOn;
    //    }

    //    public void cadgAddOnDownloads_OnNeedRebind(object sender, EventArgs e)
    //    {
    //        cadgAddOnDownloads.DataBind();
    //    }

    //    public void cadgAddOnDownloads_OnNeedDataSource(object sender, EventArgs e)
    //    {
    //        cadgAddOnDownloads_GetDataSource();
    //    }

    //    public void cadgAddOnDownloads_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
    //    {
    //        cadgAddOnDownloads.Sort = oArgs.SortExpression;
    //    }

    //    private void cadgAddOnDownloads_DeleteCommand(object sender, ComponentArt.Web.UI.GridItemEventArgs e)
    //    {
    //        Int32 intMnTagID = ConvertUtility.ConvertToInt32(e.Item["IntMnTagID"], 0);

    //        Lib.Erp.ProdReg_IntModelBLL.DeleteByIntMnTagID(intMnTagID);
    //        UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);
    //        cadgAddOnDownloads.DataBind();
    //        Response.Redirect(Request.RawUrl);
    //    }
    //    #endregion

    //    public string StaticResourceClassKey
    //    {
    //        get { return "ADM_STCTRL_ProdRegAdmin_AddOnInternalModelCtrl"; }
    //    }

    //    public string GetStaticResource(string resourceKey)
    //    {
    //        return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
    //    }

    //    public String GetDate(object dateTimeString)
    //    {
    //        if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
    //        String llCC = UIHelper.GetBrowserLang();
    //        CultureInfo culture = new CultureInfo(llCC);
    //        DateTime updatedt = Convert.ToDateTime(dateTimeString);

    //        return updatedt.ToString("d", culture);
    //    }

    //    protected void bttAddOnInternalMNAddNew_Click(object sender, EventArgs e)
    //    {
    //        String selectedModel = cmbInternalMN.SelectedValue.Trim();
    //        DateTime dtExp = DateTime.MinValue;
    //        if (caAddOnExpireCalendar.SelectedDate != DateTime.MinValue)
    //            dtExp = caAddOnExpireCalendar.SelectedDate;
    //        if (selectedModel.IsNullOrEmptyString()) return;

    //        Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;
    //        if (prodReg == null) return;

    //        List<Lib.Erp.ProdReg_IntModel> internalModels = null;
    //        Boolean isModelAlreadyAdded = false;
    //        internalModels = Lib.Erp.ProdReg_IntModelBLL.GetIncludedmodelsByProdRegID(prodReg.ProdRegID, out internalModels);

    //        if (internalModels != null)
    //        {
    //            foreach (Lib.Erp.ProdReg_IntModel intModel in internalModels)
    //            {
    //                if (intModel.IntModelNumber.Equals(selectedModel))
    //                {
    //                    isModelAlreadyAdded = true;
    //                    break;
    //                }

    //            }
    //        }

    //        if (!isModelAlreadyAdded)
    //        {
    //            Lib.Erp.ProdReg_IntModelBLL.Insert(prodReg.ProdRegID
    //                , selectedModel
    //                , UIHelper.GetCurrentUserOrSignIn().Email
    //                , dtExp);
    //            ClearCache(prodReg.ProdRegID);
    //            cadgAddOnDownloads.DataBind();
    //            Response.Redirect(Request.RawUrl);
    //        }
    //        else
    //        {
    //            ltrMsg.Text = GetStaticResource("MSG_AlreadyAdded");
    //        }
    //    }

    //    private void ClearCache(Int32 prodRegID)
    //    {
    //        App_Lib.AppCacheFactories.DownloadCacheFactory dlDataCache =
    //               new App_Lib.AppCacheFactories.DownloadCacheFactory();
    //        dlDataCache.TouchCacheFile();
    //        dlDataCache.RemoveFromCache(prodRegID);
    //        UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);
    //    }
    //}
}