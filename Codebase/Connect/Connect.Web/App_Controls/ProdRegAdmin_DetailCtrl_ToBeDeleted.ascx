﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_DetailCtrl_ToBeDeleted.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_DetailCtrl_ToBeDeleted" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%--<%@ Register Src="~/App_Controls/ProdRegAdmin_JapanUSBKeyCtrl.ascx" TagName="ProdRegAdmin_JapanUSBKeyCtrl"
    TagPrefix="uc1" %>--%>
<anrui:GlobalWebBoxedPanel ID="pnlProdRegInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,pnlProdRegInfo.HeaderText%>">
    <script type="text/javascript">
        function editGrid(rowId) {
            cadgProdRegData.edit(cadgProdRegData.getItemFromClientId(rowId));
        }
        function editRow() {
            cadgProdRegData.editComplete();
        }
        
    </script>
    <div class="settingrow">
        <table>
            <tr>
                <td style="width: 50px;">
                    <span class="settinglabel">
                        <asp:Localize ID="lcalMN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalMN.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrMN" runat="server"></asp:Literal>
                </td>
                <td style="width: 10px;">
                    &nbsp;
                </td>
                <td align="right">
                    <span class="settinglabel">
                        <asp:Localize ID="lcalSN" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalSN.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrSN" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalStatusCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalStatusCode.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="cmbProdRegStatus" runat="server">
        </asp:DropDownList>
    </div>
    <div class="settingrow">
        <span class="settinglabel">
            <asp:Localize ID="lcalModelTarget" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalModelTarget.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="cmbProdRegModelTarget" runat="server">
            <asp:ListItem Text="not available" Value="none" Selected="True"></asp:ListItem>
            <asp:ListItem Text="UH" Value="UH"></asp:ListItem>
            <asp:ListItem Text="MH" Value="MH"></asp:ListItem>
            <asp:ListItem Text="UG" Value="UG"></asp:ListItem>
            <asp:ListItem Text="MG" Value="MG"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div id="divJPSupportExpire" runat="server" class="settingrow" visible="false">
        <span class="settinglabel">
            <asp:Localize ID="lcalJPSupportExpireOn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalJPSupportExpireOn.Text%>"></asp:Localize></span>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>
                    <ComponentArt:Calendar ID="caJPSupportExpireCalendarPicker" ControlType="Picker"
                        PickerFormat="Custom" PickerCustomFormat="MMM dd yyyy" PickerCssClass="picker"
                        runat="server">
                        <ClientEvents>
                            <SelectionChanged EventHandler="caJPSupportExpireCalendarPicker_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </td>
                <td style="font-size: 10px;">
                    &nbsp;
                </td>
                <td>
                    <img id="calendar_button" alt="" onclick="caJPSupportExpireCalendar.setSelectedDate(caJPSupportExpireCalendarPicker.getSelectedDate());caJPSupportExpireCalendar.show();"
                        class="calendar_button" src="//static.cdn-anritsu.com/controls/componentart/icons/btn_calendar.gif"
                        width="25" height="22" />
                </td>
            </tr>
        </table>
        <ComponentArt:Calendar runat="server" ID="caJPSupportExpireCalendar" PopUp="Custom"
            PopUpExpandControlId="calendar_button" AllowMultipleSelection="false" AllowWeekSelection="false"
            AllowMonthSelection="false" ControlType="Calendar" AutoTheming="true" Width="170"
            Height="100">
            <ClientEvents>
                <SelectionChanged EventHandler="caJPSupportExpireCalendar_onSelectionChanged" />
            </ClientEvents>
        </ComponentArt:Calendar>
    </div>
    <%--  <div class="settingrow">
        <fieldset id="fsIncludeJapanUSBKey">
            <legend>
                <asp:Localize ID="lcalIncludeJapanUSBKey" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalIncludeJapanUSBKey.Text%>"></asp:Localize></legend>
            <uc1:ProdRegAdmin_JapanUSBKeyCtrl ID="ProdRegAdmin_JapanUSBKeyCtrl1" runat="server" />
        </fieldset>
    </div>--%>
    <div class="settingrow">
        <fieldset id="fsIncludeDownloadSrc">
            <legend>
                <asp:Localize ID="lcalIncludeDLSrcOptions" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalIncludeDLSrcOptions.Text%>"></asp:Localize></legend>
            <asp:CheckBox ID="cbxIncludeDlSrcGW" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeDlSrcGW.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeDlSrcJP" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeDlSrcJP.Text%>"
                Checked="true" />
        </fieldset>
    </div>
    <div class="settingrow">
        <fieldset id="fsIncludeModelOptions">
            <legend>
                <asp:Localize ID="lcalIncludeModelOptions" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalIncludeModelOptions.Text%>"></asp:Localize></legend>
            <asp:CheckBox ID="cbxIncludeMNPrimary" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNPrimary.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeMNTags" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNTags.Text%>"
                Checked="true" />
            <asp:CheckBox ID="cbxIncludeMNRegSpfc" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,cbxIncludeMNRegSpfc.Text%>"
                Checked="true" />
        </fieldset>
    </div>
    <div class="settingrow">
        <table>
            <tr>
                <td>
                    <span class="settinglabel">
                        <asp:Localize ID="lcalCreatedOn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalCreatedOn.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrCreatedonUTC" runat="server"></asp:Literal>
                </td>
                <td align="right">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalModifiedOn" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalModifiedOn.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrModifiedOnUTC" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow" style="text-align: right;">
        <p class="msg">
            <asp:Literal ID="ltrProductRegInfoMsg" runat="server"></asp:Literal></p>
        <asp:Button ID="bttUpdateProductRegInfo" runat="server" Text="update" OnClick="bttUpdateProductRegInfo_Click" />
        <asp:Button ID="bttDeleteProductReg" runat="server" Text="delete" OnClick="bttDeleteProductReg_Click"
            CausesValidation="false" />
    </div>
    <div class="settingrow">
        <fieldset id="fsSubmittedUserInfo">
            <legend>
                <asp:Localize ID="lcalProdInfoAddOnData" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalProdInfoAddOnData.Text%>"></asp:Localize></legend>
            <%-- <span class="settinglabel-10">
            <asp:Localize ID="lcalProdInfoAddOnData" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,lcalProdInfoAddOnData.Text%>"></asp:Localize></span>--%>
            <ComponentArt:DataGrid ID="cadgProdRegReadOnlyData" AutoTheming="true" ClientIDMode="AutoID"
                AllowPaging="false" AllowColumnResizing="true" EnableViewState="true" RunningMode="Callback"
                ShowFooter="false" Width="100%" runat="server">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="ProdRegDataID">
                        <Columns>
                            <ComponentArt:GridColumn DataField="ProdRegID" Visible="false" />
                            <ComponentArt:GridColumn DataField="ProdRegDataID" Visible="false" />
                            <ComponentArt:GridColumn DataField="DataKey" HeadingText="DataKey" Width="150" Align="Center" />
                            <ComponentArt:GridColumn DataField="DataValue" Width="250" Align="Center" />
                            <ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="Created On" Width="250"
                                Align="Center" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
            </ComponentArt:DataGrid>
        </fieldset>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlAllRegProd" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,pnlAllRegProd.HeaderText%>">
    <div class="settingrow">
        <ComponentArt:DataGrid ID="cadgAllRegProducts" AutoTheming="true" ClientIDMode="AutoID"
            PagerStyle="Numbered" AllowColumnResizing="true" ShowHeader="true" EnableViewState="true"
            RunningMode="Callback" ShowFooter="true" PageSize="10" AllowPaging="true" Width="100%"
            runat="server">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="ProdRegID">
                    <Columns>
                        <ComponentArt:GridColumn DataField="ProdRegID" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModelNumber" HeadingText="Model" Width="80" />
                        <ComponentArt:GridColumn DataField="SerialNumber" HeadingText="Serial" Width="80" />
                        <ComponentArt:GridColumn DataField="StatusCode" HeadingText="Status" Width="70" />
                        <ComponentArt:GridColumn DataField="WebAccessKey" HeadingText=" " DataCellClientTemplateId="WFEditTemplate"
                            Width="60" Align="Center" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="WFEditTemplate">
                    <a href='/prodregadmin/productregdetail?rwak=## DataItem.GetMember("WebAccessKey").Value ##'>
                        details</a>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:DataGrid>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdRegAdmin_DetailCtrl" />
