﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    //public partial class ProdRegAdmin_DetailCtrl_ToBeDeleted : CustomControlBase.ProdRegAdmin_DetailBase, App_Lib.UI.IStaticLocalizedCtrl
    //{

    //    protected override void OnInit(EventArgs e)
    //    {
    //        cadgProdRegReadOnlyData.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(cadgProdRegReadOnlyData_OnNeedRebind);
    //        cadgProdRegReadOnlyData.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(cadgProdRegReadOnlyData_OnNeedDataSource);
    //        cadgProdRegReadOnlyData.NeedChildDataSource += new ComponentArt.Web.UI.Grid.NeedChildDataSourceEventHandler(cadgProdRegReadOnlyData_OnNeedChildData);
    //        cadgProdRegReadOnlyData.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(cadgProdRegReadOnlyData_OnPageChanged);
    //        cadgProdRegReadOnlyData.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(cadgProdRegReadOnlyData_OnSort);

    //        cadgAllRegProducts.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(cadgAllRegProducts_OnNeedRebind);
    //        cadgAllRegProducts.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(cadgAllRegProducts_OnNeedDataSource);
    //        //cadgRegProducts.NeedChildDataSource += new ComponentArt.Web.UI.Grid.NeedChildDataSourceEventHandler(OnNeedChildData);
    //        cadgAllRegProducts.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(cadgAllRegProducts_OnPageChanged);
    //        cadgAllRegProducts.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(cadgAllRegProducts_OnSort);

    //        base.OnInit(e);

    //        if (!IsPostBack)
    //        {
    //            String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
    //            WebUtility.GenerateProcessingScript(bttUpdateProductRegInfo, "onclick", processingText);
    //            WebUtility.GenerateProcessingScript(bttDeleteProductReg, "onclick", processingText);

    //        }

    //        bttDeleteProductReg.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteReg") + "')";
    //    }

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        if (!IsPostBack)
    //        {
    //            BindStatus();
    //            BindProductRegistrationInfo();
    //            cadgAllRegProducts_BuildTopLevel();
    //        }
    //    }


    //    private void GetProdRegReadOnlyDataSource()
    //    {
    //        DataTable tb = Lib.ProductRegistration.Acc_ProductRegisteredDataBLL.SelectByProdRegID(ProductReg.ProdRegID);
    //        DataView dv = null;
    //        if (tb != null)
    //        {
    //            dv = tb.DefaultView;
    //            dv.RowFilter = "DataKey LIKE 'SubmittedBy*' OR DataKey LIKE 'RenewalReq*'";
    //        }

    //        cadgProdRegReadOnlyData.DataSource = dv;
    //        //Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectActiveAll(App_Lib.UIHelper.GetSelectedAccountID(true));
    //    }
    //    private void BindStatus()
    //    {
    //        DataTable tb = Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectStatus();
    //        cmbProdRegStatus.Items.Clear();
    //        foreach (DataRow r in tb.Rows)
    //        {
    //            String statusCode = r["StatusCode"].ToString();
    //            String statusText = ConvertUtility.ConvertNullToEmptyString(GetGlobalResourceObject("COMMON", statusCode.ToLowerInvariant()));
    //            ListItem item = new ListItem(statusText, statusCode.ToLowerInvariant());
    //            cmbProdRegStatus.Items.Add(item);
    //        }
    //    }

    //    public void BindProductRegistrationInfo()
    //    {
    //        Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;

    //        if (prodReg == null)
    //        {
    //            this.Visible = false;
    //            return;
    //        }
    //        cmbProdRegStatus.SelectedValue = prodReg.StatusCode;
    //        if (prodReg.ModelTarget.IsNullOrEmptyString()) cmbProdRegModelTarget.SelectedValue = "none";
    //        else cmbProdRegModelTarget.SelectedValue = prodReg.ModelTarget;

    //        ltrMN.Text = prodReg.ModelNumber;
    //        ltrSN.Text = prodReg.SerialNumber;
    //        ltrCreatedonUTC.Text = GetDate(prodReg.CreatedOnUTC);
    //        ltrModifiedOnUTC.Text = "n/a";
    //        if (prodReg.ModifiedOnUTC != DateTime.MinValue)
    //        {
    //            ltrModifiedOnUTC.Text = GetDate(prodReg.ModifiedOnUTC);
    //        }

    //        if (prodReg.JPSupportExpireOn != DateTime.MinValue)
    //        {
    //            caJPSupportExpireCalendarPicker.SelectedDate = prodReg.JPSupportExpireOn;
    //            //txtJPSupportExpireOn.Text = prodReg.JPSupportExpireOn.ToString("MMM-dd-yyyy");
    //        }

    //        App_Lib.AppCacheFactories.ProductConfigCacheFactory dataCache = new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
    //        Lib.Erp.Erp_ProductConfig_TBD prodCfg = dataCache.GetProductConfigInfo(prodReg.ModelNumber);


    //        divJPSupportExpire.Visible = (prodCfg != null && prodCfg.IsPaidSupportModel);
    //        cbxIncludeDlSrcGW.Checked = prodReg.IncludeDLSrcGW;
    //        cbxIncludeDlSrcJP.Checked = prodReg.IncludeDLSrcJP;
    //        cbxIncludeMNPrimary.Checked = prodReg.IncludeModelPrimary;
    //        cbxIncludeMNTags.Checked = prodReg.IncludeModelTags;
    //        cbxIncludeMNRegSpfc.Checked = prodReg.IncludeModelRegSpecific;
    //    }

    //    #region " cadgProdRegReadOnlyData "
    //    private void cadgProdRegReadOnlyData_BuildTopLevel()
    //    {
    //        GetProdRegReadOnlyDataSource();
    //    }

    //    public void cadgProdRegReadOnlyData_OnNeedRebind(object sender, EventArgs e)
    //    {
    //        cadgProdRegReadOnlyData.DataBind();
    //    }

    //    public void cadgProdRegReadOnlyData_OnNeedDataSource(object sender, EventArgs e)
    //    {
    //        cadgProdRegReadOnlyData_BuildTopLevel();
    //    }

    //    public void cadgProdRegReadOnlyData_OnNeedChildData(object sender, ComponentArt.Web.UI.GridNeedChildDataSourceEventArgs e)
    //    {
    //        if (e.Item.Level == 0)
    //        {
    //        }
    //    }

    //    public void cadgProdRegReadOnlyData_OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs e)
    //    {
    //        cadgProdRegReadOnlyData.CurrentPageIndex = e.NewIndex;
    //    }

    //    public void cadgProdRegReadOnlyData_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs e)
    //    {
    //        cadgProdRegReadOnlyData.Sort = e.SortExpression;
    //    }



    //    public void cadgProdRegReadOnlyData_ItemCommand(object sender, ComponentArt.Web.UI.GridItemCommandEventArgs e)
    //    {
    //        Int32 wfTaskID = ConvertUtility.ConvertToInt32(e.Item["WFTaskID"], 0);
    //    }

    //    #endregion

    //    #region " cadgAllRegProducts "
    //    private void cadgAllRegProducts_BuildTopLevel()
    //    {
    //        Guid accountID = ProductReg.AccountID;
    //        if (accountID.IsNullOrEmptyGuid()) return;
    //        List<SearchByOption> searchOptions = new List<SearchByOption>();
    //        SearchByOption so = new SearchByOption("AccountID", accountID);
    //        searchOptions.Add(so);
    //        cadgAllRegProducts.DataSource = Lib.ProductRegistration.Acc_ProductRegisteredBLL.AdminProdRegSearch(
    //            searchOptions);
    //    }

    //    public void cadgAllRegProducts_OnNeedRebind(object sender, EventArgs oArgs)
    //    {
    //        cadgAllRegProducts.DataBind();
    //    }

    //    public void cadgAllRegProducts_OnNeedDataSource(object sender, EventArgs oArgs)
    //    {
    //        cadgAllRegProducts_BuildTopLevel();
    //    }

    //    public void cadgAllRegProducts_OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
    //    {
    //        cadgAllRegProducts.CurrentPageIndex = oArgs.NewIndex;
    //    }

    //    public void cadgAllRegProducts_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
    //    {
    //        cadgAllRegProducts.Sort = oArgs.SortExpression;
    //    }
    //    #endregion


    //    #region IStaticLocalizedCtrl Members

    //    public string StaticResourceClassKey
    //    {
    //        get { return "ADM_STCTRL_ProdRegAdmin_DetailCtrl"; }
    //    }

    //    public string GetStaticResource(string resourceKey)
    //    {
    //        return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
    //    }

    //    #endregion


    //    public String GetDate(object dateTimeString)
    //    {
    //        if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
    //        String llCC = UIHelper.GetBrowserLang();
    //        CultureInfo culture = new CultureInfo(llCC);
    //        DateTime updatedt = Convert.ToDateTime(dateTimeString);

    //        return updatedt.ToString("d", culture);
    //    }

    //    protected void bttUpdateProductRegInfo_Click(object sender, EventArgs e)
    //    {

    //        try
    //        {
    //            ltrProductRegInfoMsg.Text = String.Empty;
    //            // ProdRegAdmin_JapanUSBKeyCtrl1.ClearControls();
    //            Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;
    //            Lib.Erp.Erp_ProductConfig_TBD prodCfg = ProductCfg;
    //            String modelTarget = String.Empty;
    //            if (cmbProdRegModelTarget.SelectedValue != "none") modelTarget = cmbProdRegModelTarget.SelectedValue;

    //            DateTime dtJPSupportExpDate = caJPSupportExpireCalendarPicker.SelectedDate;
    //            if (prodCfg.IsPaidSupportModel && dtJPSupportExpDate == DateTime.MinValue && cmbProdRegStatus.SelectedIndex == 0)
    //            {
    //                throw new ArgumentException(GetStaticResource("ltrProductRegInfoMsg.Text.SupportExpRequired"));
    //            }



    //            //if (caJPSupportExpireCalendarPicker.SelectedDate != DateTime.MinValue )//(!txtJPSupportExpireOn.Text.IsNullOrEmptyString())
    //            //{
    //            //    //validate date
    //            //    if (ConvertUtility.ConvertToDateTime(txtJPSupportExpireOn.Text.Trim(), DateTime.MinValue) == DateTime.MinValue)
    //            //        throw new ArgumentException("Invalid date.");
    //            //}


    //            //if (!txtJPSupportExpireOn.Text.IsNullOrEmptyString())
    //            //dtJPSupportExpDate = ConvertUtility.ConvertToDateTime(txtJPSupportExpireOn.Text.Trim(), DateTime.MinValue);
    //            //dtJPSupportExpDate = caJPSupportExpireCalendarPicker.SelectedDate;

    //            Lib.ProductRegistration.Acc_ProductRegisteredBLL.Update(prodReg.WebAccessKey
    //                , cmbProdRegStatus.SelectedValue.ToLowerInvariant()
    //                , dtJPSupportExpDate
    //                , cbxIncludeDlSrcGW.Checked
    //                , cbxIncludeDlSrcJP.Checked
    //                , cbxIncludeMNPrimary.Checked
    //                , cbxIncludeMNTags.Checked
    //                , cbxIncludeMNRegSpfc.Checked
    //                , modelTarget);

    //            ClearCache(prodReg.ProdRegID);

    //            UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);//reset
    //            ltrProductRegInfoMsg.Text = GetStaticResource("UpdateMsg");
    //            bttUpdateProductRegInfo.Focus();
    //        }
    //        catch (ArgumentException aex)
    //        {
    //            ltrProductRegInfoMsg.Text = aex.Message;
    //            bttUpdateProductRegInfo.Focus();
    //        }
    //    }

    //    protected void bttDeleteProductReg_Click(object sender, EventArgs e)
    //    {
    //        Guid webAccessKey = ProductReg.WebAccessKey;
    //        if (webAccessKey.IsNullOrEmptyGuid()) return;
    //        Lib.ProductRegistration.Acc_ProductRegistered prodReg =
    //            Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectByWebAccessKeyForAdmin(webAccessKey);
    //        if (prodReg == null) return;
    //        Lib.ProductRegistration.Acc_ProductRegisteredBLL.Delete(prodReg.ProdRegID);
    //        ClearCache(ProductReg.ProdRegID);
    //        UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);//reset
    //        WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.ProdRegAdmin_Home);
    //    }

    //    private void ClearCache(Int32 prodRegID)
    //    {
    //        App_Lib.AppCacheFactories.DownloadCacheFactory dlDataCache =
    //               new App_Lib.AppCacheFactories.DownloadCacheFactory();
    //        dlDataCache.TouchCacheFile();
    //        dlDataCache.RemoveFromCache(prodRegID);
    //    }
    //}
}