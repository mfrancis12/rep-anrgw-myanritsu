﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_JapanUSBKeyCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_JapanUSBKeyCtrl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlUSB" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,pnlUSB.HeaderText%>">
<div class="settingrow">
    <p>
        <asp:Localize ID="lcalAboutUSB" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalAboutUSB.Text%>"></asp:Localize></p>
</div>
<div class="settingrow">
    <span class="settinglabel">
        <asp:Localize ID="lcalCurrentUSBKey" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalCurrentUSBKey.Text%>"></asp:Localize></span>
    <asp:TextBox ID="txtCurrentUSBKey" runat="server" ReadOnly="true" Enabled="false"
        SkinID="tbx-250"></asp:TextBox>
    <asp:Button ID="bttRemoveCurrentUSB" runat="server" SkinID="SmallButton" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,bttRemoveCurrentUSB.Text%>'
        OnClick="bttRemoveCurrentUSB_Click" />
</div>
<fieldset>
    <legend>
        <asp:Localize ID="lcalFindUSBLegend" runat="server" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalFindUSBLegend.Text%>'></asp:Localize></legend>
    <label>
        <asp:Localize ID="lcalFindUSBDesc" runat="server" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalFindUSBDesc.Text%>'></asp:Localize></label>
    <div class="settingrow">
        <asp:TextBox ID="txtSearchUSB" runat="server" SkinID="tbx-250" ValidationGroup="vgSearchJPUSB"></asp:TextBox>
        <asp:Button ID="bttSearchUSB" runat="server" SkinID="SmallButton" ValidationGroup="vgSearchJPUSB"
            Text="Search USB" OnClick="bttSearchUSB_Click" />
    </div>
    <div class="settingrow">
        <asp:GridView ID="gvJPUSB" runat="server" AutoGenerateColumns="false" OnRowCommand="gvJPUSB_RowCommand">
            <Columns>
                <asp:BoundField DataField="USBNo" HeaderText="USBNo" />
                <asp:BoundField DataField="EmailAddress" HeaderText="USBEmail" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="bttSelectUSB" runat="server" CommandName="UpdateUSBCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "USBNo") %>'
                            CausesValidation="false" SkinID="SmallButton" Text="Associate this USB" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="settingrow">
        <p class='msg'>
            <asp:Label ID="lblJapanUSBMsg" runat="server"></asp:Label></p>
    </div>
</fieldset>
<fieldset>
    <legend>
        <asp:Localize ID="lcalAddUSBLegend" runat="server" Text='<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalAddUSBLegend.Text%>'></asp:Localize></legend>
    <div class="settingrow">
        <div class="settingrow">
            <span class="settinglabel-10">
                <asp:Localize ID="lcalNewUSBNo" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalNewUSBNo.Text %>" />:</span>
            <asp:TextBox ID="txtNewUSBNo" runat="server" SkinID="tbx-300"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvNewUSBNo" runat="server" ControlToValidate="txtNewUSBNo" ValidationGroup="AddNewUSB"
                Display="Dynamic" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revNewUSBNo" runat="server" ControlToValidate="txtNewUSBNo" ValidationGroup="AddNewUSB"
                Display="Dynamic" ErrorMessage="<%$Resources:Common,InvalidMsg%>" ValidationExpression="^[a-zA-Z0-9-_]+$"></asp:RegularExpressionValidator>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10">
                <asp:Localize ID="lcalNewUSBOwnerEmail" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,lcalNewUSBOwnerEmail.Text %>" />:</span>
            <asp:TextBox ID="txtNewUSBOwnerEmail" runat="server" SkinID="tbx-300"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvNewUSBOwnerEmail" runat="server" ControlToValidate="txtNewUSBOwnerEmail" ValidationGroup="AddNewUSB"
                Display="Dynamic" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">&nbsp;</span>
            <asp:Button ID="bttNewUSBAdd" runat="server" SkinID="SmallButton" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl,bttNewUSBAdd.Text %>"
                OnClick="bttNewUSBAdd_Click" ValidationGroup="AddNewUSB" />
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">&nbsp;</span>
            <p class='msg'>
                <asp:Label ID="lblNewUSBMsg" runat="server"></asp:Label></p>
        </div>
    </div>
</fieldset>
</anrui:GlobalWebBoxedPanel>
<%--<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_ProdRegAdmin_JapanUSBKeyCtrl" />--%>
