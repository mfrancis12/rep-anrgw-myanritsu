﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Controls
{
    //public partial class ProdRegAdmin_JapanUSBKeyCtrl : CustomControlBase.ProdRegAdmin_DetailBase, App_Lib.UI.IStaticLocalizedCtrl
    //{

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        if (!IsPostBack)
    //        {
    //            if (ProductReg == null)
    //            {
    //                this.Visible = false;
    //                return;
    //            }
    //            txtCurrentUSBKey.Text = ProductReg.USBNo;
    //        }
    //    }

    //    protected void bttSearchUSB_Click(object sender, EventArgs e)
    //    {
    //        BindGrid(txtSearchUSB.Text.Trim());
    //    }

    //    public void BindGrid(string USBString)
    //    {
    //        gvJPUSB.DataSource = Lib.DongleDownload.DongleLicBLL.FindDongle(USBString);
    //        gvJPUSB.DataBind();
    //    }

    //    protected void gvJPUSB_RowCommand(object sender, GridViewCommandEventArgs e)
    //    {
    //        if (e.CommandName == "UpdateUSBCommand")
    //        {
    //            lblJapanUSBMsg.Text = String.Empty;
    //            lblNewUSBMsg.Text = String.Empty;
    //            Guid webAccessKey = ProductReg.WebAccessKey;
    //            if (webAccessKey.IsNullOrEmptyGuid()) return;
    //            String usbNo = ConvertUtility.ConvertNullToEmptyString(e.CommandArgument.ToString());
    //            DataRow row = Lib.DongleDownload.DongleLicBLL.FindDongleByUSBNo(usbNo);

    //            Guid usbOwner = ConvertUtility.ConvertToGuid(row["MembershipId"], Guid.Empty);

    //            Int32 returnCode = Lib.ProductRegistration.Acc_ProductRegisteredBLL.UpdateJapanUSB(webAccessKey, usbNo, usbOwner);
    //            if (returnCode == 0) //user do not have access to this account or registration does not exist
    //            {
    //                lblJapanUSBMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + GetStaticResource("UserNoAccess");
    //                return;
    //            }
    //            ClearCache(ProductReg.ProdRegID);
    //            txtCurrentUSBKey.Text = usbNo;
    //            lblJapanUSBMsg.Text = GetStaticResource("USBAssociatedMsg");
    //        }
    //    }

    //    private void ClearCache(Int32 prodRegID)
    //    {
    //        App_Lib.AppCacheFactories.DownloadCacheFactory dlDataCache =
    //               new App_Lib.AppCacheFactories.DownloadCacheFactory();
    //        dlDataCache.TouchCacheFile();
    //        dlDataCache.RemoveFromCache(prodRegID);
    //        UIHelper.Admin_ProdRegInfoSet(ProdRegWebAccessKey, null);
    //    }

    //    /// <summary>
    //    /// Clears all the controls
    //    /// </summary>
    //    public void ClearControls()
    //    {
    //        lblJapanUSBMsg.Text = String.Empty;
    //        lblNewUSBMsg.Text = String.Empty;
    //        txtSearchUSB.Text = String.Empty;
    //        txtNewUSBNo.Text = String.Empty;
    //        txtNewUSBOwnerEmail.Text = String.Empty;
    //        //txtNewUSBCode.Text = String.Empty;
    //        gvJPUSB.DataSource = null;
    //        gvJPUSB.DataBind();
    //    }

    //    protected void bttRemoveCurrentUSB_Click(object sender, EventArgs e)
    //    {
    //        lblJapanUSBMsg.Text = String.Empty;
    //        Int32 returnCode = Lib.ProductRegistration.Acc_ProductRegisteredBLL.UpdateJapanUSB(ProdRegWebAccessKey, String.Empty, Guid.Empty);
    //        ClearCache(ProductReg.ProdRegID);
    //        txtCurrentUSBKey.Text = String.Empty;
    //    }

    //    #region IStaticLocalizedCtrl Members

    //    public string StaticResourceClassKey
    //    {
    //        get { return "ADM_STCTRL_USBAddNew"; }
    //    }

    //    public string GetStaticResource(string resourceKey)
    //    {
    //        return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
    //    }

    //    #endregion

    //    protected void bttNewUSBAdd_Click(object sender, EventArgs e)
    //    {
    //        if (!Page.IsValid) return;
    //        lblJapanUSBMsg.Text = String.Empty;
    //        lblNewUSBMsg.Text = String.Empty;
    //        try
    //        {
    //            Lib.Security.Sec_UserMembership adminUser = UIHelper.GetCurrentUserOrSignIn();
    //            String usbNo = txtNewUSBNo.Text.Trim();
    //            //String usbCode = txtNewUSBCode.Text.Trim();
    //            String email = txtNewUSBOwnerEmail.Text.Trim();

    //            DataTable tbExistingUSB = Lib.DongleDownload.DongleLicBLL.FindDongle(usbNo);
    //            if (tbExistingUSB != null && tbExistingUSB.Rows.Count > 0)
    //            {
    //                throw new ArgumentException(GetStaticResource("USBKeyExists"));
    //            }

    //            Lib.Security.Sec_UserMembership usbOwner = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
    //            if (usbOwner == null)
    //            {
    //                throw new ArgumentException(GetStaticResource("UserDoesNotExist"));
    //            }

    //            List<Lib.Account.Acc_AccountMaster> usbOwnerAccounts = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(usbOwner.MembershipId);
    //            if (usbOwnerAccounts == null || usbOwnerAccounts.Count < 1)
    //            {
    //                throw new ArgumentException(GetStaticResource("UserNotInOrg"));
    //            }

    //            if (!Lib.DongleDownload.DongleLicBLL.IsValidUSBNoAndPassword(usbNo, "webgatekey"))
    //            {
    //                throw new ArgumentException(GetStaticResource("InvalidUsbNo"));
    //            }

    //            Lib.DongleDownload.DongleLicBLL.Insert(usbNo, usbOwner.Email, adminUser.Email);
    //            lblNewUSBMsg.Text = String.Format("{0} {1}.", usbNo, GetStaticResource("USBAddedMsg"));
    //            BindGrid(usbNo);
    //            txtNewUSBNo.Text = String.Empty;
    //            txtNewUSBOwnerEmail.Text = String.Empty;
    //            //txtNewUSBCode.Text = String.Empty;
    //        }
    //        catch (Exception ex)
    //        {
    //            lblNewUSBMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
    //        }
    //    }
    //}
}