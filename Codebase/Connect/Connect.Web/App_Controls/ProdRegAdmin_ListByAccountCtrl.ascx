﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_ListByAccountCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_ListByAccountCtrl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlAllRegProd" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ProdRegAdmin_ListByAccountCtrl,pnlAllRegProd.HeaderText%>">
    <div class="settingrow">
        <ComponentArt:DataGrid ID="cadgAllRegProducts" AutoTheming="true" ClientIDMode="AutoID"
            PagerStyle="Numbered" AllowColumnResizing="true" ShowHeader="true" EnableViewState="true"
            RunningMode="Callback" ShowFooter="true" PageSize="10" AllowPaging="true" Width="100%"
            runat="server">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="ProdRegID">
                    <Columns>
                        <ComponentArt:GridColumn DataField="ModelNumber" HeadingText="Model" Width="80" />
                        <ComponentArt:GridColumn DataCellClientTemplateId="ctSN" Width="100" HeadingText="Serial" />
                        <ComponentArt:GridColumn DataField="StatusCode" HeadingText="Status" Width="60" />
                        <%--   <ComponentArt:GridColumn DataField="JPSupportExpireOn" HeadingText="JP Support Expire" Width="100" FormatString="MMM dd, yyyy" />   --%>
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstJPSupportExpireOn" HeadingText="JP Support Expire"
                            Width="100" />
                        <ComponentArt:GridColumn DataField="JPSupportExpireOn" Visible="false" />
                        <ComponentArt:GridColumn DataField="ProdRegID" Visible="false" />
                        <ComponentArt:GridColumn DataField="WebAccessKey" Visible="false" />
                        <ComponentArt:GridColumn DataField="SerialNumber" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ctSN">
                    <a title='registration details' href='/prodregadmin/productregdetail?rwak=## DataItem.getMember("WebAccessKey").get_value() ##'>
                        ## DataItem.getMember("SerialNumber").get_value() ##</a>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="gstJPSupportExpireOn">
                    <Template>
                        <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["JPSupportExpireOn"]) %>'></asp:Literal>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:DataGrid>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdRegAdmin_ListByAccountCtrl" />
