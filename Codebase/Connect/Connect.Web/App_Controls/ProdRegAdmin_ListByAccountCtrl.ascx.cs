﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdRegAdmin_ListByAccountCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid AccountID
        {
            get { return ConvertUtility.ConvertToGuid(ViewState["vsAccountID"], Guid.Empty); }
            set { ViewState["vsAccountID"] = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            cadgAllRegProducts.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(cadgAllRegProducts_OnNeedRebind);
            cadgAllRegProducts.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(cadgAllRegProducts_OnNeedDataSource);
            cadgAllRegProducts.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(cadgAllRegProducts_OnPageChanged);
            cadgAllRegProducts.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(cadgAllRegProducts_OnSort);
            cadgAllRegProducts.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(cadgAllRegProducts_OnFilter);
            cadgAllRegProducts.GroupCommand += new ComponentArt.Web.UI.Grid.GroupCommandEventHandler(cadgAllRegProducts_OnGroup);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!AccountID.IsNullOrEmptyGuid())
                {
                    cadgAllRegProducts_GetGridDataSource();
                    cadgAllRegProducts.DataBind();
                }

            }
        }

        private void cadgAllRegProducts_GetGridDataSource()
        {
            cadgAllRegProducts.DataSource = Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectWithFilter(AccountID, 
                String.Empty, String.Empty, String.Empty);
        }

        public void cadgAllRegProducts_OnNeedRebind(object sender, EventArgs oArgs)
        {
            cadgAllRegProducts.DataBind();
        }

        public void cadgAllRegProducts_OnNeedDataSource(object sender, EventArgs oArgs)
        {
            cadgAllRegProducts_GetGridDataSource();
        }

        public void cadgAllRegProducts_OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cadgAllRegProducts.CurrentPageIndex = oArgs.NewIndex;
        }

        public void cadgAllRegProducts_OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cadgAllRegProducts.Filter = oArgs.FilterExpression;
        }

        public void cadgAllRegProducts_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cadgAllRegProducts.Sort = oArgs.SortExpression;
        }

        public void cadgAllRegProducts_OnGroup(object sender, ComponentArt.Web.UI.GridGroupCommandEventArgs oArgs)
        {
            cadgAllRegProducts.GroupBy = oArgs.GroupExpression;
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = UIHelper.GetBrowserLang();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdRegAdmin_ListByAccountCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }
    }
}