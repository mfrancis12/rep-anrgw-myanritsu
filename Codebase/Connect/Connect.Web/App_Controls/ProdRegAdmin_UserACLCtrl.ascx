﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_UserACLCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_UserACLCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlUserACL" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,pnlUserACL.HeaderText%>">
    <div id="div1" class="settingrow">
        <asp:Localize ID="lcalUserACLDesc" runat="server" Text='<%$ Resources:STCTRL_ProdRegAdmin_UserACLCtrl,lcalUserACLDesc.Text %>'></asp:Localize>
    </div>
    <div id="divUserACL" class="settingrow">
        <asp:CheckBox ID="cbxNotifyUserUponApproval" runat="server" Checked="false" Text="<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,cbxNotifyUserUponApproval.Text%>"
            ToolTip="<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,cbxNotifyUserUponApproval.ToolTip%>" />
        <center>
            <asp:GridView ID="gvUserACL" runat="server" AutoGenerateColumns="false" OnRowCommand="gvUserACL_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft">
                        <ItemTemplate>
                            <%# ShowFullName(DataBinder.Eval(Container.DataItem, "MembershipId"))%>
                            <%--  <%#DataBinder.Eval(Container.DataItem, "FullName")%>--%><br />
                            <a style='font-weight: normal!important;' href='<%# DataBinder.Eval(Container.DataItem, "EmailAddress", "mailto:{0}") %>'>
                                <%# DataBinder.Eval(Container.DataItem, "EmailAddress")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StatusCode" HeaderText='<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.StatusCode.HeaderText%>' />
                    <asp:BoundField DataField="LastStatusChangedBy" HeaderText='<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.LastStatusChangedBy.HeaderText%>' />
                    <%-- <asp:BoundField DataField="LastStatusChangedOnUTC" HeaderText='<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.LastStatusChangedOnUTC.HeaderText%>'  DataFormatString="{0:MMM dd, yyyy}"/>--%>
                    <asp:TemplateField HeaderText="<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.LastStatusChangedOnUTC.HeaderText%>">
                        <ItemTemplate>
                            <asp:Literal ID="ltrLastStatusChangedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"LastStatusChangedOnUTC")) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft">
                        <ItemTemplate>
                            <asp:Button ID="bttUserACLGrant" runat="server" CausesValidation="false" SkinID="SmallButton"
                                CommandName="GrantAccessCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MembershipId") %>'
                                Text='<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.bttUserACLGrant.Text%>'
                                Visible='<%# !(Boolean)DataBinder.Eval(Container.DataItem, "IsApprovedUserACL") %>' />
                            <asp:Button ID="bttUserACLRemove" runat="server" CausesValidation="false" SkinID="SmallButton"
                                CommandName="RemoveAccessCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProdRegUserACLID") %>'
                                Text='<%$Resources:STCTRL_ProdRegAdmin_UserACLCtrl,gvUserACL.bttUserACLRemove.Text%>'
                                Visible='<%# (Boolean)DataBinder.Eval(Container.DataItem, "IsApprovedUserACL") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </center>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdRegAdmin_UserACLCtrl" />
