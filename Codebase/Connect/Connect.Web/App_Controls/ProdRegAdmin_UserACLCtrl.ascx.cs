﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;

//namespace Anritsu.Connect.Web.App_Controls
//{
//    public partial class ProdRegAdmin_UserACLCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
//    {       

//        protected void Page_Load(object sender, EventArgs e)
//        {
//            if (!IsPostBack)
//            {
//                if (ProductReg == null || ProductRegAccount == null || ProductCfg == null || !ProductCfg.UserACLRequired)
//                {
//                    this.Visible = false;
//                    return;
//                }

//                BindAvailableUsersAndACL();
//            }
//        }

//        private void BindAvailableUsersAndACL()
//        {
//            DataTable tb = Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.SelectByProdRegID(ProductReg.ProdRegID);
//            gvUserACL.DataSource = tb;
//            gvUserACL.DataBind();
//        }

//        protected string ShowFullName(Object MembershipId)
//        {
//            Guid memId = (Guid)MembershipId;
//            Lib.Security.Sec_UserMembership memUser = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(memId);

//            String firstnameLanguage = DetectNameLanguage(memUser.FirstName);
//            String lastnameLanguage = DetectNameLanguage(memUser.LastName);

//            string fullName = memUser.LastName + "," + memUser.FirstName;

//            if ((firstnameLanguage.StartsWith("ja") || firstnameLanguage.StartsWith("en")) && (lastnameLanguage.StartsWith("ja") || lastnameLanguage.StartsWith("en")))
//            {
//                fullName = memUser.LastName + "," + memUser.FirstName;
//            }
//            else if (!string.IsNullOrEmpty(memUser.FirstNameInEnglish) && !string.IsNullOrEmpty(memUser.LastNameInEnglish))
//            {
//                fullName = memUser.LastNameInEnglish + "," + memUser.FirstNameInEnglish;
//            }

//            return fullName;
//        }

//        public String GetDate(object dateTimeString)
//        {
//            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
//            String llCC = UIHelper.GetBrowserLang();
//            CultureInfo culture = new CultureInfo(llCC);
//            DateTime updatedt = Convert.ToDateTime(dateTimeString);

//            return updatedt.ToString("d", culture);
//        }

//        private String DetectNameLanguage(String input)
//        {
//            if (input.IsNullOrEmptyString()) return String.Empty;

//            String detectedLang = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.DetectTxtLanguage(input);
//            if (detectedLang != "en")//just in case if google is wrong
//            {
//                Regex engRgex = new Regex("[a-zA-Z0-9']+");
//                MatchCollection enChars = engRgex.Matches(input);
//                if (enChars != null && enChars.Count > 0) detectedLang = "en";
//            }
//            return detectedLang;
//        }

//        #region IStaticLocalizedCtrl Members

//        public string StaticResourceClassKey
//        {
//            get { return "STCTRL_ProdRegAdmin_UserACLCtrl"; }
//        }

//        public string GetStaticResource(string resourceKey)
//        {
//            return ConvertUtility.ConvertNullToEmptyString (this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
//        }

//        #endregion

//        protected void gvUserACL_RowCommand(object sender, GridViewCommandEventArgs e)
//        {
//            if (e.CommandName.Equals("GrantAccessCommand", StringComparison.InvariantCultureIgnoreCase))
//            {
//                Guid membershipId = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
//                Lib.Security.Sec_UserMembership requestedForUser = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(membershipId);
//                Lib.Security.Sec_UserMembership requestedByUser = UIHelper.GetCurrentUserOrSignIn();
//                Lib.ProductRegistration.Acc_ProductRegistered prodReg = Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectByProdRegID(ProductReg.ProdRegID);

//                Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.Insert_AsActive(prodReg
//                    , ProductReg.AccountID
//                    , requestedForUser
//                    , requestedByUser
//                    , cbxNotifyUserUponApproval.Checked);

//                Response.Redirect(Request.RawUrl);
//            }
//            else if (e.CommandName.Equals("RemoveAccessCommand", StringComparison.InvariantCultureIgnoreCase))
//            {
//                Int32 prodRegUserACLID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
//                Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.Delete(prodRegUserACLID);
//                Response.Redirect(Request.RawUrl);
//            }
//        }
//    }
//}