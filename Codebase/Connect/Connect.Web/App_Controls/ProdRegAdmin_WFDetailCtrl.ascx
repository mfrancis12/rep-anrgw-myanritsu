﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdRegAdmin_WFDetailCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdRegAdmin_WFDetailCtrl" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlWFMaster" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,pnlWFMaster.HeaderText%>">
    <asp:FormView ID="fvWFMaster" runat="server">
        <ItemTemplate>
            <div class="settingrow">
                <table>
                    <tr>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalWFInstID" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalWFInstID.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrWFInstID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "WFInstID") %>'></asp:Literal></span>
                        </td>
                        <td style="width: 20px;">
                            &nbsp;
                        </td>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalMN" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalMN.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrMN" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ModelNumber") %>'></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="settingrow">
                <table>
                    <tr>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalUserEmail" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalUserEmail.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrUserEmail" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserEmailAddress") %>'></asp:Literal>
                        </td>
                        <td style="width: 30px;">
                            &nbsp;
                        </td>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalUserIP" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalUserIP.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrUserIPAddress" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UserIPAddress") %>'></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="settingrow">
                <table>
                    <tr>
                        <td valign="top">
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalSNList" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalSNList.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <p style="width: 600px; overflow: auto;">
                                <asp:Literal ID="ltrSerialNumbers" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNumbers") %>'></asp:Literal></p>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="settingrow">
                <table>
                    <tr>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalCreatedOn" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalCreatedOn.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrCreatedonUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"CreatedOnUTC")) %>'></asp:Literal>
                        </td>
                        <td style="width: 20px;">
                            &nbsp;
                        </td>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalIsCompleted" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalIsCompleted.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbxIsCompleted" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsCompleted") %>'
                                Enabled="false" />
                        </td>
                        <td style="width: 20px;">
                            &nbsp;
                        </td>
                        <td>
                            <span class="settinglabel-10">
                                <asp:Localize ID="lcalAllTasksCompletedOn" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalAllTasksCompletedOn.Text%>"></asp:Localize></span>
                        </td>
                        <td>
                            <asp:Literal ID="ltrAllTasksCompletedOn" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"AllTasksCompletedOnUTC")) %>'></asp:Literal>
                        </td>
                    </tr>
                </table>
            </div>
        </ItemTemplate>
    </asp:FormView>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlCompanyInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,pnlCompanyInfo.HeaderText%>">
    <div class="settingrow">
        <table>
            <tr>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalCompName" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalCompName.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrCompanyName" runat="server"></asp:Literal>
                </td>
                <td style="width: 25px;">
                    &nbsp;
                </td>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalRuby" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalRuby.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrRuby" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow">
        <table>
            <tr>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalStatus" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalStatus.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrAccountStatusCode" runat="server"></asp:Literal>
                </td>
                <td style="width: 25px;">
                    &nbsp;
                </td>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalIsVerified" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalIsVerified.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Image ID="imgAccountVerifiedAndApproved" runat="server" ImageUrl="//static.cdn-anritsu.com/apps/connect/img/verified.gif" />
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow">
        <table>
            <tr>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalAgreeToTerms" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalAgreeToTerms.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrAgreedToTerms" runat="server"></asp:Literal>
                </td>
                <td style="width: 25px;">
                    &nbsp;
                </td>
                <td>
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalAgreedToTermsOnUTC" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalAgreedToTermsOnUTC.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrAgreedToTermsOnUTC" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow">
        <table>
            <tr>
                <td valign="top">
                    <span class="settinglabel-10">
                        <asp:Localize ID="lcalAddress" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,lcalAddress.Text%>"></asp:Localize></span>
                </td>
                <td>
                    <asp:Literal ID="ltrCompanyAddress" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <div class="settingrow" style="text-align: right;">
        <asp:Button ID="bttVerifyAndApproveAccount" runat="server" Text="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,bttVerifyAndApproveAccount.Text%>"
            SkinID="SmallButton" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlWFTasks" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_ProdRegAdmin_WFDetailCtrl,pnlWFTasks.HeaderText%>">
    <div class="settingrow">
        <ComponentArt:DataGrid ID="cadgRegProductWFTasks" AutoTheming="true" ClientIDMode="AutoID"
            PagerStyle="Numbered" AllowColumnResizing="true" EnableViewState="true" RunningMode="Callback"
            ShowFooter="true" PageSize="50" Width="100%" runat="server">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="WFTaskID">
                    <Columns>
                        <ComponentArt:GridColumn DataField="TaskTitle" HeadingText="Task Title" Width="150" />
                        <ComponentArt:GridColumn DataField="WFTaskID" HeadingText="WFTaskID" Visible="false" />
                        <ComponentArt:GridColumn DataField="TaskType" HeadingText="TaskType" Width="80" />
                        <ComponentArt:GridColumn DataField="IsCompleted" HeadingText="Completed" Width="60" />
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstCompletedOnUTC" Width="80"
                            HeadingText="Completed On" />
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstCreatedOnUTC" Width="80" HeadingText="Created On" />
                        <%--<ComponentArt:GridColumn DataField="CompletedOnUTC" HeadingText="Completed On" FormatString="MMM dd, yyyy" Width="80" />--%>
                        <%--<ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="Created On" FormatString="MMM dd, yyyy" Width="80"/>--%>
                        <ComponentArt:GridColumn DataCellServerTemplateId="gstApprove" />
                        <ComponentArt:GridColumn DataField="CompletedOnUTC" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedOnUTC" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="TaskDataID">
                    <Columns>
                        <ComponentArt:GridColumn DataField="TaskDataID" HeadingText="TaskDataID" Width="80" />
                        <ComponentArt:GridColumn DataField="DataKey" HeadingText="DataKey" Width="80" />
                        <ComponentArt:GridColumn DataField="DataValue" HeadingText="DataValue" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="gstApprove">
                    <Template>
                        <asp:LinkButton ID="lbttApproveTask" runat="server" Text="approve" CommandName="ApproveTaskCommand"></asp:LinkButton>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="gstCompletedOnUTC">
                    <Template>
                        <asp:Literal ID="ltrCompletedOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["CompletedOnUTC"]) %>'></asp:Literal>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="gstCreatedOnUTC">
                    <Template>
                        <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(Container.DataItem["CreatedOnUTC"]) %>'></asp:Literal>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="WFInstEditTemplate">
                    <a href="javascript:deleteRow('## DataItem.ClientId ##')">Delete</a>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:DataGrid>
    </div>
</anrui:GlobalWebBoxedPanel>
<script type="text/javascript" language="javascript">
    function deleteRow(rowId) {
        cadgRegProductWFTasks.deleteItem(cadgRegProductWFTasks.getItemFromClientId(rowId));
    }
</script>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdRegAdmin_WFDetailCtrl" />
