﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdRegAdmin_WFDetailCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid WFInstID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.WFInstID], Guid.Empty);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            cadgRegProductWFTasks.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            cadgRegProductWFTasks.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            cadgRegProductWFTasks.NeedChildDataSource += new ComponentArt.Web.UI.Grid.NeedChildDataSourceEventHandler(OnNeedChildData);
            cadgRegProductWFTasks.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            cadgRegProductWFTasks.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            cadgRegProductWFTasks.ItemCommand += new ComponentArt.Web.UI.Grid.ItemCommandEventHandler(cadgRegProductWFTasks_ItemCommand);
            cadgRegProductWFTasks.DeleteCommand += new ComponentArt.Web.UI.Grid.GridItemEventHandler(this.cadgRegProductWFTasks_DeleteCommand);
            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindWFMaster();
            }
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = UIHelper.GetBrowserLang();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }


        private void BindWFMaster()
        {
            //DataRow r = Lib.ProductRegistration.Acc_ProductRegWorkflowBLL.WFMaster_Select(WFInstID);
            //if (r == null)
            //{
            //    this.Visible = false;
            //    return;
            //}

            //fvWFMaster.DataSource = r.Table;
            //fvWFMaster.DataBind();

            //Guid accID = ConvertUtility.ConvertToGuid(r["AccountID"], Guid.Empty);
            //BindCompanyInfo(accID);
            //buildTopLevel();
            //cadgRegProductWFTasks.DataBind();
        }

        public void BindCompanyInfo(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return;
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            if (acc == null) return;

            ltrCompanyName.Text = acc.CompanyName;
            ltrRuby.Text = acc.CompanyNameInRuby;
            ltrAccountStatusCode.Text = acc.AccountStatusCode;
            if(acc.IsVerifiedAndApproved)
            {
                imgAccountVerifiedAndApproved.ImageUrl = "//static.cdn-anritsu.com/apps/connect/img/verified.gif";
                bttVerifyAndApproveAccount.Visible = false;
            }
            else
            {
                imgAccountVerifiedAndApproved.ImageUrl = "//static.cdn-anritsu.com/apps/connect/img/alert.png";
                bttVerifyAndApproveAccount.Visible = true;
            }
            ltrAgreedToTerms.Text = acc.AgreedToTerms.ToString();
            ltrAgreedToTermsOnUTC.Text = "n/a";
            if (acc.AgreedToTermsOnUTC != DateTime.MinValue) ltrAgreedToTermsOnUTC.Text = GetDate(acc.AgreedToTermsOnUTC.ToString());
            ltrCompanyAddress.Text = acc.CompanyAddress;
        }

        private void buildTopLevel()
        {
           // cadgRegProductWFTasks.DataSource = Lib.ProductRegistration.Acc_ProductRegWorkflowBLL.WFTask_SelectByWF(WFInstID);
        }

        public void OnNeedRebind(object sender, EventArgs e)
        {
            cadgRegProductWFTasks.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs e)
        {
            buildTopLevel();
        }

        public void OnNeedChildData(object sender, ComponentArt.Web.UI.GridNeedChildDataSourceEventArgs e)
        {
            if (e.Item.Level == 0)
            {
                Int32 wfTaskID = ConvertUtility.ConvertToInt32(e.Item["WFTaskID"], 0);
              //  e.DataSource = Lib.ProductRegistration.Acc_ProductRegWorkflowBLL.WFTaskData_SelectByTaskID(wfTaskID);
            }
            


        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs e)
        {
            cadgRegProductWFTasks.CurrentPageIndex = e.NewIndex;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs e)
        {
            cadgRegProductWFTasks.Sort = e.SortExpression;
        }

        public void cadgRegProductWFTasks_ItemCommand(object sender, ComponentArt.Web.UI.GridItemCommandEventArgs e)
        {
            Int32 wfTaskID = ConvertUtility.ConvertToInt32(e.Item["WFTaskID"], 0);

        }

        private void cadgRegProductWFTasks_DeleteCommand(object sender, ComponentArt.Web.UI.GridItemEventArgs e)
        {
            Int32 wfTaskID = ConvertUtility.ConvertToInt32(e.Item["WFTaskID"], 0);
            //Lib.ProductRegistration.Acc_ProductRegWorkflowBLL.WFTask_SelectByWF
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }
}