﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_RenewSupportCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdReg_RenewSupportCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlProductInfo" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_RenewSupportCtrl,pnlProductInfo.HeaderText %>">
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalMN" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalMN.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrMN" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalSN" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalSN.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrSN" runat="server"></asp:Literal>
    </div>
    <div class="settingrow" id="divPaidSupportExp" runat="server">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalSupportExpiredOn" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalSupportExpiredOn.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrSupportExpiredOn" runat="server"></asp:Literal>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlCompanyInfo" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_RenewSupportCtrl,pnlCompanyInfo.HeaderText %>">
    <div class="settingrow" style="padding-right: 250px">
        <span class="msg">
            <asp:Literal ID="ltrUpdateProfileMsg" runat="server" Visible="false" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,ltrUpdateProfileMsg.Text%>"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalUserFullName" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalUserFullName.Text%>"></asp:Localize>:</span>
        <span>
            <asp:Literal ID="ltrUserFullName" runat="server"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalUserEmail" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalUserEmail.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrUserEmail" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <asp:Literal ID="ltrCompanyInfo" runat="server"></asp:Literal></div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlRenewAddOnForm" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_RenewSupportCtrl,pnlRenewAddOnForm.HeaderText %>" Visible="false">
<div class="settingrow">
<asp:PlaceHolder ID="phRenewAddOnForm" runat="server"></asp:PlaceHolder>
</div>
</anrui:GlobalWebBoxedPanel>
<div class="settingrowrgt">
        <asp:Button ID="bttSubmit" runat="server" Text="<%$Resources:STCTRL_ProdReg_RenewSupportCtrl,bttSubmit.Text%>" OnClick="bttSubmit_Click" />
</div>
<div class="settingrow" style='height: 10px;'>&nbsp;</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_RenewSupportCtrl" />
