﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using System.Globalization;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdReg_RenewSupportCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid RegisteredProductWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        private Boolean RequestSentToAdmin
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState["vs_PaidSupportRenewSent"], false); }
            set { ViewState["vs_PaidSupportRenewSent"] = value; }
        }

        private String ModelNumbersToRenew
        {
            get
            {
                return Request.QueryString[KeyDef.QSKeys.ModelNumber].ConvertNullToEmptyString().Trim();
            }
        }

        private Lib.ProductRegistration.Acc_ProductRegistered ProductReg
        {
            get
            {
                Lib.ProductRegistration.Acc_ProductRegistered prodReg = UIHelper.User_ProdRegInfoGetActive(RegisteredProductWebAccessKey);
                Lib.Account.Acc_AccountMaster selectedAccount = UIHelper.GetSelectedAccount(false, true);
                if (prodReg == null || prodReg.AccountID != selectedAccount.AccountID) throw new HttpException(404, "Page not found.");
                return prodReg;
            }
        }

        private Lib.Erp.Erp_ProductConfig_TBD ProductCfg
        {
            get
            {
                string mn = ProductReg.ModelNumber;
                App_Lib.AppCacheFactories.ProductConfigCacheFactory dataCache = new App_Lib.AppCacheFactories.ProductConfigCacheFactory();
                Lib.Erp.Erp_ProductConfig_TBD prdCfg = dataCache.GetProductConfigInfo(mn.Trim());
                Lib.Account.Acc_AccountMaster selectedAccount = UIHelper.GetSelectedAccount(false, true);
                if (prdCfg == null || (prdCfg.VerifyCompany && !selectedAccount.IsVerifiedAndApproved))
                {
                    throw new HttpException(404, "Page not found.");
                }
                return prdCfg;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitProductInfo();
            }
            LoadAddOnFormForRenew(ProductCfg, ProductReg);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    InitProductInfo();
            //}
        }

        private void InitProductInfo()
        {
            Guid accountID = UIHelper.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);

            App_Lib.AnrSso.ConnectSsoUser user = UIHelper.GetCurrentUserOrSignIn();
            Lib.Security.Sec_UserMembership memUser = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(user.MembershipId);

            Lib.ProductRegistration.Acc_ProductRegistered prodReg = ProductReg;
            Lib.Erp.Erp_ProductConfig_TBD prdCfg = ProductCfg;

            pnlCompanyInfo.HeaderText += String.Format("&nbsp;&nbsp;&nbsp;&nbsp;<a class='smalllink' href='/myaccount/companyprofile?{0}={1}' >{2}</a>"
               , KeyDef.QSKeys.ReturnURL
               , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}"
                 , KeyDef.UrlList.ProductSupportRenew
                 , KeyDef.QSKeys.RegisteredProductWebAccessKey
                 , RegisteredProductWebAccessKey.ToString()))
               , this.GetGlobalResourceObject("STCTRL_ProdReg_RenewSupportCtrl", "hlEditCompanyInfo.Text")
               );

            String firstnameLanguage = DetectNameLanguage(user.FirstName);
            String lastnameLanguage = DetectNameLanguage(user.LastName);

            ltrUserFullName.Text = user.LastName + "," + user.FirstName;

            if ((firstnameLanguage.StartsWith("ja") || firstnameLanguage.StartsWith("en")) && (lastnameLanguage.StartsWith("ja") || lastnameLanguage.StartsWith("en")))
            {
                ltrUserFullName.Text = user.LastName + "," + user.FirstName;
            }
            else if (!string.IsNullOrEmpty(memUser.FirstNameInEnglish) && !string.IsNullOrEmpty(memUser.LastNameInEnglish))
            {
                ltrUserFullName.Text = memUser.LastNameInEnglish + "," + memUser.FirstNameInEnglish;
            }
            else
            {
                ltrUpdateProfileMsg.Visible = true;
                bttSubmit.Enabled = false;
            }

            ltrCompanyInfo.Text = CompanyProfileWzCtrl.GetReadOnlyCompanyInfo(accountID, false, false, true);

            // ltrUserFullName.Text = user.FullName;
            ltrUserEmail.Text = user.Email;

            ltrMN.Text = prodReg.ModelNumber;
            if (!ModelNumbersToRenew.IsNullOrEmptyString()) ltrMN.Text += String.Format(" [{0}]", ModelNumbersToRenew);

            ltrSN.Text = prodReg.SerialNumber;
            if (prdCfg.IsPaidSupportModel) ltrSupportExpiredOn.Text = GetDate(prodReg.JPSupportExpireOn);
            divPaidSupportExp.Visible = prdCfg.IsPaidSupportModel;

            //LoadAddOnFormForRenew(prdCfg, prodReg);

        }

        private void LoadAddOnFormForRenew(Lib.Erp.Erp_ProductConfig_TBD prdCfg, Lib.ProductRegistration.Acc_ProductRegistered prodReg)
        {
            if (prodReg == null || prdCfg == null || prdCfg.AddOnFormID.IsNullOrEmptyGuid()) return;
            
            Lib.Security.Sec_UserMembership loggedInUser = App_Lib.UIHelper.GetCurrentUserOrSignIn();
            if (loggedInUser == null) return;

            Guid addOnFormID = prdCfg.AddOnFormID;
            Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.AddOnFormID);
            if (formData == null || formData.FormID.IsNullOrEmptyGuid()) return;

            pnlRenewAddOnForm.HeaderText = GetStaticResource("RenewHeaderInfo");
            //this.GetGlobalResourceObject(String.Format("FRM_{0}", prdCfg.AddOnFormID.ToString().ToUpperInvariant()), "HeaderText").ToString();
            App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phRenewAddOnForm, formData, loggedInUser);

            DataTable tbAddOnData = Lib.ProductRegistration.Acc_ProductRegisteredDataBLL.SelectByProdRegID(prodReg.ProdRegID);
            if (tbAddOnData != null)
            {
                App_Features.DynamicForm.UILib.DynamicFormUtility.SetFormData(phRenewAddOnForm, tbAddOnData);
                pnlRenewAddOnForm.Visible = true;
            }

            
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = UIHelper.GetBrowserLang();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        private String DetectNameLanguage(String input)
        {
            if (input.IsNullOrEmptyString()) return String.Empty;

            String detectedLang = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.DetectTxtLanguage(input);
            if (detectedLang != "en")//just in case if google is wrong
            {
                Regex engRgex = new Regex("[a-zA-Z0-9']+");
                MatchCollection enChars = engRgex.Matches(input);
                if (enChars != null && enChars.Count > 0) detectedLang = "en";
            }
            return detectedLang;
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            SubmitRenewRequest();
        }

        private void SubmitRenewRequest()
        {
            if (!Page.IsValid) return;

            Guid accountID = UIHelper.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);

            App_Lib.AnrSso.ConnectSsoUser user = UIHelper.GetCurrentUserOrSignIn();
            Lib.ProductRegistration.Acc_ProductRegistered prodReg =
                    Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectActiveByWebAccessKey(UIHelper.GetSelectedAccountID(true), RegisteredProductWebAccessKey);
            if (prodReg == null) throw new HttpException(404, "Page not found.");

            Lib.Erp.Erp_ProductConfig_TBD prdCfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(prodReg.ModelNumber);
            if (prdCfg == null) throw new HttpException(404, "Page not found.");

            List<KeyValuePair<String, String>> addOnFormData = CaptureAddOnFormData();
            String mnToRenew = Request.QueryString[KeyDef.QSKeys.ModelNumber].ConvertNullToEmptyString().Trim();
            String userIP = WebUtility.GetUserIP();
            String geoCountryCode = String.Empty;
            AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi != null) geoCountryCode = gi.CountryCode;

            if (!RequestSentToAdmin)
            {
                
                Boolean emailSent = false;
                String msg = String.Empty;
                RequestSentToAdmin = Lib.ProductRegistration.Acc_ProductRegisteredWF.SubmitSupportRenewal(prodReg, prdCfg,
                    acc, user, mnToRenew, addOnFormData, userIP, geoCountryCode, out emailSent, out msg);

                if (!emailSent) throw new ArgumentException("Unable to send email - " + msg);
                if (!RequestSentToAdmin) throw new ArgumentException("ERROR: " + msg);
            }

            if (RequestSentToAdmin)
            {
                String url = String.Format("{0}", KeyDef.UrlList.ProductSupportRenewAck);
                WebUtility.HttpRedirect(this, url);
            }
        }

        private List<KeyValuePair<String, String>> CaptureAddOnFormData()
        {
            #region " capture data "

            List<KeyValuePair<string, string>> formData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(phRenewAddOnForm);
            if (formData != null)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                foreach (KeyValuePair<string, string> kv in formData)
                {
                    if (kv.Key.IsNullOrEmptyString()) continue;
                    if (!data.ContainsKey(kv.Key.Trim()))
                    {
                        data.Add(kv.Key.Trim(), kv.Value.Trim());
                    }
                }
            }

            return formData;
            #endregion
        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdReg_RenewSupportCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey) as String;
        }
    }
}