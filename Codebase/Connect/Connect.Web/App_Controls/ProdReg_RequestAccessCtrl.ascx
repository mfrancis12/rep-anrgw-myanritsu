﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_RequestAccessCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdReg_RequestAccessCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlProductInfo1" runat="server" ShowHeader="true"
    HeaderText="<%$ Resources:STCTRL_RegProduct-RequestAccess,pnlProductInfo.HeaderText %>">
    <div class="settingrow">
        <center>
            <asp:Localize ID="lcalMsg" runat="server"></asp:Localize></center>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalModelNumberTxt" runat="server" Text="<%$ Resources:STCTRL_RegProduct-RequestAccess,ModelNumber.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrlModelNumber" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalSNTxt" runat="server" Text="<%$ Resources:STCTRL_RegProduct-RequestAccess,SerialNumber.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrlSerialNumber" runat="server"></asp:Literal>
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:GlobalWebBoxedPanel ID="pnlCompanyInfo" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_RegProduct-RequestAccess,pnlCompanyInfo.HeaderText %>">
    <div class="settingrow" style="padding-left: 250px">
        <span class="msg">
            <asp:Literal ID="ltrUpdateProfileMsg" runat="server" Visible="false" Text="<%$Resources:STCTRL_RegProduct-RequestAccess,trUpdateProfileMsg.Text%>"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalUserFullName" runat="server" Text="<%$Resources:STCTRL_RegProduct-RequestAccess,lcalUserFullName.Text%>"></asp:Localize>:</span>
        <span>
            <asp:Literal ID="ltrUserFullName" runat="server"></asp:Literal>
        </span>
    </div>
    <div class="settingrow">
        <span class="settinglabelplain">
            <asp:Localize ID="lcalUserEmail" runat="server" Text="<%$Resources:STCTRL_RegProduct-RequestAccess,lcalUserEmail.Text%>"></asp:Localize>:</span>
        <asp:Literal ID="ltrUserEmail" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <asp:Literal ID="ltrCompanyInfo" runat="server"></asp:Literal></div>
</anrui:GlobalWebBoxedPanel>
<div class="settingrow">
    <center>
        <asp:Button ID="bttSubmit" runat="server" SkinID="SmallButton" Text="<%$Resources:STCTRL_RegProduct-RequestAccess,bttSubmit.Text%>"
            OnClick="bttSubmit_Click" /></center>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_RegProduct-RequestAccess" />
