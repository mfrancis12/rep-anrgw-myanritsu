﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdReg_RequestAccessCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {

        public Guid ProdRegWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitProductInfo();
            }
        }
        private void InitProductInfo()
        {
            Guid selectedAccountID = UIHelper.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster account = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(selectedAccountID);
            Lib.Security.Sec_UserMembership usr = UIHelper.GetCurrentUserOrSignIn();
            Lib.Security.Sec_UserMembership memUser = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(usr.MembershipId);

            Lib.ProductRegistration.Acc_ProductRegistered prodReg =
            Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectActiveByWebAccessKey(
                selectedAccountID, ProdRegWebAccessKey);

            if (prodReg == null) throw new HttpException(404, "Page not found.");

            Lib.Erp.Erp_ProductConfig_TBD prdCfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(prodReg.ModelNumber);
            if (prdCfg == null) throw new HttpException(404, "Page not found.");

            Boolean isRequested = false;
            Boolean hasAccess = Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.CheckUserAccess(prodReg.ProdRegID
                , selectedAccountID, usr.MembershipId, out isRequested);

            String firstnameLanguage = DetectNameLanguage(usr.FirstName);
            String lastnameLanguage = DetectNameLanguage(usr.LastName);


            ltrUserFullName.Text = usr.LastName + "," + usr.FirstName;

            if ((firstnameLanguage.StartsWith("ja") || firstnameLanguage.StartsWith("en")) || (lastnameLanguage.StartsWith("ja") || lastnameLanguage.StartsWith("en")))
            {
                ltrUserFullName.Text = usr.LastName + "," + usr.FirstName;
            }
            else if (!string.IsNullOrEmpty(memUser.FirstNameInEnglish) && !string.IsNullOrEmpty(memUser.LastNameInEnglish))
            {
                ltrUserFullName.Text = memUser.LastNameInEnglish + "," + memUser.FirstNameInEnglish;
            }
            else
            {
                ltrUpdateProfileMsg.Visible = true;
                bttSubmit.Enabled = false;
            }

            ltrCompanyInfo.Text = CompanyProfileWzCtrl.GetReadOnlyCompanyInfo(selectedAccountID, false, false, true);

            // ltrUserFullName.Text = user.FullName;
            ltrUserEmail.Text = usr.Email;
            ltrlModelNumber.Text = prodReg.ModelNumber;
            ltrlSerialNumber.Text = prodReg.SerialNumber;
            //Hlstatus.NavigateUrl = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegHomeUrl");

            pnlCompanyInfo.HeaderText += String.Format("&nbsp;&nbsp;&nbsp;&nbsp;<a class='smalllink' href='../myaccount/companyprofile?{0}={1}' >{2}</a>"
            , KeyDef.QSKeys.ReturnURL
              , HttpUtility.UrlEncode(String.Format("{0}?{1}={2}"
                 , KeyDef.UrlList.RegProd_RequestAccess
                 , KeyDef.QSKeys.RegisteredProductWebAccessKey
                 , ProdRegWebAccessKey.ToString()))
            , this.GetGlobalResourceObject("STCTRL_RegProduct-RequestAccess", "hlEditCompanyInfo.Text")
            );

            if (!prdCfg.UserACLRequired || hasAccess)
            {
                String url = String.Format("{0}?{1}={2}"
                    , KeyDef.UrlList.ProductSupport
                    , KeyDef.QSKeys.RegisteredProductWebAccessKey
                    , ProdRegWebAccessKey);

                WebUtility.HttpRedirect(null, url);
            }

            if (prdCfg.UserACLRequired && !hasAccess && isRequested)
            {
                lcalMsg.Text = this.GetGlobalResourceObject("STCTRL_RegProduct-RequestAccess", "lcalMsg.Text.AlreadyRequested").ToString();
                bttSubmit.Visible = false;
            }

        }

        private String DetectNameLanguage(String input)
        {
            if (input.IsNullOrEmptyString()) return String.Empty;

            String detectedLang = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.DetectTxtLanguage(input);
            if (detectedLang != "en")//just in case if google is wrong
            {
                Regex engRgex = new Regex("[a-zA-Z0-9']+");
                MatchCollection enChars = engRgex.Matches(input);
                if (enChars != null && enChars.Count > 0) detectedLang = "en";
            }
            return detectedLang;
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            Guid selectedAccountID = UIHelper.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster account = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(selectedAccountID);
            Lib.Security.Sec_UserMembership usr = UIHelper.GetCurrentUserOrSignIn();

            Lib.ProductRegistration.Acc_ProductRegistered prodReg =
            Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectActiveByWebAccessKey(
                selectedAccountID, ProdRegWebAccessKey);

            if (prodReg == null) throw new HttpException(404, "Page not found.");

            Lib.Erp.Erp_ProductConfig_TBD prdCfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(prodReg.ModelNumber);
            if (prdCfg == null) throw new HttpException(404, "Page not found.");

            Boolean isRequested = false;
            Boolean hasAccess = Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.CheckUserAccess(prodReg.ProdRegID
                , selectedAccountID, usr.MembershipId, out isRequested);

            if (!prdCfg.UserACLRequired || hasAccess)
            {
                String url = String.Format("{0}?{1}={2}"
                    , KeyDef.UrlList.ProductSupport
                    , KeyDef.QSKeys.RegisteredProductWebAccessKey
                    , ProdRegWebAccessKey);

                WebUtility.HttpRedirect(null, url);
            }

            if (prdCfg.UserACLRequired && !hasAccess && isRequested)
            {
                lcalMsg.Text = this.GetGlobalResourceObject("STCTRL_RegProduct-RequestAccess", "lcalMsg.Text.AlreadyRequested").ToString();
                bttSubmit.Visible = false;
            }
            else
            {
                Lib.ProductRegistration.Acc_ProductRegisteredUserACLBLL.Insert_AsRequest(prodReg, prdCfg, usr, account, UIHelper.GetGlobalWebCultureGroupId());
                String sendUrl = String.Format("{0}", KeyDef.UrlList.RegProd_RequestAccessAck);
                WebUtility.HttpRedirect(this, sendUrl);
            }

        }

        public string StaticResourceClassKey
        {
            get { return "STCTRL_RegProduct-RequestAccess"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }
        //public static String GetResourceString(string resourceKey)
        //{
        //    String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_RegProduct-RequestAccess", resourceKey));
        //    if (String.IsNullOrEmpty(str)) str = resourceKey;
        //    return str;
        //}

    }
}