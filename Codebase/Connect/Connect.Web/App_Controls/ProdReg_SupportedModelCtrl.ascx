﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdReg_SupportedModelCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ProdReg_SupportedModelCtrl" EnableViewState="false"%>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$ Resources:STCTRL_ProdReg_SupportedModelCtrl,pnlContainer.HeaderText %>">
<asp:Panel ID="pnlSupportedModelSearch" runat="server" DefaultButton="bttModelSearch">
<div class="settingrow"><asp:Localize ID="lcalModelSearchDesc" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,lcalModelSearchDesc.Text%>"></asp:Localize></div>
<div class="settingrow">
    <asp:TextBox ID="txtModelSearch" runat="server"></asp:TextBox>
    <asp:Button ID="bttModelSearch" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,bttModelSearch.Text%>" SkinID="SmallButton" onclick="bttModelSearch_Click" />
</div>
<div class="settingrow">
<ComponentArt:DataGrid ID="cadgModels" runat="server" ClientIDMode="AutoID" ShowSearchBox="true" ShowFooter="true" PageSize="10"
 AllowPaging="true" AutoTheming="true" PagerStyle="Buttons" RunningMode="Server" EmptyGridText="<%$ Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.EmptyGridText %>">
<Levels>
    <ComponentArt:GridLevel DataKeyField="ModelNumber">
        <Columns>
            <ComponentArt:GridColumn DataField="ModelNumber" HeadingText="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.ColMN%>"/>
            <ComponentArt:GridColumn DataCellServerTemplateId="gstRegister" Width="50"/>
        </Columns>
    </ComponentArt:GridLevel>
</Levels>
<ServerTemplates>
    <ComponentArt:GridServerTemplate ID="gstRegister">
        <Template>
            <asp:HyperLink ID="hlRegister" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.TextRegister%>" 
            NavigateUrl='<%# "~/myproduct/regproduct-serialnumbers?mn=" + Container.DataItem["ModelNumber"] %>'></asp:HyperLink>
        </Template>
    </ComponentArt:GridServerTemplate>
</ServerTemplates>
<ClientTemplates>
    <ComponentArt:ClientTemplate ID="ctRegister">
         <a href='/myproduct/regproduct-serialnumbers?mn=## DataItem.getMember("ModelNumber").get_value() ##' title='Register'>
         <asp:Localize ID="lcalRegister" runat="server" Text="<%$Resources:STCTRL_ProdReg_SupportedModelCtrl,cadgModels.TextRegister%>"></asp:Localize></a>
    </ComponentArt:ClientTemplate>
    <ComponentArt:ClientTemplate ID="ctImage">
        <img src='## DataItem.getMember("ImageUrl").get_value() ##' alt='## DataItem.getMember("ModelNumber").get_value() ##' width="100px" height="100px" />
    </ComponentArt:ClientTemplate>
</ClientTemplates>
</ComponentArt:DataGrid>
</div>
</asp:Panel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_SupportedModelCtrl" />
</anrui:GlobalWebBoxedPanel>