﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using ComponentArt.Web.UI;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdReg_SupportedModelCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString["mn"]).Trim().ToUpperInvariant();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            cadgModels.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            cadgModels.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            cadgModels.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            cadgModels.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            cadgModels.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(OnFilter);
            base.OnInit(e);

            if (!IsPostBack)
            {
                String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
                WebUtility.GenerateProcessingScript(bttModelSearch, "onclick", processingText);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                txtModelSearch.Text = ModelNumber;
                GetGridDataSource();
                cadgModels.DataBind();
                
                
            }


        }

        private void GetGridDataSource()
        {
            App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory dataCache
                = new App_Lib.AppCacheFactories.ProductConfigActiveModelCacheFactory();
            DataTable tb = dataCache.GetActiveModels();
            DataView dv = null;
            if(tb != null)
            {
                dv = tb.DefaultView;
                if (ModelNumber.IsNullOrEmptyString())
                    dv.RowFilter = String.Empty;
                else
                    dv.RowFilter = String.Format("ModelNumber LIKE '{0}%'", ModelNumber.Trim());
            }
            cadgModels.DataSource = dv;
        }

        public void OnNeedRebind(object sender, EventArgs oArgs)
        {
            cadgModels.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs oArgs)
        {
            GetGridDataSource();
        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cadgModels.CurrentPageIndex = oArgs.NewIndex;
        }

        public void OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cadgModels.Filter = oArgs.FilterExpression;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cadgModels.Sort = oArgs.SortExpression;
        }

        protected void bttModelSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.LocalPath + "?mn=" + HttpUtility.UrlEncode(txtModelSearch.Text.Trim()));
        }

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }
    }

}