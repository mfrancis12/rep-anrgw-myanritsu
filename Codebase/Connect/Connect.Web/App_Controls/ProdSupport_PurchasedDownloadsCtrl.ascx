﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdSupport_PurchasedDownloadsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ProdSupport_PurchasedDownloadsCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true"  HeaderText='<%$ Resources:STCTRL_ProdSupport_PurchasedDownloadsCtrl,pnlContainer.HeaderText %>'>
<div class="settingrow" style="min-height: 150px;">
<center>
<asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false">
    <Columns>
       
    </Columns>
    <EmptyDataTemplate>
        <div style="width: 650px;"><p class="emptymsg"><asp:Localize ID="lcalNoWarrantyData" runat="server" Text='<%$ Resources:STCTRL_ProdSupport_PurchasedDownloadsCtrl,lcalNoWarrantyData.Text %>'></asp:Localize></p></div>
    </EmptyDataTemplate>
</asp:GridView>
</center>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdSupport_PurchasedDownloadsCtrl" />
</anrui:GlobalWebBoxedPanel>