﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
//using GP = Anritsu.AnrCommon.GlobalProductInfoLib;
//using DLSDK = Anritsu.AnrCommon.GWDownloadRouteInfo.SDK;


namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ProdSupport_PurchasedDownloadsCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPurchasedDownloads();
            }
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_ProdSupport_PurchasedDownloadsCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        private void BindPurchasedDownloads()
        {
            gvList.DataSource = null;
            gvList.DataBind();
        }
    }
}