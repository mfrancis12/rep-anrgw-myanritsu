﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProdSupport_UpdateForm.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.ProdSupport_UpdateForm" %>
<anrui:GlobalWebBoxedPanel ID="pnlAddOnFormInfo" runat="server" ShowHeader="true"
    HeaderText="<%$Resources:STCTRL_ProdReg_UpdateForm,pnlAddOnFormInfo.HeaderText%>">
    <%--<div class="settingrow"><asp:Localize ID="lcalOtherInfoDesc" runat="server" Text="<%$ Resources:STCTRL_ProdReg_UpdateForm,lcalOtherInfoDesc.Text %>"></asp:Localize></div>
<div class="settingrow">
    <span class="settinglabelplain"><asp:Localize ID="lcalMN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_UpdateForm,lcalMN.Text %>"></asp:Localize>: </span>
    <asp:Literal ID="ltrMN" runat="server" Text=""></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabelplain"><asp:Localize ID="lcalSN" runat="server" Text="<%$ Resources:STCTRL_ProdReg_UpdateForm,lcalSN.Text %>"></asp:Localize>: </span>
    <asp:Literal ID="ltrSN" runat="server" Text=""></asp:Literal>
</div>
<div class="settingrow">
<asp:PlaceHolder ID="phSNAddOnForm" runat="server"></asp:PlaceHolder>
</div>--%>
    <div class="settingrow">
        <asp:PlaceHolder ID="phAddOnFormInfo" runat="server"></asp:PlaceHolder>
    </div>
    <div class="settingrow" style="text-align: right;">
      <p class="msg">
            <asp:Literal ID="ltrUpdateFormMsg" runat="server"></asp:Literal></p>
        <asp:Button ID="bttUpdateForm" runat="server" Text="<%$ Resources:STCTRL_ProdReg_UpdateForm,bttUpdateForm.Text %>"
            OnClick="bttUpdateForm_Click" />
    </div>
</anrui:GlobalWebBoxedPanel>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ProdReg_UpdateForm" />
