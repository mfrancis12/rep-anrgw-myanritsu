﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    ////public partial class ProdSupport_UpdateForm : CustomControlBase.ProdRegAdmin_DetailBase, App_Lib.UI.IStaticLocalizedCtrl
    ////{
    ////    public String ReturnURL
    ////    {
    ////        get
    ////        {
    ////            String returnURL = Request.QueryString[KeyDef.QSKeys.ReturnURL];
    ////            if (String.IsNullOrEmpty(returnURL)) returnURL = "~/myproduct/productsupport_updateform";
    ////            return returnURL;
    ////        }
    ////    }

    ////    protected void Page_Init(object sender, EventArgs e)
    ////    {
    ////        if (!IsPostBack)
    ////        {
    ////        }
    ////        LoadAddOnForm(ProductCfg, ProductReg);
    ////    }

    ////    protected void Page_Load(object sender, EventArgs e)
    ////    {

    ////    }

    ////    private void LoadAddOnForm(Lib.Erp.Erp_ProductConfig_TBD prdCfg, Lib.ProductRegistration.Acc_ProductRegistered prodReg)
    ////    {
    ////        if (prodReg == null || prdCfg == null || prdCfg.AddOnFormID.IsNullOrEmptyGuid()) return;

    ////        Lib.Security.Sec_UserMembership userInfo = App_Lib.UIHelper.GetCurrentUserOrSignIn();
    ////        //Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(MembershipID);
    ////        if (userInfo == null) return;

    ////        Guid addOnFormID = prdCfg.AddOnFormID;
    ////        Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.AddOnFormID);
    ////        if (formData == null || formData.FormID.IsNullOrEmptyGuid()) return;

    ////        App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phAddOnFormInfo, formData, userInfo);

    ////        DataTable tbAddOnData = Lib.ProductRegistration.Acc_ProductRegisteredDataBLL.SelectByProdRegID(prodReg.ProdRegID);
    ////        if (tbAddOnData != null)
    ////        {
    ////            App_Features.DynamicForm.UILib.DynamicFormUtility.SetFormData(phAddOnFormInfo, tbAddOnData);
    ////            phAddOnFormInfo.Visible = true;
    ////        }
    ////    }

    ////    private void SubmitForm()
    ////    {
    ////        App_Lib.AnrSso.ConnectSsoUser user = UIHelper.GetCurrentUserOrSignIn();

    ////        List<KeyValuePair<String, String>> addOnFormData = CaptureAddOnFormData();
    ////        String mnToRenew = Request.QueryString[KeyDef.QSKeys.ModelNumber].ConvertNullToEmptyString().Trim();
    ////        String userIP = WebUtility.GetUserIP();
    ////        String geoCountryCode = String.Empty;
    ////        AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
    ////        if (gi != null) geoCountryCode = gi.CountryCode;

    ////        Lib.ProductRegistration.Acc_ProductRegisteredWF.SubmitForm(ProductReg, ProductCfg,
    ////                user, mnToRenew, addOnFormData, userIP, geoCountryCode);
    ////    }

    ////    private List<KeyValuePair<String, String>> CaptureAddOnFormData()
    ////    {
    ////        #region " capture data "

    ////        List<KeyValuePair<string, string>> formData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(phAddOnFormInfo);
    ////        if (formData != null)
    ////        {
    ////            Dictionary<string, string> data = new Dictionary<string, string>();
    ////            foreach (KeyValuePair<string, string> kv in formData)
    ////            {
    ////                if (kv.Key.IsNullOrEmptyString()) continue;
    ////                if (!data.ContainsKey(kv.Key.Trim()))
    ////                {
    ////                    data.Add(kv.Key.Trim(), kv.Value.Trim());
    ////                }
    ////            }
    ////        }

    ////        return formData;
    ////        #endregion
    ////    }

    ////    protected void bttUpdateForm_Click(object sender, EventArgs e)
    ////    {
    ////        if (!Page.IsValid) return;
    ////        SubmitForm();
    ////        ltrUpdateFormMsg.Text = GetStaticResource("UpdatedSuccessMsg");
    ////        // WebUtility.HttpRedirect(null, Request.RawUrl);
    ////        // WebUtility.HttpRedirect(null, ReturnURL);
    ////    }

    ////    #region old code
    ////    //private void LoadData(Lib.Security.Sec_UserMembership loggedInUser, Lib.DynamicForm.Frm_Form formData)
    ////    //{
    ////    //    DataTable formdt = Lib.ProductRegistration.Acc_ProductRegisteredDataBLL.SelectByProdRegID(ProdRegID);

    ////    //    #region " capture data "
    ////    //    //capture data for the SN(s) here if valid
    ////    //    List<KeyValuePair<string, string>> formsData = App_Features.DynamicForm.UILib.DynamicFormUtility.GetFormValues(phSNAddOnForm);
    ////    //    if (formData != null)
    ////    //    {
    ////    //        Dictionary<string, string> data = new Dictionary<string, string>();
    ////    //        foreach (KeyValuePair<string, string> kv in formsData)
    ////    //        {
    ////    //            if (kv.Key.IsNullOrEmptyString()) continue;
    ////    //            //if (!data.ContainsKey(kv.Key.Trim()))
    ////    //            //{
    ////    //            //    data.Add(kv.Key.Trim(), kv.Value.Trim());
    ////    //            //}
    ////    //            foreach (DataRow rItem in formdt.Rows)
    ////    //            {
    ////    //                if (kv.Key.Equals(rItem["DataKey"].ToString()))
    ////    //                {
    ////    //                    if (!data.ContainsKey(kv.Key.Trim()))
    ////    //                    {
    ////    //                    data.Add(kv.Key.Trim(), rItem["DataValue"].ToString());
    ////    //                    }
    ////    //                }

    ////    //            }
    ////    //        }

    ////    //        //foreach (Control ctl in phSNAddOnForm.Controls)
    ////    //        //{
    ////    //        //    ctl.
    ////    //        //}

    ////    //        //App_Features.DynamicForm.UILib.DynamicFormUtility.LoadFormControls(Page, phSNAddOnForm, formData, loggedInUser, data);

    ////    //        //if (prdCfg.AddOnFormPerSN)
    ////    //        //{
    ////    //        //    Lib.ProductRegistration.Acc_ProductRegCartItemDataBLL.InsertUpdate(CartItemID, data);
    ////    //        //}
    ////    //        //else
    ////    //        //{
    ////    //            //foreach (Lib.ProductRegistration.Acc_ProductRegCartItem item in cartg.CartItems)
    ////    //            //{
    ////    //            //    Lib.ProductRegistration.Acc_ProductRegCartItemDataBLL.InsertUpdate(item.CartItemID, data);
    ////    //            //}
    ////    //       // }

    ////    //    }
    ////    //    #endregion
    ////    //}

    ////    //private void CheckProductReg()
    ////    //{
    ////    //    Lib.ProductRegistration.Acc_ProductRegCart cartg =
    ////    //        Lib.ProductRegistration.Acc_ProductRegCartBLL.SelectByWebAccessKey(ProdRegCartWebAccessKey);
    ////    //    if (cartg == null) throw new HttpException(404, "Page not found.");
    ////    //    Guid accountID = UIHelper.GetSelectedAccountID(true);
    ////    //    if (!cartg.AccountID.Equals(accountID)) throw new HttpException(404, "Page not found.");

    ////    //    String userEmail = UIHelper.GetCurrentUserOrSignIn().Email;
    ////    //    if (!cartg.LatestUserEmail.Equals(userEmail))
    ////    //    {
    ////    //        //cart was created by other user in the same account
    ////    //        Lib.ProductRegistration.Acc_ProductRegCartBLL.UpdateUser(cartg.CartWebAccessKey, userEmail);
    ////    //    }
    ////    //    Lib.Erp.Erp_ProductConfig prdCfg = Lib.Erp.Erp_ProductConfigBLL.SelectByModelNumber(cartg.ModelNumber);
    ////    //    if (prdCfg == null) throw new HttpException(404, "Page not found."); ;

    ////    //    //if (CartItemID > 0)
    ////    //    //{
    ////    //    //    if (!Lib.ProductRegistration.Acc_ProductRegCartItemBLL.IsContainInItem(cartg.CartItems, CartItemID)) //someone hacking
    ////    //    //        throw new HttpException(404, "Page not found.");
    ////    //    //    Lib.ProductRegistration.Acc_ProductRegCartItem item =
    ////    //    //        Lib.ProductRegistration.Acc_ProductRegCartItemBLL.FindItemByCartItemID(cartg.CartItems, CartItemID);
    ////    //    //    if (item == null) throw new HttpException(404, "Page not found.");
    ////    //    //    else if (item.IsAddOnFormCompleted) WebUtility.HttpRedirect(null, GetNextUrl(prdCfg, cartg));
    ////    //    //}


    ////    //    if (prdCfg.AddOnFormID.IsNullOrEmptyGuid())
    ////    //    {
    ////    //        WebUtility.HttpRedirect(null, GetNextUrl(prdCfg, cartg));
    ////    //    }

    ////    //}


    ////    //Lib.Security.Sec_UserMembership loggedInUser = App_Lib.UIHelper.GetCurrentUserOrSignIn();
    ////    //Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(prdCfg.AddOnFormID);
    ////    //App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phSNAddOnForm, formData, loggedInUser);

    ////    //private String GetNextUrl(Lib.Erp.Erp_ProductConfig prdCfg, Lib.ProductRegistration.Acc_ProductRegCart cart)
    ////    //{
    ////    //    if (cart == null || prdCfg == null) return KeyDef.UrlList.MyProducts;

    ////    //    String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.ProductSupportRenewAck
    ////    //           , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
    ////    //           );

    ////    //    if (!prdCfg.AddOnFormID.IsNullOrEmptyGuid())
    ////    //    {
    ////    //        if (prdCfg.AddOnFormPerSN)
    ////    //        {
    ////    //            Lib.ProductRegistration.Acc_ProductRegCartItem nextItem = cart.NextItemForAddOnForm;
    ////    //            if (nextItem != null)
    ////    //            {
    ////    //                url = String.Format("{0}?{1}={2}&{3}={4}", KeyDef.UrlList.RegProd_Other
    ////    //                   , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
    ////    //                   , KeyDef.QSKeys.ProdRegCartItemID, nextItem.CartItemID
    ////    //                   );
    ////    //            }
    ////    //        }
    ////    //        else
    ////    //        {
    ////    //            url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Other
    ////    //                   , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
    ////    //                   );
    ////    //        }
    ////    //    }
    ////    //    else //addon form is done
    ////    //    {
    ////    //        if (!prdCfg.FeedbackFormID.IsNullOrEmptyGuid())
    ////    //        {
    ////    //            url = String.Format("{0}?{1}={2}", KeyDef.UrlList.RegProd_Feedback
    ////    //                       , KeyDef.QSKeys.ProdRegCartWebAccessKey, ProdRegCartWebAccessKey
    ////    //                       );
    ////    //        }
    ////    //    }
    ////    //    return url;
    ////    //}
    ////    #endregion

    ////    #region IStaticLocalizedCtrl Members

    ////    public string StaticResourceClassKey
    ////    {
    ////        get { return "STCTRL_ProdReg_UpdateForm"; }
    ////    }

    ////    public string GetStaticResource(string resourceKey)
    ////    {
    ////        return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey) as String;
    ////    }

    ////    #endregion
    ////}
}