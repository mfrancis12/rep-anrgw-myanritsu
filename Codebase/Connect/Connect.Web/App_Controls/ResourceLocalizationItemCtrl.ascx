﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceLocalizationItemCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.ResourceLocalizationItemCtrl" %>
<div>
<asp:DataGrid ID="dgList" runat="server" ShowFooter="false" ShowHeader="false" 
        AutoGenerateColumns="false" onitemcommand="dgList_ItemCommand" Width="100%">
        <Columns>
            <asp:TemplateColumn HeaderText="Content" ItemStyle-CssClass="dgstyle1-item-lft" ItemStyle-Width="75%">
                <ItemTemplate>
                    <div>
                    <%# Server.HtmlDecode(Convert.ToString(Eval("ContentSnippet"))) %>
                        </div>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Locale">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "Locale") %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn ItemStyle-CssClass="dgstyle1-item-lft">
                <ItemTemplate>
                    <asp:HyperLink ID="hlEdit" runat="server" 
                    NavigateUrl='<%# BuildResEditUrl(DataBinder.Eval(Container.DataItem, "ContentID") ) %>' Text='<%$ Resources:STCTRL_ResourceLocalizationItemCtrl,hlEdit.Text %>' CssClass="button"></asp:HyperLink>
                    <asp:Button ID="bttDelete" runat="server" Text='<%$ Resources:STCTRL_ResourceLocalizationItemCtrl,bttDelete.Text %>' CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ContentID") %>' 
                    Visible='<%# DataBinder.Eval(Container.DataItem, "Locale").ToString() != "en" %>' />
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_ResourceLocalizationItemCtrl" />
</div>
