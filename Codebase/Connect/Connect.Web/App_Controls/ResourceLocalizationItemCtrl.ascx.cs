﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class ResourceLocalizationItemCtrl : System.Web.UI.UserControl
    {
        public String ClassKey { get { return ( ViewState["ResLClassKey"] == null ? String.Empty : ViewState["ResLClassKey"].ToString()); } set { ViewState["ResLClassKey"] = value; } }
        public String ResKey { get { return (ViewState["ResLResKey"] == null ? String.Empty : ViewState["ResLResKey"].ToString()); } set { ViewState["ResLResKey"] = value; } }

        public String DefaultResourceValue
        {
            private get;
            set;
        }

        protected override void OnInit(EventArgs e)
        {
         
            
            base.OnInit(e);

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty(ClassKey) || String.IsNullOrEmpty(ResKey))
                {
                    this.Visible = false;
                    return;
                }
                BindLocalizedStrings();
            }
        }

        public void BindLocalizedStrings()
        {
            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(ClassKey.Trim(), ResKey.Trim());
            if (tb == null || tb.Rows.Count < 1)
            {
                Int32 defaultContentID = CreateDefaultContent();
                if (defaultContentID < 1)
                {
                    this.Visible = false;
                    return;
                }
                tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(ClassKey.Trim(), ResKey.Trim());
            }
            dgList.DataSource = tb;
            dgList.DataBind();
        }

        private Int32 CreateDefaultContent()
        {
            return Lib.Content.Res_ContentStoreBLL.Insert(ClassKey.Trim(), ResKey.Trim(), "en", DefaultResourceValue ?? ResKey.Trim(), false);
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                TextBox txtLocale = (TextBox)e.Item.FindControl("txtLocale");
                TextBox txtContent = (TextBox)e.Item.FindControl("txtContent");
                CultureInfo locale = new CultureInfo(txtLocale.Text.Trim());
                Lib.Content.Res_ContentStoreBLL.Insert(ClassKey, ResKey, locale.Name, txtContent.Text.Trim(), true);
                BindLocalizedStrings();
            }

            else if (e.CommandName == "DeleteCommand")
            {
                Int32 contentID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.Content.Res_ContentStoreBLL.DeleteByContentID(contentID);
                BindLocalizedStrings();
            }
        }

        public string BuildResEditUrl(object contentIDObj)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(contentIDObj, 0);
            if (contentID < 1) return String.Empty;
            return String.Format("~/siteadmin/contentadmin/contentdetails?contentid={0}&returnurl={1}", contentID, HttpUtility.UrlEncode(Request.RawUrl));
        }
    }
}