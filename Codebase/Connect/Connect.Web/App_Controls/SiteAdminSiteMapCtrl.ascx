﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteAdminSiteMapCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.SiteAdminSiteMapCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlSiteMap" runat="server" ShowHeader="true" HeaderText="Site Map">
    <fieldset>
        <legend>Parent page url</legend>
        <table border="0" cellpadding="5" cellspacing="5">
            <tr>
                <td><asp:TextBox ID="txtParentPageUrl" runat="server" SkinID="tbx-500"></asp:TextBox></td>
                <td><asp:Button ID="bttSave" runat="server" Text="save" onclick="bttSave_Click" SkinID="SmallButton"  ValidationGroup="vgSiteMap" /></td>
            </tr>
        </table>
        <label class="failureNotification"><asp:RequiredFieldValidator ID="rfvSiteMapParentPageUrl" runat="server" ControlToValidate="txtParentPageUrl" ValidationGroup="vgSiteMap" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator></label>
    </fieldset>
    <fieldset>
        <legend>Bread Crumbs</legend>
        <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundColumn DataField="PageUrl" HeaderText="Page Url" ItemStyle-CssClass="dgstyle1-item-lft"></asp:BoundColumn>
            <asp:BoundColumn DataField="ParentPageUrl" HeaderText="Parent Page Url" ItemStyle-CssClass="dgstyle1-item-lft"></asp:BoundColumn>
        </Columns>
        </asp:DataGrid>
    </fieldset>
</anrui:GlobalWebBoxedPanel>