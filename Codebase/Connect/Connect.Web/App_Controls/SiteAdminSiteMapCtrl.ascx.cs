﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class SiteAdminSiteMapCtrl : System.Web.UI.UserControl
    {
        public String PageUrl { get; set; }

        protected override void OnInit(EventArgs e)
        {
            PageUrl = Request.QueryString[App_Lib.KeyDef.QSKeys.PageURL];
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSiteMapData();
            }
        }

        private void LoadSiteMapData()
        {
            txtParentPageUrl.Text = "";
            if (String.IsNullOrEmpty(PageUrl)) return;
            Lib.UI.Site_Sitemap sitemap = Lib.BLL.BLLSite_Sitemap.SelectByPageUrl(PageUrl);
            if (sitemap != null)
            {
                txtParentPageUrl.Text = sitemap.ParentPageUrl;
            }

            dgList.DataSource = Lib.BLL.BLLSite_Sitemap.SelectForBreadCrumbByPageUrl(PageUrl);
            dgList.DataBind();
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            if (!txtParentPageUrl.Text.StartsWith("~/"))
            {
                rfvSiteMapParentPageUrl.IsValid = false;
                return;
            }
            Lib.UI.Site_Sitemap sitemap = Lib.BLL.BLLSite_Sitemap.SelectByPageUrl(PageUrl);
            if (sitemap == null)//new
            {
                Lib.BLL.BLLSite_Sitemap.Insert(PageUrl, txtParentPageUrl.Text.Trim());                
            }
            else
            {
                Lib.BLL.BLLSite_Sitemap.Update(PageUrl, txtParentPageUrl.Text.Trim());
            }
            LoadSiteMapData();
        }
    }
}