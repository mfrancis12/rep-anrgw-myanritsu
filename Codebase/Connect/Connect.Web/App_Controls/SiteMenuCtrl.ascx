﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteMenuCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.SiteMenuCtrl" EnableViewState="false" %>
<div id="sitemenuwrap">
    <asp:Menu ID="smnu" runat="server" ClientIDMode="Static" SkipLinkText=""
        Orientation="Horizontal" DataSourceID="smd" EnableTheming="false" 
        StaticEnableDefaultPopOutImage="false" 
        onmenuitemdatabound="smnu_MenuItemDataBound">
    <StaticMenuItemStyle CssClass="staticitem" HorizontalPadding="15px" VerticalPadding="8px" />
    <DynamicMenuStyle BorderStyle="Solid" BorderWidth="2px" BorderColor="#086F6F"/>
    <DynamicMenuItemStyle CssClass="dynamicitem" BackColor="#E8FFED" HorizontalPadding="10px" VerticalPadding="5px" ForeColor="#086F6F"/>
    </asp:Menu>
<asp:SiteMapDataSource ID="smd" runat="server" StartingNodeUrl="~/" ShowStartingNode="false" EnableViewState="false" />
</div>