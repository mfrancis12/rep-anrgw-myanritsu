﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class SiteMenuCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void smnu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            //if(e.Item != null && e.Item.NavigateUrl.Contains("/app_redir/redirgw"))
            if(e.Item != null && e.Item.NavigateUrl.StartsWith("http"))
            {
                e.Item.Target = "_blank";
            }
        }

        
    }
}