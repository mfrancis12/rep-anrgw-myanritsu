﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StateCtrl.ascx.cs" EnableViewState="true" Inherits="Anritsu.Connect.Web.App_Controls.StateCtrl" %>
<div class="input-select">
<asp:DropDownList ID="cmbState" runat="server">
</asp:DropDownList>
    </div>
<asp:TextBox ID="txtState" runat="server" MaxLength="100"></asp:TextBox>
<span class="msg">
    <asp:RequiredFieldValidator ID="rfvStateBox" runat="server" ControlToValidate="cmbState" Display="Dynamic" ValidationGroup="vgCompInfo"
        ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator></span>

