﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class StateCtrl : System.Web.UI.UserControl
    {
        private int _cultureGroupID;
        private DataTable _states;
        public string ValidationGroup { get; set; }
        public bool _IsUnitedStates { get; set; }
        public bool _IsJapan { get; set; }
        public bool IsStateRequired { get; set; }

        public string SelectedStateText
        {
            get
            {
                if (_IsUnitedStates || _IsJapan)
                {
                    if (cmbState.SelectedItem == null) return string.Empty;
                    return cmbState.SelectedItem.Text;
                }
                else
                {
                    return txtState.Text.Trim();
                }
            }
        }
        public string SelectedStateValue
        {
            get
            {
                if (_IsUnitedStates || _IsJapan)
                {
                    if (cmbState.SelectedItem == null) return string.Empty;
                    return cmbState.SelectedValue;
                }
                else
                {
                    return txtState.Text.Trim();
                }
            }
        }

        //public StateCtrl()
        //{
        //    if (string.IsNullOrEmpty(BasePage.SelectedCountryCode))
        //    {
        //        _IsUnitedStates = (BasePage.GetCurrentCultureGroupId() == 1 ? true : false);
        //        _IsJapan = (BasePage.GetCurrentCultureGroupId() == 2 ? true : false);
        //    }
        //    else
        //    {
        //        _IsUnitedStates = (BasePage.SelectedCountryCode == "US" ? true : false);
        //        _IsJapan = (BasePage.SelectedCountryCode == "JP" ? true : false);
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            _cultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            if (!IsPostBack)
            {
                if (_IsUnitedStates)
                {
                    PopulateCountries("US");
                    cmbState.Visible = rfvStateBox.Enabled = true; 
                    txtState.Style.Add("display", "none");
                    rfvStateBox.ControlToValidate = "cmbState";
                }
                else if (_IsJapan)
                {
                    PopulateCountries("JP");
                    cmbState.Visible = true;
                    rfvStateBox.Enabled = IsStateRequired;
                    txtState.Style.Add("display", "none");
                    rfvStateBox.ControlToValidate = "cmbState";
                }
                else
                {
                    txtState.Style.Add("display", "block");
                    cmbState.Visible = false;
                    rfvStateBox.Enabled = IsStateRequired;
                    rfvStateBox.ControlToValidate = "txtState";
                }
            }
            rfvStateBox.ValidationGroup = ValidationGroup;
            
            if (_IsUnitedStates)
            {
                if (cmbState.Items.Count.Equals(0))
                    PopulateCountries("US");
                cmbState.Visible = rfvStateBox.Enabled = true;
                txtState.Style.Add("display", "none");
                rfvStateBox.ControlToValidate = "cmbState";
            }
            else if (_IsJapan)
            {
                if (cmbState.Items.Count.Equals(0))
                    PopulateCountries("JP");
                cmbState.Visible = true;
                rfvStateBox.Enabled = IsStateRequired;
                txtState.Style.Add("display", "none");
                rfvStateBox.ControlToValidate = "cmbState";
            }
            else
            {
                cmbState.Visible = false;
                rfvStateBox.Enabled = IsStateRequired;
                txtState.Style.Add("display", "block");
                rfvStateBox.ControlToValidate = "txtState";
            }
        }

        void PopulateCountries(string countryCode)
        {
            _states = Lib.BLL.BLL_State.GetAllStates(countryCode);
            cmbState.DataSource = _states;
            cmbState.DataTextField = "StateName";
            cmbState.DataValueField = "ISOCode";
            cmbState.DataBind();

            ListItem _select = new ListItem("---" + this.GetGlobalResourceObject("common", "select") + "---", string.Empty);
            cmbState.Items.Insert(0, _select);
        }

        public override string ToString()
        {
            string retString = string.Empty;
            if (_IsUnitedStates || _IsJapan)
            {
                retString = cmbState.SelectedValue;
            }
            else
            {
                retString = txtState.Text;
            }
            return retString;
        }

        public void clearText()
        {
            txtState.Text = string.Empty;
        }

        //Set the state if country is US or JP
        public void setUSState(string stateName)
        {
            cmbState.SelectedIndex = cmbState.Items.IndexOf(cmbState.Items.FindByText(stateName));
        }
        public void setOtherState(string stateName)
        {
            txtState.Text = stateName;
        }

        public void Clear()
        {
            cmbState.Visible = false;
            txtState.Style.Add("display", "block");
            txtState.Text = string.Empty;
        }
       
        public void refreshControl(object sender, EventArgs e, string country)
        {
            if (!string.IsNullOrEmpty(country))
            {
                if (country.Equals("US"))
                {
                    _IsUnitedStates = true;
                    _IsJapan = false;
                }
                else if (country.Equals("JP"))
                {
                    _IsJapan = true;
                    _IsUnitedStates = false;
                }
            }
            this.Page_Load(sender, e);
            PopulateCountries(country);
        }

        public void setState(bool isUnitedStates, bool isJapan, string stateName)
        {
            if (isUnitedStates || isJapan)
            {
                if (string.IsNullOrEmpty(stateName))
                {
                    cmbState.SelectedIndex = 0;
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                }
                else if (!string.IsNullOrEmpty(stateName))
                {
                    if (stateName.Length == 2)
                    {
                        if (isUnitedStates)
                        {
                            PopulateCountries("US");
                            cmbState.SelectedValue = stateName;
                        }
                        else if (isJapan)
                        {
                            PopulateCountries("JP");
                            cmbState.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        if (isUnitedStates)
                        {
                            PopulateCountries("US");
                            cmbState.SelectedIndex = 0;
                        }
                        else if (isJapan)
                        {
                            PopulateCountries("JP");
                            cmbState.SelectedValue = stateName;
                        }
                    }
                    //cmbState.SelectedValue = stateName;
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                    return;
                }
                else
                {
                    for (int i = 0; i < cmbState.Items.Count; i++)
                    {
                        if (cmbState.Items[i].Text.ToLower().Equals(stateName.ToLower()))
                        {
                            cmbState.SelectedIndex = i;
                            txtState.Style.Add("display", "none");
                            cmbState.Visible = true;
                            return;
                        }
                        if (cmbState.Items[i].Value.ToLower().Equals(stateName.ToLower()))
                        {
                            cmbState.SelectedIndex = i;
                            txtState.Style.Add("display", "none");
                            cmbState.Visible = true;
                            return;
                        }
                    }
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                    return;
                }
            }
            else
            {
                txtState.Style.Add("display", "block");
                cmbState.Visible = false;
                txtState.Text = stateName;
            }
        }
    }
}
