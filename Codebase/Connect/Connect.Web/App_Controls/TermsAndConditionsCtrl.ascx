﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TermsAndConditionsCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.TermsAndConditionsCtrl" EnableViewState="false" %>
<div class="settingrow">
<div style=" padding: 10px; height: 300px; overflow: auto;">
    <asp:Localize ID="lcalTerms" runat="server" Text="<%$ Resources:STCTRL_TermsAndConditionsCtrl,lcalTerms.Text %>"></asp:Localize>
</div>
</div>
<div class="settingrow" style="text-align: center;">
    <asp:Button ID="bttAgree" runat="server" Text="<%$ Resources:STCTRL_TermsAndConditionsCtrl,bttAgree.Text %>" Visible="false" onclick="bttAgree_Click" />
    <asp:Button ID="bttCancel" runat="server" 
        Text="<%$ Resources:STCTRL_TermsAndConditionsCtrl,bttCancel.Text %>" 
        Visible="false" CausesValidation="false" onclick="bttCancel_Click" />
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_TermsAndConditionsCtrl" />