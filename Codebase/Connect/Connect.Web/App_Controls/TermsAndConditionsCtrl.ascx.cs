﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls
{
    public partial class TermsAndConditionsCtrl : System.Web.UI.UserControl
    {
        public String ReturnURL
        {
            get
            {
                if (HttpContext.Current.Session!= null && HttpContext.Current.Session[KeyDef.QSKeys.ReturnURL] != null)
                {
                    return HttpContext.Current.Session[KeyDef.QSKeys.ReturnURL].ToString();
                }
                else
                {
                    return "~/home";
                }
                //if (Request.QueryString[KeyDef.QSKeys.ReturnURL].IsNullOrEmptyString())
                //    return "~/home";
                //else return Request.QueryString[KeyDef.QSKeys.ReturnURL].Trim();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Guid accountID = AccountUtility.GetSelectedAccountID(false);
                    if (accountID.IsNullOrEmptyGuid()) return;

                    Lib.Account.Acc_AccountMaster am = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
                    if (am != null && !am.AgreedToTerms)
                    {
                        bttAgree.Visible = bttCancel.Visible = true;
                    }
                }
            }
        }

        protected void bttAgree_Click(object sender, EventArgs e)
        {
            Guid accountID = AccountUtility.GetSelectedAccountID(false);
            Lib.BLL.BLLAcc_AccountMaster.UpdateAgreeTerms(accountID);
            string returnurl = ReturnURL;
            HttpContext.Current.Session[KeyDef.QSKeys.ReturnURL] = null;
            WebUtility.HttpRedirect(null, returnurl);
        }

        protected void bttCancel_Click(object sender, EventArgs e)
        {
            WebUtility.HttpRedirect(null, "~/home");
        }

       
    }
}