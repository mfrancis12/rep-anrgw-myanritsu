﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TermsProductSupportCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Controls.TermsProductSupportCtrl" EnableViewState="false" %>
<div class="settingrow">
<div style=" padding: 10px; height: 300px; overflow: auto;">
    <asp:Localize ID="lcalTerms" runat="server" Text="<%$ Resources:STCTRL_TermsProductSupportCtrl,lcalTerms.Text %>"></asp:Localize>
</div>
</div>
<div class="settingrow" style="text-align: center;">
    <asp:Button ID="bttAgree" runat="server" Text="<%$ Resources:STCTRL_TermsProductSupportCtrl,bttAgree.Text %>" Visible="true" onclick="bttAgree_Click" />
    <asp:Button ID="bttCancel" runat="server" 
        Text="<%$ Resources:STCTRL_TermsProductSupportCtrl,bttCancel.Text %>" 
        Visible="true" CausesValidation="false" onclick="bttCancel_Click" />
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_TermsProductSupportCtrl" EnableViewState="false" />