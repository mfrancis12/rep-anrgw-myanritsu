﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class TermsProductSupportCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String ReturnURL
        {
            get
            {
                if (Request.QueryString[KeyDef.QSKeys.ReturnURL].IsNullOrEmptyString())
                    return KeyDef.UrlList.MyProducts;
                else return Request.QueryString[KeyDef.QSKeys.ReturnURL].Trim();
            }
        }

        public Guid ProdRegWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ProdRegWebAccessKey.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found.");
            }
        }

        protected void bttAgree_Click(object sender, EventArgs e)
        {
            Session[KeyDef.SSKeys.JPProdTermsPerSession] = true;

            List<Guid> agreedTermsWebAccessKeys = Session[KeyDef.SSKeys.JPProdTermsAgreedWebAccessKeys] as List<Guid>;
            if (agreedTermsWebAccessKeys == null) agreedTermsWebAccessKeys = new List<Guid>();
            if (!agreedTermsWebAccessKeys.Contains(ProdRegWebAccessKey))
            {
                agreedTermsWebAccessKeys.Add(ProdRegWebAccessKey);
                Session[KeyDef.SSKeys.JPProdTermsAgreedWebAccessKeys] = agreedTermsWebAccessKeys;
            }
            WebUtility.HttpRedirect(null, ReturnURL);
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { throw new NotImplementedException(); }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        protected void bttCancel_Click(object sender, EventArgs e)
        {
            WebUtility.HttpRedirect(null, KeyDef.UrlList.MyProducts);
        }
    }
}