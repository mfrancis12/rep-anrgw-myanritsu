﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserListCtrl.ascx.cs"
    Inherits="Anritsu.Connect.Web.App_Controls.UserListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:STCTRL_UserListCtrl,pnlContainer.HeaderText %>'>
    <div class="settingrow">
        <center>
            <span class="msg">
                <asp:Literal ID="ltrNotifyMsg" runat="server"></asp:Literal></span></center>
    </div>
    <div class="settingrow">
        <center>
            <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" OnRowCommand="gvList_RowCommand" AllowSorting="true" OnSorting="gvList_Sorting"
                AllowPaging="true" PageSize="10" OnRowDataBound="gvList_RowDataBound" Width="100%"
                OnPageIndexChanging="gvList_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="EmailAddress" SortExpression="EmailAddress" HeaderText='<%$ Resources:STCTRL_UserListCtrl,gvList.HeaderText_EmailAddress %>'
                        ItemStyle-CssClass="gvstyle1-item-lft" />
                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" SortExpression="LastName" HeaderText="<%$Resources:STCTRL_UserListCtrl,gvList.HeaderText_FullName%>">
                        <ItemTemplate>
                            <asp:Literal ID="ltrFullName" runat="server" Text='<%# GetFullName(DataBinder.Eval(Container.DataItem, "FirstName"), DataBinder.Eval(Container.DataItem, "LastName") ) %>'></asp:Literal>
                            <asp:Literal ID="ltrTeamOwner" runat="server" Text="(Team Owner)" Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "TeamOwner")) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--   <asp:BoundField DataField="LastLoginUTC" HeaderText='<%$ Resources:STCTRL_UserListCtrl,gvList.HeaderText_LastLoginUTC %>'
                        DataFormatString="{0:MMM dd, yyyy}" ItemStyle-CssClass="gvstyle1-item-lft" ItemStyle-Width="80px" />--%>
                    <asp:TemplateField HeaderText="<%$ Resources:STCTRL_UserListCtrl,gvList.HeaderText_LastLoginUTC %>"
                        ItemStyle-CssClass="gvstyle1-item-lft" ItemStyle-Width="80px">
                        <ItemTemplate>
                            <asp:Literal ID="ltrLastLoginUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"LastLoginUTC")) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:STCTRL_UserListCtrl,gvList.HeaderText_CanManageUsers %>'
                        ItemStyle-Width="60px">
                        <ItemTemplate>
                            <asp:CheckBox ID="cbxIsManager" runat="server" Text=" " />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="gvstyle1-item-lft" ItemStyle-Width="140px">
                        <ItemTemplate>
                            <asp:Button ID="bttUpdateUser" runat="server" CommandName="UpdateUserCommand" SkinID="small-button-lpa"
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MembershipId") %>'
                                Text='<%$ Resources:STCTRL_UserListCtrl,bttUpdateUserType.Text %>' />&nbsp;&nbsp;
                            <asp:Button ID="bttRemoveUser" runat="server" CommandName="RemoveUserCommand" SkinID="small-button-lpa"
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MembershipId") %>'
                                Text='<%$ Resources:STCTRL_UserListCtrl,bttRemoveUser.Text %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </center>
    </div>
    <asp:Panel ID="pnlNewUser" runat="server">
            <div class="settingrow">
                <fieldset id="panel-new-user">
                    <legend class="gray">
                        <asp:Localize ID="lcalAddNewUser" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalAddNewUser.Text%>"></asp:Localize></legend>
                    <p>
                        <asp:Localize ID="lcalAddUserDesc" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalAddUserDesc.Text%>"></asp:Localize></p>
                    <div class="settingrow">
                        <asp:ValidationSummary ID="vSumNewUser" runat="server" DisplayMode="BulletList" ShowMessageBox="false" />
                    </div>
                    <div class="settingrow group input-text required width-60">
                        <label>
                            <asp:Localize ID="lcalNewUserFirstName" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalNewUserFirstName.Text%>"></asp:Localize>:</label>
                        <p class="error-message">
                            <asp:Localize ID="lclNewFrstname" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,rfvNewUserFN.ErrorMessage%>"></asp:Localize>
                        </p>
                        <p class="server-error-message">
                            <asp:RegularExpressionValidator ID="revNewUserFN" runat="server" ControlToValidate="txtNewUserFN" Display="Dynamic" ValidationExpression='^[^<>%]*$' ValidationGroup="vgNewUser"
                          ErrorMessage="<%$Resources:STCTRL_UserListCtrl,revNewUserFN.ErrorMessage%>"></asp:RegularExpressionValidator>
                        </p>
                        <asp:TextBox ID="txtNewUserFN" runat="server" ValidationGroup="vgNewUser"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="rfvNewUserFN" runat="server" ValidationGroup="vgNewUser" Display="Dynamic"
                            ControlToValidate="txtNewUserFN" ErrorMessage="<%$Resources:STCTRL_UserListCtrl,rfvNewUserFN.ErrorMessage%>">*</asp:RequiredFieldValidator>--%>
                         
                    </div>
                    <div class="settingrow width-60 group input-text required">
                        <label>
                            <asp:Localize ID="lcalNewUserLastName" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalNewUserLastName.Text%>"></asp:Localize>:</label>
                        <p class="error-message">
                            <asp:Localize ID="lclNeUsrErrmsg" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,rfvNewUserLN.ErrorMessage%>"></asp:Localize>
                        </p>
                        <p class="server-error-message">
                            <asp:RegularExpressionValidator ID="revNewUserLN" runat="server" ControlToValidate="txtNewUserLN" Display="Dynamic" ValidationExpression='^[^<>%]*$' ValidationGroup="vgNewUser"
                          ErrorMessage="<%$Resources:STCTRL_UserListCtrl,revNewUserLN.ErrorMessage%>"></asp:RegularExpressionValidator>
                        </p>
                        <asp:TextBox ID="txtNewUserLN" runat="server" ValidationGroup="vgNewUser"></asp:TextBox>
                        
                        <%--<asp:RequiredFieldValidator ID="rfvNewUserLN" runat="server" ValidationGroup="vgNewUser" Display="Dynamic"
                            ControlToValidate="txtNewUserLN" ErrorMessage="<%$Resources:STCTRL_UserListCtrl,rfvNewUserLN.ErrorMessage%>">*</asp:RequiredFieldValidator>--%>
                         <%--<asp:RegularExpressionValidator ID="revNewUserLN" runat="server" ControlToValidate="txtNewUserLN" Display="Dynamic" ValidationExpression='^[^<>%]*$' ValidationGroup="vgNewUser"
                          ErrorMessage="<%$Resources:STCTRL_UserListCtrl,revNewUserLN.ErrorMessage%>"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="settingrow width-60 group input-text required ">
                        <label>
                            <asp:Localize ID="lcalNewUserEmail" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalNewUserEmail.Text%>"></asp:Localize></label>
                        <p class="error-message">
                            <asp:Localize ID="txtNewUsrEmailErrMsg" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,rfvNewUserEmail.ErrorMessage%>"></asp:Localize>
                        </p>
                        <p class="custom-error-message" style="margin: 1em 0">
                            <%--<asp:RegularExpressionValidator ID="revNewUserEmail" runat="server" ControlToValidate="txtNewUserEmail" ValidationGroup="vgNewUser" Display="Dynamic"
            ErrorMessage='<%$Resources:STCTRL_UserListCtrl,revNewUserEmail.ErrorMessage %>' ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                            <asp:Label ID="lclNewUsrEmlErrMsg" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,revNewUserEmail.ErrorMessage %>"></asp:Label>
                        </p>
                        <asp:TextBox ID="txtNewUserEmail" runat="server" ValidationGroup="vgNewUser" CssClass="email"></asp:TextBox>
                        
                       <%-- <asp:RequiredFieldValidator ID="rfvNewUserEmail" runat="server" ValidationGroup="vgNewUser" Display="Dynamic"
                            ControlToValidate="txtNewUserEmail" ErrorMessage="<%$Resources:STCTRL_UserListCtrl,rfvNewUserEmail.ErrorMessage%>">*</asp:RequiredFieldValidator>--%>
                        <%--<asp:RegularExpressionValidator ID="revNewUserEmail" runat="server" ControlToValidate="txtNewUserEmail" ValidationGroup="vgNewUser" Display="Dynamic"
            ErrorMessage='<%$Resources:STCTRL_UserListCtrl,revNewUserEmail.ErrorMessage %>' ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="settingrow">
                      <%--  <span class="settinglabel-10 nopadding">
                            <asp:Localize ID="lcalIsManager" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalIsManager.Text%>"></asp:Localize>
                            &nbsp;&nbsp;</span>--%>
                        <asp:CheckBox ID="cbxIsManager" runat="server" Text=" <%$Resources:STCTRL_UserListCtrl,lcalIsManager.Text%>" />
                        <%--<asp:Localize ID="lcalIsManager" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,lcalIsManager.Text%>"></asp:Localize>--%>
                    </div>
                   <%-- <div class="settingrow" style="height:1px">
                    </div>--%>
                 
                      <div class="settingrow">
                            <asp:CheckBox ID="cbxNewUserAgreeTerms" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,cbxNewUserAgreeTerms.Text%>" />
                    </div>
                    <div class="settingrow">
                        <span class="msg">
                            <asp:Literal ID="ltrNewUserMsg" runat="server"></asp:Literal></span>
                    </div>
                    <div class="settingrow">
                        <asp:Button ID="bttAddNewUser" runat="server" Text="<%$Resources:STCTRL_UserListCtrl,bttAddNewUser.Text%>" SkinID="submit-btn"
                             ValidationGroup="vgNewUser" OnClick="bttAddNewUser_Click" />
                    </div>
                </fieldset>
            </div>
    </asp:Panel>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_UserListCtrl" />
</anrui:GlobalWebBoxedPanel>
