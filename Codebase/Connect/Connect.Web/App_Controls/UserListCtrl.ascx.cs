﻿using System;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.Security;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin;

namespace Anritsu.Connect.Web.App_Controls
{
    public partial class UserListCtrl : UserControl, IStaticLocalizedCtrl
    {

        public Guid AccountId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID],
                      AccountUtility.GetSelectedAccountID(true));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Sec_UserMembership user = LoginUtility.GetCurrentUser(true);
                Boolean canManage = Page.User.IsInRole("accmgr") || user.IsAdministrator;
                pnlNewUser.Visible = canManage;
                BindUsers();
                //  var accountId = AccountUtility.GetSelectedAccountID(true);
                Acc_AccountMaster acc = BLLAcc_AccountMaster.SelectByAccountID(AccountId);

                if (acc == null)
                {
                    this.Visible = false;
                    return;
                }
                pnlContainer.HeaderText = String.Format("{0} -> {1} " + GetStaticResource("UsersString"), acc.CompanyName, acc.AccountName);
                lcalAddNewUser.Text += " " + acc.AccountName;
            }
            ClearMessages();
        }

        private void ClearMessages()
        {
            ltrNewUserMsg.Text = String.Empty;
            ltrNotifyMsg.Text = String.Empty;
        }
        private void BindUsers()
        {
            //Guid accountID = AccountUtility.GetSelectedAccountID(true);

            gvList.DataSource = BLLAcc_AccountMaster.UserMembership_SelectByAccountID_ForUser(AccountId);
            gvList.DataBind();

        }

        protected string GetFullName(Object firstName, Object lastName)
        {
            return lastName + "," + firstName;
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }


        protected void gvList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvList.PageIndex = e.NewPageIndex;

            BindUsers();

        }
        protected void gvList_Sorting(object sender, GridViewSortEventArgs e)
        {
            var dataTable = BLLAcc_AccountMaster.UserMembership_SelectByAccountID_ForUser(AccountId);
            if (dataTable != null)
            {
                //Sort the data.
                dataTable.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression); ;
                gvList.DataSource = dataTable;
                gvList.DataBind();
            }
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_UserListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion

        protected void bttAddNewUser_Click(object sender, EventArgs e)
        {
            ltrNewUserMsg.Text = string.Empty;

            if (!Page.IsValid) return;
            if (!cbxNewUserAgreeTerms.Checked)
            {
                ltrNewUserMsg.Text = GetStaticResource("MSG_MustAgreeTermsToAddUser");
                bttAddNewUser.Focus();
                return;
            }
            //Guid accountID = AccountUtility.GetSelectedAccountID(true);
            Boolean isUserAlreadyadded = false;
            String email = HttpUtility.HtmlEncode(txtNewUserEmail.Text.Trim());

            DataTable dt = BLLAcc_AccountMaster.UserMembership_SelectByAccountID_ForUser(AccountId);
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (dt.Rows[i]["EmailAddress"].ToString().Equals(email))
                {
                    isUserAlreadyadded = true;
                    ltrNewUserMsg.Text = GetStaticResource("MSG_UserAlreadyExists");
                    bttAddNewUser.Focus();
                    break;
                }
            }

            String firstName = HttpUtility.HtmlEncode(txtNewUserFN.Text.Trim());
            String lastName = HttpUtility.HtmlEncode(txtNewUserLN.Text.Trim());
            Sec_UserMembership actingUser = LoginUtility.GetCurrentUserOrSignIn();
            if (!isUserAlreadyadded)
            {
                Sec_UserMembership user = BLLSec_UserMembership.AddNewUserPendingLogin(AccountId
                    , email, firstName, lastName, cbxIsManager.Checked
                    , WebUtility.GetUserIP()
                    , actingUser.Email
                    , actingUser.FullName);
                BindUsers();

                ltrNewUserMsg.Text = GetStaticResource("MSG_NewUserAdded");
                Sec_UserMembership usr = BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
                var addnewuser = new ConnectUserAddNew();
                addnewuser.SendEmails(usr, AccountId);
                bttAddNewUser.Focus();
            }

            txtNewUserEmail.Text = String.Empty;
            txtNewUserFN.Text = String.Empty;
            txtNewUserLN.Text = String.Empty;

        }

        protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
                var bttRemoveUser = e.Row.FindControl("bttRemoveUser") as Button;
                var bttUpdateUser = e.Row.FindControl("bttUpdateUser") as Button;
                var canManage = (Page.User.IsInRole("accmgr") || user.IsAdministrator);
                var membershipId = ConvertUtility.ConvertToGuid(DataBinder.Eval(e.Row.DataItem, "MembershipId"), Guid.Empty);
                var isManager = e.Row.FindControl("cbxIsManager") as CheckBox;
                var isUserManager = ConvertUtility.ConvertToBoolean(DataBinder.Eval(e.Row.DataItem, "IsManager"), false);
                isManager.Checked = isUserManager;
                 bttRemoveUser.Visible = bttUpdateUser.Visible = isManager.Enabled = canManage && (user.MembershipId != membershipId);
                //gvList.Columns[4].Visible= bttRemoveUser.Visible = bttUpdateUser.Visible = isManager.Enabled = canManage && (user.MembershipId != membershipId);
                bttRemoveUser.OnClientClick = "return confirm ('" + GetGlobalResourceObject("common", "ConfirmDeleteRegMsg") + "')";
            }
        }

        protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RemoveUserCommand")
            {
                //var accountID = AccountUtility.GetSelectedAccountID(true);
                var membershipId = ConvertUtility.ConvertToGuid(e.CommandArgument, Guid.Empty);
                //MA-1404
                BLLSec_Role.Delete(membershipId, AccountId);
                //BLLSec_Role.Delete(membershipId, AccountId, "accmgr");
                //BLLSec_Role.Delete(membershipId, AccountId, "accuser");
                BindUsers();
                ltrNotifyMsg.Text = GetStaticResource("MSG_UserRemoved");
            }
            else if (e.CommandName == "UpdateUserCommand")
            {
                //Guid accountID = AccountUtility.GetSelectedAccountID(true);
                Guid membershipId = ConvertUtility.ConvertToGuid(e.CommandArgument, Guid.Empty);
                GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                CheckBox cbxIsManager = row.FindControl("cbxIsManager") as CheckBox;
                String ip = WebUtility.GetUserIP();
                Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();

                if (cbxIsManager.Checked)
                {
                    BLLSec_Role.Insert(membershipId, AccountId, "accmgr", ip, user.Email, user.FullName);
                }
                else
                {
                    BLLSec_Role.Delete(membershipId, AccountId, "accmgr");
                }
                BindUsers();
                ltrNotifyMsg.Text = GetStaticResource("MSG_UserUpdated");
            }
        }
    }
}