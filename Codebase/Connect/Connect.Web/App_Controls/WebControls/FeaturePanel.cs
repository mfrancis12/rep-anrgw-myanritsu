﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls.WebControls
{
    public class FeaturePanel : Panel
    {
        //private bool _NoBorder = false;
        private string _CssOuterWrapperClass = "panelwrapper boxedpanel";
        public string CssOuterWrapperClass { get { return _CssOuterWrapperClass; } set { _CssOuterWrapperClass = value; } }
        //public string HeaderText { get; set; }
        //public bool ShowHeader { get; set; }
        public bool IsSideBarLinkList { get; set; }
        //public bool NoBorder { get { return _NoBorder; } set { _NoBorder = value; } }
        public Lib.UI.Site_ModulePage ModulePageData 
        {
            get
            {
                App_Lib.UI.FeatureCtrlBase ctrlBase = this.Parent as App_Lib.UI.FeatureCtrlBase;
                if (ctrlBase == null) return null;
                return ctrlBase.ModulePageData;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null) return;
            if (ModulePageData == null) return;  
            writer.Write(string.Format("<div id='{0}' class='{1}'>", this.ClientID, this._CssOuterWrapperClass));
            writer.Write(string.Format("<a id='{0}_moduleanchor' class='moduleanchor'></a>", this.ClientID));
            if (ModulePageData.ModuleInfo.ShowHeader)
            {
                String headerText = HttpContext.GetGlobalResourceObject(ModulePageData.ModuleInfo.ResourceClassKey, "ModuleTitle").ToString();
                writer.Write(string.Format("<h2 class='art-PostHeader moduletitle'>{0}</h2>", headerText));
            }
            writer.Write("<div class='modulecontentwrap'>");
            if (!ModulePageData.ModuleInfo.NoBorder) writer.Write(string.Format("<div class=' modulecontent' id='boxcontent_{0}'>", this.ClientID));

            base.RenderContents(writer);

            if (Page.User.Identity.IsAuthenticated)
            {
                App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUser(false);

                if (usr != null && (usr.IsAdministrator || Roles.IsUserInRole("ceditor")) && LoginUtility.IsAdminMode())
                {
                    String editURL = ModulePageData.ModuleInfo.EditContentUrl;
                    if (!String.IsNullOrEmpty(editURL))
                    {
                    
                        writer.Write("<div class=\"editfeature\">");
                        writer.Write(string.Format("<a href='{0}'>edit content</a>", Page.ResolveUrl(ModulePageData.ModuleInfo.EditContentUrl)));
                        writer.Write(string.Format("<a href='{0}'>settings</a>", Page.ResolveUrl(ModulePageData.ModuleInfo.EditSettingsUrl)));
                        writer.Write("</div>");
                    }
                }
            }
            writer.Write("</div>");

            if (!ModulePageData.ModuleInfo.NoBorder) writer.Write("</div>");
            writer.Write("</div>");
        }
    }
}