﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Controls.WebControls
{
    public class GlobalWebBoxedPanel : Panel
    {
        public string CssOuterWrapperClass
        {
            get
            {
                if (ViewState[AddPrefixName("CssOuterWrapperClass")] == null)
                    ViewState[AddPrefixName("CssOuterWrapperClass")] = "panelwrapper boxedpanel";
                return ViewState[AddPrefixName("CssOuterWrapperClass")] as string;
            }
            set { ViewState[AddPrefixName("CssOuterWrapperClass")] = value; }
        }
        public string HeaderText { get { return ViewState[AddPrefixName("HeaderText")] as String; } set { ViewState[AddPrefixName("HeaderText")] = value; } }
        public bool ShowHeader
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("ShowHeader")], true); }
            set { ViewState[AddPrefixName("ShowHeader")] = value; }
        }

        public bool IsSideBarLinkList
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("IsSideBarLinkList")], false); }
            set { ViewState[AddPrefixName("IsSideBarLinkList")] = value; }
        }

        public bool NoBorder
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("_NoBorder")], false); }
            set { ViewState[AddPrefixName("_NoBorder")] = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        private string AddPrefixName(string input)
        {
            return string.Format("{0}{1}", this.ID, input);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null || writer == null) return;

            writer.Write(string.Format("<div id='{0}' class='{1}'>", this.ClientID, this.CssOuterWrapperClass));
            writer.Write(string.Format("<a id='{0}_moduleanchor' class='moduleanchor'></a>", this.ClientID));
            if (ShowHeader) writer.Write(string.Format("<h2 class='art-PostHeader moduletitle'>{0}</h2>", HeaderText));
            writer.Write("<div class='modulecontentwrap'>");
            String clientID = this.ClientID.ToUpper();
            Boolean left = clientID.Contains("LEFT");
            if (!NoBorder)
            {
                if (left)
                    writer.Write(string.Format("<div class='leftmenu' id='boxcontent_{0}'>", this.ClientID));
                else
                    writer.Write(string.Format("<div class='modulecontent' id='boxcontent_{0}'>", this.ClientID));
            }

            RenderContents(writer);

            writer.Write("</div>");
            if (!NoBorder) writer.Write("</div>");
            writer.Write("</div>");
        }

        //protected override void Render(HtmlTextWriter writer)
        //{
        //    if (HttpContext.Current == null)
        //    {
        //        writer.Write("[" + this.ID + "]");
        //        return;
        //    }

        //    writer.Write(string.Format("<div id='{0}' class='{1}'>", this.ClientID, this._CssOuterWrapperClass));
        //    writer.Write(string.Format("<a id='{0}_moduleanchor' class='moduleanchor'></a>", this.ClientID));
        //    if (ShowBoxTitle)
        //    {
        //        if (ShowBoxTitleBg)
        //            writer.Write(string.Format("<h2 class='art-PostHeader moduletitle'>{0}</h2>", BoxTitle));
        //        else
        //            writer.Write(BoxTitle);
        //    }
        //    writer.Write("<div class=''>");
        //    writer.Write(string.Format("<div class=' modulecontent' id='boxcontent_{0}'>", this.ClientID));

        //    if (IsSideBarLinkList) writer.Write("<div class='gwsbrmenu'>");            

        //    //content
        //    base.RenderContents(writer);

        //    if (IsSideBarLinkList) writer.Write("</div>");
        //    writer.Write("</div>");
        //    writer.Write("</div>");
        //    writer.Write("</div>");
        //}
    }
}