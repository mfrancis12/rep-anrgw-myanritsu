﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls.WebControls
{
    public class PageTitlePanel : WebControl, INamingContainer
    {
        public Boolean AppendAnritsu { get; set; }
        //<asp:Localize ID="lcalPageTitle" runat="server" Text="<%$Resources:ThisPage,PageTitle %>"></asp:Localize>
        public PageTitlePanel()
        {
            AppendAnritsu = true;
            if (HttpContext.Current == null) { return; }
            this.EnableViewState = false;
            EnsureChildControls();
        }

        private void Initialize()
        {
            if (HttpContext.Current == null) { return; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HttpContext.Current == null) { return; }
            Initialize();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null)
            {
                writer.Write("[" + this.ID + "]");
                return;
            }
            String pageTitle = HttpContext.GetGlobalResourceObject(SiteUtility.GetPageUrl(), "PageTitle").ToString();
            if (!String.IsNullOrEmpty(pageTitle) && !pageTitle.Equals("PageTitle", StringComparison.InvariantCultureIgnoreCase))
            {
                if (AppendAnritsu)
                {
                    pageTitle += " - " + HttpContext.GetGlobalResourceObject("COMMON", "Anritsu").ToString();
                }
                writer.Write(pageTitle);
            }
            
        }
    }
}