﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls.WebControls
{
    public class SiteAdminEditPagePanel : WebControl, INamingContainer
    {
        public SiteAdminEditPagePanel()
        {
            if (HttpContext.Current == null) { return; }
            EnsureChildControls();
        }

        private void Initialize()
        {
            if (HttpContext.Current == null) { return; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HttpContext.Current == null) { return; }
            Initialize();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null)
            {
                writer.Write("[" + this.ID + "]");
                return;
            }
            if (!Page.User.Identity.IsAuthenticated) return;

            Guid selectedAccID = AccountUtility.GetSelectedAccountID(true);
            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUser(false);
            if (LoginUtility.IsAdminMode())
            {
         
                String url = String.Empty;
                url = this.Page.ResolveClientUrl(App_Lib.KeyDef.UrlList.SiteAdminPages.Home);
                writer.Write(string.Format("<li class='global-menu__item admin-link' > <button type ='button' onclick= window.open('{0}'" + ","+"'_self')>{1}</button></li>",url, HttpContext.GetGlobalResourceObject(KeyDef.UrlList.SiteAdminPages.Home, "PageTitle")));
              


                //if (user != null && user.IsAdministrator)
                //{
                //    writer.Write(string.Format("<li class='util__contact-us'><a class='blueit-hover' href='{0}?{1}={2}'>{3}</a></li>"
                //        , this.Page.ResolveClientUrl(KeyDef.UrlList.SiteAdminPages.PageConfig_PageSettings)
                //        , App_Lib.KeyDef.QSKeys.PageURL
                //        , HttpUtility.UrlEncode(SiteUtility.GetPageUrl())
                //        , HttpContext.GetGlobalResourceObject(App_Lib.KeyDef.UrlList.SiteAdminPages.Home, "EditPage")));
                //}
                //writer.Write("</ul></div>");


            }

            if (user != null && user.IsTempUser)
            {
                string tempmsg = HttpContext.GetGlobalResourceObject("Common", "TempUserMessage.Text").ToString();
                tempmsg = tempmsg.Replace("spsignout", "../spsignout?lang=" + HttpContext.Current.Request.QueryString[App_Lib.KeyDef.QSKeys.SiteLocale]);
                writer.Write(tempmsg);

            }
        }
    }
}