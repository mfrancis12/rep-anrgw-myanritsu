﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Controls.WebControls
{   

    public class StaticResourceEditPanel : WebControl, INamingContainer
    {
        public String StaticResourceClassKey { get; set; }
        public List<KeyValuePair<String, String>> AdditionalAdminLinks { get; set; }

        public StaticResourceEditPanel()
        {
            if (HttpContext.Current == null) { return; }
            this.EnableViewState = false;
            EnsureChildControls();
        }

        private void Initialize()
        {
            if (HttpContext.Current == null) { return; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HttpContext.Current == null) { return; }
            Initialize();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null || !Page.User.Identity.IsAuthenticated)
            {
               // writer.Write("[" + this.ID + "]");
                this.Visible = false;
                return;
            }
            Lib.Security.Sec_UserMembership usr = LoginUtility.GetCurrentUser(false);

            if (!String.IsNullOrEmpty(StaticResourceClassKey) && usr != null && usr.IsAdministrator && LoginUtility.IsAdminMode())
            {
                //String editURL = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.SiteLocale_ManageStaticLocale
                //    , KeyDef.QSKeys.ResClassKey, StaticResourceClassKey);
                String editURL = String.Format("{0}?{1}=true&{2}={3}",
                    KeyDef.UrlList.SiteAdminPages.AdminContentSearch,
                    KeyDef.QSKeys.AdminContentSearch_Auto,
                    KeyDef.QSKeys.AdminContentSearch_PreClassKey, StaticResourceClassKey);

                if (!String.IsNullOrEmpty(editURL))
                {

                    writer.Write("<div class=\"editfeature\">");
                    writer.Write(string.Format("<a href='{0}'>edit resources</a>", Page.ResolveUrl(editURL)));
                    if (AdditionalAdminLinks != null)
                    {
                        foreach (KeyValuePair<String, String> al in AdditionalAdminLinks)
                        {
                            if (!string.IsNullOrEmpty(al.Key) && !String.IsNullOrEmpty(al.Value))
                            {
                                writer.Write(string.Format("<a href='{0}'>{1}</a>"
                                    , Page.ResolveUrl(al.Value)
                                    , al.Key));
                            }
                        }
                    }
                    writer.Write("</div>");
                }
            }
        }

        
    }
}