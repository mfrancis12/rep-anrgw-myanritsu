﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicFormCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.DynamicFormCtrl" %>
<anrui:FeaturePanel ID="featureContainer" runat="server">
<div class="settingrow"><asp:PlaceHolder ID="phFrm" runat="server"></asp:PlaceHolder></div>
<div class="settingrow">
<p class='failureNotification'><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></p>
<asp:Panel ID="pnlFrmSubmit" runat="server" HorizontalAlign="Right">
    <asp:Button ID="bttFrmSubmit" runat="server" Text="Submit" onclick="bttFrmSubmit_Click"/>
</asp:Panel>
</div>
</anrui:FeaturePanel>