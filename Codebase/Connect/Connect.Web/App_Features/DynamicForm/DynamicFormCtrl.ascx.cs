﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm
{
    public partial class DynamicFormCtrl : UILib.DynamicFormBase
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (ModulePageData == null) return;
            InitFormControls(phFrm);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadContent();
        }

        protected void LoadContent()
        {
            if (ModulePageData == null) return;
            String classKey = ModulePageData.ModuleInfo.ResourceClassKey;
        }

        protected void bttFrmSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            //ltrMsg.Text = "";
            //Guid submitKey = SubmitForm(phFrm, ltrMsg);
            //if (!submitKey.IsNullOrEmptyGuid())
            //{
            //    String url = String.Format("{0}?{1}={2}", FormData.AfterSubmitURL, App_Lib.KeyDef.QSKeys.FormSubmitKey, submitKey.ToString().ToUpperInvariant());
            //    WebUtility.HttpRedirect(null, url);
            //}
        }
    }
}