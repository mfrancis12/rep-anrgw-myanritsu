﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicFormStaticCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.DynamicFormStaticCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText='<%$ Resources:STCTRL_DynamicFormStaticCtrl,pnlContainer.HeaderText %>'>
<div class="settingrow"><asp:PlaceHolder ID="phFrm" runat="server"></asp:PlaceHolder></div>
<div class="settingrow">
<p class='failureNotification'><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></p>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_DynamicFormStaticCtrl" />
</anrui:GlobalWebBoxedPanel>