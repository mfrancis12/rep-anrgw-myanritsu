﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm
{
    public partial class DynamicFormStaticCtrl : UILib.DynamicFormStaticBase, App_Lib.UI.IStaticLocalizedCtrl
    {
        public String FormClassKey
        {
            get
            {
                return String.Format("FRM_{0}", FormID.ToString().ToUpperInvariant());
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (FormID.IsNullOrEmptyGuid()) return;    
            InitFormControls(phFrm);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadContent();
        }

        protected void LoadContent()
        {
            Lib.DynamicForm.Frm_Form frm = Lib.BLL.BLLFrm_Form.SelectByFormID(FormID);
            if (frm == null)
            {
                this.Visible = false;
                return;
            }
            pnlContainer.HeaderText = this.GetGlobalResourceObject(FormClassKey, "HeaderText").ToString();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_DynamicFormStaticCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
            if (String.IsNullOrEmpty(str)) str = resourceKey;
            return str;
        }

        #endregion

        public List<KeyValuePair<String, String>> GetFormValues()
        {
            return GetFormValues(phFrm);
        }

        
    }
}