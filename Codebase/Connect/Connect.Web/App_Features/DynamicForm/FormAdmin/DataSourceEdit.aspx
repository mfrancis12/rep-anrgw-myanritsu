﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="DataSourceEdit.aspx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.DataSourceEdit" %>

<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register Src="WebControls/DataSourceListCtrl.ascx" TagName="DataSourceListCtrl" TagPrefix="uc1" %>
<%@ Register Src="WebControls/DataSourceEditCtrl.ascx" TagName="DataSourceEditCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:PlaceHolder runat="server">
        <link href="<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/css/south-street/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet" />
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-datasources">
                <asp:Localize ID="lcalTabDS" runat="server" Text="<%$Resources:ADM_STCTRL_DataSourceAdmin_DataSourceListCtrl,lcalTabDS.Text%>"></asp:Localize></a></li>
            <li><a href="#tabs-dsedit">
                <asp:Localize ID="lcalTabDSEdit" runat="server" Text="<%$Resources:ADM_STCTRL_DataSourceAdmin_DataSourceEditCtrl,lcalTabDSEdit.Text%>"></asp:Localize></a></li>
        </ul>
        <div id="tabs-datasources">
            <uc1:DataSourceListCtrl ID="DataSourceListCtrl1" runat="server" />
        </div>
        <div id="tabs-dsedit">
            <uc2:DataSourceEditCtrl ID="DataSourceEditCtrl1" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    <script type="text/javascript" defer="defer">
        (function () {
            function getScript(url, success) {
                var script = document.createElement('script');
                script.src = url;
                var head = document.getElementsByTagName('head')[0],
               done = false;
                script.onload = script.onreadystatechange = function () {
                    if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                        done = true;
                        success();
                        script.onload = script.onreadystatechange = null;
                        head.removeChild(script);
                    }
                };
                head.appendChild(script);
            }
            getScript('<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/js/jquery-custom-v1.js', function () {
                $(function () { $("#tabs").tabs({ cookie: { expires: 30 } }); });
                function SelectTab(tabName) { $("#tabs").tabs('select', tabName); }
            });
        })();
    </script>

</asp:Content>
