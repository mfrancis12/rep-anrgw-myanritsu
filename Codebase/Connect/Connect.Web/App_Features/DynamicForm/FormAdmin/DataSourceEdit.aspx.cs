﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin
{
    public partial class DataSourceEdit : App_Lib.UI.BP_MyAnritsu
    {
        protected override void OnInit(EventArgs e)
        {

            DataSourceEditCtrl1.DataSourceID = ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.DataSourceID], 0);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}