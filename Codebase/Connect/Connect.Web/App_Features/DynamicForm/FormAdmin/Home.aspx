﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.Home" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register src="WebControls/FormInfoCtrl.ascx" tagname="FormInfoCtrl" tagprefix="uc1" %>
<%@ Register src="WebControls/FormFieldsetListCtrl.ascx" tagname="FormFieldsetListCtrl" tagprefix="uc2" %>
<%@ Register src="WebControls/FormFieldsetEditCtrl.ascx" tagname="FormFieldsetEditCtrl" tagprefix="uc3" %>
<%@ Register src="WebControls/FormControlEditCtrl.ascx" tagname="FormControlEditCtrl" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<asp:PlaceHolder runat="server">
<link href="<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/css/south-street/jquery-ui-1.8.16.custom.css"  type="text/css" rel="stylesheet" />
        </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div id="tabs">
	<ul>
		<li><a href="#tabs-frmInfo"><asp:Localize ID="lcalTabFormInfo" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormInfoCtrl,lcalTabFormInfo.Text%>"></asp:Localize></a></li>
        <li><a href="#tabs-fslist"><asp:Localize ID="lcalTabFieldsetList" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetListCtrl,lcalTabFieldsetList.Text%>"></asp:Localize></a></li>
        <li><a href="#tabs-fsedit"><asp:Localize ID="lcalTabFieldsetEdit" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,lcalTabFieldsetEdit.Text%>"></asp:Localize></a></li>
        <li><a href="#tabs-fsctrledit"><asp:Localize ID="lcalTabFieldsetControlEdit" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalTabFieldsetControlEdit.Text%>"></asp:Localize></a></li>
	</ul>
	<div id="tabs-frmInfo">
	    <uc1:FormInfoCtrl ID="FormInfoCtrl1" runat="server" />
	</div>
    <div id="tabs-fslist">
	    <uc2:FormFieldsetListCtrl ID="FormFieldsetListCtrl1" runat="server" />
	</div>
     <div id="tabs-fsedit">
	    <uc3:FormFieldsetEditCtrl ID="FormFieldsetEditCtrl1" runat="server" />
	</div>
    <div id="tabs-fsctrledit">
	    <uc4:FormControlEditCtrl ID="FormControlEditCtrl1" runat="server" />
	</div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
 <script type="text/javascript" defer="defer">
     (function () {
         function getScript(url, success) {
             var script = document.createElement('script');
             script.src = url;
             var head = document.getElementsByTagName('head')[0],
            done = false;
             script.onload = script.onreadystatechange = function () {
                 if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                     done = true;
                     success();
                     script.onload = script.onreadystatechange = null;
                     head.removeChild(script);
                 }
             };
             head.appendChild(script);
         }
         getScript('<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/js/jquery-custom-v1.js', function () {
             $(function () { $("#tabs").tabs({ cookie: { expires: 30} }); });
            /* $(function () {
                 if (getQueryString("frmid") && getQueryString("fsid") && getQueryString("fwscid")) {
                 }
                 else if (getQueryString("frmid") && getQueryString("fsid")) {
                     $("#tabs").tabs({ disabled: [3] });
                 }
                 else if (getQueryString("frmid")) {
                     $("#tabs").tabs({ disabled: [2, 3] });
                 }
                 else {
                     $("#tabs").tabs({ disabled: [1, 2, 3] });
                 }
             });*/
         });
     })();
	</script>
</asp:Content>
