﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin
{
    public partial class Home : App_Lib.UI.BP_MyAnritsu
    {
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ModuleID], 0);
            }
        }

        public Guid FormID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.FormID], Guid.Empty);
            }
        }

        public Int32 FieldsetID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.FieldsetID], 0);
            }
        }

        public Int32 FwscID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.FieldsetControlID], 0);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Lib.DynamicForm.Frm_Form frm = null;
            if (ModuleID > 0)
            {
                Lib.UI.Site_Module module = Lib.BLL.BLLSite_Module.SelectByModuleID(ModuleID);//validate module                
                if (module != null)
                {
                    frm = Lib.BLL.BLLFrm_Form.SelectByModuleID(ModuleID, true);
                }
                if (frm == null) throw new HttpException(404, "Page not found.");
                WebUtility.HttpRedirect(null, String.Format("home?{0}={1}", KeyDef.QSKeys.FormID, frm.FormID));
                Response.End();
            }
            else
            {
                frm = Lib.BLL.BLLFrm_Form.SelectByFormID(FormID);
            }

            if (frm != null)
            {
                FormInfoCtrl1.FormID = frm.FormID;

                FormFieldsetListCtrl1.FormID = frm.FormID;

                FormFieldsetEditCtrl1.FormID = frm.FormID;
                FormFieldsetEditCtrl1.FieldsetID = FieldsetID;

                FormControlEditCtrl1.FormID = frm.FormID;
                FormControlEditCtrl1.FieldsetID = FieldsetID;
                FormControlEditCtrl1.FwscID = FwscID;
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}