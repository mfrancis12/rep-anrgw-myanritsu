﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataSourceEditCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.DataSourceEditCtrl" %>
<%@ Register src="../../../../App_Controls/ResourceLocalizationItemCtrl.ascx" tagname="ResourceLocalizationItemCtrl" tagprefix="uc1" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_DataSourceAdmin_DataSourceEditCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
    <center>
     <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" 
            ShowFooter="true" onitemcommand="dgList_ItemCommand" 
            onitemdatabound="dgList_ItemDataBound">
            <Columns>
             <asp:TemplateColumn HeaderText="DataItem Information" ItemStyle-CssClass="dgstyle1-item-lft" FooterStyle-CssClass="dgstyle1-item-lft">
                    <ItemTemplate>
                        <div class="settingrow">
                            <span class="settinglabelplain">Item Text:</span>
                            <uc1:ResourceLocalizationItemCtrl ID="rlDSItemText" runat="server" />
                        </div>
                        <div class="settingrow">
                            <span class="settinglabelplain">Item Value:</span><br />
                            <uc1:ResourceLocalizationItemCtrl ID="rlDSItemValue" runat="server"/>
                        </div>   
                        <div class="settingrow">
                            <fieldset>
                                <legend>Other Info</legend>
                                 <div class="settingrow">
                                 <span class="settinglabel-10">OrderNum:</span>
                            <asp:TextBox ID="txtOrderNum" runat="server" SkinID="tbx-50" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNum") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvOrderNum" runat="server" ControlToValidate="txtOrderNum" ValidationGroup="vgManageDSData" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>    
                                 </div>
                                 <div class="settingrow">
                                 <span class="settinglabel-10">Is Selected?</span>
                                 <asp:CheckBox ID="cbxIsSelected" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "IsSelected") %>'></asp:CheckBox>
                                 </div>
                                 <div class="settingrow">
                                 <span class="settinglabel-10">&nbsp;</span>
                                 <asp:Button ID="dgList_bttUpdate" runat="server" CommandName="UpdateCommand" SkinID="SmallButton" Text="update"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DSDataID") %>' />&nbsp;&nbsp;
                                 <asp:Button ID="dgList_bttDelete" runat="server" CommandName="DeleteCommand" SkinID="SmallButton" Text="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DSDataID") %>' />
                                 </div>
                            </fieldset>
                        </div>    
                        <hr />                                       
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </center>
    <fieldset>
        <legend>Add New</legend>
        <div class="settingrow">
            <span class="settinglabel-10">Item Text (English):</span>
            <asp:TextBox ID="txtItemTextInEng" runat="server" SkinID="tbx-400" Text=""></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvItemText" runat="server" ControlToValidate="txtItemTextInEng" ValidationGroup="vgManageDSDataNew" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>    
        </div> 
        <div class="settingrow">
            <span class="settinglabel-10">Item Value (English):</span>
            <asp:TextBox ID="txtItemValueInEng" runat="server" SkinID="tbx-400" Text=""></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvItemValue" runat="server" ControlToValidate="txtItemValueInEng" ValidationGroup="vgManageDSDataNew" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>    
        </div> 
         <div class="settingrow">
            <span class="settinglabel-10">OrderNum:</span>
            <asp:TextBox ID="txtOrderNum" runat="server" SkinID="tbx-50" Text="1"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvOrderNum" runat="server" ControlToValidate="txtOrderNum" ValidationGroup="vgManageDSDataNew" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>    
        </div> 
        <div class="settingrow">
            <span class="settinglabel-10">Is Selected?</span>
            <asp:CheckBox ID="cbxIsSelected" runat="server"></asp:CheckBox>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10">&nbsp;</span>
            <asp:Button ID="dgList_bttAdd" runat="server" SkinID="SmallButton" Text="Add" 
                ValidationGroup="vgManageDSDataNew" onclick="dgList_bttAdd_Click" />
        </div>     
    </fieldset>
</div>
 <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DataSourceAdmin_DataSourceEditCtrl" />
</anrui:GlobalWebBoxedPanel>
