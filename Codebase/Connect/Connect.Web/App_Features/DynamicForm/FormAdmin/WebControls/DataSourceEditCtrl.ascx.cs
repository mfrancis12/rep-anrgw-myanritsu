﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class DataSourceEditCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 DataSourceID { get; set; }

        private Lib.DynamicForm.Frm_FormControlDataSource _DS;
        public Lib.DynamicForm.Frm_FormControlDataSource DataSourceInfo
        {
            get
            {
                if (_DS == null)
                {
                    _DS = Lib.BLL.BLLFrm_FormControlDataSource.SelectByDataSourceID(DataSourceID);
                }
                return _DS;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (DataSourceID < 1)
                {
                    this.Visible = false;
                    return;
                }

                InitFields();
            }
        }

        private void InitFields()
        {
            if (DataSourceID < 1) return;
            if (DataSourceInfo == null)
            {
                this.Visible = false;
                return;
            }

            pnlContainer.HeaderText = DataSourceInfo.DataSourceName;

            BindDataSourceData();

        }

        private void BindDataSourceData()
        {
            dgList.DataSource = Lib.BLL.BLLFrm_FormControlDataSourceData.SelectByDataSourceID(DataSourceID);
            dgList.DataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_DataSourceAdmin_DataSourceEditCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "DeleteCommand")
            {
                Int32 dsDataID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.DynamicForm.Frm_FormControlDataSourceData d = Lib.BLL.BLLFrm_FormControlDataSourceData.SelectByDSDataID(dsDataID);
                Lib.BLL.BLLFrm_FormControlDataSourceData.DeleteByDSDataID(dsDataID, d.DataSourceID);
                BindDataSourceData();
            }
            if (e.CommandName == "UpdateCommand")
            {
                Int32 dsDataID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                //Lib.BLL.BLLFrm_FormControlDataSourceData.DeleteByDSDataID(dsDataID);
                //BindDataSourceData();
            }
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 dsDataID = ConvertUtility.ConvertToInt32(DataBinder.Eval(e.Item.DataItem, "DSDataID").ToString(), 0);
                App_Controls.ResourceLocalizationItemCtrl rlDSItemText = e.Item.FindControl("rlDSItemText") as App_Controls.ResourceLocalizationItemCtrl;
                rlDSItemText.ClassKey = Lib.BLL.BLLFrm_FormControlDataSource.ResClassKey(DataSourceInfo.DataSourceID);
                rlDSItemText.ResKey = Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemText(dsDataID);

                App_Controls.ResourceLocalizationItemCtrl rlDSItemValue = e.Item.FindControl("rlDSItemValue") as App_Controls.ResourceLocalizationItemCtrl;
                rlDSItemValue.ClassKey = Lib.BLL.BLLFrm_FormControlDataSource.ResClassKey(DataSourceInfo.DataSourceID);
                rlDSItemValue.ResKey = Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemValue(dsDataID);
             
            }
            
        }

        protected void dgList_bttAdd_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            
            Int32 orderNum = ConvertUtility.ConvertToInt32( txtOrderNum.Text.Trim(), 0);
            Int32 dsDataID = Lib.BLL.BLLFrm_FormControlDataSourceData.Insert(DataSourceID, cbxIsSelected.Checked, orderNum);
            if (dsDataID > 0)
            {
                String classKey = Lib.BLL.BLLFrm_FormControlDataSource.ResClassKey(DataSourceInfo.DataSourceID);
                Lib.Content.Res_ContentStoreBLL.Insert(classKey,
                    Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemText(dsDataID), 
                    "en", 
                    txtItemTextInEng.Text
                    , true);
                Lib.Content.Res_ContentStoreBLL.Insert(classKey,
                    Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemValue(dsDataID),
                    "en",
                    txtItemValueInEng.Text
                    , true);
            }
            String url = String.Format("datasourceedit?{0}={1}", KeyDef.QSKeys.DataSourceID, DataSourceID);
            WebUtility.HttpRedirect(null, url);
        }
    }
}