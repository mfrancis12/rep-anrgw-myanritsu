﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataSourceListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.DataSourceListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_DataSourceAdmin_DataSourceListCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
    <center>
    <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" 
            ShowFooter="true" onitemcommand="dgList_ItemCommand">
            <Columns>
                <asp:TemplateColumn HeaderText="DataSourceName">
                    <ItemTemplate>
                        <asp:TextBox ID="txtDataSourceName" runat="server" SkinID="tbx-300" Text='<%# DataBinder.Eval(Container.DataItem, "DataSourceName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDataSourceName" runat="server" ControlToValidate="txtDataSourceName" ValidationGroup="vgManageDS" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtDataSourceName" runat="server" SkinID="tbx-300"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDataSourceName" runat="server" ControlToValidate="txtDataSourceName" ValidationGroup="vgManageDSNew" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:Button ID="dgList_bttUpdate" runat="server" CommandName="UpdateCommand" SkinID="SmallButton" Text="update"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DataSourceID") %>' />
                        <asp:Button ID="dgList_bttDetails" runat="server" CommandName="DetailsCommand" SkinID="SmallButton" Text="details" OnClientClick="$('#tabs').tabs('select', '#tabs-dsedit');"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DataSourceID") %>' />
                        <asp:Button ID="dgList_bttDelete" runat="server" CommandName="DeleteCommand" SkinID="SmallButton" Text="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "DataSourceID") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="dgList_bttAdd" runat="server" CommandName="AddCommand" SkinID="SmallButton" Text="Add" ValidationGroup="vgManageDSNew" />
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </center>
</div>
 <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_DataSourceAdmin_DataSourceListCtrl" />
</anrui:GlobalWebBoxedPanel>