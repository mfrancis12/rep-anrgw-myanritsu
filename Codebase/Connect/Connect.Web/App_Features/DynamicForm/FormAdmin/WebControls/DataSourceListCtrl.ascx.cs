﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class DataSourceListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindList();
            }
        }

        private void BindList()
        {
            dgList.DataSource = Lib.BLL.BLLFrm_FormControlDataSource.SelectAll();
            dgList.DataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_DataSourceAdmin_DataSourceListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                if (!Page.IsValid) return;
                TextBox txtDataSourceName = e.Item.FindControl("txtDataSourceName") as TextBox;
                Int32 ds = Lib.BLL.BLLFrm_FormControlDataSource.Insert(txtDataSourceName.Text.Trim());
                if (ds > 0) BindList();

            }
            else if (e.CommandName == "UpdateCommand")
            {
                if (!Page.IsValid) return;
                Int32 dataSourceID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                TextBox txtDataSourceName = e.Item.FindControl("txtDataSourceName") as TextBox;
                Lib.BLL.BLLFrm_FormControlDataSource.Update(dataSourceID, txtDataSourceName.Text.Trim());
                BindList();
            }
            else if (e.CommandName == "DeleteCommand")
            {
                if (!Page.IsValid) return;
                Int32 dataSourceID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLFrm_FormControlDataSource.DeleteByDataSourceID(dataSourceID);
                BindList();
            }
            else if (e.CommandName == "DetailsCommand")
            {
                Int32 dataSourceID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                String url = String.Format("datasourceedit?{0}={1}", KeyDef.QSKeys.DataSourceID, dataSourceID);
                WebUtility.HttpRedirect(null, url);
            }
        }
    }
}