﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormControlEditCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormControlEditCtrl" %>
<%@ Register src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" tagname="ResourceLocalizationItemCtrl" tagprefix="uc1" %>
<%@ Register src="FormControlPropertyEditCtrl.ascx" tagname="FormControlPropertyEditCtrl" tagprefix="uc2" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_FwscID" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_FwscID.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCtrlEdt_FwscID" runat="server" SkinID="tbx-20" Enabled="false" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_FieldName" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_FieldName.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCtrlEdt_FieldName" runat="server" SkinID="tbx-200"></asp:TextBox>
</div>
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_CtrlOrder" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_CtrlOrder.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCtrlEdt_CtrlOrder" runat="server" SkinID="tbx-50" Text="1"></asp:TextBox>
</div>
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_IsReqField" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_IsReqField.Text%>"></asp:Localize></span>
        <asp:CheckBox ID="cbxCtrlEdt_IsRequired" runat="server" Text=" " />
</div>

<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_CtrlWidth" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_CtrlWidth.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCtrlEdt_CtrlWidth" runat="server" SkinID="tbx-100" Text="200px"></asp:TextBox>&nbsp;<span class="tiptext">example: 200px</span>
</div>
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_CtrlHeight" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_CtrlHeight.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtCtrlEdt_CtrlHeight" runat="server" SkinID="tbx-100" Text=""></asp:TextBox>
</div>
<asp:Panel ID="pnlDS" runat="server" Visible="false">
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_DS" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_DS.Text%>"></asp:Localize></span>
        <asp:DropDownList ID="cmbCtrlEdt_DS" runat="server" DataTextField="DataSourceName" DataValueField="DataSourceID"></asp:DropDownList>
        <asp:HyperLink ID="hlManageDS" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,hlManageDS.Text%>" NavigateUrl="~/App_Features/DynamicForm/FormAdmin/DataSourceEdit" Target="_blank"></asp:HyperLink>
</div>
</asp:Panel>
<div class="settingrow">
    <span class="settinglabel-10">&nbsp;</span>
    <asp:Button ID="bttCtrlEdt_Save" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,bttCtrlEdt_Save.Text%>" onclick="bttCtrlEdt_Save_Click" />
</div>
<br />
<div class="settingrow">
    <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_CtrlDisplayText" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_CtrlDisplayText.Text%>"></asp:Localize></span>
    <uc1:ResourceLocalizationItemCtrl ID="ResLcItemCtrlEdt_DisplayText" runat="server" />
</div>
<div class="settingrow">
    <span class="settinglabel-10"><asp:Localize ID="lcalCtrlEdt_CtrlReportText" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormControlEditCtrl,lcalCtrlEdt_CtrlReportText.Text%>"></asp:Localize></span>
    <uc1:ResourceLocalizationItemCtrl ID="ResLcItemCtrlEdt_ReportText" runat="server" />
</div>

<div class="settingrow">
    <uc2:FormControlPropertyEditCtrl ID="FormControlPropertyEditCtrl1" runat="server" />
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FormControlEditCtrl" />
</anrui:GlobalWebBoxedPanel>