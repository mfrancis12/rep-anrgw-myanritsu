﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormControlEditCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid FormID { get; set; }
        public Int32 FieldsetID { get; set; }
        public String ClassKey { get { return Lib.BLL.BLLFrm_Form.ResClassKey(FormID); } }
        public Int32 FwscID { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //ResLcItemFSEdt_Legend.ClassKey = ClassKey;
            //ResLcItemFSEdt_Legend.ResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(FieldsetID) + "Legend";

            cmbCtrlEdt_DS.DataSource = Lib.BLL.BLLFrm_FormControlDataSource.SelectAll();
            cmbCtrlEdt_DS.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FormID.IsNullOrEmptyGuid() || FieldsetID < 1 || FwscID < 1)
                {
                    this.Visible = false;
                    return;
                }

                ResLcItemCtrlEdt_DisplayText.ClassKey = ClassKey;
                ResLcItemCtrlEdt_DisplayText.ResKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(FieldsetID, FwscID, UILib.FormControlPropertyKeys.DisplayText);

                ResLcItemCtrlEdt_ReportText.ClassKey = ClassKey;
                ResLcItemCtrlEdt_ReportText.ResKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(FieldsetID, FwscID, UILib.FormControlPropertyKeys.ReportText);

                FormControlPropertyEditCtrl1.FwscID = FwscID;
                //FormControlListCtrl1.ModuleID = ModuleID;
                InitFieldsetControl();
            }
        }

        private void InitFieldsetControl()
        {
            Lib.DynamicForm.Frm_FormFieldsetControl fsctrl = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FwscID);
            if (fsctrl == null)
            {
                this.Visible = false;
                return;
            }

            pnlContainer.HeaderText = String.Format("ControlID: {0}", FwscID);
            txtCtrlEdt_FwscID.Text = fsctrl.FwscID.ToString();
            txtCtrlEdt_CtrlOrder.Text = fsctrl.ControlOrder.ToString();
            txtCtrlEdt_CtrlWidth.Text = "";
            txtCtrlEdt_FieldName.Text = fsctrl.FieldName;
            cbxCtrlEdt_IsRequired.Checked = fsctrl.IsRequired;
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ptyWidth = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(fsctrl.Properties, UILib.FormControlPropertyKeys.CtrlWidth);
            if (ptyWidth != null) txtCtrlEdt_CtrlWidth.Text = ptyWidth.PropertyValue;

            txtCtrlEdt_CtrlHeight.Text = "";
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ptyHeight = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(fsctrl.Properties, UILib.FormControlPropertyKeys.CtrlHeight);
            if (ptyHeight != null) txtCtrlEdt_CtrlHeight.Text = ptyHeight.PropertyValue;

            pnlDS.Visible = fsctrl.ControlInfo.IsDataSourceRequired;
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_FormAdmin_FormControlEditCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        protected void bttCtrlEdt_Save_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            SaveCtrl();

        }

        private void SaveCtrl()
        {
            Lib.DynamicForm.Frm_FormFieldsetControl fsctrl = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FwscID);
            if (fsctrl == null)
            {
                this.Visible = false;
                return;
            }

            Int32 ctrlOrder = ConvertUtility.ConvertToInt32(txtCtrlEdt_CtrlOrder.Text.Trim(), 1);
            Int32 dataSourceID = 0;
            if (fsctrl.ControlInfo.IsDataSourceRequired) dataSourceID = ConvertUtility.ConvertToInt32(cmbCtrlEdt_DS.SelectedValue, 0);
            Lib.BLL.BLLFrm_FormFieldsetControl.Update(fsctrl.FwscID, ctrlOrder, cbxCtrlEdt_IsRequired.Checked, dataSourceID, txtCtrlEdt_FieldName.Text.Trim());
            Lib.BLL.BLLFrm_FormFieldsetControlProperty.InsertUpdate(fsctrl.FwscID, UILib.FormControlPropertyKeys.CtrlWidth, txtCtrlEdt_CtrlWidth.Text);
            Lib.BLL.BLLFrm_FormFieldsetControlProperty.InsertUpdate(fsctrl.FwscID, UILib.FormControlPropertyKeys.CtrlHeight, txtCtrlEdt_CtrlHeight.Text);
        }
    }
}