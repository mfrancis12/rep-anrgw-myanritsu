﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormControlListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormControlListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_FormAdmin_FormControlListCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
    <center>
    <asp:DataGrid ID="dgControls" runat="server" AutoGenerateColumns="false" 
            ShowFooter="true" onitemdatabound="dgControls_ItemDataBound" 
            onitemcommand="dgControls_ItemCommand">
            <Columns>
                <asp:TemplateColumn HeaderText="Form Control" ItemStyle-CssClass="dgstyle1-item-lft">
                    <ItemTemplate>
                        <asp:Literal ID="dgControlsNew_ltrControl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ControlInfo.ControlName") %>'></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="dgControlsNew_cmbFormControls" runat="server" DataTextField="ControlName" DataValueField="ControlID"></asp:DropDownList>
                    </FooterTemplate>
                </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Display Text (eng)" ItemStyle-CssClass="dgstyle1-item-lft" FooterStyle-CssClass="dgstyle1-item-lft">
                    <ItemTemplate>
                        <div class="settingrow">
                            <span class="settinglabel-10">Field Name:</span>
                            <asp:Literal ID="dgControls_ltrFieldName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FieldName") %>'></asp:Literal>
                        </div>                  
                        <div class="settingrow">      
                        <span class="settinglabel-10">Display Text:</span>
                        <asp:Literal ID="dgControls_ltrDisplayTextInEng" runat="server"></asp:Literal>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="settingrow">      
                            <span class="settinglabel-10">Display Text:</span>
                            <asp:TextBox ID="dgControls_txtDisplayTextInEng" runat="server" SkinID="tbx-300" Text="In english"></asp:TextBox><br />
                        </div>
                        <div class="settingrow">
                            <span class="settinglabel-10">Field Name:</span>
                            <asp:TextBox ID="dgControls_txtFieldName" runat="server" SkinID="tbx-300"></asp:TextBox><br />
                        </div>
                        <div class="settingrow">
                            <asp:CheckBox ID="dgControlsNew_cbxIsReq" runat="server" Text="Is required field?" Checked="true" />
                        </div>
                    </FooterTemplate>
                </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Order">
                    <ItemTemplate>
                        <asp:TextBox ID="dgControls_txtOrder" runat="server" SkinID="tbx-20" Text='<%# DataBinder.Eval(Container.DataItem, "ControlOrder") %>'></asp:TextBox>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dgControlsNew_txtOrder" runat="server" SkinID="tbx-20" Text="1"></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:Button ID="dgControls_bttDetails" runat="server" CommandName="DetailsCommand" SkinID="SmallButton" Text="details" OnClientClick="$('#tabs').tabs('select', '#tabs-fsctrledit');"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FwscID") %>' />
                        <asp:Button ID="dgControls_bttDelete" runat="server" CommandName="DeleteCommand" SkinID="SmallButton" Text="delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FwscID") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="dgControlsNew_bttAdd" runat="server" CommandName="AddCommand" SkinID="SmallButton" Text="Add" />
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </center>
</div>
 <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_FormAdmin_FormControlListCtrl" />
</anrui:GlobalWebBoxedPanel>