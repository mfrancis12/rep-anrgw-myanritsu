﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;


namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormControlListCtrl : System.Web.UI.UserControl
    {
        public Int32 FieldsetID { get { return ConvertUtility.ConvertToInt32(ViewState["FrmCtrlLst_FieldsetID"], 0); } set { ViewState["FrmCtrlLst_FieldsetID"] = value; } }
        public Guid FormID { get { return ConvertUtility.ConvertToGuid(ViewState["FrmCtrlLst_FormID"], Guid.Empty); } set { ViewState["FrmCtrlLst_FormID"] = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFieldsetControls();
            }
        }

        private void BindFieldsetControls()
        {
            List<Lib.DynamicForm.Frm_FormFieldsetControl> controls = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFieldsetID(FieldsetID);
            dgControls.DataSource = controls;
            dgControls.DataBind();
        }

        protected void dgControls_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                DropDownList dgControlsNew_cmbFormControls = (DropDownList)e.Item.FindControl("dgControlsNew_cmbFormControls");
                dgControlsNew_cmbFormControls.DataSource = Lib.BLL.BLLFrm_FormControl.FormControls;
                dgControlsNew_cmbFormControls.DataBind();
            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal dgControls_ltrDisplayTextInEng = (Literal)e.Item.FindControl("dgControls_ltrDisplayTextInEng");
                String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(FormID);
                Int32 fwscID = ConvertUtility.ConvertToInt32(DataBinder.Eval(e.Item.DataItem, "FwscID"), 0);
                String resKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(FieldsetID, fwscID, "DisplayText");
                dgControls_ltrDisplayTextInEng.Text = this.GetGlobalResourceObject(classKey, resKey).ToString();
            }
        }

        protected void dgControls_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                DropDownList dgControlsNew_cmbFormControls = (DropDownList)e.Item.FindControl("dgControlsNew_cmbFormControls");
                TextBox dgControlsNew_txtOrder = (TextBox)e.Item.FindControl("dgControlsNew_txtOrder");
                TextBox dgControls_txtDisplayTextInEng = (TextBox)e.Item.FindControl("dgControls_txtDisplayTextInEng");
                TextBox dgControls_txtFieldName = (TextBox)e.Item.FindControl("dgControls_txtFieldName");

                if (dgControls_txtDisplayTextInEng.Text.IsNullOrEmptyString() || dgControls_txtFieldName.Text.IsNullOrEmptyString()) return;



                CheckBox dgControlsNew_cbxIsReq = (CheckBox)e.Item.FindControl("dgControlsNew_cbxIsReq");
                Int32 controlID = ConvertUtility.ConvertToInt32(dgControlsNew_cmbFormControls.SelectedValue, 0);
                Int32 controlOrder = ConvertUtility.ConvertToInt32(dgControlsNew_txtOrder.Text, 1);
                Lib.BLL.BLLFrm_FormFieldsetControl.Insert(FieldsetID, controlID, controlOrder
                    , dgControlsNew_cbxIsReq.Checked
                    , FormID
                    , dgControls_txtDisplayTextInEng.Text.Trim()
                    , dgControls_txtDisplayTextInEng.Text.Trim()
                    , dgControls_txtFieldName.Text.Trim()
                    );
                BindFieldsetControls();
            }
            else if (e.CommandName == "DetailsCommand")
            {
                Int32 fwscID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                WebUtility.HttpRedirect(null, String.Format("home?{0}={1}&{2}={3}&{4}={5}"
                    , KeyDef.QSKeys.FormID, FormID
                    , KeyDef.QSKeys.FieldsetID, FieldsetID
                    , KeyDef.QSKeys.FieldsetControlID, fwscID
                    ));
            }
            else if (e.CommandName == "DeleteCommand")
            {
                Int32 fwscID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLFrm_FormFieldsetControl.Delete(fwscID);
                BindFieldsetControls();
            }
        }
    }
}