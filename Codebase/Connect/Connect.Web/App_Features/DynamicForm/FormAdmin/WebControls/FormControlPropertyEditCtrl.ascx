﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormControlPropertyEditCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormControlPropertyEditCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:STCTRL_FormAdmin_FormControlPropertyEditCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
    <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" ShowFooter="true">
        <Columns>
            <asp:TemplateColumn ItemStyle-CssClass="dgstyle1-item-lft" FooterStyle-CssClass="dgstyle1-item-lft">
                <ItemTemplate>
                 <div class="settingrow">
        <span class="settinglabel-20"><asp:Localize ID="lcalCtrlPtyEdt_Key" runat="server" Text="<%$Resources:STCTRL_FormAdmin_FormControlPropertyEditCtrl,lcalCtrlPtyEdt_Key.Text%>"></asp:Localize></span>
      <asp:TextBox ID="txtPropertyKey" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PropertyKey") %>' SkinID="tbx-100"></asp:TextBox>
</div>
                <div class="settingrow">
        <span class="settinglabel-20"><asp:Localize ID="lcalCtrlPtyEdt_Value" runat="server" Text="<%$Resources:STCTRL_FormAdmin_FormControlPropertyEditCtrl,lcalCtrlPtyEdt_Value.Text%>"></asp:Localize></span>
      <asp:TextBox ID="txtPropertyValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PropertyValue") %>' SkinID="tbx-500"></asp:TextBox>
</div>
                </ItemTemplate>
                <FooterTemplate>
                                     <div class="settingrow">
        <span class="settinglabel-20"><asp:Localize ID="lcalCtrlPtyEdt_Key" runat="server" Text="<%$Resources:STCTRL_FormAdmin_FormControlPropertyEditCtrl,lcalCtrlPtyEdt_Key.Text%>"></asp:Localize></span>
      <asp:TextBox ID="txtPropertyKey" runat="server" SkinID="tbx-100"></asp:TextBox>
</div>
                <div class="settingrow">
        <span class="settinglabel-20"><asp:Localize ID="lcalCtrlPtyEdt_Value" runat="server" Text="<%$Resources:STCTRL_FormAdmin_FormControlPropertyEditCtrl,lcalCtrlPtyEdt_Value.Text%>"></asp:Localize></span>
      <asp:TextBox ID="txtPropertyValue" runat="server" SkinID="tbx-500"></asp:TextBox>
</div>
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="bttUpdate" runat="server" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PropertyID") %>' SkinID="SmallerButton" Text="update" />
                    <asp:Button ID="bttDelete" runat="server" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PropertyID") %>' SkinID="SmallerButton" Text="delete" />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Button ID="bttAdd" runat="server" CommandName="addCommand" SkinID="SmallerButton" Text="add" />
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="STCTRL_FormAdmin_FormControlPropertyEditCtrl" />
</anrui:GlobalWebBoxedPanel>