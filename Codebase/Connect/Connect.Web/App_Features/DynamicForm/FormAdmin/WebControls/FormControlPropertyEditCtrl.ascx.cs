﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormControlPropertyEditCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Int32 FwscID { get; set; }
        public List<String> ExcludeKeys { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindControlProperties();
            }
        }

        private void BindControlProperties()
        {
            Lib.DynamicForm.Frm_FormFieldsetControl fsCtrl = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFwscID(FwscID);
            if (fsCtrl == null)
            {
                this.Visible = false;
                return;
            }

            var excl = from p in fsCtrl.Properties where (ExcludeKeys == null || !ExcludeKeys.Contains( p.PropertyKey ))
                       select p;

            dgList.DataSource = excl.ToList<Lib.DynamicForm.Frm_FormFieldsetControlProperty>();
            dgList.DataBind();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_FormAdmin_FormControlPropertyEditCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}