﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormFieldsetEditCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormFieldsetEditCtrl" %>
<%@ Register src="~/App_Controls/ResourceLocalizationItemCtrl.ascx" tagname="ResourceLocalizationItemCtrl" tagprefix="uc1" %>

<%@ Register src="FormControlListCtrl.ascx" tagname="FormControlListCtrl" tagprefix="uc2" %>

<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
        <span class="settinglabel-10"><asp:Localize ID="lcalFSEdt_FieldsetID" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,lcalFSEdt_FieldsetID.Text%>"></asp:Localize></span>
        <asp:TextBox ID="txtFSEdt_FieldsetID" runat="server" SkinID="tbx-100" Enabled="false" ReadOnly="true"></asp:TextBox>
    </div>
<div class="settingrow">
    <span class="settinglabel-10"><asp:Localize ID="lcalFSEdt_OrderNum" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,lcalFSEdt_OrderNum.Text%>"></asp:Localize></span>
    <asp:TextBox ID="txtFSEdt_OrderNum" runat="server" SkinID="tbx-100" Text="1"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabel-10"><asp:Localize ID="lcalFSEdt_IsInternal" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,lcalFSEdt_IsInternal.Text%>"></asp:Localize></span>
    <asp:CheckBox ID="cbxFSEdt_IsInternal" runat="server" />
</div>
<div class="settingrow">
    <span class="settinglabel-10"><asp:Localize ID="lcalFSEdt_Legend" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl,lcalFSEdt_Legend.Text%>"></asp:Localize></span>
    <uc1:ResourceLocalizationItemCtrl ID="ResLcItemFSEdt_Legend" runat="server" />
</div>
<div class="settingrow">
<uc2:FormControlListCtrl ID="FormControlListCtrl1" runat="server" />
</div>
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl" />
</anrui:GlobalWebBoxedPanel>