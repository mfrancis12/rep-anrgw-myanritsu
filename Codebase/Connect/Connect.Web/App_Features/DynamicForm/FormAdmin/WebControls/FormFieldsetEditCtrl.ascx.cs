﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormFieldsetEditCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid FormID { get; set; }
        public Int32 FieldsetID { get; set; }
        public String ClassKey { get { return Lib.BLL.BLLFrm_Form.ResClassKey(FormID);} }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //ResLcItemFSEdt_Legend.ClassKey = ClassKey;
            //ResLcItemFSEdt_Legend.ResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(FieldsetID) + "Legend";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FormID == Guid.Empty || FieldsetID < 1)
                {
                    this.Visible = false;
                    return;
                }
                
                ResLcItemFSEdt_Legend.ClassKey = ClassKey;
                ResLcItemFSEdt_Legend.ResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(FieldsetID) + "Legend";

                FormControlListCtrl1.FieldsetID = FieldsetID;
                FormControlListCtrl1.FormID = FormID;
                InitFieldset();
            }
        }

        private void InitFieldset()
        {
            Lib.DynamicForm.Frm_FormFieldset fs = Lib.BLL.BLLFrm_FormFieldset.SelectByFieldsetID(FieldsetID);
            if (fs == null)
            {
                this.Visible = false;
                return;
            }

            pnlContainer.HeaderText = this.GetGlobalResourceObject(ClassKey, Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(FieldsetID) + "Legend").ToString() + " #" + FieldsetID.ToString();
            txtFSEdt_FieldsetID.Text = fs.FieldsetID.ToString();
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_FormAdmin_FormFieldsetEditCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}