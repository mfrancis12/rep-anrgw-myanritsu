﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormFieldsetListCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormFieldsetListCtrl" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FormFieldsetListCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
 <center>
        <asp:DataGrid ID="dgFieldsets" runat="server" ShowFooter="true" 
                AutoGenerateColumns="false" onitemcommand="dgFieldsets_ItemCommand" 
                onitemdatabound="dgFieldsets_ItemDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="Legend" ItemStyle-CssClass="dgstyle1-item-lft">
                    <ItemTemplate>
                        <asp:Literal ID="ltrFieldsets_LegendInEng" runat="server"></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dgFieldsetsNew_txtLegendInEng" runat="server" SkinID="tbx-300" Text="In english"></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Order">
                    <ItemTemplate>
                        <asp:TextBox ID="dgFieldsets_txtOrder" runat="server" SkinID="tbx-20" Text='<%# DataBinder.Eval(Container.DataItem, "OrderNum") %>'></asp:TextBox>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="dgFieldsetsNew_txtOrder" runat="server" SkinID="tbx-20" Text="1"></asp:TextBox>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="IsInternal">
                    <ItemTemplate>
                    <asp:CheckBox ID="dgFieldsets_cbxIsInternal" runat="server" SkinID="tbx-100" Text='<%# DataBinder.Eval(Container.DataItem, "IsInternal") %>'></asp:CheckBox>
                    </ItemTemplate>
                    <FooterTemplate>
                    <asp:CheckBox ID="dgFieldsetsNew_cbxIsInternal" runat="server" SkinID="tbx-100"></asp:CheckBox>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:Button ID="dgFieldsets_bttUpdate" runat="server" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FieldsetID") %>' Text="update" SkinID="SmallButton" />
                        <asp:Button ID="dgFieldsets_bttDelete" runat="server" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FieldsetID") %>' Text="delete" SkinID="SmallButton" />
                        <asp:Button ID="dgFieldsets_bttDetails" runat="server" OnClientClick="$('#tabs').tabs('select', '#tabs-fsedit');" CommandName="DetailsCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FieldsetID") %>' Text="details" SkinID="SmallButton" />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="dgFieldsetsNew_bttAdd" runat="server" CommandName="AddCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FieldsetID") %>' Text="add" SkinID="SmallButton" />
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        </center>
</div>
 <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FormFieldsetListCtrl" />
</anrui:GlobalWebBoxedPanel>