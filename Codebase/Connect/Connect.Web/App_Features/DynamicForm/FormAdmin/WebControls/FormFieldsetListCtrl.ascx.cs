﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormFieldsetListCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid FormID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FormID.IsNullOrEmptyGuid())
                {
                    this.Visible = false;
                    return;
                }
                
                BindFormFieldsets();
            }
        }

        private void BindFormFieldsets()
        {
            if (FormID.IsNullOrEmptyGuid()) return;
            List<Lib.DynamicForm.Frm_FormFieldset> fieldsets = Lib.BLL.BLLFrm_FormFieldset.SelectByFormID(FormID);
            dgFieldsets.DataSource = fieldsets;
            dgFieldsets.DataBind();
        }

        protected void dgFieldsets_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddCommand")
            {
                TextBox dgFieldsetsNew_txtLegendInEng = (TextBox)e.Item.FindControl("dgFieldsetsNew_txtLegendInEng");
                TextBox dgFieldsetsNew_txtOrder = (TextBox)e.Item.FindControl("dgFieldsetsNew_txtOrder");
                CheckBox dgFieldsetsNew_cbxIsInternal = (CheckBox)e.Item.FindControl("dgFieldsetsNew_cbxIsInternal");

                if (dgFieldsetsNew_txtLegendInEng.Text.IsNullOrEmptyString()) return;
                Int32 orderNum = ConvertUtility.ConvertToInt32(dgFieldsetsNew_txtOrder.Text.Trim(), 1);

                Int32 newFieldsetID = Lib.BLL.BLLFrm_FormFieldset.Insert(FormID, orderNum, dgFieldsetsNew_cbxIsInternal.Checked, dgFieldsetsNew_txtLegendInEng.Text.Trim());
                //if (newFieldsetID < 1)
                //{
                //    ltrMsgFieldsets.Text = "Unable to add new fieldset.";
                //    return;
                //}
                BindFormFieldsets();
            }
            else if (e.CommandName == "DeleteCommand")
            {
                Int32 fieldsetID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                if (fieldsetID > 0) Lib.BLL.BLLFrm_FormFieldset.DeleteByFieldsetID(fieldsetID);
                BindFormFieldsets();
            }
            else if (e.CommandName == "UpdateCommand")
            {
                Int32 fieldsetID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                TextBox dgFieldsets_txtOrder = (TextBox)e.Item.FindControl("dgFieldsets_txtOrder");
                CheckBox dgFieldsets_cbxIsInternal = (CheckBox)e.Item.FindControl("dgFieldsets_cbxIsInternal");
                Int32 orderNum = ConvertUtility.ConvertToInt32(dgFieldsets_txtOrder.Text.Trim(), 1);
                Lib.BLL.BLLFrm_FormFieldset.Update(fieldsetID, orderNum, dgFieldsets_cbxIsInternal.Checked);
                BindFormFieldsets();
            }
            else if (e.CommandName == "DetailsCommand")
            {
                Int32 fieldsetID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                WebUtility.HttpRedirect(null, String.Format("home?{0}={1}&{2}={3}"
                    , KeyDef.QSKeys.FormID, FormID
                    , KeyDef.QSKeys.FieldsetID, fieldsetID
                    ));
            }
        }

        protected void dgFieldsets_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Int32 fieldsetID = ConvertUtility.ConvertToInt32(DataBinder.Eval(e.Item.DataItem, "FieldsetID").ToString(), 0);
                String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(FormID);//Lib.BLL.BLLSite_Module.GetResourceClassKey(ModuleID);
                String resKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(fieldsetID) + "Legend";
                Literal ltrFieldsets_LegendInEng = (Literal)e.Item.FindControl("ltrFieldsets_LegendInEng");
                ltrFieldsets_LegendInEng.Text = HttpContext.GetGlobalResourceObject(classKey, resKey).ToString();
            }
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "STCTRL_FormAdmin_FormFieldsetListCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}