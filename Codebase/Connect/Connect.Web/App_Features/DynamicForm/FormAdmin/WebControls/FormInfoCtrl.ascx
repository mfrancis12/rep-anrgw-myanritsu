﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormInfoCtrl.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls.FormInfoCtrl" %>
<%@ Register src="../../../../App_Controls/ResourceLocalizationItemCtrl.ascx" tagname="ResourceLocalizationItemCtrl" tagprefix="uc1" %>
<%@ Register src="../../../../App_Controls/ResourceLocalizationCtrl.ascx" tagname="ResourceLocalizationCtrl" tagprefix="uc2" %>
<anrui:GlobalWebBoxedPanel ID="pnlContainer" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STCTRL_FormAdmin_FormInfoCtrl,pnlContainer.HeaderText%>">
<div class="settingrow">
<center>
    <asp:DataGrid ID="dgForms" runat="server" AutoGenerateColumns="false" 
        ShowFooter="false" onitemcommand="dgForms_ItemCommand">
        <Columns>
            <asp:BoundColumn DataField="ModuleID" HeaderText="ModuleID"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="FormName">
                <ItemTemplate>
                    <asp:TextBox ID="txtFormName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormName")  %>' SkinID="tbx-100"></asp:TextBox>
                    <asp:HiddenField ID="hddnFormID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "FormID")  %>' />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="bttFormUpdate" runat="server" CommandName="UpdateFormCommand" SkinID="SmallButton" Text="Update" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FormID")  %>' />
                    <asp:Button ID="bttFormDetails" runat="server" CommandName="DetailFormCommand" SkinID="SmallButton" Text="Details" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FormID")  %>'/>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</center>
</div>
<asp:Panel ID="pnlFormDetails" runat="server">
<div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalfsFormFormID" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormInfoCtrl,lcalfsFormFormID.Text%>"></asp:Localize></span>
            <asp:TextBox ID="txtfsFormFormID" runat="server" SkinID="tbx-300" Enabled="false"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalfsFormCreatedOnUTC" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormInfoCtrl,lcalfsFormCreatedOnUTC.Text%>"></asp:Localize></span>
            <asp:TextBox ID="txtfsFormCreatedOnUTC" runat="server" SkinID="tbx-100" Enabled="false"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabel-10"><asp:Localize ID="lcalfsFormHeaderText" runat="server" Text="<%$Resources:ADM_STCTRL_FormAdmin_FormInfoCtrl,lcalfsFormHeaderText%>"></asp:Localize></span>
            <uc1:ResourceLocalizationItemCtrl ID="reslcFormHeaderText" runat="server"/>
        </div>
</asp:Panel>
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_FormAdmin_FormInfoCtrl" />
   
</anrui:GlobalWebBoxedPanel>