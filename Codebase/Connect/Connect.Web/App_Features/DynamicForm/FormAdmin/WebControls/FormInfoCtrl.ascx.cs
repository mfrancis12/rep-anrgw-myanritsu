﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormAdmin.WebControls
{
    public partial class FormInfoCtrl : System.Web.UI.UserControl, App_Lib.UI.IStaticLocalizedCtrl
    {
        public Guid FormID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFormList();
                BindFormInfo();
            }
            reslcFormHeaderText.ClassKey = "FRM_" + FormID.ToString().ToUpperInvariant();
            reslcFormHeaderText.ResKey = "HeaderText";
        }

        private void BindFormList()
        {
            dgForms.DataSource = Lib.BLL.BLLFrm_Form.SelectAll();
            dgForms.DataBind();

            if (FormID != Guid.Empty)
            {
                foreach (DataGridItem dgi in dgForms.Items)
                {
                    Guid formID = ConvertUtility.ConvertToGuid((dgi.FindControl("hddnFormID") as HiddenField).Value.Trim(), Guid.Empty);
                    if (formID == FormID) dgForms.SelectedIndex = dgi.ItemIndex;
                }
            }
        }

        private void BindFormInfo()
        {
            pnlFormDetails.Visible = false;
            Lib.DynamicForm.Frm_Form formInfo = Lib.BLL.BLLFrm_Form.SelectByFormID(FormID);
            if (formInfo != null)
            {
                txtfsFormFormID.Text = formInfo.FormID.ToString().ToUpperInvariant();
                txtfsFormCreatedOnUTC.Text = formInfo.CreatedOnUTC.ToShortDateString();
                pnlFormDetails.Visible = true;
            }
            
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STCTRL_FormAdmin_FormInfoCtrl"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            throw new NotImplementedException();
        }

        #endregion

        protected void dgForms_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "UpdateFormCommand")
            {
                Guid formID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                TextBox txtFormName = e.Item.FindControl("txtFormName") as TextBox;
                TextBox txtAfterSubmitURL = e.Item.FindControl("txtAfterSubmitURL") as TextBox;
                if (!string.IsNullOrEmpty(txtFormName.Text))
                {
                    Lib.BLL.BLLFrm_Form.Update(formID, txtFormName.Text.Trim());
                }
            }
            else if (e.CommandName == "DetailFormCommand")
            {
                Guid formID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                WebUtility.HttpRedirect(null, String.Format("home?{0}={1}", App_Lib.KeyDef.QSKeys.FormID, formID.ToString().ToUpperInvariant()));
            }
        }
    }
}