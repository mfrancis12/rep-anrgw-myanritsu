﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_CheckBox.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_CheckBox" %>
<div class="dfrm-row group input-checkbox">
<asp:CheckBox ID="cbxInput" runat="server" Text='<%# DisplayText %>' />
<span class='dfrm-msg-red'><asp:CustomValidator ID="cvInput" runat="server" onservervalidate="rfvInput_ServerValidate" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:CustomValidator></span>
</div>