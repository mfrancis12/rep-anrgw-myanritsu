﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_CheckBox : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(cbxInput);
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rfvInput_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = cbxInput.Checked;
        }

        #region IDynamicFormControl Members

        public string SelectedValue
        {
            get
            {
                return cbxInput.Checked.ToString();
            }
            set
            {
                cbxInput.Checked = ConvertUtility.ConvertToBoolean(value, false);
            }
        }

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion

        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get { return FieldName; }
        }

        #endregion
    }
}