﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_CheckBoxList.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_CheckBoxList" %>
<div class="dfrm-row">
<p class='dfrm-label'>
    <span class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></span>
    <span class='dfrm-msg-red'><asp:CustomValidator ID="cvInput" runat="server" onservervalidate="cvInput_ServerValidate" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:CustomValidator></span>
</p>
<asp:CheckBoxList ID="cbxListInput" runat="server" SkinID="DFrmSkin" Width='<%# CtrlWidth %>' Height='<%# CtrlHeight %>' RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CellPadding="5" CellSpacing="5"></asp:CheckBoxList>
</div>
