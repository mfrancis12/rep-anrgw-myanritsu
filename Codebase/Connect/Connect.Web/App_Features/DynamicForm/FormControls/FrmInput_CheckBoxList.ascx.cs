﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_CheckBoxList : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(cbxListInput);

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IDynamicFormControl Members

        public string SelectedValue
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (ListItem li in cbxListInput.Items)
                {
                    if (li.Selected) sb.AppendFormat("{0};", li.Value);
                }
                return sb.ToString();
            }
            set
            {
                Char[] deli = new Char[] { ';' };
                String[] v = value.Split(deli);
                if (v != null)
                {
                    foreach (String s in v)
                    {
                        ListItem li = cbxListInput.Items.FindByValue(s);
                        if (li != null) li.Selected = true;
                    }
                }
            }
        }

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion

        protected void cvInput_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Boolean atLeastOneSelected = false;
            foreach (ListItem item in cbxListInput.Items)
            {
                if (item.Selected)
                {
                    atLeastOneSelected = true;
                    break;
                }
            }
            args.IsValid = atLeastOneSelected;
        }

        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get { return FieldName; }
        }

        #endregion
    }
}