﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_CountryList.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_CountryList" %>
<div class="dfrm-row group input-select">
<%--<p class='dfrm-label'>--%>
    <label class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></label>
    <span class='dfrm-msg-red'><asp:CustomValidator ID="cvInput" runat="server" onservervalidate="cvInput_ServerValidate" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:CustomValidator></span>
<%--</p>--%>
<asp:DropDownList ID="cmbListInput" runat="server" ></asp:DropDownList>
</div>
