﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_CountryList : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            SetCtrlProperties(cmbListInput);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCountries();
            }
        }

        private void BindCountries()
        {
            cmbListInput.Items.Clear();
            DataTable tb = Lib.BLL.BLLIso_Country.SelectAll();
            if(tb == null) {
                this.Visible = false;
                return;
            }
            
            foreach(DataRow r in tb.Rows)
            {
                String resKey = r["ISOCountryNameResKeyValue"].ToString();
                ListItem li = new ListItem();
                li.Value = r["Alpha2"].ToString().ToUpperInvariant();
                li.Text = this.GetGlobalResourceObject(Lib.BLL.BLLIso_Country.ISOCountryNameClassKey, r["ISOCountryNameResKeyValue"].ToString()).ToString();
                cmbListInput.Items.Add(li);
            }
            
        }

        protected void cvInput_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = cmbListInput.SelectedIndex > -1;
        }

        #region IDynamicFormControl Members

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public string FieldKeyName
        {
            get { return FieldName; }
        }

        public string SelectedValue
        {
            get
            {
                return cmbListInput.SelectedValue;
            }
            set
            {
                cmbListInput.SelectedValue = value;
            }
        }

        public bool IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion

        
    }
}