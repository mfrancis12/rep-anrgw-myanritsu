﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Anritsu.AnrCommon.CoreLib;



namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_DropDownList : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cvInput_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = cmbListInput.SelectedIndex > -1;
        }

        #region IDynamicFormControl Members

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public string SelectedValue
        {
            get
            {
                return cmbListInput.SelectedValue;
            }
            set
            {
                cmbListInput.SelectedValue = value;
            }
        }

        public bool IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion


        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get { return FieldName; }
        }

        #endregion
    }
}