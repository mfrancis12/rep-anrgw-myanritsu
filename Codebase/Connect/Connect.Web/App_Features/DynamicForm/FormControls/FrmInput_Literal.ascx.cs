﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_Literal : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(ltrText);
            //if (ControlData != null && ControlData.Properties != null)
            //{
            //    Type controlType = ltrText.GetType();
            //    PropertyInfo[] ctrlProperties = controlType.GetProperties();
            //    foreach (Lib.DynamicForm.Frm_FormFieldsetControlProperty p in ControlData.Properties)
            //    {
            //        SetCtrlProperty(ltrText, ctrlProperties, p.PropertyKey, p.PropertyValue);
            //    }
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IDynamicFormControl Members

        public String SelectedValue
        {
            get
            {
                return ltrText.Text;
            }
            set
            {
                ltrText.Text = value;
            }
        }

        public String FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return true; }
        }

        #endregion

        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get { return FieldName; }
        }

        #endregion
    }
}