﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_RadioButton.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_RadioButton" %>
<div class="dfrm-row">
<p class='dfrm-label'>
    <span class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></span>
</p>
<asp:RadioButton ID="rdoInput" runat="server" SkinID="DFrmSkin" />
</div>
