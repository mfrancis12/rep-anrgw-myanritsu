﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_RadioButtonList.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_RadioButtonList" %>

<div class="dfrm-row">
<p class='dfrm-label'>
    <span class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></span>
    <span class='dfrm-msg-red'><asp:CustomValidator ID="cvInput" runat="server" onservervalidate="rfvInput_ServerValidate" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:CustomValidator></span>
</p>
    <div style="width:415px">
<asp:RadioButtonList ID="rdoListInput" runat="server" SkinID="DFrmSkin" Width="100%" Height='<%# CtrlHeight %>' RepeatDirection="Horizontal" RepeatLayout="Table" >
   </asp:RadioButtonList>
        </div>
</div>
