﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_RadioButtonList : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(rdoListInput);

        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rfvInput_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Boolean atLeastOneSelected = false;
            foreach (ListItem item in rdoListInput.Items)
            {
                if (item.Selected)
                {
                    atLeastOneSelected = true;
                    break;
                }
            }
            args.IsValid = atLeastOneSelected;
        }

        #region IDynamicFormControl Members

        public string SelectedValue
        {
            get
            {
                //StringBuilder sb = new StringBuilder();
                //foreach (ListItem li in rdoListInput.Items)
                //{
                //    if (li.Selected) sb.AppendFormat("{0};", li.Value);
                //}
                //return sb.ToString();
                return rdoListInput.SelectedValue;
            }
            set
            {
                Char[] deli = new Char[] { ';' };
                String[] v = value.Split(deli);
                if (v != null)
                {
                    foreach (String s in v)
                    {
                        ListItem li = rdoListInput.Items.FindByValue(s);
                        if (li != null) li.Selected = true;
                    }
                }
            }
        }

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion

        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get { return FieldName; }
        }

        #endregion
    }
}