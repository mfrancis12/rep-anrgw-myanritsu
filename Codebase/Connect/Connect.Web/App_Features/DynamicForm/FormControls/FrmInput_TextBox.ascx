﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_TextBox.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_TextBox" %>
<div class="dfrm-row group input-text">
<%--<p class='dfrm-label'>--%>
    <label class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></label>
    <span class='dfrm-msg-red'><asp:RequiredFieldValidator ID="rfvInput" runat="server" ControlToValidate="txtInput" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator></span>
<%--</p>--%>
<asp:TextBox ID="txtInput" runat="server" TextMode="SingleLine"></asp:TextBox>
</div>
