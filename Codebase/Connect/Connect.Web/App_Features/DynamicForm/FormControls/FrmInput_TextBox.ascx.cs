﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_TextBox : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(txtInput);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IDynamicFormControl Members

        public string SelectedValue
        {
            get
            {
                return txtInput.Text;
            }
            set
            {
                txtInput.Text = value;
            }
        }

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion




        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get
            {
                return FieldName;
            }
        }

        #endregion
    }
}