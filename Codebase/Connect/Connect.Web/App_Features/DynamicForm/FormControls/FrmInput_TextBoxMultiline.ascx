﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_TextBoxMultiline.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_TextBoxMultiline" %>
<div class="dfrm-row group input-textarea">
<%--<p class='dfrm-label'>--%>
    <label class='dfrm-label-text'><asp:Localize ID="lcalLabel" runat="server" Text='<%# DisplayText %>'></asp:Localize></label>
    <span class='dfrm-msg-red'><asp:RequiredFieldValidator ID="rfvInput" runat="server" ControlToValidate="txtInput" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator></span>
<%--</p>--%>
<asp:TextBox ID="txtInput" runat="server" TextMode="MultiLine"></asp:TextBox>
</div>
