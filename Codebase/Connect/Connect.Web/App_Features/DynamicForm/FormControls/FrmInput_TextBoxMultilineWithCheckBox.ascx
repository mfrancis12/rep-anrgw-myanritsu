﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FrmInput_TextBoxMultilineWithCheckBox.ascx.cs" Inherits="Anritsu.Connect.Web.App_Features.DynamicForm.FormControls.FrmInput_TextBoxMultilineWithCheckBox" %>
<div class="dfrm-row group input-textarea">
<%--<p class='dfrm-label'>--%>
    <span class='dfrm-label-text'><asp:CheckBox ID="cbxInput" runat="server" AutoPostBack="true" 
        Text='<%# DisplayText %>' oncheckedchanged="cbxInput_CheckedChanged" /></span>
    <span class='dfrm-msg-red'><asp:RequiredFieldValidator ID="rfvInput" runat="server" ControlToValidate="txtInput" Enabled="false" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator></span>
<%--</p>--%>
<asp:TextBox ID="txtInput" runat="server" TextMode="MultiLine" Visible="false"></asp:TextBox>
</div>
