﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.FormControls
{
    public partial class FrmInput_TextBoxMultilineWithCheckBox : UILib.DynamicFormControlBase, UILib.IDynamicFormControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            SetCtrlProperties(txtInput);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IDynamicFormControl Members

        public string SelectedValue
        {
            get
            {

                return cbxInput.Checked ? txtInput.Text : String.Empty;
            }
            set
            {
                String strValue = value;
                if (strValue.IsNullOrEmptyString())
                {
                    cbxInput.Checked = false;
                    txtInput.Text = String.Empty;
                }
                else
                {
                    cbxInput.Checked = true;
                    txtInput.Text = strValue;
                }
            }
        }

        public string FormInputCtrlID
        {
            get { return this.ID; }
        }

        public Boolean IsReadyOnlyCtrl
        {
            get { return false; }
        }

        #endregion




        #region IDynamicFormControl Members


        public string FieldKeyName
        {
            get
            {
                return FieldName;
            }
        }

        #endregion

        protected void cbxInput_CheckedChanged(object sender, EventArgs e)
        {
            txtInput.Visible = cbxInput.Checked;
            if (!cbxInput.Checked) txtInput.Text = String.Empty;
        }
    }
}