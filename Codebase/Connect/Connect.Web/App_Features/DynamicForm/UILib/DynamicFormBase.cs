﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public abstract class DynamicFormBase : App_Lib.UI.FeatureCtrlBase
    {
        private Lib.Security.Sec_UserMembership LoggedInUser
        {
            get
            {
                //approval forms are only for logged in users
                return LoginUtility.GetCurrentUserOrSignIn();
            }
        }

        private Lib.DynamicForm.Frm_Form _FormData;
        public Lib.DynamicForm.Frm_Form FormData
        {
            get
            {
                if (ModulePageData == null)
                {
                    _FormData = null;
                    return null;
                }

                if (_FormData == null)
                {
                    _FormData = Lib.BLL.BLLFrm_Form.SelectByModuleID(ModulePageData.ModuleID, false);
                }
                return _FormData;
            }
        }

        public String ValidationGroup
        {
            get
            {
                if (FormData == null) return String.Empty;
                return String.Format("dyfVG{0}", FormData.FormID.ToString().ToUpperInvariant());
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        public void InitFormControls(PlaceHolder phFrm)
        {
            if (FormData == null) return;
            Lib.Security.Sec_UserMembership usr = LoggedInUser;
            List<Lib.DynamicForm.Frm_FormFieldset> fieldsets = Lib.BLL.BLLFrm_FormFieldset.SelectByModuleID(FormData.ModuleID);
            if (fieldsets == null) return;
            HtmlGenericControl fieldset;
            HtmlGenericControl legend;            
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(FormData.FormID);

            foreach (Lib.DynamicForm.Frm_FormFieldset ff in fieldsets)
            {
                //if (ff.IsInternal && !Features.DynamicForm.BLL.BLLFormFieldset.IsAuthorizedToView(ff.FieldsetID, userEmail))
                //{
                //    continue;
                //}

                fieldset = new HtmlGenericControl("fieldset");
                fieldset.ID = String.Format("dyfFS{0}", ff.FieldsetID);
                fieldset.Attributes.Add("class", "dfrm-fieldset");
                legend = new HtmlGenericControl("legend");
                String fsLegendResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(ff.FieldsetID) + "Legend";
                legend.InnerText =  this.GetGlobalResourceObject(classKey, fsLegendResKey).ToString();
                fieldset.Controls.Add(legend);

                List<Lib.DynamicForm.Frm_FormFieldsetControl> controls = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFieldsetID(ff.FieldsetID);
                if (controls != null)
                {
                    Panel pnl = new Panel();
                    pnl.ID = String.Format("dyfFSPnl{0}", ff.FieldsetID);
                    pnl.SkinID = "DFrmSkin";
                    int ctrlCount = 0;
                    foreach (Lib.DynamicForm.Frm_FormFieldsetControl ffc in controls)
                    {
                        DynamicFormControlBase ctrl = Page.LoadControl(ffc.ControlInfo.ControlPath) as DynamicFormControlBase;
                        if (ctrl == null) continue;
                        ctrl.ID = String.Format("dyfCtrl{0}", ffc.FwscID);
                        ctrl.LoadData(FormData.FormID, ffc, ValidationGroup);

                        pnl.Controls.Add(ctrl);
                        ctrlCount++;
                    }
                    if (ctrlCount > 0) fieldset.Controls.Add(pnl);
                }
                fieldset.DataBind();
                phFrm.Controls.Add(fieldset);
            }

            Button bttFrmSubmit = this.FindControl("bttFrmSubmit") as Button;
            if (bttFrmSubmit != null)
            {
                bttFrmSubmit.ValidationGroup = ValidationGroup;
            }
        }

        public void FindFormControls(ControlCollection ctrlCollection, ref Dictionary<String, String> keyValues)
        {
            if (keyValues == null) keyValues = new Dictionary<string, string>();
            if (ctrlCollection == null) return;
            foreach (Control c in ctrlCollection)
            {
                if (c is DynamicFormControlBase && c is UILib.IDynamicFormControl)
                {
                    UILib.IDynamicFormControl iCtrl = c as UILib.IDynamicFormControl;
                    if(!iCtrl.IsReadyOnlyCtrl) keyValues.Add(iCtrl.FormInputCtrlID, iCtrl.SelectedValue.Trim());
                }
                else if (c.Controls != null)
                {
                    FindFormControls(c.Controls, ref keyValues);
                }
            }
        }

        //public Guid SubmitForm(PlaceHolder phFrm, Literal ltrMsg)
        //{
        //    if (phFrm == null) return Guid.Empty;
        //    if (FormData == null) return Guid.Empty;
        //    Lib.Security.Sec_UserMembership usr = LoggedInUser;
        //    if (usr == null) return Guid.Empty;

        //    try
        //    {
        //        Dictionary<String, String> keyValues = new Dictionary<String, String>();
        //        FindFormControls(phFrm.Controls, ref keyValues);               

        //        if (keyValues.Count < 1) throw new ArgumentException("Unable to submit the form.");
        //        Int32 submitID = 0;
        //        Guid submitKey = Guid.Empty;

        //        Boolean submitted = Lib.BLL.BLLFrm_FormSubmitMaster.Insert(FormData.FormID, FormData.FormName
        //            , usr.Email
        //            , keyValues
        //            , out submitID, out submitKey);
        //        if (!submitted) return Guid.Empty;
        //        return submitKey;
        //    }
        //    catch (ArgumentNullException anex)
        //    {
        //        if (ltrMsg != null) ltrMsg.Text = anex.Message;
        //    }
        //    catch (ArgumentException aex)
        //    {
        //        if (ltrMsg != null) ltrMsg.Text = aex.Message;

        //    }
        //    return Guid.Empty;
        //}
    }
}