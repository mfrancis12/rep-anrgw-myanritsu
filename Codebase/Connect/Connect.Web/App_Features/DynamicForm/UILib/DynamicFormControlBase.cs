﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Reflection;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public abstract class DynamicFormControlBase : System.Web.UI.UserControl
    {
        public Unit CtrlWidth { get; set; }
        public Unit CtrlHeight { get; set; }
        public Lib.DynamicForm.Frm_FormFieldsetControl ControlData { get; set; }
        public Guid FormID { get; set; }
        public String DisplayText
        {
            get
            {
                if (ControlData == null) return String.Empty;
                String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(FormID);
                String resKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(ControlData.FieldsetID, ControlData.FwscID
                    , UILib.FormControlPropertyKeys.DisplayText);
                return this.GetGlobalResourceObject(classKey, resKey).ToString();
            }
        }
        public String FieldName
        {
            get
            {
                if (ControlData == null) return String.Empty;
                return ControlData.FieldName;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        public virtual void LoadData(Guid formID, Lib.DynamicForm.Frm_FormFieldsetControl controlData, String validationGroup)
        {
            if (controlData == null || formID.IsNullOrEmptyGuid()) return;
            ControlData = controlData;
            FormID = formID;
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ctrlWidth = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(controlData.Properties, UILib.FormControlPropertyKeys.CtrlWidth);
            Lib.DynamicForm.Frm_FormFieldsetControlProperty ctrlHeight = Lib.BLL.BLLFrm_FormFieldsetControlProperty.Find(controlData.Properties, UILib.FormControlPropertyKeys.CtrlHeight);
            if (ctrlWidth != null && !String.IsNullOrEmpty(ctrlWidth.PropertyValue)) CtrlWidth = new Unit(ctrlWidth.PropertyValue);
            if (ctrlHeight != null && !String.IsNullOrEmpty(ctrlHeight.PropertyValue)) CtrlHeight = new Unit(ctrlHeight.PropertyValue);

            RequiredFieldValidator rfv = this.FindControl("rfvInput") as RequiredFieldValidator;
            if (rfv != null)
            {
                rfv.Enabled = controlData.IsRequired;
                rfv.ValidationGroup = validationGroup;
            }
            CustomValidator cvInput = this.FindControl("cvInput") as CustomValidator;
            if (cvInput != null)
            {
                cvInput.Enabled = controlData.IsRequired;
                cvInput.ValidationGroup = validationGroup;
            }

        }

        public void SetCtrlProperty(System.Web.UI.Control ctrl, PropertyInfo[] ctrlProperties, String propertyName, String propertyValue)
        {            
            
            if (ctrl == null || ctrlProperties == null || propertyName.IsNullOrEmptyString()) return;
            var pty = from p in ctrlProperties
                      where p.Name.Equals(propertyName, StringComparison.InvariantCultureIgnoreCase)
                      select p;
            PropertyInfo pi = pty.FirstOrDefault<PropertyInfo>();
            if (pi != null)
            {
                if(pi.PropertyType == typeof(String))
                {
                    pi.SetValue(ctrl, propertyValue, null);
                }
                else if (pi.PropertyType == typeof(Int32))
                {
                    pi.SetValue(ctrl, ConvertUtility.ConvertToInt32(propertyValue, 0), null);
                }
                else if (pi.PropertyType == typeof(Boolean))
                {
                    pi.SetValue(ctrl, ConvertUtility.ConvertToBoolean(propertyValue, false), null);
                }
                else if (pi.PropertyType == typeof(DateTime))
                {
                    pi.SetValue(ctrl, ConvertUtility.ConvertToDateTime(propertyValue, DateTime.MinValue), null);
                }
                //else if (pi.PropertyType == typeof(TextBoxMode))
                //{
                //    switch (propertyValue)
                //    {
                //        case "MultiLine":
                //            pi.SetValue(ctrl, TextBoxMode.MultiLine, null);
                //            break;
                //        case "Password":
                //            pi.SetValue(ctrl, TextBoxMode.Password, null);
                //            break;
                //        default:
                //            pi.SetValue(ctrl, TextBoxMode.SingleLine, null);
                //            break;
                //    }

                //}
                
            }
        }

        public virtual void SetCtrlProperties(System.Web.UI.Control ctrl)
        {
            if (ControlData != null && ControlData.Properties != null)
            {
                Type controlType = ctrl.GetType();

                #region " load datasource "
                if (ControlData.ControlInfo.IsDataSourceRequired && ControlData.DataSourceID > 0)
                {
                    Lib.DynamicForm.Frm_FormControlDataSource ds = Lib.BLL.BLLFrm_FormControlDataSource.SelectByDataSourceID(ControlData.DataSourceID);
                    if (ds != null)
                    {
                        String classKey = Lib.BLL.BLLFrm_FormControlDataSource.ResClassKey(ds.DataSourceID);
                            if (controlType == typeof(CheckBoxList))
                            {
                                CheckBoxList lstCtrl = (CheckBoxList)ctrl;
                                 foreach (Lib.DynamicForm.Frm_FormControlDataSourceData data in ds.DataItems)
                                 {
                                     String text = HttpContext.GetGlobalResourceObject(classKey, Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemText(data.DSDataID)).ToString();
                                     String value = HttpContext.GetGlobalResourceObject(classKey, Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemValue(data.DSDataID)).ToString();
                                     ListItem ls = new ListItem(text, value);
                                   
                                     ls.Selected = data.IsSelected;
                                     lstCtrl.Items.Add(ls);
                                 }
                            }
                            else if (controlType == typeof(RadioButtonList))
                            {
                                RadioButtonList lstCtrl = (RadioButtonList)ctrl;
                               foreach (Lib.DynamicForm.Frm_FormControlDataSourceData data in ds.DataItems)
                                 {
                                     String text = HttpContext.GetGlobalResourceObject(classKey, Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemText(data.DSDataID)).ToString();
                                     String value = HttpContext.GetGlobalResourceObject(classKey, Lib.BLL.BLLFrm_FormControlDataSourceData.ResKeyItemValue(data.DSDataID)).ToString();
                                     ListItem ls = new ListItem(text, value);
                                     ls.Selected = data.IsSelected;
                                     lstCtrl.Items.Add(ls);
                                 }
                            }
                    }
                } 
                #endregion

                
                PropertyInfo[] ctrlProperties = controlType.GetProperties();
                Boolean isListCtrl = (controlType == typeof(CheckBoxList) || controlType == typeof(RadioButtonList));
                foreach (Lib.DynamicForm.Frm_FormFieldsetControlProperty p in ControlData.Properties)
                {
                    if (isListCtrl && p.PropertyKey.StartsWith("ListItem_"))
                    {
                        ////Char[] deliItemProperty = new Char[] { '|' };
                        ////String[] itemProperties = p.PropertyValue.Split(deliItemProperty);
                        ////if (itemProperties != null)
                        ////{
                        ////    String text = String.Empty;
                        ////    String value = String.Empty;
                        ////    Boolean selected = false;
                        ////    Boolean enabled = true;

                        ////    foreach (String sliP in itemProperties)
                        ////    {
                        ////        if (sliP.IsNullOrEmptyString()) continue;

                        ////        if (sliP.StartsWith("Text=", StringComparison.InvariantCultureIgnoreCase)) text = sliP.Substring(5);
                        ////        else if (sliP.StartsWith("VALUE=", StringComparison.InvariantCultureIgnoreCase)) value = sliP.Substring(6);
                        ////        else if (sliP.StartsWith("Selected=", StringComparison.InvariantCultureIgnoreCase)) selected = ConvertUtility.ConvertToBoolean(sliP.Substring(9), false);
                        ////        else if (sliP.StartsWith("Enabled=", StringComparison.InvariantCultureIgnoreCase)) enabled = ConvertUtility.ConvertToBoolean(sliP.Substring(8), true);
                        ////    }
                        ////    if (!String.IsNullOrEmpty(text) && !string.IsNullOrEmpty(value))
                        ////    {
                        ////        ListItem ls = new ListItem(text, value);
                        ////        ls.Selected = selected;
                        ////        ls.Enabled = enabled;
                        ////        if (controlType == typeof(CheckBoxList))
                        ////        {
                        ////            CheckBoxList lstCtrl = (CheckBoxList)ctrl;
                        ////            lstCtrl.Items.Add(ls);
                        ////        }
                        ////        else if (controlType == typeof(RadioButtonList))
                        ////        {
                        ////            RadioButtonList lstCtrl = (RadioButtonList)ctrl;
                        ////            lstCtrl.Items.Add(ls);
                        ////        }
                        ////    }
                        ////}
                    }
                    else
                    {
                        SetCtrlProperty(ctrl, ctrlProperties, p.PropertyKey, p.PropertyValue);
                    }
                }
            }
        }
    }
}