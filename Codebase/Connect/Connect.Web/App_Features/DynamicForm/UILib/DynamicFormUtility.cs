﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public static class DynamicFormUtility
    {
        public static String FormResourceClassKey(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return String.Empty;
            return String.Format("FRM_{0}", formID.ToString().ToUpperInvariant());
        }

        public static String ValidationGroup(Guid formID)
        {
            if (formID.IsNullOrEmptyGuid()) return String.Empty;
            return String.Format("dyfVG{0}", formID.ToString().ToUpperInvariant());
        }

        public static void InitFormControls(Page pg, PlaceHolder phFrm, Lib.DynamicForm.Frm_Form formData, Lib.Security.Sec_UserMembership user)
        {
            if (formData == null) return;

            List<Lib.DynamicForm.Frm_FormFieldset> fieldsets = Lib.BLL.BLLFrm_FormFieldset.SelectByFormID(formData.FormID);
            if (fieldsets == null) return;
            HtmlGenericControl fieldset;
            HtmlGenericControl legend;
            String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(formData.FormID);

            foreach (Lib.DynamicForm.Frm_FormFieldset ff in fieldsets)
            {
                //if (ff.IsInternal && !Features.DynamicForm.BLL.BLLFormFieldset.IsAuthorizedToView(ff.FieldsetID, userEmail))
                //{
                //    continue;
                //}

                fieldset = new HtmlGenericControl("fieldset");
                fieldset.ID = String.Format("dyfFS{0}", ff.FieldsetID);
                fieldset.Attributes.Add("class", "dfrm-fieldset");
                legend = new HtmlGenericControl("legend");
                String fsLegendResKey = Lib.BLL.BLLFrm_FormFieldset.GetResourceResKeyPrefix(ff.FieldsetID) + "Legend";
                legend.InnerText = HttpContext.GetGlobalResourceObject(classKey, fsLegendResKey).ToString();
                fieldset.Controls.Add(legend);

                List<Lib.DynamicForm.Frm_FormFieldsetControl> controls = Lib.BLL.BLLFrm_FormFieldsetControl.SelectByFieldsetID(ff.FieldsetID);
                if (controls != null)
                {
                    Panel pnl = new Panel();
                    pnl.ID = String.Format("dyfFSPnl{0}", ff.FieldsetID);
                    pnl.SkinID = "DFrmSkin";
                    int ctrlCount = 0;
                    foreach (Lib.DynamicForm.Frm_FormFieldsetControl ffc in controls)
                    {
                        DynamicFormControlBase ctrl = pg.LoadControl(ffc.ControlInfo.ControlPath) as DynamicFormControlBase;
                        if (ctrl == null) continue;
                        ctrl.ID = String.Format("dyfCtrl{0}", ffc.FwscID);
                        ctrl.LoadData(formData.FormID, ffc, ValidationGroup(formData.FormID));

                        pnl.Controls.Add(ctrl);
                        ctrlCount++;
                    }
                    if (ctrlCount > 0) fieldset.Controls.Add(pnl);
                }
                fieldset.DataBind();
                phFrm.Controls.Add(fieldset);
            }

            Button bttFrmSubmit = pg.FindControl("bttFrmSubmit") as Button;
            if (bttFrmSubmit != null)
            {
                bttFrmSubmit.ValidationGroup = ValidationGroup(formData.FormID);
            }
        }

        public static void FindFormControls(ControlCollection ctrlCollection, ref List<KeyValuePair<String, String>> keyValues)
        {
            if (keyValues == null) keyValues = new List<KeyValuePair<String, String>>();
            if (ctrlCollection == null) return;
            foreach (Control c in ctrlCollection)
            {
                if (c is DynamicFormControlBase && c is UILib.IDynamicFormControl)
                {
                    UILib.IDynamicFormControl iCtrl = c as UILib.IDynamicFormControl;
                    String keyStr = iCtrl.FieldKeyName;//String.Format("{0}_|_{1}", iCtrl.FormInputCtrlID, iCtrl.FieldKeyName);
                    if (!iCtrl.IsReadyOnlyCtrl)
                    {
                        keyValues.Add(new KeyValuePair<string, string>(keyStr, iCtrl.SelectedValue.Trim()));
                        //keyValues.Add(iCtrl.FormInputCtrlID, iCtrl.SelectedValue.Trim());
                        
                    }
                }
                else if (c.Controls != null)
                {
                    FindFormControls(c.Controls, ref keyValues);
                }
            }
        }

        public static void FindFormControl(ControlCollection ctrlCollection, String dataKey, ref UILib.IDynamicFormControl foundCtrl)
        {
            if (dataKey.IsNullOrEmptyString() || ctrlCollection == null || foundCtrl != null) return;

            foreach (Control c in ctrlCollection)
            {
                if (c is DynamicFormControlBase && c is UILib.IDynamicFormControl)
                {
                    UILib.IDynamicFormControl iCtrl = c as UILib.IDynamicFormControl;
                    if (dataKey.Equals(iCtrl.FieldKeyName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        foundCtrl = iCtrl;
                        break;
                    }
                }
                else if (c.Controls != null && foundCtrl == null)
                {
                    FindFormControl(c.Controls, dataKey, ref foundCtrl);
                }
            }
        }

        public static void FindFormControlValues(ControlCollection ctrlCollection, ref List<FormControlData> data)
        {
            if (data == null) data = new List<FormControlData>();
            if (ctrlCollection == null) return;
            foreach (Control c in ctrlCollection)
            {
                if (c is DynamicFormControlBase && c is UILib.IDynamicFormControl)
                {
                    DynamicFormControlBase dCtrl = c as DynamicFormControlBase;
                    UILib.IDynamicFormControl iCtrl = c as UILib.IDynamicFormControl;
                    String keyStr = iCtrl.FieldKeyName;//String.Format("{0}_|_{1}", iCtrl.FormInputCtrlID, iCtrl.FieldKeyName);
                    if (!iCtrl.IsReadyOnlyCtrl)
                    {
                        data.Add(new FormControlData(iCtrl.FieldKeyName
                            , dCtrl.FormID
                            , dCtrl.ControlData.FieldsetID
                            , dCtrl.ControlData.FwscID
                            , iCtrl.SelectedValue.Trim()));
                      
                    }
                }
                else if (c.Controls != null)
                {
                    FindFormControlValues(c.Controls, ref data);
                }
            }
        }

        public static List<KeyValuePair<String, String>> GetFormValues(PlaceHolder phFrm)
        {
            if (phFrm == null) return null;
            List<KeyValuePair<String, String>> keyValues = new List<KeyValuePair<String, String>>();
            FindFormControls(phFrm.Controls, ref keyValues);
            return keyValues;
        }

        public static List<FormControlData> GetFormData(PlaceHolder phFrm)
        {
            if (phFrm == null) return null;
            List<FormControlData> dataList = new List<FormControlData>();
            FindFormControlValues(phFrm.Controls, ref dataList);
            return dataList;
        }

        public static void SetFormData(PlaceHolder phFrm, DataTable formKeyValueData)
        {
            if (phFrm == null || formKeyValueData == null || formKeyValueData.Rows.Count < 1) return;
            ControlCollection ctrlCollection = phFrm.Controls;
            if (ctrlCollection == null) return;

            foreach (DataRow r in formKeyValueData.Rows)
            {
                String dataKey = r["DataKey"].ToString();
                if (dataKey.StartsWith("Submitted", StringComparison.InvariantCultureIgnoreCase)) continue;
                UILib.IDynamicFormControl iCtrl = null;
                FindFormControl(ctrlCollection, dataKey, ref iCtrl);
                if (iCtrl != null)
                {
                    iCtrl.SelectedValue = r["DataValue"].ToString();
                }
            }
            
        }
    }
}