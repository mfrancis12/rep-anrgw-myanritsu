﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public class FormControlData
    {
        public String FieldKeyName { get; set; }
        public Guid FormID { get; set; }
        public Int32 FieldsetID { get; set; }
        public Int32 FieldsetControlID { get; set; }
        public String SelectedValue { get; set; }
        public String DisplayText
        {
            get
            {
                if (FormID == Guid.Empty) return String.Empty;
                String classKey = Lib.BLL.BLLFrm_Form.ResClassKey(FormID);
                String resKey = Lib.BLL.BLLFrm_FormFieldsetControl.GenerateResourceKey(FieldsetID, FieldsetControlID
                    , UILib.FormControlPropertyKeys.DisplayText);
                return HttpContext.GetGlobalResourceObject(classKey, resKey).ToString();
            }
        }

        public FormControlData() { }

        public FormControlData(String fieldKeyName, Guid formID, Int32 fieldsetID, Int32 fieldsetControlID, String selectedValue)
        {
            FieldKeyName = fieldKeyName;
            FormID = formID;
            FieldsetID = fieldsetID;
            FieldsetControlID = fieldsetControlID;
            SelectedValue = selectedValue;
        }
    }
}