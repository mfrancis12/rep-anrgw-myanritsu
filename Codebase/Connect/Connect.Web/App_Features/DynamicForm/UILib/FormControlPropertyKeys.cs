﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public class FormControlPropertyKeys
    {
        public const String DisplayText = "DisplayText";
        public const String ReportText = "ReportText";
        public const String CtrlWidth = "CtrlWidth";
        public const String CtrlHeight = "CtrlHeight";
        //public const String TextBoxMode = "TextBoxMode";
        //public const String TextBoxRows = "Rows";
        public const String Text = "Text";
        public const String Checked = "Checked";
    }
}