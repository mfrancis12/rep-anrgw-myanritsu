﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Features.DynamicForm.UILib
{
    public interface IDynamicFormControl
    {
        String FormInputCtrlID { get; }
        String FieldKeyName { get; }
        String SelectedValue { get; set; }
        Boolean IsReadyOnlyCtrl { get;}
    }
}