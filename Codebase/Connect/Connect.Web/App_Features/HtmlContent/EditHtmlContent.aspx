﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="EditHtmlContent.aspx.cs" Inherits="Anritsu.Connect.Web.App_Features.HtmlContent.EditHtmlContent" ValidateRequest="false"%>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register src="~/App_Controls/ModuleInfo.ascx" tagname="ModuleInfo" tagprefix="uc3" %>
<%@ Register src="~/App_Controls/ModuleOnPageCtrl.ascx" tagname="ModuleOnPageCtrl" tagprefix="uc4" %>
<%@ Register src="~/App_Controls/ModuleSettingCtrl.ascx" tagname="ModuleSettingCtrl" tagprefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<asp:PlaceHolder runat="server">
<link href="<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/css/south-street/jquery-ui-1.8.16.custom.css"   type="text/css" rel="stylesheet" />
        </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div id="tabs">
	<ul>
		<li><a href="#tabs-resources"><asp:Localize ID="lcalTabPageRes" runat="server" Text="<%$Resources:ThisPage,lcalTabPageRes.Text%>"></asp:Localize></a></li>
		<li><a href="#tabs-moduleinfo"><asp:Localize ID="lcalTabModuleinfo" runat="server" Text="<%$Resources:STCTRL_ModuleInfo,lcalTabModuleinfo.Text%>"></asp:Localize></a></li>
        <li><a href="#tabs-moduleonpages"><asp:Localize ID="lcalTabModuleOnPages" runat="server" Text="<%$Resources:STCTRL_ModuleOnPageCtrl,lcalTabModuleOnPages.Text%>"></asp:Localize></a></li>
        <li><a href="#tabs-modulesettings"><asp:Localize ID="lcalTabModuleSettings" runat="server" Text="<%$Resources:STCTRL_ModuleSettingCtrl,lcalTabModuleSettings.Text%>"></asp:Localize></a></li>
	</ul>
	<div id="tabs-resources">
        <asp:HyperLink ID="hlLocalizations" runat="server" Text="Localizations"></asp:HyperLink>
	</div>
    <div id="tabs-moduleinfo">
        <uc3:ModuleInfo ID="ModuleInfo1" runat="server" />
	</div>
    <div id="tabs-moduleonpages">
        <uc4:ModuleOnPageCtrl ID="ModuleOnPageCtrl1" runat="server" />
	</div>
    <div id="tabs-modulesettings">
        <uc5:ModuleSettingCtrl ID="ModuleSettingCtrl1" runat="server" />
	</div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
<script type="text/javascript" src="/App_ClientControls/tiny_mce/tiny_mce.js"></script>
     <script type="text/javascript" defer="defer">
         (function () {
             function getScript(url, success) {
                 var script = document.createElement('script');
                 script.src = url;
                 var head = document.getElementsByTagName('head')[0],
            done = false;
                 script.onload = script.onreadystatechange = function () {
                     if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                         done = true;
                         success();
                         script.onload = script.onreadystatechange = null;
                         head.removeChild(script);
                     }
                 };
                 head.appendChild(script);
             }
             getScript('<%= ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/js/jquery-custom-v1.js', function () {
                 getScript('/App_ClientControls/tiny_mce/tiny_mce.js', function () {
                    $(function () { $("#tabs").tabs({ cookie: { expires: 30} }); });
                    function SelectTab(tabName) { $("#tabs").tabs('select', tabName); }
                    tinyMCE.init({
                        theme: "advanced",
                        mode: "textareas",
                        forced_root_block: ""
                    });
        })
             });
         })();
	</script>
    
</asp:Content>