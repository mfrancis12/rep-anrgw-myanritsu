﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.App_Features.HtmlContent
{
    public partial class EditHtmlContent : App_Lib.UI.BP_FeatureContentEdit
    {
        protected override void OnInit(EventArgs e)
        {
            //if (!IsPostBack)
            {
                String classKey = GetModuleData(false).ResourceClassKey;
                //Example: https://my.anritsu.com/siteadmin/contentadmin/contenthome?srhmd=true&srhclass=STCTRL_ResourceLocalizationCtrl
                hlLocalizations.NavigateUrl = String.Format("{0}?{1}=true&{2}={3}", KeyDef.UrlList.SiteAdminPages.AdminContentSearch
                    , KeyDef.QSKeys.AdminContentSearch_Auto
                    , KeyDef.QSKeys.AdminContentSearch_PreClassKey
                    , classKey);
            }


            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}