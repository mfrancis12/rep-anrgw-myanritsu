﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Features.HtmlContent
{
    public partial class HtmlCtrl : App_Lib.UI.FeatureCtrlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadContent();
        }

        protected void LoadContent()
        {
            if (ModulePageData == null) return;
            String classKey = ModulePageData.ModuleInfo.ResourceClassKey;
            ltrHtml.Text = HttpUtility.HtmlDecode(HttpContext.GetGlobalResourceObject(classKey, "HtmlContent").ToString());
        }
    }
}