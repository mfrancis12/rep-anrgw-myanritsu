﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Anritsu.Connect.Lib.Cfg;


namespace Anritsu.Connect.Web.App_Handlers
{
    /// <summary>
    /// Summary description for GetModels
    /// </summary>
    public class GetModels : IHttpHandler
    {
        /// <summary>
        /// Get models
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest Request = context.Request;
            if (Request.HttpMethod == "GET")
                context.Response.Write("Get requests are not accepted");
            else if(Request.HttpMethod == "POST")
            {
                String qry = HttpContext.Current.Request["searchqry"];
                String type = String.IsNullOrEmpty(HttpContext.Current.Request["type"])
                    ? ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
                    : HttpContext.Current.Request["type"];
                //assign default type
                if (string.IsNullOrWhiteSpace(qry))
                {
                    context.Response.Write("Please Pass Searchqry");
                    context.Response.End();
                }
                else
                {
                    context.Response.Write(DoSearch(qry, type));
                    context.Response.End();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private String DoSearch(String searchQry, String configType)
        {
            String modelNumberSearch = searchQry.Trim();
            string json = string.Empty;
            if (!modelNumberSearch.Contains("%") && !modelNumberSearch.Contains("--"))
            {
                DataTable dataFound = Lib.Cfg.ModelConfigBLL.SelectByModelAndConfigType(modelNumberSearch, false, configType);
                JavaScriptSerializer js = new JavaScriptSerializer();
                json = GetJson(dataFound);

                if (dataFound != null && dataFound.Rows.Count > 0)
                {

                }
            }
            return json;
        }
        public string GetJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new

            System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
              new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "ModelNumber")
                        row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}