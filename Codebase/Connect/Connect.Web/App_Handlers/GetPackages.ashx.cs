﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;


namespace Anritsu.Connect.Web.App_Handlers
{
    /// <summary>
    /// Summary description for GetModels
    /// </summary>
    public class GetPackages : IHttpHandler
    {
        /// <summary>
        /// Get models
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest Request = context.Request;
            if (Request.HttpMethod == "GET")
                context.Response.Write("Post requests are accepted");
            else if(Request.HttpMethod == "POST")
            {
                String qry = HttpContext.Current.Request["searchqry"];
                String accid = HttpContext.Current.Request["accid"];
                Guid accno;
                if (string.IsNullOrWhiteSpace(qry) || string.IsNullOrWhiteSpace(accid) || !Guid.TryParse(accid,out accno))
                {
                    context.Response.Write("Please Pass Valida Data");
                    context.Response.End();
                }
                else
                {
                    context.Response.Write(DoSearch(qry, accno));
                    context.Response.End();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private String DoSearch(String searchQry, Guid accid)
        {
            String packageName = searchQry.Trim();
            string json = string.Empty;
            if (!String.IsNullOrEmpty(packageName))
            {
                DataTable dataFound = Lib.Package.PackageBLL.SelectAllActivePackages(packageName, accid);
                JavaScriptSerializer js = new JavaScriptSerializer();
                json = GetJson(dataFound);

                if (dataFound != null && dataFound.Rows.Count > 0)
                {

                }
            }
            return json;
        }
        public string GetJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new

            System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
              new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName == "PackageName" || col.ColumnName == "PackageID")
                        row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }
    }
}