﻿using Amazon.CloudFront;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.AWS;
using Anritsu.Connect.Web.App_Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Handlers
{
    /// <summary>
    /// Summary description for SISDocumentHandler
    /// </summary>
    public class PIDDocumentHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            var filePath = ConvertUtility.ConvertToString(context.Request.QueryString[KeyDef.QSKeys.TargetURL], string.Empty);
            var cloudFrontKey = AWSUtility_Settings.PID_KeyPairID_Get();
            var cloudFrontUrl = AWSUtility_Settings.PID_BaseUrlHost_Get();
            var privateKeyFile = new FileInfo(HttpContext.Current.Server.MapPath(
                                    ConfigUtility.AppSettingGetValue("PID.AWS.CloudFront.PEM")));
            var expiryTimeOutInMinutes = ConvertUtility.ConvertToInt32(
                            ConfigUtility.AppSettingGetValue("PID.AWS.S3.SignedCookieTimeOutInMinutes"), 10);
            context.Response.Redirect(GetAwsSignedUrl(cloudFrontUrl, privateKeyFile, filePath, 
                cloudFrontKey, expiryTimeOutInMinutes));
            context.Response.End();
        }

        public static string GetAwsSignedUrl(string cloudFrontUrl, FileInfo privateKeyFile, string downloadFilePath,
           string cloudFrontKey, int expiryTimeOutInMinutes)
        {
            string signedUrl = AmazonCloudFrontUrlSigner.GetCannedSignedURL(
                AmazonCloudFrontUrlSigner.Protocol.https,
                new Uri(cloudFrontUrl).Host,
                privateKeyFile,
                downloadFilePath,
                cloudFrontKey,
                DateTime.Now.AddMinutes(expiryTimeOutInMinutes));

            return signedUrl;
        }

        private void RedirectToCustomError(HttpContext context)
        {
            context.Response.StatusCode = 403;
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}