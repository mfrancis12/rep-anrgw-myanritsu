﻿/*
 * Author:        kishore kumar M
 * Created Date:  24/07/2015
 * Modified Date: 
 * Modified By:   kishore kumar M
 * Purpose:       Genrates signed cookies for the AWS s3 objects and redirects to s3 location
 */
using Amazon.CloudFront;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.AWS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Handlers
{
    /// <summary>
    /// Summary description for PrivateDownloadHandler
    /// </summary>
    public class PrivateDownloadHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    {
        private string DownloadSource
        {
            get
            {
                return
                    ConvertUtility.ConvertToString(
                        HttpContext.Current.Request.QueryString[KeyDef.QSKeys.DownloadSource], String.Empty);
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            var session = context.Session;
            //construct allowed download source list
            var allowedSources = new List<String>()
            {
                ModelConfigTypeInfo.CONFIGTYPE_JP,
                ModelConfigTypeInfo.CONFIGTYPE_UK_ESD
            };
            if (session == null || session[KeyDef.SSKeys.PrivateDownloads] == null || string.IsNullOrEmpty(DownloadSource) || (!allowedSources.Contains(DownloadSource))) RedirectToCustomError(context);
            var filePath = ConvertUtility.ConvertToString(context.Request.QueryString[KeyDef.QSKeys.TargetURL], string.Empty);
            var downlaodPaths = ((List<string>)session[KeyDef.SSKeys.PrivateDownloads]);
            if (downlaodPaths == null || string.IsNullOrEmpty(filePath)) RedirectToCustomError(context);
           
            if (!downlaodPaths.Contains(filePath)) RedirectToCustomError(context);

            switch (DownloadSource)
            {
                case ModelConfigTypeInfo.CONFIGTYPE_JP:
                    var cloudFrontKey = AWSUtility_Settings.JP_KeyPairID_Get();
                    // The RSA key pair file (.pem file) that contains the private key    
                    var privateKeyFile = new FileInfo(HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("JPDL.AWS.CloudFront.PEM")));

                    var cloudFrontUrl = String.Format("{0}/{1}", AWSUtility_Settings.JP_BaseUrlHost_Get(), filePath);
                    HandleCloudFrontSignedCookie(cloudFrontKey, privateKeyFile, cloudFrontUrl,
                        ConvertUtility.ConvertToInt32(
                            ConfigUtility.AppSettingGetValue("JPDL.AWS.S3.SignedCookieTimeOut"), 1200), context);
                    break;
                case ModelConfigTypeInfo.CONFIGTYPE_UK_ESD:
                    var ukcloudFrontKey = AWSUtility_Settings.TAU_KeyPairID_Get();
                    // The RSA key pair file (.pem file) that contains the private key    
                    var ukprivateKeyFile = new FileInfo(HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("TAUDL.AWS.CloudFront.PEM")));
                    context.Response.Write(ukprivateKeyFile);
                    var ukcloudFrontUrl = String.Format("{0}/{1}", AWSUtility_Settings.TAU_BaseUrlHost_Get(), filePath);
                    HandleCloudFrontSignedCookie(ukcloudFrontKey, ukprivateKeyFile, ukcloudFrontUrl,
                        ConvertUtility.ConvertToInt32(
                            ConfigUtility.AppSettingGetValue("TAU.AWS.S3.SignedCookieTimeOut"), 1200), context);
                    break;
                default:
                    RedirectToCustomError(context);
                    break;
            }
        }

        private void HandleCloudFrontSignedCookie(string cloudFrontKey, FileInfo privateKeyFile, string cloudFrontUrl, int timeOut, HttpContext context)
        {
            var cookies = AmazonCloudFrontCookieSigner.GetCookiesForCannedPolicy(
                   HttpUtility.UrlPathEncode(cloudFrontUrl),
                   cloudFrontKey,
                   privateKeyFile,
                   DateTime.Now.AddSeconds(timeOut)); // Time until which the signed cookies are valid
            // Set signed cookies for precanned policies
            context.Response.Cookies.Add(new HttpCookie(cookies.Expires.Key, cookies.Expires.Value) { Domain = "anritsu.com" });
            context.Response.Cookies.Add(new HttpCookie(cookies.Signature.Key, cookies.Signature.Value) { Domain = "anritsu.com" });
            context.Response.Cookies.Add(new HttpCookie(cookies.KeyPairId.Key, cookies.KeyPairId.Value) { Domain = "anritsu.com" });
            context.Response.Redirect(cloudFrontUrl);
            context.Response.End();
        }

        private void RedirectToCustomError(HttpContext context)
        {
            context.Response.StatusCode = 403;
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}