function ajaxMethod(url, type, data){
	$.ajax({
		url: url, 
		type: type,
		data: data, 
		contentType: "application/json; charset=utf-8",
        dataType: "text",
        success: function (msg) { },
        error: function (e) { alert("An Error Occured: " + e.statusText); }
	});
}