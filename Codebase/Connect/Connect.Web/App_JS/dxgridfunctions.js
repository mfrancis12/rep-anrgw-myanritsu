﻿function Grid_ToggleGroupRow(grid, idx) {
    if (grid == null) return;
    if (grid.IsGroupRow(idx)) {
        if (grid.IsGroupRowExpanded(idx))
            grid.CollapseRow(idx);
        else
            grid.ExpandRow(idx);
    }
}
var isDetailRowExpanded = new Array();
function OnRowClick(s, e) {

    if (isDetailRowExpanded[e.visibleIndex] != true)
        s.ExpandDetailRow(e.visibleIndex);
    else
        s.CollapseDetailRow(e.visibleIndex);
}
function OnDetailRowExpanding(s, e) {
    isDetailRowExpanded[e.visibleIndex] = true;
}
function OnDetailRowCollapsing(s, e) {
    isDetailRowExpanded[e.visibleIndex] = false;
}
