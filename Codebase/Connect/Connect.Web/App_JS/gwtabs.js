﻿/*var tabs = new Array("tab01", "tab02", "tab03", "tab04", "tab05");*/

function selectTab(selected) {
    if (!document.getElementById) return;
    for (i = 0; i <= tabs.length - 1; i++) {
        if (!document.getElementById(tabs[i])) continue;
        if (tabs[i] == selected) {
            document.getElementById("tab" + tabs[i]).className = "tabnameselected";
            document.getElementById(tabs[i]).style.display = "block";
            if (document.getElementById("hfTab"))
                document.getElementById("hfTab").value = selected;

        } else {
            document.getElementById("tab" + tabs[i]).className = "tabname";
            document.getElementById(tabs[i]).style.display = "none";
        }
        document.getElementById(tabs[i]).style.borderBottom = "none";
    }
}

function load() {
    if (!document.getElementById) return;
    for (i = 0; i <= tabs.length - 1; i++) {
        if (!document.getElementById(tabs[i])) continue;

        var nodes = document.getElementById(tabs[i]).childNodes;
        for (var j = 0; j < nodes.length; j++) {
            if (nodes[j].className == "tabtitle") {
                nodes[j].style.display = "none";
            }
        }

        var linkButton = document.getElementById(tabs[i] + "button");
        if (linkButton) {
            linkButton.style.display = "none";
        }
    }
}

function changeButton(selected) {
    if (!document.getElementById) return;
    var body = document.getElementById("tabwindowitems");
    var buttonArea = document.getElementById("tabwindowbuttonarea");
    var button = document.getElementById("tabwindowbutton");
    var footer = document.getElementById("tabwindowfooter");
    var link = document.getElementById("tabwindowbuttonlink");
    if (!body || !buttonArea || !button || !footer) return;
    var visible = (selected == "tab03" || selected == "tab04" || selected == "tab05") ? true : false;
    buttonArea.style.display = (visible) ? "block" : "none";
    body.style.height = (visible) ? "151px" : "181px";
    
    var buttonText = "";
    switch (selected) {
        case "tab03":
            buttonText = "Go To MediaRoom";
            break;
        case "tab04":
            buttonText = "News Archives";
            break;
        case "tab05":
            buttonText = "More Information";
            break;
        default:
            break;
    }
    link.firstChild.nodeValue = buttonText;
    link.setAttribute("title", buttonText);
}

function loadTabs() {
    load();
    selectTab('tab01');
    changeButton('tab01');
}
function GetQueryString() {
    var qsParm = new Array();
    var query = window.location.search.substring(1);
    var parms = query.split('&');
    for (var i = 0; i < parms.length; i++) {
        var pos = parms[i].indexOf('=');
        if (pos > 0) {
            var key = parms[i].substring(0, pos);
            var val = parms[i].substring(pos + 1);
            qsParm[key] = val;
        }
    }
    return qsParm;
} 
/*
function addLoadEvent() { load(); var qs = GetQueryString(); selectTab(qs["tab"] != null ? qs["tab"] : document.getElementById('hfTab').value); }; */