﻿"use strict";
var app = angular.module('myAnristuApp', ['ui.bootstrap', 'ngSanitize']);
app.controller('mycontroller', function ($scope, locationService) {
    $scope.modelKey = '';
    $scope.modelData = {};
    $scope.modelFilterList = '';
    $scope.functionKey = '';
    $scope.functionData = {};
    $scope.functionFilterList = '';
    $scope.optionData = {};
    $scope.optionKey = '';
    $scope.optionFilterList = '';
    $scope.resultData = '';
    $scope.tableData = '';
    $scope.loading = true;
    $scope.itemsPerPage = 0;
    $scope.errorMessage = '';
    $scope.productTitle = false;
    $scope.paginationLinks = false;
    $scope.paginationEle = true;
    $scope.maxSize = 20;
    $scope.localResultData = {};
    $scope.functionFilters = true;
    $scope.optionFilters = true;
    $scope.modelDataEmpty = true;

    locationService.getModel().then(function (d) {
        $scope.loading = true;
        $scope.modelFilterList = d.data;
        $scope.errorMessage = '';
        $scope.loading = false;
        $scope.modelDataEmpty = false;
    }, function () {
        $scope.modelDataEmpty = true;
    });

    $scope.getFunction = function () {
        $scope.loading = true;
        $scope.functionKey = '';
        $scope.optionKey = '';
        $scope.functionFilterList = '';
        $scope.optionFilterList = '';
        if ($scope.errorMessage) {
            $scope.errorMessage = $scope.errorMessage;
        }
        $scope.modelKey = ($scope.modelData && $scope.modelData.ModelFilter) || '';
        if ($scope.modelKey) {
            locationService.getFunction($scope.modelKey).then(function (d) {
                $scope.functionFilterList = d.data;
                $scope.loading = false;
                $scope.functionFilters = false;

            }, function (res) {
                if (res.data.ErrorMessage) {
                    $scope.loading = false;
                }
                if (res.status === 404) {
                    $scope.functionFilters = true;
                }
                if (res.status === 401) {
                    window.location = window.location.hostname;
                }
            });
        }
        else {
            $scope.resetValues();
            $scope.functionFilters = true;
        }
    }

    $scope.getOption = function () {
        $scope.loading = true;
        $scope.optionKey = '';
        $scope.optionFilterList = '';
        if ($scope.errorMessage) {
            $scope.errorMessage = $scope.errorMessage;
        }
        $scope.functionKey = ($scope.functionData && $scope.functionData.FunctionFilter) || '';
        if ($scope.functionKey) {
            locationService.getOption($scope.modelKey, $scope.functionKey).then(function (d) {
                $scope.optionFilterList = d.data;
                $scope.loading = false;
                $scope.optionFilters = false;
            }, function (res) {
                if (res.data.ErrorMessage) {
                    $scope.loading = false;
                }
                if (res.status === 404) {
                    $scope.optionFilters = true;
                }
                if (res.status === 401) {
                    window.location = window.location.hostname;
                }
            });
        }
        else {
            $scope.optionFilters = true;
            $scope.loading = false;
        }
    }

    $scope.getOptionResults = function () {
        $scope.loading = true;
        if ($scope.errorMessage) {
            $scope.errorMessage = $scope.errorMessage;
        }
        $scope.optionKey = ($scope.optionData && $scope.optionData.OptionFilter) || '';
        if ($scope.optionKey) {
            locationService.getOptionResults($scope.modelKey, $scope.functionKey, $scope.optionKey).then(function (d) {
                $scope.loading = false;
            }, function (res) {
                if (res.data.ErrorMessage) {
                    $scope.loading = false;
                }
                if (res.status === 401) {
                    window.location = window.location.hostname;
                }
            });
        } else {
            $scope.loading = false;
        }
    }

    $scope.resetValues = function (e) {
        $scope.loading = true;
        if (e) {
            e.preventDefault();
        }
        $scope.productTitle = true;
        $scope.paginationLinks = true;
        $scope.resultData = '';
        $scope.modelData = {};
        $scope.functionKey = '';
        $scope.optionKey = '';
        $scope.functionFilterList = '';
        $scope.optionFilterList = '';
        $scope.errorMessage = '';
        $scope.tableData = $scope.localResultData;
        $scope.setResultData();
        $scope.functionFilters = true;
        $scope.optionFilters = true;
        $scope.updateModelFilterSelection = false;
        $scope.updateFunctionFilterSelection = false;
        $scope.updateOptionFilterSelection = false;
        $(".my-anristu-breadcrum > *").hide();
    };

    $scope.setResultData = function () {
        $scope.totalItems = $scope.tableData.length;
        $scope.itemsPerPage = 20;
        $scope.currentPage = 1;
        $scope.$watch('currentPage + itemsPerPage', function () {
            var begin = (($scope.currentPage - 1) * $scope.itemsPerPage),
                end = begin + $scope.itemsPerPage;
            $scope.resultData = $scope.tableData.slice(begin, end);
            $scope.loading = false;
            $scope.productTitle = false;
            $scope.paginationLinks = false;
            $scope.paginationEle = false;
        });
    };

    $scope.loadGridResults = function () {
        $scope.tableData = productList;
        $scope.localResultData = productList;
        if (productList.ErrorMessage) {
            $scope.errorMessage = productList.ErrorMessage;
            $scope.loading = false;
            return false;
        }
        $scope.setResultData();

    }

    $scope.loadGridResults();

    $scope.showResult = function (e) {
        $scope.loading = true;
        if (e) {
            e.preventDefault();
        }
        locationService.getTable($scope.modelKey, $scope.functionKey, $scope.optionKey).then(function (d) {
            $scope.errorMessage = '';
            $scope.tableData = d.data;
            if (d.data.ErrorMessage) {
                $scope.errorMessage = d.data.ErrorMessage;
                $scope.loading = false;
                return false;
            } else {
                    if ($scope.modelKey && $scope.modelKey != '') {
                        $(".select-filter-txt").show();
                        $(".view-system-model").show().text($(".select-model-filter .dropdown-menu.inner li.selected:not(:first-child)").find("a span.text").text());
                    }
                    else {
                        $(".view-system-model").hide().text("");
                    }
                    if ($scope.functionKey && $scope.functionKey != '') {
                        $(".view-function").show().text($(".select-function-filter .dropdown-menu.inner li.selected:not(:first-child)").find("a span.text").text());
                    }
                    else {
                        $(".view-function").hide().text("");
                    }
                    if ($scope.optionKey && $scope.optionKey != '') {
                        $(".view-option").show().text($(".select-option-filter .dropdown-menu.inner li.selected:not(:first-child)").find("a span.text").text());
                    }
                    else {
                        $(".view-option").hide().text("");
                    }
                
                $scope.setResultData();
            }
        }, function (res) {
            $scope.loading = false;
            $scope.errorMessage = '';
            if (res.data.ErrorMessage) {
                $scope.errorMessage = res.data.ErrorMessage;
            }
        });
    }

    $scope.pageChanged = function () {
        setTimeout(function () {
            $("html, body").stop().animate({
                scrollTop: $('.pagination-wrapper').offset().top
            }, 0);
            return false;
        }, 100);
    }
    $scope.clearFilters = function (e) {
        $scope.loading = true;
        if (e) {
            e.preventDefault();
        }
        $scope.resetValues();
    }

    $scope.$watch(function () {
        $('.custom-select').selectpicker('refresh');
    });

    $(document).on("click", ".bootstrap-select > .dropdown-toggle", function (e) {
        e.stopPropagation();
        $("div.dropdown-menu").hide();
        $(this).parent(".bootstrap-select").find(".dropdown-menu").toggle();
    });

    $(document).click(function () {
        $(".dropdown-menu").hide();
    });

}).factory('locationService', function ($http) {
    var fac = {},
    returnVal = '',
    apiUrl = '/api/myproduct/';
    fac.getModel = function () {
        return $http.get(apiUrl + 'GetProductFilters');
    }
    fac.getFunction = function (modelKey) {
        return $http.get(apiUrl + 'GetProductFilters?mk=' + escape(modelKey));
    }
    fac.getOption = function (modelKey, functionKey) {
        return $http.get(apiUrl + 'GetProductFilters?mk=' + escape(modelKey) + '&fk=' + escape(functionKey));
    }
    fac.getOptionResults = function (modelKey, functionKey, optionKey) {
        return $http.get(apiUrl + 'GetProductFilters?mk=' + escape(modelKey) + '&fk=' + escape(functionKey) + '&ok=' + escape(optionKey));
    }
    fac.getTable = function (modelKey, functionKey, optionKey) {

        if (modelKey !== '') {
            returnVal = $http.get(apiUrl + 'GetMyProducts?mk=' + escape(modelKey));
            if (functionKey && !functionKey == '') {
                returnVal = $http.get(apiUrl + 'GetMyProducts?mk=' + escape(modelKey) + '&fk=' + escape(functionKey));
                if (optionKey && !optionKey == '') {
                    returnVal = $http.get(apiUrl + 'GetMyProducts?mk=' + escape(modelKey) + '&fk=' + escape(functionKey) + '&ok=' + escape(optionKey));
                }
            }
        } else {
            returnVal = $http.get(apiUrl + 'GetMyProducts');
        }
        return returnVal;
    }
    return fac;
});