﻿var StringyfiedContent = '';
$(function () {
    var jsonUrl = "/App_Handlers/GetModels.ashx";
    $("#model>input[type=text]").tokenInput(jsonUrl,
        {
            preventDuplicates: true,
            prePopulate: getPrepopulateModels(),
            method: "POST",
            queryParam: "searchqry",
            contentType: 'json',
            minChars: 3,
            hintText: 'Enter atleast 3 characters of a model',
            onAdd: function () {
                validateTokenizer('token-validator');
            },
            onDelete: function () {
                validateTokenizer('token-validator');
            }
        });
    $("#model input[type=text]").blur(function () {
        var selectedModels = $('#model>input[type=text]').tokenInput("get");
        var sendSelectedModels = [];
        StringyfiedContent = '';
        $.each(selectedModels, function (index, value) {
            if (index < (selectedModels.length - 1))
                StringyfiedContent = StringyfiedContent + value.ModelNumber + ';';
            else
                StringyfiedContent = StringyfiedContent + value.ModelNumber;
        });
        $("#model input[type=hidden]").val(StringyfiedContent);
    })
    activateActiveCheckbox();
})

getPrepopulateModels = function () {
    var models = $("#model input[type=hidden]").val();
    var prepopulated = [];
    if (models.length > 0) {
        var modelsArry = models.split(';');
        for (var i = 0; i < modelsArry.length; i++) {
            var modelObj = new Model(modelsArry[i]);
            prepopulated.push(modelObj);
        }
    }
    return prepopulated;
}

Model = function (ModelNumber) {
    this.ModelNumber = ModelNumber;
}

validateTokenizer = function (id) {
    var IsSelected = $('#model>input[type=text]').tokenInput("get");
    if (IsSelected.length <= 0) {
        $("#" + id).css('display', 'block');
        $("#" + CHECKBOXID).prop("checked", false);
        $("#" + CHECKBOXID).prop("disabled", true);
        return false;
    }
    else {
        $("#" + id).css('display', 'none');
        //$("#" + CHECKBOXID).prop("checked", false);
        $("#" + CHECKBOXID).prop("disabled", false);
        return true;
    }

}

function activateActiveCheckbox() {
    var IsSelected = $('#model .token-input-token');
    if (IsSelected.length <= 0) {
        $("#" + CHECKBOXID).prop("disabled", true);
    }
    else {
        $("#" + CHECKBOXID).prop("disabled", false);
    }
}

