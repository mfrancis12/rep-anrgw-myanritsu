var StringyfiedContent = '';
$(function () {
    var jsonUrl = "/App_Handlers/GetPackages.ashx";
    $("#model>input[type=text]").tokenInput(jsonUrl,
        {
            propertyToSearch: "PackageName",
            tokenValue: "PackageID",
            preventDuplicates: true,
            method: "POST",
            queryParam: "searchqry",
            contentType: 'json',
            minChars: 3,
            hintText: 'Enter atleast 3 characters of an active package',
            tokenLimit: 1,
            onAdd: function () {
                validateTokenizer('token-validator');
            }
        });


    $("#model input[type=text]").blur(function () {
        var selectedModels = $('#model>input[type=text]').tokenInput("get");
        var sendSelectedModels = [];
        StringyfiedContent = '';
        $.each(selectedModels,function(index,value)
        {
            if(index < (selectedModels.length - 1))
                StringyfiedContent = StringyfiedContent + value.PackageID + ';';
            else
                StringyfiedContent = StringyfiedContent + value.PackageID;
        });
        $("#model input[type=hidden]").val(StringyfiedContent);
        $('#model>input[type=text]').val(selectedModels[0].PackageName);
    })
})

validateTokenizer = function (id) {
    var IsSelected = $('#model>input[type=text]').tokenInput("get");
    if (IsSelected.length <= 0) {
        $("#" + id).css('display', 'block');
        return false;
    }
    else {
        $("#" + id).css('display', 'none');
        return true;
    }

}


