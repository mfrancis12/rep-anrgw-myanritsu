
/*Start of script in SupportType_ManageCtrl */

var objOrganization = new Object;
var objEmail = new Object;

function loadSuggestions(suggestions) {
    $.ajax({
        url: '/api/ProductSupportType/ModelNumber/?param=' + suggestions + '&modelConfigType=downloads-JP',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onSuggestionsLoaded,
        error: function (e) {
            alert(e);
        }
    });
}
function loadEditSuggestions(suggestions) {

    $.ajax({
        url: '/api/ProductSupportType/ModelNumber/?param=' + suggestions + '&modelConfigType=downloads-JP',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onEditSuggestionsLoaded,
        error: function (e) {
            alert(e);
        }
    });
}

function loadEmailEditSuggestions(suggestions) {
    $.ajax({
        url: '/api/ProductSupportType/EmailAddresses/?param=' + suggestions,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onEmailEditSuggestionsLoaded,
        error: function (e) {
            alert(e);
        }
    });
}
function onEmailEditSuggestionsLoaded(data) {
    $(function () {
        var emailListArray = [];
        objEmail = JSON.parse(data);
        for (i = 0; i < objEmail.length; i++) {
            emailListArray.push({
                label: objEmail[i].EmailAddress,
                id: objEmail[i].Id
            });
        }
        $("input[id*='dxtxtEmailOrg']").autocomplete({
            source: emailListArray,
            minLength: 3,
            select: function (event, data) {
                $(this).autocomplete('enable'),
               $("input[id*='dxMemId']").val(data.item.id);
                $("input[id*='dxMemId']").text(data.item.id);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(this).val("");
                }
            }
        });
        $("input[id*='dxtxtEmailOrg']").focus(function () {
            $(this).autocomplete("search", $(this).val());
        });
    });
}
function onEmailSuggestionsLoaded(data) {
    $(function () {
        var emailListArray = [];
        objEmail = JSON.parse(data);
        for (i = 0; i < objEmail.length; i++) {
            emailListArray[i] = objEmail[i].EmailAddress;
        }
        $("#cphContentTop_SupportType_ManageCtrl1_txtEmOrg").autocomplete({
            source: emailListArray,
            minLength: 3,
            select: function (event, data) {
                $(this).autocomplete('enable');
            }
        });
        $("#cphContentTop_SupportType_ManageCtrl1_txtEmOrg").focus(function () {
            $(this).autocomplete("search", $(this).val());
        });
    });
}
function loadEmailSuggestions(emailSuggestions) {
    //Get suggestions only if input is valid
    //loadSuggestions($(this).val());
    $.ajax({
        url: '/api/ProductSupportType/EmailAddresses/?param=' + emailSuggestions,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onEmailSuggestionsLoaded,
        error: function (e) {
            alert(e);
        }
    });
}

function onOrganizationSuggestionsLoaded(data) {

    $(function () {
        var organizationListArray = [];
        objOrganization = JSON.parse(data);
        for (i = 0; i < objOrganization.length; i++) {
            organizationListArray[i] = objOrganization[i].OrganizationName;
        }
        $("#cphContentTop_SupportType_ManageCtrl1_txtEmOrg").autocomplete({
            source: organizationListArray,
            minLength: 3,

            select: function (event, data) {
                $(this).autocomplete('enable');
            }
        });
        $("#cphContentTop_SupportType_ManageCtrl1_txtEmOrg").focus(function () {
            $(this).autocomplete("search", $(this).val());
        });
    });
}
function loadOrganizationSuggestions(organizationalSuggest) {

    $.ajax({
        url: '/api/ProductSupportType/Organizations/?param=' + organizationalSuggest,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onOrganizationSuggestionsLoaded,
        error: function (e) {
            console.log(e.error);
        }
    });

}
function onEditOrganizationSuggestionsLoaded(data) {
    $(function () {
        var organizationListArray = [];
        var objOrg = JSON.parse(data);
        for (i = 0; i < objOrg.length; i++) {
            organizationListArray.push(
                {
                    label: objOrg[i].OrganizationName,
                    id: objOrg[i].OrganizationId,
                }
                );
        }
        $("input[id*='dxtxtEmailOrg']").autocomplete({
            source: organizationListArray,
            minLength: 3,
            select: function (event, data) {
                $(this).autocomplete('enable');
                $("input[id*='dxAcctId']").val(data.item.id);
                $("input[id*='dxAcctId']").text(data.item.id);
            },
            change: function (event, ui) {
                if (!ui.item) {
                    $(this).val("");
                }
            }
        });
        $("input[id*='dxtxtEmailOrg']").focus(function () {
            $(this).autocomplete("search", $(this).val());
        });
    });

}
function onEditOrganizationSuggestions(organizationalSuggest) {
    $.ajax({
        url: '/api/ProductSupportType/Organizations/?param=' + organizationalSuggest,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: onEditOrganizationSuggestionsLoaded,
        error: function (e) {
            console.log(e.error);
        }
    });

}

$(document).ready(function () {
    $(".date-expire .dxeEditAreaSys").attr("Placeholder", "Select expiration date");


    //fix for IE7 and IE8
    var browser = navigator.appVersion;
    if (browser.indexOf("MSIE") != -1 && (browser.indexOf("9.0") != -1 || browser.indexOf("8.0") != -1 || browser.indexOf("7.0"))) {
        addPlaceHolder();
    }
    function addPlaceHolder() {
        $("input[placeholder], select[placeholder]").each(function () {
            if ($(this).val() == "") {
                $(this).addClass("gray");
                $(this).val($(this).attr("placeholder"));
            }
        });

        $("input[placeholder], select[placeholder]").focus(function () {
            if ($(this).val() == $(this).attr("placeholder")) {
                $(this).removeClass("gray");
                $(this).val("");
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).addClass("gray");
                $(this).val($(this).attr("placeholder"));
            }
        }).blur();
    }
    function removePlaceHolder(that) {
        that.find("input[placeholder], select[placeholder]").each(function () {
            if ($(this).val() == $(this).attr("placeholder")) {
                $(this).removeClass("gray");
                $(this).val("");
            }
        });
    }

    $("input[placeholder], select[placeholder]").parents("form").submit(function () {
        if (browser.indexOf("MSIE") != -1 && (browser.indexOf("9.0") != -1 || browser.indexOf("8.0") != -1 || browser.indexOf("7.0"))) {
            removePlaceHolder($(this));
            if ($(this).find(".msg").length > 0) {
                addPlaceHolder();
            }
        }

    });
    try {
        var mdlArr = [];
        var mdlSel; var snSel;
        if (supportFlag) {
            $(txtModelNumber).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '/api/ProductSupportType/ModelNumber/?param=' + $(txtModelNumber).val() + '&modelConfigType=downloads-JP',
                        dataType: 'json',
                        type: 'GET',
                        success: function (data) {
                            var d = JSON.parse(data);
                            mdlArr = d;
                            response($.map(d, function (item) {
                               // $(txtModelNumber).val(item);
                                return {
                                    value: item

                                }
                            }));

                        },
                        error: function (error) {

                        }
                    })
                },
                minLength: 2,
                select: function (event, data) {
                    $(this).autocomplete('enable');
                    mdlSel = data.item.label;
                    $(txtSerialNumber).val("");

                }

            });
            $(txtModelNumber).change(function () {
                //if (mdlArr.indexOf($(this).val()) < 0) {
                //    $(this).val("");
                //}
                if ($.inArray($(this).val(), mdlArr) < 0) {
                    $(this).val("");
                }

            });
           $(txtModelNumber).focusout(function () {
               if ($(this).val() != mdlSel) {
                   //$('#<%=txtSerialNumber.ClientID%>').val("");
               } else {
                   $(txtModelNumber).val(mdlSel);
               }
           });
            var sn = [];

            $(txtSerialNumber).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '/api/ProductSupportType/GetSerialNumber/?model=' + $(txtModelNumber).val() + '&serial=' + $(txtSerialNumber).val(),
                        dataType: 'json',
                        type: 'GET',
                        success: function (data) {
                            var d = JSON.parse(data);
                            sn = d;
                            response($.map(d, function (item) {
                                return {
                                    value: item

                                }
                            }))
                        },
                        error: function (error) {

                        }
                    })
                },
                mustMatch: true,
                select: function (event, data) {
                    $(this).autocomplete('enable');
                    snSel = data.item.label;
                }

            });

            //$(txtSerialNumber).change(function () {
            //    //if (sn.indexOf($(this).val()) < 0) {
            //    //    $(this).val("");
            //    //}
            //    if ($.inArray($(this).val(), sn) < 0) {
            //        $(this).val("");
            //    }
            //});

            var orgArr = [];
            var emArr = [];
            var selEm;
            $(txtEmOrg).autocomplete({
                source: function (request, response) {
                    if ($(ddlSelType + ' option:selected').index() == 1) {
                        $.ajax({
                            url: '/api/ProductSupportType/EmailAddresses/?param=' + $(txtEmOrg).val(),
                            dataType: 'json',
                            type: 'GET',
                            success: function (data) {
                                var d = JSON.parse(data);
                                emArr = d;
                                response($.map(d, function (item) {
                                    return {
                                        value: item.EmailAddress,
                                        id: item.Id
                                    }
                                }))
                            },
                            error: function (error) {
                                response($.map(d, function (item) {

                                }))
                            }
                        })
                    }
                    if ($(ddlSelType + ' option:selected').index() == 2) {
                        $.ajax({
                            url: '/api/ProductSupportType/Organizations/?param=' + $(txtEmOrg).val(),
                            dataType: 'json',
                            type: 'GET',
                            success: function (data) {
                                var d = JSON.parse(data);
                                orgArr = d;
                                response($.map(d, function (item) {
                                    return {
                                        label: item.OrganizationName,
                                        value: item.OrganizationName,
                                        id: item.OrganizationId
                                    }
                                }))
                            },
                            error: function (error) {
                                response($.map(d, function (item) {

                                }))
                            }
                        })


                    }
                    if ($(ddlSelType + ' option:selected').index() == 0) {
                        response($.map(d, function (item) {

                        }));
                    }
                },
                select: function (event, ui) {
                    //$(txtOrgId).text(ui.item.id); 
                    $(txtOrgId).val(ui.item.id);
                    selEm = ui.item.label;
                    $(txtEmOrg).prop("class", "");
                    $(txtEmOrg).prop("class", "ui-autocomplete-input");
                },
                minLength: 3,
                change: function (event, ui) {
                    if (!ui.item) {
                        $(this).val("");
                    }
                }
            });
        }
    } catch (e) {

    }

    $('#cphContentTop_SupportType_ManageCtrl1_ddlSelType').change(function () {
        if ($('#cphContentTop_SupportType_ManageCtrl1_ddlSelType option:selected').text() === 'Email Address') {
            $('#cphContentTop_SupportType_ManageCtrl1_txtEmOrg').attr('value', "");

        }
        else if ($('#cphContentTop_SupportType_ManageCtrl1_ddlSelType option:selected').text() === 'Organization') {
            $('#cphContentTop_SupportType_ManageCtrl1_txtEmOrg').attr('value', "");


        }
        else { }
    });


});
function onkeyup() {
    //loadEditSuggestions($("input[id*='dxlblModelNumber']").val());
    $("input[id*='dxlblModelNumber']").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/api/ProductSupportType/ModelNumber/?param=' + $("input[id*='dxlblModelNumber']").val() + '&modelConfigType=downloads-JP',
                dataType: 'json',
                type: 'GET',
                success: function (data) {
                    var d = JSON.parse(data);
                    response($.map(d, function (item) {
                        return {
                            value: item
                        }
                    }))
                },
                error: function (error) {

                }
            })
        }
      ,
        minLength: 2

    });

}
function onkeyupEmailOrg() {
    if ($("input[id*='ddlSelTypeEdit']").val() == 0) {
        loadEmailEditSuggestions($("input[id*='dxtxtEmailOrg']").val());
    }
    else if ($("input[id*='ddlSelTypeEdit']").val() == 1) {
        onEditOrganizationSuggestions($("input[id*='dxtxtEmailOrg']").val());
    }
    else { }

}
function validate() {
    validateTokenizer('token-validator');
    if (Page_ClientValidate())
        return validateTokenizer('token-validator');
}
var onloadModelValue;
function clearModelBox() {
    onloadModelValue = $("input[id*='dxlblModelNumber']").val();
    $("input[id*='dxlblModelNumber']").attr('value', "");
}
function restoreModelBoxValue() {
    if ($("input[id*='dxlblModelNumber']").val().length == 0) {
        $("input[id*='dxlblModelNumber']").val(onloadModelValue);
    }
}

function dropDownValueChanged() {

    $("input[id*='dxtxtEmailOrg']").attr('value', "");


}
var sn1 = [];
var snSel1;
function onkeyupSerialNumber() {
    $("input[id*='dxtxtSerialNumber']").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/api/ProductSupportType/GetSerialNumber/?model=' + $("input[id*='dxlblModelNumber']").val() + '&serial=' + $("input[id*='dxtxtSerialNumber']").val(),
                dataType: 'json',
                type: 'GET',
                success: function (data) {
                    //alert(data);
                    var d = JSON.parse(data);
                    sn1 = d;
                    response($.map(d, function (item) {
                        return {
                            value: item

                        }
                    }))
                },
                error: function (error) {

                }
            })
        },
        mustMatch: true,
        minLength: 3,
        change: function (event, ui) {
            if (!ui.item) {
                $(this).val("");
            }
        },
        select: function (event, data) {
            $(this).autocomplete('enable');
            snSel1 = data.item.label;
        }
    });
}
var onloadSerialValue;
function clearSerialTxt() {
    onloadSerialValue = $("input[id*='dxtxtSerialNumber']").val();
    $("input[id*='dxtxtSerialNumber']").val("");
}

function restoreSerialBox() {
    if ($("input[id*='dxtxtSerialNumber']").val().length == 0) {
        $("input[id*='dxtxtSerialNumber']").val(onloadSerialValue);
    }

}


/* End of script in SupportType_ManageCtrl */

/*Start of script in PackageAdminDetail_JPCtrl*/

var qs = 'Downloads-JP';
$(function () {
    getLoadValues();
});

getLoadValues = function () {
    try {
        var packagePath = document.getElementById(txtPackagePath);
        packagePath.title = packagePath.value;
    } catch (e) {
    }
}

function browseFiles() {
    var packagePath = document.getElementById(txtPackagePath);
    var packpath = document.getElementById(hfPackagePath);
    if (!window.showModalDialog) {
        window.open("BrowsejpPackage.aspx", 'Anritsu', "height=400,width=500,top=150,left=500");
    } else {
        var retvalue = window.showModalDialog("BrowsejpPackage.aspx", 'Anritsu', "dialogHeight: 400px; dialogWidth: 500px; edge: Raised; help: Yes; resizable: Yes; status: No;dialogLeft:500px;dialogTop:150px");

        if (typeof retvalue === 'undefined') {
            if (typeof window.returnValue === 'undefined')
                packagePath.value = '';
            else {
                packagePath.title = packagePath.value = packpath.value = window.returnValue;
            }
        }
        else
            packagePath.title = packagePath.value = packpath.value = retvalue;
        return false;
    }
}
function filePath(path) {
    var packagePath = document.getElementById(txtPackagePath);
    var packpath = document.getElementById(hfPackagePath);
    if (!path) {
        packagePath.value = '';
    }
    else {
        packagePath.title = packagePath.value = packpath.value = path;
    }
}
function ConfirmOnDeleteOrRemove(deleteMessage) {
    if (confirm(deleteMessage) == true)
        return true;
    else
        return false;
}
function clickButton(e, buttonid) {
    var evt = e ? e : window.event;
    var bt = document.getElementById(buttonid);

    if (bt) {
        if (evt.keyCode == 13) {
            bt.click();
            return false;
        }
    }
}
function validate() {
    validateTokenizer('token-validator');
    if (Page_ClientValidate())
        return validateTokenizer('token-validator');
}

function OnGridSelectionChanged(s, e) {
    if (e.isChangedOnServer == false) {

        SelectAllCheckBox.SetChecked(s.GetSelectedRowCount() == s.GetVisibleRowsOnPage());

    }

}

$(document).ready(function () {
    try {
        if (PackageAdminDetail_JPCtrlFlag) {
            $(txtPkgGrpName).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '/api/ProductSupportType/PackageGroupNames/?grpName=' + $(txtPkgGrpName).val(),
                        dataType: 'json',
                        type: 'GET',
                        success: function (data) {
                            var d = JSON.parse(data);
                            sn = d;
                            response($.map(d, function (item) {
                                return {
                                    value: item

                                }
                            }))
                        },
                        error: function (error) {

                        }
                    })
                },
                minLength: 3,
                select: function (event, data) {
                    $(this).autocomplete('enable');
                }

            });
        }
    } catch (e) {
    }
});

/* End of script in PackageAdminDetail_JPCtrl */

/*Start of script in PackageFileDetailCtrl*/

$(document).ready(function () {
    try {
        var cmbAddNewLang = document.getElementById(cmbAddNewLang);
        if (cmbAddNewLang != null) {
            cmbAddNewLang[0].selected = true;
        }
    } catch (e) {
    }
});
function ConfirmOnDelete(deleteMessage) {
    if (confirm(deleteMessage) == true)
        return true;
    else
        return false;
}
function MutExChkList(chk) {

    var chkList = chk.parentNode.parentNode.parentNode;

    var chks = chkList.getElementsByTagName("input");

    for (var i = 0; i < chks.length; i++) {

        if (chks[i] != chk && chk.checked) {

            chks[i].checked = false;

        }

    }

}

/* End of script in PackageFileDetailCtrl*/

/*Start of script in siteDefault.Master*/

(function () {
    function getScript(url, success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0], done = false;
        script.onload = script.onreadystatechange = function () {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//images.cdn-anritsu.com/apps/connect/js/common-v3.js', function () {
    });
})();

/* End of script in siteDefault.Master*/

