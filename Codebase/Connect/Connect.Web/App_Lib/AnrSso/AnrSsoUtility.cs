﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
using Atp.Saml;
using Atp.Saml.Binding;
using Atp.Saml2;
using Atp.Saml2.Binding;
using System.Data;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Anritsu.Connect.Web.App_Lib.AnrSso
{

    internal static class AnrSsoUtility
    {
        private const String USERSESSIONKEY = "anrssoUser";

        public static X509Certificate2 GetSpCertificate()
        {
            String certFileName = ConfigUtility.AppSettingGetValue("AnrSso.Sp.Cert");
            String certPwd = ConfigUtility.AppSettingGetValue("AnrSso.Sp.CertPwd");
            AppCacheFactories.CertificateCacheFactory certCache = new AppCacheFactories.CertificateCacheFactory();
            return certCache.GetData(certFileName, certPwd);

        }

        public static X509Certificate2 GetIdpCertificate()
        {
            String certFileName = ConfigUtility.AppSettingGetValue("AnrSso.Idp.Cert");
            AppCacheFactories.CertificateCacheFactory certCache = new AppCacheFactories.CertificateCacheFactory();
            return certCache.GetData(certFileName, null);
        }

        public static string GetAbsoluteUrl(Page page, string relativeUrl)
        {
            return new Uri(page.Request.Url, page.ResolveUrl(relativeUrl)).ToString();
        }

        private static AuthnRequest SamlLoginRequest_CreateAuthnRequest(String spResourceUrl)
        {
            // Create some URLs to identify the service provider to the identity provider.
            // As we're using the same endpoint for the different bindings, add a query string parameter to identify the binding.
            string issuerUrl = WebUtility.GetAbsoluteUrl("~/");
            String acsBaseUrl = WebUtility.GetAbsoluteUrl(ConfigUtility.AppSettingGetValue("AnrSso.Sp.AcsUrl"));
            string assertionConsumerServiceUrl = string.Format("{0}?{1}={2}&{3}={4}",
                acsBaseUrl.StartsWith("http://") ? acsBaseUrl.Replace("http://", "https://") : acsBaseUrl,
                KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl)
                );

            // Create the authentication request.
            AuthnRequest authnRequest = new AuthnRequest();
            authnRequest.Destination = ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login");
            authnRequest.Issuer = new Issuer(issuerUrl);
            authnRequest.ForceAuthn = false;
            authnRequest.NameIdPolicy = new NameIdPolicy(null, null, true);
            authnRequest.ProtocolBinding = ConfigUtility.AppSettingGetValue("AnrSso.Sp.SamlBinding.SpToIdp"); // "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
            authnRequest.AssertionConsumerServiceUrl = assertionConsumerServiceUrl;
            X509Certificate2 x509Certificate = GetSpCertificate();
            authnRequest.Sign(x509Certificate);
            return authnRequest;

        }

        public static void SamlLoginRequest_SendHttpPost(String spResourceUrl)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;

            string relayState = Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new TimeSpan(1, 0, 0));
            AuthnRequest authnRequest = SamlLoginRequest_CreateAuthnRequest(spResourceUrl);
            String idpUrl = string.Format("{0}?{1}={2}&{3}={4}",
               ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login"),
               KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl)
               );
            authnRequest.SendHttpPost(context.Response, idpUrl, relayState);
            context.Response.End();
        }

        public static void SamlLoginRequest_SendHttpPost(String spResourceUrl, bool isNew)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;

            string relayState = Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new TimeSpan(1, 0, 0));
            AuthnRequest authnRequest = SamlLoginRequest_CreateAuthnRequest(spResourceUrl);
            String idpUrl;
            var langUri = new Uri(spResourceUrl);

            var lang = langUri.Query.Contains("?") ? langUri.Query.Replace('?', '&') : !string.IsNullOrEmpty(langUri.Query) ? string.Format("&{0}", langUri.Query) : string.Empty;
            if (isNew)
            {
                if (!string.IsNullOrEmpty(lang))
                {
                    idpUrl = string.Format("{0}?{1}={2}{7}&{3}={4}&{5}={6}",
                      ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login"),
                      KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                       KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl),
                        KeyDef.QSKeys.IsNew, "True", lang
                       );
                }
                else
                {
                    idpUrl = string.Format("{0}?{1}={2}&{3}={4}&{5}={6}",
                     ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login"),
                     KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                      KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl),
                       KeyDef.QSKeys.IsNew, "True"
                      );
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(lang))
                {
                    idpUrl = string.Format("{0}?{1}={2}{5}&{3}={4}",
                        ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login"),
                        KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                        KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl), lang
                        );
                }
                else
                {
                    idpUrl = string.Format("{0}?{1}={2}&{3}={4}",
                        ConfigUtility.AppSettingGetValue("AnrSso.Idp.Login"),
                        KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                        KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl)
                        );
                }


            }
            authnRequest.SendHttpPost(context.Response, idpUrl, relayState);
            context.Response.End();
        }

        public static void SamlLogoutRequest_SendHttpPost(String spResourceUrl)
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;
            // Create a logout request.
            LogoutRequest logoutRequest = new LogoutRequest();
            logoutRequest.Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/"));
            logoutRequest.NameId = new NameId(context.User.Identity.Name);

            String idpUrl = string.Format("{0}?{1}={2}&{5}={6}&{3}={4}",
              ConfigUtility.AppSettingGetValue("AnrSso.Idp.Logout"),
                KeyDef.QSKeys.Sso.SPToken, ConfigUtility.AppSettingGetValue("AnrSso.Sp.SpTokenID"),
                KeyDef.QSKeys.Sso.WReply, HttpUtility.UrlEncode(spResourceUrl), KeyDef.QSKeys.SiteLocale, context.Request.QueryString[App_Lib.KeyDef.QSKeys.SiteLocale].ConvertNullToEmptyString()
              );

            X509Certificate2 x509Certificate = GetSpCertificate();
            logoutRequest.Redirect(context.Response, idpUrl, null, x509Certificate.PrivateKey);
            context.Response.End();
        }

        public static Atp.Saml2.Response BuildAcsResponse(out string relayState)
        {
            Atp.Saml2.Response samlResponse = null;
            relayState = null;
            HttpContext context = HttpContext.Current;
            if (context == null) return null;

            string bindingType = ConfigUtility.AppSettingGetValue("AnrSso.Sp.SamlBinding.IdpToSp");
            switch (bindingType)
            {
                case SamlBindingUri.HttpPost:
                    samlResponse = Atp.Saml2.Response.Create(context.Request);
                    relayState = samlResponse.RelayState;
                    break;

                default:
                    throw new NotImplementedException();
            }

            // Verify the response's signature.
            X509Certificate2 x509Certificate = AnrSsoUtility.GetIdpCertificate();
            if (samlResponse != null && samlResponse.Validate(x509Certificate))
                return samlResponse;
            return null;
        }

        public static AnrSso.ConnectSsoUser InitSsoUser(Atp.Saml2.Response samlRes, Boolean createIfNew)
        {
            if (samlRes == null) return null;
            Assertion samlAssertion = samlRes.Assertions[0] as Assertion;
            if (samlAssertion == null) return null;
            Guid usersecKey = ConvertUtility.ConvertToGuid(samlAssertion.Subject.NameId.NameIdentifier, Guid.Empty);
            if (usersecKey.IsNullOrEmptyGuid()) return null;

            Int32 gwUserID = ConvertUtility.ConvertToInt32(FindSamlAttributeValue("gwuserid", samlAssertion), 0);
            String email = FindSamlAttributeValue("emailaddress", samlAssertion);

            Boolean isNewUser = false;
            #region " get info from connect userdb"

            Lib.Security.Sec_UserMembership cntUser = null;
            //fix for CV-1105
            // if (gwUserID > 0) cntUser = Lib.BLL.BLLSec_UserMembership.SelectByGWUserId(gwUserID);
            if (!email.IsNullOrEmptyString()) cntUser = Lib.BLL.BLLSec_UserMembership.SelectByEmailAddressOrUserID(email);
            if (cntUser == null) //this user is not in connect yet
            {
                // user
                cntUser = new Lib.Security.Sec_UserMembership();
                isNewUser = true;
            }

            AnrSso.ConnectSsoUser user = new AnrSso.ConnectSsoUser(cntUser);
            #endregion
            user.GWUserId = gwUserID;
            user.Email = email;
            user.GWSecretKey = usersecKey;
            user.UserID = FindSamlAttributeValue("loginname", samlAssertion);
            user.CompanyName = FindSamlAttributeValue("companyname", samlAssertion);
            user.JobTitle = FindSamlAttributeValue("jobtitle", samlAssertion);
            user.FirstName = FindSamlAttributeValue("firstname", samlAssertion);
            user.LastName = FindSamlAttributeValue("lastname", samlAssertion);
            user.MiddleName = FindSamlAttributeValue("middlename", samlAssertion);
            user.UserStreetAddress1 = FindSamlAttributeValue("streetaddress1", samlAssertion);
            user.UserStreetAddress2 = FindSamlAttributeValue("streetaddress2", samlAssertion);
            user.UserCity = FindSamlAttributeValue("city", samlAssertion);
            user.UserState = FindSamlAttributeValue("state", samlAssertion);
            user.UserZipCode = FindSamlAttributeValue("zipcode", samlAssertion);
            user.UserAddress1 = FindSamlAttributeValue("streetaddress1", samlAssertion);
            user.UserAddress2 = FindSamlAttributeValue("streetaddress2", samlAssertion);
            user.UserAddressCity = FindSamlAttributeValue("city", samlAssertion);
            user.UserAddressState = FindSamlAttributeValue("state", samlAssertion);
            user.UserAddressPostalCode = FindSamlAttributeValue("zipcode", samlAssertion);
            user.UserAddressCountryCode = FindSamlAttributeValue("countrycode", samlAssertion);
            user.PhoneNumber = FindSamlAttributeValue("phonenumber", samlAssertion);
            user.FaxNumber = FindSamlAttributeValue("faxnumber", samlAssertion);
            user.UserPrimaryCultureCode = FindSamlAttributeValue("primaryculturecode", samlAssertion);
            int memTeamCount = 0;
            if (!isNewUser)
            {
                memTeamCount = Connect.Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(user.MembershipId).Count;
            }

            if (isNewUser || memTeamCount == 0)
            {
                if (createIfNew)
                {
                    var newUser = user;
                    //Create My Anritsu user
                    if (isNewUser)
                        newUser = new AnrSso.ConnectSsoUser(Lib.BLL.BLLSec_UserMembership.Insert(user));
                    //Create a new account with (CompanyName-FirstName)
                    Lib.BLL.BLLAcc_AccountMaster.CreateAccount(newUser.MembershipId
                        , String.Concat(user.CompanyName, "-", newUser.FirstName)
                        , user.CompanyName
                        , String.Empty
                        , WebUtility.GetUserIP()
                        , newUser.Email
                        , newUser.FullName);
                }
            }
            else
            {
                //update
                Lib.BLL.BLLSec_UserMembership.Update(user);
            }

            if (user.GWUserId < 1 || user.GWSecretKey.IsNullOrEmptyGuid()) return null;
            Regex regex = new Regex(ConfigurationManager.AppSettings["TempEmailPattern"]);
            Match match = regex.Match(user.Email);
            if (match.Success)
            {
                user.IsTempUser = true;
            }
            return user;
        }

        private static String FindSamlAttributeValue(string attributeName, Assertion samlAssertion)
        {
            if (samlAssertion == null) return null;
            String attNameUrnToFind = String.Format("urn:oid:anrsso:{0}", attributeName);
            Atp.Saml2.Attribute attFound = null;
            foreach (AttributeStatement attstmt in samlAssertion.AttributeStatements)
            {
                if (attstmt.Attributes != null)
                {
                    IList<Atp.Saml2.Attribute> genatt = attstmt.Attributes.Cast<Atp.Saml2.Attribute>().ToList();
                    var sa = from a in genatt
                             where a.Name.Equals(attNameUrnToFind, StringComparison.InvariantCultureIgnoreCase)
                             select a;
                    if (sa != null && sa.Count() == 1)
                    {
                        attFound = sa.First();
                        break;
                    }
                }

            }
            if (attFound != null && attFound.Values != null && attFound.Values.Count == 1)
                return ConvertUtility.ConvertNullToEmptyString(attFound.Values[0].Data);
            return String.Empty;
        }

        public static AnrSso.ConnectSsoUser CreateUserFromSamlResponse(Atp.Saml2.Response samlResponse, Boolean createIfNew)
        {
            if (samlResponse == null) return null;

            Assertion samlAssertion = samlResponse.Assertions[0] as Assertion;
            if (samlAssertion == null || samlAssertion.Subject == null || samlAssertion.Subject.NameId == null) return null;

            Guid userSecretKey = ConvertUtility.ConvertToGuid(samlAssertion.Subject.NameId.NameIdentifier, Guid.Empty);
            if (userSecretKey.IsNullOrEmptyGuid()) return null;

            AnrSso.ConnectSsoUser user = AnrSsoUtility.InitSsoUser(samlResponse, createIfNew);
            return user;
        }

        public static AnrSso.ConnectSsoUser LoggedInUser_Get()
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return null;
            if (!context.User.Identity.IsAuthenticated)
            {
                LoggedInUser_Set(null);
                return null;
            }
            if (context.Session == null || context.Session[USERSESSIONKEY] == null) // session state is null but user is logged in so try to get it from cache
            {
                App_Lib.AppCacheFactories.ConnectUserCacheFactory userCache = new AppCacheFactories.ConnectUserCacheFactory();
                //extract GwUser Id from cookie
                var gwUserId = ExtractGwUserIdFromIdentity();
                return userCache.GetData(gwUserId) as AnrSso.ConnectSsoUser;
            }

            return context.Session[USERSESSIONKEY] as AnrSso.ConnectSsoUser;
        }

        private static string ExtractGwUserIdFromIdentity()
        {
             var decryptedAuthCookie =
              SecurityUtilities.ClientCacheEncrypter.DecryptString(HttpContext.Current.User.Identity.Name);
                if (string.IsNullOrEmpty(decryptedAuthCookie)) return null;
                var splittedIek = decryptedAuthCookie.Split('|');
                int aClUserId = 0;
                return (splittedIek.Length != 2 || !int.TryParse(splittedIek[0], out aClUserId))?null:aClUserId.ToString();
        }

        public static void LoggedInUser_Set(AnrSso.ConnectSsoUser user)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return;

            context.Session[USERSESSIONKEY] = user;
            if (user != null)
            {
                App_Lib.AppCacheFactories.ConnectUserCacheFactory userCache = new AppCacheFactories.ConnectUserCacheFactory();
                userCache.SetData(user);
            }
        }

        public static void DoDefaultSignout()
        {
            HttpContext context = HttpContext.Current;
            if (context == null) return;
            if (context.Session != null) context.Session.Abandon();
            FormsAuthentication.SignOut();
            context.Response.Redirect(FormsAuthentication.DefaultUrl, true);
        }

        public static bool GetRememberMeCookieType(Atp.Saml2.Response samlRes)
        {
            bool rememberme = false;
            if (samlRes == null) return false;
            Assertion samlAssertion = samlRes.Assertions[0] as Assertion;
            if (samlAssertion == null) return false;
            bool.TryParse(FindSamlAttributeValue("rememberme", samlAssertion), out rememberme);
            return rememberme;
        }
    }

    public static class SecurityUtilities
    {
        public class ClientCacheEncrypter
        {
            private const string Enckey = "4038C253-85C4-436A-8775-C413DDBBE05C";//don't change this
            public static string EncryptString(string input)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.EncryptData(input);
            }
            public static string DecryptString(string encryptedValue)
            {
                var enc = new AnritsuEncrypter(Enckey);
                return enc.DecryptData(encryptedValue);
            }
        }

        public class AnritsuEncrypter
        {
            private const int MinKeyLength = 9;
            static private readonly byte[] Key = new Byte[8];
            static private readonly byte[] Iv = new Byte[8];
            static private string _keystring = string.Empty;//enter key //"E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C";

            public AnritsuEncrypter(string key)
            {
                _keystring = key;
            }
            public void SetEncryptionKeyByIntId(int valueId)
            {
                string segment = "_" + valueId;
                string key = segment;
                while (key.Length < MinKeyLength)
                {
                    key += segment;
                }
                _keystring = key;
            }
            /// <summary>
            /// Function to encrypt data
            /// string Length cannot exceed 92160 bytes.
            /// </summary>
            /// <param name="valueToEncrypt"></param>
            /// <returns></returns>
            public string EncryptData(string valueToEncrypt)
            {
                string mResultString;//Return Result
                //1. string Length cannot exceed 92160 bytes.
                //Otherwise, buffer will overflow.
                //See point 3 for reasons
                if (valueToEncrypt.Length > 92160)
                {
                    mResultString = "Error. Data string too large. Keep within 90Kb.";
                    return mResultString;
                }
                //2. Generate the Keys
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for encryption";
                    return mResultString;
                }
                //3. Prepare the string
                //The first 5 character of the string is formatted to store
                //the actual length of the data.
                //This is the simplest way to remember to original length of the data,
                //without resorting to complicated computations.
                valueToEncrypt = string.Format("{0,5:00000}" + valueToEncrypt,
                         valueToEncrypt.Length);
                //4. Encrypt the Data
                var mDataBytes = new byte[valueToEncrypt.Length];
                var mAsciiEnc = new ASCIIEncoding();
                //UTF8Encoding aEnc = new UTF8Encoding();
                mAsciiEnc.GetBytes(valueToEncrypt, 0, valueToEncrypt.Length, mDataBytes, 0);
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesEncrypt = mDesProvider.CreateEncryptor(Key, Iv);
                //5. Perpare the streams
                var mInputStream = new MemoryStream(mDataBytes);
                var mTransformStream = new CryptoStream(mInputStream,
                    mDesEncrypt, CryptoStreamMode.Read);
                var mOutputStream = new MemoryStream();
                //6. Start performing the encryption
                int mBytesRead;
                var mOutputBytes = new byte[1024];
                do
                {
                    mBytesRead = mTransformStream.Read(mOutputBytes, 0, 1024);
                    if (mBytesRead != 0)
                        mOutputStream.Write(mOutputBytes, 0, mBytesRead);
                }
                while (mBytesRead > 0);
                //7. Returns the encrypted result after it is base64 encoded
                //In this case, the actual result is converted to base64 so that
                //it can be transported over the HTTP protocol without deformation.
                if (mOutputStream.Length == 0)
                    mResultString = "";
                else
                    mResultString = Convert.ToBase64String(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                return mResultString;
            }
            /// <summary>
            /// Function to decrypt data
            /// </summary>
            /// <param name="sStringToEncrypt"></param>
            /// <returns></returns>
            public string DecryptData(string valueToEncrypt)
            {
                string mResultString;
                //1. Generate the Key used for decrypting
                if (!InitKey())
                {
                    mResultString = "Error. Fail to generate key for decryption.";
                    return mResultString;
                }
                //2. Initialize the service provider
                var mDesProvider = new DESCryptoServiceProvider();
                ICryptoTransform mDesDecrypt = mDesProvider.CreateDecryptor(Key, Iv);
                //3. Prepare the streams
                var mOutputStream = new MemoryStream();
                var mTransformStream = new CryptoStream(mOutputStream, mDesDecrypt,
                    CryptoStreamMode.Write);
                //4. Remember to revert the base64 encoding into a byte array
                //to restore the original encrypted data stream
                byte[] mPlainBytes;
                try
                {
                    mPlainBytes = Convert.FromBase64CharArray(valueToEncrypt.ToCharArray(),
                        0, valueToEncrypt.Length);
                }
                catch (Exception)
                {
                    mResultString = "Error. Input Data is not base64 encoded.";
                    return mResultString;
                }
                long mRead = 0;
                long mTotal = valueToEncrypt.Length;
                try
                {
                    //5. Perform the actual decryption
                    while (mTotal >= mRead)
                    {
                        mTransformStream.Write(mPlainBytes, 0, mPlainBytes.Length);
                        //descsp.BlockSize=64
                        mRead = mOutputStream.Length + Convert.ToUInt32(
                            ((mPlainBytes.Length / mDesProvider.BlockSize) *
                            mDesProvider.BlockSize));
                    }
                    var mAsciiEnc = new ASCIIEncoding();
                    mResultString = mAsciiEnc.GetString(mOutputStream.GetBuffer(),
                        0, (int)mOutputStream.Length);
                    //6. Trim the string to return only the meaningful data
                    //Remember that in the encrypt function, the first 5 character
                    //holds the length of the actual data. This is the simplest way
                    //to remember to original length of the data, without resorting
                    //to complicated computations.
                    string mStringLength = mResultString.Substring(0, 5);
                    int mLen = Convert.ToInt32(mStringLength);
                    mResultString = mResultString.Substring(5, mLen);
                    //strResult = strResult.Remove(0,5);
                    return mResultString;
                }
                catch
                {
                    mResultString = "Error. Decryption Failed. Possibly due to incorrect Key"
                        + " or corrputed data";
                    return mResultString;
                }
            }
            /// <summary>
            /// Private function to generate the keys into member variables
            /// </summary>
            /// <returns></returns>
            static private bool InitKey()
            {
                try
                {
                    // Convert Key to byte array
                    var mKeyBytes = new byte[_keystring.Length];
                    var mAsciiEnc = new ASCIIEncoding();
                    mAsciiEnc.GetBytes(_keystring, 0, _keystring.Length, mKeyBytes, 0);
                    //Hash the key using SHA1
                    var mShaProvider = new SHA1CryptoServiceProvider();
                    byte[] mHashBytes = mShaProvider.ComputeHash(mKeyBytes);
                    int i;
                    // use the low 64-bits for the key value
                    for (i = 0; i < 8; i++)
                        Key[i] = mHashBytes[i];
                    for (i = 8; i < 16; i++)
                        Iv[i - 8] = mHashBytes[i];
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}