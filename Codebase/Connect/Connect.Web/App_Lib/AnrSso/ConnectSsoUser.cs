﻿using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.AnrSso
{
    [Serializable]
    public class ConnectSsoUser : Sec_UserMembership
    {
        public Guid GWSecretKey { get; set; }
        public String CompanyName { get; set; }
        public String UserAddress1 { get; set; }
        public String UserAddress2 { get; set; }
        public String UserAddressCity { get; set; }
        public String UserAddressPostalCode { get; set; }
        public String UserAddressState { get; set; }
        public String UserPrimaryCultureCode { get; set; }


        public ConnectSsoUser() { }

        public ConnectSsoUser(Sec_UserMembership user)
        {
            if (user == null) return;
            MembershipId = user.MembershipId;
            GWUserId = user.GWUserId;
            Email = user.Email;
            base.UserID = user.UserID;
            base.LastName = user.LastName;
            base.LastNameInRuby = user.LastNameInRuby;
            base.LastNameInEnglish = user.LastNameInEnglish;
            base.FirstName = user.FirstName;
            base.FirstNameInRuby = user.FirstNameInRuby;
            base.FirstNameInEnglish = user.FirstNameInEnglish;
            base.MiddleName = user.MiddleName;
            base.MiddleNameInRuby = user.MiddleNameInRuby;
            base.IsIndividualEmailAddress = user.IsIndividualEmailAddress;
            base.IsApproved = user.IsApproved;
            base.IsLockedOut = user.IsLockedOut;
            base.LastLoginDate = user.LastLoginDate;
            base.LastActivityDate = LastActivityDate;
            base.LastLockOutUTC = LastLockOutUTC;
            base.UserStreetAddress1 = user.UserStreetAddress1;
            base.UserStreetAddress2 = user.UserStreetAddress2;
            base.UserCity = user.UserCity;
            base.UserState = user.UserState;
            base.UserZipCode = user.UserZipCode;
            base.PhoneNumber = user.PhoneNumber;
            base.FaxNumber = user.FaxNumber;
            base.JobTitle = user.JobTitle;
            base.UserAddressCountryCode = user.UserAddressCountryCode;
            base.ModifiedOnUTC = user.ModifiedOnUTC;
            base.CreatedOnUTC = user.CreatedOnUTC;
            base.IsAdministrator = user.IsAdministrator;
            base.TeamsList = user.TeamsList;

        }

        public static ConnectSsoUser SetMyAnritsuUser(Sec_UserMembership myAnritsuUser, DataRow ssoUserData)
        {
            if (myAnritsuUser == null) return null;
            if (ssoUserData == null) return new ConnectSsoUser(myAnritsuUser);

            ConnectSsoUser user = new ConnectSsoUser(myAnritsuUser);
            user.UserID = ConvertUtility.ConvertToString(ssoUserData["LoginName"], String.Empty);
            user.CompanyName = ConvertUtility.ConvertToString(ssoUserData["CompanyName"], String.Empty);
            user.JobTitle = ConvertUtility.ConvertToString(ssoUserData["JobTitle"], String.Empty);
            user.FirstName = ConvertUtility.ConvertToString(ssoUserData["FirstName"], String.Empty);
            user.LastName = ConvertUtility.ConvertToString(ssoUserData["LastName"], String.Empty);
            user.MiddleName = ConvertUtility.ConvertToString(ssoUserData["MiddleName"], String.Empty);
            user.UserStreetAddress1 = ConvertUtility.ConvertToString(ssoUserData["StreetAddress1"], String.Empty);
            user.UserStreetAddress2 = ConvertUtility.ConvertToString(ssoUserData["StreetAddress2"], String.Empty);
            user.UserAddressCity = ConvertUtility.ConvertToString(ssoUserData["City"], String.Empty);
            user.UserState = ConvertUtility.ConvertToString(ssoUserData["State"], String.Empty);
            user.UserZipCode = ConvertUtility.ConvertToString(ssoUserData["ZipCode"], String.Empty);
            user.PhoneNumber = ConvertUtility.ConvertToString(ssoUserData["PhoneNumber"], String.Empty);
            user.FaxNumber = ConvertUtility.ConvertToString(ssoUserData["FaxNumber"], String.Empty);
            user.UserAddressCountryCode = ConvertUtility.ConvertToString(ssoUserData["CountryCode"], String.Empty);

            return user;
        }
    }
}