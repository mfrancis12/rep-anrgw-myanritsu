﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public abstract class BaseAppCacheFactory
    {
        protected string _CacheFileDependencyPath = String.Empty;
        public abstract string CacheFileDependencyPath { get; }
        public abstract string CacheKeyPrefix { get; }
        public virtual void TouchCacheFile()
        {
            using (StreamWriter sw = new StreamWriter(CacheFileDependencyPath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString());
                sw.Close();
            }
        }

        public virtual String GetCacheKey(String addOnCacheKey)
        {
            if (String.IsNullOrEmpty(addOnCacheKey)) return CacheKeyPrefix;
            return String.Format("{0}_{1}", CacheKeyPrefix, addOnCacheKey.Trim());
        }

        public virtual void RemoveItemFromCache(String addOnCacheKey)
        {
            String cacheKey = GetCacheKey(addOnCacheKey);
            if (string.IsNullOrEmpty(cacheKey)) return;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;
            context.Cache.Remove(cacheKey);
        }

        public virtual void RemoveAll()
        {
            TouchCacheFile();
        }

        public abstract Object GetData(String addOnCacheKey);

        public virtual Object TryRetrieveDataFromCache(String cacheKey)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return null;
            return context.Cache.Get(cacheKey);
        }

        protected virtual void SetCacheData(DateTime cacheAbsoluteExpDate, String cacheKey, Object data)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;

            if (data == null) context.Cache.Remove(cacheKey);
            else
            {
                context.Cache.Insert(cacheKey, data,
                    new CacheDependency(CacheFileDependencyPath), cacheAbsoluteExpDate, Cache.NoSlidingExpiration);
            }
        }
    }
}