﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class CertificateCacheFactory : BaseAppCacheFactory
    {
        private string _CacheFileDependencyPath = String.Empty;
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/SPCertCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntSPCert"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 GetData(String certFileName, String certPwd)
        {
            if (string.IsNullOrEmpty(certFileName)) return null;

            String cacheKey = GetCacheKey(certFileName.ToLowerInvariant());
            HttpContext context = HttpContext.Current;
            X509Certificate2 cert = null;
            if (context == null || context.Cache == null)
            {
                cert = this.LoadCertificate(certFileName, certPwd);
            }
            else
            {
                cert = context.Cache.Get(cacheKey) as X509Certificate2;
                if (cert == null)
                {
                    cert = this.LoadCertificate(certFileName, certPwd);
                    if (cert != null)
                    {
                        DateTime absExpDate = DateTime.UtcNow.AddDays(30);
                        context.Cache.Insert(cacheKey, cert,
                            new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
                    }
                }
            }
            return cert;
        }

        private X509Certificate2 LoadCertificate(string fileName, string password)
        {
            String certFilePath = Path.Combine(
                HttpContext.Current.Server.MapPath(ConfigUtility.AppSettingGetValue("AnrSso.CertStore")), fileName);

            X509Certificate2 cert = new X509Certificate2(certFilePath, password, X509KeyStorageFlags.MachineKeySet);
            return cert;
        }
    }
}