﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class ConnectUserCacheFactory : BaseAppCacheFactory
    {
        private string _CacheFileDependencyPath = String.Empty;
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ConnectUserCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntUserCache"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            if (string.IsNullOrEmpty(addOnCacheKey)) return null;

            //Guid userMembershipId = ConvertUtility.ConvertToGuid(addOnCacheKey, Guid.Empty);
            //if (userMembershipId.IsNullOrEmptyGuid()) return null;

            String cacheKey = GetCacheKey(addOnCacheKey.ToLowerInvariant());
            HttpContext context = HttpContext.Current;
            App_Lib.AnrSso.ConnectSsoUser user = null;
            if (context == null || context.Cache == null)
            {
                //get the user object from the DB

                return null;
            }
            else
            {
                user = context.Cache.Get(cacheKey) as App_Lib.AnrSso.ConnectSsoUser;
            }
            return user;
        }

        public void SetData(App_Lib.AnrSso.ConnectSsoUser user)
        {
            if (user == null || user.MembershipId.IsNullOrEmptyGuid()) return;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;
            DateTime absExpDate = DateTime.UtcNow.AddDays(30);
            String cacheKey = GetCacheKey(user.GWUserId.ToString().ToLowerInvariant());
            context.Cache.Insert(cacheKey, user,
                new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
        }
    }
}