﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Data;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class Downloads_GlobalWeb_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/Downloads_GlobalWeb_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntGWDL"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public List<Downloads_PublicData> GetData(String modelNumber, Int32 cultureGroupID)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            if (cultureGroupID < 1) cultureGroupID = 1;

            String cacheKey = GetCacheKey(String.Format("_{0}_{1}", modelNumber.Trim().ToUpperInvariant(), cultureGroupID));

            var dataFound = TryRetrieveDataFromCache(cacheKey)
                as List<Downloads_PublicData>;
            if (dataFound == null)
            {
                dataFound = RetrieveDataFromSource(modelNumber, cultureGroupID);
                DateTime absExpDate = DateTime.UtcNow.AddHours(2);
                SetCacheData(absExpDate, cacheKey, dataFound);
            }

            return dataFound;
        }

        private List<Downloads_PublicData> RetrieveDataFromSource(String modelNumber, Int32 cultureGroupID)
        {
            Lib.Cfg.ProductConfig prodConfig = ProductConfigUtility.SelectByModel(false, modelNumber.Trim());
            if (prodConfig == null) return null;

            // var list = (List<Downloads_PublicData>)Lib.Product.DownloadsBLL.GlobalWebDownloads_Select(modelNumber, cultureGroupID);// new List<ObjectDataSources.EndUser_ProductSupport.Downloads_PublicData>();
            var list = Lib.Product.DownloadsBLL.GetDownloadsFromCMS(modelNumber, cultureGroupID);

            //if (list != null)
            //{
            //    foreach (var r in list)
            //        list.Add(new ObjectDataSources.EndUser_ProductSupport.Downloads_PublicData(r));
            //}

            #region " Model tags for public downloads "
            if (prodConfig.ModelTags != null)
            {
                List<Downloads_PublicData> tbMNTag = null;
                foreach (String tmn in prodConfig.ModelTags)
                {
                    //tbMNTag = (List<Downloads_PublicData>)Lib.Product.DownloadsBLL.GlobalWebDownloads_Select(tmn, cultureGroupID);
                    tbMNTag = Lib.Product.DownloadsBLL.GetDownloadsFromCMS(tmn, cultureGroupID);
                    //if (tbMNTag != null)
                    //{
                    //    foreach (DataRow r in tbMNTag.Rows)
                    //        list.Add(new ObjectDataSources.EndUser_ProductSupport.Downloads_PublicData(r));
                    //}
                }
            }
            #endregion

            if (list == null) return null;

            var q = (from d in list select d).Distinct();
            //return (from d in list select d).Distinct() as List<ObjectDataSources.EndUser_ProductSupport.Downloads_PublicData>;

            return q.ToList<Downloads_PublicData>();


        }
    }
}