﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources;
using Anritsu.Connect.Web.App_Lib.AnrSso;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class Downloads_JPDLVersionTable_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/Downloads_JPDLVersionTable_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntJPDL_VersionTable"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public DataTable GetData(String modelNumber, Int32 cultureGroupID)
        {
            if (modelNumber.IsNullOrEmptyString())  return null;
            if (cultureGroupID < 1) cultureGroupID = 1;

            String cacheKey = GetCacheKey(String.Format("_{0}_{1}",
                modelNumber.Trim().ToUpperInvariant(),
                cultureGroupID
                ));

            DataTable dataFound = TryRetrieveDataFromCache(cacheKey) as DataTable;
            if (dataFound == null)
            {
                dataFound = RetrieveDataFromSource(modelNumber, cultureGroupID);
                DateTime absExpDate = DateTime.UtcNow.AddDays(1);
                SetCacheData(absExpDate, cacheKey, dataFound);
            }
            return dataFound;
        }

        private DataTable RetrieveDataFromSource(String modelNumber,
            Int32 cultureGroupID
            )
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            if (cultureGroupID < 1) cultureGroupID = 1;

            return Lib.Product.DownloadsBLL.JPDownloads_VersionTable_Select(modelNumber); 
        }
    }
}