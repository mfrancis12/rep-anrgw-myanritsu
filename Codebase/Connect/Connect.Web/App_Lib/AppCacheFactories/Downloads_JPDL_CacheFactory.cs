﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class Downloads_JPDL_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/Downloads_JPDL_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntJPDLNoUSB"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData> GetData(String modelNumber, 
            Int32 cultureGroupID, 
            AnrSso.ConnectSsoUser loggedInUser,
            Guid prodRegWebToken
            )
        {
            if (modelNumber.IsNullOrEmptyString() || loggedInUser == null || prodRegWebToken.IsNullOrEmptyGuid()) return null;
            if (cultureGroupID < 1) cultureGroupID = 1;
            
            String cacheKey = GetCacheKey(String.Format("_{0}_{1}_{2}", 
                modelNumber.Trim().ToUpperInvariant(), 
                cultureGroupID,
                loggedInUser.GWUserId
                ));

            List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData> dataFound = TryRetrieveDataFromCache(cacheKey)
                as List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData>;
            if (dataFound == null)
            {
                dataFound = RetrieveDataFromSource(modelNumber, cultureGroupID, loggedInUser, prodRegWebToken);
                DateTime absExpDate = DateTime.UtcNow.AddDays(1);
                SetCacheData(absExpDate, cacheKey, dataFound);
            }

            return dataFound as List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData>;
        }

        private List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData> RetrieveDataFromSource(String modelNumber,
            Int32 cultureGroupID,
             AnrSso.ConnectSsoUser loggedInUser,
            Guid prodRegWebToken
            )
        {
            if (modelNumber.IsNullOrEmptyString() || loggedInUser == null || prodRegWebToken.IsNullOrEmptyGuid()) return null;
            if (cultureGroupID < 1) cultureGroupID = 1;

            Lib.Cfg.ProductConfig prodConfig = ProductConfigUtility.SelectByModel(false, modelNumber.Trim());
            if (prodConfig == null) return null;

            List<String> userEmails = new List<String>() { LoginUtility.GetCurrentUserOrSignIn().Email };
            DataTable tb = Lib.Product.DownloadsBLL.JPNonUSBDownloads_Select(modelNumber, cultureGroupID, userEmails);

            List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData> list = new List<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData>();
            if (tb != null)
            {
                foreach (DataRow r in tb.Rows)
                    list.Add(new ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData(prodRegWebToken, r));
            }

            if (list == null) return null;

            var q = (from d in list select d).Distinct();
            //return (from d in list select d).Distinct() as List<ObjectDataSources.EndUser_ProductSupport.Downloads_PublicData>;

            return q.ToList<ObjectDataSources.EndUser_ProductSupport.Downloads_PrivateJapanDlData>();


        }
    }
}