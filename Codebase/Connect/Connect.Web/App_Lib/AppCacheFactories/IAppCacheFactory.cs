﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    interface IAppCacheFactory
    {
        String CacheFileDependencyPath { get; }
        String CacheKey { get; }
        void TouchCacheFile();
    }
}
