﻿using System;
using System.Collections.Generic;
using System.Web;
using Amazon.S3.Model;
using Anritsu.Connect.Lib.AWS;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class JapanDonloadsCacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/JPDLAWSCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntJPDL"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public List<S3Object> GetData(String prefix, String regionCfgKey, String type, String delimeter)
        {
            List<S3Object> dataFound = TryRetrieveDataFromCache(CacheKeyPrefix) as List<S3Object>;
            if (dataFound == null)
                dataFound = RetrieveDataFromAWS(prefix, regionCfgKey, type, delimeter, CacheKeyPrefix);
            if (dataFound == null) return null;
            return dataFound;
        }

        private List<S3Object> RetrieveDataFromAWS(String prefix, String regionCfgKey, String type, String delimeter, String cacheKey)
        {
            var awsclient = new AWSUtility_S3(type);
            List<S3Object> s3Objects =awsclient.GetS3Objects(type, regionCfgKey, prefix, delimeter);
            DateTime absExpDate = DateTime.UtcNow.AddDays(1);
            SetCacheData(absExpDate, cacheKey, s3Objects);
            return s3Objects;
        }

        //AEKTODO: delete. dummy method for UnitTest
        public List<S3Object> Dummy_GetData(String prefix, String regionCfgKey, String type, String delimeter)
        {
            var list = new List<S3Object>();
            list.Add(new S3Object() 
            {
                ETag = String.Empty,
                Key = "",//dl/Downloads/MD8430A_MX843080A/v111a Engineering Version/Ver1_11/",
                LastModified = new DateTime(),
                Owner = new Owner(),
                Size = 0,//1,
                StorageClass = new Amazon.S3.S3StorageClass(String.Empty)
            });
            return list;
        }
    }
}