﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class LinksHomeCacheFactory : IAppCacheFactory
    {
        private string _CacheFileDependencyPath = String.Empty;
        public string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/LinksHomeCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public string CacheKey
        {
            get { return "acLnkHome"; }
        }

        public void TouchCacheFile()
        {
            using (StreamWriter sw = new StreamWriter(CacheFileDependencyPath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString());
                sw.Close();
            }
        }

        public DataTable GetLinks(String locale, Guid accountID)
        {
            HttpContext context = HttpContext.Current;
            DataTable tb = null;
            if (context == null || context.Cache == null)
            {
                tb = GetLinksFromDB(locale, accountID);
            }
            else
            {
                string cacheKey = String.Format("{0}_{1}", CacheKey, locale);
                if (!accountID.IsNullOrEmptyGuid()) cacheKey += "_" + accountID.ToString();

                tb = context.Cache.Get(cacheKey) as DataTable;
                if (tb == null || tb.Rows.Count < 1)
                {
                    tb = GetLinksFromDB(locale, accountID);
                    context.Cache.Insert(cacheKey, tb,
                        new CacheDependency(CacheFileDependencyPath), DateTime.UtcNow.AddDays(30), Cache.NoSlidingExpiration);
                }
            }
            return tb;
        }

        private DataTable GetLinksFromDB(String locale, Guid accountID)
        {
            return Lib.CustomLink.CustomLinkDisplayBLL.Select("~/home", "CONNECTHOME"
                   , locale, String.Empty, accountID);
        }
    }
}