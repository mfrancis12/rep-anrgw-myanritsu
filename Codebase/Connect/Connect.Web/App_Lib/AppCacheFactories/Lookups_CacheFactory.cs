﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class Lookups_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/Lookups_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntLookupsChe"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public List<AccountConfig_DistPortal_Status> AccountConfig_DistPortalStatus_SelectAll()
        {
            String cacheKey = GetCacheKey("_ACCDISTPST");

            List<AccountConfig_DistPortal_Status> dataFound = TryRetrieveDataFromCache(cacheKey) as List<AccountConfig_DistPortal_Status>;
            if (dataFound == null)
                dataFound = RetrieveDataFromSource_AccountConfig_DistPortalStatus(cacheKey);

            return dataFound;
        }

        private List<AccountConfig_DistPortal_Status> RetrieveDataFromSource_AccountConfig_DistPortalStatus(String cacheKey)
        {
            if (cacheKey.IsNullOrEmptyString()) return null;
           List<AccountConfig_DistPortal_Status> list = Lib.Account.AccountConfig_DistPortal_StatusBLL.SelectAll();
            DateTime absExpDate = DateTime.UtcNow.AddDays(30);
            SetCacheData(absExpDate, cacheKey, list);
            return list;
        }
    }
}