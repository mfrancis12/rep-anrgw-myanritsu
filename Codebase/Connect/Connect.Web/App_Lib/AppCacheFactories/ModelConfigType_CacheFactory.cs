﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class ModelConfigType_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ModelConfigType_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntMDLCFGTYP"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public Lib.Cfg.ModelConfigTypeInfo GetConfigInfo(String modelConfigType)
        {
            if (modelConfigType.IsNullOrEmptyString()) return null;
            String cacheKey = GetCacheKey("_ALL");

            List<Lib.Cfg.ModelConfigTypeInfo> dataFound = TryRetrieveDataFromCache(cacheKey) as List<Lib.Cfg.ModelConfigTypeInfo>;
            if (dataFound == null)
                dataFound = RetrieveDataFromSource(cacheKey, modelConfigType);

            if (dataFound == null) return null;

            var vmct = (from mct in dataFound where mct.ModelConfigType.Equals(modelConfigType.Trim(), StringComparison.InvariantCultureIgnoreCase) select mct);
            if(vmct == null || vmct.Count() != 1) return null;
            return vmct.First();
        }

        private List<Lib.Cfg.ModelConfigTypeInfo> RetrieveDataFromSource(String cacheKey, String modelNumber)
        {
            if (cacheKey.IsNullOrEmptyString()) return null;
            List<Lib.Cfg.ModelConfigTypeInfo> mct = Lib.Cfg.ModelConfigTypeBLL.SelectAll();
            DateTime absExpDate = DateTime.UtcNow.AddDays(30);
            SetCacheData(absExpDate, cacheKey, mct);
            return mct;
        }
    }
}