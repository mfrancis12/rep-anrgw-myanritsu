﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class ModelConfig_CacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ModelConfig_CacheFactory.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntMDLCFG"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public Lib.Cfg.ProductConfig GetData(Boolean allowSelfRegOnly, String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return null;
            String cacheKey = GetCacheKey(String.Format("_{0}", modelNumber.Trim().ToUpperInvariant()));

            Lib.Cfg.ProductConfig dataFound = TryRetrieveDataFromCache(cacheKey) as Lib.Cfg.ProductConfig;
            if (dataFound == null)
                dataFound = RetrieveDataFromSource(cacheKey, modelNumber);

            if (dataFound == null) return null;

            if (allowSelfRegOnly)
            {
                return dataFound.Reg_AllowSelfReg ? dataFound : null;
            }

            return dataFound;
        }

        private Lib.Cfg.ProductConfig RetrieveDataFromSource(String cacheKey, String modelNumber)
        {
            if (cacheKey.IsNullOrEmptyString()) return null;
            Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(modelNumber);
            DateTime absExpDate = DateTime.UtcNow.AddDays(1);
            SetCacheData(absExpDate, cacheKey, pcfg);
            return pcfg;
        }

        public String ModelCacheKey(String modelNumber)
        {
            if (modelNumber.IsNullOrEmptyString()) return String.Empty;
            return GetCacheKey(String.Format("_{0}", modelNumber.Trim().ToUpperInvariant()));
        }
    }
}