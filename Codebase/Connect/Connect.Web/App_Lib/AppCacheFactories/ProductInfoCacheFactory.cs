﻿using System;
using System.Collections.Generic;
using System.Web;
using Amazon.S3.Model;
using Anritsu.Connect.Lib.AWS;
using Anritsu.Connect.Lib.Product;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class ProductInfoCacheFactory : BaseAppCacheFactory
    {
        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ProductInfoCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "cntPINFO"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public List<ErpProductInfo> GetData()
        {
            var dataFound = TryRetrieveDataFromCache(CacheKeyPrefix) as List<ErpProductInfo> ?? RetrieveDataFromSource();
            return dataFound;
        }

        private List<ErpProductInfo> RetrieveDataFromSource()
        {
            //get Product information and stored it in cache
            var listProdInfo= ProductInfoBLL.SelectAllModelNumberInErpDB();
            DateTime absExpDate = DateTime.UtcNow.AddDays(1);
            SetCacheData(absExpDate, CacheKeyPrefix, listProdInfo);
            return listProdInfo;
        }
    }
}