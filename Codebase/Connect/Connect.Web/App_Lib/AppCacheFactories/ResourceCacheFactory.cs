﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.AppCacheFactories
{
    public class ResourceCacheFactory : IAppCacheFactory
    {
        private string _CacheFileDependencyPath = String.Empty;
        public string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_CacheFileDependencyPath))
                    _CacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ResourceCache.config");
                return _CacheFileDependencyPath;
            }
        }

        public string CacheKey
        {
            get { return "acResCache"; }
        }

        public void TouchCacheFile()
        {
            using (StreamWriter sw = new StreamWriter(CacheFileDependencyPath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString());
                sw.Close();
            }
        }

        public String GetContentString(String resClass, String resKey, String culture)
        {

            if (resClass.IsNullOrEmptyString() || resKey.IsNullOrEmptyString()) return null;
            
            HttpContext context = HttpContext.Current;
            String contentStr = String.Empty;
            Boolean contentFound = false;
            CultureInfo ci = new CultureInfo(culture);
            if (ci == null || ci.Name.IsNullOrEmptyString()) ci = new CultureInfo("en");
            if (context == null || context.Cache == null)
            {
                contentFound = Lib.Content.Res_ContentStoreBLL.GetContentStr(resClass, 
                    resKey, ci, true, out contentStr);
            }
            else
            {
                String cacheKeyForRes = GetCacheKey(resClass.Trim(), resKey.Trim(), ci.Name);
                contentStr = context.Cache.Get(cacheKeyForRes) as String;
                if (contentStr.IsNullOrEmptyString())
                {
                    contentFound = Lib.Content.Res_ContentStoreBLL.GetContentStr(resClass,
                    resKey, ci, true, out contentStr);
                    if (contentFound)
                    {
                        context.Cache.Insert(cacheKeyForRes, contentStr,
                            new CacheDependency(CacheFileDependencyPath));
                    }
                }
                else
                {
                    contentFound = true;
                }
            }

            return contentFound ? contentStr : resKey;
        }

        public void RemoveFromCache(String resClass, String resKey, String culture)
        {            
            HttpContext context = HttpContext.Current;
            if (context == null || context.Cache == null) return;
            String cacheKeyForRes = GetCacheKey(resClass.Trim(), resKey.Trim(), culture);
            context.Cache.Remove(cacheKeyForRes);
        }

        private String GetCacheKey(String resClass, String resKey, String culture)
        {
            return String.Format("{0}_{1}_{2}_{3}", CacheKey, resClass.Trim(), resKey.Trim(), culture.Trim());
        }
    }
}