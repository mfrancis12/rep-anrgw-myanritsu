﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Microsoft.Runtime;
using System.Net;
using System.Text;

using System.Json;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Lib.GoogleAPIs.GoogleTranslateAPI
{
    public class GoogleTranslateUtil
    {
        public static String DetectTxtLanguage(String q)
        {
            return DetectLanguage(q);
        }
        public static String TranslateFromEnglish(String targetLang, String q)
        {
            return Translate(GoogleTranslateSupportedLangCode.English, targetLang, q);
        }

        public static String Translate(String sourceLang, String targetLang, String q)
        {
            if (q.IsNullOrEmptyString()) return String.Empty;
            //else if (q.Length > 5000) return q; //Google Translate API currently only support 5000 chars

            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add("X-HTTP-Method-Override", "POST");
                dynamic result = JsonValue.Parse(webClient.DownloadString(BuildGoogleTranslateAPIRequestUrl(sourceLang, targetLang, q)));

                String translatedText = String.Empty;
                if (result != null && result.data != null && result.data.translations.Count > 0)
                {
                    translatedText = result.data.translations[0].translatedText;
                }
                return HttpUtility.HtmlEncode(translatedText);
            }
        }

        private static String BuildGoogleTranslateAPIRequestUrl(String sourceLang, String targetLang, String textToTranslate)
        {
            StringBuilder url = new StringBuilder(ConfigUtility.AppSettingGetValue("Connect.Web.GoogleTranslateAPI.EndpointUrl"));
            url.Append("?");
            url.AppendFormat("key={0}", ConfigUtility.AppSettingGetValue("Connect.Web.GoogleTranslateAPI.APIKey"));
            url.AppendFormat("&source={0}", sourceLang.Trim());
            url.AppendFormat("&target={0}", targetLang.Trim());
            url.Append("&format=html");
            string cleanedStr = HttpUtility.HtmlDecode(textToTranslate.ConvertNullToEmptyString().Trim());
            url.AppendFormat("&q={0}", HttpUtility.UrlEncode(cleanedStr));
            return url.ToString();
        }

        public static String DetectLanguage(String q)
        {
            if (q.IsNullOrEmptyString()) return String.Empty;
            //else if (q.Length > 5000) return q; //Google Translate API currently only support 5000 chars

            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add("X-HTTP-Method-Override", "POST");
                dynamic result = JsonValue.Parse(webClient.DownloadString(BuildGoogleDetectAPIRequestUrl(q)));

                String textLanguage = String.Empty;
                if (result != null && result.data != null)
                {
                    textLanguage = result.data.detections[0][0].language;
                }
                return textLanguage;
            }
        }

        private static String BuildGoogleDetectAPIRequestUrl(String sourceLang)
        {
            StringBuilder url = new StringBuilder(ConfigUtility.AppSettingGetValue("Connect.Web.GoogleDetectAPI.EndpointUrl"));
            url.AppendFormat("key={0}", ConfigUtility.AppSettingGetValue("Connect.Web.GoogleTranslateAPI.APIKey"));
            url.AppendFormat("&q={0}", sourceLang.Trim());
            return url.ToString();
        }
    }
}