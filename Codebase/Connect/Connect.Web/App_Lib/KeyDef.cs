﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib
{
 public class ConfigKeys
    {
        public static string GwdataCdnPath
        {
            get
            {
                //get the production cdn url if app setting value is not specified
                return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["GwdataCdnPath"]) ?
                    ConfigurationManager.AppSettings["GwdataCdnPath"] :
                    "//dl.cdn-anritsu.com";
            }
        }
    }
    internal static class KeyDef
    {

        public class QSKeys
        {
            public const String SiteLocale = "lang";
            public const String ReturnURL = "ReturnURL";
            public const String TargetURL = "TargetURL";
            public const String SsoUrlID = "sulid";
            public const String PageURL = "pgurl";
            public const String ModuleID = "mdlid";
            public const String FormID = "frmid";
            public const String FieldsetID = "fsid";
            public const String FieldsetControlID = "fwscid";
            public const String ResClassKey = "rckey";
            public const String FormSubmitKey = "frmsmtkey";
            public const String DataSourceID = "dsid";
            public const String ProdRegWFInstID = "prwfiid";
            public const String RegisteredProductWebAccessKey = "rwak";
            public const String ProdRegCartWebAccessKey = "crtwak";
            public const String ProdRegCartItemID = "citm";
            public const String ModelNumber = "mn";
            public const String ProductOwnerRegion = "prgn";
            public const String ProfileType = "pftype";
            public const String ProfileStatus = "pfstatus";
            public const String AccountID = "accid";
            public const String CompanyName = "orgname";
            public const String AccountStatusCode = "accstatus";
            public const String IsAccountVerified = "accveri";
            public const String WFInstID = "wfinstid";
            public const String DownloadSource = "dlsc";
            public const String DownloadID = "dlid";
            public const String AddNewOrg = "addnew";
            public const String MemMembershipId = "memid";
            public const String MemEmailAddress = "mememail";
            public const String MemLastName = "memln";
            public const String MemFirstName = "memfn";
            public const String MemIsAdmin = "memadm";
            public const String AdminContentSearch_Auto = "srhmd";
            public const String AdminContentSearch_PreClassKey = "srhclass";
            public const String AdminContentSearch_PreResKey = "srhreskey";
            public const String AdminContent_ContentID = "contentid";
            public const String IsAccountChanged = "accchngstatus";
            public const String IsAddressChanged = "addchngstatus";
            public const String Admin_USBNo = "admjpusb";
            public const String MyProductsFilter = "filter";
            public const String NotificationId = "nfnid";
            public const String PackageId = "pid";
            public const String Package_Success = "pack_success";
            public const String Package_Copied = "pack_copied";
            public const String FileId = "fid";
            public const String PName = "pname";
            public const String ActTab = "act";
			public const String SerialNumber = "sn";
            public const String ProductsModifyMode = "prodmodmode";

            public class Sso
            {
                public const String SPToken = "sptkn";
                public const String WReply = "wreply";
            }

            public const String Admin_ProdReg_SelectedTabID = "tab";
            public const String CalCertFileName = "calcert";
            public const String ProdRegItemID = "prid";
            public const String SIS_ProductID = "pid";
            public const String SIS_ProductFilterID = "pfid";
            public const String SIS_DocTypeFilterID = "dtfid";
            public const String Admin_CustomLinkID = "admclnkid";
            public const String IsNew = "isNew";
            public const String EntryId = "Eid";
            public const String SupportTypeCode = "typecode";
            public const String ModelConfigType = "mdlcfg";
        }

        public class SSKeys
        {
            internal const String OrganizationListByMembershipId = "ssOrgsByMem";
            internal const String AccountOrIPBasedCultureGroup = "ssAccIPCG";
            internal const String BrowserBasedCultureGroupId = "ssBrowserCGID";
            internal const String LoggedInUser = "loggedinuser";
            public const String JPProdTermsPerSession = "ssJPProdTermsPerSession";
            public const String JPProdTermsAgreedWebAccessKeys = "ssJPProdTermsPerSN";
            public const string SelectedAccountID = "ssCurrAccid";
            public const string SelectedAccount = "ssCurrAcc";
            public const string ShowMessage = "ssMsg";
            public const string MyRegProductsCache = "ssMyRegProds";
            public const string AdminUserSearchOptions = "ssAdminUserSearchOptions";
            public const string AdminProdRegSearchOptions = "ssAdminProdRegSearchOptions";
            public const String AdminOrgSearchOptions = "ssAdminOrgSearchOptions";
            public const string AdminContentSearchOptions = "ssAdminContentSearchOptions";
            public const string AdminContentSearchResults = "ssAdminContentSearchResults";

            public const String AdminSupportTypeSearchOptions = "ssAdminSupportTypeSearchOptions";

            public const String AdminProdReg = "ssAdminProdReg";
            public const String AdminProdCfg = "ssAdminProdCfg";
            public const String AdminAccountInfo = "ssAdminAccInfo";
            public const String ShowAllRegistrationInProgress = "ssShowAllRegInProg";
            //public const String HomeSupportResourceAddOnLinksGet = "ssHomeSuptResrAddOnLinks";

            public const String UserProdReg = "ssUserProdReg";
            public const String UserProdCfg = "ssUserProdCfg";
            //public const String Dongle_AccessPass = "ssDongleAP";
            public const String JapanUSB_AccessPass = "ssJPDongleAP";
            public const String UserProdSupportData = "ssUserPrdSuprt";
            public const String ProdRegDetailData = "ssProdRegDetail";

            public const String ProdSupport_EndUser_PrivateData = "ssUserPrdSuprtPrivateData";
            public const String ProdSupport_WarrantyData = "ssPrdSuprtWty";
            public const String ProdSupport_Data = "ssPrdSuprtData";
            public const String ProdSupport_PubDownloadData = "ssPrdSuprtPubDLData";

            public const String EndUser_ProdReg_RegCartInfo = "ssEUsrProdRegCart";
            public const String Admin_ProdReg_ItemStatus = "ssAdmProdRegItemStatus";
            public const String Admin_ProdReg_MasterInfo = "ssAdmPRM_";
            public const String Admin_ProdReg_Data = "ssAdmPRM_Data";

            public const String MyProducts_List = "ssMyPrds";
            public const String PurchasedSupport_AgreementNotice = "psagmt";
            public const String PrivateDownloads = "downloadPaths";
			
			public const String ToBeUpdatedJPPackages = "tobeUpdatedJPPackages";
            public const String ToBeUpdatedMBMCPackages = "tobeUpdatedMBMCPackages";
            public const String HasTauDownloads = "hasTAUDownloads";
            public const string ddlSerialNumberId = "snid";
            public const string Selected_SerialNumber = "sn";
            public const string Selected_ModelNumber = "mn";
        }

        public class MsgKeys
        {
            public const string DefaultOrg = "Please Update";
        }

        public class FormKeys
        {
            internal const String IEK = "iek";
        }

        public class CacheKeys
        {
            public const string UserCache = "pche_lgnusr";
        }

        public class UrlList
        {
            public const String SignIn = "~/spsignin";
            public const String SignOut = "~/spsignout";
            public class SiteAdminPages
            {
                public const String Home = "~/siteadmin/home";
                public const String PageConfig_PageSettings = "~/siteadmin/pageconfig/pagesettings";
                //public const String SiteLocale_ManageStaticLocale = "~/siteadmin/staticlocale/managestaticlocale";
                public const String FormAdmin_List = "~/siteadmin/formadmin/formlist";
                public const String FormAdmin_Details = "~/siteadmin/formadmin/formdetails";
                public const String FormAdmin_FieldsetDetails = "~/siteadmin/formadmin/fieldsetdetails";
                public const String FormAdmin_FieldsetControlDetails = "~/siteadmin/formadmin/fieldsetcontroldetails";
                public const String FormAdmin_Preview = "~/siteadmin/formadmin/formpreview";

                public const String ProdRegAdmin_Home = "~/prodregadmin/home";

                public const String AdminUserSearch = "~/siteadmin/orguseradmin/connectusersearch";
                public const String AdminUserSearchResult = "~/siteadmin/orguseradmin/connectusers";
                public const String AdminUserDetail = "~/siteadmin/orguseradmin/connectuserdetail";

                public const String AdminProdRegSearch = "~/prodregadmin/productregsearch";
                public const String AdminProdRegSearch_JP = "~/prodregadmin/productregsearch_jp";
                public const String AdminProdRegSearchResult = "~/prodregadmin/home";
                public const String AdminProdRegSearchResult_JP = "~/prodregadmin/productregsearch_jp_result";
                public const String AdminProdRegGeneratePDF = "~/prodregadmin/productregdetailpdf";
                public const String AdminProdRegGeneratePrint = "~/prodregadmin/productregdetailprint";

                public const String AdminProdRegDetail_RegInfo_ToBeDeleted = "~/prodregadmin/productregdetail-ToBeDeleted";
                public const String AdminProdRegDetail_AccInfo_ToBeDeleted = "~/prodregadmin/productregdetail-acc-ToBeDeleted";
                public const String AdminProdRegDetail_AddOnDownloads_ToBeDeleted = "~/prodregadmin/productregdetail-addondl-ToBeDeleted";
                public const String AdminProdRegDetail_UserAcl_ToBeDeleted = "~/prodregadmin/productregdetail-useracl-ToBeDeleted";
                public const String AdminProdRegDetail_JpUsb_ToBeDeleted = "~/prodregadmin/productregdetail-jpusb-ToBeDeleted";
                public const String AdminProdRegDetail_BatchApproval_ToBeDeleted = "~/prodregadmin/productregdetail-batchapprove-ToBeDeleted";
                public const String AdminProdRegDetail_FormInfo_ToBeDeleted = "~/prodregadmin/productregdetail-forminfo-ToBeDeleted";

                public const String AdminContentSearch = "~/siteadmin/contentadmin/contenthome";
                public const String AdminContentSearchResult = "~/siteadmin/contentadmin/contentsearchresults";
                public const String AdminContentDetails = "~/siteadmin/contentadmin/contentdetails";

                public const String AdminJapanUSB_List = "~/siteadmin/usbkeyadmin/usbkeys";
                public const String AdminJapanUSB_AddNew = "~/siteadmin/usbkeyadmin/usbaddnew";
                public const String AdminJapanUSB_Detail = "~/siteadmin/usbkeyadmin/usbdetail";

                public const String NotificationAdmin_Details = "~/siteadmin/notificationadmin/notificationdetails";
                public const String NotificationAdmin_List = "~/siteadmin/NotificationAdmin/notificationlist";
                public const String NotificationAdmin_Preview = "~/siteadmin/notificationadmin/notificationpreview";

                public const String PackageAdmin_Details = "~/siteadmin/packageadmin/packagedetails";
                public const String PackageAdmin_List = "~/siteadmin/packageadmin/packagelist";
                public const String PackageAdmin_Details_Jp = "~/siteadmin/packageadmin/packagedetails_jp";
                public const String PackageAdmin_List_Jp = "~/siteadmin/packageadmin/PackageList_jp";

                public class ProdRegAdminPages
                {
                    public const String Manage_RegistrationAddNew = "~/prodregadmin/manage_registration_addnew";
                    public const String Manage_RegistrationAddNew_JP = "~/prodregadmin/manage_registration_addnew_JP";
                    public const String Manage_Registration = "~/prodregadmin/manage_registration";
                    public const String Manage_Registration_JP = "~/prodregadmin/manage_registration_JP";
                    public const String Manage_Account = "~/prodregadmin/manage_account";
                    public const String Manage_USMMD = "~/prodregadmin/manage_config_usmmd";
                    public const String Manage_JPSW = "~/prodregadmin/manage_config_jpsw";
                    public const String Manage_JPSW_UserACL = "~/prodregadmin/manage_config_jpsw_useracl";
                    public const String Manage_ESD = "~/prodregadmin/manage_config_esd";
                    //public const String Manage_SIS = "~/prodregadmin/manage_config_sis";
                    public const String Manage_DKSW = "~/prodregadmin/manage_config_dksw";
                    public const String Search = "~/prodregadmin/productregsearch";
                    public const String SearchResults = "~/prodregadmin/home";
                }

                public class Admin_OrgPages
                {
                    public const String OrgSearch = "~/siteadmin/orguseradmin/orgsearch";
                    public const String OrgSearchResult = "~/siteadmin/orguseradmin/organizations";
                    public const String OrgDetails = "~/siteadmin/orguseradmin/orgdetail";
                    public const String OrgDetailsUsers = "~/siteadmin/orguseradmin/orgdetail_users";
                    public const String OrgDetailsProdRegs = "~/siteadmin/orguseradmin/orgdetail_prodregs";
                    public const String OrgDetailsTAUPackagePermissions = "~/siteadmin/orguseradmin/OrgDetails_TAUPackagePermissions";
                    public const String OrgDetailsDeletedProdRegs = "~/siteadmin/orguseradmin/orgdetail_deletedprodregs";
                }

                public class Admin_DistPortalPages
                {
                    public const String OrgDetailsDistributorPortal = "~/siteadmin/distportaladmin/orgdetail_DistPortal";
                    public const String ProdFilterDetails = "~/siteadmin/distportaladmin/sis_prodfilterdetails";
                    public const String ProdFilterList = "~/siteadmin/distportaladmin/sis_prodfilterlist";
                    public const String DocTypeFilterDetails = "~/siteadmin/distportaladmin/sis_doctypefilterdetails";
                    public const String DocTypeFilterList = "~/siteadmin/distportaladmin/sis_doctypefilterlist";
                }

                public class Admin_CustomLinkPages
                {
                    public const String CustomLinks = "~/siteadmin/customlinkadmin/customlinks";
                    public const String CustomLinkDetails = "~/siteadmin/customlinkadmin/customlinkdetails";
                    public const String CustomLinkRefAcc = "~/siteadmin/customlinkadmin/customlinkrefacc";
                    public const String CustomLinkRefSisProd = "~/siteadmin/customlinkadmin/customlinkrefsisprod";
                }
            }
            public const String ManageOrg = "~/myaccount/companyprofile";
            public const String MyProducts = "~/myproduct/home";
            public const String ManageUsers = "~/myaccount/manageusers";
            public const String Home = "~/home";
            public const String ProductSupportAgreement = "~/myproduct/productsupportterms";
            public const String ProductSupport = "~/myproduct/productsupport";//TBD
            public const String ProductSupportPublic = "~/myproduct/productsupport";
            public const String ProductSupportPrivate = "~/myproduct/productsupport-private";
            public const String ProductSupportPrivate_JPUSB = "~/myproduct/productsupport-privatejpusb";
            public const String ProductSupportSoftwareDownloads = "~/myproduct/productsupport-sw";
            public const String ProductSupportPurchasedDownloads = "~/myproduct/productsupport-swp";
            public const String ProductSupportWarranty = "~/myproduct/productsupport-wty";
            public const String ProductSupportCalCert = "~/myproduct/productsupport-calcert";
            public const String ProductSupportRenew = "~/myproduct/productsupport-renew";
            public const String ProductSupportRenewAck = "~/myproduct/productsupport-renew-ack";
            public const String ConnectDownloadURL = "/myproduct/connectdownload";
            public const String TermsOfUse = "~/termsofuse";
            public const String ContactUs = "~/contactus/web-master";
            public const String ContactUsSubmit = "~/contactus/webmaster-submit";
            public const String RegProd_SelectModel = "~/myproduct/regproduct-select";
            public const String RegProd_SerialNumbers = "~/myproduct/regproduct-serialnumbers";
            public const String RegProd_Other = "~/myproduct/regproduct-other";
            public const String RegProd_Feedback = "~/myproduct/regproduct-feedback";
            public const String RegProd_Submit = "~/myproduct/regproduct-submit";
            public const String RegProd_RequestAccess = "~/myproduct/regproduct-requestaccess";
            public const String RegProd_RequestAccessAck = "~/myproduct/regproduct-requestaccess-ack";
            public const String ProductSupportPrivateDownloads = "~/myproduct/productsupport-privatedownloads";
            public const String ProductSupportPrivateLicense = "~/myproduct/productsupport-privatelicense";
            public const String ProductSupportPrivateVersion = "~/myproduct/productsupport-privateversion";
            //public const String ErpProd_ConfigList = "~/prodregadmin/productmodelconfig";
            //public const String ErpProd_ConfigJPMList = "~/prodregadmin/productmodelconfig-jp-m";
            //public const String ErpProd_ConfigUSMMDList = "~/prodregadmin/productmodelconfig-us-mmd";
            //public const String ErpProd_ConfigDESAList = "~/prodregadmin/productmodelconfig-de-sa";
            //public const String ErpProd_ConfigDetail = "~/prodregadmin/productmodelconfigdetail";
            //public const String ErpProd_ConfigDetail_JPAN = "~/prodregadmin/prodmodelconfigdetail-jp-an";
            //public const String ErpProd_ConfigDetail_JPM = "~/prodregadmin/prodmodelconfigdetail_jp_m";
            //public const String ErpProd_ConfigDetail_USMMD = "~/prodregadmin/prodmodelconfigdetail_us-mmd";
            //public const String ErpProd_ConfigDetail_DESA = "~/prodregadmin/prodmodelconfigdetail_de-sa";


            public const String CfgModel = "~/prodregadmin/modelconfig";
            public const String CfgModel_USMMD = "~/prodregadmin/modelconfig_usmmd";
            public const String CfgModel_JP = "~/prodregadmin/modelconfig_jp";
            public const String CfgModel_JPAN = "~/prodregadmin/modelconfig_jp-an";
            public const String CfgModel_DK = "~/prodregadmin/modelconfig_dk";
            public const String CfgModel_UK = "~/prodregadmin/modelconfig_uk";
            public const String CfgModelDetail = "~/prodregadmin/modelconfigdetail";
            public const String CfgModelDetail_USMMD = "~/prodregadmin/modelconfigdetail_usmmd";
            public const String CfgModelDetail_JP = "~/prodregadmin/modelconfigdetail_jp";
            public const String CfgModelDetail_JPAN = "~/prodregadmin/modelconfigdetail_jp-an";
            public const String CfgModelDetail_UK = "~/prodregadmin/modelconfigdetail_uk";
            public const String CfgModelDetail_DK = "~/prodregadmin/modelconfigdetail_dk";


            public const String ShowMessagePage = "~/message";
            public const String AccessDeniedPage = "~/UnauthorizedAccess";
            public const String SelectTeam = "~/myaccount/selectcompany";
            public const String PrivateDownloadHandler = "/App_Handlers/PrivateDownloadHandler.ashx";
            public const String PIDDocumentHandler = "/App_Handlers/PIDDocumentHandler.ashx";

            public class MyDistributorInfoPages
            {
                public const String Home = "~/mydistributorinfo/home";
                public const String ProductFavorites = "~/mydistributorinfo/distproductfavorites";
                public const String ProductSupport = "~/mydistributorinfo/distproductsupport";
                public const String ProductSupport_ResourceLib = "~/mydistributorinfo/distproductsupport-resourcelibrary";
                public const String ProductSupport_Gallery = "~/mydistributorinfo/distproductsupport-gallery";
                //public const String ProductSupport_SalesInfo = "~/mydistributorinfo/distproductsupport-sales";
                //public const String ProductSupport_Others = "~/mydistributorinfo/distproductsupport-others";
            }
        }

        public class UserRoles
        {
            public const String Admin_SiteAdmin = "siteadmin";
            //public const String Admin_ProductReg_Admin = "adm-prdreg-admin";
            //public const String Admin_ProductReg_Approver = "adm-prdreg-approver";
            public const String Admin_ManageProdReg = "adm-manageprdreg";
            public const String Admin_ManageCompany = "adm-verify-company";
            public const String Admin_ReportMMDFeedback = "adm-report-mmdfeedback";
            public const String Admin_ReportUS = "adm-reportus";
            public const String Admin_ReportJP = "adm-reportjp";
            public const String Admin_ReportTAU = "adm-reporttau";
            public const String User_OrgUser = "accuser";
            public const String User_OrgManager = "accmgr";

            public const String Admin_ManageFileEngg_UKTAU = "adm-mngfile-engg-uk-tau";
            public const String Admin_ManageFileCommEngg_UKTAU = "adm-mngfile-commengg-uk-tau";
            public const String AdminManageJPSupportConfigAndPackages = "adm-mngsupportconfig-package-jp";


            //public const String Admin_SIS = "adm-sis";

            #region new roles
            /// <summary>
            /// Product config roles
            /// </summary>
            public const String Admin_ProductConfig_JPAN = "adm-prdconfig-jp-an";
            public const String Admin_ProductConfig_JPM = "adm-prdconfig-jp-m";
            public const String Admin_ProductConfig_USMMD = "adm-prdconfig-us-mmd";
            public const String Admin_ProductConfig_UKTAU = "adm-prdconfig-uk-tau";
            public const String Admin_ProductConfig_DKM = "adm-prdconfig-dk-m";


            /// <summary>
            /// Product registration roles
            /// </summary>
            public const String Admin_ManageProductReg = "adm-manageprdreg";// Can Delete master model information
            //public const String Admin_ProductReg_JPSW = "adm-prdreg-jpsw";
            public const String Admin_ProductReg_JPM = "adm-prdreg-jpm";
            public const String Admin_ProductReg_JPAN = "adm-prdreg-jpan";
            public const String Admin_ProductReg_ESD = "adm-prdreg-esd";
            //public const String Admin_ProductReg_SIS = "adm-prdreg-sis";
            public const String Admin_ProductReg_USMMD = "adm-prdreg-usmmd";
            public const String Admin_ProductReg_DKSW = "adm-prdreg-dkm";
            #endregion

            public const String Admin_SIS_EMEA = "adm-sis-emea";
            public const String Admin_SIS_US = "adm-sis-us";
            public const String Admin_SIS_Russia = "adm-sis-rus";

            public const String Admin_CustomLink_AU = "adm-lnk_en-au";
            public const String Admin_CustomLink_CN = "adm-lnk_zh-cn";
            public const String Admin_CustomLink_GB = "adm-lnk_en-gb";
            public const String Admin_CustomLink_JP = "adm-lnk_ja-jp";
            public const String Admin_CustomLink_KR = "adm-lnk_ko-kr";
            public const String Admin_CustomLink_RU = "adm-lnk_ru-ru";
            public const String Admin_CustomLink_US = "adm-lnk_en-us";

        }

        public class ResClassKeys
        {
            public const String ProductCfgClassKey = "MNCfg_CustomText";
        }

        public class ResKeyKeys
        {
            public const String ProductCfgPrefixResKey = "Model_";
        }

        public class CookieKeys
        {
            public const String DongleLicSessionCookie = "AnrDglLic";
        }

        public class ContextKeys
        {
            public const String GwCultureCode = "gwCultureCode";
        }
    }
}