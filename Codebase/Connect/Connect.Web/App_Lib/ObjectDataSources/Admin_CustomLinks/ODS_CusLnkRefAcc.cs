﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.CustomLink;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_CustomLinks
{
    public class ODS_CusLnkRefAcc
    {
        public List<CustomLink_AccountRef> SelectByCustomLinkID(Int32 customLinkID, Boolean refreshFromDB)
        {
            return Lib.CustomLink.CustomLink_AccountRefBLL.SelectByCustomLinkID(customLinkID, refreshFromDB);
        }

        public Int32 DeleteRef(Int32 customLinkAccRefID)
        {
            if (customLinkAccRefID < 1) return -1;
            Lib.CustomLink.CustomLink_AccountRefBLL.DeleteByCustomLinkAccRefID(customLinkAccRefID);
            return 1;
        }
    }
}