﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.CustomLink;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_CustomLinks
{
    public class ODS_CustomLinks
    {
        public List<CustomLink_Master> SelectByLinkAdminRoles(Boolean refreshFromDB)
        {
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();

            List<String> linkAdminRoles = new List<string>();
            List<CustomLink_OwnerRole> availableRoles = CustomLink_OwnerRolesBLL.SelectAll(false);
            foreach (CustomLink_OwnerRole or in availableRoles)
            {
                if (LoginUtility.IsAdminRole(or.Role))
                    linkAdminRoles.Add(or.Role);
            }

            return Lib.CustomLink.CustomLink_MasterBLL.SelectByLinkAdminRoles(refreshFromDB, linkAdminRoles);
        }
    }
}