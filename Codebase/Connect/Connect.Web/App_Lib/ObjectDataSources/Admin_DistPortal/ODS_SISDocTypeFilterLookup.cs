﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal
{
    public class ODS_SISDocTypeFilterLookup
    {
        public List<PIDDocTypeFilter> SelectFilters()
        {
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            List<PIDDocTypeFilter> all = PIDDocTypeFilterBLL.SelectAll_ListOnly();
            if (all == null) return null;
            List<String> userRoles = new List<string>();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_EMEA);
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_US)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_US);

            var query = from p in all
                        where user.IsAdministrator || userRoles.Contains(p.DocTypeFilterOwnerRole)
                        select p;
            return query.ToList();
        }

        public List<PIDDocTypeFilter> SelectWithNoFilterItem()
        {
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            List<PIDDocTypeFilter> all = PIDDocTypeFilterBLL.SelectAll_ListOnly();
            if (all == null) return null;
            List<String> userRoles = new List<string>();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_EMEA);
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_US)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_US);

            var query = from p in all
                        where user.IsAdministrator || userRoles.Contains(p.DocTypeFilterOwnerRole)
                        select p;
            List<PIDDocTypeFilter> list = query.ToList();
            PIDDocTypeFilter noFilter = new PIDDocTypeFilter();
            noFilter.DocTypeFilterName = "Show All SIS Document Types";
            noFilter.DocTypeFilterID = 0;
            list.Add(noFilter);
            return list;
        }
    }
}