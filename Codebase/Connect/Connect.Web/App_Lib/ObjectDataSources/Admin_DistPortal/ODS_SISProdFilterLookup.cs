﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_DistPortal
{
    public class ODS_SISProdFilterLookup
    {
        public List<PIDProdFilter> SelectProductFilters()
        {
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            List<PIDProdFilter> all = PIDProdFilterBLL.SelectAll_ListOnly();
            if (all == null) return null;
            List<String> userRoles = new List<string>();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_EMEA);
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_US)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_US);

            var query = from p in all
                        where user.IsAdministrator || userRoles.Contains(p.FilterOwnerRole)
                        select p;
            return query.ToList();
        }

        public List<PIDProdFilter> SelectProductFiltersWithNoFilterItem()
        {
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            List<PIDProdFilter> all = PIDProdFilterBLL.SelectAll_ListOnly();
            if (all == null) return null;
            List<String> userRoles = new List<string>();
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_EMEA);
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_US)) userRoles.Add(KeyDef.UserRoles.Admin_SIS_US);

            var query = from p in all
                        where user.IsAdministrator || userRoles.Contains(p.FilterOwnerRole)
                        select p;
            List<PIDProdFilter> list = query.ToList();
            PIDProdFilter noFilter = new PIDProdFilter();
            noFilter.FilterName = "Show All SIS Products";
            noFilter.ProdFilterID = 0;
            list.Add(noFilter);
            return list;
        }
    }
}