﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_Org
{
    public class ODS_Account
    {
        public Acc_AccountMaster Account_SelectByAccountID(Guid accountID)
        {
            if (accountID.IsNullOrEmptyGuid()) return null;
            return AccountUtility.Admin_AccountInfoGet(accountID);
        }
    }
}