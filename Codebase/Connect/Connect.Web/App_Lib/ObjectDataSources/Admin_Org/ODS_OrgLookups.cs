﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Web.App_Lib.AppCacheFactories;
namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_Org
{
    public class ODS_OrgLookups
    {
        public List<AccountConfig_DistPortal_Status> AccountConfig_DistPortal_Status_SelectAll()
        {
            Lookups_CacheFactory cacheMgr = new Lookups_CacheFactory();
            return cacheMgr.AccountConfig_DistPortalStatus_SelectAll();
        }

        public List<AccountLookup> SelectAll()
        {
            return AccountLookupBLL.SelectAll(false);
        }
    }
}