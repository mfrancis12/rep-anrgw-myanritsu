﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_ProdReg
{
    public class ODS_ProdReg_Data
    {
        public List<ProdReg_Data> ProdReg_Data_Select(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return null;

            String key = String.Format("{0}_{1}", KeyDef.SSKeys.Admin_ProdReg_Data, webToken.ToString());
            List<ProdReg_Data> data = context.Session[key] as List<ProdReg_Data>;
            if (data == null)
            {
                data = ProdReg_DataBLL.SelectByWebToken(webToken);
                context.Session[key] = data;
            }
            return data;
        }
    }
}