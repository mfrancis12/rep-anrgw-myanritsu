﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.Admin_USB
{
    public class ODS_JPUSB
    {
        public DataTable FindJPUSB(String keyword)
        {
            return Lib.DongleDownload.DongleLicBLL.FindDongle(keyword);
        }

        public DataTable SelectByJPUSBNO(String jpUSBNo)
        {
            return Lib.ProductRegistration.ProdReg_Item_JapanUSBBLL.SelectByUSBKey(jpUSBNo);
        }
    }
}