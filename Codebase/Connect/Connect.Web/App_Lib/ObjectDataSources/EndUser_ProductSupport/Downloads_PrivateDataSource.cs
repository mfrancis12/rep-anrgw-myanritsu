﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    public class Downloads_PrivateDataSource
    {
        private List<Downloads_PrivateJapanDlData> JapanNonUSB_GetData(String modelNumber, Int32 gwCultureGroupID, Guid webToken)
        {
            App_Lib.AppCacheFactories.Downloads_JPDL_CacheFactory cacheManger = new AppCacheFactories.Downloads_JPDL_CacheFactory();
            List<Downloads_PrivateJapanDlData> data = cacheManger.GetData(modelNumber.Trim(), gwCultureGroupID, LoginUtility.GetCurrentUserOrSignIn(), webToken);
            return data;
        }

        public List<Downloads_PrivateJapanDlData> JapanNonUSB_SelectByWebToken(Guid webToken)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            ProdReg_DataSource regData = new ProdReg_DataSource();
            //TODO
            //Lib.ProductRegistration.ProdReg_Master regInfo = regData.RegInfo_Select(webToken);
            //if (regInfo == null) return null;
            //TODO
            List<Lib.ProductRegistration.ProdReg_Item> configItems = null;//regData.RegItems_Select(webToken);
            if (configItems == null) return null;

            Int32 gwCultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            List<Downloads_PrivateJapanDlData> downloadData = new List<Downloads_PrivateJapanDlData>();
            foreach (Lib.ProductRegistration.ProdReg_Item item in configItems)
            {
                if (Lib.ProductRegistration.ProdReg_ItemBLL.IsValidSupport(item))
                {
                    List<Downloads_PrivateJapanDlData> data = JapanNonUSB_GetData(item.ModelNumber, gwCultureGroupID, webToken);
                    if (data != null) downloadData.AddRange(data);
                }
            }

            var q = (from s in downloadData select s).Distinct();
            return q.ToList();
        }

        private DataTable JapanVersionTable_GetData(String modelNumber, Int32 gwCultureGroupID)
        {
            App_Lib.AppCacheFactories.Downloads_JPDLVersionTable_CacheFactory cacheManger =
                new AppCacheFactories.Downloads_JPDLVersionTable_CacheFactory();

            return cacheManger.GetData(modelNumber.Trim(), gwCultureGroupID);
        }

        private DataTable JapanVersionTable_GetTableSchema()
        {
            DataTable tbDisplay = new DataTable("JPVersionTBL");
            DataColumn col;

            col = new DataColumn("ModelNumber", typeof(String));
            tbDisplay.Columns.Add(col);

            col = new DataColumn("VersionFileName", typeof(String));
            tbDisplay.Columns.Add(col);

            col = new DataColumn("VersionMessage", typeof(String));
            tbDisplay.Columns.Add(col);

            col = new DataColumn("VersionFileURL", typeof(String));
            tbDisplay.Columns.Add(col);

            tbDisplay.AcceptChanges();

            return tbDisplay;
        }

        public DataTable JapanUSBandNonUSB_GetDownloadVersionTBL(Guid webToken)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || webToken.IsNullOrEmptyGuid()) return null;
            Int32 gwCultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            Boolean isJapanese = gwCultureGroupID == 2;
            ProdReg_DataSource regData = new ProdReg_DataSource();

            //TODO
            List<Lib.ProductRegistration.ProdReg_Item> configItems = null;//regData.RegItems_Select(webToken);
            if (configItems == null) return null;

            DataTable tbVersionTable = JapanVersionTable_GetTableSchema();
            foreach (Lib.ProductRegistration.ProdReg_Item item in configItems)
            {
                DataTable tb = JapanVersionTable_GetData(item.ModelNumber, gwCultureGroupID);
                if (tb != null)
                {
                    foreach (DataRow r in tb.Rows)
                    {
                        DataRow rDisplay = tbVersionTable.NewRow();
                        String modelNumber = r["ModelName"].ToString();
                        rDisplay["ModelNumber"] = modelNumber;
                        if (isJapanese)
                        {
                            String verFileName = r["FileName"].ToString();
                            //if (verFileName.IsNullOrEmptyString()) r["FileNameEng"].ToString();
                            rDisplay["VersionFileName"] = verFileName;

                            String verMessage = r["Message"].ToString();
                            //if (verMessage.IsNullOrEmptyString()) r["MessageEng"].ToString();
                            if (verMessage.Trim().StartsWith("バージョンマッ")) verMessage = String.Empty;
                            rDisplay["VersionMessage"] = verMessage;

                        }
                        else
                        {
                            String verFileName = r["FileNameEng"].ToString();
                            rDisplay["VersionFileName"] = verFileName;

                            String verMessage = r["MessageEng"].ToString();
                            if (verMessage.StartsWith("Before downloading,")) verMessage = String.Empty;
                            rDisplay["VersionMessage"] = verMessage;
                        }

                        if (rDisplay["VersionFileName"].ToString().IsNullOrEmptyString()) continue;
                        // rDisplay["VersionMessage"] = "JUST TESTING version message.";
                        rDisplay["VersionFileURL"] = DownloadUtility.GetMyAnritsuDownloadURL(webToken, modelNumber.Trim(), "0", "dlv");
                        tbVersionTable.Rows.Add(rDisplay);
                    }
                    tbVersionTable.AcceptChanges();
                }
            }
            return tbVersionTable;
        }

        private List<Downloads_PrivateJapanDlData> JapanUSB_GetData(String modelNumber, Int32 gwCultureGroupID, Guid webToken, String usbNo)
        {
            App_Lib.AppCacheFactories.Downloads_JPDLUSB_CacheFactory cacheManger = new AppCacheFactories.Downloads_JPDLUSB_CacheFactory();
            List<Downloads_PrivateJapanDlData> data = cacheManger.GetData(modelNumber.Trim(), gwCultureGroupID,
                LoginUtility.GetCurrentUserOrSignIn(),
                webToken,
                usbNo
                );
            return data;
        }

        public List<Downloads_PrivateJapanDlData> JapanUSB_SelectByWebToken(Guid webToken, String usbNo)
        {
            if (webToken.IsNullOrEmptyGuid() || usbNo.IsNullOrEmptyString()) return null;
            ProdReg_DataSource regData = new ProdReg_DataSource();
            //TODO
            //Lib.ProductRegistration.ProdReg_Master regInfo = regData.RegInfo_Select(webToken);
            //if (regInfo == null) return null;
            //TODO
            List<Lib.ProductRegistration.ProdReg_Item> configItems = null;//regData.RegItems_Select(webToken);
            if (configItems == null) return null;

            Int32 gwCultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            List<Downloads_PrivateJapanDlData> downloadData = new List<Downloads_PrivateJapanDlData>();
            foreach (Lib.ProductRegistration.ProdReg_Item item in configItems)
            {
                List<Downloads_PrivateJapanDlData> data = JapanUSB_GetData(item.ModelNumber, gwCultureGroupID, webToken, usbNo);
                if (data != null) downloadData.AddRange(data);
            }

            var q = (from s in downloadData select s).Distinct();
            return q.ToList();
        }

        #region TAU Download Support Methods

        public bool TAUDownloads_HasData(string modelNumber, DataTable dtTeams, ProdReg_SessionData prodRegInfo,string serialNumber)
        {
            var configItems =
                prodRegInfo.RegItems.Where(reg => reg.ModelConfigType.Equals(ModelConfigTypeInfo.CONFIGTYPE_UK_ESD,
                    StringComparison.CurrentCultureIgnoreCase) &&
                                                  reg.SerialNumber.Equals(serialNumber,
                                                      StringComparison.CurrentCultureIgnoreCase)).ToList();

            if (configItems.Count < 1) return false;
            if (configItems.Any(item => item.StatusCode == ProdReg_ItemStatus.active))
            {
                return Lib.Package.PackageBLL.ActivePackagesByModelNumberCount(modelNumber, dtTeams);
            }
            return false;
        }

        public DataTable SelectTAUPackagesByAccountID(DataTable dtTeams, string modelNumner, bool isLicensePkg,string serialNumber)
        {
            if (HttpContext.Current.Items["hasPrivateDownloadsForTAU"] != null)
            {
                bool hasPrivateDownloadsForTAU;
                try { hasPrivateDownloadsForTAU = (bool)HttpContext.Current.Items["hasPrivateDownloadsForTAU"]; }
                catch { hasPrivateDownloadsForTAU = true; }
                return hasPrivateDownloadsForTAU ?
                    PackageBLL.SelectPackagesByAccountID(dtTeams, modelNumner, isLicensePkg) : null;
            }
            else
            {
                return PackageBLL.SelectPackagesByAccountID(dtTeams, modelNumner, isLicensePkg);
            }
        }

        public DataTable SelectTAUPackageFilesByPackageID(long PackageID)
        {
            return Lib.Package.PackageBLL.SelectPackageFilesByPackageID(PackageID);
        }

        public bool PreConditionCheckForRegistrationApproval(string modelNumber,string configType, List<ProdReg_Item> prodRegItems)
        {
            //get the context product information
            var productConfig = ProductConfigUtility.SelectByModel(false, modelNumber);
            //if no product config found
            if (productConfig == null) return false;

            var modelConfig =
                productConfig.ModelConfigs.FirstOrDefault(model => model.ModelConfigType.Equals(configType, StringComparison.CurrentCultureIgnoreCase));
            //if no region product config found
            if (modelConfig == null) return false;
            if (!modelConfig.Verify_Registration) return true;
            //get product registration status
            if (prodRegItems == null || prodRegItems.Count < 1)
                return false;

            var activeStatusItem =
                prodRegItems.FirstOrDefault(reg =>
                    reg.StatusCode == ProdReg_ItemStatus.active);
            return activeStatusItem != null;
        }

        #endregion

        #region JP Downloads SupportMethods

        public DataTable SelectJpPackageGroups(String modelNumber, String serialNumber, Guid membershipId, String packageType, String modelConfigType, String pkgGroupName, DataTable dtTeams)
        {
            var listAccounts = AccountUtility.GenerateUserTeamIds(true);
            if (listAccounts == null || listAccounts.Rows.Count <= 0) { return null; }
            if (HttpContext.Current.Items["hasPrivateDownloadsForJP"] != null)
            {
                bool hasPrivateDownloadsForJP;
                try { hasPrivateDownloadsForJP = (bool)HttpContext.Current.Items["hasPrivateDownloadsForJP"]; }
                catch { hasPrivateDownloadsForJP = true; }
                return hasPrivateDownloadsForJP ? PackageBLL.SelectJpPackageGroups(modelNumber, serialNumber, membershipId, packageType, modelConfigType, pkgGroupName, dtTeams) : null;
            }
            else
            {
                return PackageBLL.SelectJpPackageGroups(modelNumber, serialNumber, membershipId, packageType, modelConfigType, pkgGroupName, dtTeams);
            }
        }

        public bool JpDownloadsHasPackages(string modelNumber, string serialNumber, out bool hasDownloadpkgs, out bool hasLicensepkgs, out bool hasVersionpkgs)
        {
            hasDownloadpkgs = hasLicensepkgs = hasVersionpkgs = false;
            var regData = new ProdReg_DataSource();
            var configItems = regData.RegItems_Select(AccountUtility.GenerateUserTeamIds(true), modelNumber, serialNumber).Where(c => (c.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JP || c.ModelConfigType == ModelConfigTypeInfo.CONFIGTYPE_JPAN) && c.SerialNumber.Equals(serialNumber, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (configItems.Count < 1) return false;
            if (configItems.Any(item => item.StatusCode == ProdReg_ItemStatus.active || item.StatusCode == ProdReg_ItemStatus.supportrenewsubmitted))
            {
                PackageBLL.SelectJpPackageInfo(modelNumber, serialNumber, LoginUtility.GetCurrentUser(true).MembershipId, ModelConfigTypeInfo.CONFIGTYPE_JP
                   , out hasDownloadpkgs, out hasLicensepkgs, out hasVersionpkgs, AccountUtility.GenerateUserTeamIds(true));
            }
            return (hasVersionpkgs || hasDownloadpkgs || hasLicensepkgs);
        }

        #endregion
    }
}