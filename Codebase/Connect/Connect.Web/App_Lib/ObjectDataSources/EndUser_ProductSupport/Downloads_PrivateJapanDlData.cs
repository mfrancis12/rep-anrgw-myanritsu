﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    [Serializable]
    public class Downloads_PrivateJapanDlData
    {
        public Guid ProdRegWebToken { get; internal set; }
        public Int32 DownloadID { get; internal set; }
        public String CategoryName { get; internal set; }
        public String DownloadType { get; internal set; }
        public String DownloadURL { get; internal set; }
        public String FullDownloadURL { get; internal set; }
        public String DownloadTitle { get; internal set; }
        public String ModelNumber { get; internal set; }
        public DateTime UpdatedDate { get; internal set; }
        public DateTime ReleaseDate { get; internal set; }
        public String Version { get; internal set; }
        public String SizeKB { get; internal set; }
        public String SizeMB { get; internal set; }
        public String FileName { get; internal set; }
        public String FileExt { get; internal set; }
        public String ModelTarget { get; internal set; }
        public String DownloadTarget { get; internal set; }
        public DateTime UpDate { get; internal set; }
        public String ModelName { get; internal set; }
        public String VersionFileName { get; internal set; }
        public Boolean EncodeFlag { get; internal set; }
        public Boolean ShowNewFlag { get; internal set; }
        public String CommentText { get; internal set; }
        public String SoftTitle { get; internal set; }
        public String SoftName { get; internal set; }

        public Downloads_PrivateJapanDlData() { }

        public Downloads_PrivateJapanDlData(Guid webToken, DataRow data) 
        {
            if (data == null || webToken.IsNullOrEmptyGuid()) return;
            ProdRegWebToken = webToken;
            DownloadID = ConvertUtility.ConvertToInt32(data["DownloadID"], 0);

            CategoryName = ConvertUtility.ConvertNullToEmptyString(data["CategoryName"]);
            DownloadType = ConvertUtility.ConvertNullToEmptyString(data["DownloadType"]);
            DownloadURL = ConvertUtility.ConvertNullToEmptyString(data["DownloadURL"]);

            DownloadTitle = ConvertUtility.ConvertNullToEmptyString(data["DownloadTitle"]);
            ModelNumber = ConvertUtility.ConvertNullToEmptyString(data["ModelNumber"]);

            Boolean isUSBRequired = ConvertUtility.ConvertToBoolean(data["JPUSB_Required"], false);
            FullDownloadURL = DownloadUtility.GetMyAnritsuDownloadURL(webToken, ModelNumber, DownloadID.ToString(), isUSBRequired ? "dl3" : "dl2");
            DateTime releaseDate = ConvertUtility.ConvertToDateTime(data["ReleaseDate"], DateTime.MinValue);
            if (releaseDate != DateTime.MinValue) ReleaseDate = releaseDate;

            DateTime updatedDate = ConvertUtility.ConvertToDateTime(data["UpDate"], DateTime.MinValue);
            if (updatedDate != DateTime.MinValue) UpdatedDate = updatedDate;

            Version = ConvertUtility.ConvertNullToEmptyString(data["Version"]);
            SizeKB = ConvertUtility.ConvertNullToEmptyString(data["SizeKB"]);
            SizeMB = ConvertUtility.ConvertNullToEmptyString(data["SizeMB"]);
            FileName = ConvertUtility.ConvertNullToEmptyString(data["FileName"]);
            FileExt = ConvertUtility.ConvertNullToEmptyString(data["FileExt"]);
            ModelTarget = ConvertUtility.ConvertNullToEmptyString(data["ModelTarget"]);
            DownloadTarget = ConvertUtility.ConvertNullToEmptyString(data["DownloadTarget"]);
            
            DateTime upDate = ConvertUtility.ConvertToDateTime(data["UpDate"], DateTime.MinValue);
            if (upDate != DateTime.MinValue) UpDate = upDate;

            ModelName = ConvertUtility.ConvertNullToEmptyString(data["ModelName"]);
            VersionFileName = ConvertUtility.ConvertNullToEmptyString(data["VersionFileName"]);
            EncodeFlag = ConvertUtility.ConvertToBoolean(data["EncodeFlag"], false);
            ShowNewFlag = ConvertUtility.ConvertToBoolean(data["ShowNewFlag"], false);
            CommentText = ConvertUtility.ConvertNullToEmptyString(data["CommentText"]);
            SoftTitle = ConvertUtility.ConvertNullToEmptyString(data["SoftTitle"]);
            SoftName = ConvertUtility.ConvertNullToEmptyString(data["SoftName"]);


        }
    }
}