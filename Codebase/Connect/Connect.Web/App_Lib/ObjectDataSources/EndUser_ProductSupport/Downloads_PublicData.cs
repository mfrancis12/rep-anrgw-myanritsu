﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    [Serializable]
    public class Downloads_PublicData
    {
        public Int32 DownloadID { get; internal set; }
        public Int32 CategoryID { get; internal set; }
        public String CategoryName { get; internal set; }
        public String Title { get; internal set; }
        public String ModelNumber { get; internal set; }
        public String FullDownloadURL { get; internal set; }
        public String SizeKB { get; internal set; }
        public String Version { get; internal set; }
        public DateTime ReleaseDateUTC { get; internal set; }
        public DateTime ModifiedOnUTC { get; internal set; }
        public DateTime CreatedOnUTC { get; internal set; }

         public Downloads_PublicData() { }

         public Downloads_PublicData(DataRow data) 
        {
            if (data == null) return;
            DownloadID = ConvertUtility.ConvertToInt32(data["DownloadID"], 0);
            CategoryID = ConvertUtility.ConvertToInt32(data["CategoryID"], 0);
            CategoryName = ConvertUtility.ConvertNullToEmptyString(data["CategoryName"]);
            ModelNumber = ConvertUtility.ConvertNullToEmptyString(data["ModelNumber"]);
            Title = ConvertUtility.ConvertNullToEmptyString(data["Title"]);
            String downloadURL = ConvertUtility.ConvertNullToEmptyString(data["DownloadURL"]);
            if (!downloadURL.IsNullOrEmptyString())
            {
                if (downloadURL.StartsWith("/"))
                    FullDownloadURL = String.Format("{0}{1}", ConfigUtility.AppSettingGetValue("Connect.GWDownloadBaseUrl"), downloadURL);
            }
            SizeKB = ConvertUtility.ConvertNullToEmptyString(data["SizeKB"]);
            Version = ConvertUtility.ConvertNullToEmptyString(data["Version"]);
            
            DateTime releaseDateUTC = ConvertUtility.ConvertToDateTime(data["ReleaseDateUTC"], DateTime.MinValue);
            if (releaseDateUTC != DateTime.MinValue) ReleaseDateUTC = releaseDateUTC;

            DateTime modifiedOnUTC = ConvertUtility.ConvertToDateTime(data["ModifiedOnUTC"], DateTime.MinValue);
            if (modifiedOnUTC != DateTime.MinValue) ModifiedOnUTC = modifiedOnUTC;

            DateTime createdOnUTC = ConvertUtility.ConvertToDateTime(data["CreatedOnUTC"], DateTime.MinValue);
            if (createdOnUTC != DateTime.MinValue) createdOnUTC = CreatedOnUTC;

        }
    }
}