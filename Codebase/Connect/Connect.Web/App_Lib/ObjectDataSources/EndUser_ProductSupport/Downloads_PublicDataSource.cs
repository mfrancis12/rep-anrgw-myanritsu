﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.Connect.Web.App_Lib.AppCacheFactories;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    public class Downloads_PublicDataSource
    {
        private List<Data.Downloads_PublicData> GetData(string modelNumber, int gwCultureGroupID)
        {
            var cacheManger = new Downloads_GlobalWeb_CacheFactory();
            var data = cacheManger.GetData(modelNumber.Trim(), gwCultureGroupID);
            return data;
        }

        public bool HasDataForSoftwareDownloads(string modelNumber)
        {
            var data = SelectByModelNumber(modelNumber, "pub-sw");
            return data != null && data.Count > 0;
        }

        public bool HasDataForDocumentations(string modelNumber)
        {
            var data = SelectByModelNumber(modelNumber, "pub-doc");
            return data != null && data.Count > 0;
        }

        public List<Data.Downloads_PublicData> SelectByModelNumber(string modelNumber, String docType)
        {
            var gwCultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            var downloadData = GetData(modelNumber, gwCultureGroupID);
            if (downloadData == null) return null;
            switch (docType)
            {
                case "pub-sw":
                    return (from pd in downloadData where pd.CategoryType.Equals("Software") select pd).Distinct().ToList();
                case "pub-doc":
                    return (from pd in downloadData where !pd.CategoryType.Equals("Software") select pd).Distinct().ToList();
                case "all":
                    return (from pd in downloadData where pd.CategoryID != 99999999 select pd).Distinct().ToList();
            }
            return downloadData;
        }
    }
}