﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    public class MyRegisteredProductBasicInfo
    {
        public String MasterModel { get; set; }
        // public String ProductName { get; set; }
        public String ProductSmallImageUrl { get; set; }
        // public String ProductImageUrl { get; set; }
        public String ProductModelAndName { get; set; }
    }
}