﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Net;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Product;
using System.Text;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib.AppCacheFactories;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    public class ProdReg_DataSource
    {
        ProductInfoCacheFactory prodInfoCache = new ProductInfoCacheFactory();
        //Culture/region code constants
        private const int JPERPSOURCE = 10;
        private const int USCULTURECODE = 1;
        private const int JPCULTURECODE = 2;
        public ProdReg_SessionData GetData(DataTable dtteams, string modelNumber, string serialNumber="")
        {
            HttpContext context = HttpContext.Current;
            //return the control if request is not able to aquire session object
            if (context == null || context.Session == null) return null;
           // string sessionKey = "";
           var sessionKey = string.Format("{0}_{1}_{2}", KeyDef.SSKeys.ProdSupport_Data, modelNumber, serialNumber);
            //if serial number is empty
            if (string.IsNullOrEmpty(serialNumber))
            {
                var prod_RegData = new ProdReg_SessionData();
                prod_RegData.RegInfo = ProdReg_MasterBLL.SelectByModelAndTeams(dtteams, modelNumber);
                prod_RegData.RegItems = Lib.ProductRegistration.ProdReg_ItemBLL.SelectByModelAndTeams_AsList(dtteams,
                    modelNumber);
                if (prod_RegData.RegInfo == null || prod_RegData.RegInfo.Count <= 0) return null;
               
                context.Session[sessionKey] = prod_RegData;
                return prod_RegData;
            }
            var data = context.Session[sessionKey] as ProdReg_SessionData;
            //check if Product Registration data exists
            if (data == null)
            {
                data = new ProdReg_SessionData();
                data.RegInfo = ProdReg_MasterBLL.SelectByModelAndTeams(dtteams, modelNumber);
                data.RegItems = Lib.ProductRegistration.ProdReg_ItemBLL.SelectByModelAndTeams_AsList(dtteams,
                    modelNumber);
                if (data.RegInfo != null && data.RegInfo.Count > 0)
                {
                    data.CalCerts = CalCertInfoBLL.Search(modelNumber, string.IsNullOrEmpty(serialNumber) ? data.RegInfo[0].MasterSerial : serialNumber);
                }
                context.Session[sessionKey] = data;
            }
            return data;
        }
      

        public Lib.ProductRegistration.ProdReg_Master RegInfo_Select(DataTable dtteams, string modelNumber, string serialNumber)
        {
            ProdReg_SessionData data = GetData(dtteams, modelNumber, serialNumber);
            if (data == null || data.RegInfo == null || data.RegInfo.Count < 1) return null;
            return data.RegInfo[0];
        }

        public List<Lib.ProductRegistration.ProdReg_Item> RegItems_Select(DataTable dtteams, string modelNumber, string serialNumber)
        {
            ProdReg_SessionData data = GetData(dtteams, modelNumber, serialNumber);
            if (data == null) return null;
            return data.RegItems;
        }
        public List<ProdReg_MasterWithProductInfo> GetMasterRegistrations(String includeType, Boolean refresh)
        {
            if (includeType.IsNullOrEmptyString()) return null;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return null;

            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);

            Guid userMembershipID = LoginUtility.GetCurrentUserOrSignIn().MembershipId;
            Int32 cultureGroupID = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();// not account based acc.GWCultureGroupIDByCountryCod

            String key = String.Format("{0}_{1}_{2}_{3}_{4}", KeyDef.SSKeys.MyProducts_List,
                cultureGroupID,
                acc.AccountID.ToString(),
                userMembershipID,
                includeType);
            List<ProdReg_MasterWithProductInfo> data = context.Session[key] as List<ProdReg_MasterWithProductInfo>;
            if (data == null || refresh)
            {
                data = new List<ProdReg_MasterWithProductInfo>();
                List<ProdReg_Master> pmList = Lib.ProductRegistration.ProdReg_MasterBLL.ForUser_Select(acc.AccountID, userMembershipID
                    , includeType, cultureGroupID);

                if (pmList == null) return null;

                var models = new StringBuilder();
                var mdlLst = new List<string>();
                foreach (var pm in pmList.Where(pm => !mdlLst.Contains(pm.MasterModel)))
                {
                    mdlLst.Add(pm.MasterModel);
                }
                models.Append(string.Join<string>(",", mdlLst));
                var prodInfo = GetProductInfo(mdlLst, models, cultureGroupID);
                foreach (var pm in pmList)
                {
                    var pi = new ProdReg_MasterWithProductInfo();
                    pi.ProdRegID = pm.ProdRegID;
                    pi.WebToken = pm.WebToken;
                    pi.AccountID = pm.AccountID;
                    pi.Description = pm.Description;
                    pi.MasterModel = pm.MasterModel;
                    pi.MasterSerial = pm.MasterSerial;
                    pi.CreatedOnUTC = pm.CreatedOnUTC;
                    ProductConfig pcfg = null;//ProductConfigUtility.SelectByModel(false, pm.MasterModel);
                    //pi.ProductImageURL = pcfg.CustomImageUrl;
                    //pi.ProductSmallImageURL = pcfg.CustomImageUrl;

                    var pinfo = prodInfo != null ? prodInfo.FirstOrDefault(i => i != null && i.ModelNumber.Equals(pm.MasterModel, StringComparison.InvariantCultureIgnoreCase)) : null;
                    //set product image url from sitecore else set it from my anritsu
                    if (pinfo != null && !string.IsNullOrEmpty(pinfo.ProductImageURL) &&
                        !pinfo.ProductImageURL.Equals(ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine"), StringComparison.InvariantCultureIgnoreCase))
                        pi.ProductImageURL = pinfo.ProductImageURL;
                    else
                    {
                        pcfg = ProductConfigUtility.SelectByModel(false, pm.MasterModel);
                        if (null != pcfg && !string.IsNullOrEmpty(pcfg.CustomImageUrl))
                            pi.ProductImageURL = pcfg.CustomImageUrl;
                        else
                            pi.ProductImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;
                    }

                    //set product thumbnail image from sitecore else set produt image url
                    if (pinfo != null && !string.IsNullOrEmpty(pinfo.ProductSmallImageURL) &&
                        !pinfo.ProductSmallImageURL.Equals(ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine"), StringComparison.InvariantCultureIgnoreCase))
                        pi.ProductSmallImageURL = pinfo.ProductSmallImageURL;
                    else if (!string.IsNullOrEmpty(pi.ProductImageURL))
                        pi.ProductSmallImageURL = pi.ProductImageURL;
                    else
                        pi.ProductSmallImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;

                    //if (pi.ProductImageURL.IsNullOrEmptyString()) pi.ProductImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;
                    if (pi.ProductImageURL.StartsWith("/images", StringComparison.InvariantCultureIgnoreCase))
                        pi.ProductImageURL = String.Format("{1}{0}", pi.ProductImageURL, ConfigUtility.AppSettingGetValue("GwdataCdnPath"));

                    //if (pi.ProductSmallImageURL.IsNullOrEmptyString()) pi.ProductSmallImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;
                    if (pi.ProductSmallImageURL.StartsWith("/images", StringComparison.InvariantCultureIgnoreCase))
                        pi.ProductSmallImageURL = String.Format("{1}{0}", pi.ProductSmallImageURL, ConfigUtility.AppSettingGetValue("GwdataCdnPath"));

                    if (pinfo != null)
                    {
                        pi.ProductName = pinfo.ProductName ?? string.Empty;
                        pi.ProductPageURL = pinfo.ProductPageURL ?? string.Empty;
                    }

                    data.Add(pi);
                }
                context.Session[key] = data;
            }
            return data;
        }

        public List<ProdReg_MasterWithProductInfo> GetMasterRegistrations(DataTable teamIds, string mk, string fk, string ok)
        {
            var context = HttpContext.Current;
            //check the session object 
            // if (context == null || context.Session == null) return null;

            // var acc = AccountUtility.GetSelectedAccount(false, true);
            // var userMembershipId = LoginUtility.GetCurrentUserOrSignIn().MembershipId;
            var cultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();

            //var key = String.Format("{0}_{1}_{2}_{3}", KeyDef.SSKeys.MyProducts_List,
            //    cultureGroupId,
            //    acc.AccountID,
            //    userMembershipId);
            //var data = context.Session[key] as List<ProdReg_MasterWithProductInfo>;
            //if (data != null) return data;
            var data = new List<ProdReg_MasterWithProductInfo>();
            //get the registered products of all the teams associated to the logged in user
            var dtProducts = ProdReg_MasterBLL.SelectRegisteredProducts(teamIds, mk, fk, ok);

            if (dtProducts == null || dtProducts.Rows.Count < 1) return null;

            var models = new StringBuilder();
            var mdlLst = new List<string>();

            //add all the registered models to the list
            foreach (var dr in dtProducts.Rows.Cast<DataRow>().Where(dr => !mdlLst.Contains(ConvertUtility.ConvertToString(dr["MasterModel"], ""))))
            {
                mdlLst.Add(ConvertUtility.ConvertToString(dr["MasterModel"], ""));
            }

            models.Append(string.Join<string>(",", mdlLst));
            //get the product information from CMS, My Anritsu DB and ERP.
            var prodInfo = GetProductInfo(mdlLst, models, cultureGroupId);

            foreach (DataRow drProd in dtProducts.Rows)
            {
                var pi = new ProdReg_MasterWithProductInfo
                {
                    //AccountID = ConvertUtility.ConvertToGuid(drProd["AccountID"], Guid.Empty),
                    MasterModel = ConvertUtility.ConvertToString(drProd["MasterModel"], string.Empty)
                };

                var pinfo = prodInfo != null ? prodInfo.FirstOrDefault(i => i != null && i.ModelNumber.Equals(pi.MasterModel, StringComparison.InvariantCultureIgnoreCase)) : null;
                //set product image url from sitecore else set it from my anritsu
                if (pinfo != null && !string.IsNullOrEmpty(pinfo.ProductImageURL) &&
                    !pinfo.ProductImageURL.Equals(ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine"), StringComparison.InvariantCultureIgnoreCase))
                    pi.ProductImageURL = pinfo.ProductImageURL;
                else
                {
                    var pcfg = ProductConfigUtility.SelectByModel(false, pi.MasterModel);
                    if (null != pcfg && !string.IsNullOrEmpty(pcfg.CustomImageUrl))
                        pi.ProductImageURL = pcfg.CustomImageUrl;
                    else
                        pi.ProductImageURL = ProductInfo.ProductDefaultImageUrl;
                }

                //set product thumbnail image from sitecore else set produt image url
                if (pinfo != null && !string.IsNullOrEmpty(pinfo.ProductSmallImageURL) &&
                    !pinfo.ProductSmallImageURL.Equals(ConfigUtility.AppSettingGetValue("Connect.AnritsuLogoWithTagLine"), StringComparison.InvariantCultureIgnoreCase))
                    pi.ProductSmallImageURL = pinfo.ProductSmallImageURL;
                else if (!string.IsNullOrEmpty(pi.ProductImageURL))
                    pi.ProductSmallImageURL = pi.ProductImageURL;
                else
                    pi.ProductSmallImageURL = ProductInfo.ProductDefaultImageUrl;

                //if (pi.ProductImageURL.IsNullOrEmptyString()) pi.ProductImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;
                if (pi.ProductImageURL.StartsWith("/images", StringComparison.InvariantCultureIgnoreCase))
                    pi.ProductImageURL = String.Format("{1}{0}", pi.ProductImageURL, ConfigUtility.AppSettingGetValue("GwdataCdnPath"));

                //if (pi.ProductSmallImageURL.IsNullOrEmptyString()) pi.ProductSmallImageURL = Lib.Product.ProductInfo.ProductDefaultImageUrl;
                if (pi.ProductSmallImageURL.StartsWith("/images", StringComparison.InvariantCultureIgnoreCase))
                    pi.ProductSmallImageURL = String.Format("{1}{0}", pi.ProductSmallImageURL, ConfigUtility.AppSettingGetValue("GwdataCdnPath"));

                //MA-1442
                if (pi.ProductSmallImageURL.Contains(ConfigUtility.AppSettingGetValue("StaticCdnPath")))
                {
                    pi.ProductSmallImageURL = pi.ProductSmallImageURL.Replace(ConfigUtility.AppSettingGetValue("StaticCdnPath"), 
                        $"{ConfigUtility.AppSettingGetValue("GwdataCdnPath")}/images/legacy-images");
                }
                else if (pi.ProductSmallImageURL.Contains(ConfigUtility.AppSettingGetValue("CdnOriginName")))
                {
                    pi.ProductSmallImageURL = pi.ProductSmallImageURL.Replace(ConfigUtility.AppSettingGetValue("CdnOriginName"),
                        ConfigUtility.AppSettingGetValue("GwdataCdnPath"));
                }
                if (pi.ProductSmallImageURL.StartsWith("http://")) { pi.ProductSmallImageURL = pi.ProductSmallImageURL.Replace("http://", "https://"); }

                if (pinfo != null)
                {
                    pi.ProductName = pinfo.ProductName ?? string.Empty;
                    pi.ProductPageURL = pinfo.ProductPageURL ?? string.Empty;
                }

                data.Add(pi);
            }
            //context.Session[key] = data;
            return data;
        }

        private List<ProductInfo> GetProductInfo(List<string> prods, StringBuilder models, int cultureGroupID)
        {
            if (string.IsNullOrEmpty(models.ToString())) return null;
            var prodInfo = new List<ProductInfo>();
            //Get Prod Info from sitecore API
            var productInfoFromCMS =  ProductInfo_Api(cultureGroupID, models);
            //filter the products
            var filterProducts = GetFilteredModels(productInfoFromCMS, prods);
            //get producst info from default region en-US
            var defaultProductInfoFromCMS =  cultureGroupID == 1 ? productInfoFromCMS : ProductInfo_Api(1, filterProducts);
            var erpProductInfo = new Dictionary<string, ProductInfo>();
            foreach (var prod in prods)
            {
                //check whether prodinfo found
                var pinfo = (productInfoFromCMS != null ? productInfoFromCMS.FirstOrDefault(i => i.ModelNumber.Equals(prod, StringComparison.InvariantCultureIgnoreCase)) : null) ??
                            (defaultProductInfoFromCMS != null ? defaultProductInfoFromCMS.FirstOrDefault(i => i.ModelNumber.Equals(prod, StringComparison.InvariantCultureIgnoreCase)) : null);
                if (pinfo == null)
                {
                    //get it from ERP DB
                    //check in dictionay
                    if (erpProductInfo.ContainsKey(prod))
                        pinfo = erpProductInfo[prod];
                    else
                    {
                        //get the ERP model info from cache
                        if (cultureGroupID == USCULTURECODE || cultureGroupID == JPCULTURECODE)
                        {
                            var prodList = prodInfoCache.GetData();
                            pinfo = ProductInfoBLL.SelectByModelNumberInErpDBCache(prod, prodList, SiteUtility.BrowserLang_IsJapanese() ? JPERPSOURCE : 10);
                            if (!erpProductInfo.ContainsKey(prod) && pinfo != null)
                                erpProductInfo.Add(prod, pinfo);
                        }
                    }
                }
                prodInfo.Add(pinfo);
            }
            return prodInfo;
        }

        private StringBuilder GetFilteredModels(List<ProductInfo> productInfoFromCMS, List<string> prods)
        {
            //get cms model list
            if (productInfoFromCMS == null || prods == null) return null;
            var cmsModels = productInfoFromCMS.Select(p => p.ModelNumber).ToList();
            var filteredModels = prods.Where(prod => !cmsModels.Contains(prod)).ToList();
            if (filteredModels.Count > 0)
            {
                var models = new StringBuilder();
                models.Append(string.Join<string>(",", filteredModels));
                return models;
            }
            return null;
        }

        private static List<ProductInfo> ProductInfo_Api(Int32 cultureGroupID, StringBuilder models)
        {
            return !string.IsNullOrEmpty(Convert.ToString(models)) ? Lib.Product.ProductInfoBLL.GetProductInfo_FromCMSDB(cultureGroupID.ToString(), models.ToString().TrimEnd(new char[] { ',' })) : null;
        }


        public void UpdateProdReg_Description(Int32 ProdRegID, String Description)
        {
            String desc = Description.ConvertNullToEmptyString();
            if (desc == String.Empty) desc = "..";
            Lib.ProductRegistration.ProdReg_MasterBLL.ForUser_UpdateDesc(ProdRegID, desc);

        }

        public List<MyRegisteredProductBasicInfo> GetMasterRegistrationsBasic(String includeType, Boolean refresh)
        {
            List<ProdReg_MasterWithProductInfo> myProducts = GetMasterRegistrations(includeType, refresh);
            if (myProducts == null) return null;
            var query = (from p in myProducts
                         group p by new { MasterModel = p.MasterModel.ToLower(), p.ProductName, p.ProductSmallImageURL, p.ProductImageURL } into grp
                         select grp.FirstOrDefault());
            var query2 = from p in query
                         select new MyRegisteredProductBasicInfo()
                         {
                             MasterModel = p.MasterModel,
                             //ProductName = HttpUtility.HtmlDecode(p.ProductName),
                             ProductSmallImageUrl = p.ProductSmallImageURL,
                             //ProductImageUrl = p.ProductImageURL,
                             ProductModelAndName = p.ProductName.IsNullOrEmptyString() ? p.MasterModel : String.Format("{0} - {1}", p.MasterModel, HttpUtility.HtmlDecode(p.ProductName))
                         };

            return query2.OrderBy(p => p.MasterModel).ToList();
        }

        public List<MyRegisteredProductBasicInfo> GetMasterRegistrationsBasic(DataTable dtTeamIds, ref HttpStatusCode clientStatusCode, string mk, string fk, string ok)
        {
            
            var myProducts = GetMasterRegistrations(dtTeamIds, mk, fk, ok);
            if (myProducts == null) return null;
            var query2 = from p in myProducts
                         select new MyRegisteredProductBasicInfo()
                         {
                             MasterModel = p.MasterModel,
                             ProductSmallImageUrl = p.ProductSmallImageURL,
                            // ProductModelAndName= Anritsu.Connect.Data.DAProductInfo.GetPIMMappedModelProductName(p.MasterModel)
                              ProductModelAndName = p.ProductName.IsNullOrEmptyString() ? Anritsu.Connect.Data.DAProductInfo.GetPIMMappedModelProductName(p.MasterModel) : String.Format("{0} - {1}", p.MasterModel, HttpUtility.HtmlDecode(p.ProductName))
                         };
            var result = query2.OrderBy(p => p.MasterModel).ToList();
            clientStatusCode = (result.Count < 1) ? HttpStatusCode.NotFound : HttpStatusCode.OK;
            return result;
        }

        public List<ProdReg_MasterWithProductInfo> GetMasterRegistrationsByModel(String includeType, Boolean refresh, String modelNumber)
        {
            List<ProdReg_MasterWithProductInfo> myProducts = GetMasterRegistrations(includeType, refresh);
            if (myProducts == null) return null;
            var query = (from p in myProducts
                         where p.MasterModel.Equals(modelNumber, StringComparison.InvariantCultureIgnoreCase)
                         select p);
            return query.ToList();
        }
    }
}