﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    [Serializable]
    public class ProdReg_MasterWithProductInfo
    {
        public Int32 ProdRegID { get; set; }
        public Guid WebToken { get; set; }
        public Guid AccountID { get; set; }
        public String Description { get; set; }
        public String MasterModel { get; set; }
        public String MasterSerial { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public String ProductName { get; set; }
        public String ProductImageURL { get; set; }
        public String ProductSmallImageURL { get; set; }
        public String ProductPageURL { get; set; }
    }
}