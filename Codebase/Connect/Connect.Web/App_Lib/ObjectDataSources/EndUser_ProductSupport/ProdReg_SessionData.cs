﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    [Serializable]
    public class ProdReg_SessionData
    {
        //public Guid WebToken { get; set; }
        public List<Lib.ProductRegistration.ProdReg_Master> RegInfo { get; set; }
        public List<Lib.ProductRegistration.ProdReg_Item> RegItems { get; set; }
        public List<Lib.Product.CalCertInfo> CalCerts { get; set; }
    }
}