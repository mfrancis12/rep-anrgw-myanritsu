﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    [Serializable]
    public class Warranty_Data
    {
        public Guid WebToken { get; set; }
        public List<Lib.Product.WarrantyInfo> WarrantyData { get; set; }
        public Boolean ShowWarrantyInfo { get; set; }
    }
}