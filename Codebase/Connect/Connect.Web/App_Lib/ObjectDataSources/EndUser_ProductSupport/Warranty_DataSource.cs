﻿using System;
using System.Collections.Generic;
using System.Web;
using Anritsu.Connect.Lib.Product;
using System.Data;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport
{
    public class Warranty_DataSource
    {
        public List<WarrantyInfo> Select(DataTable dtTeams, string modelNumber, string serialNumber, out Boolean showWarrantyInfo)
        {
            //showWarrantyInfo = true;
            //if(webToken.IsNullOrEmptyGuid()) return null;
            //HttpContext context = HttpContext.Current;
            //if (context == null || context.Session == null) return null;

            Warranty_Data data = null;//context.Session[KeyDef.SSKeys.ProdSupport_WarrantyData] as Warranty_Data;
            if (data == null)
            {
                data = new Warranty_Data();
                //data.WebToken = webToken;
                data.WarrantyData = WarrantyInfoBLL.WarrantyInfo_SelectByProdRegWebToken(dtTeams, modelNumber, serialNumber, out showWarrantyInfo);
                data.ShowWarrantyInfo = showWarrantyInfo;
                // context.Session[KeyDef.SSKeys.ProdSupport_WarrantyData] = data;
            }
            showWarrantyInfo = data.ShowWarrantyInfo;
            return data.WarrantyData;
        }

        public List<WarrantyInfo> Select(DataTable dtTeams, string modelNumber, string serialNumber)
        {
            var showWarrantyInfo = true;
            return Select(dtTeams, modelNumber, serialNumber, out showWarrantyInfo);
        }

        public Boolean HasData(DataTable dtTeams, string modelNumber, string serialNumber, out Boolean showWarrantyInfo)
        {
            List<WarrantyInfo> lst = Select(dtTeams, modelNumber, serialNumber, out showWarrantyInfo);
            return lst != null && lst.Count > 0;
        }
    }
}