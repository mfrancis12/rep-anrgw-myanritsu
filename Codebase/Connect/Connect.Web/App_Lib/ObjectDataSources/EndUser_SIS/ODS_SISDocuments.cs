﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS
{
    public class ODS_SISDocuments
    {
        private Int32 _PID = 0;
        private List<PIDDocumentInfo> _AllDocs;
        private List<PIDDocumentInfo> AllDocs
        {
            get
            {
                if (_AllDocs == null)
                {
                    RefreshData();
                }
                return _AllDocs;
            }
        }

        private Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        private void RefreshData()
        {
            if (_PID < 1) return;
            Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            Int32 sisLangID = GetSISLangID();
            _AllDocs = ApplySISDocTypeFilter(acc, sisLangID, PIDDocumentInfoBLL.SelectByPID(_PID));
            if (_AllDocs != null)
            {
                String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
                foreach (PIDDocumentInfo doc in _AllDocs)
                {
                    doc.DocumentType = PIDDocumentTypeBLL.SelectDocTypeNameByID(doc.DocumentTypeID, browserLangFromApp);
                    doc.Language = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("common_pid", String.Format("pidlang_{0}", doc.LanguageID)));
                    doc.AccessType = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("common_pid", String.Format("accesstype_{0}", doc.AccessTypeID)));
                }
            }
        }

        public List<PIDDocumentInfo> SelectDocs(Int32 pid, String catName)
        {
            if (pid != _PID)
            {
                _PID = pid;
                RefreshData();
            }
            List<PIDDocumentInfo> list = AllDocs;
            if (list == null) return null;
            
            return PIDDocumentInfoBLL.FilterByCategory(list, catName);

            //_PID = pid;
            //List<PIDDocumentInfo> list = AllDocs;
            //if (list == null) return null;
            //List<PIDDocumentInfo> results = PIDDocumentInfoBLL.FilterByCategory(list, catName);

            //String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            //if (results == null) return null;
            //foreach (PIDDocumentInfo doc in results)
            //{
            //    doc.DocumentType = PIDDocumentTypeBLL.SelectDocTypeNameByID(doc.DocumentTypeID, browserLangFromApp);
            //    doc.Language = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("common_pid", String.Format("pidlang_{0}", doc.LanguageID)));
            //    doc.AccessType = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("common_pid", String.Format("accesstype_{0}", doc.AccessTypeID)));
            //}
            //return results;

        }

        private List<PIDDocumentInfo> ApplySISDocTypeFilter(Acc_AccountMaster acc, Int32 sisLangID, List<PIDDocumentInfo> originalList)
        {
            if (acc == null || acc.AccountID.IsNullOrEmptyGuid() || acc.Config_DistributorPortal == null) return null;
            if (originalList == null) return null;
            if (acc.Config_DistributorPortal.SIS_DocTypeFilter == 0) return originalList;//no filter

            PIDDocTypeFilter filter = PIDDocTypeFilterBLL.SelectByFilterID(acc.Config_DistributorPortal.SIS_DocTypeFilter, sisLangID);
            if (filter == null || filter.FilterItems == null || filter.FilterItems.Count < 1)
                return null;

            var query = from doc in originalList
                        join filterItem in filter.FilterItems on doc.DocumentTypeID equals filterItem.SIS_DocTypeID
                        select doc;

            return query.ToList();
        }

        //too complicate for now due to various video formats and browser support
        //public List<PID_GalleryItem> SelectPIDGallery(Int32 pid)
        //{
        //    _PID = pid;
        //    List<PIDDocumentInfo> list = AllDocs;
        //    if (list == null) return null;
        //    List<String> pidGalleryExts = ConfigUtility.AppSettingGetValue("PID.GalleryExts").Split('|').ToList();
            
        //    String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
        //    if (list == null) return null;
        //    var query = from pidDoc in list
        //                where !pidDoc.FileLink.IsNullOrEmptyString() || pidGalleryExts.Contains(Path.GetExtension(pidDoc.FileName))
        //                select pidDoc;

        //    List<PIDDocumentInfo> results = query.ToList();
        //    if (results == null) return null;
        //    List<PID_GalleryItem> pidGallery = new List<PID_GalleryItem>();
        //    PID_GalleryItem item;
        //    List<String> pidGalleryImageExts = ConfigUtility.AppSettingGetValue("PID.GalleryExts.Images").Split('|').ToList();
        //    foreach (PIDDocumentInfo doc in results)
        //    {
        //        item = new PID_GalleryItem();
        //        item.HrefUrl = doc.FullDownloadURL;
        //        item.FileName = doc.DocumentName;
        //        String fileExt = String.Empty;
        //        if(!doc.FileName.IsNullOrEmptyString()) 
        //            fileExt = Path.GetExtension(doc.FileName).ToLowerInvariant();

        //        if (pidGalleryImageExts.Contains(fileExt))
        //        {
        //            item.ImageUrl = doc.FullDownloadURL;
        //            item.FileExt = fileExt;
        //        }
        //        else
        //        {
        //            item.ImageUrl = "/images/legacy-images/apps/connect/img/crystal-clear-app-browser.png";  
        //        }
        //        pidGallery.Add(item);
        //    }
        //    return pidGallery;
        //}
    }
}