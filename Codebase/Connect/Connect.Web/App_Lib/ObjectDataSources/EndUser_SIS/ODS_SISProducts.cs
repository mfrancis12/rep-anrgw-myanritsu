﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.PID;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS
{
    public class ODS_SISProducts
    {
        public ODS_SISProducts()
        {
     
        }

        private Int32 GetSISLangID()
        {
            String browserLangFromApp = SiteUtility.BrowserLang_GetFromApp().ToLowerInvariant();
            return PIDUtility.PID_ConvertToSISLangID(browserLangFromApp);
        }

        public List<PIDProductInfo> SelectAll()
        {
            return SelectByKeyword(String.Empty);
        }

        public List<PIDProductInfo> SelectByKeyword(String keyword)
        {
            Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            if(acc.Config_DistributorPortal == null) return null;

            Int32 sisLangID = GetSISLangID();
            List<PIDProductInfo> list = PIDProductInfoBLL.SearchSISProducts(sisLangID, keyword);

            return ApplySISProductFilter(acc, sisLangID, list);
        }

        public List<PIDProductInfo> SelectSISFavoriteProducts()
        {
            Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            if (acc == null || acc.Config_DistributorPortal == null) return null;

            Guid accID = acc.AccountID;
            if (accID.IsNullOrEmptyGuid()) return null;
            Int32 sisLangID = GetSISLangID();
            List<PIDProductInfo> list = PIDProductInfoBLL.GetFavoriteProducts(sisLangID, accID);

            return ApplySISProductFilter(acc, sisLangID, list);
        }

        public List<PIDProductInfo> ApplySISProductFilter(Acc_AccountMaster acc, Int32 sisLangID, List<PIDProductInfo> originalList)
        {
            if (acc == null || acc.AccountID.IsNullOrEmptyGuid() || acc.Config_DistributorPortal == null) return null;
            if (originalList == null) return null;
            //if (acc.Config_DistributorPortal.SIS_ProdFilter == 0) return originalList;//no filter
          
            List<Int32> accesstype = new List<Int32>();
            if (acc.Config_DistributorPortal.SIS_AccessType_Public)
                accesstype.Add(2);
            if (acc.Config_DistributorPortal.SIS_AccessType_Private)
                accesstype.Add(3);

            //List<PIDProductInfo> viewList = new List<PIDProductInfo>();
            if (acc.Config_DistributorPortal.SIS_ProdFilter == 0)
                            return (from prod in originalList
                            where accesstype.Contains(prod.AccessType)
                            select prod).ToList();
            

            
            PIDProdFilter filter = PIDProdFilterBLL.SelectByFilterID(acc.Config_DistributorPortal.SIS_ProdFilter, sisLangID);
            if (filter == null || filter.FilterItems == null || filter.FilterItems.Count < 1)
                return null;

            var query = from product in originalList
                        join filterItem in filter.FilterItems on product.PID equals filterItem.SIS_PID
                        where accesstype.Contains(product.AccessType)
                        select product;

             return query.ToList();
        }

        public PIDProductInfo SelectByPID(int pid)
        {
            if (pid < 1) return null;
            Lib.Account.Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
            if (acc == null
                || acc.Config_DistributorPortal == null
                || !acc.Config_DistributorPortal.StatusCode.Equals("active", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }
            Int32 sisLangID = GetSISLangID();
            PIDProductInfo pinfo = PIDProductInfoBLL.SelectByPID(pid, sisLangID);
            if (pinfo == null) return null;
            if (pinfo.AccessType == 2 && !acc.Config_DistributorPortal.SIS_AccessType_Public)
                return null;
            if (pinfo.AccessType == 3 && !acc.Config_DistributorPortal.SIS_AccessType_Private)
                return null;
            return pinfo;

        }

        //public PIDProductInfoDetails SelectPIDProductDetails(Int32 pid)
        //{
        //    PIDProductInfoDetails details = PIDProductInfoDetailsBLL.SelectDetails(pid);
        //    if (details == null) return null;
        //    App_Lib.
        //}
    }
}