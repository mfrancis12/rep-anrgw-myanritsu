﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_SIS
{
    [Serializable]
    public class PID_GalleryItem
    {
        public String HrefUrl { get; set; }
        public String ImageUrl { get; set; }
        public String FileExt { get; set; }
        public String FileName { get; set; }
    }
}