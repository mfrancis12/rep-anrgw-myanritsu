﻿using System;
using System.Web.Compilation;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.Providers
{
    public class CmsResourceProviderFactory : ResourceProviderFactory
    {
        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            return new ConnectResourceProvider(classKey);
        }

        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            string classKey = virtualPath;
            if (!string.IsNullOrEmpty(virtualPath))
            {
                virtualPath = virtualPath.Remove(0, 1);
                classKey = virtualPath.Remove(0, virtualPath.IndexOf('/') + 1);
            }
            return new ConnectResourceProvider(classKey);
        }
    }

    public class ConnectResourceProvider : IResourceProvider, IDisposable
    {
        private string _ClassKey = string.Empty;
        internal static List<ConnectResourceProvider> LoadedProviders = new List<ConnectResourceProvider>();

        private bool _Disposed;
        protected bool Disposed
        {
            get
            {
                lock (this)
                {
                    return _Disposed;
                }
            }
        }

        public ConnectResourceProvider(string classKey)
        {
            this._ClassKey = classKey;
            //if (classKey.Contains(".aspx") || classKey == "ThisPage")
            //{
            //    this._ClassKey = GetCurrentPageUrl();
            //}

            lock (LoadedProviders)
            {
                LoadedProviders.Add(this);
            }
        }

        ~ConnectResourceProvider()
        {
            Cleanup();
        }

        protected void Cleanup()
        {
            try
            {
                //if (this._ResourceCache != null) this._ResourceCache.Clear();
            }
            finally
            {
                //base.Cleanup();
            }
        }

        public void ClearCache()
        {
            //if (this._ResourceCache != null) this._ResourceCache.Clear();
            //App_Lib.AppCacheFactories.ResourceCacheFactory resDataCache = new AppCacheFactories.ResourceCacheFactory();
            //resDataCache.TouchCacheFile();
        }

        public static void UnloadProviders()
        {
            // copy the collection to clear cache for providers
            // this helps performance by not holding onto lock
            // while looping
            List<ConnectResourceProvider> tmpCollection;
            lock (LoadedProviders)
            {
                tmpCollection = new List<ConnectResourceProvider>(LoadedProviders);
            }
            foreach (ConnectResourceProvider provider in tmpCollection)
            {
                provider.ClearCache();
            }
        }

        

        public static void RemoveFromCache(String contentType, String resourceKey, CultureInfo ci)
        {
            App_Lib.AppCacheFactories.ResourceCacheFactory resDataCache = new AppCacheFactories.ResourceCacheFactory();
            resDataCache.RemoveFromCache(contentType, resourceKey, ci.Name);
        }

        #region IDisposable Members

        public void Dispose()
        {
            lock (this)
            {
                if (_Disposed == false)
                {
                    Cleanup();
                    _Disposed = true;

                    GC.SuppressFinalize(this);
                }
            }
        }

        #endregion

        #region IResourceProvider Members

        public object GetObject(string resourceKey, CultureInfo culture)
        {
            try
            {
                if (Disposed) throw new ObjectDisposedException("ResourceProvider object is already disposed.");
                if (string.IsNullOrEmpty(resourceKey)) throw new ArgumentNullException("resourceKey");
                if (culture == null || String.IsNullOrEmpty(culture.Name)) culture = CultureInfo.CurrentUICulture;
                if (culture == null || String.IsNullOrEmpty(culture.Name)) culture = new CultureInfo("en");
                string contentType = _ClassKey;
                if (_ClassKey.Contains(".aspx") || _ClassKey == "ThisPage")
                {
                    contentType = GetCurrentPageUrl();
                }
               
                //if (contentType.Contains("ASPxperienceStringId.Pager_SummaryFormat") || resourceKey.Contains("ASPxperienceStringId.Pager_SummaryFormat"))
                //{
                //    string contentTypeClassName = contentType;
                //}

                App_Lib.AppCacheFactories.ResourceCacheFactory resDataCache = new AppCacheFactories.ResourceCacheFactory();
                return resDataCache.GetContentString(contentType, resourceKey.Trim(), culture.Name);
            }
            catch (Exception ex)
            {
                throw ex;
                //return string.Empty;
            }
        }

        public IResourceReader ResourceReader
        {
            get { throw new NotImplementedException(); }

        }

        #endregion

        public static string GetCurrentPageUrl()
        {
            if (HttpContext.Current == null) return null;
            string relUrl = "~" + HttpContext.Current.Request.Path;
            return relUrl.ToLowerInvariant();
        }
    }
}