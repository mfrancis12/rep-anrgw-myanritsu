﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Lib.Providers
{
    public class ConnectRoleProvider : System.Web.Security.RoleProvider
    {
        public const string PROVIDERNAME = "ConnectMembershipProvider";
        private string _ApplicationName = "Anritsu.Connect";
        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            List<String> roles = Lib.BLL.BLLSec_Role.SelectAllJustRoles();
            if (roles == null) return new List<String>().ToArray();
            return roles.ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            if(!HttpContext.Current.User.Identity.IsAuthenticated) return null;
            Boolean redirectIfNoAccount = !SiteUtility.GetPageUrl().StartsWith(KeyDef.UrlList.ManageOrg);
            Guid selectedAccountID = AccountUtility.GetSelectedAccountID(redirectIfNoAccount);
            if (selectedAccountID.IsNullOrEmptyGuid()) return new List<String>().ToArray();

            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUser(false);
            if (user == null) return null;

            List<String> roles = Lib.BLL.BLLSec_Role.SelectJustRoles(user.MembershipId, selectedAccountID);
            if (user.IsAdministrator) roles.Add("siteadmin");
            if (roles == null) return new List<String>().ToArray();
            return roles.ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string[] roles = GetRolesForUser(username);
            if (roles == null || roles.Length < 1) return false;
            return roles.Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            List<String> roles = Lib.BLL.BLLSec_Role.SelectAllJustRoles();
            if (roles == null) return false;
            foreach (String r in roles)
            {
                if (r.Equals(roleName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}