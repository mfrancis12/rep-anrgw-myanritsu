﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Anritsu.Connect.Web.App_Lib
{
    [Serializable]
    public class SearchOptionAndResults
    {
        public Lib.SearchByOption SearchOptions { get; set; }
        public DataTable SearchResults { get; set; }
    }
}