﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_Distributor : BP_MyAnritsu
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                AnrCommon.CoreLib.WebUtility.DisableBrowserCache();
                Acc_AccountMaster acc = AccountUtility.GetSelectedAccount(false, true);
                
                if (acc == null
                    || acc.Config_DistributorPortal == null
                    || !acc.Config_DistributorPortal.StatusCode.Equals("active")
                    || (!acc.Config_DistributorPortal.SIS_AccessType_Private && !acc.Config_DistributorPortal.SIS_AccessType_Public))
                {
                    throw new HttpException(404, "Page not found.");
                }
            }
            base.OnLoad(e);

        }
    }
}