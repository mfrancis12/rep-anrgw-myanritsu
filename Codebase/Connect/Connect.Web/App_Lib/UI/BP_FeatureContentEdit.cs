﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_FeatureContentEdit : BP_MyAnritsu
    {
        private Lib.UI.Site_Module _ModuleData = null;
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.ModuleID], 0);
            }
        }

        public Lib.UI.Site_Module GetModuleData(Boolean refresh)
        {
            if (refresh || _ModuleData == null)
            {
                _ModuleData = Lib.BLL.BLLSite_Module.SelectByModuleID(ModuleID);
                if (_ModuleData == null) throw new HttpException(404, "Page not found.");
            }
            return _ModuleData;
        }
    }
}