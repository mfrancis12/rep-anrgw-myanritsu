﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_JapanUSBAdmin : BP_MyAnritsu
    {
        public String JapanUSBNo
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.Admin_USBNo]);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminMode()) throw new HttpException(404, "Page not found.");
            if (!IsPostBack)
            {

            }

            base.OnInit(e);
        }

        //public void InitTabs(TabStrip tabCtrl, JapanUSBKeyAdminTab selectedTabType)
        //{
        //    TabStripTab tab;
        //    tab = new TabStripTab();
        //    tab.Text = GetTabText(JapanUSBKeyAdminTab.FindUSB);
        //    tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_List;
        //    tabCtrl.Tabs.Add(tab);
        //    if (selectedTabType == JapanUSBKeyAdminTab.FindUSB)
        //        tabCtrl.SelectedTab = tab;


        //    tab = new TabStripTab();
        //    tab.Text = GetTabText(JapanUSBKeyAdminTab.USBAddNew);
        //    tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_AddNew;
        //    tabCtrl.Tabs.Add(tab);
        //    if (selectedTabType == JapanUSBKeyAdminTab.USBAddNew)
        //        tabCtrl.SelectedTab = tab;

        //    if (selectedTabType == JapanUSBKeyAdminTab.USBDetail)
        //    {
        //        String query = String.Format("?{0}={1}", KeyDef.QSKeys.Admin_USBNo, JapanUSBNo.ToString());
        //        tab = new TabStripTab();
        //        tab.Text = GetTabText(JapanUSBKeyAdminTab.USBDetail);
        //        tab.NavigateUrl = KeyDef.UrlList.SiteAdminPages.AdminJapanUSB_Detail + query;
        //        tabCtrl.Tabs.Add(tab);
        //        tabCtrl.SelectedTab = tab;
        //    }
        //}

        //private String GetTabText(JapanUSBKeyAdminTab tabType)
        //{
        //    String resKey = "TabRegInfo";
        //    switch (tabType)
        //    {
        //        case JapanUSBKeyAdminTab.FindUSB:
        //            resKey = "TabFindUSB";
        //            break;
        //        case JapanUSBKeyAdminTab.USBAddNew:
        //            resKey = "TabUSBAddNew";
        //            break;
        //        case JapanUSBKeyAdminTab.USBDetail:
        //            resKey = "TabUSBDetail";
        //            break;
        //    }
        //    Object txtObj = GetGlobalResourceObject("JapanUSBKeyAdminTab", resKey).ToString();
        //    if (txtObj == null) return String.Empty;
        //    return txtObj.ToString();
        //}
    }
}