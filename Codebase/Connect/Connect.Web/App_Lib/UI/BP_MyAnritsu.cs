﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_MyAnritsu : System.Web.UI.Page
    {
        private bool _LoadModules = true;
        public bool LoadModules { get { return _LoadModules; } set { _LoadModules = value; } }
        //public const string PostBackEventTarget = "__EVENTTARGET";
        protected override void OnInit(EventArgs e)
        {
            //CheckPageSecurityAndCache();
            SetPageTitle(String.Empty);

            base.OnInit(e);
            DisableClientCaching();
            if (LoadModules)
            {
                InitModules();
            }
            //if (Response.ContentType.Equals("text/html", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    Response.Filter = new App_Lib.UI.GWSsoFilter(Response.Filter);
            //}

        }

        private void InitModules()
        {
            String pageUrl = SiteUtility.GetPageUrl();
            List<Lib.UI.Site_ModulePage> modulesOnPage = Lib.BLL.BLLSite_ModulePage.SelectByPageUrl(pageUrl);
            if (modulesOnPage == null || modulesOnPage.Count < 1) return;
            //Lib.Security.Sec_UserMembership usr = App_Lib.UIHelper.GetCurrentUser();
            Control placeHolder = null;
            foreach (Lib.UI.Site_ModulePage m in modulesOnPage)
            {
                if (m.ModuleInfo.HideFromLoggedInUser && User.Identity.IsAuthenticated) continue;
                if (!m.ModuleInfo.IsPublic)
                {
                    if (!User.Identity.IsAuthenticated) continue;
                }

                MasterPage mp = Master;
                placeHolder = mp.FindControl(m.PlaceHolder);
                while (placeHolder == null && mp.Master != null)
                {
                    mp = mp.Master;
                    placeHolder = mp.FindControl(m.PlaceHolder);
                }
                if (placeHolder == null) continue;
                Control ctrl = Page.LoadControl(m.ModuleInfo.FeatureInfo.ControlPath);
                if (ctrl == null) continue;
                App_Lib.UI.FeatureCtrlBase fcb = (App_Lib.UI.FeatureCtrlBase)ctrl;
                fcb.ModulePageData = m;
                placeHolder.Controls.Add(ctrl);
                placeHolder.Visible = true;
            }
        }

        public String GetPageTitleFromResource()
        {
            String classKey = SiteUtility.GetPageUrl();
            string text = GetGlobalResourceObject(classKey, "BrowserTitle").ToString();
            if (String.IsNullOrEmpty(text) || text.Equals("BrowserTitle")) text = GetGlobalResourceObject(classKey, "PageTitle").ToString();
            return HttpUtility.HtmlEncode(text);
        }

        public void SetPageTitle(String overwriteTitle)
        {
            String pageTitle = HttpUtility.HtmlEncode(overwriteTitle);
            if (pageTitle.IsNullOrEmptyString()) pageTitle = GetPageTitleFromResource();
            if (pageTitle.IsNullOrEmptyString()) pageTitle = GetGlobalResourceObject("COMMON", "Anritsu").ToString();
            else pageTitle += " - " + GetGlobalResourceObject("COMMON", "Anritsu").ToString();

            this.Title = pageTitle;
        }

        //protected override void Render(HtmlTextWriter writer)
        //{
        //    string content = string.Empty;

        //    using (var stringWriter = new StringWriter())
        //    {
        //        using (var htmlWriter = new HtmlTextWriter(stringWriter))
        //        {
        //            // render the current page content to our temp writer
        //            base.Render(htmlWriter);
        //            htmlWriter.Close();
        //            content = stringWriter.ToString();
        //        }
        //    }

        //    //if(HttpContext.Current.User.Identity.IsAuthenticated)
        //    {
        //        Regex reg = new Regex(@"\shref=[""'](?:https??://www.anritsu.com/.*\.aspx)[""']\s", RegexOptions.IgnoreCase);
        //        MatchCollection mc = reg.Matches(content);
        //        foreach(Match m in mc)
        //        {
        //            String newStr = m.Value;
        //        }
        //        //String newContent = Regex.Replace(content, @"<a\s.*?href=(?:["'](http://abc.com/articles/([0-9])+)["']).*?>(.*?)</a>"
        //        //    content.Replace("http://www.anritsu.com", String.Format("~/app_redir/redirgw?targeturl={0}"
        //    }
        //    // replace our placeholders
        //    //string newContent = content.Replace("$placeholder1$", "placeholder1 data").Replace("$placeholder2$", "placeholder2 data");


        //    // write the new html to the page
        //    writer.Write(content);
        //}
        
        private void DisableClientCaching()
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Is this required for FireFox? Would be good to do this without magic strings.
            // Won't it overwrite the previous setting
            Response.Headers.Add("Cache-Control", "no-cache, no-store");
            Response.Cache.SetNoStore();
            // Why is it necessary to explicitly call SetExpires. Presume it is still better than calling
            // Response.Headers.Add( directly
            Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));

        }

    }
}