﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_MyAnritsu_SiteAdmin : BP_MyAnritsu
    {
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminMode()) throw new HttpException(403, "Forbidden.");
            base.OnInit(e);
        }
    }
}