﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public enum ProductRegAdmin_Tabs
    {
        Registration, Account, USMMD, JapanSW, JapanSW_UserACL, ESD, SIS, DKSW
    }

    public class BP_ProdRegAdmin : BP_MyAnritsu_SiteAdmin
    {
        public Guid ProdRegMaster_WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        private Lib.ProductRegistration.ProdReg_Master _ProdRegMasterData;
        public Lib.ProductRegistration.ProdReg_Master ProdRegMasterData
        {
            get
            {
                if (_ProdRegMasterData != null) return _ProdRegMasterData;
                _ProdRegMasterData = Lib.ProductRegistration.ProdReg_MasterBLL.SelectByWebToken(ProdRegMaster_WebToken);
                return _ProdRegMasterData;
            }
        }

        public void LoadGeneralInfo(Literal ltrAddOnPageTitle)
        {
            Lib.ProductRegistration.ProdReg_Master regInfo = ProdRegMasterData;
            ltrAddOnPageTitle.Text = HttpUtility.HtmlEncode(String.Format(" - {0} [{1}]", regInfo.MasterModel, regInfo.MasterSerial));
            SetPageTitle(String.Format("{0} {1}", regInfo.MasterModel.Trim(), GetPageTitleFromResource()));
               
        }

        //public void LoadGeneralInfo(Literal ltrAddOnPageTitle, HyperLink hlPrintable)
        //{
        //    Lib.ProductRegistration.ProdReg_Master regInfo = ProdRegMasterData;
        //    ltrAddOnPageTitle.Text = HttpUtility.HtmlEncode(String.Format(" - {0} [{1}]", regInfo.MasterModel, regInfo.MasterSerial));
        //    SetPageTitle(String.Format("{0} {1}", regInfo.MasterModel.Trim(), GetPageTitleFromResource()));

        //    hlPrintable.NavigateUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.AdminProdRegGeneratePrint
        //        , KeyDef.QSKeys.RegisteredProductWebAccessKey, ProdRegMaster_WebToken);
        //}
    }
}