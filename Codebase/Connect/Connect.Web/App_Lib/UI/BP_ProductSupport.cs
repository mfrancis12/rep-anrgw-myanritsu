﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.UI;
using DevExpress.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BP_ProductSupport : BP_MyAnritsu
    {
        private ProdReg_DataSource _prodRegDataSorce = new ProdReg_DataSource();
        public string ActiveTab = "";
        //MA-1395
        //public String UrlQuery { get { return String.Format("?{0}={1}&{2}={3}", KeyDef.QSKeys.ModelNumber, ModelNumber, KeyDef.QSKeys.SerialNumber, SerialNumber); } }

        public Guid ProdRegWebAccessKey
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        public string ModelNumber
        {
            get
            {
                //MA-1395
                //return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.ModelNumber]);
                if (string.IsNullOrWhiteSpace(ConvertUtility.ConvertNullToEmptyString(Session[KeyDef.SSKeys.Selected_ModelNumber])))
                    Response.Redirect(KeyDef.UrlList.MyProducts);
                return ConvertUtility.ConvertNullToEmptyString(Session[KeyDef.SSKeys.Selected_ModelNumber]);                
            }
        }

        public String ActTab
        {
            get
            {
                return ConvertUtility.ConvertToString(Request.QueryString[KeyDef.QSKeys.ActTab], String.Empty);
            }
        }
        public String ResTab
        {
            get
            {
                return ConvertUtility.ConvertToString(Request.QueryString["resource"], String.Empty);
            }
        }
        private string _serialNumber;
        public string SerialNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(_serialNumber))
                    return _serialNumber;
                //extract the serial number from request
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[KeyDef.SSKeys.ddlSerialNumberId] != null)
                {
                    //get the value from Request params
                    _serialNumber = Request[HttpContext.Current.Session[KeyDef.SSKeys.ddlSerialNumberId].ToString()];
                    if (_serialNumber != null)
                        return _serialNumber;
                }
                //MA-1395
                //if (string.IsNullOrEmpty(_serialNumber) && Request.QueryString[KeyDef.QSKeys.SerialNumber] != null)
                //    _serialNumber = Request.QueryString[KeyDef.QSKeys.SerialNumber];
                if (string.IsNullOrEmpty(_serialNumber) && HttpContext.Current.Session[KeyDef.SSKeys.Selected_SerialNumber] != null)
                    _serialNumber = HttpContext.Current.Session[KeyDef.SSKeys.Selected_SerialNumber].ToString();
                //if serial number is null or empty get the defaul serial number.
                if (string.IsNullOrEmpty(_serialNumber))
                {
                    var data = _prodRegDataSorce.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber);
                    if (data != null && data.RegInfo != null && data.RegInfo.Count > 0)
                        _serialNumber = data.RegInfo[0].MasterSerial;                        
                }
                return _serialNumber;
            }
            set { _serialNumber = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                AnrCommon.CoreLib.WebUtility.DisableBrowserCache();
                LoginUtility.CheckTermsOfUseAgreement();
                
            }
            base.OnLoad(e);

        }
        public void InitNotifyAssistant(Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductInfoCtrl ProdInfoctrl)
        {
            ProdInfoctrl.DisplayDownloadButton();
        }
        public void InitTabs(ASPxTabControl tabCtrl)
        {
            if (tabCtrl == null || Page.IsCallback) return;
            //get the references of tabs
            Tab tbPublic = tabCtrl.Tabs.FindByName("tbPublic");
            Tab tbPrivate = tabCtrl.Tabs.FindByName("tbPrivate");
            Tab tbWarranty = tabCtrl.Tabs.FindByName("tbWarranty");
            Tab tbCalCert = tabCtrl.Tabs.FindByName("tbCalCert");

            var ds = new ProdReg_DataSource();
            var prodRegInfo = ds.GetData(AccountUtility.GenerateUserTeamIds(), ModelNumber, SerialNumber);
            if (prodRegInfo == null) throw new HttpException(404, "Page not found.");
            //set the urls
            //MA-1395
            HttpContext.Current.Session[KeyDef.SSKeys.Selected_SerialNumber] = SerialNumber; 
            tbPublic.NavigateUrl = KeyDef.UrlList.ProductSupportPublic;// + UrlQuery; MA-1395
            tbPrivate.NavigateUrl = KeyDef.UrlList.ProductSupportPrivate;// + UrlQuery; MA-1395
            tbWarranty.NavigateUrl = KeyDef.UrlList.ProductSupportWarranty;// + UrlQuery; MA-1395
            tbCalCert.NavigateUrl = KeyDef.UrlList.ProductSupportCalCert;// + UrlQuery; MA-1395


            var wtySrc = new Warranty_DataSource();
            bool showWarrantyInfo;
            showWarrantyInfo = (wtySrc.HasData(AccountUtility.GenerateUserTeamIds(), ModelNumber, SerialNumber, out showWarrantyInfo) && showWarrantyInfo);
            var showCalcerts = (prodRegInfo.CalCerts != null && prodRegInfo.CalCerts.Count > 0);
            //var pubSrc = new Downloads_PublicDataSource();

            var privSrc = new Downloads_PrivateDataSource();
            var hasPrivateDownloadsForJP = false;
            
            var hasPrivateDownloadsForTAU = privSrc.TAUDownloads_HasData(ModelNumber, AccountUtility.GenerateUserTeamIds(), prodRegInfo, SerialNumber);
            bool hasDownloadpkgs, hasLicensepkgs, hasVersionpkgs;
            hasPrivateDownloadsForJP = privSrc.JpDownloadsHasPackages(ModelNumber, SerialNumber, out hasDownloadpkgs, out hasLicensepkgs, out hasVersionpkgs);
            var showPrivateDownloads = (Convert.ToBoolean(ConfigUtility.AppSettingGetValue("Connect.Web.EnablePaidSupportInfo"))
                                         && (hasPrivateDownloadsForTAU || hasPrivateDownloadsForJP));

            HttpContext.Current.Items["hasPrivateDownloadsForTAU"] = hasPrivateDownloadsForTAU;
            HttpContext.Current.Items["hasPrivateDownloadsForJP"] = hasPrivateDownloadsForJP;

            tbWarranty.Visible = showWarrantyInfo;
            tbCalCert.Visible = showCalcerts;
            tbPrivate.Visible = showPrivateDownloads;
            tabCtrl.ActiveTab = tbPublic;
            tbPublic.NavigateUrl = string.Format("{0}?resource={1}", tbPublic.NavigateUrl, "1");
            //Handle Redirects/Navigations
            if (IsActiveTab(KeyDef.UrlList.ProductSupportWarranty))
            {
                if (showWarrantyInfo)
                {
                    tabCtrl.ActiveTab = tbWarranty;
                    return;
                }
                RedirectToPrivateDownloads(showPrivateDownloads, tbPrivate,tbPublic, tabCtrl);
            }
            if (IsActiveTab(KeyDef.UrlList.ProductSupportCalCert))
            {
                if (showCalcerts)
                {
                    tabCtrl.ActiveTab = tbCalCert;
                    return;
                }
                RedirectToPrivateDownloads(showPrivateDownloads, tbPrivate, tbPublic,tabCtrl);
            }
            RedirectToPrivateDownloads(showPrivateDownloads, tbPrivate, tbPublic, tabCtrl);
            
            if (IsActiveTab(KeyDef.UrlList.ProductSupportPrivateDownloads) ||
                IsActiveTab(KeyDef.UrlList.ProductSupportPrivateVersion) ||
                IsActiveTab(KeyDef.UrlList.ProductSupportPrivateLicense) ||
                IsActiveTab(KeyDef.UrlList.ProductSupportPrivate))
            {
                tabCtrl.ActiveTab = tbPrivate;
                ActiveTab = "Private";


            }
        }

        private bool IsActiveTab(string navihationUrl)
        {
            return Request.Path.Equals(navihationUrl.Replace("~", String.Empty), StringComparison.InvariantCultureIgnoreCase);
        }
        private void RedirectToPrivateDownloads(bool showPrivateDownloads,Tab privateDwlTab, Tab publicDwlTab, ASPxTabControl tabCtrl)
        {
            if (showPrivateDownloads)
            {
                if ((IsActiveTab(KeyDef.UrlList.ProductSupport) ||
                    IsActiveTab(KeyDef.UrlList.ProductSupportCalCert) ||
                    IsActiveTab(KeyDef.UrlList.ProductSupportWarranty)) && !ResTab.Equals("1"))
                {
                    //redirect to private downloads
                    Response.Redirect(privateDwlTab.NavigateUrl);
                }
            }
            else if (!IsActiveTab(KeyDef.UrlList.ProductSupport))
            {
                Response.Redirect(publicDwlTab.NavigateUrl);
            }
            else
                tabCtrl.ActiveTab = publicDwlTab;
        }
    }

}