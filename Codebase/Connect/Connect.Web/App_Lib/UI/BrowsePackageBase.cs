﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Lib.AWS;
using Anritsu.Connect.Lib.Package;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.CodeParser;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class BrowsePackageBase : Page
    {
        public String BasePath { get; set; }

        public String AwsConfigType { get; set; }

        public String DlActiveRegion { get; set; }

        public String DownloadRegion { get; set; }

        public TreeNode AddNodes(string path, TreeNode parentNode)
        {
            var s3UtilClient = new AWSUtility_S3(AwsConfigType);

            S3ObjectsWithPrefixes objs3Objects = s3UtilClient.GetS3ObjectsWithPrefixes(path, "/", AwsConfigType, DlActiveRegion);
            List<string> list = objs3Objects.CommonPrefixes;
            if (path == BasePath)
                ApplyPackageRoles(ref list);
            if (list.Count == 0)
                return null;
            TreeNode node = new TreeNode(path, path);
            if (parentNode.ChildNodes.Count == 0)
            {
                for (int index = 0; index < list.Count; index++)
                {

                    if (path.EndsWith("/"))
                        path = path.Substring(0, path.Length - 1);
                    //string directory = objList.Directories[index];            
                    TreeNode objChildNode = new TreeNode(list[index].Replace(path, "").Replace("/", ""), path + "/" + list[index].Replace(path, "").Replace("/", ""));
                    objChildNode.PopulateOnDemand = true;
                    objChildNode.Target = "_blank";

                    parentNode.ChildNodes.Add(objChildNode);
                }
            }
            return node;
        }

        private void ApplyPackageRoles(ref List<String> list)
        {
            switch (DownloadRegion)
            {
                case "TAU":
                    if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) return;
                    if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU))
                        list = list.Where(s => s.Contains(Package.TauPackageTypes.EnginerringPackage)).ToList();
                    break;
                case "JP":
                    break;
            }
        }
    }
}