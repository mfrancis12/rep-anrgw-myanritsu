﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public abstract class FeatureCtrlBase : System.Web.UI.UserControl
    {
        
        public Lib.UI.Site_ModulePage ModulePageData { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
        }

        public virtual String GetStaticResource(String staticResourceClassKey, String resourceKey)
        {
            String str = resourceKey;
            Object obj = HttpContext.GetGlobalResourceObject(staticResourceClassKey, resourceKey);
            if (obj != null) str = obj.ToString();
            return str;
        }

    }
}