﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public interface IFeatureCtrl
    {
        //Guid FeatureID { get; }
        //String StaticResourceClassKey { get; }
        String GetStaticResource(String resourceKey);
        String EditUrl(Int32 moduleID);
    }
}