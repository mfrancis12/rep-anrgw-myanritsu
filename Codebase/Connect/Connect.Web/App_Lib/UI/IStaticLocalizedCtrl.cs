﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public interface IStaticLocalizedCtrl
    {
        String StaticResourceClassKey { get; }
        String GetStaticResource(String resourceKey);
    }
}