﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    [Serializable]
    public class MessageData
    {
        public String MessageText { get; set; }
        public String BackUrl { get; set; }
        public String NextUrl { get; set; }
    }
}