﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Cfg;
namespace Anritsu.Connect.Web.App_Lib.UI
{
    public abstract class ModelConfigDetail_ItemCtrlBase : System.Web.UI.UserControl
    {
        public abstract String BatchChangeSessionKey { get; }

        public List<String> SelectedModels 
        { 
            get 
            {
                if (Session[BatchChangeSessionKey] == null) return new List<String>();
                return Session[BatchChangeSessionKey] as List<String>; 
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!ModelNumber.IsNullOrEmptyString())
                {
                    Session[BatchChangeSessionKey] = null;
                }

                Boolean isAuthorized = CheckAuthorizeAccess();
                if (!isAuthorized)
                {
                    this.Visible = false;
                    return;
                }

            }
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
        }

        public String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[App_Lib.KeyDef.QSKeys.ModelNumber]).Trim().ToUpperInvariant();
            }
        }

        public ProductConfig PConfig
        {
            get
            {
                Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(ModelNumber);
                return pcfg;
            }
        }

        public bool CheckAuthorizeAccess()
        {
            bool isAuthorized = true;
            if (ModelNumber.IsNullOrEmptyString())
            {
                if (SelectedModels.Count < 1)
                {
                    isAuthorized = false;
                }
                else
                {
                    foreach (String mn in SelectedModels)
                    {
                        ProductConfig pcfg = ProductConfigBLL.SelectByModelNumber(mn.Trim());
                        if (pcfg == null) isAuthorized = false;
                        else
                        {
                            int authorizedConfigTypeCount = 0;
                            foreach (ModelConfig mc in pcfg.ModelConfigs)
                            {
                                ModelConfigTypeInfo mcti = ProductConfigUtility.ModelConfigType_GetInfo(mc.ModelConfigType);
                                if (mcti == null) isAuthorized = false;
                                else
                                {
                                    if (LoginUtility.IsAdminRole(mcti.ManageRole))
                                        authorizedConfigTypeCount++;
                                }
                            }
                            if (authorizedConfigTypeCount < 1) isAuthorized = false;
                        }
                    }
                }
            }
            else
            {
                ProductConfig pc = PConfig;
                if (pc == null) isAuthorized = false;
                else
                {
                    int authorizedConfigTypeCount = 0;
                    foreach (ModelConfig mc in pc.ModelConfigs)
                    {
                        ModelConfigTypeInfo mcti = ProductConfigUtility.ModelConfigType_GetInfo(mc.ModelConfigType);
                        if (mcti == null) isAuthorized = false;
                        else
                        {
                            if (LoginUtility.IsAdminRole(mcti.ManageRole))
                                authorizedConfigTypeCount++;
                        }
                    }
                    if (authorizedConfigTypeCount < 1) isAuthorized = false;
                }
            }
            return isAuthorized;
        }

        public void ClearSelectedModels()
        {
            if (Session[BatchChangeSessionKey] != null)
                Session[BatchChangeSessionKey] = null;
        }
    }
}