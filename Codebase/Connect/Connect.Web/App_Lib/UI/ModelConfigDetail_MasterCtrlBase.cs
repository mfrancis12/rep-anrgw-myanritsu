﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public abstract class ModelConfigDetail_MasterCtrlBase : System.Web.UI.UserControl
    {
        public bool IsMasterInfoReadOnly 
        { 
            get { 
                return ConvertUtility.ConvertToBoolean(ViewState["vsAdmMN_MasterInfoRO"], true); 
            } 
            set { 
                ViewState["vsAdmMN_MasterInfoRO"] = value; 
            } 
        }

        public string ModelConfigType { get { return ConvertUtility.ConvertNullToEmptyString(ViewState["vsAdmMN_ModelConfigType"]); } set { ViewState["vsAdmMN_ModelConfigType"] = value; } }        

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ModelNumber.IsNullOrEmptyString())
                {
                    this.Visible = false;
                    return;
                }
                
            }
            AuthorizeModelNumber();

            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
        }

        public String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString["mn"]).Trim().ToUpperInvariant();
            }
        }

        public ProductConfig PConfig
        {
            get
            {
                Lib.Cfg.ProductConfig pcfg = Lib.Cfg.ProductConfigBLL.SelectByModelNumber(ModelNumber);
                return pcfg;
            }
        }

        public String CustomTextClassKey { get { return KeyDef.ResClassKeys.ProductCfgClassKey; } }

        public String CustomTextResKey { get { return string.Format("{0}{1}", KeyDef.ResKeyKeys.ProductCfgPrefixResKey, ModelNumber.Trim()); } }

        
        public virtual bool IsManageMasterInfoAllowed()
        {
            if (ModelNumber.IsNullOrEmptyString()) return false;
            return !IsMasterInfoReadOnly;
        }

        public void AuthorizeModelNumber()
        {
            //we have this in web.config so may not need this: if (!LoginUtility.IsAdminRole(AdminRole)) throw new HttpException(403, "Forbidden.");
            if (!ModelNumber.IsNullOrEmptyString())
            {
                ProductConfig pcfg = PConfig;
                if (pcfg == null)
                {
                    throw new HttpException(403, "Forbidden.");//something wrong here
                }
                else
                {
                    //var q = from mcfg in pcfg.ModelConfigs
                    //        where mcfg.ModelConfigType.Equals(ModelConfigType, StringComparison.InvariantCultureIgnoreCase)
                    //        select mcfg;
                    //if (q == null || q.Count() < 1) throw new HttpException(403, "Forbidden.");
                    //IsMasterInfoReadOnly = (q == null || q.Count() < 1);
                    IsMasterInfoReadOnly = true;
                    Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
                    if (user.IsAdministrator)
                    {
                        IsMasterInfoReadOnly = false;
                    }
                    else if (pcfg.ModelConfigs.Count == 1)
                    {
                        foreach (Lib.Cfg.ModelConfig mc in pcfg.ModelConfigs)
                        {
                            if (SetModelConfigPerms(mc)) continue;
                            else break;
                        }
                    }
                    else if (pcfg.ModelConfigs.Count > 1)
                    {
                        int activeMn = 0;
                        Lib.Cfg.ModelConfig actMc = null;
                        foreach (Lib.Cfg.ModelConfig mc in pcfg.ModelConfigs)
                        {
                            if (mc.ModelConfigStatus == ModelConfigStatus.STATUS_ACTIVE)
                            {
                                actMc = mc;
                                activeMn++;
                            }
                        }
                        if (activeMn.Equals(1))
                        {
                            bool modelConfigPerms = SetModelConfigPerms(actMc);
                        }
                    }
                }
            }
        }

        private bool SetModelConfigPerms(ModelConfig mc)
        {
            Lib.Cfg.ModelConfigTypeInfo mcti = App_Lib.Utils.ProductConfigUtility.ModelConfigType_GetInfo(mc.ModelConfigType);
            if (mcti == null) return true;
            bool isAdminRole = LoginUtility.IsAdminRole(mcti.ManageRole);
            if (isAdminRole && ModelConfigType.IsNullOrEmptyString()) //generic page
            {
                IsMasterInfoReadOnly = false;
                return true;
            }
            else if (isAdminRole && !ModelConfigType.IsNullOrEmptyString() &&
                     ModelConfigType.Equals(mc.ModelConfigType, StringComparison.InvariantCultureIgnoreCase))
            {
                IsMasterInfoReadOnly = false;
                return true;
            }
            return false;
        }
    }
}