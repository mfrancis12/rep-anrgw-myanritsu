﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public abstract class ModelConfigList_CtrlBase : System.Web.UI.UserControl
    {
        public abstract String BatchChangeSessionKey { get; }
        public abstract String SelectedProductCountSessionKey { get; }

        public List<String> SelectedModels 
        { 
            get 
            {
                if (Session[BatchChangeSessionKey] == null) return new List<String>();
                return Session[BatchChangeSessionKey] as List<String>; 
            } 
        }
        
        protected String ModelNumber
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[App_Lib.KeyDef.QSKeys.ModelNumber]);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        }

        public Boolean SetBatchConfigure(List<object> keys, Int32 visibleRowCount)
        {
            SetSelectedModels(keys, visibleRowCount);
            return (keys != null && keys.Count > 1 && visibleRowCount > 0);            
        }

        public void SetSelectedModels(List<object> keys, Int32 visibleRowCount)
        {
            if (keys == null)
            {
                Session[SelectedProductCountSessionKey] = 0;
                Session[BatchChangeSessionKey] = null;
                return;
            }

            List<String> selectedModelsList = new List<String>();
            selectedModelsList = keys.Select(x => x.ToString()).ToList();

            Session[SelectedProductCountSessionKey] = visibleRowCount;
            Session[BatchChangeSessionKey] = selectedModelsList;
        }
    }
}