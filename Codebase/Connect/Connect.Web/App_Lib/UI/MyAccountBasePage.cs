﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Erp;
using Anritsu.Connect.Lib.ProductRegistration;


namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class MyAccountBasePage : UI.BP_MyAnritsu
    {
        protected override void OnInit(EventArgs e)
        {
            if (!IsPostBack)
            {

            }

            base.OnInit(e);
        }

        public void SetNewPageTitle(Literal ltrAddOnPageTitle)
        {
            String str = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_CompanyProfileWzCtrl", "AddNewOrg"));
            ltrAddOnPageTitle.Text = HttpUtility.HtmlEncode(String.Format(" - {0}", str));
            SetPageTitle(String.Format("{0}", str));
        }


    }

}