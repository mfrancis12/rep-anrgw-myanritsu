﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Interactive;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf.HtmlToPdf;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public class PDFManager
    {

        public static PdfDocument MakePDFFromURL(string url)
        {
            PdfDocument pdf = new PdfDocument();
            //pdf.PageSettings.SetMargins(1.0f, 1.5f, 1.0f, 1.5f);
            pdf.PageSettings.Margins.All = 40.0f;
            pdf.PageSettings.Orientation = PdfPageOrientation.Portrait;

            PdfPage page = pdf.Pages.Add();
            //SizeF pageSize = page.GetClientSize();
            PdfUnitConvertor convertor = new PdfUnitConvertor();
            float width = convertor.ConvertToPixels(page.GetClientSize().Width, PdfGraphicsUnit.Point);

            using (var html = new HtmlConverter())
            {
                html.AutoDetectPageBreak = true;
                using (System.Drawing.Image img = html.ConvertToImage(url, ImageType.Metafile, (int)width, -1, AspectRatio.KeepWidth))
                {
                    if (img != null)
                    {
                        PdfMetafile metafile = (PdfMetafile)PdfImage.FromImage(img);
                        metafile.Quality = 100;
                        PdfMetafileLayoutFormat format = new PdfMetafileLayoutFormat();
                        format.Break = PdfLayoutBreakType.FitPage;
                        format.Layout = PdfLayoutType.Paginate;
                        //pdf.PageSettings.Height = img.Height;
                        format.SplitTextLines = false;
                        format.SplitImages = false;
                        //metafile.Draw(page, new RectangleF(0, 0, pageSize.Width, -1), format);
                        metafile.Draw(page, new RectangleF(0, 0, width, -1), format);
                    }
                }
            }
            return pdf;
        }
    }
}