﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib.Utils;
using DevExpress.Web;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public enum PackageConfigTab
    {
        PackageTypeMbmc = 0,
        PackageTypeJp = 1
    }

    public class PackageConfigBasePage : BP_MyAnritsu
    {

        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsPackageAdminRole()) throw new HttpException(404, "Page not found.");
            base.OnInit(e);
        }


        //<summary>
        //Checks whether user is super admin
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = user.IsAdministrator;
            return isAdminMode;
        }

        public PackageConfigTab SelectedTab
        {
            get
            {
                var path = Request.Path.ToLowerInvariant();
                if (path.Equals(KeyDef.UrlList.SiteAdminPages.PackageAdmin_List.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return PackageConfigTab.PackageTypeMbmc;
                return path.Equals(KeyDef.UrlList.SiteAdminPages.PackageAdmin_List_Jp.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase) ? PackageConfigTab.PackageTypeJp : PackageConfigTab.PackageTypeMbmc;
            }
        }

        public void InitConfigTabs(ASPxTabControl supportTabs)
        {
            InitTabs(supportTabs, SelectedTab);
        }

        public void InitTabs(ASPxTabControl supportTabs, PackageConfigTab selectedTabType)
        {
            Tab tab;
             if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU) || LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU))
            {
                tab = new Tab
                {
                    Text = GetModelConfigTabText(PackageConfigTab.PackageTypeMbmc),
                    NavigateUrl = KeyDef.UrlList.SiteAdminPages.PackageAdmin_List
                };
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == PackageConfigTab.PackageTypeMbmc)
                    supportTabs.ActiveTab = tab;
            }
            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages))
            {
                tab = new Tab
                {
                    Text = GetModelConfigTabText(PackageConfigTab.PackageTypeJp),
                    NavigateUrl = KeyDef.UrlList.SiteAdminPages.PackageAdmin_List_Jp
                };
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == PackageConfigTab.PackageTypeJp)
                    supportTabs.ActiveTab = tab;
            }
        }

        private String GetModelConfigTabText(PackageConfigTab tabType)
        {
            String resKey = "package-MBMC";
            switch (tabType)
            {
                case PackageConfigTab.PackageTypeJp:
                    resKey = "package-JP";
                    break;
            }
            Object txtObj = GetGlobalResourceObject("PackageCategory", resKey).ToString();
            return txtObj.ToString();
        }


        /// <summary>
        /// Returns selected page url from tab selected
        /// </summary>
        /// <param name="tabType"></param>
        public void RedirectToSelectedTab(PackageConfigTab tabType)
        {
            var pageUrl = String.Empty;
            switch (tabType)
            {

                case PackageConfigTab.PackageTypeMbmc:
                    pageUrl = KeyDef.UrlList.SiteAdminPages.PackageAdmin_List;
                    break;
                case PackageConfigTab.PackageTypeJp:
                    pageUrl = KeyDef.UrlList.SiteAdminPages.PackageAdmin_List;
                    break;
            }
            Response.Redirect(pageUrl);
        }

        public void LoadModelConfigInfo(Literal ltrAddOnPageTitle, String itemType)
        {
            ltrAddOnPageTitle.Text = HttpUtility.HtmlEncode(String.Format(" - {0}", itemType));
        }

    }
}