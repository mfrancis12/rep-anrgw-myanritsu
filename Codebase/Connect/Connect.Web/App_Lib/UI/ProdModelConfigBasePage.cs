﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Lib.Erp;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib.ProductRegistration;
using DevExpress.Web;
using Anritsu.Connect.Lib.Cfg;

namespace Anritsu.Connect.Web.App_Lib.UI
{
    public enum ProductModelConfigTab
    {
       Master=0, ItemType_JPM = 1, ItemType_JPAN = 2, ItemType_USMMD = 3, ItemType_ESD = 4, ItemType_DK = 6
    }

    public class ProdModelConfigBasePage : BP_MyAnritsu
    {

        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsProdConfigAdminRole()) throw new HttpException(404, "Page not found.");
            if (!IsPostBack)
            {

            }

            base.OnInit(e);
        }

        //public ProductModelConfigTab SelectedTab
        //{
        //    get
        //    {
        //        if (App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_JPAN))
        //            return ProductModelConfigTab.Region_JP_AN;
        //        else if (App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_JPM))
        //            return ProductModelConfigTab.Region_JP_M;
        //        else if (App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD))
        //            return ProductModelConfigTab.Region_US_MMD;
        //        else if (App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_DESA))
        //            return ProductModelConfigTab.Region_DE_SA;
        //        else return ProductModelConfigTab.Region_JP_AN;
        //    }
        //}

        //<summary>
        //Checks whether user is super admin
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = user.IsAdministrator;
            return isAdminMode;
        }

        public ProductModelConfigTab SelectedTab
        {
            get
            {
                String path = Request.Path.ToLowerInvariant();
                if (path.Equals(KeyDef.UrlList.CfgModel.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.Master;
                if (path.Equals(KeyDef.UrlList.CfgModel_JP.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.ItemType_JPM;
                else if (path.Equals(KeyDef.UrlList.CfgModel_JPAN.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.ItemType_JPAN;
                else if (path.Equals(KeyDef.UrlList.CfgModel_USMMD.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.ItemType_USMMD;
                else if (path.Equals(KeyDef.UrlList.CfgModel_DK.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.ItemType_DK;
                else if (path.Equals(KeyDef.UrlList.CfgModel_UK.Replace("~", ""), StringComparison.InvariantCultureIgnoreCase))
                    return ProductModelConfigTab.ItemType_ESD;
                else
                    return ProductModelConfigTab.Master;
            }
        }

        public void InitConfigTabs(ASPxTabControl supportTabs)
        {
            InitTabs(supportTabs, SelectedTab);
        }

        public void InitTabs(ASPxTabControl supportTabs, ProductModelConfigTab selectedTabType)
        {

            Tab tab;

           
                tab = new Tab();
                tab.Text = GetModelConfigTabText(ProductModelConfigTab.Master);
                tab.NavigateUrl = KeyDef.UrlList.CfgModel;
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == ProductModelConfigTab.Master)
                    supportTabs.ActiveTab = tab;
            

            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_JPM))
            {
              tab = new Tab();
              tab.Text = GetModelConfigTabText(ProductModelConfigTab.ItemType_JPM);
              tab.NavigateUrl = KeyDef.UrlList.CfgModel_JP;
              supportTabs.Tabs.Add(tab);
             // supportTabs.ActiveTab = tab;
              if (selectedTabType == ProductModelConfigTab.ItemType_JPM)
                    supportTabs.ActiveTab = tab;
            }

            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_JPAN))
            {
                tab = new Tab();
                tab.Text = GetModelConfigTabText(ProductModelConfigTab.ItemType_JPAN);
                tab.NavigateUrl = KeyDef.UrlList.CfgModel_JPAN;
                supportTabs.Tabs.Add(tab);
               if (selectedTabType == ProductModelConfigTab.ItemType_JPAN)
                    supportTabs.ActiveTab = tab;
            }

            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD))
            {
                tab = new Tab();
                tab.Text = GetModelConfigTabText(ProductModelConfigTab.ItemType_USMMD);
                tab.NavigateUrl = KeyDef.UrlList.CfgModel_USMMD;
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == ProductModelConfigTab.ItemType_USMMD)
                    supportTabs.ActiveTab = tab;
            }

            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_UKTAU))
            {
                tab = new Tab();
                tab.Text = GetModelConfigTabText(ProductModelConfigTab.ItemType_ESD);
                tab.NavigateUrl = KeyDef.UrlList.CfgModel_UK;
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == ProductModelConfigTab.ItemType_ESD)
                    supportTabs.ActiveTab = tab;
            }

            if (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_DKM))
            {
                tab = new Tab();
                tab.Text = GetModelConfigTabText(ProductModelConfigTab.ItemType_DK);
                tab.NavigateUrl = KeyDef.UrlList.CfgModel_DK;
                supportTabs.Tabs.Add(tab);
                if (selectedTabType == ProductModelConfigTab.ItemType_DK)
                    supportTabs.ActiveTab = tab;
            }
                        
        }

        private String GetModelConfigTabText(ProductModelConfigTab tabType)
        {
            String resKey = "master-models";
            switch (tabType)
            {
                case ProductModelConfigTab.Master:
                    resKey = "master-models";
                    break;
                case ProductModelConfigTab.ItemType_JPM:
                    resKey = "downloads-jp";
                    break;
                case ProductModelConfigTab.ItemType_JPAN:
                    resKey = "downloads-jp-an";
                    break;
                case ProductModelConfigTab.ItemType_USMMD:
                    resKey = "downloads-us";
                    break;
                case ProductModelConfigTab.ItemType_DK:
                    resKey = "downloads-dk";
                    break;
                case ProductModelConfigTab.ItemType_ESD:
                    resKey = "downloads-uk";
                    break;

            }
            Object txtObj = GetGlobalResourceObject("ItemType", resKey).ToString();
            if (txtObj == null) return String.Empty;
            return txtObj.ToString();
        }


        /// <summary>
        /// Returns selected page url from tab selected
        /// </summary>
        /// <param name="tabType"></param>
        public void RedirectToSelectedTab(ProductModelConfigTab tabType)
        {
            String pageUrl = String.Empty;
            switch (tabType)
            {

                case ProductModelConfigTab.Master:
                    pageUrl = KeyDef.UrlList.CfgModel;
                    break;
                case ProductModelConfigTab.ItemType_JPM:
                    pageUrl = KeyDef.UrlList.CfgModel_JP;
                    break;
                case ProductModelConfigTab.ItemType_JPAN:
                    pageUrl = KeyDef.UrlList.CfgModel_JPAN;
                    break;
                case ProductModelConfigTab.ItemType_USMMD:
                    pageUrl = KeyDef.UrlList.CfgModel_USMMD;
                    break;
                case ProductModelConfigTab.ItemType_DK:
                    pageUrl = KeyDef.UrlList.CfgModel_DK;
                    break;
                case ProductModelConfigTab.ItemType_ESD:
                    pageUrl = KeyDef.UrlList.CfgModel_UK;
                    break;
                default:
                    pageUrl = KeyDef.UrlList.CfgModel;
                    break;
            }
            Response.Redirect(pageUrl);
        }

        public void LoadModelConfigInfo(Literal ltrAddOnPageTitle, String itemType)
        {
            ltrAddOnPageTitle.Text = HttpUtility.HtmlEncode(String.Format(" - {0}", itemType));
            // SetPageTitle(String.Format("{0} {1}", ProductReg.ModelNumber.Trim(), GetPageTitleFromResource()));

        }

    }
}