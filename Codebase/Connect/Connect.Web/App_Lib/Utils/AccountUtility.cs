﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Data;

using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    public static class AccountUtility
    {
        public static void AdminOrgSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminOrgSearchOptions] = searchOptions;
        }

        public static List<SearchByOption> AdminOrgSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminOrgSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                AdminOrgSearchOptionsSet(opt);
            }
            return opt;
        }

        public static DataTable AccountsForCurrentUserGet(bool refresh)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return null;
            DataTable tb = context.Session[KeyDef.SSKeys.OrganizationListByMembershipId] as DataTable;
            if (tb == null || tb.Rows.Count < 1 || refresh)
            {
                tb = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipIdAsTable(LoginUtility.GetCurrentUserOrSignIn().MembershipId);
            }
            return tb;
        }

        public static Lib.Security.Sec_UserCache SetSelectedAccountID(Guid accountID, AnrSso.ConnectSsoUser user)
        {
            if (accountID.IsNullOrEmptyGuid() || user == null) return null;
            HttpContext context = HttpContext.Current;
            List<Lib.Account.Acc_AccountMaster> accounts = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(user.MembershipId);
            if (accounts == null)
            {
                return null;
            }

            foreach (Lib.Account.Acc_AccountMaster acc in accounts)
            {
                if (acc.AccountID.Equals(accountID))
                {
                    if (context.Session != null) context.Session[KeyDef.SSKeys.SelectedAccountID] = accountID;
                    if (context.Request.Cookies[".cntrl"] != null)
                    {
                        HttpCookie myCookie = new HttpCookie(".cntrl");
                        myCookie.Expires = DateTime.Now.AddDays(-1d);
                        context.Response.Cookies.Add(myCookie);
                    }

                    return Lib.BLL.BLLSec_UserCache.Save(user.MembershipId
                        , Lib.Security.Sec_UserCacheKeys.SelectedAccountID
                        , accountID.ToString().ToUpperInvariant());
                }
            }
            return null;
        }

        public static Lib.Account.Acc_AccountMaster GetSelectedAccount(Boolean refresh, Boolean redirectIfNoneSelected)
        {
            Guid accountID = GetSelectedAccountID(redirectIfNoneSelected);
            if (accountID.IsNullOrEmptyGuid()) return null;
            if (HttpContext.Current.Session == null) return Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            else
            {
                Lib.Account.Acc_AccountMaster acc = HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccount] as Lib.Account.Acc_AccountMaster;
                if (acc == null || refresh || accountID != acc.AccountID)
                {
                    acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
                    HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccount] = acc;
                }
                if ((acc == null && redirectIfNoneSelected)
                    || (acc != null && acc.InfoUpdateRequired && !SiteUtility.GetPageUrl().Equals(KeyDef.UrlList.ManageOrg, StringComparison.InvariantCultureIgnoreCase)))
                {
                    String redirectURL = String.Format("{0}?ReturnURL={1}", KeyDef.UrlList.ManageOrg,
                        HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
                    WebUtility.HttpRedirect(null, redirectURL);
                }
                return acc;
            }
        }

        public static Guid GetSelectedAccountID(Boolean redirectIfNone)
        {
            if (HttpContext.Current == null) return Guid.Empty;
            Guid selectedAccountID = Guid.Empty;
            if (HttpContext.Current.Session != null)
                selectedAccountID = ConvertUtility.ConvertToGuid(HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccountID], Guid.Empty);

            if (selectedAccountID.IsNullOrEmptyGuid())
            {
                AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                Lib.Security.Sec_UserCache usrCache =
                    Lib.BLL.BLLSec_UserCache.SelectByUserCacheID(user.MembershipId, Lib.Security.Sec_UserCacheKeys.SelectedAccountID);
                if (usrCache == null)
                {
                    //auto select
                    List<Lib.Account.Acc_AccountMaster> accounts = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(user.MembershipId);
                    if (accounts != null && accounts.Count > 0)
                    {
                        selectedAccountID = accounts[0].AccountID;
                        if (HttpContext.Current.Session != null)
                            HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccountID] = selectedAccountID;
                        usrCache = SetSelectedAccountID(selectedAccountID, user);
                    }
                }
                else
                {
                    selectedAccountID = ConvertUtility.ConvertToGuid(usrCache.DataValue, Guid.Empty);
                    if (HttpContext.Current.Session != null)
                        HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccountID] = selectedAccountID;
                }

                //#region " dongle session "
                //Lib.DongleDownload.DongleLic usbLic = App_Lib.JapanUSBHelper.JapanUSBLicDataGet(true);
                //if (usbLic != null && usbLic.DongleInfo != null && usbLic.DongleInfo.Rows.Count > 0)
                //{
                //    String usbNo = ConvertUtility.ConvertNullToEmptyString(usbLic.DongleInfo.Rows[0]["USBNo"]);
                //    Guid accountIDByUSB = Lib.ProductRegistration.Acc_ProductRegisteredBLL.SelectAccountIDByUSBNo(usbNo, user.MembershipId);
                //    if (!accountIDByUSB.IsNullOrEmptyGuid())
                //    {
                //        selectedAccountID = accountIDByUSB;
                //        if (HttpContext.Current.Session != null)
                //            HttpContext.Current.Session[KeyDef.SSKeys.SelectedAccountID] = selectedAccountID;
                //    }
                //}
                //#endregion

                if (redirectIfNone && selectedAccountID.IsNullOrEmptyGuid()
                    && !SiteUtility.GetPageUrl().Equals(KeyDef.UrlList.ManageOrg, StringComparison.InvariantCultureIgnoreCase))
                {
                    WebUtility.HttpRedirect(null, KeyDef.UrlList.ManageOrg);
                }
            }
            return selectedAccountID;
        }

        public static Guid AnritsuMasterAccountID()
        {
            return ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Connect.Web.AnritsuMasterAccountID"), Guid.Empty);
        }

        public static void Admin_AccountInfoSet(Guid accountID, Lib.Account.Acc_AccountMaster accInfo)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null || accountID.IsNullOrEmptyGuid()) return;
            string sessionKey = String.Format("{0}_{1}", KeyDef.SSKeys.AdminAccountInfo, accountID.ToString());
            if (accInfo == null) HttpContext.Current.Session.Remove(sessionKey);
            else HttpContext.Current.Session[sessionKey] = accInfo;
        }

        public static Lib.Account.Acc_AccountMaster Admin_AccountInfoGet(Guid accountID)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null || accountID.IsNullOrEmptyGuid()) return null;

            string sessionKey = String.Format("{0}_{1}", KeyDef.SSKeys.AdminAccountInfo, accountID.ToString());
            Lib.Account.Acc_AccountMaster acc = context.Session[sessionKey] as Lib.Account.Acc_AccountMaster;
            if (acc == null)
            {
                acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
                context.Session[sessionKey] = acc;
            }
            return acc;
        }
        /// <summary>
        ///Get the list of teams assoicated for the logged in user 
        /// </summary>
        /// <param name="active">used to pull verifed accounts</param>
        /// <returns></returns>
        public static DataTable GenerateUserTeamIds(bool active = false)
        {
            //get Team list table
            var dtTeams = LoginUtility.GetCurrentUserOrSignIn().TeamsList;
            if (dtTeams != null && dtTeams.Rows.Count > 0)
            {
                DataView dv = null;
                if (active)
                {
                    //filter verified accounts/teams
                    DataRow[] drs = dtTeams.Select("IsVerifiedAndApproved=1");
                    if (drs != null && drs.Count() > 0)
                    {
                        //convert to table and select only Id of the team
                        dv = new DataView(drs.CopyToDataTable());
                        return dv.ToTable(true, "Id");
                    }
                }
                else
                {
                    dv = new DataView(dtTeams);
                    return dv.ToTable(true, "Id");
                }
            }
            return null;
        }

    }
}