﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Lib.ProductRegistration;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    public static class Admin_ProductRegUtility
    {
        public static List<ProdReg_Item_Status> ProdReg_Item_Status_SelectAll(Boolean refresh)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null)
            {
                return Lib.ProductRegistration.ProdReg_Item_StatusBLL.SelectAll();
            }

            List<Lib.ProductRegistration.ProdReg_Item_Status> list = context.Session[KeyDef.SSKeys.Admin_ProdReg_ItemStatus] as
                List<Lib.ProductRegistration.ProdReg_Item_Status>;

            if (list == null || refresh)
            {
                list = Lib.ProductRegistration.ProdReg_Item_StatusBLL.SelectAll();
                context.Session[KeyDef.SSKeys.Admin_ProdReg_ItemStatus] = list;
            }
            return list;
        }

        public static ProdReg_Master ProdReg_MasterInfo(Guid webToken, Boolean refresh)
        {
            if (webToken.IsNullOrEmptyGuid()) return null;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null)
            {
                return ProdReg_MasterBLL.SelectByWebToken(webToken);
            }
            String sessionKey = String.Format("{0}{1}", KeyDef.SSKeys.Admin_ProdReg_MasterInfo, webToken.ToString().ToLowerInvariant());
            ProdReg_Master pm = context.Session[sessionKey] as ProdReg_Master;
            if (pm == null || refresh)
            {
                pm = ProdReg_MasterBLL.SelectByWebToken(webToken);
                if (pm == null) context.Session.Remove(sessionKey);
                else context.Session[sessionKey] = pm;
            }
            return pm;
        }
    }
}