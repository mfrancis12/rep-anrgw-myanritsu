﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class Admin_SearchUtility
    {
        public static void UserSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminUserSearchOptions] = searchOptions;
        }

        public static List<SearchByOption> UserSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminUserSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                UserSearchOptionsSet(opt);
            }
            return opt;
        }

        public static void OrgSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminOrgSearchOptions] = searchOptions;
        }

        public static List<SearchByOption> OrgSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminOrgSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                OrgSearchOptionsSet(opt);
            }
            return opt;
        }

        public static void AdminProdRegSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminProdRegSearchOptions] = searchOptions;
        }

        public static List<SearchByOption> AdminProdRegSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminProdRegSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                AdminProdRegSearchOptionsSet(opt);
            }
            return opt;
        }

        public static List<SearchByOption> AdminSupportTypeSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminSupportTypeSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                AdminSupportTypeSearchOptionsSet(opt);
            }
            return opt;
        }

        public static void AdminSupportTypeSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminSupportTypeSearchOptions] = searchOptions;
        }
    }
}