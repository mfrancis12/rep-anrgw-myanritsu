﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    public static class ContentSearchUtility
    {
        public static void AdminContentSearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SSKeys.AdminContentSearchOptions] = searchOptions;
        }

        public static List<SearchByOption> AdminContentSearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            List<SearchByOption> opt =
                HttpContext.Current.Session[KeyDef.SSKeys.AdminContentSearchOptions] as List<SearchByOption>;
            if (reset)
            {
                opt = new List<SearchByOption>();
                AdminContentSearchOptionsSet(opt);
            }
            return opt;
        }
    }
}