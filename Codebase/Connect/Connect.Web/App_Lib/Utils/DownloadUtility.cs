﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class DownloadUtility
    {
        public static String GetMyAnritsuDownloadURL(Guid prodRegWebToken, String modelNumber, String downloadID, String downloadSrc)
        {
            return String.Format("{0}?{1}={2}&{3}={4}&{5}={6}&{7}={8}", KeyDef.UrlList.ConnectDownloadURL
                , KeyDef.QSKeys.RegisteredProductWebAccessKey, prodRegWebToken.ToString()
                , KeyDef.QSKeys.ModelNumber, HttpUtility.UrlDecode(modelNumber.Trim())
                , KeyDef.QSKeys.DownloadID, HttpUtility.UrlDecode(downloadID)
                , KeyDef.QSKeys.DownloadSource, downloadSrc);
        }

    }
}