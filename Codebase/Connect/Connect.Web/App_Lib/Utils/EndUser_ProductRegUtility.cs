﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.AnrCommon.CoreLib;
using EM = Anritsu.AnrCommon.EmailSDK;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class EndUser_ProductRegUtility
    {
        public static Acc_ProductRegCart RegCart_SelectByCartToken(Guid cartToken, Boolean refreshFromDB)
        {
            HttpContext context = HttpContext.Current;
            if (cartToken.IsNullOrEmptyGuid() || context == null) return null;
            if (context.Session == null) return Acc_ProductRegCartBLL.SelectByWebAccessKey(cartToken);
            String sessionKey = String.Format("{0}_{1}", KeyDef.SSKeys.EndUser_ProdReg_RegCartInfo, cartToken.ToString());

            Acc_ProductRegCart cartg = context.Session[sessionKey] as Acc_ProductRegCart;
            if (cartg == null || refreshFromDB)
            {
                cartg = Acc_ProductRegCartBLL.SelectByWebAccessKey(cartToken);
                context.Session[sessionKey] = cartg;
            }
            return cartg;
        }

        public static void EndUser_SubmitRegistration(Guid prodRegCartToken, out Boolean atleastOneAutoApproved)
        {
            atleastOneAutoApproved = false;
            if (prodRegCartToken.IsNullOrEmptyGuid()) throw new ArgumentNullException("prodRegCartToken");
            HttpContext context = HttpContext.Current;
            Acc_ProductRegCart cartg = EndUser_ProductRegUtility.RegCart_SelectByCartToken(prodRegCartToken, true);
            if (cartg == null) throw new ArgumentNullException("cartg");
            Lib.Cfg.ProductConfig prodCfg = ProductConfigUtility.SelectByModel(true, cartg.ModelNumber);
            if (cartg == null) throw new ArgumentNullException("prodCfg");

            if (cartg.CartItems == null || cartg.CartItems.Count < 1) throw new ArgumentException("You need at least one item to register.");

            Lib.Account.Acc_AccountMaster account = AccountUtility.GetSelectedAccount(false, true);
            AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
            Int32 gwCultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            String userIPAddress = WebUtility.GetUserIP();
            String userFullName = GetUserFullName(user);

            #region " new product registrations "
            Boolean validateSerialNumber = false;  //no need to validate again because the cart is already validated
            Int32 regActiveCount = 0;
            Dictionary<String, Guid> regWebTokens = new Dictionary<String, Guid>();
            foreach (Acc_ProductRegCartItem cartItem in cartg.CartItems)
            {
                Dictionary<String, String> additionalData = InitAddOnData(cartg, cartItem, user, userIPAddress);
                Guid regWebToken = Guid.Empty;
                Int32 itemRegStatusCount = 0;
                Int32 regID = ProdReg_MasterBLL.Insert(account.AccountID, cartg.ModelNumber, cartItem.SerialNumber, user.Email,
                    additionalData, validateSerialNumber,
                    out regWebToken, out itemRegStatusCount);
                regWebTokens.Add(cartItem.SerialNumber, regWebToken);
                regActiveCount += itemRegStatusCount;
            }

            if (regWebTokens.Count < 1) throw new ArgumentException("Unable to register products.");

            Acc_ProductRegCartBLL.DeleteCart(prodRegCartToken);//delete cart after registration
            atleastOneAutoApproved = regActiveCount > 0;
            EndUser_SubmitRegistration_SendEmails(cartg, prodCfg, account, user, regWebTokens, gwCultureGroupId);
            #endregion
        }

        private static void EndUser_SubmitRegistration_SendEmails(Acc_ProductRegCart cartg, 
            Lib.Cfg.ProductConfig prodCfg,
            Lib.Account.Acc_AccountMaster account,
            AnrSso.ConnectSsoUser user,
            Dictionary<String, Guid> prodRegWebAccessKeys,
            Int32 gwCultureGroupId)
        {
            List<EM.EmailWebRef.ReplaceKeyValueType> emailData = Email_GetKeyValues(cartg, prodCfg, account, user, prodRegWebAccessKeys);
            if (emailData == null) return;

            Boolean atleastOneAutoApproved = false;
            Boolean atleastOnePending = false;
            foreach(Lib.Cfg.ModelConfig mnConfig in prodCfg.ModelConfigs)
            {
                if (mnConfig.ModelConfigStatus.Equals("active", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (mnConfig.Reg_InitialStatus.Equals("active", StringComparison.InvariantCultureIgnoreCase))
                        atleastOneAutoApproved = true;
                    else
                        atleastOnePending = true;
                }
            }

            if (atleastOneAutoApproved)
            {
                #region " approved at least one already so send confirm email to customer "
                EM.EmailWebRef.SendTemplatedEmailCallRequest reqUserConfirmEmail =
                    new EM.EmailWebRef.SendTemplatedEmailCallRequest();
                reqUserConfirmEmail.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegEmail.UserConfirm");
                reqUserConfirmEmail.ToEmailAddresses = new string[] { user.Email };
                reqUserConfirmEmail.CultureGroupId = gwCultureGroupId;
                reqUserConfirmEmail.SubjectReplacementKeyValues = emailData.ToArray();
                reqUserConfirmEmail.BodyReplacementKeyValues = emailData.ToArray();
                var respcode = EM.EmailServiceManager.SendTemplatedEmail(reqUserConfirmEmail);
                #endregion
            }

            if (atleastOnePending)
            {
                foreach (Lib.Cfg.ModelConfig mnConfig in prodCfg.ModelConfigs)
                {
                    if (mnConfig.ModelConfigStatus.Equals("active", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Lib.Cfg.ModelConfigTypeInfo mct = ProductConfigUtility.ModelConfigType_GetInfo(mnConfig.ModelConfigType);
                        if (mct == null) continue;
                        try
                        {
                            List<EM.EmailWebRef.ReplaceKeyValueType> adminEmailData = new List<EM.EmailWebRef.ReplaceKeyValueType>(emailData);

                            EM.EmailWebRef.ReplaceKeyValueType kv;
                            kv = new EM.EmailWebRef.ReplaceKeyValueType();
                            kv.TemplateKey = "[[VerifyCompany]]";
                            kv.ReplacementValue = mnConfig.Verify_Company.ToString().ToUpperInvariant();
                            adminEmailData.Add(kv);

                            kv = new EM.EmailWebRef.ReplaceKeyValueType();
                            kv.TemplateKey = "[[VerifyProductSN]]";
                            kv.ReplacementValue = mnConfig.Verify_Registration.ToString().ToUpperInvariant();
                            adminEmailData.Add(kv);

                            kv = new EM.EmailWebRef.ReplaceKeyValueType();
                            kv.TemplateKey = "[[HasSupportExpiration]]";
                            kv.ReplacementValue = mnConfig.HasSupportExpiration.ToString();
                            adminEmailData.Add(kv);
                            adminEmailData.AddRange(emailData);

                            #region " send email(s) to admin for approval "
                            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest reqAdminForApprovalEmail =
                            new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
                            reqAdminForApprovalEmail.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegEmail.AdminForApproval");
                            reqAdminForApprovalEmail.DistributionListKey = mct.EmailDistKey_NotifyReg;
                            reqAdminForApprovalEmail.CultureGroupId = gwCultureGroupId;
                            reqAdminForApprovalEmail.SubjectReplacementKeyValues = adminEmailData.ToArray();
                            reqAdminForApprovalEmail.BodyReplacementKeyValues = adminEmailData.ToArray();
                           var respcode= EM.EmailServiceManager.SendDistributedTemplatedEmail(reqAdminForApprovalEmail);
                            #endregion

                        }
                        catch// (Exception adminNotifyEx)
                        {
                            //log here on error if u want to but swollow to avoid other admins not getting emails
                        }
                    }
                }

                //send email to customer at last because they may have invalid email address
                #region " send submitted email to customer "
                EM.EmailWebRef.SendTemplatedEmailCallRequest reqUserSubmittedAckEmail =
                                new EM.EmailWebRef.SendTemplatedEmailCallRequest();
                reqUserSubmittedAckEmail.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegEmail.UserSubmittedAck");// replace with Connect.Web.ProdRegEmail.UserSubmittedAck when JP goes live
                reqUserSubmittedAckEmail.ToEmailAddresses = new string[] { user.Email };
                reqUserSubmittedAckEmail.CultureGroupId = gwCultureGroupId;
                reqUserSubmittedAckEmail.SubjectReplacementKeyValues = emailData.ToArray();
                reqUserSubmittedAckEmail.BodyReplacementKeyValues = emailData.ToArray();
               var response= EM.EmailServiceManager.SendTemplatedEmail(reqUserSubmittedAckEmail);
                #endregion

            }
        }

        private static Dictionary<String, String> InitAddOnData(Acc_ProductRegCart cart,
            Acc_ProductRegCartItem cartItem,
            AnrSso.ConnectSsoUser user,
            String userIPAddress
            )
        {
            if (cart == null || cartItem == null || user == null) return null;
            Dictionary<String, String> additionalData = new Dictionary<String, String>();
            additionalData.Add("SubmittedBy_Email", user.Email);
            additionalData.Add("SubmittedBy_FullName", user.FullName);
            additionalData.Add("SubmittedBy_IP", userIPAddress);
            if (!user.PhoneNumber.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Phone", user.PhoneNumber.Trim());
            if (!user.FaxNumber.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Fax", user.FaxNumber.Trim());
            if (!user.UserState.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_State", user.UserState.Trim());
            if (!user.UserCity.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_City", user.UserCity.Trim());
            if (!user.UserAddress1.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Add1", user.UserAddress1.Trim());
            if (!user.UserAddress2.IsNullOrEmptyString()) additionalData.Add("SubmittedBy_Add2", user.UserAddress2.Trim());
            if (cartItem.ItemData != null)
            {
                foreach (Acc_ProductRegCartItemData data in cartItem.ItemData)
                {
                    if (!additionalData.ContainsKey(data.DataKey)) additionalData.Add(data.DataKey, data.DataValue);
                }
            }
            return additionalData;
        }

        private static String GetUserFullName(AnrSso.ConnectSsoUser user)
        {
            if (user == null) return String.Empty;
            #region user full name
            String userFullName = string.Empty;

            if (!string.IsNullOrEmpty(user.FirstNameInEnglish) && !string.IsNullOrEmpty(user.LastNameInEnglish))
                userFullName = user.LastNameInEnglish + "," + user.FirstNameInEnglish;
            else
                userFullName = user.LastName + "," + user.FirstName;

            #endregion
            return userFullName;
        }

        private static List<EM.EmailWebRef.ReplaceKeyValueType> Email_GetKeyValues(
            Acc_ProductRegCart cartg,
            Lib.Cfg.ProductConfig prodCfg,
            Lib.Account.Acc_AccountMaster account,
            AnrSso.ConnectSsoUser user,
            Dictionary<String, Guid> prodRegWebAccessKeys)
        {
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;
            if (cartg != null && cartg.CartItems != null && cartg.CartItems.Count > 0)
            {
                #region " product information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[ModelNumber]]";
                kv.ReplacementValue = cartg.ModelNumber;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SerialNumbers]]";
                kv.ReplacementValue = cartg.SerialNumberDelimitedString;
                kvList.Add(kv);

                //kv = new EM.EmailWebRef.ReplaceKeyValueType();
                //kv.TemplateKey = "[[VerifyCompany]]";
                //kv.ReplacementValue = prodCfg..ToString().ToUpperInvariant();
                //kvList.Add(kv);

                //kv = new EM.EmailWebRef.ReplaceKeyValueType();
                //kv.TemplateKey = "[[VerifyProductSN]]";
                //kv.ReplacementValue = verifyProductSN.ToString().ToUpperInvariant();
                //kvList.Add(kv);

                //kv = new EM.EmailWebRef.ReplaceKeyValueType();
                //kv.TemplateKey = "[[IsPaidSupportModel]]";
                //kv.ReplacementValue = prdCfg.IsPaidSupportModel.ToString();
                //kvList.Add(kv);
                
                if (prodRegWebAccessKeys != null)
                {
                    String adminApprovalUrlFormat = ConfigUtility.AppSettingGetValue("Connect.Web.ProdRegEmail.AdminApprovalUrlFormat");
                    StringBuilder sbRegApprovalUrls = new StringBuilder();
                    sbRegApprovalUrls.Append("<ul>");
                    foreach (String sn in prodRegWebAccessKeys.Keys)
                    {
                        Guid wak = prodRegWebAccessKeys[sn];
                        if (wak == Guid.Empty) continue;
                        sbRegApprovalUrls.AppendFormat("<li><a href='{0}'>{1}</a></li>"
                            , adminApprovalUrlFormat.Replace("[[PRODREGWAK]]", wak.ToString())
                            , sn);
                    }
                    sbRegApprovalUrls.Append("</ul>");
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[ProdRegApprovalURLs]]";
                    kv.ReplacementValue = sbRegApprovalUrls.ToString();//System.Web.HttpUtility.HtmlEncode(sbRegApprovalUrls.ToString());
                    kvList.Add(kv);
                }
                #endregion

            }

            if (user != null)
            {
                #region " user information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
                kvList.Add(kv);
                #endregion
            }

            if (account != null)
            {
                #region " account information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountID]]";
                kv.ReplacementValue = account.AccountID.ToString().ToUpperInvariant();
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountName]]";
                kv.ReplacementValue = account.AccountName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyName]]";
                kv.ReplacementValue = account.CompanyName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyAddress]]";
                kv.ReplacementValue = account.CompanyAddress;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountStatusCode]]";
                kv.ReplacementValue = account.AccountStatusCode;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-IsVerifiedAndApproved]]";
                kv.ReplacementValue = account.IsVerifiedAndApproved.ToString().ToUpperInvariant();
                kvList.Add(kv);
                #endregion
            }
            return kvList;
        }

        private static List<EM.EmailWebRef.ReplaceKeyValueType> Email_GetKeyValues_ForPaidSupportRenew(
            Lib.Security.Sec_UserMembership user
            , Lib.Account.Acc_AccountMaster account
             , ProdReg_Master prodRegMaster
            , ProdReg_Item itemToRenew
            , Lib.Cfg.ProductConfig pcfg)
        {
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;
            if (prodRegMaster != null && itemToRenew != null)
            {
                #region " product information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[ModelNumber]]";
                kv.ReplacementValue = itemToRenew.ModelNumber;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SerialNumber]]";
                kv.ReplacementValue = itemToRenew.SerialNumber;
                kvList.Add(kv);

                if (itemToRenew.Config_JPSW != null)
                {
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[PaidSupportExpiredOn]]";
                    kv.ReplacementValue = itemToRenew.Config_JPSW.JPSupportContract.ToString("d");
                    kvList.Add(kv);
                }

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[WebAccessKey]]";
                kv.ReplacementValue = prodRegMaster.WebToken.ToString();
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[RenewRequestedForModelNumber]]";
                kv.ReplacementValue = itemToRenew.ModelNumber;
                kvList.Add(kv);
                #endregion
            }

            if (user != null)
            {
                #region " user information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = user.Email;
                kvList.Add(kv);


                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(user.FirstNameInEnglish))
                    kv.ReplacementValue = user.FirstNameInEnglish;
                else
                    kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(user.LastNameInEnglish))
                    kv.ReplacementValue = user.LastNameInEnglish;
                else
                    kv.ReplacementValue = user.LastName;
                #endregion
            }

            if (account != null)
            {
                #region " account information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountID]]";
                kv.ReplacementValue = account.AccountID.ToString().ToUpperInvariant();
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountName]]";
                kv.ReplacementValue = account.AccountName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyName]]";
                kv.ReplacementValue = account.CompanyName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-CompanyAddress]]";
                kv.ReplacementValue = account.CompanyAddress;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountStatusCode]]";
                kv.ReplacementValue = account.AccountStatusCode;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-IsVerifiedAndApproved]]";
                kv.ReplacementValue = account.IsVerifiedAndApproved.ToString().ToUpperInvariant();
                kvList.Add(kv);
                #endregion
            }

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[BaseUrl]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);
            return kvList;
        }

        public static Boolean EndUser_SubmitSupportRenewal(AnrSso.ConnectSsoUser user
            , Lib.Account.Acc_AccountMaster account
            , ProdReg_Master prodRegMaster
            , ProdReg_Item itemToRenew
            , Lib.Cfg.ProductConfig pcfg
            , List<KeyValuePair<String, String>> addOnFormData
            , Int32 gwCultureGroupId
            , out Boolean emailSent
            , out String messageOut)
        {
            messageOut = String.Empty;
            emailSent = false;
            if (addOnFormData == null) addOnFormData = new List<KeyValuePair<string, string>>();

            #region user full name
            String userFullName = string.Empty;

            if (!string.IsNullOrEmpty(user.FirstNameInEnglish) && !string.IsNullOrEmpty(user.LastNameInEnglish))
                userFullName = user.LastNameInEnglish + "," + user.FirstNameInEnglish;
            else
                userFullName = user.LastName + "," + user.FirstName;

            #endregion

            try
            {
                Int32 prodRegID = prodRegMaster.ProdRegID;
                String userIP = WebUtility.GetUserIP();

                #region " renew info log "
                Random rand = new Random();
                String renewPrefix = String.Format("RenewalReq{0}_{1}", rand.Next(1000, 9999), DateTime.UtcNow.ToString("MMM-dd-yyyy"));

                KeyValuePair<string, string> kvpLog;
                kvpLog = new KeyValuePair<string, string>(String.Format("{0}_MN", renewPrefix), itemToRenew.ModelNumber);
                addOnFormData.Add(kvpLog);

                kvpLog = new KeyValuePair<string, string>(String.Format("{0}_RequestedBy", renewPrefix), userFullName);
                addOnFormData.Add(kvpLog);

                kvpLog = new KeyValuePair<string, string>(String.Format("{0}_IP", renewPrefix), userIP);
                addOnFormData.Add(kvpLog);

                String geoCountryCode = String.Empty;
                AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
                if (gi != null) geoCountryCode = gi.CountryCode;
                kvpLog = new KeyValuePair<string, string>(String.Format("{0}_GEO", renewPrefix), geoCountryCode);
                addOnFormData.Add(kvpLog);

                #endregion

                foreach(KeyValuePair<String,String> kv in addOnFormData)
                {
                    ProdReg_DataBLL.Save(prodRegID, kv.Key, kv.Value);
                }
                //ProdReg_ItemBLL.UpdateStatus(itemToRenew.ProdRegItemID, "supportrenewsubmitted", "SYSTEM");

                String msg = String.Empty;
                emailSent = PaidSupportRenew_NotifyRequest(user, account, prodRegMaster, itemToRenew, pcfg, out msg);
                if (!emailSent) throw new ArgumentException(msg);

                return true;
            }
            catch (Exception ex)
            {
                messageOut = ex.Message;
                return false;
            }
        }

        public static Boolean PaidSupportRenew_NotifyRequest(AnrSso.ConnectSsoUser user
            , Lib.Account.Acc_AccountMaster account
            , ProdReg_Master prodRegMaster
            , ProdReg_Item itemToRenew
            , Lib.Cfg.ProductConfig pcfg
            , out String errorMsg)
        {
            if (user == null) throw new ArgumentNullException("user");
            if (account == null) throw new ArgumentNullException("account");
            if (prodRegMaster == null) throw new ArgumentNullException("prodReg");
            if (itemToRenew == null) throw new ArgumentNullException("itemToRenew");
            if (pcfg == null) throw new ArgumentNullException("prdCfg");
            errorMsg = String.Empty;
            Lib.Cfg.ModelConfigTypeInfo mct = ProductConfigUtility.ModelConfigType_GetInfo(itemToRenew.ModelConfigType);

            List<EM.EmailWebRef.ReplaceKeyValueType> emailInfo = Email_GetKeyValues_ForPaidSupportRenew(user, account, prodRegMaster, itemToRenew, pcfg);
            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest email =
                   new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.PaidSupportRenewEmailForAdmin");
            email.DistributionListKey = mct.EmailDistKey_NotifyReg;
            email.CultureGroupId = 1;
            email.SubjectReplacementKeyValues = emailInfo.ToArray();
            email.BodyReplacementKeyValues = emailInfo.ToArray();
            EM.EmailWebRef.BaseResponseType resp = EM.EmailServiceManager.SendDistributedTemplatedEmail(email);
            if (resp != null) errorMsg = resp.ResponseMessage;
            return (resp != null && resp.StatusCode == 0);
        }
    }
}