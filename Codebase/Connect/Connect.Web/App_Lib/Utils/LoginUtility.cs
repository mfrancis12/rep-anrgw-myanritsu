﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Globalization;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using DevExpress.XtraRichEdit.API.Native.Implementation;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class LoginUtility
    {
        public static void SignOut()
        {
            if (HttpContext.Current == null) { return; }
            if (HttpContext.Current.Request == null) { return; }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Redirect(KeyDef.UrlList.SignOut, true);

        }

        public static AnrSso.ConnectSsoUser GetCurrentUserOrSignIn()
        {
            return GetCurrentUser(true);
        }

        public static AnrSso.ConnectSsoUser GetCurrentUser(Boolean redirectIfNull)
        {
            AnrSso.ConnectSsoUser usr = AnrSsoUtility.LoggedInUser_Get();
            //if user null and auth cookie exists load user from db and set the caching
            if (usr == null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                //get the user and set 
                var decryptedAuthCookie =
              SecurityUtilities.ClientCacheEncrypter.DecryptString(HttpContext.Current.User.Identity.Name);
                if (string.IsNullOrEmpty(decryptedAuthCookie)) return null;
                var splittedIek = decryptedAuthCookie.Split('|');
                int aClUserId = 0;
                if (splittedIek.Length != 2 || !int.TryParse(splittedIek[0], out aClUserId))
                    HttpContext.Current.Response.Redirect(KeyDef.UrlList.SignIn);

                string secretKey = splittedIek[1];
                Lib.Security.Sec_UserMembership cntUser = null;
                if (aClUserId > 0) cntUser = Lib.BLL.BLLSec_UserMembership.SelectByGWUserId(aClUserId);
                usr = new AnrSso.ConnectSsoUser(cntUser);
                AnrSsoUtility.LoggedInUser_Set(usr);
                usr = AnrSsoUtility.LoggedInUser_Get();
            }
            if (usr != null || !redirectIfNull) return usr;
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
            HttpContext.Current.Response.End();
            //return null;
            return usr;
        }

        public static Boolean IsAdminMode()
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.User == null || !context.User.Identity.IsAuthenticated) return false;
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUserOrSignIn();
            if (usr == null) return false;
            Guid selectedAccID = AccountUtility.GetSelectedAccountID(false);
            if (usr == null || AccountUtility.AnritsuMasterAccountID() != selectedAccID) return false;
            Boolean isAdminMode = (usr.IsAdministrator
                                   || IsProdConfigAdminRole()
                                   || IsProdRegAdminRole()
                                   || IsDistPortalAdminRole()
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageCompany)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ReportMMDFeedback)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ReportUS)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ReportJP)
                                   || context.User.IsInRole(KeyDef.UserRoles.Admin_ReportTAU)
                                                       );
            return isAdminMode;
        }

        public static void CheckTermsOfUseAgreement()
        {
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            Lib.Account.Acc_AccountMaster am = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
           
           
            if (am == null) //avoid object reference error
            {
                String redirectURL = String.Format("{0}?ReturnURL={1}", KeyDef.UrlList.ManageOrg,
                      HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
                WebUtility.HttpRedirect(null, redirectURL);
            }
            else
            {
                if (!am.AgreedToTerms)
                {
                    HttpContext.Current.Session[KeyDef.QSKeys.ReturnURL] = HttpContext.Current.Request.RawUrl;
                    // String url = String.Format("{0}?{1}={2}", KeyDef.UrlList.TermsOfUse, KeyDef.QSKeys.ReturnURL, HttpUtility.UrlEncode(HttpContext.Current.Request.RawUrl));
                    String url = KeyDef.UrlList.TermsOfUse;
                    WebUtility.HttpRedirect(null, url);
                }
            }
        }

        public static void CheckSiteAdminUserRegistration()
        {
            Guid selectedAccID = AccountUtility.GetSelectedAccountID(false);
            if (AccountUtility.AnritsuMasterAccountID() == selectedAccID)
            {
                AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUserOrSignIn();
                List<String> allowedUserEmails = ConfigUtility.AppSettingGetValue("Connect.Web.SiteAdminAcc.AllowReg").Split(';').ToList();
                if (allowedUserEmails == null || !allowedUserEmails.Contains(user.Email, StringComparer.InvariantCultureIgnoreCase))
                {
                    SiteUtility.RedirectToMessagePage("You are not allowed to register products in this account.", KeyDef.UrlList.Home, KeyDef.UrlList.Home);
                }
            }
        }

        /// <summary>
        /// checks if the user is permitted
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static Boolean IsAdminRole(String userRole)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.User == null || !context.User.Identity.IsAuthenticated) return false;
            App_Lib.AnrSso.ConnectSsoUser usr = LoginUtility.GetCurrentUser(false);
            if (usr == null) return false;
            Guid selectedAccID = AccountUtility.GetSelectedAccountID(false);
            if (usr == null || AccountUtility.AnritsuMasterAccountID() != selectedAccID) return false;
            Boolean isAdminMode = (usr.IsAdministrator || context.User.IsInRole(userRole));
            return isAdminMode;
        }

        /// <summary>
        /// Checks if Prod config admin
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public static Boolean IsProdConfigAdminRole()
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.User == null || !context.User.Identity.IsAuthenticated) return false;
            App_Lib.AnrSso.ConnectSsoUser usr = GetCurrentUser(false);
            if (usr == null) return false;
            Guid selectedAccID = AccountUtility.GetSelectedAccountID(false);
            if (usr == null || AccountUtility.AnritsuMasterAccountID() != selectedAccID) return false;
            Boolean isAdminMode = (usr.IsAdministrator
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_JPAN)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_JPM)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_USMMD)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_UKTAU)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_DKM)
              );
            return isAdminMode;
        }

        /// <summary>
        /// Checks if Prod Reg admin
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        public static Boolean IsProdRegAdminRole()
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.User == null || !context.User.Identity.IsAuthenticated) return false;
            App_Lib.AnrSso.ConnectSsoUser usr = GetCurrentUser(false);
            if (usr == null) return false;
            Guid selectedAccID = AccountUtility.GetSelectedAccountID(false);
            if (usr == null || AccountUtility.AnritsuMasterAccountID() != selectedAccID) return false;
            Boolean isAdminMode = (usr.IsAdministrator
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageProdReg)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_USMMD)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_JPM)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_JPAN)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_ESD)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_DKSW)
             );
            return isAdminMode;
        }

        public static Boolean IsDistPortalAdminRole()
        {
            return (IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA) || IsAdminRole(KeyDef.UserRoles.Admin_SIS_US));

        }

        public static Boolean IsPackageAdminRole()
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.User == null || !context.User.Identity.IsAuthenticated) return false;
            ConnectSsoUser usr = GetCurrentUser(false);
            if (usr == null) return false;
            Guid selectedAccId = AccountUtility.GetSelectedAccountID(false);
            if (AccountUtility.AnritsuMasterAccountID() != selectedAccId) return false;
            Boolean isAdminMode = (usr.IsAdministrator
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)
             || context.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU)
             || context.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages)
              );
            return isAdminMode;
        }
        public static void SetLoggedInStatus(bool loggedInStatus)
        {
            //Check for cookie exists
            var loginStatusCookie =
                HttpContext.Current.Request.Cookies[ConfigUtility.AppSettingGetValue("Connect.Web.LoggedInStatus")];
            var timeOut = FormsAuthentication.Timeout.Minutes;
            if (loginStatusCookie == null || String.IsNullOrEmpty(loginStatusCookie.Value))
            {
                //Initialize the cookie and set the value
                if (loggedInStatus)
                {
                  
                    loginStatusCookie = new HttpCookie(ConfigUtility.AppSettingGetValue("Connect.Web.LoggedInStatus"),
                        loggedInStatus.ToString())
                    {
                        Domain = "anritsu.com",
                        Secure = false,
                        Expires = DateTime.Now.AddMinutes(timeOut > 0 ? timeOut : 30),
                    };
                }
            }
            else
            {
                loginStatusCookie.Expires = loggedInStatus ? DateTime.Now.AddMinutes(timeOut > 0 ? timeOut : 30) :
                    DateTime.Now.AddDays(-1);
            }
            if (loginStatusCookie != null && loggedInStatus)
                HttpContext.Current.Response.Cookies.Add(loginStatusCookie);
            else
            {
                var clonedCookie = new HttpCookie(ConfigUtility.AppSettingGetValue("Connect.Web.LoggedInStatus"))
                {
                    Expires = DateTime.Now.AddDays(-1d),
                    Domain = "anritsu.com"
                };
                HttpContext.Current.Response.Cookies.Add(clonedCookie);
            }
        }
    }
}