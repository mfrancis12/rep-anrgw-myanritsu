﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class ProductConfigUtility
    {
        public static Lib.Cfg.ProductConfig SelectByModel(Boolean allowSelfRegOnly, String modelNumber)
        {
            App_Lib.AppCacheFactories.ModelConfig_CacheFactory dataCache = new AppCacheFactories.ModelConfig_CacheFactory();
            return dataCache.GetData(allowSelfRegOnly, modelNumber);
        }

        public static Lib.Cfg.ModelConfigTypeInfo ModelConfigType_GetInfo(String modelConfigType)
        {
            App_Lib.AppCacheFactories.ModelConfigType_CacheFactory dataCache = new AppCacheFactories.ModelConfigType_CacheFactory();
            return dataCache.GetConfigInfo(modelConfigType);
        }
    }
}