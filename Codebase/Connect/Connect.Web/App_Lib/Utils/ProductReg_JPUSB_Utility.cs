﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Globalization;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.AnrSso;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class ProductReg_JPUSB_Utility
    {
        public static Lib.DongleDownload.DongleLic JapanUSBLicDataGet(Boolean userValidatedOnly)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return null;
            Lib.DongleDownload.DongleLic lic = context.Session[KeyDef.SSKeys.JapanUSB_AccessPass] as Lib.DongleDownload.DongleLic;
            if (lic == null) return null;
            if (userValidatedOnly && !lic.IsUserValidated) return null;
            return lic;
        }

        public static void JapanUSBLicDataSet(Lib.DongleDownload.DongleLic keyInfo)
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return;
            context.Session[KeyDef.SSKeys.JapanUSB_AccessPass] = keyInfo;
        }

        public static Boolean CheckValidJapanUSBLicSession()
        {
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null) return false;
            Lib.DongleDownload.DongleLic lic = JapanUSBLicDataGet(true);
            return lic != null;
        }
    }
}