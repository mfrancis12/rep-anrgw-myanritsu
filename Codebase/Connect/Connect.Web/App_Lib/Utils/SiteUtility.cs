﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Globalization;
using System.Data;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.AnrSso;

namespace Anritsu.Connect.Web.App_Lib.Utils
{
    internal static class SiteUtility
    {
        public static void SignOut()
        {
            if (HttpContext.Current == null) { return; }
            if (HttpContext.Current.Request == null) { return; }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Redirect(KeyDef.UrlList.SignOut, true);

        }

        public static String BrowserLang_GetFromApp()
        {
            return System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
        }

        public static string GetPageUrl()
        {
            if (HttpContext.Current == null) return null;
            string relUrl = "~" + HttpContext.Current.Request.FilePath;
            return relUrl;
        }

        public static Boolean BrowserLang_IsJapanese()
        {
            String llCC = BrowserLang_GetFromApp();
            return llCC.Equals("ja", StringComparison.InvariantCultureIgnoreCase)
                || llCC.Equals("ja-jp", StringComparison.InvariantCultureIgnoreCase);
        }

        public static String DetectGlobalWebRegion()
        {
            string locale = "en-AU";
            HttpContext context = HttpContext.Current;
            if (context == null) return locale;
            Boolean isUserAuthenticated = context.Request.IsAuthenticated;

            if (isUserAuthenticated)
            {
                Lib.Account.Acc_AccountMaster selectedAccount = AccountUtility.GetSelectedAccount(false, false);
                if (selectedAccount != null)
                {
                    String countryCode = selectedAccount.CompanyCountryCode;
                    String sessionKey = String.Format("{0}_{1}_{2}", KeyDef.SSKeys.AccountOrIPBasedCultureGroup, isUserAuthenticated, countryCode);
                    if (context.Session != null)
                    {
                        String inSesssion = ConvertUtility.ConvertNullToEmptyString(HttpContext.Current.Session[sessionKey]);
                        if (!inSesssion.IsNullOrEmptyString()) return inSesssion;
                    }

                    Int32 cultureGroupId = 1;
                    Lib.BLL.BLLIso_Country.GetCultureGroupInfoByCountry(selectedAccount.CompanyCountryCode
                        , out cultureGroupId, out locale);
                    if (!locale.IsNullOrEmptyString())
                    {
                        HttpContext.Current.Session[sessionKey] = locale;
                        return locale;
                    }
                }
            }

            String ip = WebUtility.GetUserIP();
            String sessionKeyForIP = String.Format("{0}_{1}-{2}", KeyDef.SSKeys.AccountOrIPBasedCultureGroup, isUserAuthenticated, ip);
            String inSesssionForIP = ConvertUtility.ConvertNullToEmptyString(HttpContext.Current.Session[sessionKeyForIP]);
            if (!inSesssionForIP.IsNullOrEmptyString()) return inSesssionForIP;

            AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi == null) return "en-AU";
            else
            {
                HttpContext.Current.Session[sessionKeyForIP] = gi.Region_CultureCode;
                return gi.Region_CultureCode;
            }

        }

        public static Int32 BrowserLang_GetGlobalWebCultureGroupId()
        {
            String llCC = BrowserLang_GetFromApp();
            Int32 cultureGroupId = 1;
            HttpContext context = HttpContext.Current;
            if (context == null || context.Session == null)
            {
                cultureGroupId = Lib.BLL.BLLIso_Country.GetCultureGroupIDByCultureCode(llCC);
            }
            else
            {
                string sessionKey = String.Format("{0}_{1}", KeyDef.SSKeys.BrowserBasedCultureGroupId, llCC);
                cultureGroupId = ConvertUtility.ConvertToInt32(context.Session[sessionKey], 0);
                if (cultureGroupId < 1)
                {
                    cultureGroupId = Lib.BLL.BLLIso_Country.GetCultureGroupIDByCultureCode(llCC);
                    context.Session[sessionKey] = cultureGroupId;
                }
            }
            return cultureGroupId;
        }

        public static String GeoLB_IPBasedGWRegion()
        {
            HttpContext context = HttpContext.Current;
            String gwRegionCode = "en-AU";
            if (context == null) return gwRegionCode;

            String sessionKey = "GeoLB_IPBasedGWRegionCode";
            if (context.Session != null && context.Session[sessionKey] != null)
            {
                String gwRegionCodeInSession = context.Session[sessionKey].ToString();
                if (!gwRegionCodeInSession.IsNullOrEmptyString()) return gwRegionCodeInSession;
            }

            String ip = WebUtility.GetUserIP();
            AnrCommon.GeoLib.GeoInfo gi = AnrCommon.GeoLib.BLL.BLLGeoInfo.GetCountryInfoFromIP4();
            if (gi != null) gwRegionCode = gi.Region_CultureCode;

            if (context.Session != null) context.Session[sessionKey] = gwRegionCode;
            return gwRegionCode;
        }

        public static void RedirectToMessagePage(String msgText, String backUrl, String nextUrl)
        {
            UI.MessageData msg = new UI.MessageData();
            msg.MessageText = ConvertUtility.ConvertNullToEmptyString(msgText).Trim();
            msg.BackUrl = ConvertUtility.ConvertNullToEmptyString(backUrl).Trim();
            msg.NextUrl = ConvertUtility.ConvertNullToEmptyString(nextUrl).Trim();
            HttpContext context = HttpContext.Current;
            if (context.Session == null) throw new ArgumentNullException("Session");
            context.Session[App_Lib.KeyDef.SSKeys.ShowMessage] = msg;
            context.Response.Redirect(KeyDef.UrlList.ShowMessagePage);            
        }

    }
}