﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.App_MasterPages
{
    public partial class DocHeaderMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetRightText(String text)
        {
            ltrRightText.Text = text;
        }

        public void SetSloganText(String text)
        {
            ltrSlogan.Text = text;
        }

        public void SetSloganBottomText(String text)
        {
            ltrSloganBottom.Text = text;
        }
    }
}