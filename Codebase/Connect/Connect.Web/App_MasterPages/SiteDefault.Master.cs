﻿using System;
using System.Web;
using System.Web.UI;
using Anritsu.Connect.Lib.BLL;
using Anritsu.Connect.Lib.UI;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Configuration;

namespace Anritsu.Connect.Web.App_MasterPages
{
    public partial class SiteDefault : MasterPage
    {
        protected string CDNPath
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["GwdataCdnPath"]); }
        }
        public bool ShowPageTitle
        {
            set { pt.Visible = value; divpagetitle.Visible = value; }
          
        }

        public String MyAnritsuChildLinks
        {
            get
            {
                return String.IsNullOrEmpty(_myAnritsuChildLinks)
                    ? GwLeftMenuApi.GenerateSideMenuHtml(GwLeftMenuApi.GetAbstractMyAnritsuChildLinks(HasDistributorPortalRole()))
                    : _myAnritsuChildLinks;
            }
            set { _myAnritsuChildLinks = value; }
        }

        private String _myAnritsuChildLinks;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (!Page.User.Identity.IsAuthenticated) return;
            var acc = BLLAcc_AccountMaster.SelectByAccountID(AccountUtility.GetSelectedAccountID(false));
            if (acc == null) return;
            //hlSelectAccount.NavigateUrl = KeyDef.UrlList.SelectTeam;
            //hlSelectAccount.Text = HttpUtility.HtmlEncode(acc.AccountName);
           // ltrGwSideMenu.Text = ""; GwLeftMenuApi.GetMyAnritsuLeftMenu(MyAnritsuChildLinks);

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Is this required for FireFox? Would be good to do this without magic strings.
            // Won't it overwrite the previous setting
            Response.Headers.Add("Cache-Control", "no-cache, no-store");
            Response.Cache.SetNoStore();
            // Why is it necessary to explicitly call SetExpires. Presume it is still better than calling
            // Response.Headers.Add( directly
            Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));
        }

        private bool HasDistributorPortalRole()
        {
            if (!Page.User.Identity.IsAuthenticated) return false;
            var acc = AccountUtility.GetSelectedAccount(false, true);
            if (acc != null)
            {
                return (acc.Config_DistributorPortal != null &&
                        acc.Config_DistributorPortal.StatusCode.Equals("active",
                            StringComparison.InvariantCultureIgnoreCase));

            }
            return false;
        }

    }
}