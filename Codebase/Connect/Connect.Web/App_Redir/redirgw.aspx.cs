﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib.CryptoUtilities;
namespace Anritsu.Connect.Web.App_Redir
{
    public partial class redirgw : System.Web.UI.Page
    {
        private String TargetURL
        {
            get
            {
                return Request.QueryString[KeyDef.QSKeys.TargetURL].ConvertNullToEmptyString();
            }
        }

        private Int32 UrlID
        {
            get
            {
                return ConvertUtility.ConvertToInt32( Request.QueryString[KeyDef.QSKeys.SsoUrlID], 0);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            String targetUrl = TargetURL;
            if (targetUrl.IsNullOrEmptyString())
            {
                DataRow r = Lib.UI.Site_LegacySsoUrlBLL.SelectBySsoUrlID(UrlID);
                if (r == null) WebUtility.HttpRedirect(null, KeyDef.UrlList.Home);
                targetUrl = r["TargetUrl"].ToString().Trim();
            }
            if (!Request.IsAuthenticated) WebUtility.HttpRedirect(null, targetUrl);

            String[] iekDomains = ConfigUtility.AppSettingGetValue("Connect.Web.GWSSO.IekDomains").ConvertNullToEmptyString().Split(',');
            Uri targetUri = new Uri(targetUrl);
            if (iekDomains == null || targetUri == null) WebUtility.HttpRedirect(null, KeyDef.UrlList.Home);

            App_Lib.AnrSso.ConnectSsoUser user = LoginUtility.GetCurrentUser(false);
            if (user != null && iekDomains.Contains(targetUri.Host.ToString().ToLowerInvariant()))
            {
                AnritsuEncrypter enc = new AnritsuEncrypter("E2BE0601-0AFE-4306-8FE3-FDC60CB3FD6C");                
                String iekEncryptedValue = enc.EncryptData(
                   String.Format("{0}|AC|{1}", user.GWUserId, user.GWSecretKey.ToString()));
                String url = ConfigUtility.AppSettingGetValue("Connect.Web.GWSSO.RouteUrl");
                NameValueCollection inputs = new NameValueCollection();
                //inputs.Add("TargetURL", HttpUtility.UrlEncode(targetUrl));
                inputs.Add("TargetURL", targetUrl);
                inputs.Add("iek", HttpUtility.UrlEncode(iekEncryptedValue));
                WebUtility.DoPost("frmWWW1", "POST", url, inputs);
            }
            else
            {
                WebUtility.HttpRedirect(null, targetUrl);
            }
        }
    }
}