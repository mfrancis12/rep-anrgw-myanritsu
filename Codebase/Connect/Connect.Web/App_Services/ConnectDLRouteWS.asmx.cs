﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.Connect.Web.App_Services
{
    /// <summary>
    /// Summary description for ConnectDLRouteWS
    /// </summary>
    [WebService(Namespace = "https://webservices.anritsu.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ConnectDLRouteWS : System.Web.Services.WebService
    {
        /// <summary>
        /// Lock down this later so that only apps from RS can access this
        /// </summary>
        /// <param name="tokenID"></param>
        /// <param name="logAccess"></param>
        /// <returns></returns>
        [WebMethod]
        public Lib.DownloadRoute.GW_DownloadRoutingToken GetDownloadInfo(String tokenID, Boolean logAccess)
        {
            Guid tokenGuidID = ConvertUtility.ConvertToGuid(tokenID, Guid.Empty);
            if (tokenGuidID.IsNullOrEmptyGuid()) return null;

            return Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.SelectByTokenID(tokenGuidID, logAccess);
        }

        //[WebMethod]
        //public Guid AddDownloadInfo(Lib.DownloadRoute.DownloadTypeCode downloadType,
        //    String fileName,
        //    String downloadCategory,
        //    String modelNumber,
        //    String requestedUserEmail,
        //    String requestedUserName,
        //    String requestedUserSessionID,
        //    String requestedUserIP,
        //    Int32 encodeFlag)
        //{
        //    if (downloadType == null || fileName.IsNullOrEmptyString() || requestedUserEmail.IsNullOrEmptyString() || requestedUserIP.IsNullOrEmptyString()) return Guid.Empty;

        //    return Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.Insert(downloadType,
        //        fileName,
        //        downloadCategory,
        //        modelNumber,
        //        requestedUserEmail,
        //        requestedUserName,
        //        requestedUserSessionID,
        //        requestedUserIP,
        //        encodeFlag);
        //}

        //[WebMethod]
        //public Lib.DownloadRoute.GW_DownloadRoutingToken ExtendExp(String tokenID, DateTime newExpUTC)
        //{
        //    Guid tokenGuidID = ConvertUtility.ConvertToGuid(tokenID, Guid.Empty);
        //    if (tokenGuidID.IsNullOrEmptyGuid()) return null;

        //    Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.UpdateTokenExp(tokenGuidID, newExpUTC);
        //    return Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.SelectByTokenID(tokenGuidID);
        //}

    }
}
