﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ActivityLog;

namespace Anritsu.Connect.Web.App_Services
{
    /// <summary>
    /// Summary description for TAUDLClientLog
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class TAUDLClientLog : System.Web.Services.WebService
    {
        [WebMethod(Description = "", EnableSession = false), ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        public string LogClientAttemptedDL(string FilePath
            , string FileSize
            , string EmailID
            , string ClientIP
            , string ModelConfig)
        {
            String task = String.Format(ActivityLogNotes.FILE_DOWNLOAD_ATTEMPT, FilePath, ConvertBytesToKilobytes(FileSize));
            //update the client log
            Lib.ActivityLog.ActivityLogBLL.ClientLogInsert(EmailID, FilePath, task, ConvertUtility.ConvertToInt64(FileSize, 0), ClientIP, ModelConfig);
            //disable all caches
            Context.Response.Cache.SetAllowResponseInBrowserHistory(false);
            Context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Context.Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
            Context.Response.Cache.SetMaxAge(TimeSpan.Zero);
            Context.Response.Cache.SetNoServerCaching();
            Context.Response.Cache.SetNoStore();
            Context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            return "SUCCESS";
        }
        protected Double ConvertBytesToKilobytes(object bytes)
        {
            long inputbytes = ConvertUtility.ConvertToInt64(bytes, 0);
            if (inputbytes == 0) return 0.0;
            return Math.Round((inputbytes / 1024f), 2);
        }
    }
}
