﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="Web-Master.aspx.cs" Inherits="Anritsu.Connect.Web.ContactUs.Web_Master" %>

<%@ Register Src="~/App_Controls/EndUser_General/WebMasterContactCtrl.ascx" TagPrefix="uc2" TagName="WebMasterContactCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">
        <uc2:WebMasterContactCtrl runat="server" id="WebMasterContactCtrl1" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>

