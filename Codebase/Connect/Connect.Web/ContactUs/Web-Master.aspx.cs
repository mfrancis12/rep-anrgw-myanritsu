﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.ContactUs
{
    public partial class Web_Master : App_Lib.UI.BP_MyAnritsu
    {
        public String VisitedFromUrl
        {
            get
            {
                String URL = String.Empty;
                if (Request.UrlReferrer == null)
                {
                    URL = KeyDef.UrlList.ContactUs;
                }
                else
                {
                    URL = Request.UrlReferrer.AbsoluteUri;
                }
               return URL;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if(!IsPostBack)
                WebMasterContactCtrl1.URL = VisitedFromUrl;
            base.OnInit(e);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }


    }
}