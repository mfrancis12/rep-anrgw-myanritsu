﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="WebMaster-Submit.aspx.cs" Inherits="Anritsu.Connect.Web.ContactUs.WebMaster_Submit" %>

<%@ Register Src="~/App_Controls/EndUser_General/WebMasterContact_SubmittedCtrl.ascx" TagPrefix="uc1" TagName="WebMasterContact_SubmittedCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div style="min-height: 400px;">
        <uc1:WebMasterContact_SubmittedCtrl runat="server" id="WebMasterContact_SubmittedCtrl" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
