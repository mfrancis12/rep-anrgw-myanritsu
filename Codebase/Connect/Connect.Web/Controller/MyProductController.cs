﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ProductRegistration;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
using Anritsu.Connect.Web.App_Lib.Utils;
using Newtonsoft.Json;

namespace Anritsu.Connect.Web.Controller
{
    //filter to handle the exception at controller level
    [WebAPIErrorFilter]
    public class MyProductController : ApiController
    {
        readonly ProdReg_DataSource _datasrcProd = new ProdReg_DataSource();
        // GET api/<controller> Test method
        public IEnumerable<string> Get()
        {
            string s = null;
            s.Count();
            return new[] { "value1", "value2" };
        }

        /// <summary>
        ///This API method is used to get all filter list associated with the logged in user registered products (All teams)
        /// </summary>
        [Authorize]
        public HttpResponseMessage GetProductFilters(string mk = "", string fk = "")
        {
            //get the TeamIds associated with the logged in user
            var clientStatusCode = HttpStatusCode.OK;
            var result = ProdReg_MasterBLL.SelectProductFilters(AccountUtility.GenerateUserTeamIds(), ref clientStatusCode, mk, fk);
            //return the Json response to the client
            return ComposeJsonResponse(clientStatusCode, result);
        }


        /// <summary>
        /// This API method is used to get the list of registered products of the logged in user (all associated team products)
        /// </summary>
        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetMyProducts(string mk = "", string fk = "", string ok = "")
        {
            //get the TeamIds associated with the logged in user
            var clientStatusCode = HttpStatusCode.OK;
            var registrations = _datasrcProd.GetMasterRegistrationsBasic(AccountUtility.GenerateUserTeamIds(), ref clientStatusCode, mk, fk, ok);
            string jsonString;
            //get the resource string from DB
            if (registrations == null || registrations.Count < 1)
            {
                var resValue = ConvertUtility.ConvertNullToEmptyString(HttpContext.GetGlobalResourceObject("STCTRL_MyRegisteredProductsCtrl", "EmptyData.Text"));
                jsonString = JsonConvert.SerializeObject(new ErrorInfo() { ErrorMessage = resValue, StatusCode = 200 });
            }
            else
                jsonString = JsonConvert.SerializeObject(registrations);
            //return the Json response the the client
            return ComposeJsonResponse(clientStatusCode, jsonString);
        }

        #region Helper methods
        private HttpResponseMessage ComposeJsonResponse(HttpStatusCode clientStatus, string result)
        {
            var response = Request.CreateResponse(clientStatus);
            //set the response content type to json
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion
    }
}