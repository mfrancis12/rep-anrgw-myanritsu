﻿using System;
using System.Web;
using System.Web.Http;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Web.App_Lib.Utils;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace Anritsu.Connect.Web
{

    public class ProductSupportTypeController : ApiController
    {
        ProductSupportEntity prodSupport = new ProductSupportEntity();

        /// <summary>
        /// Returns list of model numbers with given modelconfigType and param
        /// </summary>
        /// <param name="param">substring of modelnumer 3 chars minimum</param>
        /// <param name="modelConfigType">ModelConfigType</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string ModelNumber([FromUri]string param, [FromUri]string modelConfigType)
        {
            try
            {
                
                var models = prodSupport.ModelNumber(param, modelConfigType);
                return JsonConvert.SerializeObject(models);
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Returns list of user emailaddress starting with param
        /// </summary>
        /// <param name="param">substring of email address</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string EmailAddresses([FromUri]string param)
        {
            try
            {
                var models = prodSupport.EmailAddresses(param);
                return JsonConvert.SerializeObject(models);
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Returns list of organizations containing param
        /// </summary>
        /// <param name="param">substring of organization</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string Organizations([FromUri]string param)
        {
            try
            {
                var orgs = prodSupport.Organizations(param);
                return JsonConvert.SerializeObject(orgs);
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Creates a new Product Support Entry in the database
        /// </summary>
        /// <param name="formValues"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public object AddProductSupportEntry([FromBody] dynamic formValues)
        {
            try
            {
                int status = Convert.ToInt32(prodSupport.AddProductSupportEntry(formValues));
                if (status > 0)
                {
                    HttpContext.Current.Response.StatusCode = 201;

                }
                else
                {
                    HttpContext.Current.Response.StatusCode = 500;
                }

                return HttpContext.Current.Response.Status;
            }
            catch (Exception)
            {
                throw;
                return 500;
            }
        }
        /// <summary>
        /// Returns the list of Product Support types.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public object GetProductSupportList()
        {
            try
            {
                return JsonConvert.SerializeObject(prodSupport.GetProductSupportList());
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Authorize]
        public object GetSerialNumber([FromUri] string model, [FromUri] string serial)
        {
            //return
             var registeredSerialNumbers =  BLL_SupportEntry.GetSerialNumbers(model,serial);
             var gsnSerialNumbers = Lib.Erp.Erp_GSNDBBLL.GSNDB_SearchSerialNumbers(model, serial);
             return JsonConvert.SerializeObject(GetRegisteredAndGSNSerialNumbers(registeredSerialNumbers,gsnSerialNumbers));
            
        }

        private object GetRegisteredAndGSNSerialNumbers(object registeredSerialNumbers, List<String> gsnSerialNumbers)
        {
            List<String> registered = (List<String>)registeredSerialNumbers;
            return registered.Union(gsnSerialNumbers);
        }

        /// <summary>
        /// Updates the product support entry in database
        /// </summary>
        /// <param name="formValues"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public object UpdateProductSupportEntry([FromBody] dynamic formValues)
        {
            try
            {

                formValues.Add();
                int status = Convert.ToInt32(prodSupport.UpdateProductSupportEntry(formValues));
                if (status > 0)
                {
                    HttpContext.Current.Response.StatusCode = 201;

                }
                else
                {
                    HttpContext.Current.Response.StatusCode = 304;
                }

                return HttpContext.Current.Response.Status;
            }
            catch (Exception)
            {
                throw;
                return 500;
            }
        }

        /// <summary>
        /// Deletes the product support entry from database
        /// </summary>
        /// <param name="id">Id of product support entry</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public object DeleteProductSupportEntry([FromUri] string id)
        {
            try
            {

                int status = Convert.ToInt32(prodSupport.DeleteProductSupportEntry(id));
                if (status > 0)
                {
                    HttpContext.Current.Response.StatusCode = 200;

                }
                else
                {
                    HttpContext.Current.Response.StatusCode = 304;
                }

                return HttpContext.Current.Response.Status;
            }
            catch (Exception)
            {
                throw;
                return 500;
            }
        }

        /// <summary>
        /// Returns list of package group names
        /// </summary>
        
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public string PackageGroupNames([FromUri] string grpName)
        {
            try
            {
                var groupNames = BLL_PackageFile.GetPackageGrpNames(grpName);
                return JsonConvert.SerializeObject(groupNames);
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }
    }
}