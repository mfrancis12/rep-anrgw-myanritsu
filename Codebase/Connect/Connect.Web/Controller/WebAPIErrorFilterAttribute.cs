﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;
using Anritsu.AnrCommon.CoreLib;
using Newtonsoft.Json;

namespace Anritsu.Connect.Web.Controller
{
    public class WebAPIErrorFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //get the exception raised and log it to DB
            Lib.AppLog.LogError(context.Exception, HttpContext.Current);
            var errorInfo = new ErrorInfo() {ErrorMessage = "Internal server error, please contact administrator",StatusCode = 500};
            //set the response 
            var responseMsg = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.InternalServerError,
                Content =
                    new StringContent(JsonConvert.SerializeObject(errorInfo), Encoding.UTF8,
                        "application/json")
            };
            context.Response = responseMsg;
        }
    }
}