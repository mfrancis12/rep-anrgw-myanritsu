﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Anritsu.Connect.Web.Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow" style="min-height: 400px;">
        <asp:Localize ID="lcalCustomErrorMsg" runat="server" Text="<%$ Resources:common,lcalCustomErrorMsg.Text %>"></asp:Localize></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
