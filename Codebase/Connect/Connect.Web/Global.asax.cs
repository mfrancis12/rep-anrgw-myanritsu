﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
            RouteTable.Routes.MapHttpRoute(
           name: "DefaultApi",
            routeTemplate: "api/{controller}/{action}/{id}",
           defaults: new { id = System.Web.Http.RouteParameter.Optional }
         );
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //forcing all non SSL request to SSL as all pages requires Auth cookie
            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) &&
                HttpContext.Current.Request.IsLocal.Equals(false))
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"]
                                  + HttpContext.Current.Request.RawUrl);
            }
            if (Request.Path.Equals("/default.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = "/home";
                Response.End();
            }
            else if (Request.Path.EndsWith(".aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = Request.RawUrl.Replace(".aspx", string.Empty);
                Response.End();
            }
            ProcessLocaleFromURL();
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            //set the GW Culture to the session
            if (HttpContext.Current.Session == null) return;
            var sessionKey = SiteUtility.BrowserLang_GetFromApp();
            if (String.IsNullOrEmpty(Convert.ToString(Session[sessionKey])))
                Session[sessionKey] = Lib.BLL.BLLIso_Country.GetIsoCountryInfoByBrowserLang(sessionKey);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Lib.AppLog.LogError(Server.GetLastError(), HttpContext.Current);
            //Server.ClearError();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith("~/api");
        }
        //protected void Application_PostRequestHandlerExecute(object sender, EventArgs e)
        //{
        //    HttpApplication app = (HttpApplication)sender;

        //    if (app.Response.ContentType.Equals("text/html", StringComparison.InvariantCultureIgnoreCase)
        //            //|| app.Response.ContentType.Equals("text/plain", StringComparison.InvariantCultureIgnoreCase)
        //            )
        //    {
        //        app.Response.Filter = new App_Lib.UI.GWSsoFilter(app.Response.Filter);
        //    }
        //}

        private static void RegisterRoutes(RouteCollection routes)
        {
            string appRootPath = HttpContext.Current.Server.MapPath("~/");
            DirectoryInfo rootInfo = new DirectoryInfo(appRootPath);
            var dirs = from d in rootInfo.GetDirectories()
                       //where !d.FullName.Contains("DfsrPrivate")
                       where ((d.Attributes & FileAttributes.Hidden) == 0) && ((d.Attributes & FileAttributes.System) == 0)
                       select d;

            List<FileInfo> fiASPX = new List<FileInfo>();
            fiASPX.AddRange(rootInfo.GetFiles("*.aspx", SearchOption.TopDirectoryOnly));
            foreach (DirectoryInfo df in dirs)
            {
                FileInfo[] fi = df.GetFiles("*.aspx", SearchOption.AllDirectories);
                if (fi != null && fi.Length > 0)
                    fiASPX.AddRange(fi);
            }

            foreach (FileInfo fi in fiASPX)
            {
                if (fi.Name == "CmsPageLoader") continue;
                string webPath = fi.FullName.Replace(appRootPath, string.Empty).Replace("\\", "/");
                string routeUrl = webPath.Replace(".aspx", string.Empty);
                string physicalFile = "~/" + webPath;
                routes.MapPageRoute(routeUrl, routeUrl, physicalFile, false);
            }
            ////Framework.BLL.BLLCmsPage.RegisterRoutes(routes);
        }

        private void ProcessLocaleFromURL()
        {
            if (HttpContext.Current == null) return;
            NameValueCollection qs = HttpContext.Current.Request.QueryString;
            if (qs.AllKeys.Contains(KeyDef.QSKeys.SiteLocale))
            {

                var locale =ConvertUtility.ConvertToString(qs[KeyDef.QSKeys.SiteLocale], string.Empty);
                if(locale.ToLower().Equals("pub"))
                    WebUtility.HttpRedirectWithUpdatedQueryStrings(new Control(), string.Format("?{0}={1}",KeyDef.QSKeys.SiteLocale,Thread.CurrentThread.CurrentCulture.Name));

                CultureInfo ci = new CultureInfo(locale);
                if (ci != null)
                {
                    Thread.CurrentThread.CurrentUICulture = ci;
                    //we should not overwrite the CurrentCulture which is for date,currency,number,etc. formatting
                    //Thread.CurrentThread.CurrentCulture = ci;
                }
                
                //WebUtility.HttpRedirectWithUpdatedQueryStrings(null, newQS);
            }
        }
    }
}