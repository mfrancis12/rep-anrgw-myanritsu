﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="Anritsu.Connect.Web.Message" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div style="min-height: 300px">
<div class="settingrow" style="text-align: center;">
    <p class="msg"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></p>
    <br /><br />
</div>
<div class="text-center">
    <asp:HyperLink ID="hlBack" runat="server" Text="<%$Resources:STCPG_MSG,hlBack.Text %>" NavigateUrl="~/home" SkinID="button"></asp:HyperLink>
    <asp:HyperLink ID="hlNext" runat="server" Text="<%$Resources:STCPG_MSG,hlNext.Text %>" NavigateUrl="~/home" SkinID="button"></asp:HyperLink>
</div>
<div class="clearfloat"></div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">

</asp:Content>
