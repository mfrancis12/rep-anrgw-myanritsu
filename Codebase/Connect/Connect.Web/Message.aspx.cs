﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.UI;

namespace Anritsu.Connect.Web
{
    public partial class Message : App_Lib.UI.BP_MyAnritsu
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MessageData msgData = Session[App_Lib.KeyDef.SSKeys.ShowMessage] as MessageData;
                if (msgData == null)
                {
                    Response.Redirect(App_Lib.KeyDef.UrlList.Home);
                }
                else
                {
                    ltrMsg.Text = HttpUtility.HtmlEncode(ConvertUtility.ConvertNullToEmptyString(msgData.MessageText));
                    if (!msgData.BackUrl.IsNullOrEmptyString())
                    {
                        hlBack.NavigateUrl = msgData.BackUrl;
                        hlBack.Visible = true;
                    }
                    if (!msgData.NextUrl.IsNullOrEmptyString())
                    {
                        hlNext.NavigateUrl = msgData.NextUrl;
                        hlNext.Visible = true;
                    }
                    //Session.Remove(App_Lib.KeyDef.SSKeys.ShowMessage);
                }
            }
        }
    }
}