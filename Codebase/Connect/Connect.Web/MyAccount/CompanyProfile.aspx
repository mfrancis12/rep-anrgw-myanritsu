﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="CompanyProfile.aspx.cs" Inherits="Anritsu.Connect.Web.MyAccount.CompanyProfile" %>
<%@ Register src="~/App_Controls/CompanyProfileWzCtrl.ascx" tagname="CompanyProfileWzCtrl" tagprefix="uc2" %>
<%@ Register src="~/App_Controls/MyAccountMenuCtrl.ascx" tagname="MyAccountMenuCtrl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
<asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Middle" runat="server"><uc1:MyAccountMenuCtrl ID="MyAccountMenuCtrl1" runat="server" /></asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="settingrow">
<uc2:CompanyProfileWzCtrl ID="CompanyProfileWzCtrl1" runat="server" />
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    
</asp:Content>
