﻿using System;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.MyAccount
{
    public partial class CompanyProfile : MyAccountBasePage
    {
        public Guid AccountId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID], AccountUtility.GetSelectedAccountID(false));
            }
        }
        public Boolean AddAsNew
        {
            get
            {
                return Request.QueryString[KeyDef.QSKeys.AddNewOrg] == "1";
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CompanyProfileWzCtrl1.AddAsNew = AddAsNew;
            if (AddAsNew) SetNewPageTitle(ltrAddOnPageTitle);
            base.OnInit(e);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //check if the current user belongs to the company
                if (!CurrentUserHasAccessToTeam()) throw new HttpException(403, "Forbidden.");
                CompanyProfileWzCtrl1.LoadCompanyProfile(AccountId);
                Session[KeyDef.SSKeys.OrganizationListByMembershipId] = null;
            }
        }

        private bool CurrentUserHasAccessToTeam()
        {
            var dt =AccountUtility.AccountsForCurrentUserGet(false);
            if (dt == null || dt.Rows.Count == 0) return false;
            var drs = dt.Select("AccountID='" + AccountId + "'");
            return drs.Length > 0;
        }
    }
}