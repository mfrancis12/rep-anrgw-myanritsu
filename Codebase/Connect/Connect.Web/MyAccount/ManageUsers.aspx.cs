﻿using Anritsu.Connect.Web.App_Lib.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.MyAccount
{
    public partial class ManageUsers : App_Lib.UI.MyAccountBasePage
    {
        public Guid AccountId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID], AccountUtility.GetSelectedAccountID(false));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //check if the current user belongs to the company
            if (!CurrentUserHasAccessToTeam()) throw new HttpException(403, "Forbidden.");
        }
        private bool CurrentUserHasAccessToTeam()
        {
            var dt = AccountUtility.AccountsForCurrentUserGet(false);
            if (dt == null || dt.Rows.Count == 0) return false;
            var drs = dt.Select("AccountID='" + AccountId + "'");
            return drs.Length > 0;
        }
    }
}