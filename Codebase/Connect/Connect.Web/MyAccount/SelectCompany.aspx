﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="SelectCompany.aspx.cs" Inherits="Anritsu.Connect.Web.MyAccount.SelectCompany" %>
<%@ Register src="~/App_Controls/CompanyProfileListCtrl.ascx" tagname="CompanyProfileListCtrl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.cart-datagrid-cell{cursor: pointer!important;}
.cart-datagrid-row-selected .cart-datagrid-cell {background:#E1EBEC!important; border-bottom-color:#0FAF46!important; font-weight: bold;}
.cart-datagrid-row-hover .cart-datagrid-cell {border-bottom-color:#0FAF46; background:#E1EBEC!important; font-weight: bold; text-decoration: underline!important;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<uc1:CompanyProfileListCtrl ID="CompanyProfileListCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    
</asp:Content>
