﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Lib;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.MyAccount
{
    public partial class SelectCompany : App_Lib.UI.MyAccountBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindAccountList();
            }
        }

        //private void BindAccountList()
        //{
        //    List<Lib.Account.Acc_AccountMaster> lst = 
        //        Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(App_Lib.UIHelper.GetCurrentUserOrSignIn().MembershipId);
        //    dgList.DataSource = lst;
        //    dgList.DataBind();
        //}

        //protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        //{
        //    if (e.CommandName.Equals("SelectCommand"))
        //    {
        //        Guid accountID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
        //        if (accountID == null) App_Lib.UIHelper.SignOut();
        //        Lib.Security.Sec_UserMembership user = App_Lib.UIHelper.GetCurrentUserOrSignIn();
        //        Lib.Security.Sec_UserCache usrCache = new Lib.Security.Sec_UserCache();
        //        usrCache.DataKey = Lib.Security.Sec_UserCacheKeys.SelectedAccountID;
        //        usrCache.DataValue = accountID.ToString().ToUpperInvariant();
        //        usrCache.MembershipId = user.MembershipId;
        //        Lib.BLL.BLLSec_UserCache.Save(usrCache);
        //        App_Lib.UIHelper.RedirectToReturnURL();
        //    }
        //}
    }
}