﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/DistributorPortalMaster.Master" AutoEventWireup="true" CodeBehind="DistProductSupport.aspx.cs" Inherits="Anritsu.Connect.Web.MyDistributorInfo.DistProductSupport" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistSIS_ProductInfoCtrl.ascx" TagPrefix="uc2" TagName="DistSIS_ProductInfoCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistSIS_ProductInfoTabsCtrl.ascx" TagPrefix="uc1" TagName="DistSIS_ProductInfoTabsCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistSIS_ProductInfo_PageTitleCtrl.ascx" TagPrefix="uc3" TagName="DistSIS_ProductInfo_PageTitleCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistSIS_ProdInfo_DescCtrl.ascx" TagPrefix="uc4" TagName="DistSIS_ProdInfo_DescCtrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDistPortalHead" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphDistPortalPageTitle" runat="server">
    <uc3:DistSIS_ProductInfo_PageTitleCtrl runat="server" ID="DistSIS_ProductInfo_PageTitleCtrl1" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphDistPortalTopList" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphDistPortalLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphDistPortalLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphDistPortalLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphDistPortalContentTop" runat="server">
    <div class="settingrow"><uc2:DistSIS_ProductInfoCtrl runat="server" ID="DistSIS_ProductInfoCtrl1" /></div>
    <div class="settingrow">
    <uc1:DistSIS_ProductInfoTabsCtrl runat="server" ID="DistSIS_ProductInfoTabsCtrl1" />
    <div class="cart-tabstrip-inner-wrapper" style="width:99%; min-height: 50px; border-top: none; padding-left: 10px;">
        <uc4:DistSIS_ProdInfo_DescCtrl runat="server" id="DistSIS_ProdInfo_DescCtrl1" />    
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphDistPortalContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphDistPortalContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphDistPortalContentBottom" runat="server">
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto({
                show_title: false,
                allow_resize: true,
                social_tools: false,
            });
        });
</script>
</asp:Content>
