﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/DistributorPortalMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.MyDistributorInfo.Home" %>

<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/SISProductListCtrl.ascx" TagPrefix="uc2" TagName="SISProductListCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_DistributorSupport/DistributorHomeTabsCtrl.ascx" TagPrefix="uc1" TagName="DistributorHomeTabsCtrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphDistPortalHead" runat="server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphDistPortalPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphDistPortalTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphDistPortalLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphDistPortalLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphDistPortalLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="cphDistPageHeaderRight" runat="server">
    <a href="http://distributor.eu.anritsu.com" class="button right" style="margin-top:24px">Distributor Portal</a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphDistPortalContentTop" runat="server">
    <uc1:DistributorHomeTabsCtrl runat="server" ID="DistributorHomeTabsCtrl1" />
    <div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 50px; border-top: none; padding-left: 10px; box-sizing: border-box; background: #f9f9f9 ">
        <uc2:SISProductListCtrl runat="server" ID="SISProductListCtrl1" />
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphDistPortalContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphDistPortalContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphDistPortalContentBottom" runat="server">
    <script type="text/javascript" charset="utf-8">
        $("#DevXTbl").mousedown(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto({
                show_title: false,
                allow_resize: true,
                social_tools: false,
            });
        });

    </script>
</asp:Content>
