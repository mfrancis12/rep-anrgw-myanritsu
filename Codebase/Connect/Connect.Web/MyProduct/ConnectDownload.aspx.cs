﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources;
using Anritsu.Connect.Web.App_Lib.ObjectDataSources.EndUser_ProductSupport;
//using DLSDK = Anritsu.AnrCommon.GWDownloadRouteInfo.SDK;

namespace Anritsu.Connect.Web.MyProduct
{
    public enum AWS_ConfigRegionCode_DL1
    {
        US, EU, JP
    }

    public partial class ConnectDownload : Page
    {

        public Guid ProdRegWebToken
        {
            get
            {
                Guid wak = ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
                if (wak.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");
                return wak;
            }
        }

        public String ModelNumber
        {
            get
            {
                String mn = ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.ModelNumber]);
                if (mn.IsNullOrEmptyString()) throw new HttpException(404, "Page not found");
                return mn;
            }
        }
        private string _serialNumber;
        public String SerialNumber
        {
            get
            {
                string sn = ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.SerialNumber]);
                if (sn.IsNullOrEmptyString()) throw new HttpException(404, "Page not found");
                return sn;
            }
        }

        public String DownloadSource
        {
            get
            {
                String dlSrc = ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.DownloadSource]);
                if (dlSrc.IsNullOrEmptyString()
                    || (dlSrc != "dl1" && dlSrc != "dl2" && dlSrc != "dl3" && dlSrc != "dlv" && dlSrc != "calcert")
                    )
                {
                    throw new HttpException(404, "Page not found");
                }
                return dlSrc;
            }
        }

        public String DownloadID
        {
            get
            {
                String dlID = ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.DownloadID]);
                if (dlID.IsNullOrEmptyString()) throw new HttpException(404, "Page not found");
                return dlID;
            }
        }

        private AWS_ConfigRegionCode_DL1 GetConfigRegionCode()
        {
            AWS_ConfigRegionCode_DL1 code = AWS_ConfigRegionCode_DL1.JP;

            String geoLBRegionCode = SiteUtility.GeoLB_IPBasedGWRegion();
            switch (geoLBRegionCode.ToUpperInvariant())
            {
                case "EN-US":
                    if (ConvertUtility.ConvertToBoolean(Lib.AWS.AWSUtility_S3.ReadS3BucketConfig("DL1", "US", "Enabled"), false))
                        code = AWS_ConfigRegionCode_DL1.US;
                    break;
                case "EN-GB":
                case "RU-RU":
                    if (ConvertUtility.ConvertToBoolean(Lib.AWS.AWSUtility_S3.ReadS3BucketConfig("DL1", "EU", "Enabled"), false))
                        code = AWS_ConfigRegionCode_DL1.EU;
                    break;
                    //the rest will go to JP
            }
            return code;
        }

        /// <summary>
        /// These are for BlowFish encrypted files (controlled)
        /// These don't use CloudFront.  Direct signed url access to S3 bucket.
        /// HTTPS is not supported if we want to use our own domain name
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="awsPreFix"></param>
        /// <returns></returns>
        private String GenerateDL1URL(String filePath, String awsPreFix)
        {
            String fileRelUrl = filePath.Replace(@"\", "/").ToLowerInvariant();
            if (fileRelUrl.StartsWith("/")) fileRelUrl = fileRelUrl.Substring(1);
            //if (!awsPreFix.IsNullOrEmptyString()) fileRelUrl = awsPreFix + "/" + fileRelUrl;

            ////String fileNameInS3 = downloadURL.Replace(@"\", "/").ToLowerInvariant();
            //////test
            //////fileNameInS3 = "cma50/2008102318435500.zip";
            Lib.AWS.AWSUtility_S3 s3 = new Lib.AWS.AWSUtility_S3("DL1");
            String geoRegionCode = GetConfigRegionCode().ToString();
            return s3.GeneratePreSignedURL(fileRelUrl, awsPreFix, geoRegionCode);

            // String cloudFrontURL = String.Format("{0}/{1}", ConfigUtility.AppSettingGetValue("DL1.AWS.CloudFront.BaseUrl"), fileRelUrl).ToLowerInvariant();
            //String pathToPolicyStatement =  ConfigUtility.AppSettingGetValue("DL1.AWS.CloudFront.CannedPolicyPath");
            //String pathToPrivateKey =  ConfigUtility.AppSettingGetValue("DL1.AWS.CloudFront.PrivateKeyPath");
            //String privateKeyID =  ConfigUtility.AppSettingGetValue("DL1.AWS.CloudFront.PrivateKeyID");
            //return App_Lib.AWS.AWSCloudFrontUtil.GenerateCloudFrontPrivateURLCanned(cloudFrontURL, "hours", "1", pathToPolicyStatement, pathToPrivateKey, privateKeyID);
        }

        /// <summary>
        /// These are for non-encrypted downloads and version tables
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="awsPreFix"></param>
        /// <returns></returns>
        private String GenerateDL2URL(String filePath, String awsPreFix, Int32 urlToLastHour)
        {
            String fileRelUrl = filePath.Replace(@"\", "/").ToLowerInvariant();
            if (fileRelUrl.StartsWith("/")) fileRelUrl = fileRelUrl.Substring(1);
            if (!awsPreFix.IsNullOrEmptyString()) fileRelUrl = awsPreFix + fileRelUrl;

            String baseURL = Lib.AWS.AWSUtility_Settings.DL2_BaseUrlHost_Get();
            String cloudFrontURL = String.Format("{0}/{1}", baseURL, fileRelUrl).ToLowerInvariant();

            DateTime expireUTC = DateTime.UtcNow.AddHours(urlToLastHour);
            return Lib.AWS.AWSUtility_CloudFrontSignedURL.GetPreSignedURLWithPEMKey(cloudFrontURL,
                expireUTC, Lib.AWS.AWSUtility_Settings.DL2_PEM_Get(), Lib.AWS.AWSUtility_Settings.DL2_KeyPairID_Get(), false);
        }

        private void Process_DL1(Int32 cultureGroupId, Lib.Security.Sec_UserMembership usr)
        {
            // if (webToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");

            Downloads_PublicDataSource pds = new Downloads_PublicDataSource();
            List<Anritsu.Connect.Data.Downloads_PublicData> dlData = pds.SelectByModelNumber(ModelNumber, "all");
            if (dlData == null) throw new HttpException(404, "Page not found");

            var dl = from d in dlData
                     where d.DownloadID == ConvertUtility.ConvertToInt32(DownloadID, 0)
                     && d.ModelNumber.Equals(ModelNumber, StringComparison.InvariantCultureIgnoreCase)
                     select d;
            if (dl == null || dl.Count() < 1) throw new HttpException(404, "Page not found");

            Anritsu.Connect.Data.Downloads_PublicData dlItem = dl.First();
            if (dlItem == null) throw new HttpException(404, "Page not found");

            if (dlItem.FilePath.IsNullOrEmptyString()) throw new HttpException(404, "Page not found.");
            //Session[dwlSessionKey] = "downloaded";
            //if (downloadURL.Contains("://www.anritsu.com/")) downloadURL = UIHelper.MakeGwSsoUrlWithTargetUrl(downloadURL);
            WebUtility.HttpRedirect(null, dlItem.FilePath);
        }

        private void Process_DL2(Guid webToken, Int32 cultureGroupId, Guid accountID, Lib.Security.Sec_UserMembership usr)
        {
            // if (webToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");

            Downloads_PrivateDataSource pds = new Downloads_PrivateDataSource();
            List<Downloads_PrivateJapanDlData> dlData = pds.JapanNonUSB_SelectByWebToken(webToken);
            Process_DL2orDL3(dlData, cultureGroupId, accountID, usr);
        }

        private void Process_DL3(Guid webToken, Int32 cultureGroupId, Guid accountID, Lib.Security.Sec_UserMembership usr)
        {
            if (webToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");

            Lib.DongleDownload.DongleLic dongleLic = ProductReg_JPUSB_Utility.JapanUSBLicDataGet(true);
            if (dongleLic == null || dongleLic.DongleInfo == null || dongleLic.DongleInfo.Rows.Count < 1)
            {
                SiteUtility.SignOut();
                return;
            }
            String usbNo = Lib.DongleDownload.DongleLicBLL.GetValue(dongleLic.DongleInfo, "USBNo").ConvertNullToEmptyString().Trim();

            Downloads_PrivateDataSource pds = new Downloads_PrivateDataSource();
            List<Downloads_PrivateJapanDlData> dlData = pds.JapanUSB_SelectByWebToken(webToken, usbNo);
            Process_DL2orDL3(dlData, cultureGroupId, accountID, usr);
        }

        private void Process_DL2orDL3(List<Downloads_PrivateJapanDlData> dlData, Int32 cultureGroupId, Guid accountID, Lib.Security.Sec_UserMembership usr)
        {
            // if (webToken.IsNullOrEmptyGuid() || dlData == null) throw new HttpException(404, "Page not found");
            var dl = from d in dlData
                     where d.DownloadID == ConvertUtility.ConvertToInt32(DownloadID, 0)
                     && d.ModelName.Equals(ModelNumber, StringComparison.InvariantCultureIgnoreCase)
                     select d;
            if (dl == null || dl.Count() < 1) throw new HttpException(404, "Page not found");

            Downloads_PrivateJapanDlData dlItem = dl.First();
            if (dlItem == null) throw new HttpException(404, "Page not found");

            String downloadURL = dlItem.DownloadURL;
            String downloadTitle = dlItem.DownloadTitle;
            if (downloadURL.StartsWith("/")) downloadURL = downloadURL.Substring(1, downloadURL.Length - 1);
            downloadURL = downloadURL.Replace("/", @"\");

            String fileNameForUser = downloadTitle;
            if (String.IsNullOrEmpty(Path.GetExtension(fileNameForUser)))
            {
                fileNameForUser += Path.GetExtension(downloadURL.ConvertNullToEmptyString().Replace(".bf", String.Empty));
            }

            //Guid tokenID = Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.Insert(Lib.DownloadRoute.DownloadTypeCode.JPDL,
            //downloadURL,
            //fileNameForUser,
            //dlItem.CategoryName,
            //dlItem.ModelName,
            //usr.Email,
            //String.Empty, //change this after Shash finish UserID feature
            //Session.SessionID,
            //WebUtility.GetUserIP(),
            //accountID,
            //webToken,
            //dlItem.EncodeFlag ? 1 : 0);

            // if (!tokenID.IsNullOrEmptyGuid())
            // {
            if (dlItem.EncodeFlag)
            {
                String dl1FilePath = downloadURL;
                downloadURL = GenerateDL1URL(dl1FilePath, "dl/");
            }
            else
            {
                String dl2FilePath = downloadURL;
                String awsPrefix = Lib.AWS.AWSUtility_Settings.DL2_Prefix_Download_Get();
                downloadURL = GenerateDL2URL(dl2FilePath, awsPrefix, 36);
            }
            //}

            if (downloadURL.IsNullOrEmptyString()) throw new HttpException(404, "Page not found.");
            //Session[dwlSessionKey] = "downloaded";
            //if (downloadURL.Contains("://www.anritsu.com/")) downloadURL = UIHelper.MakeGwSsoUrlWithTargetUrl(downloadURL);
            WebUtility.HttpRedirect(null, downloadURL);
        }

        private void Process_JPVersionTable(Guid webToken, Int32 cultureGroupId, Guid accountID, Lib.Security.Sec_UserMembership usr)
        {
            if (webToken.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found");
            Downloads_PrivateDataSource pds = new Downloads_PrivateDataSource();
            DataTable tbVer = pds.JapanUSBandNonUSB_GetDownloadVersionTBL(webToken);
            if (tbVer == null || tbVer.Rows.Count < 1) throw new HttpException(404, "Page not found");

            DataRow[] selectedItem = tbVer.Select(String.Format("ModelNumber='{0}'", ModelNumber.Trim()));
            if (selectedItem == null || selectedItem.Length != 1) throw new HttpException(404, "Page not found");
            DataRow rverTBL = selectedItem[0];
            String versionTableFileName = rverTBL["VersionFileName"].ToString();
            String urlToDownload = String.Empty;
            Guid tokenID = Lib.DownloadRoute.GW_DownloadRoutingTokenBLL.Insert(Lib.DownloadRoute.DownloadTypeCode.JPVERTBL,
                versionTableFileName,
                versionTableFileName,
                "MatchingTable",
                ModelNumber.Trim(),
                usr.Email,
                String.Empty, //change this after Shash finish UserID feature
                Session.SessionID,
                WebUtility.GetUserIP(),
                accountID,
                webToken,
                0);

            if (!tokenID.IsNullOrEmptyGuid())
            {
                String verTblFilePath = versionTableFileName;
                String awsPrefix = Lib.AWS.AWSUtility_Settings.DL2_Prefix_JPVersionTable_Get();
                urlToDownload = GenerateDL2URL(versionTableFileName, awsPrefix, 24);
            }

            if (urlToDownload.IsNullOrEmptyString()) throw new HttpException(404, "Page not found.");
            WebUtility.HttpRedirect(null, urlToDownload.Trim());
        }

        private void Process_CalCertDownload(ProdReg_SessionData prodRegInfo, Lib.Security.Sec_UserMembership usr)
        {
            String calcertFileName = ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.CalCertFileName]);
            if (prodRegInfo == null || prodRegInfo.CalCerts == null
                || prodRegInfo.CalCerts.Count < 1
                || calcertFileName.IsNullOrEmptyString()
                || !calcertFileName.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new HttpException(404, "Page not found");
            }
            DataRow calCertData = Lib.Product.CalCertInfoBLL.GetCalCertInfoForDownload(usr.GWUserId,
                ModelNumber,
                SerialNumber);

            if (calCertData == null) throw new HttpException(404, "Page not found");

            byte[] mCertImage = (byte[])calCertData["Cal_Pdf"];
            long mFileSize = mCertImage.LongLength;
            string mFileName = calcertFileName;
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(mCertImage);
            Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}; size={1}", mFileName, mFileSize));
            Response.End();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            WebUtility.DisableDownloadCache();

            ProdReg_DataSource regData = new ProdReg_DataSource();

            //Lib.ProductRegistration.ProdReg_Master regInfo = regData.RegInfo_Select(AccountUtility.GenerateUserTeamIds(), ModelNumber,SerialNumber);
            //if (regInfo == null) throw new HttpException(404, "Page not found");
            //List<Lib.ProductRegistration.ProdReg_Item> configItems = regData.RegItems_Select(AccountUtility.GenerateUserTeamIds(), ModelNumber,SerialNumber);
            //if (configItems == null || configItems.Count < 1) throw new HttpException(404, "Page not found");

            int cultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            Guid accountID = AccountUtility.GetSelectedAccountID(true);
            Lib.Security.Sec_UserMembership usr = LoginUtility.GetCurrentUserOrSignIn();

            //String dwlSessionKey = ProdRegWebAccessKey.ToString().ToLower() + ModelNumber.ToLower() + DownloadID.ToLower() + DownloadSource.ToLower();
            switch (DownloadSource)
            {
                case "dl1":
                    Process_DL1(cultureGroupId, usr);
                    break;
                    //case "dl2":
                    //    Process_DL2(webToken, cultureGroupId, accountID, usr);
                    //    break;
                    //case "dl3":
                    //    Process_DL3(webToken, cultureGroupId, accountID, usr);
                    //    break;
                    
                    //case "dlv":
                    //    Process_JPVersionTable(webToken, cultureGroupId, accountID, usr);
                    //    break;
                    case "calcert":
                    ProdReg_SessionData prodRegInfo = regData.GetData(AccountUtility.GenerateUserTeamIds(),ModelNumber, SerialNumber);
                    Process_CalCertDownload(prodRegInfo, usr);
                    break;
            }
        }
    }
}