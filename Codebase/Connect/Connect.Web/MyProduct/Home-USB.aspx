﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home-USB.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.Home_USB" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsCtrl.ascx" TagName="RegisteredProductsCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsTabsCtrl.ascx" TagPrefix="uc2" TagName="RegisteredProductsTabsCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/app_js/dxgridfunctions.js" type="text/javascript" charset="UTF-8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div style="padding: 0px;">
    <uc2:RegisteredProductsTabsCtrl runat="server" id="RegisteredProductsTabsCtrl1" />
    <div class="cart-tabstrip-inner-wrapper" style="width:760px; min-height: 100px; border-top: none;">
        <uc1:RegisteredProductsCtrl ID="RegisteredProductsCtrl1" runat="server" DataFilterType="dongle" />
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
