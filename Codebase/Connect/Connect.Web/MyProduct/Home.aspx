﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.Home" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsCtrl.ascx" TagName="RegisteredProductsCtrl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    
    <style type="text/css">
        .cart-datagrid-row-selected .cart-datagrid-cell
        {
            background: #E1EBEC !important;
            border-bottom-color: #0FAF46 !important;
            font-weight: bold;
        }
        .cart-datagrid-row-hover .cart-datagrid-cell
        {
            border-bottom-color: #0FAF46;
            background: #E1EBEC !important;
        }
        .tbPrdSupt_MyPrds
        {
            border: solid 1px #C2D4DA;
            width: 740px;
        }
        .templateTable td.style1
        {
            border: solid 1px #C2D4DA;
            padding: 6px;
        }
        .templateTable td.value
        {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div style="padding: 0px;">
        <div class="cart-tabstrip-inner-wrapper" style="width:99%; min-height: 50px;  padding-left: 10px;">
            <uc2:RegisteredProductsCtrl ID="RegisteredProductsCtrl1" runat="server" DataFilterType="nondongle" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto({
                show_title: false,
                allow_resize: true,
                social_tools: false,
            });
        });
</script>
</asp:Content>
