﻿using System;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.MyProduct
{

    public partial class Home : BP_MyAnritsu
    {
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }



        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);


        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                WebUtility.DisableBrowserCache();
                LoginUtility.CheckTermsOfUseAgreement();

            }
        }

    }
}