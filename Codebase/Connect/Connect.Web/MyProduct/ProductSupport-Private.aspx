﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductSupport-Private.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.ProductSupport_Private" %>

<%@ Register Src="~/App_Controls/EndUser_ProductSupport/ProductInfoCtrl.ascx" TagName="ProductInfoCtrl" TagPrefix="uc1"  %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsNavTreeCtrl.ascx" TagName="RegisteredProductsNavTreeCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/ProductSupportTabCtrl.ascx" TagName="ProductSupportTabCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/PrivateResources_TabsCtrl.ascx" TagPrefix="uc4" TagName="PrivateResources_TabsCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/PrivateResources_DownloadsCtrl.ascx" TagPrefix="uc5" TagName="PrivateResources_DownloadsCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style type="text/css">
        .cart-datagrid-row-selected .cart-datagrid-cell {
            background: #E1EBEC!important;
            border-bottom-color: #0FAF46!important;
            font-weight: bold;
        }

        .cart-datagrid-row-hover .cart-datagrid-cell {
            border-bottom-color: #0FAF46;
            background: #E1EBEC!important;
        }
    </style>
    <script src="/app_js/dxgridfunctions.js" type="text/javascript" charset="UTF-8"></script>
    <script type="text/javascript">
        function closeWindow() {            
            document.getElementById('<%= btnPopUpClose.ClientID %>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
    <div class="settingrow" style="padding-bottom: 20px;">
        <uc2:RegisteredProductsNavTreeCtrl ID="RegisteredProductsNavTreeCtrl1" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <uc1:ProductInfoCtrl ID="ProductInfoCtrl1" runat="server"  />
    <div class="clearfloat"></div>
    <div class="margin-top-30">
        <uc3:ProductSupportTabCtrl ID="ProductSupportTabCtrl1" runat="server" />
        <div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 100px; border-top: none;">
            <div style="padding: 10px;">
                <uc4:PrivateResources_TabsCtrl runat="server" ID="PrivateResources_TabsCtrl1" />
                <div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 100px; background-color: #fff; border-top: none;">
                    <uc5:PrivateResources_DownloadsCtrl runat="server" ID="PrivateResources_DownloadsCtrl" />
                </div>
            </div>
        </div>
    </div>
    <div class="clearfloat"></div>
    <div class="clearfloat">

        <dx:ASPxPopupControl ID="pcAgreementNotice" runat="server" CloseAction="CloseButton" Modal="True" Theme="AnritsuDevXTheme" Width="750px" Height="450px" ShowLoadingPanel="true"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcAgreementNotice" ScrollBars="Auto" 
            AllowDragging="True" ClientSideEvents-Closing="closeWindow"  HeaderText='<%$ Resources:~/ProductSupport-Private,pcAgreementNotice.HeaderText %>' PopupAnimationType="None" RenderMode="Lightweight" EnableViewState="False" >
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btnAgree">
                        <PanelCollection>
                            <dx:PanelContent ID="pnlAgreementContent" runat="server">
                                <asp:Localize ID="lcalAgreementContent" runat="server"></asp:Localize>
                                <div class="center-buttons" style="text-align:center">
                                    <dx:ASPxButton ID="btnAgree" Theme="AnritsuDevXTheme" runat="server" Text='<%$ Resources:~/ProductSupport-Private,btnAgree.Text %>' OnClick="btnAgree_Click" SkinID="popup-button">
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnDisAgree" Theme="AnritsuDevXTheme" runat="server" Text='<%$ Resources:~/ProductSupport-Private,btnDisAgree.Text %>' AutoPostBack="False" OnClick="btnDisAgree_Click" SkinID="popup-button">
                                    </dx:ASPxButton>
                                    <span style="display:none">
                                        <asp:Button ID="btnPopUpClose" runat="server" Text="Button" onclick="btnPopUpClose_Click" />
                                    </span>
                                </div>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>

                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
