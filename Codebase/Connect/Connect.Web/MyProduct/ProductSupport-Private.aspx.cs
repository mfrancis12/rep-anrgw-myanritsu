﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.ActivityLog;
using Anritsu.Connect.Lib.Cfg;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.MyProduct
{
    public partial class ProductSupport_Private : App_Lib.UI.BP_ProductSupport
    {
        private bool ShowAgreement
        {
            get
            {
                if (Session != null && Session[KeyDef.SSKeys.PurchasedSupport_AgreementNotice] != null) return false;
                else return true;
            }
        }

        private String getCurrentUserEmail
        {
            get
            {
                return LoginUtility.GetCurrentUser(true).Email;
            }
        }

        public string ResClassKey
        {
            get
            {
                int notificationID = GetActiveNotification();
                if (notificationID == 0)
                    return null;
                return String.Format("NOTIFICATION_{0}", notificationID.ToString());
            }
        }

        public string ResKey
        {
            get
            {
                return "NotificationText";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitTabs(ProductSupportTabCtrl1.ProductSupportTabs);
            InitNotifyAssistant(ProductInfoCtrl1);
            if (!IsPostBack)
            {
                if (ShowAgreement)
                {
                    if (LoadAgreementContent())
                    {
                        pcAgreementNotice.ShowOnPageLoad = true;
                        PrivateResources_DownloadsCtrl.Visible = false;
                        String task = String.Format(ActivityLogNotes.EXPORTNOTICE_DISPLAYED);
                        //update the client log 
                        //Agreement notification is common for TAU and JP . Hence inserting JP as ModelConfigType.
                        Lib.ActivityLog.ActivityLogBLL.ClientLogInsert(getCurrentUserEmail, "", task, 0, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
                    }
                    return;
                }
                
            }
        }

        private bool LoadAgreementContent()
        {
            //load Agreement content
            if (ResClassKey == null)
                return false;
            else
            {
                String notificationContent = Convert.ToString(HttpContext.GetGlobalResourceObject(ResClassKey, ResKey));
                lcalAgreementContent.Text = (String.IsNullOrEmpty(notificationContent)) ? " " : notificationContent;
                return (!String.IsNullOrEmpty(notificationContent));
            }

        }
        protected void btnAgree_Click(object sender, EventArgs e)
        {
            pcAgreementNotice.ShowOnPageLoad = false;
            PrivateResources_DownloadsCtrl.Visible = true;
            Session[KeyDef.SSKeys.PurchasedSupport_AgreementNotice] = false;
            //update the client log.
            //Agreement notification is common for TAU and JP . Hence inserting JP as ModelConfigType.
            String task = String.Format(ActivityLogNotes.EXPORTNOTICE_ACK_SUCCESS);
            Lib.ActivityLog.ActivityLogBLL.ClientLogInsert(getCurrentUserEmail, "", task, 0, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
            InitNotifyAssistant(ProductInfoCtrl1);
        }
        protected void btnDisAgree_Click(object sender, EventArgs e)
        {
            pcAgreementNotice.ShowOnPageLoad = false;
            String task = String.Format(ActivityLogNotes.EXPORTNOTICE_ACK_FAILED);
            //Agreement notification is common for TAU and JP . Hence inserting JP as ModelConfigType.
            Lib.ActivityLog.ActivityLogBLL.ClientLogInsert(getCurrentUserEmail, "", task, 0, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.ProductSupportPublic + this.Request.Url.Query);
        }

        private int GetActiveNotification()
        {
            int? notificationID = Lib.Notification.NotificationBLL.SelectActiveOrDefaultNotification();
            if (notificationID == null || !notificationID.HasValue) return 0;
            else return notificationID.Value;
        }

        protected void btnPopUpClose_Click(object sender, EventArgs e)
        {
            pcAgreementNotice.ShowOnPageLoad = false;
            String task = String.Format(ActivityLogNotes.EXPORTNOTICE_ACK_FAILED);
            //Agreement notification is common for TAU and JP . Hence inserting JP as ModelConfigType.
            Lib.ActivityLog.ActivityLogBLL.ClientLogInsert(getCurrentUserEmail, "", task, 0, WebUtility.GetUserIP(), ModelConfigTypeInfo.CONFIGTYPE_JP);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.ProductSupportPublic + this.Request.Url.Query);
        }
    }
}