﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductSupport-PrivateJPUSB.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.ProductSupport_PrivateJPUSB" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductInfoCtrl.ascx" tagname="ProductInfoCtrl" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsNavTreeCtrl.ascx" tagname="RegisteredProductsNavTreeCtrl" tagprefix="uc2" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductSupportTabCtrl.ascx" tagname="ProductSupportTabCtrl" tagprefix="uc3" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/PrivateResources_TabsCtrl.ascx" TagPrefix="uc4" TagName="PrivateResources_TabsCtrl" %>
<%@ Register Src="~/App_Controls/EndUser_ProductSupport/PrivateResources_JPUSBCtrl.ascx" TagPrefix="uc5" TagName="PrivateResources_JPUSBCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
<style type="text/css">
.cart-datagrid-row-selected .cart-datagrid-cell {background:#E1EBEC!important; border-bottom-color:#0FAF46!important; font-weight: bold;}
.cart-datagrid-row-hover .cart-datagrid-cell {border-bottom-color:#0FAF46; background:#E1EBEC!important; }
</style>
    <script src="/app_js/dxgridfunctions.js" type="text/javascript" charset="UTF-8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
    <div class="settingrow" style="padding-bottom: 20px;"><uc2:RegisteredProductsNavTreeCtrl ID="MyProductsTreeCtrl1" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <uc1:ProductInfoCtrl ID="ProductInfoCtrl1" runat="server" />
<div class="clearfloat"></div>
    <div>
        <uc3:ProductSupportTabCtrl ID="ProductSupportTabCtrl1" runat="server"/>
        <div class="cart-tabstrip-inner-wrapper" style="width:758px; min-height:100px; border-top: none;">
            <div style="padding: 10px;">
                <uc4:PrivateResources_TabsCtrl runat="server" ID="PrivateResources_TabsCtrl1" />
                <div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px; background-color: #fff;">
                    <uc5:PrivateResources_JPUSBCtrl runat="server" id="PrivateResources_JPUSBCtrl1" />
                </div>
            </div>
        </div>
    </div>
    <div class="clearfloat"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
