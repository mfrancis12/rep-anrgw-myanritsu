﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.MyProduct
{
    public partial class ProductSupport_PrivateJPUSB : App_Lib.UI.BP_ProductSupport
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitTabs(ProductSupportTabCtrl1.ProductSupportTabs);

                // if (!HasNonUSBDocuments()) RedirectToDefaultTab();
                //ProductSupportTab tabType = GetSelectedTab(SelectedTab);
                //if (!tabType.Equals(ProductSupportTab.Documentation))
                //    RedirectToSelectedTab(tabType);
                //else
                //    InitTabs(supportTabs, ProductSupportTab.Documentation);

                // LoadGeneralInfo(ltrAddOnPageTitle);
            }
        }
    }
}