﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductSupport-Renew-Ack.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.ProductSupport_Renew_Ack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow" style="min-height: 400px;">
        <asp:Localize ID="lcalRenewSupportRequestedMsg" runat="server" Text="<%$ Resources:STCTRL_ProdReg_RenewSupportCtrl,lcalRenewSupportRequestedMsg.Text %>"></asp:Localize></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server"></asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server"></asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server"></asp:Content>
