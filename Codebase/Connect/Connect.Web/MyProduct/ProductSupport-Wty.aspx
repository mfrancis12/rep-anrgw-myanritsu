﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductSupport-Wty.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.ProductSupport_Wty" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductInfoCtrl.ascx" tagname="ProductInfoCtrl" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/RegisteredProductsNavTreeCtrl.ascx" tagname="RegisteredProductsNavTreeCtrl" tagprefix="uc2" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/WarrantyCtrl.ascx" tagname="WarrantyCtrl" tagprefix="uc3" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductSupportTabCtrl.ascx" tagname="ProductSupportTabCtrl" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
<link rel="stylesheet" href="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">
.cart-tabstrip-group { background-color: transparent;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
<asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
<div class="settingrow" style="padding-bottom: 20px;"><uc2:RegisteredProductsNavTreeCtrl ID="RegisteredProductsNavTreeCtrl1" runat="server" /></div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
 <uc1:ProductInfoCtrl ID="ProductInfoCtrl1" runat="server" />
<div class="clearfloat"></div>
    <div>
        <uc4:ProductSupportTabCtrl ID="ProductSupportTabCtrl1" runat="server"/>
        <div class="cart-tabstrip-inner-wrapper"  style="width:758px; min-height: 100px; border-top: none;">
            <uc3:WarrantyCtrl ID="WarrantyCtrl1" runat="server"/>
        </div>
        </div>
    <div class="clearfloat"></div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
     <script type="text/javascript" charset="utf-8">
         $(document).ready(function () {
             $("a[rel^='prettyPhoto']").prettyPhoto({
                 show_title: false,
                 allow_resize: true,
                 social_tools: false,
             });
         });
</script>
</asp:Content>
