﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.MyProduct {
    
    
    public partial class ProductSupport_Wty {
        
        /// <summary>
        /// ltrAddOnPageTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrAddOnPageTitle;
        
        /// <summary>
        /// RegisteredProductsNavTreeCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.RegisteredProductsNavTreeCtrl RegisteredProductsNavTreeCtrl1;
        
        /// <summary>
        /// ProductInfoCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductInfoCtrl ProductInfoCtrl1;
        
        /// <summary>
        /// ProductSupportTabCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductSupportTabCtrl ProductSupportTabCtrl1;
        
        /// <summary>
        /// WarrantyCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.WarrantyCtrl WarrantyCtrl1;
    }
}
