﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductSupport.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.ProductSupport" %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductInfoCtrl.ascx" tagname="ProductInfoCtrl" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/PublicResourcesCtrl.ascx" tagname="PublicResourcesCtrl" tagprefix="uc4" %>
<%@ Register src="~/App_Controls/EndUser_ProductSupport/ProductSupportTabCtrl.ascx" tagname="ProductSupportTabCtrl" tagprefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<link rel="stylesheet" href="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
<script src="<%= ConfigKeys.GwdataCdnPath %>/edgecast-legacy/anrhso/prettyphoto_3.1.5/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
   <%-- <div class="settingrow" style="padding-bottom: 20px;"><uc2:RegisteredProductsNavTreeCtrl ID="RegisteredProductsNavTreeCtrl1" runat="server" /></div>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
   <uc1:ProductInfoCtrl ID="ProductInfoCtrl1" runat="server" />
<div class="clearfloat"></div>
    <div>
        <uc5:ProductSupportTabCtrl ID="ProductSupportTabCtrl1" runat="server" />
        <div class="cart-tabstrip-inner-wrapper" style="color: #FFFFFF; width:100%; min-height: 100px; background:#f9f9f9">
            <uc4:PublicResourcesCtrl ID="PublicResourcesCtrl1" runat="server"/>
        </div>
   </div>
    <div class="clearfloat"></div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $("a[rel^='prettyPhoto']").prettyPhoto({
                show_title: false,
                allow_resize: true,
                social_tools: false,
            });
        });
</script>
</asp:Content>
