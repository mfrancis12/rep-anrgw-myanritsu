﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Controls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.MyProduct
{
    public partial class RegProduct_Other : App_Lib.UI.BP_MyAnritsu
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoginUtility.CheckTermsOfUseAgreement();
                //ProdReg_OtherCtrl1.LoadForm();
            }
        }
    }
}