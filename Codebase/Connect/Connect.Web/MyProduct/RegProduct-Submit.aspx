﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="RegProduct-Submit.aspx.cs" Inherits="Anritsu.Connect.Web.MyProduct.RegProduct_Submit" %>
<%@ Register src="~/App_Controls/EndUser_Registration/ProdReg_SubmittedCtrl.ascx" tagname="ProdReg_SubmittedCtrl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div style="min-height: 400px;">
    <uc1:ProdReg_SubmittedCtrl ID="ProdReg_SubmittedCtrl1" runat="server" />
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>