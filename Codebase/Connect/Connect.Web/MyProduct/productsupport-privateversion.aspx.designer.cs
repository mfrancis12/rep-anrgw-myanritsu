﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.MyProduct {
    
    
    public partial class ProductsupportPrivateVersion {
        
        /// <summary>
        /// MyProductsTreeCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.RegisteredProductsNavTreeCtrl MyProductsTreeCtrl1;
        
        /// <summary>
        /// ProductInfoCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductInfoCtrl ProductInfoCtrl1;
        
        /// <summary>
        /// ProductSupportTabCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.ProductSupportTabCtrl ProductSupportTabCtrl1;
        
        /// <summary>
        /// PrivateResources_TabsCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResources_TabsCtrl PrivateResources_TabsCtrl1;
        
        /// <summary>
        /// PrivateResources_VersionCtrl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.EndUser_ProductSupport.PrivateResourcesVersionCtrl PrivateResources_VersionCtrl;
    }
}
