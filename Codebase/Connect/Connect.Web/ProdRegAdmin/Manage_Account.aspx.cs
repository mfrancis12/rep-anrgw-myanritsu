﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class Manage_Account : App_Lib.UI.BP_ProdRegAdmin
    {
        protected override void OnInit(EventArgs e)
        {
            AccountAdmin_DetailCtrl1.AccountID = ProdRegMasterData.AccountID;
            //LoadGeneralInfo(ltrAddOnPageTitle, hlPrintable);
            LoadGeneralInfo(ltrAddOnPageTitle);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }
    }
}