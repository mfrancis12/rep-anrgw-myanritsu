﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Manage_Config_JPSW.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.Manage_Config_JPSW" %>
<%@ Register Src="~/App_Controls/Admin_ProdReg/ProdReg_Tabs.ascx" TagPrefix="uc1" TagName="ProdReg_Tabs" %>
<%@ Register src="~/App_Controls/Admin_ProdReg/ProdReg_Item_JPDLCtrl.ascx" tagname="ProdReg_Item_JPDLCtrl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server"><asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
   <%-- <li><asp:HyperLink ID="hlPrintable" runat="server" Target="_blank" Text="<%$ Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,hlPrintable.Text %>"></asp:HyperLink></li>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc1:ProdReg_Tabs runat="server" id="ProdReg_Tabs1" />
    <div id="tabContent" class="tabContent">
        <div class="settingrow" style="padding: 0 0 0 4px">
            <uc2:ProdReg_Item_JPDLCtrl ID="ProdReg_Item_JPDLCtrl1" runat="server" ModelConfigType="downloads-jp" />
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
    
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
