﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class Manage_Config_JPSW : App_Lib.UI.BP_ProdRegAdmin
    {
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPM)) throw new HttpException(404, "Page not found.");
            base.OnInit(e);
        }

       
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                
            }
        }
    }
}