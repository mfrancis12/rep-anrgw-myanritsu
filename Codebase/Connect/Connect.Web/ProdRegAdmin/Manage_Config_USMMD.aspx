﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Manage_Config_USMMD.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.Manage_Config_USMMD" %>
<%@ Register Src="~/App_Controls/Admin_ProdReg/ProdReg_Item_USMMDCtrl.ascx" TagPrefix="uc2" TagName="ProdReg_Item_USMMDCtrl" %>
<%@ Register Src="~/App_Controls/Admin_ProdReg/ProdReg_Tabs.ascx" TagPrefix="uc1" TagName="ProdReg_Tabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc1:ProdReg_Tabs runat="server" id="ProdReg_Tabs1" />
    <div id="tabContent" style="min-height: 300px; margin: 10px;color: Black;">
        <div class="settingrow">
            <uc2:ProdReg_Item_USMMDCtrl runat="server" id="ProdReg_Item_USMMDCtrl" ItemType="downloads-us" />
        </div>
    </div>
</div>    
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
