﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class Manage_Registration : App_Lib.UI.BP_ProdRegAdmin
    {
        public Guid ProdRegMaster_WebToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[App_Lib.KeyDef.QSKeys.RegisteredProductWebAccessKey], Guid.Empty);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //  if (!App_Lib.UIHelper.IsAdminRole(KeyDef.UserRoles.Admin_ProductReg_JPSW)) throw new HttpException(404, "Page not found.");
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }

            //if JP admin access this page,redirect.
            if (Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_JPM) ||
                Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_JPAN))
            {

                String url = String.Format("{0}?{1}={2}",
                                KeyDef.UrlList.SiteAdminPages.ProdRegAdminPages.Manage_Registration_JP,
                                KeyDef.QSKeys.RegisteredProductWebAccessKey,
                                ProdRegMaster_WebToken.ToString());
                Response.Redirect(url);
            }
        }
    }
}