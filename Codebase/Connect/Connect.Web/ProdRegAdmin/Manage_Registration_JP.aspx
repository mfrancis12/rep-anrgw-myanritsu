﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Manage_Registration_JP.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.Manage_Registration_JP" %>
<%@ Register Src="~/App_Controls/Admin_ProdReg/ProdReg_Tabs.ascx" TagPrefix="uc1" TagName="ProdReg_Tabs" %>
<%@ Register src="~/App_Controls/Admin_ProdReg/ProdReg_MasterCtrl_JP.ascx" tagname="ProdReg_MasterCtrl_JP" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server"><asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
   <%-- <li><asp:HyperLink ID="hlPrintable" runat="server" Target="_blank" Text="<%$ Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,hlPrintable.Text %>"></asp:HyperLink></li>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc1:ProdReg_Tabs runat="server" id="ProdReg_Tabs1" />
    <div id="tabContent" class="tabContent">   
    <div class="settingrow">
            <uc2:ProdReg_MasterCtrl_JP ID="ProdReg_MasterCtrl1" runat="server" />
        </div>
</div>
        </div>


</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
    
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
