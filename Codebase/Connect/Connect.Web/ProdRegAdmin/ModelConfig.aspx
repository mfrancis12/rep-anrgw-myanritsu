﻿<%@ Page Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ModelConfig.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.ModelConfig" %>

<%@ Register Src="~/App_Controls/Admin_ProdConfig/ModelConfig.ascx" TagPrefix="uc1" TagName="ModelConfig" %>
<%@ Register Src="~/App_Controls/Admin_ProdConfig/ClearCache_ModelConfigCtrl.ascx" TagPrefix="uc3" TagName="ClearCache_ModelConfigCtrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 100px;">
        <dx:ASPxTabControl ID="dxPconfigTabs" runat="server" Theme="AnritsuDevXTheme" Width="100%" TabAlign="Left" EnableTabScrolling="true"></dx:ASPxTabControl>
        <div id="tabContent" style="min-height: 300px; margin: 10px; color: Black;">
            <div class="settingrow"><uc1:ModelConfig runat="server" id="ModelConfig1" /></div>
            <div class="settingrow"><uc3:ClearCache_ModelConfigCtrl runat="server" ID="ClearCache_ModelConfigCtrl1" /></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>

