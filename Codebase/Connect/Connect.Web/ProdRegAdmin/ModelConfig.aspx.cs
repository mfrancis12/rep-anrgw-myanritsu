﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class ModelConfig : App_Lib.UI.ProdModelConfigBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsProdConfigAdminRole()) throw new HttpException(403, "Forbidden.");
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitConfigTabs(dxPconfigTabs);
                if ((SelectedTab != ProductModelConfigTab.Master))
                    RedirectToSelectedTab(SelectedTab);
            }

        }
    }
}