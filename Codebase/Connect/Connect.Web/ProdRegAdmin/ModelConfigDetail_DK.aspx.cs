﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class ModelConfigDetail_DK : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ProductConfig_DKM)) throw new HttpException(404, "Page not found.");
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}