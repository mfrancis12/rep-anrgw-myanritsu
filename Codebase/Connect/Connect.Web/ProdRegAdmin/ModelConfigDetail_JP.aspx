﻿<%@ Page Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ModelConfigDetail_JP.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.ModelConfigDetail_JP" %>

<%@ Register Src="~/App_Controls/Admin_ProdConfig/ModelConfigDetail_Item_JPCtrl.ascx" TagPrefix="uc1" TagName="ModelConfigDetail_Item_JPCtrl" %>
<%@ Register Src="~/App_Controls/Admin_ProdConfig/MasterModelDetailCtrl.ascx" TagPrefix="uc2" TagName="MasterModelDetailCtrl" %>
<%@ Register Src="~/App_Controls/Admin_ProdConfig/ClearCache_ModelConfigCtrl.ascx" TagPrefix="uc3" TagName="ClearCache_ModelConfigCtrl" %>
<%@ Register Src="~/App_Controls/Admin_ProdConfig/ModelConfigDetail_Item_Filter_JPCtrl.ascx" TagPrefix="uc4" TagName="ModelConfigDetail_Item_Filter_JPCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
   <%-- <li><asp:HyperLink ID="hlPrintable" runat="server" Target="_blank" Text="<%$ Resources:ADM_STCTRL_ProdRegAdmin_DetailCtrl,hlPrintable.Text %>"></asp:HyperLink></li>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
<uc2:MasterModelDetailCtrl runat="server" ID="MasterModelDetailCtrl1" />
<uc4:ModelConfigDetail_Item_Filter_JPCtrl runat="server" ID="ModelConfigDetail_Item_JPCtrl" />
 <uc1:ModelConfigDetail_Item_JPCtrl runat="server" ID="ModelConfigDetail_Item_JPCtrl1" />
<uc3:ClearCache_ModelConfigCtrl runat="server" ID="ClearCache_ModelConfigCtrl1" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
