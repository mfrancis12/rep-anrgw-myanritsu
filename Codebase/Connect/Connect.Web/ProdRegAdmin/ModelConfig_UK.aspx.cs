﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class ModelConfig_UK : App_Lib.UI.ProdModelConfigBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitConfigTabs(dxPconfigTabs);
            }
                     
        }
    }
}