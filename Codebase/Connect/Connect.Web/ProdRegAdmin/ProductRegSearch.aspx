﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ProductRegSearch.aspx.cs" Inherits="Anritsu.Connect.Web.ProdRegAdmin.ProductRegSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="//images-dev.cdn-anritsu.com/apps/connect/js/jquery.tokeninput-v3.js" ></script>
    <script src="//images-dev.cdn-anritsu.com/apps/connect/js/suggest_model-v1.js" ></script>--%>
    <style>
        .bttSearchContainer {
            margin-top:6px;
            float:right;
        }
        .batchProductRegistrationContainer {
            margin-left:45px;
            margin-bottom:-21px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <anrui:GlobalWebBoxedPanel ID="pnlSearchBy" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,pnlSearchBy.HeaderText %>" DefaultButton="bttSearch">
        <div class="width-60">
        <div class="settingrow group ">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByAccountID" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByAccountID.Text %>" TextAlign="Left" /></label>
            </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByAccountID" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByProdRegWAK" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByProdRegWAK.Text %>" TextAlign="Left" /></label>
         </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByProdRegWAK" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow group">
            <label  style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByMN" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByMN.Text %>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByMN" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchBySN" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchBySN.Text %>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchBySN" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow group input-select">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByStatus" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByStatus.Text %>" TextAlign="Left" /></label>
            <asp:DropDownList ID="cmbSearchByStatus" runat="server" DataTextField="StatusDisplayText" DataValueField="StatusCode" Width="100%"></asp:DropDownList>
        </div>
        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByEmail" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByEmail.Text %>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByEmail" runat="server"></asp:TextBox>
        </div>
        
        <%--<div class="settingrow">
    <span class="settinglabelrgt-15"><asp:CheckBox ID="cbxSearchByName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByName.Text %>" TextAlign="Left" /></span>
    <asp:TextBox ID="txtSearchByName" runat="server" SkinID="tbx-300"></asp:TextBox>
</div>--%>
        <div class="settingrow group">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByOrgName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByOrgName.Text %>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByOrgName" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow group ">
            <label style="line-height: 23px; margin-bottom: 5px">
                <asp:CheckBox ID="cbxSearchByCompanyName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,cbxSearchByCompanyName.Text %>" TextAlign="Left" /></label>
        </div>
        <div class="input-text">
            <asp:TextBox ID="txtSearchByCompanyName" runat="server"></asp:TextBox>
        </div>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">&nbsp;</span>
            <div class="bttSearchContainer">
                <asp:Button ID="bttSearch" runat="server"
                Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,bttSearch.Text %>"
                OnClick="bttSearch_Click" />
             </div>
            <%--<asp:HyperLink ID="hlAddNew" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminProdRegSearch,hlAddNew.Text %>' NavigateUrl="~/prodregadmin/productregnew" SkinID="GreenButton"></asp:HyperLink>--%>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">&nbsp;</span>
            <span class="msg">
                <asp:Literal ID="ltrMsg" runat="server"></asp:Literal></span>
        </div>
        <div class="settingrow">
            <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminProdRegSearch" EnableViewState="false" />
        </div>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlBatchApproval" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,pnlBatchApproval.Text %>" DefaultButton="btnSearch">
         <script type="text/javascript">
             var _selectNumber = 0;
             var _all = false;
             var _handle = true;
             function OnGridSelectionChanged(s, e) {
                 if (e.isChangedOnServer == false) {
                     if (e.isAllRecordsOnPage && e.isSelected)
                         _selectNumber = s.GetVisibleRowsOnPage();
                     else if (e.isAllRecordsOnPage && !e.isSelected)
                         _selectNumber = 0;
                     else if (!e.isAllRecordsOnPage && e.isSelected)
                         _selectNumber++;
                     else if (!e.isAllRecordsOnPage && !e.isSelected)
                         _selectNumber--;

                     if (_handle) {
                         SelectAllCheckBox.SetChecked(_selectNumber == s.GetVisibleRowsOnPage());
                         _handle = false;
                     }
                     _handle = true;
                 }
             }
             function OnGridEndCallback(s, e) {
                 _selectnumber = s.cpselectedrowsonpage;
             }
    </script>
        <div class="batchProductRegistrationContainer">
        <div class="settingrow group input-text width-60">
              <label>
            <asp:Localize ID="lcalCompany" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,lcalCompany.Text %>"></asp:Localize>* :</label>
            <asp:TextBox ID="txtCompany" runat="server" placeholder="<%$Resources:Common,CompPlaceHolder %>" ></asp:TextBox>
             <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="txtCompany" EnableClientScript="True"
            ValidationGroup="vgRegSrc" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>
         <div class="settingrow group input-text width-60">
              <label>
            <asp:Localize ID="lcalOrg" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,lcalOrg.Text %>"></asp:Localize>* :</label>
            <asp:TextBox ID="txtOrg" runat="server" placeholder="<%$Resources:Common,DeptPlaceHolder %>"></asp:TextBox>
               <asp:RequiredFieldValidator ID="rfvOrg" runat="server" ControlToValidate="txtOrg"
            ValidationGroup="vgRegSrc" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>
         <div class="settingrow group input-text width-60">
              <label>
            <asp:Localize ID="lcalEmail" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,lcalEmail.Text %>"></asp:Localize>* :</label>
            <asp:TextBox ID="txtEmail" runat="server" placeholder="<%$Resources:Common,EmailPlaceHolder %>"></asp:TextBox>
               <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
            ValidationGroup="vgRegSrc" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>
         <div class="settingrow group input-text width-60">
              <label>
            <asp:Localize ID="lcalModel" runat="server" Text= "<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalModelNumber.Text %>"></asp:Localize>* :</label>
            <asp:TextBox ID="txtModel" runat="server" placeholder="<%$Resources:Common,MdlPlaceHolder %>" ></asp:TextBox>
               <asp:RequiredFieldValidator ID="rfvModel" runat="server" ControlToValidate="txtModel"
            ValidationGroup="vgRegSrc" ErrorMessage="<%$Resources:Common,ERROR_Required%>"></asp:RequiredFieldValidator>
        </div>
        <div class="settingrow" style="text-align: right; margin-top: 15px;margin-right:0px;margin-bottom:13px;">
               <span class="msg" style="float:left" >
                    <asp:Literal ID="ltrConfigureMsg" runat="server"></asp:Literal>
                </span>
            <asp:Button ID="btnSearchProd" runat="server" Text="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,bttModelSearch.Text %>" ValidationGroup="vgSupType" CausesValidation="True"  OnClick="btnSearchProd_OnClick" />
            <asp:Button ID="btnReset" runat="server" Text="<%$Resources:ADM_STCTRL_SupportTypeAdmin,btnCancel.Text %>" CausesValidation="False" OnClick="btnReset_OnClick"/>
        </div>
        
        <div class="settingrow" id="SearchBox" runat="server" Visible="False">
        <fieldset id="fsConfigure" style="position: relative;">
            <legend>
                <asp:Localize ID="lcalConfigureLegend" runat="server" Text='<%$ Resources:ADM_STCTRL_ProdModelConfig_JPM,lcalConfigureLegend.Text %>'></asp:Localize></legend>
            <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                <tr>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnSelectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnSelectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e) { grid.SelectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td style="padding-right: 4px">
                        <dx:ASPxButton ID="btnUnselectAll" runat="server" Theme="AnritsuDevXTheme" Text="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,btnUnselectAll.Text%>" UseSubmitBehavior="False"
                            AutoPostBack="false">
                            <ClientSideEvents Click="function(s, e) { grid.UnselectRows(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>

            <dx:ASPxGridView ID="gvSearch" EnableViewState="False" runat="server" Theme="AnritsuDevXTheme" ClientInstanceName="grid" OnPageIndexChanged="gvSearch_OnPageIndexChanged" SettingsPager-PageSize="5" AutoGenerateColumns="False" KeyFieldName="Id" OnBeforePerformDataSelect="gvSearch_OnBeforePerformDataSelect">
                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" AllowSort="true"  />
                <Columns>
                    <dx:GridViewCommandColumn Name="Select" ShowSelectCheckbox="True" VisibleIndex="0">
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ToolTip="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,SelectAllCheckBox.Text%>" 
                                ClientSideEvents-CheckedChanged="function(s, e) { grid.SelectAllRowsOnPage(s.GetChecked()); }" ClientInstanceName="SelectAllCheckBox" OnInit="SelectAllCheckBox_Init" />
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="Id" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="Selected" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="ModelNumber" VisibleIndex="1" Caption="<%$Resources:ADM_STCTRL_ProdModelConfig_JPM,ModelNumber.Text%>" />
                    <dx:GridViewDataColumn FieldName="SerialNumber" SortOrder="Descending" VisibleIndex="2" Caption="<%$Resources:ADM_STCTRL_SupportTypeAdmin,lcalSerialNumber.Text%>" />
                 <dx:GridViewDataColumn FieldName="Email" VisibleIndex="3" Caption="<%$Resources:ADM_STCTRL_SupportTypeAdmin,ListEditItem.Email%>" ></dx:GridViewDataColumn>
                </Columns>
                 <SettingsPager Mode="ShowPager" PageSize="5" Position="Bottom" AlwaysShowPager="True">
        <PageSizeItemSettings Items="5,10,20,30,50" Caption="Page Size" ShowAllItem="True" ></PageSizeItemSettings>
    </SettingsPager>
                <Settings ShowFilterRow="True" />
                <ClientSideEvents SelectionChanged="OnGridSelectionChanged" EndCallback="OnGridEndCallback" />
            </dx:ASPxGridView>
             <div class="settingrow" style="text-align: right; margin-left: 130px;">
        <asp:Button ID="btnApprove" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminProdRegSearch,btnApprove.Text%>"  OnClick="btnApprove_OnClick" ValidationGroup="vgPackageInfo" />
    </div>
            
        </fieldset>
    </div>
        <div class="settingrow">
                
            </div>
            </div>
    </anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
