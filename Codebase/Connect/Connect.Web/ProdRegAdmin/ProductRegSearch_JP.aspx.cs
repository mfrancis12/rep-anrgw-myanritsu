﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.Connect.Lib;
using DevExpress.Web;

namespace Anritsu.Connect.Web.ProdRegAdmin
{
    public partial class ProductRegSearch_JP : App_Lib.UI.BP_ProdRegAdmin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gvSearch.DataBind();
            if (!IsPostBack)
            {
                BindProdRegStatus();
                InitProdRegSearchOptions();
            }
        }

        private void BindProdRegStatus()
        {
            cmbSearchByStatus.DataSource = Admin_ProductRegUtility.ProdReg_Item_Status_SelectAll(false);
            cmbSearchByStatus.DataBind();
        }

        private void InitProdRegSearchOptions()
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.AdminProdRegSearchOptionsGet(false);
            if (searchOptions == null || searchOptions.Count < 1) return;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "AccountID":
                        cbxSearchByAccountID.Checked = true;
                        txtSearchByAccountID.Text = ConvertUtility.ConvertEmptyGuidToEmptyString(sbo.SearchObject);
                        break;
                    case "WebToken":
                        cbxSearchByProdRegWAK.Checked = true;
                        txtSearchByProdRegWAK.Text = ConvertUtility.ConvertEmptyGuidToEmptyString(sbo.SearchObject);
                        break;
                    case "MN":
                        cbxSearchByMN.Checked = true;
                        txtSearchByMN.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "SN":
                        cbxSearchBySN.Checked = true;
                        txtSearchBySN.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "ItemStatusCode":
                        cbxSearchByStatus.Checked = true;
                        cmbSearchByStatus.SelectedValue = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "UserEmail":
                        cbxSearchByEmail.Checked = true;
                        txtSearchByEmail.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    //case "SubmittedUserName":
                    //    cbxSearchByName.Checked = true;
                    //    txtSearchByName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                    //    break;
                    case "OrgName":
                        cbxSearchByOrgName.Checked = true;
                        txtSearchByOrgName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        cbxSearchByCompanyName.Checked = true;
                        txtSearchByCompanyName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    //case "ItemType":
                    //    string itemType = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                    //    ListItem li = cmbItemType.Items.FindByValue(itemType);
                    //    if(li != null) li.Selected = true;

                    //    break;
                    case "StartDate" :
                        cbxSearchByDate.Checked = true;
                        txtSearchByStartDate.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "EndDate" :
                        cbxSearchByDate.Checked = true;
                        txtSearchByEndDate.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                }
            }
        }

        protected void bttSearch_Click(object sender, EventArgs e)
        {
            ltrMsg.Text = String.Empty;
            List<SearchByOption> searchOptions = Admin_SearchUtility.AdminProdRegSearchOptionsGet(true);

            Guid accountID = ConvertUtility.ConvertToGuid(txtSearchByAccountID.Text.Trim(), Guid.Empty);
            Guid prodRegWAK = ConvertUtility.ConvertToGuid(txtSearchByProdRegWAK.Text.Trim(), Guid.Empty);

            if (cbxSearchByAccountID.Checked) searchOptions.Add(new SearchByOption("AccountID", accountID));
            if (cbxSearchByProdRegWAK.Checked) searchOptions.Add(new SearchByOption("WebToken", prodRegWAK));
            if (cbxSearchByMN.Checked) searchOptions.Add(new SearchByOption("MN", txtSearchByMN.Text.Trim()));
            if (cbxSearchBySN.Checked) searchOptions.Add(new SearchByOption("SN", txtSearchBySN.Text.Trim()));
            if (cbxSearchByStatus.Checked) searchOptions.Add(new SearchByOption("ItemStatusCode", cmbSearchByStatus.SelectedValue));
            if (cbxSearchByEmail.Checked) searchOptions.Add(new SearchByOption("UserEmail", txtSearchByEmail.Text.Trim()));
            if (cbxSearchByOrgName.Checked) searchOptions.Add(new SearchByOption("OrgName", txtSearchByOrgName.Text.Trim()));
            if (cbxSearchByCompanyName.Checked) searchOptions.Add(new SearchByOption("CompanyName", txtSearchByCompanyName.Text.Trim()));
            
            //Add new search condition by the date.
            if (cbxSearchByDate.Checked)
            {
                var startDateStr = txtSearchByStartDate.Text.Trim();
                var endDateStr = txtSearchByEndDate.Text.Trim();

                //Verify DateTime Type.
                if (String.IsNullOrEmpty(startDateStr) || String.IsNullOrEmpty(endDateStr))
                {
                    ltrMsg.Text = GetStaticResource("MSG_DateTimeIsNull");
                    return;
                }

                DateTime startDate= new DateTime();
                DateTime endDate = new DateTime();
                if (DateTime.TryParse(startDateStr, out startDate) == false ||
                    DateTime.TryParse(endDateStr, out endDate) == false  )
                {
                    ltrMsg.Text = GetStaticResource("MSG_DateTimeInccorectFormat");
                    return;
                }

                //Verify Term.
                if(startDate > endDate)
                {
                    ltrMsg.Text = GetStaticResource("MSG_DateTimeInccorectTerm");
                    return;
                }

                // Add 1 day for SQL(startData <= db.date && endData+1 >db.date)
                DateTime endDateAdd1Day = endDate.AddDays(1);
                endDateStr = endDateAdd1Day.ToString("yyyy/MM/dd");

                searchOptions.Add(new SearchByOption("StartDate", startDateStr));
                searchOptions.Add(new SearchByOption("EndDate", endDateStr));
            }
            

            if (searchOptions.Count < 1)
            {
                ltrMsg.Text = GetStaticResource("MSG_SearchOptionRequired");
                return;
            }

            Admin_SearchUtility.AdminProdRegSearchOptionsSet(searchOptions);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.AdminProdRegSearchResult_JP);
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STPG_SiteAdminProdRegSearch_JP"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion

        protected void btnSearchProd_OnClick(object sender, EventArgs e)
        {
            LoadSearchList();
        }

        private void LoadSearchList()
        {
            var q = GetSearchList(txtCompany.Text, txtOrg.Text, txtModel.Text, txtEmail.Text);
            SearchBox.Visible = false;
            int i = 0;
            foreach (var l in q)
            {
                i++;
            }
            if (i > 0)
            {
                SearchBox.Visible = true;
                gvSearch.DataSource = q;
            }
        }

        private dynamic GetSearchList(string compName, string orgName, string model, string email)
        {
            var entities = new BLL_ProgReg();
            var searchOptions = new Dictionary<string, string>
            {
                {"Companyname", compName},
                {"ModelNumber", model},
                {"Orgname", orgName},
                {"EmailAddress", email}
            };
            return entities.GetSearchList(searchOptions);

        }

        private DataTable GetSupportEntriesTable(IEnumerable<object> list)
        {
            var dataTable = new DataTable();
            var enumerable = list as object[] ?? list.ToArray();
            if (!enumerable.Any()) return null;
            dataTable.Columns.Add("ProdRegId");
            foreach (var id in enumerable)
            {
                var dr = dataTable.NewRow();

                dr["ProdRegId"] = Convert.ToInt32(id);
                dataTable.Rows.Add(dr);
            }
            return dataTable;
        }
        protected void SelectAllCheckBox_Init(object sender, EventArgs e)
        {
            var chk = sender as ASPxCheckBox;
            Boolean cbChecked = true;
            //get the naming container
            if (chk != null)
            {
                var gridViewHeaderTemplateContainer = chk.NamingContainer as GridViewHeaderTemplateContainer;
                if (gridViewHeaderTemplateContainer != null)
                {
                    ASPxGridView grid = gridViewHeaderTemplateContainer.Grid;
                    Int32 start = grid.VisibleStartIndex;
                    Int32 end = grid.VisibleStartIndex + grid.SettingsPager.PageSize;
                    end = (end > grid.VisibleRowCount ? grid.VisibleRowCount : end);
                    for (int i = start; i < end; i++)
                    {
                        if (!grid.Selection.IsRowSelected(i))
                        {
                            cbChecked = false;
                            break;
                        }
                    }
                }
            }
            if (chk != null) chk.Checked = cbChecked;
        }

        protected void gvSearch_OnPageIndexChanged(object sender, EventArgs e)
        {
            var grid = sender as ASPxGridView;
            if (grid == null) return;
            var pageIndex = grid.PageIndex;
            gvSearch.PageIndex = pageIndex;
            LoadSearchList();
        }

        protected void btnApprove_OnClick(object sender, EventArgs e)
        {
            List<object> supportEntriesIdlist = gvSearch.GetSelectedFieldValues(new string[] { "Id" });
            var entities = GetSupportEntriesTable(supportEntriesIdlist);
            //send the entries table and package id to update the user access list
            ltrConfigureMsg.Text = "";
            var pkg = new BLL_ProgReg();
            int saved = Convert.ToInt32(pkg.BatchApprove(entities, "active", LoginUtility.GetCurrentUser(true).Email));
            if (saved > 0)
            {
                SearchBox.Visible = false;
                ltrConfigureMsg.Text = GetGlobalResourceObject("ADM_STPG_SiteAdminProdRegSearch", "ProdRegApproved").ToString();
            }
        }

        protected void gvSearch_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var q = GetSearchList(txtCompany.Text, txtOrg.Text, txtModel.Text, txtEmail.Text);
            gvSearch.DataSource = q;
        }

        protected void btnReset_OnClick(object sender, EventArgs e)
        {
            txtCompany.Text = txtOrg.Text = txtModel.Text = txtEmail.Text = string.Empty;
            SearchBox.Visible = false;
        }
    }
}