﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.ProdRegAdmin {
    
    
    public partial class ProductRegSearch_JP {
        
        /// <summary>
        /// pnlSearchBy コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.WebControls.GlobalWebBoxedPanel pnlSearchBy;
        
        /// <summary>
        /// cbxSearchByAccountID コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByAccountID;
        
        /// <summary>
        /// txtSearchByAccountID コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByAccountID;
        
        /// <summary>
        /// cbxSearchByProdRegWAK コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByProdRegWAK;
        
        /// <summary>
        /// txtSearchByProdRegWAK コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByProdRegWAK;
        
        /// <summary>
        /// cbxSearchByMN コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByMN;
        
        /// <summary>
        /// txtSearchByMN コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByMN;
        
        /// <summary>
        /// cbxSearchBySN コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchBySN;
        
        /// <summary>
        /// txtSearchBySN コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchBySN;
        
        /// <summary>
        /// cbxSearchByStatus コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByStatus;
        
        /// <summary>
        /// cmbSearchByStatus コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList cmbSearchByStatus;
        
        /// <summary>
        /// cbxSearchByEmail コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByEmail;
        
        /// <summary>
        /// txtSearchByEmail コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByEmail;
        
        /// <summary>
        /// cbxSearchByOrgName コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByOrgName;
        
        /// <summary>
        /// txtSearchByOrgName コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByOrgName;
        
        /// <summary>
        /// cbxSearchByCompanyName コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByCompanyName;
        
        /// <summary>
        /// txtSearchByCompanyName コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByCompanyName;
        
        /// <summary>
        /// cbxSearchByDate コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbxSearchByDate;
        
        /// <summary>
        /// txtSearchByStartDate コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByStartDate;
        
        /// <summary>
        /// lblBetween コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblBetween;
        
        /// <summary>
        /// txtSearchByEndDate コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSearchByEndDate;
        
        /// <summary>
        /// bttSearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button bttSearch;
        
        /// <summary>
        /// ltrMsg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrMsg;
        
        /// <summary>
        /// srep コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.WebControls.StaticResourceEditPanel srep;
        
        /// <summary>
        /// pnlBatchApproval コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.WebControls.GlobalWebBoxedPanel pnlBatchApproval;
        
        /// <summary>
        /// lcalCompany コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalCompany;
        
        /// <summary>
        /// txtCompany コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtCompany;
        
        /// <summary>
        /// rfvCompany コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvCompany;
        
        /// <summary>
        /// lcalOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalOrg;
        
        /// <summary>
        /// txtOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtOrg;
        
        /// <summary>
        /// rfvOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvOrg;
        
        /// <summary>
        /// lcalEmail コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalEmail;
        
        /// <summary>
        /// txtEmail コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtEmail;
        
        /// <summary>
        /// rfvEmail コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvEmail;
        
        /// <summary>
        /// lcalModel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalModel;
        
        /// <summary>
        /// txtModel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtModel;
        
        /// <summary>
        /// rfvModel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvModel;
        
        /// <summary>
        /// ltrConfigureMsg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrConfigureMsg;
        
        /// <summary>
        /// btnSearchProd コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSearchProd;
        
        /// <summary>
        /// btnReset コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnReset;
        
        /// <summary>
        /// SearchBox コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl SearchBox;
        
        /// <summary>
        /// lcalConfigureLegend コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalConfigureLegend;
        
        /// <summary>
        /// btnSelectAll コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnSelectAll;
        
        /// <summary>
        /// btnUnselectAll コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnUnselectAll;
        
        /// <summary>
        /// gvSearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView gvSearch;
        
        /// <summary>
        /// btnApprove コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnApprove;
    }
}
