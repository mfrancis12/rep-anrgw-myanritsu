﻿using System;
using System.Web;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.ProdRegAdmin.ProductSupport
{
    public partial class Jpsupporttypeentry : BP_MyAnritsu
    {

        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages)) throw new HttpException(403, "Forbidden");
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}