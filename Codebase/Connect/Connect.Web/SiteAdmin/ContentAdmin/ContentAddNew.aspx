﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ContentAddNew.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ContentAddNew" %>
<%@ Register src="~/App_Controls/AdminContent_Menu.ascx" tagname="AdminContent_Menu" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/AdminContent_AddNewCtrl.ascx" tagname="AdminContent_AddNewCtrl" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
<uc1:AdminContent_Menu ID="AdminContent_Menu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<uc2:AdminContent_AddNewCtrl ID="AdminContent_AddNewCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
