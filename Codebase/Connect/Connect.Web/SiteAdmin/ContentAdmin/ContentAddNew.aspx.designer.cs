﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin {
    
    
    public partial class ContentAddNew {
        
        /// <summary>
        /// AdminContent_Menu1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.AdminContent_Menu AdminContent_Menu1;
        
        /// <summary>
        /// AdminContent_AddNewCtrl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.AdminContent_AddNewCtrl AdminContent_AddNewCtrl1;
    }
}
