﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ContentDetails.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ContentDetails"
    ValidateRequest="false" %>

<%@ Register Src="~/App_Controls/AdminContent_Menu.ascx" TagName="AdminContent_Menu"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/App_ClientControls/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
    tinyMCE.init({
        theme: "advanced",
        mode: "textareas",
        convert_urls: false,
        forced_root_block: "",
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
    <uc1:AdminContent_Menu ID="AdminContent_Menu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <anrui:GlobalWebBoxedPanel ID="gwpnlContentLocale" runat="server" ShowHeader="true"
        HeaderText="Edit Translations">
        <div class="reports-links-wrapper" style="padding:10px; border: 1px solid #d4d4d4; border-radius: 6px;">
            <ul>
                
                <asp:Repeater runat="server" ID="rptLocales">
                    <ItemTemplate>
                        <li>
                            <asp:HyperLink ID="hlOtherLocales" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LocaleDisplayText") %>'
                                NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ContentID", "contentdetails?contentid={0}") %>'></asp:HyperLink></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <div class="settingrow width-60 input-select margin-top-15">
            <asp:DropDownList ID="cmbAddNewLang" runat="server" DataTextField="DisplayText" DataValueField="Locale">
            </asp:DropDownList>
        </div>
        <div class="settingrow margin-top-15">
            <asp:Button ID="bttAddNewLang" runat="server" Text="Add New"
                CausesValidation="false" OnClick="bttAddNewLang_Click" />
        </div>
    </anrui:GlobalWebBoxedPanel>
    <div class="settingrow">
        &nbsp;</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
    <div class="settingrow">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalContentClassKey" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalContentClassKey.Text %>'></asp:Localize></span>
        <asp:Literal ID="ltrContentClassKey" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalContentResKey" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalContentResKey.Text %>'></asp:Localize></span>
        <asp:Literal ID="ltrContentResKey" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalContentLocale" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalContentLocale.Text %>'></asp:Localize></span>
        <asp:Literal ID="ltrContentLocale" runat="server"></asp:Literal>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalContentAllowAutoTranslate" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalContentAllowAutoTranslate.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxContentAllowAutoTranslate" runat="server" Text=" " Enabled="false" />
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalContentStr" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalContentStr.Text %>'></asp:Localize></span>
    </div>
    <div class="settingrow">
        <textarea id="txtContent" runat="server" rows="30" style="width: 98%; height: 500px;"></textarea>
    </div>
    <div class="settingrow" id="divAutoTranslate" runat="server">
        <span class="settinglabelrgt-15">
            <asp:Localize ID="lcalAutoTranslate" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,lcalAutoTranslate.Text %>'></asp:Localize></span>
        <asp:CheckBox ID="cbxAutoTranslate" runat="server" Text=" " Enabled="false" />
    </div>
    <div class="settingrow margin-top-15">
        <asp:Button ID="bttSave" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,bttSave.Text %>'
            OnClick="bttSave_Click" />
        <asp:Button ID="bttDelete" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,bttDelete.Text %>'
            OnClick="bttDelete_Click" />
        <asp:Button ID="bttDeleteAllByClass" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminContentDetails,bttDeleteAllByClass.Text %>'
            OnClick="bttDeleteAllByClass_Click" />
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminContentDetails"
        EnableViewState="false" />
</asp:Content>
