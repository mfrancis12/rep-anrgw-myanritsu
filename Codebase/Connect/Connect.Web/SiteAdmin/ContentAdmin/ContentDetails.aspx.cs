﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin
{
    public partial class ContentDetails : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private Int32 ContentID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.AdminContent_ContentID], 0);
            }
        }

        private String ReturnURL
        {
            get
            {
                return Request.QueryString[KeyDef.QSKeys.ReturnURL];
            }
        }

        protected override void OnInit(EventArgs e)
        {
            LoadModules = false;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ContentID < 1) throw new HttpException(404, "Page not found.");
                InitContent();
            }
        }

        private void InitContent()
        {
            if (ContentID < 1) return;//new
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            if (content == null) throw new HttpException(404, "Page not found.");
            ltrContentClassKey.Text = content.ClassKey;
            ltrContentResKey.Text = content.ResourceKey;
            ltrContentLocale.Text = content.LocaleDisplayText;
            txtContent.Value = HttpUtility.HtmlDecode(content.ContentStr);
            //as per https://support.anritsu-globalweb.com/browse/MA-514
            cbxContentAllowAutoTranslate.Checked = false;//content.AllowAutoTranslate;
            Boolean isDefaultLocale = content.Locale == "en";
            divAutoTranslate.Visible = isDefaultLocale;
            bttDelete.Visible = !isDefaultLocale;

            DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(content.ClassKey, content.ResourceKey);
            rptLocales.DataSource = tb;
            rptLocales.DataBind();

            DataTable tbSupportedLocales = Lib.Content.Res_ContentStoreBLL.ContentSupportedLocale_SelectAll();

             var filtered = (from l in tbSupportedLocales.AsEnumerable()
                                            join n in tb.AsEnumerable() on l["Locale"].ToString() equals n["Locale"].ToString()
                                         into f
                                         where f.Count() == 0
                                         select l);

            DataTable filteredNewLocales = null;
            if (filtered != null && filtered.Count() > 0) filteredNewLocales = filtered.CopyToDataTable();
            if (filteredNewLocales == null || filteredNewLocales.Rows.Count < 1)
            {
                cmbAddNewLang.Visible = false;
            }
            else
            {
                DataView dv = filteredNewLocales.DefaultView;
                dv.Sort = "DisplayText ASC";
                cmbAddNewLang.DataSource = dv;
                cmbAddNewLang.DataBind();
            }
            
        }

        protected void bttAddNewLang_Click(object sender, EventArgs e)
        {
            String newlocale = cmbAddNewLang.SelectedValue;
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            if (content == null) WebUtility.HttpRedirect(null, Request.RawUrl);

            Int32 contentID = Lib.Content.Res_ContentStoreBLL.Insert(content.ClassKey, content.ResourceKey,newlocale, "-", false);
            if (contentID > 0)
            {
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.AdminContentDetails, KeyDef.QSKeys.AdminContent_ContentID, contentID));
            }
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            if (content == null) WebUtility.HttpRedirect(null, Request.RawUrl);

            String newContent = HttpUtility.HtmlEncode(txtContent.Value.Trim());
            
            Lib.Content.Res_ContentStoreBLL.Update(content.ContentID, newContent, cbxContentAllowAutoTranslate.Checked, false);
            content.ContentStr = newContent;
            App_Lib.Providers.ConnectResourceProvider.RemoveFromCache(content.ClassKey, content.ResourceKey, new CultureInfo(content.Locale));

            //Fix:  MA-1377 Remove Google translate api
            //Boolean isDefaultLocale = content.Locale == "en";
            //if (cbxAutoTranslate.Checked && isDefaultLocale)
            //{
            //    AutoTranslate(content);
            //}
            //else
            //{
            //    Lib.Content.Res_ContentStoreBLL.Update(content.ContentID, newContent, cbxContentAllowAutoTranslate.Checked);
            //    App_Lib.Providers.ConnectResourceProvider.RemoveFromCache(content.ClassKey, content.ResourceKey, new System.Globalization.CultureInfo(content.Locale));
            //}

            DoRedirect();

        }

        private void DoRedirect()
        {
            String nextUrl = ReturnURL.ConvertNullToEmptyString().Trim();
            if (nextUrl.IsNullOrEmptyString())
            {
                //Session[KeyDef.SSKeys.AdminContentSearchResults] = null;
                nextUrl = KeyDef.UrlList.SiteAdminPages.AdminContentSearchResult;
            }
            if (nextUrl != "0") WebUtility.HttpRedirect(null, nextUrl);
        }
        //Fix:  MA-1377 Remove Google translate api
        //private void AutoTranslate(Lib.Content.Res_ContentStore defaultEnglishContent)
        //{
        //    if (defaultEnglishContent == null) return;
        //    DataTable tb = Lib.Content.Res_ContentStoreBLL.ContentSupportedLocale_SelectAll();
        //    foreach (DataRow r in tb.Rows)
        //    {
        //        String lang = r["Lang"].ToString();
        //        CultureInfo targetLocale = new CultureInfo(r["Locale"].ToString());
        //        if (targetLocale.Name == "en") continue;


        //        Lib.Content.Res_ContentStore c = Lib.Content.Res_ContentStoreBLL.Select(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale, true);
        //        if (c == null)
        //        {
        //            if (lang != "en")
        //            {
        //                String translatedText = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.TranslateFromEnglish(lang, defaultEnglishContent.ContentStr);
        //                Lib.Content.Res_ContentStoreBLL.Insert(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale.Name, translatedText, false);
        //            }
        //        }
        //        else
        //        {
        //            if (lang == "en")
        //            {
        //                if(c.AllowAutoTranslate) Lib.Content.Res_ContentStoreBLL.DeleteByContentID(c.ContentID);
        //            }
        //            else
        //            {
        //                if (c.AllowAutoTranslate)
        //                {
        //                    String translatedText = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.TranslateFromEnglish(lang, defaultEnglishContent.ContentStr);
        //                    Lib.Content.Res_ContentStoreBLL.Update(c.ContentID, translatedText, c.AllowAutoTranslate, false);
        //                    App_Lib.Providers.ConnectResourceProvider.RemoveFromCache(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale);
        //                }
        //            }
        //        }
        //    }
        //}

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            if (content == null) WebUtility.HttpRedirect(null, Request.RawUrl);
            Lib.Content.Res_ContentStoreBLL.DeleteByContentID(content.ContentID);
            App_Lib.Providers.ConnectResourceProvider.RemoveFromCache(content.ClassKey, content.ResourceKey, new System.Globalization.CultureInfo(content.Locale));
            //Session[KeyDef.SSKeys.AdminContentSearchResults] = null;
            DoRedirect();
        }

        protected void bttDeleteAllByClass_Click(object sender, EventArgs e)
        {
            Lib.Content.Res_ContentStore content = Lib.Content.Res_ContentStoreBLL.SelectByContentID(ContentID);
            if (content == null) WebUtility.HttpRedirect(null, Request.RawUrl);
            Lib.Content.Res_ContentStoreBLL.DeleteByClassKeyResKey(content.ClassKey, content.ResourceKey);
            DoRedirect();
        }
    }
}