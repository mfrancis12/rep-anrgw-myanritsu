﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ContentHome.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ContentHome" %>
<%@ MasterType virtualpath="~/App_MasterPages/SiteDefault.Master" %>
<%@ Register Src="~/App_Controls/AdminContent_Menu.ascx" TagName="AdminContent_Menu"
    TagPrefix="uc1" %>
<%@ Register Src="~/App_Controls/AdminContent_AddNewCtrl.ascx" TagName="AdminContent_AddNewCtrl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
    <uc1:AdminContent_Menu ID="AdminContent_Menu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByClassKey" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,cbxSearchByClassKey.Text %>"
                TextAlign="Left" /></span>
    </div>
    <div class="dxflGroupCell_AnritsuDevXTheme input-text width-60">
        <asp:TextBox ID="txtSearchByClassKey" runat="server"></asp:TextBox>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByResKey" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,cbxSearchByResKey.Text %>"
                TextAlign="Left" /></span>
    </div>
    <div class="group input-text width-60">
        <asp:TextBox ID="txtSearchByResKey" runat="server"></asp:TextBox>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByLocale" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,cbxSearchByLocale.Text %>"
                TextAlign="Left" /></span>
    </div>
    <div class="group input-select width-60">
        <asp:DropDownList ID="cmbSearchByLocale" runat="server" DataTextField="DisplayText"
            DataValueField="Locale">
        </asp:DropDownList>
    </div>
    <div class="settingrow">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByContentKeyword" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,cbxSearchByContentKeyword.Text %>"
                TextAlign="Left" /></span>
    </div>
    <div class="width-60 group input-text">
        <asp:TextBox ID="txtSearchByContentKeyword" runat="server"></asp:TextBox>
    </div>
    <div class="settingrow margin-top-15">
        <asp:Button ID="bttSearchContent" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,bttSearchContent.Text %>"
            OnClick="bttSearchContent_Click" />
        <asp:Button ID="bttClearCache" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminContentSearch,bttClearCache.Text %>"
            OnClick="bttClearCache_Click" />
    </div>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminContentSearch"
            EnableViewState="false" />
    </div>
    <div class="settingrow" style="height: 50px;">
    </div>
    <div class="settingrow">
        <uc2:AdminContent_AddNewCtrl ID="AdminContent_AddNewCtrl1" runat="server" />
    </div>

    <fieldset>
        <legend>RESX Import/Export</legend>
        <div class="group">
            <label>FOR DEVELOPER ONLY</label>
        </div>
        <div class="group margin-top-15">
            <asp:HyperLink ID="hlResxImport" runat="server" Text="RESX Import" NavigateUrl="~/siteadmin/contentadmin/resximportexport/resximport" SkinID="button" Target="_blank"></asp:HyperLink>
            <span>&nbsp;</span>
        <asp:HyperLink ID="hlResxExport" runat="server" Text="RESX Export" NavigateUrl="~/siteadmin/contentadmin/resximportexport/resxexport" SkinID="button" Target="_blank"></asp:HyperLink>
        </div>
    </fieldset>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
