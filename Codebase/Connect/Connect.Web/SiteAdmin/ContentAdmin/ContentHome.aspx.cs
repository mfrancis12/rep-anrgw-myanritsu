﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.UI;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin
{
    public partial class ContentHome : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private Boolean AutoSearch
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[KeyDef.QSKeys.AdminContentSearch_Auto], false);
            }
        }

        private String PreSet_ClassKey
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.AdminContentSearch_PreClassKey]);
            }
        }

        private String PreSet_ResKey
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.AdminContentSearch_PreResKey]);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            LoadModules = false;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindContentLocale();
                InitSearchOptions();
            }
            //test
            //List<HyperLink> list = GwLeftMenuApi.AbstractMenu(true);
            //list.Add(
            //    new HyperLink()
            //    {
            //        ToolTip =
            //            Convert.ToString(GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlMyProducts.ToolTip")),
            //        NavigateUrl = "/myproduct/home",
            //        Text = Convert.ToString(GetGlobalResourceObject("STCTRL_LeftMainNavCtrl", "hlMyProducts.Text"))
            //    }
            //    );
            //Master.MyAnritsuSideMenu = GwLeftMenuApi.GenerateSideMenuHtml(list);
        }

        private void BindContentLocale()
        {
            cmbSearchByLocale.DataSource = Lib.Content.Res_ContentStoreBLL.SelectDistinctLocale();
            cmbSearchByLocale.DataBind();
        }

        private void InitSearchOptions()
        {
            List<SearchByOption> searchOptions = ContentSearchUtility.AdminContentSearchOptionsGet(false);
            if (searchOptions != null)
            {
                foreach (SearchByOption sbo in searchOptions)
                {
                    switch (sbo.SearchByField)
                    {
                        case "ClassKey":
                            cbxSearchByClassKey.Checked = true;
                            txtSearchByClassKey.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ResKey":
                            cbxSearchByResKey.Checked = true;
                            txtSearchByResKey.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "Locale":
                            cbxSearchByLocale.Checked = true;
                            cmbSearchByLocale.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ContentKeyword":
                            cbxSearchByContentKeyword.Checked = true;
                            txtSearchByContentKeyword.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                    }
                }
            }

            if (AutoSearch)
            {
                Boolean hasAtLeastOnePreset = false;
                if (!PreSet_ClassKey.IsNullOrEmptyString())
                {
                    cbxSearchByClassKey.Checked = true;
                    txtSearchByClassKey.Text = PreSet_ClassKey;
                    hasAtLeastOnePreset = true;
                }

                if (!PreSet_ResKey.IsNullOrEmptyString())
                {
                    cbxSearchByResKey.Checked = true;
                    txtSearchByResKey.Text = PreSet_ResKey;
                    hasAtLeastOnePreset = true;
                }

                if (hasAtLeastOnePreset) DoSearch();
            }
        }

        protected void bttSearchContent_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        private void DoSearch()
        {
            List<SearchByOption> existingSearchOptions = ContentSearchUtility.AdminContentSearchOptionsGet(false);
            List<SearchByOption> searchOptions = ContentSearchUtility.AdminContentSearchOptionsGet(true);
            if (cbxSearchByClassKey.Checked) searchOptions.Add(new SearchByOption("ClassKey", txtSearchByClassKey.Text.Trim()));
            if (cbxSearchByResKey.Checked) searchOptions.Add(new SearchByOption("ResKey", txtSearchByResKey.Text.Trim()));
            if (cbxSearchByLocale.Checked) searchOptions.Add(new SearchByOption("Locale", cmbSearchByLocale.SelectedValue));
            if (cbxSearchByContentKeyword.Checked) searchOptions.Add(new SearchByOption("ContentKeyword", txtSearchByContentKeyword.Text.Trim()));
            if (existingSearchOptions != searchOptions)
            {
                Session[KeyDef.SSKeys.AdminContentSearchResults] = null;
            }
            ContentSearchUtility.AdminContentSearchOptionsSet(searchOptions);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.AdminContentSearchResult);
        }

        protected void bttClearCache_Click(object sender, EventArgs e)
        {
            App_Lib.AppCacheFactories.ResourceCacheFactory resCache = new App_Lib.AppCacheFactories.ResourceCacheFactory();
            resCache.TouchCacheFile();

            App_Lib.AppCacheFactories.LinksHomeCacheFactory resLinkCache = new App_Lib.AppCacheFactories.LinksHomeCacheFactory();
            resLinkCache.TouchCacheFile();


        }
    }
}