﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ContentSearchResults.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ContentSearchResults" %>


<%@ Register Src="~/App_Controls/AdminContent_Menu.ascx" TagName="AdminContent_Menu"
    TagPrefix="uc1" %>
<%@ Register Src="~/App_Controls/AdminContent_AddNewCtrl.ascx" TagName="AdminContent_AddNewCtrl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
    <uc1:AdminContent_Menu ID="AdminContent_Menu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">

        <dx:ASPxGridView ID="gvContent" ClientInstanceName="gvContent" runat="server" OnBeforePerformDataSelect="gvContent_OnBeforePerformDataSelect" Theme="AnritsuDevXTheme"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="ClassKey" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                
                <dx:GridViewDataTextColumn FieldName="ClassKey" VisibleIndex="2" GroupIndex="1" Caption="Class Key"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ResourceKey" VisibleIndex="3" GroupIndex="2" Caption="Resource Key"></dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn  Caption=" " VisibleIndex="1">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Details" NavigateUrl='<%#"contentdetails?contentid="+Eval("ContentID") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn FieldName="ClassKey" VisibleIndex="4" Caption="Class Key"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Locale" Caption="Locale" VisibleIndex="4"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AllowAutoTranslate" VisibleIndex="5" Caption="Translate"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ContentSnippet" VisibleIndex="6" Caption="Content Snippet"></dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="ModifiedOnUTC" VisibleIndex="7" Caption="Modified Date">
                    <PropertiesDateEdit DisplayFormatString="d">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn FieldName="CreatedOnUTC" VisibleIndex="8" Caption="Created Date">
                    <PropertiesDateEdit DisplayFormatString="d">
                    </PropertiesDateEdit>
                </dx:GridViewDataDateColumn>

            </Columns>
            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>
           </dx:ASPxGridView>

    </div>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminContentSearchResults"
            EnableViewState="false" />
    </div>
    <div class="settingrow" style="height: 50px;">
    </div>
    <div class="settingrow">
        <uc2:AdminContent_AddNewCtrl ID="AdminContent_AddNewCtrl1" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
