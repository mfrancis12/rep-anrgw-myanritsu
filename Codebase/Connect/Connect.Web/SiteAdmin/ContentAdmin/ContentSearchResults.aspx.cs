﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Lib.Content;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin
{
    public partial class ContentSearchResults : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private DataTable SearchResults
        {
            get
            {
                DataTable tb = Session[KeyDef.SSKeys.AdminContentSearchResults] as DataTable;
                if (tb == null || tb.Rows.Count < 1)
                {
                    tb = Lib.Content.Res_ContentStoreBLL.SelectFiltered(ContentSearchUtility.AdminContentSearchOptionsGet(false));
                    if (tb != null) Session[KeyDef.SSKeys.AdminContentSearchResults] = tb;
                }
                return tb;
            }
        }



        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                gvContent.DataBind();

            }
        }



       

        protected void gvContent_OnDetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvResDetails = gvContent.FindDetailRowTemplateControl(e.VisibleIndex, "gvResDetails") as ASPxGridView;
                if (gvResDetails != null)
                {
                    gvResDetails.DataBind();
                }
            }
        }

        protected void gvContent_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var srhResults= SearchResults;//.AsEnumerable().GroupBy(i => i["ClassKey"]).Select(g=>new{ClassKey=g.Key}).ToList();
            gvContent.DataSource = srhResults;
        }

        protected void gvContent_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            Int32 contentID = ConvertUtility.ConvertToInt32(e.Keys["ContentID"], 0);
            Lib.Content.Res_ContentStoreBLL.DeleteByContentID(contentID);
            gvContent.DataBind();
        }

        protected void gvResDetails_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            //Rebind the details grid
            detailGrid.DataBind();
        }

        protected void gvResDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var details = Lib.Content.Res_ContentStoreBLL.SelectResourceKey((string)detailGrid.GetMasterRowKeyValue());
            detailGrid.DataSource = details;
        }

        protected void gvDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            //var detailGrid = (ASPxGridView)sender;
            //var gvUserDetails = gvContent.FindDetailRowTemplateControl(e., "gvUserDetails") as ASPxGridView;
            ////Rebind the details grid
            // var details = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey((string)detailGrid.GetMasterRowKeyValue());
          //  detailGrid.DataSource= 
        }

        protected void gvDetails_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            //Rebind the details grid
            detailGrid.DataBind();
        }

        protected void gvResDetails_OnDetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
           

            if (e.Expanded)
            {
                var gvdetails = ((ASPxGridView)sender).FindDetailRowTemplateControl(e.VisibleIndex, "gvDetails") as ASPxGridView;
                if (gvdetails != null)
                {
                    gvdetails.DataBind();
                }
            }
        }
    }
}