﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ContentTranslateAllUnlocked.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ContentTranslateAllUnlocked" %>
<%@ Register src="~/App_Controls/AdminContent_Menu.ascx" tagname="AdminContent_Menu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
<uc1:AdminContent_Menu ID="AdminContent_Menu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="settingrow">
<span class="settinglabel-10">Enter passcode:</span>
<asp:TextBox ID="txtPassCode" runat="server" TextMode="Password" Width="250px"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-10">From ContentID:</span>
    <asp:TextBox ID="txtStartContentID" runat="server" Text="0"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-10">To ContentID:</span>
    <asp:TextBox ID="txtEndContentID" runat="server" Text="300"></asp:TextBox>
</div>
<div class="settingrow">
    <asp:Button ID="bttTranslateAll" runat="server" Text="Auto Translate All" onclick="bttTranslateAll_Click" />
    <p class="msg"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></p>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
