﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Text;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin
{
    public partial class ContentTranslateAllUnlocked : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected override void OnInit(EventArgs e)
        {
            LoadModules = false;
            base.OnInit(e);

            String processingText = this.GetGlobalResourceObject("common", "processing").ToString();
            WebUtility.GenerateProcessingScript(bttTranslateAll, "onclick", processingText);
        }

        private Int32 InvalidPassCodeCount
        {
            get
            {
                return ConvertUtility.ConvertToInt32(ViewState["vsInvalidCount"], 0);
            }
            set
            {
                ViewState["vsInvalidCount"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bttTranslateAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassCode.Text != ConfigUtility.AppSettingGetValue("Connect.Web.TranslateAllPassCode"))
                {
                    InvalidPassCodeCount++;
                    return;
                }
                if (InvalidPassCodeCount > 3)
                {
                    //email to admin about the user here when u have time
                    throw new HttpException(403, "Forbidden");
                }

                Int32 startContentID = ConvertUtility.ConvertToInt32(txtStartContentID.Text.Trim(), -1);
                Int32 endContentID = ConvertUtility.ConvertToInt32(txtEndContentID.Text.Trim(), -1);
                if (startContentID < 0) throw new ArgumentException("Invalid FROM contentID");
                if (endContentID < 0) throw new ArgumentException("Invalid TO contentID");

                DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectForGoogleTranslateAPI(startContentID, endContentID);
                if (tb == null || tb.Rows.Count < 1) throw new ArgumentException("No content found.");

                DataTable tbSupportedLocales = Lib.Content.Res_ContentStoreBLL.ContentSupportedLocale_SelectAll();
                Int32 translatedCount = 0;
                StringBuilder sb = new StringBuilder();
                StringBuilder sbErr = new StringBuilder();
                foreach (DataRow r in tb.Rows)
                {
                    Int32 contentID = ConvertUtility.ConvertToInt32(r["ContentID"], 0);

                    if (contentID < 1) continue;
                    Lib.Content.Res_ContentStore c = Lib.Content.Res_ContentStoreBLL.SelectByContentID(contentID);
                    if (c == null) continue;
                    if (c.ContentStr.ConvertNullToEmptyString().Length > 5000)
                    {
                        sb.AppendFormat("<li>{0}({1}): {2}-->{3}</li>", c.ContentID, c.Locale, c.ClassKey, c.ResourceKey);
                        continue;
                    }

                    try
                    {
                        //Fix:  MA-1377 Remove Google translate api
                        //AutoTranslate(c, tbSupportedLocales);
                        translatedCount++;
                    }
                    catch (Exception ex)
                    {
                        sbErr.AppendFormat("<li>{0}({1}): {2}-->{3} ==>" + GetGlobalResourceObject("Common", "ErrorString").ToString() + "{4}</li>", c.ContentID, c.Locale, c.ClassKey, c.ResourceKey, ex.Message);
                    }

                }

                ltrMsg.Text = String.Format("{0} of {1} translated.<div><h3>skipped-</h3><br/><ul>{2}</ul></div>"
                    , translatedCount, tb.Rows.Count, sb.ToString());

                ltrMsg.Text += String.Format("<div><h3>Errors-</h3><ul>{0}</ul></div>", sbErr.ToString());
                //bttTranslateAll.Visible = false;
            }
            catch (Exception ex)
            {
                ltrMsg.Text = GetGlobalResourceObject("Common", "ErrorString").ToString() + ex.Message;
            }
        }

        //Fix:  MA-1377 Remove Google translate api
        //private void AutoTranslate(Lib.Content.Res_ContentStore defaultEnglishContent, DataTable supportedLocales)
        //{
        //    if (defaultEnglishContent == null || supportedLocales == null) return;

        //    foreach (DataRow r in supportedLocales.Rows)
        //    {
        //        String lang = r["Lang"].ToString();
        //        CultureInfo targetLocale = new CultureInfo(r["Locale"].ToString());
        //        if (targetLocale.Name == "en") continue;

        //        Lib.Content.Res_ContentStore c = Lib.Content.Res_ContentStoreBLL.Select(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale, true);
        //        if (c == null)
        //        {
        //            if (lang != "en")
        //            {
        //                String translatedText = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.TranslateFromEnglish(lang, defaultEnglishContent.ContentStr);
        //                Lib.Content.Res_ContentStoreBLL.Insert(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale.Name, translatedText, false);
        //            }
        //        }
        //        else
        //        {
        //            if (lang == "en")
        //            {
        //                if (c.AllowAutoTranslate) Lib.Content.Res_ContentStoreBLL.DeleteByContentID(c.ContentID);
        //            }
        //            else
        //            {
        //                if (c.AllowAutoTranslate)
        //                {
        //                    String translatedText = App_Lib.GoogleAPIs.GoogleTranslateAPI.GoogleTranslateUtil.TranslateFromEnglish(lang, defaultEnglishContent.ContentStr);
        //                    Lib.Content.Res_ContentStoreBLL.Update(c.ContentID, translatedText, c.AllowAutoTranslate, false);
        //                    App_Lib.Providers.ConnectResourceProvider.RemoveFromCache(defaultEnglishContent.ClassKey, defaultEnglishContent.ResourceKey, targetLocale);
        //                }
        //            }
        //        }
        //    }
        //}


    }
}