﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Resources;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ResxImportExport
{
    public partial class ResxExport : System.Web.UI.Page
    {
        private int scriptTimeout;
        private DateTime startTime;
        private const String UploadBaseDir = "~/App_Data/ResxTemp/Export/";
        
        private void WriteHeader()
        {
            if (HttpContext.Current == null) return;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            Response.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\" >");
            Response.Write("<head><title>Connect RESX Export</title></head><body>");
            Response.Write("<h1>Export RESX files</h1>");
            Response.Write(String.Format("<h5>Start Time: {0}</h5>", startTime.ToString("MMM-dd-yyyy hh:mm:ss tt")));
            Response.Flush();
        }

        private void WritePageFooter()
        {
            Response.Write("</body>");
            Response.Write("</html>");
            Response.Flush();
        }

        private void WritePageContent(string message)
        {
            WritePageContent(message, false);
        }

        private void WritePageContent(string message, bool showTime)
        {

            if (showTime)
            {
                TimeSpan duration = DateTime.UtcNow.Subtract(startTime);
                HttpContext.Current.Response.Write(
                    string.Format("{0} - {1}ms",
                    message,
                    duration.TotalMilliseconds));
            }
            else
            {
                HttpContext.Current.Response.Write(message);
            }
            HttpContext.Current.Response.Write("<br/>");
            HttpContext.Current.Response.Flush();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            scriptTimeout = Server.ScriptTimeout;
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Server.ScriptTimeout = int.MaxValue;
            startTime = DateTime.UtcNow;
            
            WriteHeader();

            String folderPath = String.Format("{0:MMM-dd-yyyy hh_mm_ss tt}", DateTime.UtcNow);

            String dirPath = Server.MapPath(Path.Combine(UploadBaseDir, folderPath));
            DirectoryInfo dir = new DirectoryInfo(dirPath);
            if (!dir.Exists) dir.Create();
            WritePageContent(String.Format("<p>Files will be in subdirectory: {0}</p>", dir.Name));

            DataTable tbClassKeyLocale = Lib.Content.Res_ContentStoreBLL.SelectClassKeyLocale();
            if (tbClassKeyLocale == null)
            {
                WritePageContent("<p>There are no resource strings to export.</p>");
            }
            else
            {
                WritePageContent("<ul>");
                foreach (DataRow dataClassKeyLocale in tbClassKeyLocale.Rows)
                {
                    String oriClassKey = ConvertUtility.ConvertNullToEmptyString(dataClassKeyLocale["ClassKey"]);
                    String classKey = ConvertUtility.ConvertNullToEmptyString(dataClassKeyLocale["ClassKey"]);
                    String locale = ConvertUtility.ConvertNullToEmptyString(dataClassKeyLocale["Locale"]);
                    String resxFileName = ConvertUtility.ConvertNullToEmptyString(dataClassKeyLocale["ResxFileName"]);
                    if (classKey.Contains("/")) classKey = classKey.Replace("/", "~slash~");
                    if (classKey.Contains(@"\")) classKey = classKey.Replace(@"\", "~backslash~");
                    if (classKey.Contains(":")) classKey = classKey.Replace(":", "~column~");
                    if (classKey.Contains("*")) classKey = classKey.Replace(":", "~star~");
                    if (classKey.Contains("|")) classKey = classKey.Replace("|", "~pipe~");
                    if (classKey.Contains("?")) classKey = classKey.Replace("?", "~question~");
                    if (classKey.Contains("\"")) classKey = classKey.Replace("\"", "~dquote~");
                    if (classKey.Contains("<")) classKey = classKey.Replace("<", "~less~");
                    if (classKey.Contains(">")) classKey = classKey.Replace("<", "~greater~");

                    String fileName = String.Format("{0}.{1}.resx", classKey, locale);
                    if (locale == "en") fileName = String.Format("{0}.resx", classKey);
                    
                    WritePageContent(String.Format("<li>FileName: {0}</li>", fileName));
                    
                    String fullFileName = Path.Combine(dir.FullName, fileName);
                    using (ResXResourceWriter writer = new ResXResourceWriter(fullFileName))
                    {
                        DataTable tbData = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyLocale(oriClassKey, locale);
                        if(tbData != null)
                        {
                            
                            Int32 resourceCount = 0;
                            foreach(DataRow resData in tbData.Rows)
                            {
                                String resKey = ConvertUtility.ConvertNullToEmptyString(resData["ResourceKey"]);
                                String contentStr = ConvertUtility.ConvertNullToEmptyString(resData["ContentStr"]);
                                //ResXDataNode node = new ResXDataNode(resKey, contentStr);
                                writer.AddResource(resKey, contentStr);
                                resourceCount++;
                                //WritePageContent(String.Format("<li>{0}/{1}</li>", resKey, contentStr));
                            }
                            WritePageContent(String.Format("<li>Resources: {0}</li>", resourceCount));
                        }
                        writer.Close();
                    }
                    WritePageContent("<br/>");
                }
                WritePageContent("</ul>");
            }
            WritePageContent("<h3>Completed.  Please download files from the server.</h3>");
            WritePageFooter();
        }
    }
}