﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Resources;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.Connect.Web.SiteAdmin.ContentAdmin.ResxImportExport
{
    public partial class ResxImport : System.Web.UI.Page
    {
        private int scriptTimeout;
        private DateTime startTime;
        private const String UploadBaseDir = "~/App_Data/ResxTemp/Import";

        private void WriteHeader()
        {
            if (HttpContext.Current == null) return;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            Response.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\" >");
            Response.Write("<head><title>Connect RESX Import</title></head><body>");
            Response.Write("<h1>Import RESX files</h1>");
            Response.Write(String.Format("<h5>Start Time: {0}</h5>", startTime.ToString("MMM-dd-yyyy hh:mm:ss tt")));
            Response.Write("<p>Import resx files to the resx temporary import folder and hit this page again.  All resx files will be processed.</p>");
            Response.Flush();
        }

        private void WritePageFooter()
        {
            Response.Write("</body>");
            Response.Write("</html>");
            Response.Flush();
        }

        private void WritePageContent(string message)
        {
            WritePageContent(message, false);
        }

        private void WritePageContent(string message, bool showTime)
        {

            if (showTime)
            {
                TimeSpan duration = DateTime.UtcNow.Subtract(startTime);
                HttpContext.Current.Response.Write(
                    string.Format("{0} - {1}ms",
                    message,
                    duration.TotalMilliseconds));
            }
            else
            {
                HttpContext.Current.Response.Write(message);
            }
            HttpContext.Current.Response.Write("<br/>");
            HttpContext.Current.Response.Flush();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            scriptTimeout = Server.ScriptTimeout;
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            Server.ScriptTimeout = int.MaxValue;
            startTime = DateTime.UtcNow;

            WriteHeader();

            DirectoryInfo resxDir = new DirectoryInfo(Server.MapPath(UploadBaseDir));
            if (!resxDir.Exists)
            {
                WritePageContent("Invalid Directory.");
                return;
            }
            FileInfo[] resxFiles = resxDir.GetFiles("*.resx", SearchOption.AllDirectories);
            if (resxFiles == null || resxFiles.Length < 1)
            {
                WritePageContent("No files to process.");
                return;
            }

            Response.Flush();
            foreach (FileInfo fi in resxFiles)
            {
                WritePageContent("<ul>");
                WritePageContent(String.Format("<li>Processing file <strong>{0}</strong></li>", fi.Name));
                String classKey = Path.GetFileNameWithoutExtension(fi.Name);
                String locale = "en";
                if (!Path.GetExtension(classKey).IsNullOrEmptyString())
                {
                    try
                    {
                        CultureInfo ci = new CultureInfo(Path.GetExtension(classKey).Replace(".", ""));
                        if (ci != null && !ci.Name.IsNullOrEmptyString())
                        {
                            locale = ci.Name;
                            switch (locale.ToLowerInvariant())
                            {
                                case "zh-hans":
                                    locale = "zh-CN";
                                    break;
                                case "zh-hant":
                                    locale = "zh-TW";
                                    break;
                            }
                            classKey = Path.GetFileNameWithoutExtension(classKey);
                        }
                    }
                    catch { }

                }

                if (classKey.Contains("~slash~")) classKey = classKey.Replace("~slash~", "/");
                if (classKey.Contains("~backslash~")) classKey = classKey.Replace("~backslash~", @"\");
                if (classKey.Contains("~column~")) classKey = classKey.Replace("~column~", ":");
                if (classKey.Contains("~star~")) classKey = classKey.Replace("~star~", ":");
                if (classKey.Contains("~pipe~")) classKey = classKey.Replace("~pipe~", "|");
                if (classKey.Contains("~question~")) classKey = classKey.Replace("~question~", "?");
                if (classKey.Contains("~dquote~")) classKey = classKey.Replace("~dquote~", "\"");
                if (classKey.Contains("~less~")) classKey = classKey.Replace("~less~", "<");
                if (classKey.Contains("~greater~")) classKey = classKey.Replace("~greater~", "<");


                WritePageContent(String.Format("<li>ClassKey:<strong>{0}</strong></li>", classKey));
                WritePageContent(String.Format("<li>Locale:<strong>{0}</strong></li>", locale));
                using (ResXResourceReader resxReader = new ResXResourceReader(fi.FullName))
                {
                    Int32 count = 1;
                    foreach (DictionaryEntry de in resxReader)
                    {
                        try
                        {
                            Lib.Content.Res_ContentStoreBLL.InsertUpdate(classKey, de.Key.ToString(), locale, de.Value.ToString());
                            WritePageContent(String.Format("{0}:{1} ==> {2}", count, de.Key.ToString(), de.Value.ToString()));
                            count++;
                        }
                        catch (Exception exI)
                        {
                            WritePageContent(
                                String.Format("{0}:{1} ==> {2}, ERROR:{3}", count, de.Key.ToString(), de.Value.ToString(), exI.Message));
                        }
                    }
                }
                WritePageContent("</ul>");
                WritePageContent("<br/>");
            }
            WritePageContent("Tasks Completed.", true);
            WritePageFooter();
        }

    }
}