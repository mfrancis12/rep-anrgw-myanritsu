﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="CustomLinkRefAcc.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.CustomLinkAdmin.CustomLinkRefAcc" %>
<%@ Register Src="~/App_Controls/Admin_CustomLink/CustomLinkTabsCtrl.ascx" TagPrefix="uc1" TagName="CustomLinkTabsCtrl" %>
<%@ Register Src="~/App_Controls/Admin_CustomLink/CustomLinkRef_AccountCtrl.ascx" TagPrefix="uc2" TagName="CustomLinkRef_AccountCtrl" %>
<%@ Register Src="~/App_Controls/Admin_CustomLink/CustomLinkNameCtrl.ascx" TagPrefix="uc3" TagName="CustomLinkNameCtrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    - <uc3:CustomLinkNameCtrl runat="server" ID="CustomLinkNameCtrl" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
<div>
    <uc1:CustomLinkTabsCtrl runat="server" id="CustomLinkTabsCtrl" />
     <div class="tabBorder" style="width:738px;">
    <uc2:CustomLinkRef_AccountCtrl runat="server" id="CustomLinkRef_AccountCtrl1" />
    </div>
</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
