﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="SIS_ProdFilterDetails.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.DistPortalAdmin.SIS_ProdFilterDetails" %>

<%@ Register Src="~/App_Controls/Admin_DistPortal/DistPortalAdmin_ProdFilterDetailCtrl.ascx" TagPrefix="uc1" TagName="DistPortalAdmin_ProdFilterDetailCtrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">
        <uc1:DistPortalAdmin_ProdFilterDetailCtrl runat="server" id="DistPortalAdmin_ProdFilterDetailCtrl1" />
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
