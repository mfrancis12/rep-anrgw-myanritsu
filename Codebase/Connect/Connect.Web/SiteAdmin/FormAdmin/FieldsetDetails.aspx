﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="FieldsetDetails.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.FormAdmin.FieldsetDetails" %>
<%@ Register src="~/App_Controls/FormAdmin_FieldsetDetailCtrl.ascx" tagname="FormAdmin_FieldsetDetailCtrl" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/FormAdmin_TreeCtrl.ascx" tagname="FormAdmin_TreeCtrl" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphTopList" runat="server">
    <li id="liPreviewForm" runat="server" visible="false"><asp:HyperLink ID="hlPreviewForm" runat="server" Text="Preview Form" Target="_blank"></asp:HyperLink></li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
    <uc2:FormAdmin_TreeCtrl ID="FormAdmin_TreeCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <uc1:FormAdmin_FieldsetDetailCtrl ID="FormAdmin_FieldsetDetailCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
