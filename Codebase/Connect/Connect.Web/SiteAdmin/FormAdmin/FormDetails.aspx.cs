﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.FormAdmin
{
    public partial class FormDetails : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private Guid FormID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.FormID], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!FormID.IsNullOrEmptyGuid())
                {
                    hlPreviewForm.NavigateUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.FormAdmin_Preview, KeyDef.QSKeys.FormID, FormID);
                    liPreviewForm.Visible = true;
                }
            }
        }
    }
}