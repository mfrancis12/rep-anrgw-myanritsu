﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.SiteAdmin.FormAdmin
{
    public partial class FormPreview : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private Guid FormID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.FormID], Guid.Empty);
            }
        }

        public PlaceHolder FormPlaceHolder
        {
            get
            {
                return phSNAddOnForm;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (FormID.IsNullOrEmptyGuid())
            {
                throw new HttpException(404, "Page not found.");
            }
            Lib.Security.Sec_UserMembership loggedInUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.DynamicForm.Frm_Form formData = Lib.BLL.BLLFrm_Form.SelectByFormID(FormID);
            if (formData == null) throw new HttpException(404, "Page not found.");
            App_Features.DynamicForm.UILib.DynamicFormUtility.InitFormControls(Page, phSNAddOnForm, formData, loggedInUser);
            ltrAddOnPageTitle.Text = formData.FormName;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}