﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="SearchSN.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.GlobalSN.SearchSN" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <h1>
        <asp:Localize ID="lcalHeaderTitle" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminSearchSN,lcalHeaderTitle.Text%>"></asp:Localize></h1>
    <div id="divSearch" class="settingrow width-60">
        <div class="input-text group">
        <label>
            <asp:Localize ID="lcalSearchMN" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminSearchSN,ModelNumber.Text%>"></asp:Localize>:</label>
        <asp:TextBox ID="txtSearchMN" runat="server"></asp:TextBox>
            </div>
        <div class="input-text group">
        <label>
            <asp:Localize ID="lcalSearchSN" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminSearchSN,SerialNumber.Text%>"></asp:Localize>:</label>
        <asp:TextBox ID="txtSearchSN" runat="server"></asp:TextBox>
            </div>
        <div class="group margin-top-15">
            <asp:Button ID="bttSearch" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminSearchSN,bttSearch.Text%>" OnClick="bttSearch_Click" />
        </div>
        
    </div>
    <div class="settingrow" style="overflow: auto; width: 100%; min-height: 500px;">

        <dx:ASPxGridView ID="gvGsn" ClientInstanceName="gvGsn" runat="server" Theme="AnritsuDevXTheme"
            OnBeforePerformDataSelect="gvGsn_OnBeforePerformDataSelect"
            Width="100%" AutoGenerateColumns="False" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="ModelNumber" Caption="<%$Resources:ADM_STPG_SiteAdminSearchSN,ModelNumber.Text%>" Width="200" />
                <dx:GridViewDataColumn FieldName="SerialNumber" Caption="<%$Resources:ADM_STPG_SiteAdminSearchSN,SerialNumber.Text%>" Width="100" />
                <dx:GridViewDataColumn FieldName="Division" Caption="<%$Resources:ADM_STPG_SiteAdminSearchSN,Division.Text%>" Width="100" />
            </Columns>
             <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>


       </div>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminSearchSN" EnableViewState="false" />
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
