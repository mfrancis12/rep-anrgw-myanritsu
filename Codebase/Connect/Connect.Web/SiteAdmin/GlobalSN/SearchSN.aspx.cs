﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.GlobalSN
{
    public partial class SearchSN : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public String MN
        {
            get { return ConvertUtility.ConvertNullToEmptyString(ViewState["MN"]).Trim(); }
            set { ViewState["MN"] = value; }
        }

        public String SN
        {
            get { return ConvertUtility.ConvertNullToEmptyString(ViewState["SN"]).Trim(); }
            set { ViewState["SN"] = value; }
        }

        private DataTable GSNData
        {
            get
            {
                if (Session["AdminPageGSNData"] == null)
                {
                    Session["AdminPageGSNData"] = Lib.Erp.Erp_GSNDBBLL.GSNDB_Search(MN, SN);
                }
                return Session["AdminPageGSNData"] as DataTable;
            }
            set
            {
                Session["AdminPageGSNData"] = value;
            }
        }

     


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                gvGsn.DataBind();
            }
        }

        protected void bttSearch_Click(object sender, EventArgs e)
        {
            String newMN = txtSearchMN.Text.Trim();
            String newSN = txtSearchSN.Text.Trim();
            bool refresh = !(newMN.Equals(MN) && newSN.Equals(SN));
            MN = newMN;
            SN = newSN;
            DoSearch(refresh);
        }

        private void DoSearch(bool refresh)
        {
            if (refresh) GSNData = null;
            gvGsn.DataBind();
        }
        
        protected void gvGsn_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvGsn.DataSource = GSNData;
        }
    }
}