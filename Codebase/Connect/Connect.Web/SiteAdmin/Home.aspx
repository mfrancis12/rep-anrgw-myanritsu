﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div class='siteadminmenu'>
    <ul>
        <li id="liManageResources" runat="server" visible="false"><a class='lnkNewsletter' href="contentadmin/contenthome"><asp:Literal ID="ltrManageResources" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageResources.Text%>"></asp:Literal></a></li>
        <li id="liManageModule" runat="server" visible="false"><a class='lnkNewsletter' href="moduleadmin/home"><asp:Literal ID="ltrManageModule" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageModule.Text%>"></asp:Literal></a></li>
        <li id="liManageForms" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/formadmin/formlist"><asp:Literal ID="ltrManageForms" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageForms.Text%>"></asp:Literal></a></li>
        <li id="liManageMNCfg" runat="server" visible="false"><a class='lnkContentManager' href="/prodregadmin/modelconfig"><asp:Literal ID="ltrManageMNCfg" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageMNCfg.Text%>"></asp:Literal></a></li>
        <li id="liManageUSBJP" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/usbkeyadmin/usbkeys"><asp:Literal ID="ltrManageUSBJPr" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageUSBJPr.Text%>"></asp:Literal></a></li>
        <li id="liAddRegistration" runat="server" visible="false"><a class='lnkContentManager' runat="server"  id="lnkAddRegistration" href="/prodregadmin/manage_registration_addnew"><asp:Literal ID="ltrAddRegistration" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrAddRegistration.Text%>"></asp:Literal></a></li>
        <li id="liSiteAdminSNSearch" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/globalsn/searchsn"><asp:Literal ID="ltrSiteAdminSNSearch" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrSiteAdminSNSearch.Text%>"></asp:Literal></a></li>
        <li id="liSiteAdminUserActivity" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/systemactivity/activeusers"><asp:Literal ID="ltrSiteAdminUserActivity" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrSiteAdminUserActivity.Text%>"></asp:Literal></a></li>
        <li id="liCreateOrg" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/orguseradmin/createOrg"><asp:Literal ID="ltrCreateOrg" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrCreateOrg.Text%>"></asp:Literal></a></li>
        <li id="liManageOrg" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/orguseradmin/orgsearch"><asp:Literal ID="ltrManageOrgr" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageOrgr.Text%>"></asp:Literal></a></li>
        <li id="liManageUsers" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/orguseradmin/connectusersearch"><asp:Literal ID="ltrManageUsers" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageUsers.Text%>"></asp:Literal></a></li>        
        <li id="liManageProdReg" runat="server" visible="false"><a class='lnkContentManager' runat="server"  id="lnkProductregsearch" href="/prodregadmin/productregsearch"><asp:Literal ID="ltrManageProdReg" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrManageProdReg.Text%>"></asp:Literal></a></li>        
        <%--<li id="liZohoReports" runat="server" visible="false"><a class='lnkZohoReports' href="/siteadmin/reports/home"><asp:Literal ID="ltrZohoReports" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,ltrZohoReports.Text%>"></asp:Literal></a></li> --%>        
        <li id="liManageDistPortals" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/orguseradmin/orgsearch"><asp:Literal ID="ltrManageDistPortals" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,liManageDistPortals.Text%>"></asp:Literal></a></li>       
        <li id="liManageDistPortal_ProdFilters" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/distportaladmin/sis_prodfilterlist"><asp:Localize ID="lcalManageDistPortal_ProdFilters" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lcalManageDistPortal_ProdFilters.Text%>"></asp:Localize></a></li>       
        <li id="liManageDistPortal_DocTypeFilters" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/distportaladmin/sis_doctypefilterlist"><asp:Localize ID="lcalManageDistPortal_DocTypeFilters" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lcalManageDistPortal_DocTypeFilters.Text%>"></asp:Localize></a></li>       
        <li id="liManageCustomLinks" runat="server" visible="false"><a class='lnkWebPartAdmin' href="/siteadmin/customlinkadmin/customlinks"><asp:Localize ID="lcalManageCustomLinks" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lcalManageCustomLinks.Text%>"></asp:Localize></a></li>       
        <li id="liManageNotifications" runat="server" visible="false"><a class='lnkContentManager' href="/siteadmin/notificationadmin/notificationlist"><asp:Localize ID="Localize1" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lcalManageNotification.Text%>"></asp:Localize></a></li>       
        <li id="liManagePackages" runat="server" visible="false"><a class='lnkContentManager' runat="server" id="lnkPackages" href="/siteadmin/packageadmin/packagelist"><asp:Localize ID="Localize2" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lcalManagePackage.Text%>"></asp:Localize></a></li>       
        <li id="liProductSupportEntry" runat="server" visible="False"><a class='lnkContentManager' href="/prodregadmin/productsupport/jpsupporttypeentry"><asp:Localize ID="Localize3" runat="server" Text="Product Support Type Entry"></asp:Localize></a></li>       
        <li id="liSearchProductSupportEntry" runat="server" visible="False"><a class='lnkContentManager' href="/prodregadmin/productsupport/search_jpsupporttypeentry"><asp:Localize ID="Localize4" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminHome,lclSearchProductSupportEntry.Text%>"></asp:Localize></a></li>       
    </ul>
</div>
    <div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_SiteAdminHome" /></div>
    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
