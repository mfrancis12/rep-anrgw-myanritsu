﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.SiteAdmin
{
    public partial class Home : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ShowLinks();
            }
        }

        private void ShowLinks()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();

            if (user.IsAdministrator) //siteadmin can do anything
            {
                liManageResources.Visible =
                liManageModule.Visible =
                liManageForms.Visible =
                liManageMNCfg.Visible =
                liManageUSBJP.Visible =
                liManageOrg.Visible =
                liManageUsers.Visible =
                liAddRegistration.Visible =
                liSiteAdminSNSearch.Visible =
                liSiteAdminUserActivity.Visible =
                liCreateOrg.Visible =
                
                liManageDistPortals.Visible =
                liManageDistPortal_ProdFilters.Visible =
                liManageDistPortal_DocTypeFilters.Visible =
                liManageCustomLinks.Visible =
                liManageProdReg.Visible =
                liManageNotifications.Visible =
                liManagePackages.Visible =
                liProductSupportEntry.Visible =
                liSearchProductSupportEntry.Visible = true;
            }
            else
            {
                //bool isProductRegAdmin = Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_Admin);                
                //bool isProductRegApprover = Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_Approver);
                bool isProductConfigAdmin = LoginUtility.IsProdConfigAdminRole();
                bool isProductRegApprover = LoginUtility.IsProdRegAdminRole();
                bool isManageJPUSB = Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductConfig_JPM);
                bool isManageCompany = Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageCompany);
                bool isManagePackages = (Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU) ||
                    Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU) ||
                    Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages));
                // bool isManageProdRegAdmin = Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageProductReg);
                //bool isReportJP = Page.User.IsInRole(KeyDef.UserRoles.Admin_ReportJP);
                //bool isReportUS = Page.User.IsInRole(KeyDef.UserRoles.Admin_ReportUS);
               //bool isReportTAU = Page.User.IsInRole(KeyDef.UserRoles.Admin_ReportTAU);
                bool isReportMMDFeedback = Page.User.IsInRole(KeyDef.UserRoles.Admin_ReportMMDFeedback);
                bool isDistPortalAdmin = (LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_EMEA) || LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_SIS_US));
                bool isCustomLinkAdmin = (
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_AU) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_CN) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_GB) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_JP) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_KR) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_RU) ||
                    LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_CustomLink_US)
                    );

                liManageForms.Visible = liManageMNCfg.Visible = isProductConfigAdmin;
                //liManageOrg.Visible = isProductRegApprover;
                liManageOrg.Visible = liManageUsers.Visible = isProductConfigAdmin || isProductRegApprover || isDistPortalAdmin;
                liManageProdReg.Visible = isProductRegApprover || isManageCompany;
                liCreateOrg.Visible = isManageCompany;
                liAddRegistration.Visible = isProductRegApprover;
                liManageUSBJP.Visible = isManageJPUSB;
                //liZohoReports.Visible = (isReportJP || isReportUS || isReportMMDFeedback || isReportTAU);
                liManageDistPortals.Visible = isDistPortalAdmin;
                liManageDistPortal_ProdFilters.Visible = isDistPortalAdmin;
                liManageDistPortal_DocTypeFilters.Visible = isDistPortalAdmin;
                liManageCustomLinks.Visible = isCustomLinkAdmin;
                liManagePackages.Visible = SetPackagesLink(isManagePackages); ;

                liProductSupportEntry.Visible =
                    Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages);
            }

            // Change URL, Visible Status If login user is JP admin.
            // Search Supporty Type for JP admin.
            liSearchProductSupportEntry.Visible =
                Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages);
            // Product Support Type Entry Link
            liProductSupportEntry.Visible = !(Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages));
            if (Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_JPM) ||
                Page.User.IsInRole(KeyDef.UserRoles.Admin_ProductReg_JPAN) ||
                Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages))
            {
                // Add Registration Link
                lnkAddRegistration.HRef = "/ProdRegAdmin/Manage_Registration_AddNew_JP";
            }

            if (Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages))
            {
                // Search Registered Product
                lnkProductregsearch.HRef = "/ProdRegAdmin/ProductRegSearch_JP";
            }
        }

        private bool SetPackagesLink(bool isManagePackages)
        {
            if (lnkPackages == null) return false;
            if (!isManagePackages) return false;
            if (Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU) ||
                Page.User.IsInRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU))
            {
                lnkPackages.HRef = "/siteadmin/packageadmin/packagelist";
                return true;
            }
            if (Page.User.IsInRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages))
            {
                lnkPackages.HRef = "/siteadmin/packageadmin/packagelist_jp";
                return true;
            }
            return false;
        }
    }
}