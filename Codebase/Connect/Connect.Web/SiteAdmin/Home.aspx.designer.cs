﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace Anritsu.Connect.Web.SiteAdmin {
    
    
    public partial class Home {
        
        /// <summary>
        /// liManageResources コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageResources;
        
        /// <summary>
        /// ltrManageResources コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageResources;
        
        /// <summary>
        /// liManageModule コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageModule;
        
        /// <summary>
        /// ltrManageModule コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageModule;
        
        /// <summary>
        /// liManageForms コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageForms;
        
        /// <summary>
        /// ltrManageForms コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageForms;
        
        /// <summary>
        /// liManageMNCfg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageMNCfg;
        
        /// <summary>
        /// ltrManageMNCfg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageMNCfg;
        
        /// <summary>
        /// liManageUSBJP コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageUSBJP;
        
        /// <summary>
        /// ltrManageUSBJPr コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageUSBJPr;
        
        /// <summary>
        /// liAddRegistration コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liAddRegistration;
        
        /// <summary>
        /// lnkAddRegistration コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkAddRegistration;
        
        /// <summary>
        /// ltrAddRegistration コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrAddRegistration;
        
        /// <summary>
        /// liSiteAdminSNSearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSiteAdminSNSearch;
        
        /// <summary>
        /// ltrSiteAdminSNSearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrSiteAdminSNSearch;
        
        /// <summary>
        /// liSiteAdminUserActivity コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSiteAdminUserActivity;
        
        /// <summary>
        /// ltrSiteAdminUserActivity コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrSiteAdminUserActivity;
        
        /// <summary>
        /// liCreateOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liCreateOrg;
        
        /// <summary>
        /// ltrCreateOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrCreateOrg;
        
        /// <summary>
        /// liManageOrg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageOrg;
        
        /// <summary>
        /// ltrManageOrgr コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageOrgr;
        
        /// <summary>
        /// liManageUsers コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageUsers;
        
        /// <summary>
        /// ltrManageUsers コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageUsers;
        
        /// <summary>
        /// liManageProdReg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageProdReg;
        
        /// <summary>
        /// lnkProductregsearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkProductregsearch;
        
        /// <summary>
        /// ltrManageProdReg コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageProdReg;
        
        
        /// <summary>
        /// liManageDistPortals コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageDistPortals;
        
        /// <summary>
        /// ltrManageDistPortals コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrManageDistPortals;
        
        /// <summary>
        /// liManageDistPortal_ProdFilters コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageDistPortal_ProdFilters;
        
        /// <summary>
        /// lcalManageDistPortal_ProdFilters コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalManageDistPortal_ProdFilters;
        
        /// <summary>
        /// liManageDistPortal_DocTypeFilters コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageDistPortal_DocTypeFilters;
        
        /// <summary>
        /// lcalManageDistPortal_DocTypeFilters コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalManageDistPortal_DocTypeFilters;
        
        /// <summary>
        /// liManageCustomLinks コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageCustomLinks;
        
        /// <summary>
        /// lcalManageCustomLinks コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize lcalManageCustomLinks;
        
        /// <summary>
        /// liManageNotifications コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManageNotifications;
        
        /// <summary>
        /// Localize1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize Localize1;
        
        /// <summary>
        /// liManagePackages コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liManagePackages;
        
        /// <summary>
        /// lnkPackages コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkPackages;
        
        /// <summary>
        /// Localize2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize Localize2;
        
        /// <summary>
        /// liProductSupportEntry コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liProductSupportEntry;
        
        /// <summary>
        /// Localize3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize Localize3;
        
        /// <summary>
        /// liSearchProductSupportEntry コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl liSearchProductSupportEntry;
        
        /// <summary>
        /// Localize4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize Localize4;
        
        /// <summary>
        /// srep コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::Anritsu.Connect.Web.App_Controls.WebControls.StaticResourceEditPanel srep;
    }
}
