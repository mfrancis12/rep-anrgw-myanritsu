﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="EditModule.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ModuleAdmin.EditModule"  ValidateRequest="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<anrui:GlobalWebBoxedPanel ID="pnlModules" runat="server" ShowHeader="true" HeaderText="Modules">
<fieldset>
    <legend>Module Details</legend>
    <div class="settingrow">
            <label class="settinglabel-10"><asp:Localize ID="lcalModule_ModuleID" runat="server" Text="<%$Resources:ThisPage,lcalModule_ModuleID.Text%>"></asp:Localize></label>
            <asp:Literal ID="ltrModule_ModuleID" runat="server" Text="new"></asp:Literal>
        </div>
    <div class="settingrow group input-select width-60">
            <label><asp:Localize ID="lcalModule_Feature" runat="server" Text="<%$Resources:ThisPage,lcalModule_Feature.Text%>"></asp:Localize></label>
            <asp:DropDownList ID="cmbModules" runat="server" DataTextField="FeatureName" DataValueField="FeatureID"></asp:DropDownList>
        </div>
        <div class="settingrow group input-text width-60">
            <label><asp:Localize ID="lcalModule_ModuleName" runat="server" Text="<%$Resources:ThisPage,lcalModule_ModuleName.Text%>"></asp:Localize></label>
            <asp:TextBox ID="txtModule_ModuleName" runat="server"></asp:TextBox>
        </div>
         <div class="settingrow group input-checkbox">
            <p><asp:Localize ID="lcalModule_Others" runat="server" Text="<%$Resources:ThisPage,lcalModule_Others.Text%>"></asp:Localize></p>
            <%--<table>
                <tbody>--%>
                    <p><label><asp:CheckBox ID="cbxShowHeader" runat="server" Text="Show Header" /></label></p>
                    <p><label><asp:CheckBox ID="cbxNoBorder" runat="server" Text="No Border" /></label></p>
                    <p><label><asp:CheckBox ID="cbxIsPublic" runat="server" Text="Is Public" /></label></p>
                    <p><label><asp:CheckBox ID="cbxHideFromLoggedInUser" runat="server" Text="Hide from logged in user" /></label></p>
               <%-- </tbody>
            </table>--%>
        </div>
        <div class="settingrow group margin-top-15">
            <asp:Button ID="bttModuleSave" runat="server"  ValidationGroup="vgModuleDetails" Text="<%$Resources:ThisPage,bttModuleSave.Text%>" 
                onclick="bttModuleSave_Click" />
            <asp:Button ID="bttModuleEditContent" runat="server"  ValidationGroup="vgModuleDetails" Text="<%$Resources:ThisPage,bttModuleEditContent.Text%>"
                onclick="bttModuleEditContent_Click" />
        </div>
</fieldset>
<fieldset>
    <legend>Add Module To Page</legend>
    <div class="settingrow group input-text width-60">
        <label><asp:Localize ID="lcalModulePage_PageUrl" runat="server" Text="<%$Resources:ThisPage,lcalModulePage_PageUrl.Text%>"></asp:Localize></label>
        <asp:TextBox ID="txtModulePage_PageUrl" runat="server" Text="~/home"></asp:TextBox>
    </div>
    <div class="settingrow group input-select width-60">
            <label><asp:Localize ID="lcalModulePage_Cph" runat="server" Text="<%$Resources:ThisPage,lcalModulePage_Cph.Text%>"></asp:Localize></label>
            <asp:DropDownList ID="cmbModulePageCPH" runat="server">
                <asp:ListItem Text="cphContentTop" Value="cphContentTop" Selected="True"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Top" Value="cphLeft_Top"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Middle" Value="cphLeft_Middle"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Bottom" Value="cphLeft_Bottom"></asp:ListItem>
                <asp:ListItem Text="cphContentBottom" Value="cphContentBottom"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterLeft" Value="cphContentCenterLeft"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterRight" Value="cphContentCenterRight"></asp:ListItem>
            </asp:DropDownList>
        </div>
    <div class="settingrow group margin-top-15">
            <asp:Button ID="bttModulePageAdd" runat="server" ValidationGroup="vgModuleDetails"
                Text="<%$Resources:ThisPage,bttModulePageAdd.Text%>" 
                onclick="bttModulePageAdd_Click" />
        </div>
        <label class="failureNotification"><asp:Literal ID="ltrAddModuleToPageMsg" runat="server"></asp:Literal></label>
</fieldset>
<fieldset>
    <legend>Module on Pages</legend>
    <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" onitemcommand="dgList_ItemCommand" OnItemDataBound="dgList_ItemDataBound" ShowFooter="true">
        <Columns>
            <asp:BoundColumn DataField="PageUrl" HeaderText="PageUrl" ItemStyle-Width="300px" ItemStyle-CssClass="dgstyle1-item-lft"></asp:BoundColumn>
             <asp:TemplateColumn HeaderText="PlaceHolder">
                <ItemTemplate>
                    <asp:DropDownList ID="cmbModulePageItemCph" runat="server">
                <asp:ListItem Text="cphContentTop" Value="cphContentTop" Selected="True"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Top" Value="cphLeft_Top"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Middle" Value="cphLeft_Middle"></asp:ListItem>
                <asp:ListItem Text="cphLeft_Bottom" Value="cphLeft_Bottom"></asp:ListItem>
                <asp:ListItem Text="cphContentBottom" Value="cphContentBottom"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterLeft" Value="cphContentCenterLeft"></asp:ListItem>
                <asp:ListItem Text="cphContentCenterRight" Value="cphContentCenterRight"></asp:ListItem>
            </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Order">
                <ItemTemplate>
                    <asp:TextBox ID="txtModulePageLoadOrder" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LoadOrder") %>' SkinID="tbx-20"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="CreatedOnUTC" HeaderText="CreatedOnUTC" DataFormatString="{0:d}"></asp:BoundColumn>
             <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="bttModulePageUpdate" runat="server" Text="update" SkinID="SmallButton" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PageRefID") %>'  ValidationGroup=""/>
                    <asp:Button ID="bttModulePageDelete" runat="server" Text="delete" SkinID="SmallButton" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PageRefID") %>'  ValidationGroup=""/>
                </ItemTemplate>
                <FooterTemplate>
                    
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</fieldset>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
