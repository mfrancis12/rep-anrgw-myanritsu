﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;


namespace Anritsu.Connect.Web.SiteAdmin.ModuleAdmin
{
    public partial class EditModule : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public Int32 ModuleID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.ModuleID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitFields();
                
            }
        }

        private void InitFields()
        {
            cmbModules.DataSource = Lib.BLL.BLLSite_Feature.SelectAll();
            cmbModules.DataBind();

            LoadModuleDetails();
        }

        private void ResetFields()
        {
            ltrModule_ModuleID.Text = ModuleID.ToString();
            cmbModules.SelectedIndex = 0;
            txtModule_ModuleName.Text = "";
            cbxHideFromLoggedInUser.Checked = cbxIsPublic.Checked = cbxNoBorder.Checked = cbxShowHeader.Checked = false;
        }

        private void LoadModuleDetails()
        {
            ResetFields();
            dgList.Visible = false;
            ltrModule_ModuleID.Text = ModuleID.ToString();
            if (ModuleID < 1) return;
            Lib.UI.Site_Module m = Lib.BLL.BLLSite_Module.SelectByModuleID(ModuleID);
            if (m == null) return;
            cmbModules.SelectedValue = m.FeatureID.ToString();
            txtModule_ModuleName.Text = m.ModuleName;
            cbxShowHeader.Checked = m.ShowHeader;
            cbxNoBorder.Checked = m.NoBorder;
            cbxIsPublic.Checked = m.IsPublic;
            cbxHideFromLoggedInUser.Checked = m.HideFromLoggedInUser;
            ModuleOnPagesDataBind();
        }

        protected void bttModuleSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            Int32 moduleID = ConvertUtility.ConvertToInt32(ltrModule_ModuleID.Text, 0);
            Lib.UI.Site_Module m = Lib.BLL.BLLSite_Module.SelectByModuleID(moduleID);
            Boolean isNew = (m == null);
            if (m == null)
            {
                Int32 newModuleID = Lib.BLL.BLLSite_Module.Insert(ConvertUtility.ConvertToInt32(cmbModules.SelectedValue, 10)
                 , txtModule_ModuleName.Text
                 , cbxShowHeader.Checked
                 , cbxNoBorder.Checked
                 , cbxIsPublic.Checked
                 , cbxHideFromLoggedInUser.Checked);
                WebUtility.HttpRedirect(null, String.Format("editmodule?{0}={1}", App_Lib.KeyDef.QSKeys.ModuleID, newModuleID));
            }
            else
            {
                Lib.BLL.BLLSite_Module.Update(moduleID
                 , ConvertUtility.ConvertToInt32(cmbModules.SelectedValue, 10)
                 , txtModule_ModuleName.Text
                 , cbxShowHeader.Checked
                 , cbxNoBorder.Checked
                 , cbxIsPublic.Checked
                 , cbxHideFromLoggedInUser.Checked);
            }
            //ResetFields();
            //ModuleDataBind();
        }

        protected void bttModulePageAdd_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            ltrAddModuleToPageMsg.Text = "";
            if (!txtModulePage_PageUrl.Text.StartsWith("~/"))
            {
                ltrAddModuleToPageMsg.Text = "Invalid page url.";
                return;
            }
            String pageUrl = txtModulePage_PageUrl.Text.Trim();
            String cph = cmbModulePageCPH.SelectedValue;
            Lib.BLL.BLLSite_ModulePage.Insert(ModuleID, pageUrl, cph);
            txtModulePage_PageUrl.Text = "";
            cmbModulePageCPH.SelectedIndex = 0;
            ModuleOnPagesDataBind();
        }

        private void ModuleOnPagesDataBind()
        {
            List<Lib.UI.Site_ModulePage> lst = Lib.BLL.BLLSite_ModulePage.SelectByModuleID(ModuleID);
            dgList.DataSource = lst;
            dgList.DataBind();
            dgList.Visible = (lst != null && lst.Count > 0);
        }

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "UpdateCommand")
            {
                Int32 pageRefID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                TextBox txtModulePageLoadOrder = e.Item.FindControl("txtModulePageLoadOrder") as TextBox;
                Int32 loadOrder = ConvertUtility.ConvertToInt32(txtModulePageLoadOrder.Text, 1);
                DropDownList cmbModulePageItemCph = e.Item.FindControl("cmbModulePageItemCph") as DropDownList;
                Lib.BLL.BLLSite_ModulePage.Update(pageRefID, loadOrder, cmbModulePageItemCph.SelectedValue);
                ModuleOnPagesDataBind();
            }
            else if (e.CommandName == "DeleteCommand")
            {
                Int32 pageRefID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLSite_ModulePage.Delete(pageRefID);
                ModuleOnPagesDataBind();
            }
        }

        protected void dgList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList cmbModulePageItemCph = e.Item.FindControl("cmbModulePageItemCph") as DropDownList;
                cmbModulePageItemCph.SelectedValue = (DataBinder.Eval(e.Item.DataItem, "PlaceHolder")).ToString();
            }
        }

        protected void bttModuleEditContent_Click(object sender, EventArgs e)
        {
            Lib.UI.Site_Module m = Lib.BLL.BLLSite_Module.SelectByModuleID(ModuleID);
            if (m == null) return;
            WebUtility.HttpRedirect(null, m.EditContentUrl);
        }
    }
}