﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.ModuleAdmin.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<anrui:GlobalWebBoxedPanel ID="pnlModules" runat="server" ShowHeader="true" HeaderText="Modules">
    <asp:DataGrid ID="dgList" runat="server" AutoGenerateColumns="false" onitemcommand="dgList_ItemCommand" ShowFooter="true" Width="100%">
        <Columns>
            <asp:BoundColumn DataField="ModuleName" HeaderText="ModuleName"></asp:BoundColumn>
            <asp:BoundColumn DataField="ShowHeader" HeaderText="ShowHeader"></asp:BoundColumn>
            <asp:BoundColumn DataField="NoBorder" HeaderText="NoBorder"></asp:BoundColumn>
            <asp:BoundColumn DataField="IsPublic" HeaderText="IsPublic"></asp:BoundColumn>
            <asp:BoundColumn DataField="HideFromLoggedInUser" HeaderText="HideFromLoggedInUser"></asp:BoundColumn>
            <asp:BoundColumn DataField="CreatedOnUTC" HeaderText="CreatedOnUTC"></asp:BoundColumn>
             <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="bttModuleEdit" runat="server" SkinID="small-button-lpa" Text="edit" CommandName="EditCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ModuleID") %>' ValidationGroup="" />
                    <asp:Button ID="bttModuleDelete" runat="server" SkinID="small-button-lpa" Text="delete" CommandName="DeleteCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ModuleID") %>'  ValidationGroup=""/>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:HyperLink ID="hlAddNew" runat="server" NavigateUrl="editmodule?mdlid=0" Text="Add New" SkinID="button"></asp:HyperLink>
                </FooterTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
