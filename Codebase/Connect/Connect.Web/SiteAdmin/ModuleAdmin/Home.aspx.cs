﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;

namespace Anritsu.Connect.Web.SiteAdmin.ModuleAdmin
{
    public partial class Home : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                ModuleDataBind();
            }
        }

        

        private void ModuleDataBind()
        {
            List<Lib.UI.Site_Module> lst = Lib.BLL.BLLSite_Module.SelectAll();
            dgList.DataSource = lst;
            dgList.DataBind();
        }

        

        //protected void bttModuleSaveAsNew_Click(object sender, EventArgs e)
        //{
        //    if (!Page.IsValid) return;
        //    Lib.BLL.BLLSite_Module.Insert(ConvertUtility.ConvertToInt32(cmbModules.SelectedValue, 10)
        //        , txtModule_ModuleName.Text
        //        , cbxShowHeader.Checked
        //        , cbxNoBorder.Checked
        //        , cbxIsPublic.Checked
        //        , cbxHideFromLoggedInUser.Checked);
        //    ModuleDataBind();
        //}

        protected void dgList_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "EditCommand")
            {
                Int32 moduleID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                //LoadModuleDetails(moduleID);
                WebUtility.HttpRedirect(null, String.Format("editmodule?{0}={1}", App_Lib.KeyDef.QSKeys.ModuleID, moduleID));
            }
            else if (e.CommandName == "DeleteCommand")
            {
                Int32 moduleID = ConvertUtility.ConvertToInt32(e.CommandArgument.ToString(), 0);
                Lib.BLL.BLLSite_Module.Delete(moduleID);
                ModuleDataBind();
            }
        }

       

    }
}