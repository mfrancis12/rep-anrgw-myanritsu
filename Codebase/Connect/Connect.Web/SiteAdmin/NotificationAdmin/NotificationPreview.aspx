﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="NotificationPreview.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.NotificationAdmin.NotificationPreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
    <asp:Literal ID="ltrAddOnPageTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">
        <anrui:GlobalWebBoxedPanel ID="GlobalWebBoxedPanel1" runat="server" ShowHeader="true"
            HeaderText='<%$ Resources:~/notificationpreview,pnlContainerNotificationText.HeaderText %>'>
            <asp:PlaceHolder ID="phPreviewNotification" runat="server"></asp:PlaceHolder>
            <div class="center-buttons">
                <asp:Button ID="Button1" runat="server" Text='<%$ Resources:~/notificationpreview,btnAcknowledgeDummy.Text %>' />
                <asp:Button ID="Button2" runat="server" Text='<%$ Resources:~/notificationpreview,btnNotAcknowledgeDummy.Text %>' />
            </div>
        </anrui:GlobalWebBoxedPanel>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
