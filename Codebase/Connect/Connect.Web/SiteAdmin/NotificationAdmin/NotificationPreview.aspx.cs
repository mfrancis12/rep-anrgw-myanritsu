﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib.Notification;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.SiteAdmin.NotificationAdmin
{

    public partial class NotificationPreview : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private int NotificationID
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.NotificationId], 0);
            }
        }

        public PlaceHolder NotificationPlaceHolder
        {
            get
            {
                return phPreviewNotification;
            }
        }

        public string ResClassKey
        {
            get {
                if (NotificationID == 0)
                    return null;
                return String.Format("NOTIFICATION_{0}", NotificationID.ToString());
            }
        }

        public string ResKey
        {
            get
            {
                return "NotificationText";
            }
        }

        /// <summary>
        /// preapre Notification preview and add notification text to the place holder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (NotificationID==0)
            {
                throw new HttpException(404, "Page not found.");
            }
            Lib.Security.Sec_UserMembership loggedInUser = LoginUtility.GetCurrentUserOrSignIn();
            Notification notificationData = Lib.Notification.NotificationBLL.SelectByNotificationID(NotificationID);
            if (notificationData == null) throw new HttpException(404, "Page not found.");
            
            ltrAddOnPageTitle.Text = notificationData.NotificationName;
            Literal ltrlNotificationText=new Literal ();
            ltrlNotificationText.Text=HttpContext.GetGlobalResourceObject(ResClassKey, ResKey).ToString();
            phPreviewNotification.Controls.Add(ltrlNotificationText);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}