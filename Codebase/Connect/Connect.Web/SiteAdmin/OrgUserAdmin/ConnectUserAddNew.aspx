﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="ConnectUserAddNew.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.ConnectUserAddNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<anrui:GlobalWebBoxedPanel ID="pnlAdminAddNewUser" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminUserAddNew,pnlAdminAddNewUser.HeaderText %>">
    <div class="settingrow group input-text required width-60">
        <label><asp:Localize ID="lcalNewUserEmail" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserAddNew,lcalNewUserEmail.Text %>"></asp:Localize></label>
        <p class="error-message">
            <asp:Localize ID="lclEmlErr" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserAddNew,rfvNewUserEmail.ErrorMessage %>"></asp:Localize>
        </p>
        <asp:RegularExpressionValidator ID="revNewUserEmail" runat="server"  Display="Dynamic"
            ControlToValidate="txtNewUserEmail" ValidationGroup="vgAddNewUserByAdmin" 
            ErrorMessage='<%$Resources:ADM_STPG_SiteAdminUserAddNew,revNewUserEmail.ErrorMessage %>' 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <asp:TextBox ID="txtNewUserEmail" runat="server" ValidationGroup="vgAddNewUserByAdmin" MaxLength="300"></asp:TextBox>
        <%--<asp:RequiredFieldValidator id="rfvNewUserEmail" Display="Dynamic" runat="server" ControlToValidate="txtNewUserEmail"  ValidationGroup="vgAddNewUserByAdmin" ErrorMessage='<%$Resources:ADM_STPG_SiteAdminUserAddNew,rfvNewUserEmail.ErrorMessage %>'></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="revNewUserEmail" runat="server"  Display="Dynamic"
            ControlToValidate="txtNewUserEmail" ValidationGroup="vgAddNewUserByAdmin" 
            ErrorMessage='<%$Resources:ADM_STPG_SiteAdminUserAddNew,revNewUserEmail.ErrorMessage %>' 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
    </div>
    <div class="settingrow group input-select width-60">
        <label><asp:Localize ID="lcalNewUserOrg" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserAddNew,lcalNewUserOrg.Text %>"></asp:Localize></label>
        <asp:DropDownList ID="cmbAddToOrg" runat="server" width="310px" DataTextField="CompanyOrgName" DataValueField="AccountID" AppendDataBoundItems="true" ValidationGroup="vgAddNewUserByAdmin">
            <asp:ListItem Text="<%$Resources:ADM_STPG_SiteAdminUserAddNew,cmbAddToOrg.DefaultItemText %>" Value="selectone" Selected="True"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="settingrow margin-top-15">
        <asp:Button ID="bttAddNewUser" runat="server" 
            Text="<%$Resources:ADM_STPG_SiteAdminUserAddNew,bttAddNewUser.Text %>" 
            ValidationGroup="vgAddNewUserByAdmin" onclick="bttAddNewUser_Click" SkinID="submit-btn" />
        <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
    </div>
</anrui:GlobalWebBoxedPanel>
<div class="settingrow"><anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminUserAddNew" EnableViewState="false" /></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
