﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.EntityFramework;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using DevExpress.Web.Internal;
using EM = Anritsu.AnrCommon.EmailSDK;


namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class ConnectUserAddNew : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindOrg();
            }
        }

        private void BindOrg()
        {
            DataTable selectedAcc = new DataTable();
            selectedAcc = Lib.BLL.BLLAcc_AccountMaster.SelectAll();
            for (int i = selectedAcc.Rows.Count - 1; i >= 0; i--)
            {
                if (selectedAcc.Rows[i]["CompanyOrgName"].ToString() == "")
                    selectedAcc.Rows[i].Delete();
            }

            cmbAddToOrg.DataSource = selectedAcc;
            cmbAddToOrg.DataBind();
        }

        protected void bttAddNewUser_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            Guid accountID = ConvertUtility.ConvertToGuid(cmbAddToOrg.SelectedValue, Guid.Empty);
            if (accountID.IsNullOrEmptyGuid())
            {
                ltrMsg.Text = GetStaticResource("ltrMsg.Text.SelectOrg");
                return;
            }
            ltrMsg.Text = String.Empty;
            Lib.Security.Sec_UserMembership actingUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.Security.Sec_UserMembership user = Lib.BLL.BLLSec_UserMembership.AddNewUserPendingLogin(accountID
                , txtNewUserEmail.Text.Trim()
                , "n/a"
                , "n/a"
                , false
                , WebUtility.GetUserIP()
                , actingUser.Email
                , actingUser.FullName
                , String.Empty
                , String.Empty
                , String.Empty
                , String.Empty
                );

            if (user == null)
            {
                ltrMsg.Text = GetStaticResource("ltrMsg.Text.CannotAddNewUser");
            }
            else
            {
                SendEmails(user, accountID);
                WebUtility.HttpRedirect(null, String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.AdminUserDetail, KeyDef.QSKeys.MemMembershipId, user.MembershipId));
            }
        }


        public void SendEmails(Lib.Security.Sec_UserMembership user, Guid accountId)
        {
            #region " send submitted email to customer "

            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountId);
            var addedUser = new ConnectSsoUser(user);
            Int32 gwCultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            //if (!addedUser.UserPrimaryCultureCode.ToLower().Contains("jp"))
            //    return;
            user.FirstName = user.FirstName.Equals("n/a") ? String.Empty : user.FirstName;
            user.LastName = user.LastName.Equals("n/a") ? String.Empty : user.LastName;
            List<EM.EmailWebRef.ReplaceKeyValueType> emailData = Email_GetKeyValues(user, gwCultureGroupId, acc);
            if (emailData == null) return;

            var reqUserSubmittedAckEmail =
                            new EM.EmailWebRef.SendTemplatedEmailCallRequest();
            reqUserSubmittedAckEmail.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.AddOrgUser.NotifyUser");// replace with Connect.Web.ProdRegEmail.UserSubmittedAck when JP goes live
            reqUserSubmittedAckEmail.ToEmailAddresses = new string[] { addedUser.Email };
            reqUserSubmittedAckEmail.CultureGroupId = SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            reqUserSubmittedAckEmail.SubjectReplacementKeyValues = emailData.ToArray();
            reqUserSubmittedAckEmail.BodyReplacementKeyValues = emailData.ToArray();
            var response = EM.EmailServiceManager.SendTemplatedEmail(reqUserSubmittedAckEmail);
            #endregion

            #region " Send user added to organization notification email to admin"

            List<EM.EmailWebRef.ReplaceKeyValueType> kvListForAdmin = GetKeyValuesForAdmin(accountId, user);
            EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest email =
               new EM.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("CONNECT_ADDUSERTOORGANIZATION_ADMINJP");
            email.DistributionListKey = ConfigUtility.AppSettingGetValue("CONNECT_ADDUSERTOORGANIZATION_ADMINJP_Dkey");
            email.CultureGroupId =   SiteUtility.BrowserLang_GetGlobalWebCultureGroupId();
            email.SubjectReplacementKeyValues = kvListForAdmin.ToArray();
            email.BodyReplacementKeyValues = kvListForAdmin.ToArray();
            EM.EmailWebRef.BaseResponseType resp = EM.EmailServiceManager.SendDistributedTemplatedEmail(email);
            var errorMsg = String.Empty;
            if (resp != null)  errorMsg = resp.ResponseMessage;

            #endregion

        }

        private static List<EM.EmailWebRef.ReplaceKeyValueType> GetKeyValuesForAdmin(Guid accountID, Lib.Security.Sec_UserMembership user)
        {
            Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;

            #region " user and account information "
            if (user != null)
            {
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[FIRSTNAME]]";
                kv.ReplacementValue = Convert.ToString(user.FirstName);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[UserFirstName]]";
                kv.ReplacementValue = Convert.ToString(user.FirstName);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[UserLastName]]";
                kv.ReplacementValue = Convert.ToString(user.LastName);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "UserEmail";
                kv.ReplacementValue = Convert.ToString(user.Email);
                kvList.Add(kv);
                
            }
            if (acc != null)
            {
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[CompanyName]]";
                kv.ReplacementValue = Convert.ToString(acc.CompanyName);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[AccountName]]";
                kv.ReplacementValue = Convert.ToString(acc.AccountName);
                kvList.Add(kv);
            }
            #endregion
            return kvList;
        }

        private List<EM.EmailWebRef.ReplaceKeyValueType> Email_GetKeyValues(Lib.Security.Sec_UserMembership user, Int32 gwCultureGroupId, Lib.Account.Acc_AccountMaster acc)
        {
            var kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;
            if (user != null)
            {
                #region " User information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                kv.ReplacementValue = user.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                kv.ReplacementValue = user.LastName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SPACEHOLDER]]";
                kv.ReplacementValue = (String.IsNullOrEmpty(user.FirstName) && String.IsNullOrEmpty(user.LastName)) ? String.Empty : " ";
                kvList.Add(kv);
                #endregion
            }

            if (acc != null)
            {
                #region " account information "
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = " [[COMPANYNAME]]";
                kv.ReplacementValue = acc.CompanyName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[ORGANIZATIONNAME]]";
                kv.ReplacementValue = acc.AccountName;
                kvList.Add(kv);

                #endregion
            }

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[LOGINLINKTEXT]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[LOGINLINK]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);

            return kvList;
        }
        //protected void bttSearch_Click(object sender, EventArgs e)
        //{
        //    cmbAddToOrg.Items.Clear();
        //    cmbAddToOrg.DataSource = Lib.Security.Sec_UserAccountRoleBLL.SelectSearchAccounts(MembershipId, txtSearch.Text.Trim());
        //    cmbAddToOrg.DataBind();
        //}

        public string StaticResourceClassKey
        {
            get { return "ADM_STPG_SiteAdminUserAddNew"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }
    }
}