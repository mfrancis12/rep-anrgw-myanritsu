﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ConnectUserDetail.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.ConnectUserDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);

            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
    </script>
    <anrui:GlobalWebBoxedPanel ID="pnlUserInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,pnlUserInfo.HeaderText %>"
        DefaultButton="bttDeleteAll">
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalEmail" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalEmail.Text %>" />:</span>
            <asp:Literal ID="ltrUserEmail" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalFullName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalFullName.Text %>" />:</span>
            <asp:Literal ID="ltrFullName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalLastLoginUTC" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalLastLoginUTC.Text %>" />:</span>
            <asp:Literal ID="ltrUserLastLoginUTC" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalLastActivity" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalLastActivity.Text %>" />:</span>
            <asp:Literal ID="ltrUserLastActivityUTC" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalGWUserID" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalGWUserID.Text %>" />:</span>
            <asp:Literal ID="ltrGlobalWebUserID" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalIsSiteAdmin" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalIsSiteAdmin.Text %>" />:</span>
            <asp:CheckBox ID="cbxIsSiteAdmin" runat="server" Text=" " Enabled="false" />
        </div>
        <div class="settingrow margin-top-15">
            <asp:Button ID="bttDeleteAll" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,bttDeleteAll.Text %>"
                OnClick="bttDeleteAll_Click" />
        </div>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlOrgInfo" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,pnlOrgInfo.HeaderText %>" DefaultButton="bttUpdate">
      <asp:GridView ID="gvOrgMembership" runat="server" AutoGenerateColumns="false" OnRowCommand="gvOrgMembership_RowCommand"
                OnRowDataBound="gvOrgMembership_RowDataBound" Width="100%">
                <Columns>
                    <asp:BoundField DataField="CompanyName" HeaderText='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CompanyName.Label %>' />
                    <asp:TemplateField HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.AccountName.Label %>">
                        <ItemTemplate>
                            <asp:HyperLink id="hlAccountName" SkinID="blueit" runat="server"  Text='<%# DataBinder.Eval(Container.DataItem, "AccountName") %>' NavigateUrl='<%# GetUrl(DataBinder.Eval(Container.DataItem,"AccountID")) %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="400px" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="gvstyle1-item-lft">
                        <ItemTemplate>
                            <div class="settingrow">
                                <span class="settinglabelrgt-25">
                                    <asp:Localize ID="lcalIsManager" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsManager.Label %>'></asp:Localize>:</span>
                                <asp:CheckBox ID="cbxIsManager" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManager") %>' />
                            </div>
                            <asp:Panel ID="pnlProdRegRoles" runat="server" Visible="false">
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsSISAdmin" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.SISAdmin.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsDistributorEMEA" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsDistributorEMEA") %>' />
                                </div>
                              
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsManageFileEnggUKTAU" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.ManageFileEnggUKTAU.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsManageFileEnggUKTAU" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManageFileEnggUKTAU") %>' />
                                </div>
                                  <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsManageFileCommEnggUKTAU" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.ManageFileCommEnggUKTAU.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsManageFileCommEnggUKTAU" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManageFileCommEnggUKTAU") %>' />
                                </div>
                               
                                   <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminEMEA" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminEMEA.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminEMEA" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminEMEA") %>' />
                                  </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminAsia" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminAsia.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminAsia" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminAsia") %>' />
                                  </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminUS" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminUS.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminUS" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminUS") %>' />
                                  </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminJP" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminJP.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminJP" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminJP") %>' />
                                  </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminKO" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminKO.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminKO" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminKO") %>' />
                                  </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminRU" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminRU.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminRU" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminRU") %>' />
                                  </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsLnkAdminZH" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.LnkAdminZH.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsLnkAdminZH" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsLnkAdminZH") %>' />
                                  </div>
                                     
                                  <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsManageCompany" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsManageCompany.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsManageCompany" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManageCompany") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdConfigJPAN" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsProdConfigJPAN.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdConfigJPAN" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdConfigJPAN") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdConfigJPM" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsProdConfigJPM.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdConfigJPM" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdConfigJPM") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdConfigUSMMD" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsProdConfigUSMMD.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdConfigUSMMD" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdConfigUSMMD") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdConfigUKTAU" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsProdConfigUKTAU.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdConfigUKTAU" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdConfigUKTAU") %>' />
                                </div>
                                  <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdConfigDKM" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsProdConfigDKM.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdConfigDKM" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdConfigDKM") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsManageProdRegAdmin" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsManageProdRegAdmin.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsManageProdRegAdmin" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManageProdRegAdmin") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminJPAN" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminJPAN.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminJPAN" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminJPAN") %>' />
                                </div>
                                <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminJPM" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminJPM.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminJPM" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminJPM") %>' />
                                </div>
                               
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminESD" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminESD.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminESD" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminESD") %>' />
                                </div>
                               <%--  <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminSIS" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminSIS.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminSIS" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminSIS") %>' />
                                </div>--%>
                                <%-- <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminGSW" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminGSW.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminGSW" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminGSW") %>' />
                                </div>--%>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminUSMMD" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminUSMMD.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminUSMMD" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminUSMMD") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsProdRegAdminDKM" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsProdRegAdminDKM.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsProdRegAdminDKM" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsProdRegAdminDKM") %>' />
                                </div>
                              
                               <%-- <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsReportJP" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsReportJP.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsReportJP" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsReportViewerJP") %>' />
                                </div>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsReportUS" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsReportUS.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsReportUS" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsReportViewerUS") %>' />
                                </div>
                                  <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsReportMMDFeedback" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.IsReportMMDFeedback.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsReportMMDFeedback" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsReportViewerMMDFeedback") %>' />
                                </div>
                               <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcalIsReportTAU" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcalIsReportTAU.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsReportTAU" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsReportViewerTAU") %>' />
                                </div>--%>
                                 <div class="settingrow">
                                    <span class="settinglabelrgt-25">
                                        <asp:Localize ID="lcaIsManageSupportTypePackageJP" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.lcaIsManageSupportTypePackageJP.Label %>'></asp:Localize>:</span>
                                    <asp:CheckBox ID="cbxIsManageSupportTypePackageJP" runat="server" Text=' ' Checked='<%# DataBinder.Eval(Container.DataItem, "IsManageSupportTypePackageJP") %>' />
                                </div>
                            </asp:Panel>
                            <div class="settingrow">
                                <span class="settinglabelrgt-25">
                                    <asp:Localize ID="lcalAddedBy" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.AddedBy.Label %>'></asp:Localize>:</span>
                                <asp:Literal ID="ltrAddedByName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "AddedByName") %>'></asp:Literal>
                            </div>
                            <div class="settingrow" style="text-align: right;">
                                <span class="msg-red-small">
                                    <asp:Literal ID="ltrOrgUserUpdateMsg" runat="server"></asp:Literal></span>
                                <asp:Button ID="bttUpdate" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.bttUpdate.Text %>" Visible='<%# IsAdminMode()%>'
                                   CausesValidation="false" SkinID="small-button-lpa" CommandName="UpdateCommand" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>' />
                                <asp:Button ID="bttRemoveFromOrg" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.bttRemoveFromOrg.Text %>" Visible='<%# IsAdminMode()%>'
                                    CausesValidation="false" SkinID="small-button-lpa" CommandName="RemoveFromOrgCommand"
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AccountID") %>' />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <%-- <asp:BoundField DataField="CreatedOnUTC" HeaderText='<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CreatedOnUTC %>'
                        DataFormatString="{0:MMM dd, yyyy}" />--%>
                    <asp:TemplateField HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,gvOrgMembership.CreatedOnUTC %>">
                        <ItemTemplate>
                            <asp:Literal ID="ltrCreatedOnUTC" runat="server" Text='<%# GetDate(DataBinder.Eval(Container.DataItem,"CreatedOnUTC")) %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlAddUserToOrg" runat="server" ShowHeader="true"
        HeaderText="<%$Resources:ADM_STPG_SiteAdminUserDetail,pnlAddUserToOrg.HeaderText %>" DefaultButton="bttSearch">
        <div class="settingrow group input-text width-60">
                <label>
                    <asp:Localize ID="lcalSearchCategory" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserDetail,lcalSearchCategory.Text %>" /></label>
                <asp:TextBox ID="txtSearch" runat="server" />
               
            </div>
        <div class="group margin-top-15">
             <asp:Button ID="bttSearch" runat="server" CausesValidation="false" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,bttSearch.Text %>'
                    OnClick="bttSearch_Click" />
        </div>
            <div class="settingrow group width-60 input-select">
                  <p class="msg">
                 <asp:Localize ID="lcalErrMsg" runat="server" ></asp:Localize></p>
                <asp:DropDownList ID="cmbAddToOrg" runat="server" DataTextField="CompanyOrgName"
                    DataValueField="AccountID" AppendDataBoundItems="true">
                    <asp:ListItem Text=" Select Account To Add This User " Value="selectone" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        <div class="group margin-top-15">
                <asp:Button ID="bttAddUserToOrg" runat="server" CausesValidation="false" Text='<%$Resources:ADM_STPG_SiteAdminUserDetail,bttAddUserToOrg.Text %>'
                    OnClick="bttAddUserToOrg_Click" />
               
        </div>
    </anrui:GlobalWebBoxedPanel>
    <div class="settingrow">
        <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminUserDetail"
            EnableViewState="false" />
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
