﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Lib.Account;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using System.Globalization;
using DevExpress.DataAccess.Native.DB;
using DataTable = System.Data.DataTable;


namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class ConnectUserDetail : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        //private static System.Data.DataTable UserOrgRoles;
        private Guid MembershipId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.MemMembershipId], Guid.Empty);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);
            bttDeleteAll.OnClientClick = "return confirm ('" + GetStaticResource("ConfirmDeleteAllMsg") + "')";

            txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + bttSearch.ClientID + "')");

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUserInfo();
            }

        }

        //<summary>
        //Checks whether user is super admin
        //</summary>
        //<returns></returns>
        public static Boolean IsAdminMode()
        {
            Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();
            Boolean isAdminMode = user.IsAdministrator;
            return isAdminMode;
        }

        private void LoadUserInfo()
        {
            if (MembershipId.IsNullOrEmptyGuid()) return;//new user
            Lib.Security.Sec_UserMembership user = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(MembershipId);
            if (user == null) throw new HttpException(404, "Page not found.");
            ltrUserEmail.Text = user.Email;
            ltrFullName.Text = user.FullName;
            cbxIsSiteAdmin.Checked = user.IsAdministrator;
            ltrUserLastActivityUTC.Text = user.LastActivityDate == DateTime.MinValue ? "n/a" : GetDate(user.LastActivityDate.ToString());
            ltrUserLastLoginUTC.Text = user.LastLoginDate == DateTime.MinValue ? "n/a" : GetDate(user.LastLoginDate.ToString());
            ltrGlobalWebUserID.Text = user.GWUserId < 1 ? "n/a" : user.GWUserId.ToString();

            DataBindOrgMembership();

            bttDeleteAll.Visible = pnlAddUserToOrg.Visible = IsAdminMode();
            //DataBindAvailableOrgs();
        }

        private void DataBindOrgMembership()
        {
            //UserOrgRoles = new DataTable();
            //UserOrgRoles.Clear();
            //UserOrgRoles =
            gvOrgMembership.DataSource = Lib.Security.Sec_UserAccountRoleBLL.SelectForUserAdmin(MembershipId);
            gvOrgMembership.DataBind();
        }


        //private void DataBindAvailableOrgs()
        //{
        //    cmbAddToOrg.DataSource = Lib.Security.Sec_UserAccountRoleBLL.SelectAvailableAccounts(MembershipId);
        //    cmbAddToOrg.DataBind();
        //}

        protected void bttUserInfoSave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

        }

        protected void bttDeleteAll_Click(object sender, EventArgs e)
        {
            Lib.BLL.BLLSec_UserMembership.Delete(MembershipId);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.AdminUserSearchResult);
        }

        protected void gvOrgMembership_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "UpdateCommand")
            {
                Guid accountID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                if (accountID.IsNullOrEmptyGuid() || MembershipId.IsNullOrEmptyGuid()) return;
                GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                //CheckBox cbxIsProdRegAdmin = row.FindControl("cbxIsProdRegAdmin") as CheckBox;
                CheckBox cbxIsManager = row.FindControl("cbxIsManager") as CheckBox;
                //CheckBox cbxIsProdRegApprover = row.FindControl("cbxIsProdRegApprover") as CheckBox;
                CheckBox cbxIsDistributorEMEA = row.FindControl("cbxIsDistributorEMEA") as CheckBox;
                CheckBox cbxIsManageFileEnggUKTAU = row.FindControl("cbxIsManageFileEnggUKTAU") as CheckBox;
                CheckBox cbxIsManageFileCommEnggUKTAU = row.FindControl("cbxIsManageFileCommEnggUKTAU") as CheckBox;
                CheckBox cbxIsLnkAdminEMEA = row.FindControl("cbxIsLnkAdminEMEA") as CheckBox;
                CheckBox cbxIsLnkAdminAsia = row.FindControl("cbxIsLnkAdminAsia") as CheckBox;
                CheckBox cbxIsLnkAdminUS = row.FindControl("cbxIsLnkAdminUS") as CheckBox;
                CheckBox cbxIsLnkAdminJP = row.FindControl("cbxIsLnkAdminJP") as CheckBox;
                CheckBox cbxIsLnkAdminKO = row.FindControl("cbxIsLnkAdminKO") as CheckBox;
                CheckBox cbxIsLnkAdminRU = row.FindControl("cbxIsLnkAdminRU") as CheckBox;
                CheckBox cbxIsLnkAdminZH = row.FindControl("cbxIsLnkAdminZH") as CheckBox;
                CheckBox cbxIsManageProdRegAdmin = row.FindControl("cbxIsManageProdRegAdmin") as CheckBox;
                CheckBox cbxIsManageCompany = row.FindControl("cbxIsManageCompany") as CheckBox;
                CheckBox cbxIsProdConfigJPAN = row.FindControl("cbxIsProdConfigJPAN") as CheckBox;
                CheckBox cbxIsProdConfigJPM = row.FindControl("cbxIsProdConfigJPM") as CheckBox;
                CheckBox cbxIsProdConfigUSMMD = row.FindControl("cbxIsProdConfigUSMMD") as CheckBox;
                CheckBox cbxIsProdConfigUKTAU = row.FindControl("cbxIsProdConfigUKTAU") as CheckBox;
                CheckBox cbxIsProdConfigDKM = row.FindControl("cbxIsProdConfigDKM") as CheckBox;
                //CheckBox cbxIsProdRegAdminJPSW = row.FindControl("cbxIsProdRegAdminJPSW") as CheckBox;
                CheckBox cbxIsProdRegAdminJPM = row.FindControl("cbxIsProdRegAdminJPM") as CheckBox;
                CheckBox cbxIsProdRegAdminJPAN = row.FindControl("cbxIsProdRegAdminJPAN") as CheckBox;
                CheckBox cbxIsProdRegAdminESD = row.FindControl("cbxIsProdRegAdminESD") as CheckBox;
                CheckBox cbxIsProdRegAdminUSMMD = row.FindControl("cbxIsProdRegAdminUSMMD") as CheckBox;
                CheckBox cbxIsProdRegAdminDKM = row.FindControl("cbxIsProdRegAdminDKM") as CheckBox;
                //CheckBox cbxIsReportJP = row.FindControl("cbxIsReportJP") as CheckBox;
                //CheckBox cbxIsReportUS = row.FindControl("cbxIsReportUS") as CheckBox;
                //CheckBox cbxIsReportTAU = row.FindControl("cbxIsReportTAU") as CheckBox;
                //CheckBox cbxIsReportMMDFeedback = row.FindControl("cbxIsReportMMDFeedback") as CheckBox;
                var cbxIsManageSupportTypePackageJP =
                    row.FindControl("cbxIsManageSupportTypePackageJP") as CheckBox;
                
                Literal ltrOrgUserUpdateMsg = row.FindControl("ltrOrgUserUpdateMsg") as Literal;
                ltrOrgUserUpdateMsg.Text = String.Empty;
                Lib.Security.Sec_UserMembership user = LoginUtility.GetCurrentUserOrSignIn();

                if (cbxIsManager.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManager,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManager);

                if (cbxIsManageProdRegAdmin.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageProductRegistrationAdmin,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageProductRegistrationAdmin);

                if (cbxIsManageCompany.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageCompany,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageCompany);

                if (cbxIsManageFileEnggUKTAU.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageFileEnggUKTAU,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageFileEnggUKTAU);

                if (cbxIsManageFileCommEnggUKTAU.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageFileCommEnggUKTAU,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleManageFileCommEnggUKTAU);

                              
                if (cbxIsProdConfigJPAN.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigJPAN,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigJPAN);

                if (cbxIsProdConfigJPM.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigJPM,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigJPM);

                if (cbxIsProdConfigUSMMD.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigUSMMD,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigUSMMD);

                if (cbxIsProdConfigUKTAU.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigUKTAU,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigUKTAU);

                if (cbxIsProdConfigDKM.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigDKM,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductConfigDKM);

                if (cbxIsDistributorEMEA.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleDistributorEMEA,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleDistributorEMEA);

                if (cbxIsLnkAdminEMEA.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENGB,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENGB);

                if (cbxIsLnkAdminAsia.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENAU,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENAU);

                if (cbxIsLnkAdminUS.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENUS,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmENUS);

                if (cbxIsLnkAdminJP.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmJAJP,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmJAJP);

                if (cbxIsLnkAdminKO.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmKOKR,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmKOKR);

                if (cbxIsLnkAdminRU.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmRURU,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmRURU);

                if (cbxIsLnkAdminZH.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmZHCN,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleLnkAdmZHCN);


                if (cbxIsProdRegAdminJPM.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegJPM,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegJPM);
                if (cbxIsProdRegAdminJPAN.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegJPAN,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegJPAN);

                if (cbxIsProdRegAdminESD.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegESD,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegESD);
               
                if (cbxIsProdRegAdminUSMMD.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegUSMMD,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegUSMMD);

                if (cbxIsProdRegAdminDKM.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegDKM,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleProductRegDKM);

                /*  //if (cbxIsReportJP.Checked)
                  {
                      Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportJP,
                      WebUtility.GetUserIP(), user.Email, user.FullName);
                  }
                  else
                      Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportJP);

                  if (cbxIsReportUS.Checked)
                  {
                      Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportUS,
                      WebUtility.GetUserIP(), user.Email, user.FullName);
                  }
                  else
                      Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportUS);

                  if (cbxIsReportTAU.Checked)
                  {
                      Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportTAU,
                      WebUtility.GetUserIP(), user.Email, user.FullName);
                  }
                  else
                      Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportTAU);

                  if (cbxIsReportMMDFeedback.Checked)
                  {
                      Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportMMDFeedback,
                      WebUtility.GetUserIP(), user.Email, user.FullName);
                  }
                  else
                      Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleReportMMDFeedback);*/
                if (cbxIsManageSupportTypePackageJP.Checked)
                {
                    Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleMngSptTypePackagesJp,
                    WebUtility.GetUserIP(), user.Email, user.FullName);
                }
                else
                    Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID, Lib.Security.Sec_RoleCode.RoleMngSptTypePackagesJp);
              

                ltrOrgUserUpdateMsg.Text = GetStaticResource("UpdateMsg");

            }
            else if (e.CommandName == "RemoveFromOrgCommand")
            {
                Guid accountID = ConvertUtility.ConvertToGuid(e.CommandArgument.ToString(), Guid.Empty);
                if (accountID.IsNullOrEmptyGuid() || MembershipId.IsNullOrEmptyGuid()) return;
                Lib.BLL.BLLSec_Role.Delete(MembershipId, accountID);
                WebUtility.HttpRedirect(null, Request.RawUrl);
            }
        }

        protected void gvOrgMembership_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button bttRemoveFromOrg = e.Row.FindControl("bttRemoveFromOrg") as Button;
                bttRemoveFromOrg.OnClientClick = "return confirm ('" + GetGlobalResourceObject("common", "ConfirmDeleteRegMsg") + "')";
                Guid accountID = ConvertUtility.ConvertToGuid(DataBinder.Eval(e.Row.DataItem, "AccountID"), Guid.Empty);
                if (accountID == AccountUtility.AnritsuMasterAccountID())
                {
                    Panel pnlProdRegRoles = e.Row.FindControl("pnlProdRegRoles") as Panel;
                    pnlProdRegRoles.Visible = true;
                }
            }
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }


        public String GetUrl(object accid)
        {
            String accId = accid.ToString();
            String url = String.Format("/siteadmin/orguseradmin/orgdetail?accid={0}"
                         , HttpUtility.UrlEncode(accId));

            return url;
        }


        protected void bttSearch_Click(object sender, EventArgs e)
        {
            cmbAddToOrg.Items.Clear();
            var searchData = Lib.Security.Sec_UserAccountRoleBLL.SelectSearchAccounts(MembershipId, txtSearch.Text.Trim());
            if (searchData.Rows.Count > 0)
            {
                lcalErrMsg.Text = "";
                cmbAddToOrg.Enabled = true;
                cmbAddToOrg.DataSource = searchData;
                cmbAddToOrg.DataBind();
                cmbAddToOrg.Focus();
            }
            else
            {
                lcalErrMsg.Text = GetStaticResource("lcalOrgUserExist");
                cmbAddToOrg.Enabled = false;
            }
        }

        protected void bttAddUserToOrg_Click(object sender, EventArgs e)
        {
            Guid accountID = ConvertUtility.ConvertToGuid(cmbAddToOrg.SelectedValue, Guid.Empty);
            if (accountID.IsNullOrEmptyGuid() || MembershipId.IsNullOrEmptyGuid()) return;
            //var q =
            //    UserOrgRoles.AsEnumerable()
            //        .Where(
            //            i =>
            //                ConvertUtility.ConvertToGuid(Convert.ToString(i["AccountID"]), Guid.Empty).Equals(accountID))
            //        .ToList();
            //if (q.Any())
            //{
            //    lcalErrMsg.Text = "User already in this organization.";
            //    return;
            //}
            

            Lib.Security.Sec_UserMembership actingUser = LoginUtility.GetCurrentUserOrSignIn();
            Lib.BLL.BLLSec_Role.Insert(MembershipId, accountID,
                Lib.Security.Sec_RoleCode.RoleUser,
                WebUtility.GetUserIP(),
                actingUser.Email,
                actingUser.FullName);
            if (MembershipId.IsNullOrEmptyGuid()) return;//new user
            Lib.Security.Sec_UserMembership user = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(MembershipId);
            var addnewuser = new ConnectUserAddNew();
            addnewuser.SendEmails(user, accountID);
            WebUtility.HttpRedirect(null, Request.RawUrl);
        }

        public string StaticResourceClassKey
        {
            get { return "ADM_STPG_SiteAdminUserDetail"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(StaticResourceClassKey, resourceKey));
        }
    }
}