﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ConnectUserSearch.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.ConnectUserSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <anrui:GlobalWebBoxedPanel ID="pnlSearchBy" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminUserSearch,pnlSearchBy.HeaderText %>"
        DefaultButton="bttSearchUser">
        <div class="settingrow group">
            <span class="settinglabelrgt-15">
                <asp:CheckBox ID="cbxSearchByEmail" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,cbxSearchByEmail.Text %>"
                    TextAlign="Left" /></span>
            
        </div>
        <div class="group input-text width-60">
            <asp:TextBox ID="txtSearchByEmail" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:CheckBox ID="cbxSearchByName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,cbxSearchByName.Text %>"
                    TextAlign="Left" /></span>
        </div>
        <div class="group input-text width-60">
            <asp:TextBox ID="txtSearchByName" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:CheckBox ID="cbxSearchByOrgName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,cbxSearchByOrgName.Text %>"
                    TextAlign="Left" /></span>
        </div>
        <div class="group input-text width-60">
            <asp:TextBox ID="txtSearchByOrgName" runat="server" SkinID="tbx-300"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:CheckBox ID="cbxSearchByCompanyName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,cbxSearchByCompanyName.Text %>"
                    TextAlign="Left" /></span>
        </div>
        <div class="group input-text width-60">
            <asp:TextBox ID="txtSearchByCompanyName" runat="server"></asp:TextBox>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:CheckBox ID="cbxSearchByAdmin" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,cbxSearchByAdmin.Text %>"
                    TextAlign="Left" /></span>
            
        </div>
        <div class="group width-60">
            <asp:RadioButton ID="rdoSearchByAdminYes" runat="server" GroupName="SearchByAdmin"
                Text="yes" Checked="true" />
            <span>&nbsp;</span>
            <asp:RadioButton ID="rdoSearchByAdminNo" runat="server" GroupName="SearchByAdmin"
                Text="no" />
        </div>
        <div class="settingrow margin-top-15">
            <asp:Button ID="bttSearchUser" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminUserSearch,bttSearchUser.Text %>"
                OnClick="bttSearchUser_Click" />
            <asp:HyperLink ID="hlAddNewUser" runat="server" Text='<%$Resources:ADM_STPG_SiteAdminUserSearch,hlAddNewUser.Text %>'
                NavigateUrl="~/siteadmin/orguseradmin/connectuseraddnew" SkinID="button"></asp:HyperLink>
        </div>
        <div class="settingrow">
            <span class="msg">
                <asp:Literal ID="ltrMsg" runat="server"></asp:Literal></span>
        </div>
        <div class="settingrow">
            <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminUserSearch"
                EnableViewState="false" />
        </div>
    </anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
