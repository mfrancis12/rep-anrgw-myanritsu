﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    

    public partial class ConnectUserSearch : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitUserSearchOptions();
            }
        }

        private void InitUserSearchOptions()
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.UserSearchOptionsGet(false);
            if (searchOptions == null || searchOptions.Count < 1) return;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "MembershipID":
                        break;
                    case "EmailAddress":
                        cbxSearchByEmail.Checked = true;
                        txtSearchByEmail.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "LastNameOrFirstName":
                        cbxSearchByName.Checked = true;
                        txtSearchByName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "OrgName":
                        cbxSearchByOrgName.Checked = true;
                        txtSearchByOrgName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        cbxSearchByCompanyName.Checked = true;
                        txtSearchByCompanyName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "IsAdministrator":
                        cbxSearchByAdmin.Checked = true;
                        rdoSearchByAdminYes.Checked = ConvertUtility.ConvertToBoolean(sbo.SearchObject, true);                        
                        break;
                }
            }
        }

        protected void bttSearchUser_Click(object sender, EventArgs e)
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.UserSearchOptionsGet(true);
            if (cbxSearchByEmail.Checked) searchOptions.Add(new SearchByOption("EmailAddress", txtSearchByEmail.Text.Trim()));
            if (cbxSearchByName.Checked) searchOptions.Add(new SearchByOption("LastNameOrFirstName", txtSearchByName.Text.Trim()));
            if (cbxSearchByOrgName.Checked) searchOptions.Add(new SearchByOption("OrgName", txtSearchByOrgName.Text.Trim()));
            if (cbxSearchByCompanyName.Checked) searchOptions.Add(new SearchByOption("CompanyName", txtSearchByCompanyName.Text.Trim()));
            if (cbxSearchByAdmin.Checked) searchOptions.Add(new SearchByOption("IsAdministrator", rdoSearchByAdminYes.Checked ? true : false));

            if (searchOptions.Count < 1)
            {
                ltrMsg.Text = GetStaticResource("MSG_SearchOptionRequired");
                return;
            }

            Admin_SearchUtility.UserSearchOptionsSet(searchOptions);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.AdminUserSearchResult);
        }


        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STPG_SiteAdminConnectUserSearch"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion
    }
}