﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ConnectUsers.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.ConnectUsers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow" style="overflow: scroll; width: 100%; min-height: 500px;">

        <dx:ASPxGridView ID="gvUsers" ClientInstanceName="gvUsers" runat="server" Theme="AnritsuDevXTheme"  OnRowDeleting="gvUsers_OnRowDeleting"
            OnDetailRowExpandedChanged="gvUsers_OnDetailRowExpandedChanged" OnBeforePerformDataSelect="gvUsers_OnBeforePerformDataSelect"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="MembershipId" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="MembershipId" VisibleIndex="0" Visible="false"></dx:GridViewDataColumn>
                 <dx:GridViewDataColumn  VisibleIndex="1" Caption="EmailAddress">
                                  <DataItemTemplate>
                                       <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text='<%#Eval("EmailAddress") %>' NavigateUrl='<%#"connectuserdetail?memid="+Eval("MembershipId") %>'/>
                                      </DataItemTemplate>
                            </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="TotalOrg" VisibleIndex="2" Caption="Org" Width="250"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="LastName" VisibleIndex="3" Caption="LastName" Width="300"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="FirstName" VisibleIndex="4" Caption="FirstName" Width="300"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="Last Login (UTC)" VisibleIndex="5" Caption="Last Login (UTC)" Width="50">
                    <DataItemTemplate>
                        <%# GetDateTime(Eval("LastLoginUTC")) %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>

                <dx:GridViewDataColumn FieldName="Last Activity (UTC)" VisibleIndex="6" Caption="Last Login (UTC)" Width="50">
                    <DataItemTemplate>
                        <%# GetDate(Eval("LastActivityUTC")) %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>


                <dx:GridViewDataColumn FieldName="First Access" VisibleIndex="7" Caption="Last Login (UTC)" Width="50">
                    <DataItemTemplate>
                        <%# GetDate(Eval("CreatedOnUTC")) %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn FieldName="IsAdministrator" VisibleIndex="8" Caption="Is Super Admin" Width="50" />

            </Columns>
            <SettingsDetail ShowDetailRow="True" />
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="gvUserDetails" runat="server" Theme="AnritsuDevXTheme" KeyFieldName="FileID" OnPageIndexChanged="gvUserDetails_OnPageIndexChanged"
                        OnBeforePerformDataSelect="gvUserDetails_OnBeforePerformDataSelect" CssClass="full-width">
                        <Columns>
                            <dx:GridViewDataColumn  VisibleIndex="0" Width="60" >
                                  <DataItemTemplate>
                                       <dx:ASPxHyperLink runat="server" Text="Details" NavigateUrl='<%#"/siteadmin/orguseradmin/orgdetail?accid="+Eval("AccountID") %>'/>
                                      </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="AccountID" VisibleIndex="0" Width="20" Caption="Details" Visible="False"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="CompanyName" VisibleIndex="1" Width="20" Caption="CompanyName"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="AccountName" VisibleIndex="2" Width="20" Caption="Organization"></dx:GridViewDataColumn>
                           </Columns>
                    </dx:ASPxGridView>
                    
                   
                </DetailRow>
            </Templates>
            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsDetail ShowDetailRow="true" AllowOnlyOneMasterRowExpanded="true" />
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>



        
    </div>
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminUserSearchResults"
        EnableViewState="false" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
