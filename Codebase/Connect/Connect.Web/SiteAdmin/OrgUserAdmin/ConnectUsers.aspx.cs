﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;
using DevExpress.Web;
using DevExpress.Web.Data;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class ConnectUsers : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        //public Guid FilterMembershipID { get { return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.MemMembershipId], Guid.Empty); } }
        //public String FilterEmailAddress { get { return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.MemEmailAddress]); } }
        //public String FilterLastName { get { return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.MemLastName]); } }
        //public String FilterFirstName { get { return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[KeyDef.QSKeys.MemFirstName]); } }
        //public Boolean? FilterIsAdmin
        //{
        //    get
        //    {
        //        if (Request.QueryString[KeyDef.QSKeys.MemIsAdmin] == "0") return false;
        //        else if (Request.QueryString[KeyDef.QSKeys.MemIsAdmin] == "1") return true;
        //        else return null;
        //    }
        //}

        private DataTable _UserMembers;
        public DataTable UserMembers
        {
            get
            {
                if (_UserMembers == null)
                {
                    _UserMembers = Lib.BLL.BLLSec_UserMembership.AdminUserSearch(Admin_SearchUtility.UserSearchOptionsGet(false));
                }
                return _UserMembers;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                gvUsers.DataBind();

            }
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }
        public String GetDateTime(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
         
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString();
        }

        


        protected void gvUsers_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            
        }

        protected void gvUsers_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvUsers.DataSource = UserMembers;
        }

        protected void gvUserDetails_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            //Rebind the details grid
            detailGrid.DataBind();
        }

        protected void gvUserDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var membershipId = (Guid)detailGrid.GetMasterRowKeyValue();
            var details = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipId(membershipId);
            detailGrid.DataSource = details;
        }

        protected void gvUsers_OnDetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvUserDetails = gvUsers.FindDetailRowTemplateControl(e.VisibleIndex, "gvUserDetails") as ASPxGridView;
                if (gvUserDetails != null)
                {
                    gvUserDetails.DataBind();
                }
            }
        }

        protected void gvUsers_OnRowDeleting(object sender, ASPxDataDeletingEventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var membershipId = (Guid)detailGrid.GetMasterRowKeyValue();
            Lib.BLL.BLLSec_UserMembership.Delete(membershipId);
        }
    }
}