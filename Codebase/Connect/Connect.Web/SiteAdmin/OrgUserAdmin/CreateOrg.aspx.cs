﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class CreateOrg : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageCompany)) throw new HttpException(403, "Forbidden");
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}