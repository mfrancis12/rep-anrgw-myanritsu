﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" ValidateRequest="false"
    AutoEventWireup="true" CodeBehind="OrgDetail.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.OrgDetail" %>


<%@ Register Src="~/App_Controls/AccountAdmin_DetailCtrl.ascx" TagName="AccountAdmin_DetailCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/App_Controls/Admin_Org/OrgAdmin_TabsCtrl.ascx" TagPrefix="uc3" TagName="OrgAdmin_TabsCtrl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc3:OrgAdmin_TabsCtrl runat="server" ID="OrgAdmin_TabsCtrl1" />
    <div id="tabContent" style="min-height: 300px; margin: 10px;color: Black;">
        <div class="settingrow">
        <uc1:AccountAdmin_DetailCtrl ID="AccountAdmin_DetailCtrl1" runat="server" />
    </div>    
    </div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
