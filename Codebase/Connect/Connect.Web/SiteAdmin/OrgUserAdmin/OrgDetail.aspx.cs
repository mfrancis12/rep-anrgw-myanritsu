﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class OrgDetail : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public Guid AccountID { get { return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID], Guid.Empty); } }

        protected override void OnInit(EventArgs e)
        {
            AccountAdmin_DetailCtrl1.AccountID = AccountID;
            AccountAdmin_DetailCtrl1.AllowAddNewAccount = true;
            
            if (AccountID.IsNullOrEmptyGuid())
            {
                throw new HttpException(404, "Page not found.");
            }
            

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               

            }
        }



    }
}