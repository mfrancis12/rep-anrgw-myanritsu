﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="OrgDetail_Users.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.OrgDetail_Users" %>

<%@ Register Src="~/App_Controls/Admin_Org/OrgAdmin_TabsCtrl.ascx" TagPrefix="uc3" TagName="OrgAdmin_TabsCtrl" %>
<%@ Register Src="~/App_Controls/Admin_Org/OrgAdmin_UsersCtrl.ascx" TagPrefix="uc1" TagName="OrgAdmin_UsersCtrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc3:OrgAdmin_TabsCtrl runat="server" ID="OrgAdmin_TabsCtrl1" />
    <div id="tabContent" style="min-height: 300px; margin: 10px;color: Black;">
        <div class="settingrow">
        <uc1:OrgAdmin_UsersCtrl ID="OrgAdmin_UsersCtrl1" runat="server" />
    </div>    
    </div>
</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
     <script type="text/javascript" language="javascript" defer="defer">
         function deleteRow(rowId) { cadgUsers.deleteItem(cadgUsers.getItemFromClientId(rowId)); }
         function cadgUsers_onItemBeforeDelete(sender, eventArgs) { if (!confirm('Are you sure you want to delete?')) eventArgs.set_cancel(true); }
    </script>
</asp:Content>
