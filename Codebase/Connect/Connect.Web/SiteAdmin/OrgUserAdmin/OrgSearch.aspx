﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="OrgSearch.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.OrgSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<anrui:GlobalWebBoxedPanel ID="pnlSearchBy" runat="server" ShowHeader="true" HeaderText="<%$Resources:ADM_STPG_SiteAdminOrgSearch,pnlSearchBy.HeaderText %>" DefaultButton="bttSearchUser">
<div class="settingrow">
    <p><asp:CheckBox ID="cbxSearchByAccountID" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByAccountID.Text %>" TextAlign="Left" /></p>
</div>
    <div class="width-60 input-text group">
        <asp:TextBox ID="txtSearchByAccountID" runat="server"></asp:TextBox>
    </div>
<div class="settingrow">
    <p><asp:CheckBox ID="cbxSearchByOrgName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByOrgName.Text %>" TextAlign="Left" /></p>
    
</div>
<div class="width-60 input-text group">
    <asp:TextBox ID="txtSearchByOrgName" runat="server"></asp:TextBox>
</div>
<div class="settingrow">
    <p><asp:CheckBox ID="cbxSearchByCompanyName" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByCompanyName.Text %>" TextAlign="Left" /></p>
</div>
<div class="width-60 input-text group">
    <asp:TextBox ID="txtSearchByCompanyName" runat="server"></asp:TextBox>
</div>
<div class="settingrow group">
    <asp:CheckBox ID="cbxSearchByCompanyCountryCode" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByCompanyCountryCode.Text %>" TextAlign="Left" />
</div>
<div class="width-60 input-text group">
    <asp:TextBox ID="txtSearchByCompanyCountryCode" runat="server" ></asp:TextBox>
</div>
<div class="settingrow">
    <p><asp:CheckBox ID="cbxSearchByIsVerifiedAndApproved" runat="server" Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByIsVerifiedAndApproved.Text %>" TextAlign="Left" /></p>  
</div>
<div class="width-60 group">
    <asp:RadioButton ID="rdoIsVerifiedYes" runat="server" GroupName="rdogpIsVerified" Text="yes" Checked="true" />
    <span>&nbsp;</span>
    <asp:RadioButton ID="rdoIsVerifiedNo" runat="server" GroupName="rdogpIsVerified" Text="no" />
</div>
<div class="settingrow margin-top-15">
<asp:Button ID="bttSearch" runat="server" 
        Text="<%$Resources:ADM_STPG_SiteAdminOrgSearch,bttSearch.Text %>" 
        onclick="bttSearch_Click" />
</div>
<div class="settingrow">
    <span class="msg"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></span>
</div>
<div class="settingrow">
<anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STPG_SiteAdminOrgSearch" EnableViewState="false" />
</div>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
