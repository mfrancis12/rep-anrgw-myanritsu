﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class OrgSearch : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitSearchOptions();
            }
        }

        private void InitSearchOptions()
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.OrgSearchOptionsGet(false);
            if (searchOptions == null || searchOptions.Count < 1) return;
            foreach (SearchByOption sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "AccountID":
                        cbxSearchByAccountID.Checked = true;
                        txtSearchByAccountID.Text = ConvertUtility.ConvertEmptyGuidToEmptyString(sbo.SearchObject);
                        break;
                    case "OrgName":
                        cbxSearchByOrgName.Checked = true;
                        txtSearchByOrgName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyName":
                        cbxSearchByCompanyName.Checked = true;
                        txtSearchByCompanyName.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "CompanyCountryCode":
                        cbxSearchByCompanyCountryCode.Checked = true;
                        txtSearchByCompanyCountryCode.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "IsVerifiedAndApproved":
                        cbxSearchByIsVerifiedAndApproved.Checked = true;
                        Boolean isVerified = ConvertUtility.ConvertToBoolean(sbo.SearchObject, false);
                        rdoIsVerifiedYes.Checked = isVerified;
                        rdoIsVerifiedNo.Checked = !isVerified;
                        break;
                }
            }
        }

        protected void bttSearch_Click(object sender, EventArgs e)
        {
            List<SearchByOption> searchOptions = Admin_SearchUtility.OrgSearchOptionsGet(true);
            if (cbxSearchByAccountID.Checked) searchOptions.Add(new SearchByOption("AccountID", ConvertUtility.ConvertToGuid( txtSearchByAccountID.Text.Trim(), Guid.Empty)));
            if (cbxSearchByOrgName.Checked) searchOptions.Add(new SearchByOption("OrgName", txtSearchByOrgName.Text.Trim()));
            if (cbxSearchByCompanyName.Checked) searchOptions.Add(new SearchByOption("CompanyName", txtSearchByCompanyName.Text.Trim()));
            if (cbxSearchByCompanyCountryCode.Checked) searchOptions.Add(new SearchByOption("CompanyCountryCode", txtSearchByCompanyCountryCode.Text.Trim()));
            if (cbxSearchByIsVerifiedAndApproved.Checked) searchOptions.Add(new SearchByOption("IsVerifiedAndApproved", rdoIsVerifiedYes.Checked ? true : false));
            if (searchOptions == null || searchOptions.Count < 1)
            {
                ltrMsg.Text = GetStaticResource("MSG_SearchOptionRequired");
                return;
            }
            Admin_SearchUtility.OrgSearchOptionsSet(searchOptions);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdminPages.Admin_OrgPages.OrgSearchResult);
        }

        #region IStaticLocalizedCtrl Members

        public string StaticResourceClassKey
        {
            get { return "ADM_STPG_SiteAdminOrgSearch"; }
        }

        public string GetStaticResource(string resourceKey)
        {
            return GetGlobalResourceObject(StaticResourceClassKey, resourceKey).ToString();
        }

        #endregion
    }
}