﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    Title="" CodeBehind="OrgUserHistory.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.OrgUserHistory" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <anrui:GlobalWebBoxedPanel ID="pnlContainerNewOrg" runat="server" ShowHeader="true"
        HeaderText="<%$ Resources:ADM_STCTRL_SiteAdminOrgUserHistory,pnlContainerNewOrg.HeaderText %>">
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewCompanyName" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalCompanyName.Text %>" />:</span>
            <asp:Literal ID="ltrNewComapanyName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewAccountName" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalAccountName.Text %>" />:</span>
            <asp:Literal ID="ltrNewAccountName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewCompanyAddress" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalCompanyAddress.Text %>" />:</span>
            <table style="vertical-align: top; text-align: left; margin:0">
                <tr>
                    <td>
                        <asp:Literal ID="ltrNewCompanyAddress" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlContainerOldOrg" runat="server" ShowHeader="true"
        HeaderText="<%$ Resources:ADM_STCTRL_SiteAdminOrgUserHistory,pnlContainerOldOrg.HeaderText %>">
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldCompanyName" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalCompanyName.Text %>" />:</span>
            <asp:Literal ID="ltrOldComapanyName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldAccountName" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalAccountName.Text %>" />:</span>
            <asp:Literal ID="ltrOldAccountName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldCompanyAddress" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalCompanyAddress.Text %>" />:</span>
            <table style="vertical-align: top">
                <tr>
                    <td>
                        <asp:Literal ID="ltrOldCompanyAddress" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlUserInfo" runat="server" ShowHeader="true" HeaderText="<%$ Resources:ADM_STCTRL_SiteAdminOrgUserHistory,pnlUserInfo.HeaderText %>">
        <div class="settingrow">
            <span class="settinglabelplain">
                <asp:Localize ID="lcalUserFullName" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalUserFullName.Text%>"></asp:Localize>:</span>
            <span>
                <asp:Literal ID="ltrUserFullName" runat="server"></asp:Literal>
            </span>
        </div>
        <div class="settingrow">
            <span class="settinglabelplain">
                <asp:Localize ID="lcalUserEmail" runat="server" Text="<%$Resources:ADM_STCTRL_SiteAdminOrgUserHistory,lcalUserEmail.Text%>"></asp:Localize>:</span>
            <asp:Literal ID="ltrUserEmail" runat="server"></asp:Literal>
        </div>
    </anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
