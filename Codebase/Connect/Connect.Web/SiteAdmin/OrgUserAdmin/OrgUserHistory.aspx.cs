﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class OrgUserHistory : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public Guid AccountID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.AccountID], Guid.Empty);
            }
        }

        public Guid MembershipID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.MemMembershipId], Guid.Empty);
            }
        }

        public Boolean IsAccountChanged
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[KeyDef.QSKeys.IsAccountChanged], false);
            }
        }

        public Boolean IsAddressChanged
        {
            get
            {
                return ConvertUtility.ConvertToBoolean(Request.QueryString[KeyDef.QSKeys.IsAddressChanged], false);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUserInfo();
            }
        }
        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminMode()) throw new HttpException(403, "Forbidden.");
            base.OnInit(e);
        }

        private void LoadUserInfo()
        {
            if (AccountID.IsNullOrEmptyGuid()) throw new HttpException(403, "Forbidden.");

            BindAccountDetails();
        }

        private void BindAccountDetails()
        {
            Lib.Account.Acc_AccountMaster newAcc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(AccountID);
            if (!MembershipID.IsNullOrEmptyGuid())
            {
                Lib.Security.Sec_UserMembership user = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(MembershipID);
                //Fix:  MA-1377 Remove Google translate api
                ltrUserFullName.Text = user.FullName;
                ltrUserEmail.Text = user.Email;
            }

            ltrNewComapanyName.Text = newAcc.CompanyName;
            ltrNewAccountName.Text = newAcc.AccountName;
            ltrNewCompanyAddress.Text = newAcc.CompanyAddress;

            if (IsAccountChanged)
            {
                Lib.Account.Acc_AccountMaster_History Oldacc = Lib.BLL.BLLAcc_AccountMasterHistory.SelectAccountHistoryByID(AccountID);
                if (Oldacc != null)
                {
                    ltrOldComapanyName.Text = Oldacc.CompanyName;
                    ltrOldAccountName.Text = Oldacc.AccountName;
                }
            }
            else
            {
                ltrOldComapanyName.Text = newAcc.CompanyName;
                ltrOldAccountName.Text = newAcc.AccountName;
            }

            if (IsAddressChanged)
            {
                ltrOldCompanyAddress.Text = Lib.BLL.BLLProfile_Address.SelectHistoryAsStringByAccountID(AccountID);
                //ltrOldCompanyAddress.Text = Oldacc.CompanyAddress;
            }
            else
            {
                ltrOldCompanyAddress.Text = newAcc.CompanyAddress;
            }
            //gvOldAccount.DataSource = Lib.BLL.BLLAcc_AccountMaster.SelectDetailsByAccountID(AccountID);
            //gvOldAccount.DataBind();
        }
    }
}