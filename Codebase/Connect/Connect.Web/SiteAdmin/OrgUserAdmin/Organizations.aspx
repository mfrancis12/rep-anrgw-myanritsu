﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="Organizations.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.Organizations" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div style="width: 100%;">
        <div class="settingrow">

            <dx:ASPxGridView ID="gvAccounts" ClientInstanceName="gvAccounts" runat="server" Theme="AnritsuDevXTheme"
                OnBeforePerformDataSelect="gvAccounts_OnBeforePerformDataSelect"
                Width="100%" AutoGenerateColumns="False" KeyFieldName="AccountID" SettingsBehavior-AutoExpandAllGroups="false">
                <Columns>
                    <dx:GridViewDataColumn FieldName="CompanyName" Caption="<%$Resources:STCTRL_CompanyProfileCtrl,lcalCompanyName.Text%>" />
                    <dx:GridViewDataColumn FieldName="CountryCode" Caption="<%$Resources:STCTRL_AddressCtrl,lcalCountryCode.Text%>" />
                    <dx:GridViewDataColumn FieldName="AccountName" Caption="<%$Resources:STCTRL_CompanyProfileWzCtrl,lcalAccountName.Text%>" />
                    <dx:GridViewDataColumn FieldName="AccountStatusCode" Caption="<%$Resources:ADM_STCTRL_ProdReg_AddNewCtrl,AccountStatusCode.Text%>" />
                    <dx:GridViewDataColumn FieldName="UserCount" Caption="<%$Resources:ADM_STCTRL_OrgAdmin_TabsCtrl,tbOrgDetailsUsers.Text%>" />
                    <dx:GridViewDataColumn FieldName="IsVerifiedAndApproved" Caption="<%$Resources:ADM_STPG_SiteAdminOrgSearch,cbxSearchByIsVerifiedAndApproved.Text%>" />
                    <dx:GridViewDataColumn Caption="<%$Resources:ADM_STCTRL_ProdReg_ListCtrl,CreatedOnUTC.Text%>">
                        <DataItemTemplate>
                            <dx:ASPxLabel runat="server" Text='<%# GetDate(Eval("CreatedOnUTC")) %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn >
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Details" NavigateUrl='<%#"orgdetail?accid="+Eval("AccountID") %>' />
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="AccountID" Caption="AccountID" Visible="false" />
                    <dx:GridViewDataColumn FieldName="CreatedOnUTC" Caption="CreatedOnUTC" Visible="false" />
                </Columns>
                <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
                <SettingsPager PageSize="30"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
    <anrui:StaticResourceEditPanel ID="srep" runat="server" StaticResourceClassKey="ADM_STCTRL_SiteAdminOrganizations"
        EnableViewState="false" />
</asp:Content>
