﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class Organizations : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private DataTable _Accounts;
        public DataTable Accounts
        {
            get
            {
                if (_Accounts == null)
                {
                    _Accounts = Lib.BLL.BLLAcc_AccountMaster.SearchFilteredResult(Admin_SearchUtility.OrgSearchOptionsGet(false));
                }
                return _Accounts;
            }
            set
            {
                _Accounts = value;
            }
        }


       

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
             gvAccounts.DataBind();


            }
        }

        
        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

      

        protected void gvAccounts_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvAccounts.DataSource = Accounts;
        }
    }
}