﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    Title="" CodeBehind="UserProfileHistory.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin.UserProfileHistory" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <anrui:GlobalWebBoxedPanel ID="pnlContainerNewOrg" runat="server" ShowHeader="true"
        HeaderText="<%$ Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,pnlContainerNewProfile.HeaderText %>">
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewEmail" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalEmail.Text %>" />:</span>
            <asp:Literal ID="ltrNewEmail" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewFirstName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalFirstName.Text %>" />:</span>
            <asp:Literal ID="ltrNewFirstName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewLastName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalLastName.Text %>" />:</span>
            <asp:Literal ID="ltrNewLastName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewMiddleName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalMiddleName.Text %>" />:</span>
            <asp:Literal ID="ltrNewMiddleName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewPhoneNumber" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalPhoneNumber.Text %>" />:</span>
            <asp:Literal ID="ltrNewPhoneNumber" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewFaxNumber" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalFaxNumber.Text %>" />:</span>
            <asp:Literal ID="ltrNewFaxNumber" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewJobTitle" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalJobTitle.Text %>" />:</span>
            <asp:Literal ID="ltrNewJobTitle" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewStreetAddress1" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalStreetAddress1.Text %>" />:</span>
            <asp:Literal ID="ltrNewStreetAddress1" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewStreetAddress2" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalStreetAddress2.Text %>" />:</span>
            <asp:Literal ID="ltrNewStreetAddress2" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewCity" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalCity.Text %>" />:</span>
            <asp:Literal ID="ltrNewCity" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewState" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalState.Text %>" />:</span>
            <asp:Literal ID="ltrNewState" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewZipCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalZipCode.Text %>" />:</span>
            <asp:Literal ID="ltrNewZipCode" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalNewCountryCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalCountryCode.Text %>" />:</span>
            <asp:Literal ID="ltrNewCountryCode" runat="server"></asp:Literal>
        </div>
    </anrui:GlobalWebBoxedPanel>
    <anrui:GlobalWebBoxedPanel ID="pnlContainerOldOrg" runat="server" ShowHeader="true"
        HeaderText="<%$ Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,pnlContainerOldProfile.HeaderText %>">
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldEmail" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalEmail.Text %>" />:</span>
            <asp:Literal ID="ltrOldEmail" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldFirstName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalFirstName.Text %>" />:</span>
            <asp:Literal ID="ltrOldFirstName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldLastName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalLastName.Text %>" />:</span>
            <asp:Literal ID="ltrOldLastName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldMiddleName" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalMiddleName.Text %>" />:</span>
            <asp:Literal ID="ltrOldMiddleName" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldPhoneNumber" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalPhoneNumber.Text %>" />:</span>
            <asp:Literal ID="ltrOldPhoneNumber" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldFaxNumber" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalFaxNumber.Text %>" />:</span>
            <asp:Literal ID="ltrOldFaxNumber" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldJobTitle" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalJobTitle.Text %>" />:</span>
            <asp:Literal ID="ltrOldJobTitle" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldStreetAddress1" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalStreetAddress1.Text %>" />:</span>
            <asp:Literal ID="ltrOldStreetAddress1" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldStreetAddress2" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalStreetAddress2.Text %>" />:</span>
            <asp:Literal ID="ltrOldStreetAddress2" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldCity" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalCity.Text %>" />:</span>
            <asp:Literal ID="ltrOldCity" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldState" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalState.Text %>" />:</span>
            <asp:Literal ID="ltrOldState" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldZipCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalZipCode.Text %>" />:</span>
            <asp:Literal ID="ltrOldZipCode" runat="server"></asp:Literal>
        </div>
        <div class="settingrow">
            <span class="settinglabelrgt-15">
                <asp:Localize ID="lcalOldCountryCode" runat="server" Text="<%$Resources:ADM_STCTRL_ProdRegAdmin_ListByAccountCtrl,lcalCountryCode.Text %>" />:</span>
            <asp:Literal ID="ltrOldCountryCode" runat="server"></asp:Literal>
        </div>
    </anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
