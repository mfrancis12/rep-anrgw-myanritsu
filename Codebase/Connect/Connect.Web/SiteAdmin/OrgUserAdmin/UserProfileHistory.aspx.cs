﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.OrgUserAdmin
{
    public partial class UserProfileHistory : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public Guid MembershipID
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.QSKeys.MemMembershipId], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUserInfo();
            }
        }

        private void LoadUserInfo()
        {
            if (MembershipID.IsNullOrEmptyGuid()) return;//new user
            BindOldProfile();
            BindNewProfile();
        }

        private void BindNewProfile()
        {
            Lib.Security.Sec_UserMembership usrInfo = Lib.BLL.BLLSec_UserMembership.SelectByMembershipId(MembershipID);
            ltrNewEmail.Text = usrInfo.Email;
            ltrNewFirstName.Text = usrInfo.FirstName;
            ltrNewLastName.Text = usrInfo.LastName;
            ltrNewMiddleName.Text = usrInfo.MiddleName;
            ltrNewPhoneNumber.Text = usrInfo.PhoneNumber;
            ltrNewFaxNumber.Text = usrInfo.FaxNumber;
            ltrNewJobTitle.Text = usrInfo.JobTitle;
            ltrNewStreetAddress1.Text = usrInfo.UserStreetAddress1;
            ltrNewStreetAddress2.Text = usrInfo.UserStreetAddress2;
            ltrNewCity.Text = usrInfo.UserCity;
            ltrNewState.Text = usrInfo.UserState;
            ltrNewZipCode.Text = usrInfo.UserZipCode;
            ltrNewCountryCode.Text = usrInfo.UserAddressCountryCode;
                       
        }

        private void BindOldProfile()
        {
            Lib.Security.Sec_UserMembership usrHistoryInfo = Lib.BLL.BLLSec_UserMembership.SelectHistoryByMembershipId(MembershipID);
            ltrOldEmail.Text = usrHistoryInfo.Email;
            ltrOldFirstName.Text = usrHistoryInfo.FirstName;
            ltrOldLastName.Text = usrHistoryInfo.LastName;
            ltrOldMiddleName.Text = usrHistoryInfo.MiddleName;
            ltrOldPhoneNumber.Text = usrHistoryInfo.PhoneNumber;
            ltrOldFaxNumber.Text = usrHistoryInfo.FaxNumber;
            ltrOldJobTitle.Text = usrHistoryInfo.JobTitle;
            ltrOldStreetAddress1.Text = usrHistoryInfo.UserStreetAddress1;
            ltrOldStreetAddress2.Text = usrHistoryInfo.UserStreetAddress2;
            ltrOldCity.Text = usrHistoryInfo.UserCity;
            ltrOldState.Text = usrHistoryInfo.UserState;
            ltrOldZipCode.Text = usrHistoryInfo.UserZipCode;
            ltrOldCountryCode.Text = usrHistoryInfo.UserAddressCountryCode;
        }
    }
}