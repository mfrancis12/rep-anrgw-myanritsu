﻿
/*
 *Author: Kishore Kumar M
 *Created Date: 
 *Date Modified: 03/28/2014
 *Purpose: AWS S3 objects browsing purpose
 */

using System;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.UI;

namespace Anritsu.Connect.Web.SiteAdmin.PackageAdmin
{
    public partial class BrowsePackage : BrowsePackageBase
    {
        public BrowsePackage()
        {
            BasePath = ConfigUtility.AppSettingGetValue("TAUDL.AWS.S3.EU.BasePath");
            AwsConfigType = "TAUDL";
            DlActiveRegion = "EU";
            DownloadRegion = "TAU";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var onjParent = new TreeNode(BasePath, BasePath) {PopulateOnDemand = true};
                tvBrowsePackages.Nodes.Add(onjParent);
                tvBrowsePackages.CollapseAll();
            }
            tvBrowsePackages.TreeNodeExpanded += new TreeNodeEventHandler(tvBrowsePackages_TreeNodeExpanded);
            tvBrowsePackages.SelectedNodeChanged += new EventHandler(tvBrowsePackages_SelectedNodeChanged);
        }

        protected void tvBrowsePackages_SelectedNodeChanged(object sender, EventArgs e)
        {
            txtPackagePath.Text = tvBrowsePackages.SelectedValue;
        }

        protected void tvBrowsePackages_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            AddNodes(e.Node.Value, e.Node);
        }
    }
}