﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/DocHeaderPlainMaster.Master" AutoEventWireup="true" CodeBehind="BrowsejpPackage.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.PackageAdmin.BrowseJpPackage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
	<style type="text/css">
		#_browseButton {
			border-radius: 3px;
			color: #FFFFFF;
			cursor: pointer;
			display: inline-block;
			font-family: arial;
			font-size: 13px;
			padding:2px 0;
			font-weight: bold;    
			text-align: center;
			vertical-align: top;
			width: 36px;
		}
		#_browseButton .arrow {
			background: url("//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/white_filter.png") no-repeat scroll -1px -76px rgba(0, 0, 0, 0);
			display: inline-block;
			height: 10px;
			width: 10px;
		}
	 </style>
     <script type="text/javascript">
         function SelectAndClose() {
             txtValue = document.getElementById('<%=txtPackagePath.ClientID%>');

             if (!window.showModalDialog) {
                 opener.filePath(txtValue.value);
             } else {
                 window.returnValue = txtValue.value;
             }
             window.close();
             return false;
         }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
     <div>
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="txtPackagePath" runat="server" CssClass="toolbar" Width="300px" />
                        <span id="_browseButton" onclick="SelectAndClose()" class="button">Go
                            <%--<span class="arrow"></span>--%>

                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tableOutlineWt" style="width: 345px; height: 300px; background-color: white">
                            <table cellspacing="0" cellpadding="4" width="100%" bgcolor="#ffffff" border="0">
                                <tr>
                                    <td>

                                        <asp:TreeView ID="tvBrowsePackages" runat="server" Height="300px" ImageSet="XPFileExplorer"
                                            NodeIndent="15" Width="292px" >
                                            <ParentNodeStyle Font-Bold="False" />
                                            <RootNodeStyle ImageUrl="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/folder.gif"   />
                                            <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
                                            <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px"
                                                VerticalPadding="0px" />
                                            <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black"  HorizontalPadding="2px"
                                                NodeSpacing="0px" VerticalPadding="2px" />
                                            <LeafNodeStyle ImageUrl="//dl.cdn-anritsu.com/images/legacy-images/apps/connect/img/folder.gif" />
                                        </asp:TreeView>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>

        </div>
</asp:Content>
