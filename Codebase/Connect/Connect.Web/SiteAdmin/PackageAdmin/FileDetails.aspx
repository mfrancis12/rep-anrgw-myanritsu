﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="FileDetails.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.PackageAdmin.FileDetails" %>

<%@ Register src="~/App_Controls/Admin_ManagePackages/PackageFileDetailCtrl.ascx" tagname="PackageFileDetailCtrl" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
   
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
     <uc1:PackageFileDetailCtrl ID="pfdetailcrtl" runat="server" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
