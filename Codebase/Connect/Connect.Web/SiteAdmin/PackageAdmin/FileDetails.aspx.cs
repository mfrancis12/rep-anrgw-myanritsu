﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;

namespace Anritsu.Connect.Web.SiteAdmin.PackageAdmin
{
    public partial class FileDetails : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private int FileId
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString[KeyDef.QSKeys.FileId], 0);
            }
        }
        /// <summary>
        /// create the preview link
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FileId != 0)
                {
                    //hlPreviewNotification.NavigateUrl = String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdminPages.NotificationAdmin_Preview, KeyDef.QSKeys.NotificationId, NotificationID);
                    //liPreviewNotification.Visible = true;
                }
            }
        }
    }
}