﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="PackageDetails.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.PackageAdmin.PackageDetails" %>

<%@ Register Src="~/App_Controls/Admin_ManagePackages/PackageAdminDetailCtrl.ascx" TagName="PackageAdmin_DetailCtrl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
    <uc1:PackageAdmin_DetailCtrl ID="PackageAdmin_DetailCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
