﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="PackageList.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.PackageAdmin.PackageList" %>

<%@ Register src="~/App_Controls/Admin_ManagePackages/Package_ListCtrl.ascx" tagname="PackageAdmin_ListCtrl" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphLeft_Bottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentTop" runat="server">
     <div class="cart-tabstrip-inner-wrapper" style="width: 100%; min-height: 100px;">
        <dx:ASPxTabControl ID="dxPackgeListTabs" runat="server" Theme="AnritsuDevXTheme" Width="100%" TabAlign="Left" EnableTabScrolling="true"></dx:ASPxTabControl>
        <div id="tabContent" style="min-height: 300px; padding: 10px; color: Black; border: 1px solid #9da0aa; border-top: 0px">
            <div class="settingrow"><uc1:PackageAdmin_ListCtrl ID="PackageAdmin_ListCtrl1" runat="server" /></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
