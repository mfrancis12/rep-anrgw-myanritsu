﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Data;

namespace Anritsu.Connect.Web.SiteAdmin.PackageAdmin
{
    public partial class SendEmail : System.Web.UI.Page
    {

        private Int64 PackageID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.PackageId], 0); }
        }


        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileEngg_UKTAU) && !LoginUtility.IsAdminRole(KeyDef.UserRoles.Admin_ManageFileCommEngg_UKTAU)) throw new HttpException(403, "Forbidden");
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucSendEmail.ToAddress =  ConfigUtility.AppSettingGetValue("TAU_DownloadSupport_ToEmailAddress");
                //load From address 
                ucSendEmail.FromAddress =ConfigUtility.AppSettingGetValue("TAU_DownloadSupport_FromEmailAddress");
                //load you BCC address
                ucSendEmail.BccAddress =GetBCCList(PackageID);
                //load default Body tempalte
                ucSendEmail.Body =GetGlobalResourceObject("~/siteadmin/packageadmin/SendEmail", "ESDSupportEmailContent.Text").ToString();
            }
        }

        public String GetBCCList(long packageId)
        {
            String emailList = string.Empty;
            if (packageId < 0) return emailList;
            //get the email list
            DataTable dtEmails = Lib.Package.PackageBLL.SelectAllEmailsByPackage(packageId);
            if (!dtEmails.IsNullOrEmpty())
            {
                for (int i = 0; i < dtEmails.Rows.Count; i++)
                {
                    if (dtEmails.Rows.Count != (i + 1))
                        emailList += ConvertUtility.ConvertToString(dtEmails.Rows[i]["EmailAddress"], "") + ";";
                    else
                        emailList += ConvertUtility.ConvertToString(dtEmails.Rows[i]["EmailAddress"], "");
                }
            }
            return emailList;
        }

      
    }
}