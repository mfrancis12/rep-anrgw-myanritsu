﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;

namespace Anritsu.Connect.Web.SiteAdmin.PackageAdmin
{
    public partial class SendEmailJP : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {

        private Int64 PackageID
        {
            get { return ConvertUtility.ConvertToInt32(Request.QueryString[App_Lib.KeyDef.QSKeys.PackageId], 0); }
        }


        protected override void OnInit(EventArgs e)
        {
            if (!LoginUtility.IsAdminRole(KeyDef.UserRoles.AdminManageJPSupportConfigAndPackages)) throw new HttpException(403, "Forbidden");
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucSendEmail.ToAddress = string.Empty;
                //load From address 
                ucSendEmail.FromAddress = string.Empty;
                //load you BCC address
                ucSendEmail.BccAddress = string.Empty;
                //load default Body tempalte
                ucSendEmail.Body = string.Empty;
            }
        }
    }
}