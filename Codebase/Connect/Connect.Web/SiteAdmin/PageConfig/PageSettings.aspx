﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="PageSettings.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.PageConfig.PageSettings" ValidateRequest="false" %>
<%@ Register src="~/App_Controls/SiteAdminSiteMapCtrl.ascx" tagname="SiteAdminSiteMapCtrl" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<asp:PlaceHolder runat="server">
<link href="<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/css/south-street/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet" />
        </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
<div id="tabs">
	<ul>
		<li><a href="#tabs-resources"><asp:Localize ID="lcalTabPageRes" runat="server" Text="<%$Resources:ThisPage,lcalTabPageRes.Text%>"></asp:Localize></a></li>
		<li><a href="#tabs-properties"><asp:Localize ID="lcalTabProperties" runat="server" Text="<%$Resources:ThisPage,lcalTabProperties.Text%>"></asp:Localize></a></li>
	</ul>
	<div id="tabs-resources">
        <asp:HyperLink ID="hlLocalizations" runat="server" Text="Localizations"></asp:HyperLink>
	</div>
    <div id="tabs-properties">
        <uc3:SiteAdminSiteMapCtrl ID="SiteAdminSiteMapCtrl1" runat="server" />
	</div>
</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
 <script type="text/javascript" defer="defer">
     (function () {
         function getScript(url, success) {
             var script = document.createElement('script');
             script.src = url;
             var head = document.getElementsByTagName('head')[0],
            done = false;
             script.onload = script.onreadystatechange = function () {
                 if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                     done = true;
                     success();
                     script.onload = script.onreadystatechange = null;
                     head.removeChild(script);
                 }
             };
             head.appendChild(script);
         }
         getScript('<%= Anritsu.Connect.Web.App_Lib.ConfigKeys.GwdataCdnPath %>/images/legacy-images/apps/connect/js/jquery-custom-v1.js', function () {
             $(function () { $("#tabs").tabs({ cookie: { expires: 30} }); });
             function SelectTab(tabName) { $("#tabs").tabs('select', tabName); }
         });
     })();
	</script>
    
</asp:Content>
