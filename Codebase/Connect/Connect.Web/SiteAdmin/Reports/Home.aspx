﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.Reports.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <%--<div class="settingrow">
<iframe frameborder=0 width='760' height='500' src='https://reports.zoho.com/ZDBDataSheetView.cc?OBJID=743878000003435803&STANDALONE=true&privatelink=e92203e64c7c7c6a933a9b06fa6404a8&WIDTH=760&HEIGHT=500&LP=RIGHT&ZDB_THEME_NAME=blue&REMTOOLBAR=false&INCLUDETITLE=true&INCLUDEDESC=true'></iframe>
</div>--%>
    <anrui:GlobalWebBoxedPanel ID="gwpnlReportHomeMenu" runat="server" HeaderText="Reports">
        <div class="settingrow">
            <asp:Localize ID="lcalReportHomeDesc" runat="server" Text='<%$ Resources:ADM_STCTRL_ReportsHome,lcalReportHomeDesc.Text %>'></asp:Localize>
        </div>
        <div class='siteadminmenu'>
            <ul>
                <li id="liZohoReportsMMDFeedback" runat="server" visible="false"><a class='lnkZohoReports' href="/siteadmin/reports/general/home">MMD Feedback</a></li>
                <li id="liZohoReportsUS" runat="server" visible="false"><a class='lnkZohoReports' href="/siteadmin/reports/us/home">US</a></li>
                 <li id="liZohoReportsTAU" runat="server" visible="false"><a class='lnkZohoReports' href="/siteadmin/reports/TAU/adminlog">TAU</a></li>
                <li id="liZohoReportsJP" runat="server" visible="false"><a class='lnkZohoReports' href="/siteadmin/reports/JP/jpdownloadclientaccesslog">JP</a></li>
                <%--<li id="liZohoReportsJP" runat="server" visible="true"><a class='lnkZohoReports' href="/siteadmin/reports/jp/home">Japan</a></li>--%>
            </ul>
        </div>
        <div class="clearfloat"></div>
    </anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
