﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
namespace Anritsu.Connect.Web.SiteAdmin.StaticLocale
{
    public partial class ManageStaticLocale : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        public String ClassKey
        {
            get
            {
                return ConvertUtility.ConvertNullToEmptyString(Request.QueryString[App_Lib.KeyDef.QSKeys.ResClassKey]);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //Example: https://my.anritsu.com/siteadmin/contentadmin/contenthome?srhmd=true&srhclass=STCTRL_ResourceLocalizationCtrl
            hlLocalizations.NavigateUrl = String.Format("{0}?{1}=true&{2}={3}", KeyDef.UrlList.SiteAdminPages.AdminContentSearch
                , KeyDef.QSKeys.AdminContentSearch_Auto
                , KeyDef.QSKeys.AdminContentSearch_PreClassKey
                , ClassKey);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}