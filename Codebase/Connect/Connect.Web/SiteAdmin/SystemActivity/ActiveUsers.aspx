﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master"
    AutoEventWireup="true" CodeBehind="ActiveUsers.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.SystemActivity.ActiveUsers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
    <div class="width-60 input-text group">
        <label>Last Activity in minutes:</label>
        <asp:TextBox ID="txtMins" runat="server" Text="60"></asp:TextBox>
    </div>
    <div class="width-60 group margin-top-15">
        <asp:Button ID="bttSubmit" runat="server" Text="search" OnClick="bttSubmit_Click" />
    </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Top" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="settingrow">

        <dx:ASPxGridView ID="gvActUsers" ClientInstanceName="gvActUsers" runat="server" Theme="AnritsuDevXTheme"
            OnBeforePerformDataSelect="gvActUsers_OnBeforePerformDataSelect" OnDetailRowExpandedChanged="gvActUsers_OnDetailRowExpandedChanged"
            Width="100%" AutoGenerateColumns="False" KeyFieldName="MembershipId" SettingsBehavior-AutoExpandAllGroups="false">
            <Columns>
                <dx:GridViewDataColumn FieldName="EmailAddress" Caption="EmailAddress" Width="150" VisibleIndex="1" />
                <dx:GridViewDataColumn FieldName="FullName" Caption="Full Name" Width="120" VisibleIndex="2" />
                <dx:GridViewDataColumn FieldName="LastActivityUTC" Caption="Last Activity (UTC)" Width="130" VisibleIndex="3" />

                <dx:GridViewDataColumn FieldName="LastLoginUTC" Caption="Last Login (UTC)" Width="140" VisibleIndex="4" />
                <dx:GridViewDataColumn FieldName="CreatedOnUTC" Caption="First Access" Width="80" VisibleIndex="5" />

                <dx:GridViewDataColumn FieldName="IsAdministrator" Caption="SuperAdmin" Width="100" VisibleIndex="6" />
                <dx:GridViewDataColumn FieldName="MembershipId" Caption="MembershipId" Visible="false" />
            </Columns>
            <SettingsDetail ShowDetailRow="True" />
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="gvActUserDetails" runat="server" Theme="AnritsuDevXTheme" KeyFieldName="AccountID" OnPageIndexChanged="gvActUserDetails_OnPageIndexChanged"
                        OnBeforePerformDataSelect="gvActUserDetails_OnBeforePerformDataSelect" CssClass="full-width">
                        <Columns>
                           <dx:GridViewDataColumn FieldName="IsSelectedAccount" VisibleIndex="0" Width="20" Caption="Selected" ></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="CompanyName" VisibleIndex="1" Width="20" Caption="Company"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn FieldName="AccountName" VisibleIndex="2" Width="20" Caption="Organization"></dx:GridViewDataColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </DetailRow>
            </Templates>

            <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowFooter="true" />
            <SettingsPager PageSize="30"></SettingsPager>
        </dx:ASPxGridView>
  
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
