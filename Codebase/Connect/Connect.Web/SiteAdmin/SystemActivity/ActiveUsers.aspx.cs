﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Lib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Utils;
using System.Globalization;
using DevExpress.Web;

namespace Anritsu.Connect.Web.SiteAdmin.SystemActivity
{
    public partial class ActiveUsers : App_Lib.UI.BP_MyAnritsu_SiteAdmin
    {
        private Int32 LastActiveMinutes
        {
            get
            {
                return ConvertUtility.ConvertToInt32(Request.QueryString["min"], 10);
            }
        }
        private Int32 _LastActiveMinutes = 0;
        private DataTable _UserMembers;
        public DataTable UserMembers
        {
            get
            {
                if (_UserMembers == null || _LastActiveMinutes != LastActiveMinutes)
                {
                    _UserMembers = Lib.BLL.BLLSec_UserMembership.LastActiveUsers(LastActiveMinutes);
                    _LastActiveMinutes = LastActiveMinutes;
                }
                return _UserMembers;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                txtMins.Text = LastActiveMinutes.ToString();
                gvActUsers.DataBind();
            }
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            WebUtility.HttpRedirectWithUpdatedQueryStrings(this, String.Format("min={0}", ConvertUtility.ConvertToInt32(txtMins.Text.Trim(), 10)));
        }

        public String GetDate(object dateTimeString)
        {
            if (String.IsNullOrEmpty(dateTimeString.ToString())) return String.Empty;
            String llCC = SiteUtility.BrowserLang_GetFromApp();
            CultureInfo culture = new CultureInfo(llCC);
            DateTime updatedt = Convert.ToDateTime(dateTimeString);

            return updatedt.ToString("d", culture);
        }

        protected void gvActUsers_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            gvActUsers.DataSource = UserMembers;
        }

        protected void gvActUserDetails_OnPageIndexChanged(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            //Rebind the details grid
            detailGrid.DataBind();
        }

        protected void gvActUserDetails_OnBeforePerformDataSelect(object sender, EventArgs e)
        {
            var detailGrid = (ASPxGridView)sender;
            var membershipId = (Guid)detailGrid.GetMasterRowKeyValue();
            detailGrid.DataSource = Lib.BLL.BLLAcc_AccountMaster.SelectByMembershipIdAsTable(membershipId);
        }

        protected void gvActUsers_OnDetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                var gvActUserDetails = gvActUsers.FindDetailRowTemplateControl(e.VisibleIndex, "gvActUserDetails") as ASPxGridView;
                if (gvActUserDetails != null)
                {
                    gvActUserDetails.DataBind();
                }
            }
        }
    }
}