﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="USBKeys.aspx.cs" Inherits="Anritsu.Connect.Web.SiteAdmin.USBKeyAdmin.USBKeys" %>
<%@ Register src="~/App_Controls/Admin_JPUSB/USBAdmin_TabsCtrl.ascx" tagname="USBAdmin_TabsCtrl" tagprefix="uc1" %>
<%@ Register src="~/App_Controls/Admin_JPUSB/USBAdmin_USBListCtrl.ascx" tagname="USBAdmin_USBListCtrl" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft_Middle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="cart-tabstrip-inner-wrapper" style="width:100%; min-height: 100px;">
    <uc1:USBAdmin_TabsCtrl runat="server" id="USBAdmin_TabsCtrl1" />
    <div id="Div1" style="min-height: 300px; margin: 10px;color: Black;">
        <div class="settingrow"><uc2:USBAdmin_USBListCtrl ID="USBAdmin_USBListCtrl1" runat="server" /></div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
