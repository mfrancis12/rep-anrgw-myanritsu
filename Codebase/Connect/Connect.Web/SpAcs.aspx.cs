﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Xml;
using System.Web.Security;
using Atp.Saml;
using Atp.Saml2;
using Atp.Saml2.Binding;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.Utils;
using Atp.Saml1;

namespace Anritsu.Connect.Web
{
    public partial class SpAcs : System.Web.UI.Page
    {

        public String WREPLY
        {
            get
            {
                String returnURL = Request.QueryString["wreply"];
                if (returnURL.IsNullOrEmptyString() || !WebUtility.IsWhiteListHost(new Uri(returnURL).Host)) returnURL = ConfigUtility.GetStringProperty("Connect.Web.BaseUrl", "~/");
                return returnURL;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
                // Process the SAML response returned by the identity provider in response
                // to the authentication request sent by the service provider.
                string relayState;
                Atp.Saml2.Response samlResponse = AnrSsoUtility.BuildAcsResponse(out relayState);
                if (samlResponse != null && samlResponse.IsSuccess())
                {
                    App_Lib.AnrSso.ConnectSsoUser user = AnrSsoUtility.CreateUserFromSamlResponse(samlResponse, true);
                    
                    if (user == null || user.MembershipId.IsNullOrEmptyGuid())
                        AnrSsoUtility.DoDefaultSignout();
                    else
                    {
                        var cookie = SecurityUtilities.ClientCacheEncrypter.EncryptString(
                        string.Format("{0}{1}{2}", user.GWUserId, "|", user.GWSecretKey));
                        //FormsAuthentication.SetAuthCookie(cookie, rememberme);
                        //FormsAuthentication.SetAuthCookie(user.MembershipId.ToString(), AnrSsoUtility.GetRememberMeCookieType(samlResponse));
                        FormsAuthentication.SetAuthCookie(cookie, AnrSsoUtility.GetRememberMeCookieType(samlResponse));
                        //set logged in flag to true
                        LoginUtility.SetLoggedInStatus(true);

                        string resourceUrl = GetResourceUrl(relayState);
                        AnrSsoUtility.LoggedInUser_Set(user);
                        Response.Redirect(resourceUrl, true);
                    }
                }
                else
                {
                    AnrSsoUtility.DoDefaultSignout();
                }
            }

       
        

        private String GetResourceUrl(String relayState)
        {
            #region " originally requested resource URL "
            // Get the originally requested resource URL from the relay state.
            string resourceUrl = String.Empty;
            try
            {
                //MA-381.  if the key is not in the cache container, it throws null key error.  
                // we don't have the source code so just ignore the error.
                resourceUrl = SamlSettings.CacheProvider.Remove(relayState) as string;
            }
            catch { }
            if (String.IsNullOrEmpty(resourceUrl))
            {
                String wreply = WREPLY;

                if (!String.IsNullOrEmpty(wreply)) resourceUrl = wreply;
                if (String.IsNullOrEmpty(resourceUrl))
                {
                    Trace.Write("ServiceProvider", "Nothing in cache");
                    resourceUrl = "/home";
                }
            }
            return resourceUrl;
            #endregion
        }
    }
}