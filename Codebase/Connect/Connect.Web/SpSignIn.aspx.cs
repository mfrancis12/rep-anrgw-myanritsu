﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib;
using Anritsu.Connect.Web.App_Lib.Providers;
using Atp.Saml;
using Atp.Saml2;
using Atp.Saml2.Binding;
using Anritsu.Connect.Web.App_Lib.AnrSso;

namespace Anritsu.Connect.Web
{
    public partial class SpSignIn : System.Web.UI.Page
    {
        public String ReturnURL
        {
            get
            {
                String returnURL = Request.QueryString[App_Lib.KeyDef.QSKeys.ReturnURL];
                if (returnURL.IsNullOrEmptyString()) returnURL = ConfigUtility.GetStringProperty("Connect.Web.URLConnectHome", Request.RawUrl);
                return returnURL;
            }
        }

        public bool IsNew
        {
            get
            {
                String isNew = Request.QueryString[KeyDef.QSKeys.IsNew];
                if (!isNew.IsNullOrEmptyString())
                {
                    if (Convert.ToBoolean(isNew)) return true;
                    else return false;
                }
                else return false;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            AnrSsoUtility.SamlLoginRequest_SendHttpPost(WebUtility.GetAbsoluteUrl(ReturnURL.Trim()), IsNew);
        }
    }
}