﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpSignOut.aspx.cs" Inherits="Anritsu.Connect.Web.SpSignOut" EnableTheming="false" EnableViewState="false" Theme=""  %>
<%@ Import Namespace="Anritsu.Connect.Web.App_Lib" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
     <asp:PlaceHolder runat="server">
    <link rel="shortcut icon" href="<%= ConfigKeys.GwdataCdnPath %>/appfiles/img/icons/favicon.ico" />
        </asp:PlaceHolder>
    <title>Processing..</title>    
    <script type="text/javascript">
        function showLoadingGif() { document.getElementById("divProcessing").style.display = 'inline'; } 
    </script>
</head>
<body onload="javascript:setTimeout(showLoadingGif, 10);">
    <center>
        <div id="divProcessing" style="display:none;"><img src="<%=ConfigKeys.GwdataCdnPath %>/images/gwstyle/loading.gif"  width="128" height="15" alt="Processing.." style='margin-top: 100px;' /></div>
    </center>
</body>
</html>