﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Security;
using Atp.Saml2;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.Connect.Web.App_Lib.AnrSso;
using Anritsu.Connect.Web.App_Lib.Utils;
namespace Anritsu.Connect.Web
{
    public partial class SpSignOut : System.Web.UI.Page
    {
        public String ReturnURL
        {
            get
            {               
                String returnURL = Request.QueryString[App_Lib.KeyDef.QSKeys.ReturnURL];
                if (returnURL.IsNullOrEmptyString() || !WebUtility.IsWhiteListHost(new Uri(returnURL).Host)) returnURL = ConfigUtility.GetStringProperty("AnrSso.Sp.LogoutDefaultReturnUrl", "~/");
                return returnURL;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            String wreply = Request[App_Lib.KeyDef.QSKeys.Sso.WReply].ConvertNullToEmptyString();
            String redirectUrl = wreply.IsNullOrEmptyString() || !WebUtility.IsWhiteListHost(new Uri(wreply).Host) ? ReturnURL : wreply;
            if (Request["idplgo"] == "logout")
            {
                //X509Certificate2 x509IdpCert = Global.GetIdpCertificate();
                //LogoutResponse logoutResponse = LogoutResponse.Create(Request, x509IdpCert.PublicKey.Key);
                //remove loginstate cookie
                LoginUtility.SetLoggedInStatus(false);
                Response.Redirect(redirectUrl, true);
            }
            else
            {
                ConnectSsoUser user = LoginUtility.GetCurrentUser(false);
                //remove loginstate cookie
                LoginUtility.SetLoggedInStatus(false);
                System.Web.Security.FormsAuthentication.SignOut();
                Session.Abandon();
                string spResourceUrl = WebUtility.GetAbsoluteUrl(ReturnURL.Trim());
                AnrSsoUtility.SamlLogoutRequest_SendHttpPost(spResourceUrl);
            }
        }
    }
}