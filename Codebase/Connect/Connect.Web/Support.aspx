﻿<%@ Page Language="C#" MasterPageFile="~/App_MasterPages/SiteDefault.Master" AutoEventWireup="true" CodeBehind="Support.aspx.cs" Inherits="Anritsu.Connect.Web.Support" %>
<%@ Register src="~/App_Controls/EndUser_General/HomeSupportResourcesCtrl.ascx" tagname="HomeSupportResourcesCtrl" tagprefix="uc1" %>
<%@ MasterType virtualpath="~/App_MasterPages/SiteDefault.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="google-site-verification" content="McLZlqM-VHK4D2vxk2IjqTOwCChZaao0CYn3xS1OJ1w" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft_Top" runat="server"></asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphLeft_Middle" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cphLeft_Bottom" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <uc1:HomeSupportResourcesCtrl ID="HomeSupportResourcesCtrl1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>


