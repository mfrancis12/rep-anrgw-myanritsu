﻿<% @Language= "VBScript" CodePage=65001 %>
<!-- #include file="../includes/SqlProc.asp" -->
<!-- #include file="../includes/Library.asp" -->
<!-- #include file="../includes/Languages/CategoryList.asp" -->
<%
'// ----------------------------------------------------------
'// 無償サイトカテゴリ一覧画面：CategoryList.asp
'// 作成：2006.09.06 AEK
'// 更新：2009/08/18 AAG1 六田和彦 Summaryが空白の時、行詰めをするように変更。
'// 更新：2010/03/24 AAG1 六田 「終了」ボタンを削除。
'// ----------------------------------------------------------
	Dim obConn						' Connectionオブジェクト
	Dim UserID						' ユーザID
	Dim LoginID						' ログインID
	Dim NewGif						' New画像マーク
	Dim ObjCategoryRs					' カテゴリ一覧のレコードセット
	Dim Today						' 現在の日付
	Dim Title						' カテゴリタイトル
	Dim Summary						' カテゴリ概要
	Dim Header						' カテゴリ見出し

	' 認証済でない場合はエラー画面へ遷移する。
	UserID = Session("UserID")
	If UserID = "" Then
		Response.Redirect "./StratError.asp"
	End If

	' 公開日及び契約期間の判定に用いる現在の日付を取得する。
	Today = Date()

	' 前画面で選択された機種名を取得する。
	ModelName = Request.Form("ModelName")

	' DB接続を開始する。
	Set obConn = OpenConnection()

	' カテゴリ表示文言を取得する。
	Dim categoryTitleSql
	categoryTitleSql = _
		"SELECT [Title], [TitleEng], [Summary], [SummaryEng], [Header], [HeaderEng]" _
		& " FROM [W2_CategoryTitle]" _
		& " WHERE [ModelName] = " & GetSqlStr(ModelName)
	Set obRs = openInfoRecord(categoryTitleSql)
	If obRs.eof Then
		Title = ""
		Summary = ""
		Header = ""
	Else
		Title = Server.HTMLEncode(NullToSp(IIf(Language = 0, obRs("Title"), obRs("TitleEng"))))
		Summary = Server.HTMLEncode(NullToSp(IIf(Language = 0, obRs("Summary"), obRs("SummaryEng"))))
		Summary = Replace(Summary, vbCrLf, "<br>")
		Header = Server.HTMLEncode(NullToSp(IIf(Language = 0, obRs("Header"), obRs("HeaderEng"))))
	End If
	obRs.Close
	Set obRs = Nothing

	' 当該機種の該非区分及び契約期間を取得する。
	Dim userModelSql
	userModelSql = _
		"SELECT [Target], [Contract]" _
		& " FROM [W2_UserModel]" _
		& " WHERE" _
		& " [UserID] = " & GetSqlStr(UserID) _
		& " AND [ModelName] = " & GetSqlStr(ModelName)
	Set obRs = openInfoRecord(userModelSql)
	' 該当する顧客登録モデルが存在しない場合は、ダウンロード対象無しとする。
	If obRs.eof Then
		obRs.Close
		Set obRs = Nothing
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If
	Target = obRs("Target")
	Contract = obRs("Contract")
	obRs.Close
	Set obRs = Nothing

	' 当該機種でダウンロード対象となるオブジェクトの該非区分を取得する。
	Dim targetCond
	targetCond = GetObjectTargetMService(Target, Contract, Today)
	If targetCond = "" Then
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If

	' カテゴリ一覧取得のためのSQLを作成する。
	Dim objectSql
	objectSql = _
		"SELECT * FROM [W2_Object]" _
		& " WHERE" _
		& " [ModelName] = " & GetSqlStr(ModelName) _
		& " AND [ReleaseDate] <= " & GetSqlDateTime(Today) _
		& " AND ([User] IS NULL OR [USER] = " & GetSqlStr(UserID) & ")" _
		& " AND " & targetCond
	Dim categorySql
	categorySql = _
		"SELECT [O1].[Category], MAX([O2].[UpDate]) AS [LastUpdate]" _
		& " FROM " _
		& " (SELECT DISTINCT [Category] FROM (" & objectSql & ") [OC] WHERE [OC].[Category] IS NOT NULL) [O1]" _
		& " INNER JOIN (" & objectSql & ") [O2]" _
		& " ON [O1].[Category] = [O2].[Category] OR [O2].[Category] IS NULL" _
		& " GROUP BY [O1].[Category]" _
		& " ORDER BY [O1].[Category] ASC"

	' カテゴリ一覧を取得する。
	Set ObjCategoryRs = openInfoRecord(categorySql)

	' カテゴリ一覧が取得できない場合は、ダウンロード対象無しとする。
	If ObjCategoryRs.RecordCount = 0 Then
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If

	' HTMLを出力する。
%>

<% If Language = 0 Then %>
	<!-- #include file = "../Includes/TopNav2.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eTopNav.html" -->
<% End If %>
<script language="JavaScript">
<!--
    function CategoryList(modelName, category) {
        document.form1.ModelName.value = modelName;
        document.form1.Category.value = category;
        document.form1.target = "_top";
        document.form1.method = "post";
        document.form1.action = "DLList.asp";
        document.form1.submit();
    }
// -->
</script>
	<form name="form1">
		<input type="hidden" name="ModelName">
		<input type="hidden" name="Category">
	</form>
	<table border="0" cellpadding="0" cellspacing="13" width="585">
		<tr><td>
			<div align="left"><Font class="title"><%= Title %></Font></div>
			<p>
			<center>
				<table border="0" width="550" cellspacing="0" cellpadding="3">
					<tr><td align="left" width="500" bgcolor="#6D8EC3" colspan="4"><b><font color="#FFFFFF"><%= ModelName %></font></b></td></tr>
					<tr><td align="left" colspan="4">&nbsp;</td></tr>
<% If Summary <> "" Then %>
					<tr><td align="left" colspan="4"><b><%= Summary %></font></td></tr>
					<tr><td align="left" colspan="4">&nbsp;</td></tr>
<% End If %>
				</table>
				<table border="1" cellspacing="0" cellpadding="10" width="550">
					<tr bgcolor="#e0e0e0">
						<td align="center"><b><%= Header %></b><br></td>
						<td align="center"><b><%= Item(4,Language) %></b><br></td>
					<tr>
					<% Call ResponseCategoryList() %>
				</table>
				<br>
				<form name="form2" method="post" action="End.asp">
					<input type="button" value="<%= Item(5,Language) %>" onclick="location.href='ModelList.asp'">
				</form>
			</center>
			<br><br><br>
		</td></tr>
	</table>

<% If Language = 0 Then %>
	<!-- #include file = "../Includes/BotNav.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eBotNav.html" -->
<% End If %>

<%
	' カテゴリ一覧取得のためのレコードセット及びDB接続を破棄する。
	ObjCategoryRs.Close
	obConn.Close
	Set ObjCategoryRs = Nothing
	Set obConn = Nothing

	' HTMLの出力を終了する。
	Response.End
%>

<%
'// ------------------------------------------------------
'// 名称：　ResponseCategoryList()
'// 機能：　カテゴリ一覧のHTMLをレスポンスする。
'// 引数：　なし
'// 作成：　2006.09.06 AEK
'// ------------------------------------------------------
Sub ResponseCategoryList()

	Dim category
	Dim update
	Dim newGif

	Do Until ObjCategoryRs.eof
		category = ObjCategoryRs("Category")
		update = ObjCategoryRs("LastUpdate")

		'// 更新日が14日以内ならばNew画像を表示する
		If update > Date() - 14 Then
			newGif = "<img src='../images/New01.gif' border='0'>&nbsp;"
		Else
			newGif = ""
		End If
%>
		<tr>
			<td align="center"><a href="JavaScript:onClick=CategoryList('<%= ModelName %>', '<%= category %>');"><%= category %></a></td>
			<td align="center"><%= newGif %><%= update %><br></td>
		</tr>
<%
		ObjCategoryRs.MoveNext
	Loop

End Sub
%>
