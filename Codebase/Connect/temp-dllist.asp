﻿<% @Language= "VBScript" CodePage=65001 %>
<!-- #include file="../includes/SqlProc.asp" -->
<!-- #include file="../includes/Library.asp" -->
<!-- #include file="../includes/Languages/DLList.asp" -->
<%
'// ----------------------------------------------------------
'// オブジェクトダウンロード画面：DLList.asp
'// 作成：2003.08.27 AAQ 六田
'// 更新：2004.01.08 AAQ 六田 MG3681A用バージョンマッチングテーブルポップアップ表示追加
'// 更新：2004.01.14 AAQ 六田 MS2681A,MS2683A,MS2687B用とMS8608A,MS8609A用のバージョンマッチングテーブルポップアップ表示追加
'// 更新：2004.01.21 AAQ 六田 上記の英語版を追加。
'// 更新：2004.02.26 AAQ 六田 MS8901Aのバージョンマッチングテーブルポップアップ表示追加（日本語版のみ）
'// 更新：2004.04.23 AAQ 六田 MD8480のマッチングテーブルのファイル名変更（該当、非該当共通となった）
'//                             VersionMatchingTable_Ciph_J.pdf,VersionMatchingTable_Ciph_E.pdf
'// 更新：2004.06.15 AAQ 六田 下記メッセージ表示を追加。
'//                            「必要なファイルのみワンクリックしダウンロードしてください。」
'//                             ※右クリックで表示される”対象をファイルに保存する”メニューはご使用できませんの
'//                             でご注意ください
'// 更新：2004.10.30 AAQ 六田 MT8820A用バージョンマッチングテーブルポップアップ表示追加
'// 更新：2004.11.09 AAQ 六田　更新日が３０日以内ならばNew画像を表示するを
'//                              更新日が１４日以内ならばNew画像を表示するに変更。
'// 更新：2005.01.06 AAQ 六田 MT8820,MS2681,MS2683,MS2687,MS8608,MS8609ポップアップ表示追加（MCP1齋藤）
'// 更新：2005.03.29 AAQ 六田 3/29(火)公開 MD8480C用バージョンマッチングテーブルポップアップ表示追加(MCB1藤島)
'// 更新：2005.04.18 AAQ 六田 5/9(月)公開 MT8510B用バージョンマッチングテーブルポップアップ表示追加(MCP1齋藤)
'// 更新：2005.06.27 AAQ 六田 公開 MG3700A用バージョンマッチングテーブルポップアップ表示追加(MCP1高橋英明)
'// 更新：2005.07.13 AAQ 六田 MS8911A用バージョンマッチングテーブルポップアップ表示追加(MCP1稲葉)
'// 更新：2005.07.14 AAQ 六田 MD8495Aバージョン表示設定追加（MBP1寺澤）
'// 更新：2005.07.19 AAQ 六田 MS8911A用コメント文変更。「ダウンロードする前に必ずお読みください。」[MS8911A_Filelist]（MCP1稲葉）
'// 更新：2005.11.11 AAQ 六田 バージョン表示機種追加（MD8391A、MD8395A、MD8496A）（MBP1寺澤）
'// 更新：2005.12.05 AAQ 六田 分割ページ(FileDownList.asp)から「戻る」で戻ると選択バージョン以外のバージョンが全て表示される不具合を修正。
'// 更新：2006.03.24 AAQ 六田 MS8911A用コメント文削除。（MCP1稲葉）
'// 更新：2006.04.04 AAQ 六田 MD8480_FOR_PTSポップアップ表示追加（MCP1齋藤）
'// 更新：2006.04.04 AAQ 六田 MT8510B_TEST_PARAMETER_FOR_NOKIAポップアップ表示追加（MCA1宇佐見）
'// 更新：2006.06.26 AAQ 六田 MT8510B_TEST_PARAMETER_FOR_SONY_ERICSSONポップアップ表示追加（MCA1宇佐見）
'// 更新：2006.08.17 AEK 選択中の言語に応じたcharsetを設定する。
'// 更新：2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。
'// 更新：2006.09.13 AEK 英語表示時の「・」文字化けの対応。
'// 更新：2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。
'// 更新：2009.08.20 AAG1 六田 「戻る」ボタンの戻り先を前画面にした。
'// 更新：2010/03/24 AAG1 六田 「終了」ボタンを削除。
'// 更新：2010.11.26 AEN1 小笠原 分割機能の除去
'// ----------------------------------------------------------
Dim obConn						'// Connectionオブジェエクト
Dim obRs						'// Recordsetオブジェクト
Dim obRs2						'// Recordsetオブジェクト
Dim strSQL						'// SQL文
Dim UserID						'// ユーザID
Dim ModelName						'// 機種名
Dim USBNo						'// USBキーのシリアル番号
Dim SoftTitle						'// ソフトタイトル
Dim SoftTitle_work					'// ソフトタイトルのワーク用
Dim FileName						'// ファイル名
Dim FileSize						'// ファイルサイズ
Dim SoftName						'// ソフト名
Dim DFolder						'// ダウンロード用TMPフォルダパス
Dim NewGif						'// New画像マーク
Dim strBODY						'// MD8084専用のポップアップURL
Dim strSoftTitle					'// MD8480専用の検索条件

'>>>> Add Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。
Dim Category					'// カテゴリ
Dim ObjectTargetCond			'// ダウンロード対象オブジェクトの該非区分
Dim Today						'// 現在日付
'<<<< Add End <<<<
'>>>> Add Start >>>> 2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。
Dim VersionFileName				'// バージョンマッチングテーブルのファイル名
Dim VersionFileMessage			'// バージョンマッチングテーブルが存在する場合のメッセージ
'<<<< Add End <<<<

	'// UserID認証チェック。UserIDセッションが無効の時はエラーメッセージを出力
		UserID = Session("UserID")
		If UserID = "" Then
			Response.Redirect "./StratError.asp"
		End If

	'// 機種名取得
	ModelName = Request.Form("ModelName")

'>>>> Add Start >>>> 2006.09.01 AEK カテゴリの追加
	Category = Request.Form("Category")
	Today = Date()
'<<<< Add End <<<<

'// 下記緊急停止処理解除 2005.2.10-------- AAQ 尾形
''// MT8820A 緊急停止処置-2005.1.12---------------------------------------------------
''// 確認の為、解除 MCP1 稲葉さん 2005.01.28 AAQ 六田和彦
'	If ModelName = "MT8820A" Then
'		Response.Redirect "./MT8820AError.asp"
'	End If
'
''// オブジェクト変更確認をする為にダミー形名を読みかえる。2005.01.28 AAQ 六田和彦
'	If ModelName = "TEST001" Then
'		ModelName = "MT8820A"
'	End If
'
'
''// MT8820A 緊急停止処置-終わり------------------------------------------------------
'// 緊急停止解除終了 2005.2.10 ------AAQ 尾形

	'// MD8480専用の検索条件取得
	strSoftTitle = Request.Form("SoftTitle")

	'// エラー処理を有効にする
		On Error Resume Next

	'// ポップアップ表示制御
'>>>> Del Start >>>> 2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。
' ※機種毎に個別で用意されていたbodyタグ用文字列生成処理を削除
'<<<< Del End

	'// TMPフォルダ削除
	DFolder = Server.Mappath("download\" & Session.SessionID)
	Set ObjFSO = Server.CreateObject("Scripting.FileSystemObject")
		'// TMPフォルダの存在確認
		If ObjFSO.FolderExists(DFolder) Then
			'// ダウンロード用TMPフォルダ削除
			ObjFSO.DeleteFolder DFolder,True
		End If
	Set ObjFSO = Nothing

	'// 有/無償 非該当技術のユーザ
	Set obConn = OpenConnection()

'>>>> Add Start >>>> 2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。

	' 当該機種で無償用のバージョンマッチングテーブルファイル情報を取得する。
	Dim versionFileSql
	versionFileSql = _
		"SELECT [FileName], [FileNameEng], [Message], [MessageEng]" _
		& " FROM [W2_VersionFile]" _
		& " WHERE" _
		& " [ModelName] = " & GetSqlStr(ModelName) _
		& " AND [ServiceType] = 0"
	Set obRs = openInfoRecord(versionFileSql)

	' 言語に応じたファイル名及び表示メッセージを取得する。
	VersionFileName = ""
	VersionFileMessage = ""
	If obRs.eof = False Then
		VersionFileName = IIf(Language = 0, obRs("FileName"), obRs("FileNameEng"))
		VersionFileMessage = IIf(Language = 0, obRs("Message"), obRs("MessageEng"))
	End If
	obRs.Close
	Set obRs = Nothing

	' 当該機種にバージョンマッチングテーブルファイルが存在する場合は、ポップアップ表示のためのbodyタグを作成する。
	strBODY = ""
	If VersionFileName <> "" Then
		strBODY = "window.open('" & VersionFileName & "','win','width=800,height=600,resizable=yes,scrollbars=yes');"
	End If

'<<<< Add End

'>>>> Mod Start >>>> 2006.09.01 AEK ファイルコメント、カテゴリ、オブジェクト公開日の追加。

	' 当該機種の該非区分及び契約期間を取得する。
	Dim userModelSql
	userModelSql = _
		"SELECT [Target], [Contract]" _
		& " FROM [W2_UserModel]" _
		& " WHERE" _
		& " [UserID] = " & GetSqlStr(UserID) _
		& " AND [ModelName] = " & GetSqlStr(ModelName)
	Set obRs = openInfoRecord(userModelSql)
	' 該当する顧客登録モデルが存在しない場合は、ダウンロード対象無しとする。
	If obRs.eof Then
		obRs.Close
		Set obRs = Nothing
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If
	Target = obRs("Target")
	Contract = obRs("Contract")
	obRs.Close
	Set obRs = Nothing

	' 当該機種でダウンロード対象となるオブジェクトの該非区分を取得する。
	ObjectTargetCond = GetObjectTargetMService(Target, Contract, Today)
	If ObjectTargetCond = "" Then
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If

	' 表示対象のオブジェクトが存在するか否かを確認するためのSQLを作成する。
	Dim objectCountSql
	objectCountSql = _
		"SELECT COUNT(*) AS [Count] FROM [W2_Object]" _
		& " WHERE" _
		& " [ModelName] = " & GetSqlStr(ModelName) _
		& " AND [ReleaseDate] <= " & GetSqlDateTime(Today) _
		& " AND ([User] IS NULL OR [USER] = " & GetSqlStr(UserID) & ")" _
		& " AND ([Category] IS NULL OR [CATEGORY] = " & GetSqlStr(Category) & ")" _
		& " AND " & ObjectTargetCond
	Set obRs = openInfoRecord(objectCountSql)
	Dim count
	count = obRs("Count")
	obRs.Close
	Set obRs = Nothing
	If count = 0 Then
		obConn.Close
		Set obConn = Nothing
		Response.Redirect "./NoList2.asp"
		Response.End
	End If

	obConn.Close
	Set obConn = Nothing

'	strSQL = "select * from W2_UserModel where UserID = '" & UserID & "' and ModelName ='" & ModelName & "' order by ModelName"
'	Set obRs = openInfoRecord( strSQL )
'
'	TmpCount = 0	'// ダウンロードできるオブジェクト数
'
'	If Not obRs.eof Then
'		ModelName = obRs("ModelName")
'		Target = obRs("Target")
'		Contract = obRs("Contract")
'	
'		If IsNull(Contract) or Contract >= Date() Then	'// サポート契約期間が有効な時
'			Select Case Target
'				Case "MH"	'// 無償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = '" & Target & "' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UH"	'// 有償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "MG"	'// 無償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UG"	'// 有償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'			End Select
'			TmpWork = 1
'		Else	'// サポート契約期間が無効な時
'			Select Case Target
'				Case "UH","MG","UG"	'// 有償非該当、無償該当、有償該当
'					TmpWork = 1
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case Else
'					TmpWork = 0
'			End Select
'		End If
'
'			If TmpWork > 0 Then
'				Set obRs2 = openInfoRecord( strSQL )
'				If obRs2.RecordCount > 0 Then
'					TmpCount = TmpCount + obRs2.RecordCount
'				End If
'				obRs2.Close
'			End If
'
'	End If
'
'	obRs.Close
'	obConn.Close
'	Set obRs   = Nothing
'	Set obConn = Nothing
'
'	If TmpCount = 0 Then	'// ダウンロードできるオブジェクトがなかった場合
'		Response.Redirect "./NoList2.asp"
'		Response.End
'	End If
'
'<<<< Mod End <<<<

'// ------------------------------------------------------
'// 名称：　SearchMain()
'// 機能：　W2_UserModel,W2_Objectテーブルからダウンロードできるファイル名を出力する。
'// 引数：　なし
'// 作成：　2003.08.28 AAQ 六田
'// ------------------------------------------------------
Sub SearchMain()

	Dim i '// ループカウント
		i = 0

'>>>> Add Start >>>> 2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。

	' 当該機種にバージョンマッチングテーブルファイル情報が存在する場合はメッセージを表示する。
	If VersionFileName <> "" And VersionFileMessage <> "" Then
%>
		<tr><td align="left" colspan="3">
			<font color="#c71585"><b><%= Replace(Server.HTMLEncode(VersionFileMessage), vbCrLf, "<br>") %></b></font>
			&nbsp;[<a href="<%= VersionFileName %>" target="_blank">Version Matching Table</a>]<BR>
		</td></tr>
<%
	End If

'<<<< Add End

'>>>> Del Start >>>> 2006.09.22 AEK バージョンマッチングテーブルファイル変更機能の追加。
' ※機種毎に個別で用意されていたhtmlを削除。
'<<<< Del End
%>
					<tr>
						<td align="left" colspan="3"><font color="#c71585"><b><% =Item(10,Language) %></font></td>
					</tr>
<%
					IF ModelName = "MD8470_CDMA2000" Then
						Response.Write "<tr>"
						'//Response.Write "<td align='left' colspan='3'><font color='navy'>" & ModelName  & "</font></td>"
						'//この部分に江川さん要求の文字列を挿入する。
						Response.Write "</tr>"
					End IF
%>
					<tr>
						<td align="left" colspan="3">&nbsp;</td>
					</tr>
<%

	Set obConn = OpenConnection()

	strSQL = "select * from W2_UserModel where UserID = '" & UserID & "' and ModelName = '" & ModelName & "' order by ModelName"
	Set obRs = openInfoRecord( strSQL )

	TmpCount = 0	'// ダウンロードできるオブジェクト数

	If Not obRs.eof Then
		ModelName = obRs("ModelName")
		Target = obRs("Target")
		Contract = obRs("Contract")

'>>>> Mod Start >>>>
		strSQL = _
			"SELECT [O].*, [C].[Comment], [C].[CommentEng]" _
			& " FROM [W2_Object] [O]" _
			& " LEFT OUTER JOIN [W2_Comment] [C]" _
			& " ON [O].[CommentID] = [C].[ID]" _
			& " WHERE" _
			& " [O].[ModelName] = " & GetSqlStr(ModelName) _
			& " AND ([User] IS NULL OR [User] = " & GetSqlStr(UserID) & ")" _
			& " AND [ReleaseDate] <= " & GetSqlDateTime(Today) _
			& " AND ([Category] IS NULL OR [CATEGORY] = " & GetSqlStr(Category) & ")" _
			& " AND " & ObjectTargetCond
		If (ModelName = "MD8480") or (ModelName = "MD8480C") or (ModelName = "MD8495A") or (ModelName = "MD8391A") or (ModelName = "MD8395A") or (ModelName = "MD8496A") Then
			strSQL = strSQL & " AND SoftTitle Like '%" & strSoftTitle & "%'"
		End If
		strSQL = strSQL & " ORDER BY [O].[SoftTitle], [O].[SoftName]"
'<<<< Mod End <<<<

			Set obRs2 = openInfoRecord( strSQL )
			SoftTitle_work = ""	'// ソフトタイトル比較用ワーク

			Do Until obRs2.eof
				SoftTitle = obRs2("SoftTitle")
				FileName = obRs2("FileName")
				SoftName = obRs2("SoftName")
				FileSize = obRs2("FileSize")
				Update = obRs2("Update")
'>>>> Add Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。
				ObjCategory = obRs2("Category")
'<<<< Add End <<<<

				'// 更新日が14日以内ならばNew画像を表示する
				If Update > Date()-14 Then
					NewGif = "&nbsp;<img src='../images/New01.gif' border='0'>"
				Else
					NewGif = ""
				End If

			If SoftTitle <> SoftTitle_work Then
%>
					<tr>
						<td align="left" width="5%">&nbsp;</td>

<%'>>>> Mod Start >>>> 2006.09.13 AEK 英語表示時の「・」文字化けの対応。 %>
						<td align="left" width="95%" valign="top" colspan="2"><b>&middot; <% =SoftTitle %></b></td>
<%'						<td align="left" width="95%" valign="top" colspan="2"><b>･ <% =SoftTitle ></b></td>
'<<<< Mod End <<<< %>
					</tr>
<%
			End If

			'// selectのnameに1をプラスする strSize1,strSize12
				i = i + 1

			'// ファイル容量によって低速ダウンロードの分割容量表示を切替える。
			If FileSize > 5 Then
%>
					<tr>
						<td align="left" width="5%">&nbsp;</td>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
						<td align="left" width="70%" valign="top">&nbsp;&nbsp;&nbsp;<a href="JavaScript:onClick=ObjectDL('<% =FileName %>','<% =SoftName %>','<%= ObjCategory %>');"><% =SoftName %>.zip (<% =FileSize %>MB)</a><% =NewGif %></td>
						<td align="left" width="25%" valign="top">&nbsp;</td>
					</tr>
<%
			ElseIF FileSize > 3 Then
%>
					<tr>
						<td align="left" width="5%">&nbsp;</td>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
						<td align="left" width="70%" valign="top">&nbsp;&nbsp;&nbsp;<a href="JavaScript:onClick=ObjectDL('<% =FileName %>','<% =SoftName %>','<%= ObjCategory %>');"><% =SoftName %>.zip (<% =FileSize %>MB)</a><% =NewGif %></td>
						<td align="left" width="25%" valign="top">&nbsp;</td>
					</tr>
<%
			ElseIF FileSize > 2 Then
%>
					<tr>
						<td align="left" width="5%">&nbsp;</td>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
						<td align="left" width="70%" valign="top">&nbsp;&nbsp;&nbsp;<a href="JavaScript:onClick=ObjectDL('<% =FileName %>','<% =SoftName %>','<%= ObjCategory %>');"><% =SoftName %>.zip (<% =FileSize %>MB)</a><% =NewGif %></td>
						<td align="left" width="25%" valign="top">&nbsp;</td>
					</tr>
<%
			Else
%>
					<tr>
						<td align="left" width="5%">&nbsp;</td>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
						<td align="left" width="70%" valign="top">&nbsp;&nbsp;&nbsp;<a href="JavaScript:onClick=ObjectDL('<% =FileName %>','<% =SoftName %>','<%= ObjCategory %>');"><% =SoftName %>.zip (<% =FileSize %>MB)</a><% =NewGif %></td>
<%'						<td align="left" width="70%" valign="top">&nbsp;&nbsp;&nbsp;<a href="JavaScript:onClick=ObjectDL('<% =FileName >','<% =SoftName >');"><% =FileName > (<% =FileSize >MB)</a><% =NewGif ></td>
'<<<< Mod End <<<< %>
						<td align="left" width="25%" valign="top">&nbsp;</td>
					</tr>
<%
			End If
'>>>> Add Start >>>> 2006.09.01 AEK ファイルのコメントの表示
			ResponseComment(obRs2)
'<<<< Add End
			obRs2.MoveNext
			If Not obRs2.eof Then SoftTitle_work = SoftTitle
		Loop
	End If

	obRs.Close						'// レコードセットを閉じる
	obConn.Close						'// ＤＢとの接続を閉じる
	Set obRs   = Nothing
	Set obConn = Nothing
End Sub

'>>>> Add Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。
'// ------------------------------------------------------
'// 名称：　ResponseComment(rs)
'// 機能：　コメントをレスポンスする。
'// 引数：　rs : 出力対象のオブジェクト情報を含むRecordSet
'// 作成：　2006.09.01 AEK
'// ------------------------------------------------------
Sub ResponseComment(rs)
	Dim comment
	If Language = 0 Then
		comment = rs("Comment")
	Else
		comment = rs("CommentEng")
	End If
	If IsNULL(comment) = True Or comment = "" Then
		Exit Sub
	End If
	comment = Replace(Server.HTMLEncode(comment), vbCrlf, "<br>")
%>
	<tr>
		<td align="left" width="5%">&nbsp;</td>
		<td align="left" colspan="2">
			<div style="border:black solid 1px; color:#ff3333; padding:3px; margin-left:10px; margin-right:10px; margin-bottom:5px;">
				<%= comment %>
			</div>
		</td>
	</tr>
<%
End Sub
'<<<< Add End <<<<

%>

<% If Language = 0 Then %>
	<!-- #include file = "../Includes/TopNav2.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eTopNav.html" -->
<% End If %>
<script language="javascript">
<!--

window.onload = function(){ <% =strBODY %> }

<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
function ObjectDL(FN,SN,Category){
	document.form1.FileName.value = FN;
	document.form1.SoftName.value = SN;
	document.form1.Category.value = Category;
	document.form1.target="_top";
	document.form1.method = "post";
	document.form1.action = "FileDown.asp";
	document.form1.submit();
}
function BunkatsuObject(w,FN,SN,Category){
	if(w == "") {
		return false;
	}

	if (confirm("<% =Item(4,Language) %>" + "\r" + "<% =Item(5,Language) %>")){
		shoriwindow();
		document.form1.FileName.value = FN;
		document.form1.SoftName.value = SN;
		document.form1.Category.value = Category;
		document.form1.strSize.value = w;
		document.form1.target="_top";
		document.form1.method = "post";
		document.form1.action = "FileDownList.asp";
		document.form1.submit();
	}
}
<%'function ObjectDL(FN,SN){
'	document.form1.FileName.value = FN;
'	document.form1.SoftName.value = SN;
'	document.form1.target="_top";
'	document.form1.method = "post";
'	document.form1.action = "FileDown.asp";
'	document.form1.submit();
'}
'function BunkatsuObject(w,FN,SN){
'	if(w == "") {
'		return false;
'	}
'
'	if (confirm("<% =Item(4,Language) >" + "\r" + "<% =Item(5,Language) >")){
'	shoriwindow();
'	document.form1.FileName.value = FN;
'	document.form1.SoftName.value = SN;
'	document.form1.strSize.value = w;
'	document.form1.target="_top";
'	document.form1.method = "post";
'	document.form1.action = "FileDownList.asp";
'	document.form1.submit();
'	}
'
'	}
'<<<< Mod End <<<< %>

	function shoriwindow()
                {
                mado=window.open("","shoriwin","width=200,height=200")
                mado.document.write("<html><body>")
                mado.document.write("<center><font size=5 color=red><b><% =Item(6,Language) %></b><br></center><br>")
                mado.document.write("<% =Item(7,Language) %><br></font>")
                mado.document.write("<br>")
                mado.document.write("</body></html>")
                }
//-->
</Script>
<form name="form1">
<input type="hidden" name="ModelName" value="<% =ModelName %>">
<input type="hidden" name="FileName">
<input type="hidden" name="SoftName">
<input type="hidden" name="strSize">
<input type="hidden" name="SoftTitle" value="<% =strSoftTitle %>">
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
<input type="hidden" name="Category">
<input type="hidden" name="SelectedCategory" value="<%= Category %>">
<%'<<<< Mod End <<<< %>

<table border="0" cellpadding="0" cellspacing="13" width="585">
	<tr>
		<td>
			<div align="left"><Font class="title"><% =Item(1,Language) %></Font></div>
			<p>
			<center>
				<table border="0" width="550" cellspacing="0" cellpadding="3">
					<tr>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
						<td align="left" width="500" bgcolor="#6D8EC3" colspan="3"><b><font color="#FFFFFF">&nbsp;<% =ModelNameReplace(ModelName) %>&nbsp;<%= Category %></font></b></td>
<%'						<td align="left" width="500" bgcolor="#6D8EC3" colspan="3"><b><font color="#FFFFFF">&nbsp;<% =ModelNameReplace(ModelName) ></font></b></td>
'<<<< Mod End <<<< %>
					</tr>

					<tr>
						<td align="left" colspan="3">&nbsp;</td>
					</tr>
					<% Call SearchMain() %>
				</table>
				</form>
				<br><br>
				<table width="200">
					<tr>
<%'>>>> Mod Start >>>> 2006.09.01 AEK コメント、カテゴリ、オブジェクト公開日の追加。%>
<%
	If Category = "" Then
%>
						<form name="form2" method="post" action="ModelList.asp">
<%
	Else
%>
						<form name="form2" method="post" action="CategoryList.asp">
							<input type="hidden" name="ModelName" value="<%= ModelName %>">
							<input type="hidden" name="Category" value="<%= Category %>">
<%
	End If
%>
<%'						<form name="form2" method="post" action="ModelList.asp">
'<<<< Mod End <<<< %>
						<td align="center">
								<input type="button" value="<% =Item(2,Language) %>" onclick="history.back(); return false;">
						</td></form>
					</tr>
				</table>

			</center>


</td>
</tr>
</table>

<% If Language = 0 Then %>
	<!-- #include file = "../Includes/BotNav.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eBotNav.html" -->
<% End If %>

