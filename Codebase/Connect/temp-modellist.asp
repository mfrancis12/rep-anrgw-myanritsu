﻿<% @Language= "VBScript" CodePage=65001 %>
<!-- #include file="../includes/SqlProc.asp" -->
<!-- #include file="../includes/Library.asp" -->
<!-- #include file="../includes/Languages/ModelList.asp" -->
<%
'// ----------------------------------------------------------
'// 顧客のダウンロード機種一覧画面：ModelList.asp
'// 作成：2003.08.23 AAQ 六田
'// 更新：2004.11.09 AAQ 六田　更新日が３０日以内ならばNew画像を表示するを
'//                              更新日が１４日以内ならばNew画像を表示するに変更。
'// 更新：2005.03.29 AAQ 六田　MD8480C用バージョン一覧へジャップするJavaScriptを追加。
'// 更新：2005.07.14 AAQ 六田　MD8495A用バージョン一覧へジャップするJavaScriptを追加。
'// 更新：2005.11.11 AAQ 六田　MD8391A,MD8395A,MD8496A用バージョン一覧へジャップするJavaScriptを追加。
'// 更新：2006.08.17 AEK 選択中の言語に応じたcharsetを設定する。
'// 更新：2006.08.30 AEK 顧客登録画面（機種追加モード）へのリンクを追加する。
'// 更新：2006.09.06 AEK 機種選択によりカテゴリ一覧画面へ遷移する。
'// 更新：2006.09.07 AEK オブジェクトの公開日の判定を追加。
'// 更新：2007.01.25 MD8391A,MD8395A,MD8495A,MD8496A用バージョン一覧へジャップするJavaScriptを解除。
'// 更新：2007.12.11 MD8480シリーズのユーザのみ、お知らせhttps://esd.eu.anritsu.com/リンクを表示する。
'// 更新：2007.12.17 MD8480_FOR_PTS,MD8480_FOR_RTD,MD8480C_FOR_PTS,MD8480C_FOR_RTDのユーザのみ、お知らせを表示する。
'// 更新：2009/08/18 AAG1 六田 Global Web SSO Web Serviceに対応。機種未登録でも表示する。
'// 更新：2010/03/24 AAG1 六田 「終了」ボタンを削除。
'// 更新：2010/05/11 AAG1 六田 Item(14）コメント追加。
'// ----------------------------------------------------------
Dim obConn						'// Connectionオブジェエクト
Dim obRs						'// Recordsetオブジェクト
Dim strSQL						'// SQL文
Dim UserID						'// ユーザID
Dim LoginID						'// ログインID
Dim Password						'// パスワード
Dim USBNo						'// USBキーのシリアル番号
Dim NewGif						'// New画像マーク
Dim TmpCount						'// ダウンロードできるオブジェクト数
Dim MD8480FLAG						'// MD8480シリーズのユーザはTRUE。

'Response.Write
'Response.End

	'// 変数初期化。
	MD8480FLAG = False

	'// UserID認証チェック。UserIDセッションが無効の時はエラーメッセージを出力
		UserID = Session("UserID")
		If UserID = "" Then
			Response.Redirect "./StratError.asp"
		End If

	'// 有/無償 非該当技術のユーザ
	Set obConn = OpenConnection()

	strSQL = "select * from W2_UserModel where UserID = '" & UserID & "' order by ModelName"
	Set obRs = openInfoRecord( strSQL )

	TmpCount = 0	'// ダウンロードできるオブジェクト数

	Do Until obRs.eof
		ModelName = obRs("ModelName")
		Target = obRs("Target")
		Contract = obRs("Contract")

'>>>> Mod Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。
		Dim today
		today = Date()

		Dim condSql
		condSql = GetObjectTargetMService(Target, Contract, today)
		If condSql <> "" Then
			strSQL = _
				"SELECT * FROM [W2_Object] WHERE [ModelName] = " & GetSqlStr(ModelName) _
				& " AND ([User] Is Null or [User] = " & GetSqlStr(UserID) & ")" _
				& " AND [ReleaseDate] <= " & GetSqlDateTime(today) _
				& " AND " & condSql _
				& " ORDER BY [UpDate] DESC"
			TmpWork = 1
		Else
			TmpWork = 0
		End If

'		If IsNull(Contract) or Contract >= Date() Then	'// サポート契約期間が有効な時
'			Select Case Target
'				Case "MH"	'// 無償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = '" & Target & "' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UH"	'// 有償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "MG"	'// 無償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UG"	'// 有償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'			End Select
'			TmpWork = 1
'		Else	'// サポート契約期間が無効な時
'			Select Case Target
'				Case "UH","MG","UG"	'// 有償非該当、無償該当、有償該当
'					TmpWork = 1
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case Else
'					TmpWork = 0
'			End Select
'		End If
'<<<< Mod End <<<<

			If TmpWork > 0 Then
				Set obRs2 = openInfoRecord( strSQL )
				If obRs2.RecordCount > 0 Then
					TmpCount = TmpCount + obRs2.RecordCount
				End If
				obRs2.Close
			End If

		obRs.MoveNext
	Loop

	obRs.Close
	obConn.Close
	Set obRs   = Nothing
	Set obConn = Nothing

	If TmpCount = 0 Then	'// ダウンロードできるオブジェクトがなかった場合
		'Response.Redirect "./NoList2.asp"
		'Response.End
	End If



'// ------------------------------------------------------
'// 名称：　SearchMain()
'// 機能：　W2_UserModel,W2_Objectテーブルからダウンロードできる機種、更新日、契約満了日を出力する。
'// 引数：　なし
'// 作成：　2003.08.23 AAQ 六田
'// ------------------------------------------------------
Sub SearchMain()

'>>>> Add Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。
	Dim countRs
	Dim today
	Dim targetCondSql
	Dim condSql
	today = Date()
'<<<< Add End <<<<

	Set obConn = OpenConnection()
	strSQL = "select * from W2_UserModel where UserID = '" & UserID & "' order by ModelName"
	Set obRs = openInfoRecord( strSQL )

	TmpCount = 0	'// ダウンロードできるオブジェクト数

	Do Until obRs.eof
		ModelName = obRs("ModelName")
		Target = obRs("Target")
		Contract = obRs("Contract")
'>>>> Mod Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。
		targetCondSql = GetObjectTargetMService(Target, Contract, today)
		If targetCondSql <> "" Then
			condSql = _
				"[ModelName] = " & GetSqlStr(ModelName) _
				& " AND ([User] Is Null or [User] = " & GetSqlStr(UserID) & ")" _
				& " AND [ReleaseDate] <= " & GetSqlDateTime(today) _
				& " AND " & targetCondSql
			strSQL = _
				"SELECT [ModelName], MAX([UpDate]) AS [UpDate] FROM [W2_Object]" _
				& " WHERE" _
				& condSql _
				& " GROUP BY [ModelName]"
			strCount = _
				"SELECT COUNT(*) AS [Cnt] FROM [W2_Object]" _
				& " WHERE" _
				& condSql _
				& " AND [Category] IS NOT NULL"
			TmpWork = 1
		Else
			TmpWork = 0
		End If

'		If IsNull(Contract) or Contract >= Date() Then	'// サポート契約期間が有効な時
'			Select Case Target
'				Case "MH"	'// 無償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = '" & Target & "' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UH"	'// 有償非該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "MG"	'// 無償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case "UG"	'// 有償該当
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and (Target = 'UH' or Target = 'MH') and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'			End Select
'			TmpWork = 1
'		Else	'// サポート契約期間が無効な時
'			Select Case Target
'				Case "UH","MG","UG"	'// 有償非該当、無償該当、有償該当
'					TmpWork = 1
'					strSQL = "select * from W2_Object where ModelName = '" & ModelName & "' and Target = 'MH' and ( [User] Is Null or [User] = '" & UserID & "') order by [UpDate] desc"
'				Case Else
'					TmpWork = 0
'			End Select
'		End If
'<<<< Mod End <<<<
			If TmpWork > 0 Then
				Set obRs2 = openInfoRecord( strSQL )
'>>>> Add Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。
				Set countRs = openInfoRecord(strCount)
'<<<< Add End <<<<
				If obRs2.RecordCount > 0 Then
					Update = obRs2("UpDate")
					'// 更新日が14日以内ならばNew画像を表示する
					If Update > Date()-14 Then
						NewGif = "<img src='../images/New01.gif' border='0'>&nbsp;"
					Else
						NewGif = ""
					End If

					If IsNull(Contract) or Contract = "" Then '// サポート契約満了日が未入力ならば”－”を表示する。
						Contract = "-"
					ElseIf Contract < Date() Then '// サポート契約満了日が過ぎていたら赤色表示する。
						Contract = "<font color=red><b>" & Contract & "</b></font>"
					End If

					If (ModelName = "MD8480_FOR_PTS") or (ModelName = "MD8480_FOR_RTD") or (ModelName = "MD8480C_FOR_PTS") or (ModelName = "MD8480C_FOR_RTD") Then
						MD8480FLAG = True
					End If

%>
					<tr>
<%'>>>> Mod Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。%>
						<td align="center" width="34%"><a href="JavaScript:onClick=ObjectList('<% =ModelName %>',<%= countRs("Cnt") %>);"><% =ModelNameReplace(ModelName) %></a></td>
<%'						<td align="center" width="34%"><a href="JavaScript:onClick=ObjectList('<% =ModelName >');"><% =ModelNameReplace(ModelName) ></a></td>
'<<<< Mod End <<<< %>
						<td align="center" width="33%"><% =NewGif %><% =Update %><br></td>
						<td align="center" width="33%"><% =Contract %><br></td>
					<tr>
<%
				End If
				obRs2.Close
'>>>> Add Start >>>> 2006.09.07 AEK オブジェクトの公開日の判定を追加。
				countRs.Close
'<<<< Add End <<<<
			End If
		obRs.MoveNext
	Loop


'Response.Write strSQL
'Response.End

	obRs.Close						'// レコードセットを閉じる
	obConn.Close						'// ＤＢとの接続を閉じる
	
	Set obRs   = Nothing
	Set obConn = Nothing
End Sub

%>


<% If Language = 0 Then %>
	<!-- #include file = "../Includes/TopNav2.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eTopNav.html" -->
<% End If %>
<script language="JavaScript">
<!--
<%'>>>> Mod Start >>>> 2006.09.06 AEK 機種選択によりカテゴリ一覧画面へ遷移する。%>
	function ObjectList(MN,count){
<%'	function ObjectList(MN){
'<<<< Mod End <<<< %>

	if (MN == "MD8480") {
		document.form1.ModelName.value = MN;
		document.form1.target="_top";
		document.form1.method = "post";
		document.form1.action = "MD8480Index.asp";
		document.form1.submit();
		return false;
	}
	if (MN == "MD8480C") {
		document.form1.ModelName.value = MN;
		document.form1.target="_top";
		document.form1.method = "post";
		document.form1.action = "MD8480CIndex.asp";
		document.form1.submit();
		return false;
	}
	//	if (MN == "MD8495A") {
	//		document.form1.ModelName.value = MN;
	//		document.form1.target="_top";
	//		document.form1.method = "post";
	//		document.form1.action = "MD8495AIndex.asp";
	//		document.form1.submit();
	//		return false;
	//	}
	//	if (MN == "MD8391A") {
	//		document.form1.ModelName.value = MN;
	//		document.form1.target="_top";
	//		document.form1.method = "post";
	//		document.form1.action = "MD8391AIndex.asp";
	//		document.form1.submit();
	//		return false;
	//	}
	//	if (MN == "MD8395A") {
	//		document.form1.ModelName.value = MN;
	//		document.form1.target="_top";
	//		document.form1.method = "post";
	//		document.form1.action = "MD8395AIndex.asp";
	//		document.form1.submit();
	//		return false;
	//	}
	//	if (MN == "MD8496A") {
	//		document.form1.ModelName.value = MN;
	//		document.form1.target="_top";
	//		document.form1.method = "post";
	//		document.form1.action = "MD8496AIndex.asp";
	//		document.form1.submit();
	//		return false;
	//	}

	document.form1.ModelName.value = MN;
	document.form1.target="_top";
	document.form1.method = "post";
<%'>>>> Mod Start >>>> 2006.09.06 AEK 機種選択によりカテゴリ一覧画面へ遷移する。%>
	if(count == 0) {
		document.form1.action = "DLList.asp";
	} else {
		document.form1.action = "CategoryList.asp";
	}
<%'	document.form1.action = "DLList.asp";
'<<<< Mod End <<<< %>
	document.form1.submit();

}
// -->
</script>
<form name="form1">
<input type="hidden" name="ModelName">
</form>
<table border="0" cellpadding="0" cellspacing="13" width="585">
	<tr>
		<td>
			<div align=left><Font class="title"><% =Item(1,Language) %></Font></div>
			<p>
			<center>
				<table border="1" cellspacing="0" cellpadding="10" width="550">
					<tr bgcolor="#e0e0e0">
						<td align="center" width="34%"><b><% =Item(2,Language) %></b><br></td>
						<td align="center" width="33%"><b><% =Item(3,Language) %></b><br></td>
						<td align="center" width="33%"><b><% =Item(4,Language) %></b><br></td>
					<tr>
					<% Call SearchMain() %>
				</table>
<%'>>>> Add Start >>>>  2006.08.29 AEK 顧客登録画面（機種追加モード）へのリンクを追加する。%>
			</center>
			<br><br>
			<% =Item(14, Language) %>
			<br><br>
			<div align=left><Font class="title"><% =Item(8, Language) %></Font></div>
			<font class="text">
			<% =Item(9, Language) %>&nbsp;<a href="Registration.asp?mode=add"><b>[<% =Item(10, Language) %>]</b></a>
			</font>
			</left>
<% If MD8480FLAG = True Then %>
			<br><br>
			<table border="1" cellspacing="0" cellpadding="5" width="100%">
			<tr bgcolor="#e0e0e0">
			<td align="left">
			<div align=left><Font class="title"><% =Item(11, Language) %></Font></div>
			<font class="text"><% =Item(12, Language) %></font>
			<font class="text"><% =Item(13, Language) %></font>
			</td>
			</tr>
			</table>
<% End If %>

</td>
</tr>
</table>

<% If Language = 0 Then %>
	<!-- #include file = "../Includes/BotNav.html" -->
<% Else %>
	<!-- #include file = "../eIncludes/eBotNav.html" -->
<% End If %>

