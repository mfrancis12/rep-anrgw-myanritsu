﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Amazon;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.CloudFront;
using Amazon.CloudFront.Model;

namespace Anritsu.ConnectS3UploadConsole
{
    public class ConnectS3Uploader
    {
        private Amazon.RegionEndpoint _DL1_S3Endpoint = Amazon.RegionEndpoint.APNortheast1;
        private Amazon.RegionEndpoint _DL2_S3Endpoint = Amazon.RegionEndpoint.APNortheast1;
        private String _DL1_S3BucketName;
        private String _DL2_S3BucketName;
        private String _DL1_S3KeyPrefix;
        private String _DL2_S3KeyPrefix;


        private String _AWSAccessKey;
        private String _AWSSecretKey;
        private Int32 _S3Tiemout;
        private TransferUtility _DL1_AwsTransferUtil;
        private TransferUtility _DL2_AwsTransferUtil;
        private String _BlowFishTempFolder;
        private String _BaseFolderPath;
        private List<FileInfo> _FilesToUpload;
        private Int32 _ThreadID = 0;

        public ConnectS3Uploader(Int32 treadID, List<FileInfo> filesToUpload, String baseFolderPath)
        {
            _BaseFolderPath = baseFolderPath;
            _FilesToUpload = filesToUpload;
            _ThreadID = treadID;
            _AWSAccessKey = ConfigurationManager.AppSettings["AWS.AWSAccessKey"];
            _AWSSecretKey = ConfigurationManager.AppSettings["AWS.AWSSecretKey"];
            _S3Tiemout = Int32.Parse(ConfigurationManager.AppSettings["AWS.S3.Timeout"]);

            _BlowFishTempFolder = ConfigurationManager.AppSettings["BFEncryptTempDir"];
            DirectoryInfo diBF = new DirectoryInfo(_BlowFishTempFolder);
            if (!diBF.Exists) diBF.Create();
            _BlowFishTempFolder = diBF.FullName;

            _DL1_S3Endpoint = Amazon.RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["DL1.S3Endpoint"]);
            _DL2_S3Endpoint = Amazon.RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["DL2.S3Endpoint"]);
            _DL1_S3BucketName = ConfigurationManager.AppSettings["DL1.BucketName"];
            _DL2_S3BucketName = ConfigurationManager.AppSettings["DL2.BucketName"];
            _DL1_S3KeyPrefix = ConfigurationManager.AppSettings["DL1.Prefix"];
            _DL2_S3KeyPrefix = ConfigurationManager.AppSettings["DL2.Prefix"];

            _DL1_AwsTransferUtil = new TransferUtility(_AWSAccessKey, _AWSSecretKey, _DL1_S3Endpoint);
            _DL2_AwsTransferUtil = new TransferUtility(_AWSAccessKey, _AWSSecretKey, _DL2_S3Endpoint); 
        }

        public void UploadToS3()
        {
            //System.Threading.Thread.Sleep(10000);
            //Console.WriteLine("Tread {0} completed.", _ThreadID);
            //return;
            if (_FilesToUpload == null || _FilesToUpload.Count < 1) return;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Connect.DB.ConnStr.JPDL"].ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("", conn);
                cmd.CommandText = "SELECT * FROM [dbo].[W2_Object] (NOLOCK) WHERE [ModelName] = @ModelName AND [FileName] = @FileName";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ModelName", "");
                cmd.Parameters.AddWithValue("@FileName", "");
                foreach (FileInfo fi in _FilesToUpload)
                {

                    try
                    {
                        String modelName = String.Empty;
                        String friendlyFileNameForUser = String.Empty;
                        DirectoryInfo diParent = fi.Directory;
                        if (diParent != null)
                        {
                            modelName = diParent.Name;
                        }
                        string targetFileName = fi.FullName.Replace(_BaseFolderPath, String.Empty);
                        String prefix = _DL2_S3KeyPrefix;
                        String bucket = _DL2_S3BucketName;

                        Boolean isBlowFishFile = fi.Extension.EndsWith(".bf", StringComparison.InvariantCultureIgnoreCase);
                        if (isBlowFishFile)
                        {
                            prefix = _DL1_S3KeyPrefix;
                            bucket = _DL1_S3BucketName;
                        }

                        #region " get friendly filename "
                        String softName = String.Empty;
                        cmd.Parameters["@ModelName"].Value = modelName;
                        cmd.Parameters["@FileName"].Value = fi.Name.Replace(".bf", "");
                        SqlDataReader rdr = cmd.ExecuteReader();
                        while(rdr.Read())
                        {
                            softName = rdr["SoftName"].ToString();
                        }
                        rdr.Close();
                        
                        if (!String.IsNullOrEmpty(softName))
                        {
                            String ext = Path.GetExtension(fi.FullName.Replace(".bf", ""));
                            friendlyFileNameForUser = String.Format("{0}{1}", softName, ext);
                        }
                        
                        #endregion
                       
                        if (isBlowFishFile)
                        {
                            //encrypted file
                            String tempEncryptedFilePath = Path.Combine(_BlowFishTempFolder, String.Format("{0}.bft", Guid.NewGuid()));
                            fi.CopyTo(tempEncryptedFilePath, true);

                            String tempDecryptedFilePath = DecryptBlowfishEncryptedFile(tempEncryptedFilePath);
                            File.Delete(tempEncryptedFilePath);

                            targetFileName = targetFileName.Replace(".bf", String.Empty);
                            
                            String awsS3ObjectName = GetAwsS3ObjectName(targetFileName, prefix);
                            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
                            .WithBucketName(bucket)
                            .WithFilePath(tempDecryptedFilePath)
                            .WithTimeout(_S3Tiemout)
                            .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)
                            .WithKey(awsS3ObjectName.ToLowerInvariant())
                            ;
                            if (!String.IsNullOrEmpty(friendlyFileNameForUser))
                            {
                                string dlmode = "attachment";
                                if (Path.GetExtension(tempDecryptedFilePath).Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dlmode = "inline";
                                }
                                request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Content-Disposition", String.Format("{0};filename={1}", dlmode,
                                   friendlyFileNameForUser.ToLowerInvariant())));
                                
                            }
                            //request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Cache-Control", "max-age=7776000,must-revalidate"));//90days
                            _DL1_AwsTransferUtil.Upload(request);
                            Console.WriteLine("{0} uploaded to DL1", fi.FullName);
                            File.Delete(tempDecryptedFilePath);

                        }
                        else
                        {
                            String awsS3ObjectName = GetAwsS3ObjectName(targetFileName, prefix);
                            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
                            .WithBucketName(bucket)
                            .WithFilePath(fi.FullName)
                            .WithTimeout(_S3Tiemout)
                            .WithServerSideEncryptionMethod(ServerSideEncryptionMethod.AES256)
                            .WithKey(awsS3ObjectName.ToLowerInvariant())
                            ;
                            if (!String.IsNullOrEmpty(friendlyFileNameForUser))
                            {
                                string dlmode = "attachment";
                                if (fi.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dlmode = "inline";
                                }
                                request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Content-Disposition", String.Format("{0};filename={1}", dlmode,
                                    friendlyFileNameForUser.ToLowerInvariant())));
                            }
                            request.AddHeaders(Amazon.S3.Util.AmazonS3Util.CreateHeaderEntry("Cache-Control", "max-age=7776000,must-revalidate"));//90days
                            _DL2_AwsTransferUtil.Upload(request);
                            InvalidateCloudFrontCache(awsS3ObjectName);
                            Console.WriteLine("{0} uploaded to DL2", fi.FullName);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ERROR:{0}-{1}", fi == null ? "null" : fi.FullName, ex.ToString());
                    }

                }
            }
            Console.WriteLine("Tread {0} completed.", _ThreadID);
        }

        private string GetAwsS3ObjectName(String targetFileName, String s3FolderPrefix)
        {
            String awsS3ObjectName = targetFileName.Replace("\\", "/");
            if (awsS3ObjectName.StartsWith("/")) awsS3ObjectName = awsS3ObjectName.Substring(1);

            if (!String.IsNullOrEmpty(s3FolderPrefix))
            {
                awsS3ObjectName = String.Format("{0}{1}", s3FolderPrefix, awsS3ObjectName);
            }
            return awsS3ObjectName.ToLowerInvariant();
        }

        private String DecryptBlowfishEncryptedFile(String encryptedFilePath)
        {
            String encKey = "Lgt3tJQTnLNai42Q8KtE";

            #region " COM REF "
            COMBLOWFISHLib.Crypt cr = new COMBLOWFISHLib.Crypt();
            cr.SetKey(encKey);
            cr.FileDeCrypt(encryptedFilePath);
            return encryptedFilePath + ".bf";
           
            #endregion



        }

        public Boolean FindInS3(String targetFileNameToFind)
        {
            Boolean found = false;
            string targetFileName = targetFileNameToFind.Replace(_BaseFolderPath, String.Empty);
            String prefix = _DL2_S3KeyPrefix;
            String bucket = _DL2_S3BucketName;
            Amazon.RegionEndpoint endPoint = _DL2_S3Endpoint;

            Boolean isBlowFishFile = Path.GetExtension(targetFileNameToFind).EndsWith(".bf", StringComparison.InvariantCultureIgnoreCase);
            if (isBlowFishFile)
            {
                prefix = _DL1_S3KeyPrefix;
                bucket = _DL1_S3BucketName;
                endPoint = _DL1_S3Endpoint;
            }

            String awsS3ObjectName = GetAwsS3ObjectName(targetFileName, prefix);
            using (Amazon.S3.AmazonS3Client s3Client = new AmazonS3Client(_AWSAccessKey, _AWSSecretKey, endPoint))
            {

                //GetObjectRequest req = new GetObjectRequest();
                GetObjectMetadataRequest req = new GetObjectMetadataRequest();
                req.WithBucketName(bucket);
                req.WithKey(awsS3ObjectName);

                try
                {
                    //GetObjectResponse resp = s3Client.GetObject(req);
                    GetObjectMetadataResponse resp = s3Client.GetObjectMetadata(req);
                    found = resp != null && resp.ContentLength > 0;
                }
                catch(AmazonS3Exception aex)
                { 
                
                }
            }
            return found;
        }

        public void InvalidateCloudFrontCache(String s3ObjectKey)
        {
            List<String> objectsToInvalidate = new List<string>();
            if (s3ObjectKey.StartsWith("/")) objectsToInvalidate.Add(s3ObjectKey);
            else objectsToInvalidate.Add("/" + s3ObjectKey);
            try
            {
                using (AmazonCloudFrontClient client = new AmazonCloudFrontClient(_AWSAccessKey, _AWSSecretKey))
                {

                    InvalidationBatch ib = new InvalidationBatch();
                    Amazon.CloudFront.Model.Paths path = new Amazon.CloudFront.Model.Paths();

                    CreateInvalidationRequest req = new CreateInvalidationRequest();
                    req.WithDistributionId(ConfigurationManager.AppSettings["DL2.CloudFront.DistributionID"]);
                    req.WithInvalidationBatch(new InvalidationBatch
                    {
                        Paths = new Paths
                        {
                            Quantity = 1,
                            Items = objectsToInvalidate
                        },
                        CallerReference = DateTime.UtcNow.ToString("R")
                    });

                    CreateInvalidationResponse resp = client.CreateInvalidation(req);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
