﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using Amazon;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.CloudFront;
using System.Threading;
namespace Anritsu.ConnectS3UploadConsole
{
    class Program
    {
        private static String _ExcludeFolderPattern;
        private static String _ExcludeFileExtPattern;
        private static String _SourceFolderPath;
        private static Int32 _ModifiedSinceMinutes = -1;
        private static Int32 _NumberOfTreads = 10;
        private static Boolean _FindAndIncludeMissingObjectsInS3 = false;
        private static String _RunMode = "normal";
        static void Main(string[] args)
        {
            try
            {
                foreach (string arg in args)
                {
                    if (arg.StartsWith("-help", StringComparison.InvariantCultureIgnoreCase))
                    {
                        PrintMenu();
                        return;
                    }
                    else if (arg.StartsWith("-modifiedsinceminutes:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _ModifiedSinceMinutes = Int32.Parse(arg.Replace("-modifiedsinceminutes:", String.Empty));
                    }
                    else if (arg.StartsWith("-threads:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _NumberOfTreads = Int32.Parse(arg.Replace("-threads:", String.Empty));
                    }
                    else if (arg.StartsWith("-includemissing:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _FindAndIncludeMissingObjectsInS3 = Boolean.Parse(arg.Replace("-includemissing:", String.Empty));
                    }
                    else if (arg.StartsWith("-runmode:", StringComparison.InvariantCultureIgnoreCase))
                    {
                        _RunMode = arg.Replace("-runmode:", String.Empty);
                    }
                }


                InitConfig(args);
                if (_RunMode == "normal")
                {
                    ProcessFiles();
                }
                else if (_RunMode == "uploadfromjpdb")
                {
                    UploadFromJapanDownloadDB();
                }
                Console.WriteLine("Waiting..");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in Main: {0}", ex.Message);
            }
            
            Console.Read();
        }

        private static void PrintMenu()
        {
            Console.WriteLine("Optional parameters");
            Console.WriteLine("Modified Since ==> -modifiedsinceminutes:<-1 OR > 0");
            Console.WriteLine("Number of Threads ==> -threads:<10>");
            Console.WriteLine("RunMode ==> -runmode:<uploadfromjpdb|normal>");
        }

        private static void InitConfig(string[] args)
        {
            try
            {

            _ExcludeFolderPattern = ConfigurationManager.AppSettings["ExcludeFolderPattern"];
            _ExcludeFileExtPattern = ConfigurationManager.AppSettings["ExcludeFileExtPattern"];
            _SourceFolderPath = ConfigurationManager.AppSettings["SourcePath"];

            DirectoryInfo sourceDir = new DirectoryInfo(_SourceFolderPath);
            if (!sourceDir.Exists) throw new ArgumentNullException("Invalid source directory");

            String _BlowFishTempFolder = ConfigurationManager.AppSettings["BFEncryptTempDir"];
            DirectoryInfo diBF = new DirectoryInfo(_BlowFishTempFolder);
            if (!diBF.Exists) diBF.Create();
            
            

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in InitConfig(): " + ex.ToString());
            }
        }

        private static Boolean ValidateDirPath(string fullPath)
        {
            String pathToMatch = fullPath.Replace(_SourceFolderPath, String.Empty).Substring(1);
            Regex rgx = new Regex(_ExcludeFolderPattern, RegexOptions.IgnoreCase);
            return !rgx.IsMatch(pathToMatch);
        }

        private static Boolean ValidateFileExtPath(string ext)
        {
            Regex rgx = new Regex(_ExcludeFileExtPattern, RegexOptions.IgnoreCase);
            return !rgx.IsMatch(ext);
        }

        private static void UploadFromJapanDownloadDB()
        {
            try
            {
                List<FileInfo> fileList = new List<FileInfo>();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Connect.DB.ConnStr.JPDL"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("", conn);
                    cmd.CommandText = "SELECT * FROM [dbo].[W2_Object] (NOLOCK) ORDER BY [ModelName], [FileName]";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        String fileName = rdr["FileName"].ToString();
                        String softName = rdr["SoftName"].ToString();
                        String modelName = rdr["ModelName"].ToString();
                        if (rdr["EncodeFlag"].ToString() == "1") fileName += ".bf";
                        String filePath = Path.Combine(Path.Combine(_SourceFolderPath, modelName), fileName);
                        FileInfo fi = new FileInfo(filePath);
                        if (fi.Exists)
                        {
                            if (ValidateDirPath(fi.FullName) && ValidateFileExtPath(fi.Extension))
                                fileList.Add(fi);
                        }
                    }
                    rdr.Close();
                }
                Console.WriteLine("{0} files found to upload to S3.", fileList.Count);
                List<FileInfo>[] list = Partition<FileInfo>(fileList, _NumberOfTreads);
                Int32 count = 1;
                foreach (List<FileInfo> modFileGroup in list)
                {
                    ConnectS3Uploader s3u = new ConnectS3Uploader(count, modFileGroup, _SourceFolderPath);
                    ThreadStart job = new ThreadStart(s3u.UploadToS3);
                    Thread thread = new Thread(job);
                    thread.Start();
                    Console.WriteLine("Thread {0} started..", count++);
                    //s3u.UploadToS3();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in UploadFromJapanDownloadDB(): " + ex.ToString());
            }
        }

        private static void ProcessFiles()
        {
            DirectoryInfo sourceDir = new DirectoryInfo(_SourceFolderPath);
            try
            {
                FileInfo[] files = sourceDir.GetFiles("*.*", SearchOption.AllDirectories);
                if (files == null) return;
                List<FileInfo> modFiles = new List<FileInfo>();
                
                DateTime modifiedSince = DateTime.MinValue;
                if(_ModifiedSinceMinutes != -1)
                    modifiedSince = DateTime.UtcNow.AddMinutes(-1 * _ModifiedSinceMinutes);

                ConnectS3Uploader s3Finder = new ConnectS3Uploader(0, null, _SourceFolderPath);
                foreach (FileInfo fi in files.ToList())
                {
                    if (fi.LastWriteTimeUtc >= modifiedSince)
                    {
                        if (ValidateDirPath(fi.FullName) && ValidateFileExtPath(fi.Extension))
                            modFiles.Add(fi);
                    }
                    else
                    {
                        if (ValidateDirPath(fi.FullName) && ValidateFileExtPath(fi.Extension))
                        {
                            Console.WriteLine("Checkin in S3..{0}", fi.FullName);
                            Boolean isItInS3 = s3Finder.FindInS3(fi.FullName);
                            if (!isItInS3)
                            {
                                modFiles.Add(fi);
                            }
                        }
                    }
                }

                Console.WriteLine("{0} files found to upload to S3.", modFiles.Count);
                List<FileInfo>[] list = Partition<FileInfo>(modFiles, _NumberOfTreads);

                Int32 count = 1;
                foreach (List<FileInfo> modFileGroup in list)
                {
                    ConnectS3Uploader s3u = new ConnectS3Uploader(count, modFileGroup, _SourceFolderPath);
                    ThreadStart job = new ThreadStart(s3u.UploadToS3);
                    Thread thread = new Thread(job);
                    thread.Start();
                    Console.WriteLine("Thread {0} started..", count++);
                    //s3u.UploadToS3();
                }

                
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in ProcessFiles(): " + ex.ToString());
            }
        }

        public static List<T>[] Partition<T>(List<T> list, int totalPartitions)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            if (totalPartitions < 1)
                throw new ArgumentOutOfRangeException("totalPartitions");

            List<T>[] partitions = new List<T>[totalPartitions];

            int maxSize = (int)Math.Ceiling(list.Count / (double)totalPartitions);
            int k = 0;

            for (int i = 0; i < partitions.Length; i++)
            {
                partitions[i] = new List<T>();
                for (int j = k; j < k + maxSize; j++)
                {
                    if (j >= list.Count)
                        break;
                    partitions[i].Add(list[j]);
                }
                k += maxSize;
            }

            return partitions;
        }

        
    }
}
