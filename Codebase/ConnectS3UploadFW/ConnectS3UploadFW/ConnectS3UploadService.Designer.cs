﻿namespace Anritsu.ConnectS3UploadFW
{
    partial class ConnectS3UploadService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fswConnectS3 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.fswConnectS3)).BeginInit();
            // 
            // fswConnectS3
            // 
            this.fswConnectS3.EnableRaisingEvents = true;
            this.fswConnectS3.IncludeSubdirectories = true;
            // 
            // ConnectS3UploadService
            // 
            this.ServiceName = "ConnectS3UploadService";
            ((System.ComponentModel.ISupportInitialize)(this.fswConnectS3)).EndInit();

        }

        #endregion

        private System.IO.FileSystemWatcher fswConnectS3;
    }
}
