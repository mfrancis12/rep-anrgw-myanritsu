﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Security.Permissions;

namespace Anritsu.ConnectS3UploadFW
{
   

    partial class ConnectS3UploadService : ServiceBase
    {
        private ConnectS3UploadWatcher _ConnectWatcher;

        public ConnectS3UploadService()
        {
            InitializeComponent();
            _ConnectWatcher = new ConnectS3UploadWatcher();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            _ConnectWatcher.StartWatch();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            _ConnectWatcher.StopWatch();
        }

       

        
        
    }
}
