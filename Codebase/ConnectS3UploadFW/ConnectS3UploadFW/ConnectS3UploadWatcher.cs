﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Anritsu.ConnectS3UploadFW
{
    public class S3UploadStatus
    {
        public Boolean IsProcessing { get; set; }
        public DateTime EventStartedUTC { get; set; }
        public DateTime EventCompletedUTC { get; set; }
    }

    public class ConnectS3UploadWatcher
    {
        private static object _changeLock = new object();
        private FileSystemWatcher _FSWConnect;
        private Dictionary<String, S3UploadStatus> _S3UploadQueue;
        private String _BaseFolderToWatch;
        private String _LogPath;

        public ConnectS3UploadWatcher()
        {
            _FSWConnect = new System.IO.FileSystemWatcher();
            _FSWConnect.IncludeSubdirectories = true;

            InitConfig();

            _FSWConnect.Path = _BaseFolderToWatch;
            _FSWConnect.IncludeSubdirectories = true;
            //fswConnect.NotifyFilter = NotifyFilters.LastWrite; //NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName;
            _FSWConnect.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            // Add event handlers.
            _FSWConnect.Changed += new FileSystemEventHandler(fswConnectS3_Changed);
            _FSWConnect.Created += new FileSystemEventHandler(fswConnectS3_Changed);
            _FSWConnect.Deleted += new FileSystemEventHandler(fswConnectS3_Changed);
            _FSWConnect.Renamed += new RenamedEventHandler(fswConnectS3_Renamed);


           
        }

        public void StartWatch()
        {
            // Begin watching.
            _FSWConnect.EnableRaisingEvents = true;
        }

        public void StopWatch()
        {
            _FSWConnect.EnableRaisingEvents = false;
        }

        private void InitConfig()
        {
            _S3UploadQueue = new Dictionary<string, S3UploadStatus>();

            _LogPath = ConfigurationManager.AppSettings["Log.Path"];
            FileInfo fiLog = new FileInfo(_LogPath);
            if (!fiLog.Exists) fiLog.Create();

            TextWriterTraceListener logListener = new TextWriterTraceListener(_LogPath, "ConnectS3UploadServiceLog");
            Trace.Listeners.Add(logListener);

            try
            {

                DirectoryInfo baseFolderToWatch = new DirectoryInfo(ConfigurationManager.AppSettings["DirToWatch"]);
                if (!baseFolderToWatch.Exists) throw new ArgumentNullException("DirToWatch");
                _BaseFolderToWatch = baseFolderToWatch.FullName;

                String blowFishEncryptTempFolder = ConfigurationManager.AppSettings["BFEncryptTempDir"];
                DirectoryInfo diBF = new DirectoryInfo(blowFishEncryptTempFolder);
                if (!diBF.Exists) diBF.Create();

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in InitConfig(): " + ex.ToString());
                Trace.WriteLine("ERROR in InitConfig(): " + ex.ToString());
            }
        }

        public void fswConnectS3_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

            try
            {
                if (!ValidateDirPath(e.FullPath)) return;
                String ext = Path.GetExtension(e.FullPath);
                if (!ValidateFileExtPath(ext)) return;

                Boolean isDir = false;
                FileInfo fileInfo = new FileInfo(e.FullPath);
                if (((fileInfo.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                    && String.IsNullOrEmpty(fileInfo.Extension))
                {
                    isDir = true;
                    if (e.ChangeType != WatcherChangeTypes.Deleted)
                        return;
                }

                if (e.ChangeType == WatcherChangeTypes.Changed)
                {
                    if (!CheckInS3UploadQueue(e.FullPath)) return;
                    Console.WriteLine(String.Format("{0}:{1}\n", e.ChangeType.ToString(), e.FullPath));
                    ProcessEvent(e.FullPath, isDir, e.ChangeType);
                }
                else if (e.ChangeType == WatcherChangeTypes.Created)
                {
                    if (!CheckInS3UploadQueue(e.FullPath)) return;
                    Console.WriteLine(String.Format("{0}:{1}\n", e.ChangeType.ToString(), e.FullPath));
                    ProcessEvent(e.FullPath, isDir, e.ChangeType);
                }
                else if (e.ChangeType == WatcherChangeTypes.Deleted)
                {
                    if (!CheckInS3UploadQueue(e.FullPath)) return;
                    Console.WriteLine(String.Format("{0}:{1}\n", e.ChangeType.ToString(), e.FullPath));
                    ProcessEvent(e.FullPath, isDir, e.ChangeType);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in fswConnectS3_Changed: " + ex.ToString());
                Trace.WriteLine("ERROR in fswConnectS3_Changed: " + ex.ToString());
            }
        }

        public void fswConnectS3_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            //ProcessEvent(e.FullPath, e.OldFullPath, WatcherChangeTypes.Renamed);
            //handle in change event.  no need
            //Console.WriteLine(String.Format("RENAMED:{0}", e.FullPath));
        }

        private Boolean ValidateDirPath(string fullPath)
        {
            String pathToMatch = fullPath.Replace(_BaseFolderToWatch, String.Empty).Substring(1);
            Regex rgx = new Regex(ConfigurationManager.AppSettings["ExcludeFolderPattern"], RegexOptions.IgnoreCase);
            return !rgx.IsMatch(pathToMatch);
        }

        private Boolean ValidateFileExtPath(string ext)
        {
            Regex rgx = new Regex(ConfigurationManager.AppSettings["ExcludeFileExtPattern"], RegexOptions.IgnoreCase);
            return !rgx.IsMatch(ext);
        }

        private void ProcessEvent(string fullPath, Boolean isFolder, WatcherChangeTypes wct)
        {
            try
            {
                ConnectS3Uploader s3u = new ConnectS3Uploader(fullPath, _BaseFolderToWatch);
                ThreadStart job = null;
                if (wct == WatcherChangeTypes.Deleted)
                {
                    if (isFolder)
                    {
                        job = new ThreadStart(s3u.DeleteFolderFromS3);
                    }
                    else
                    {
                        job = new ThreadStart(s3u.DeleteFileFromS3);
                    }
                    Thread thread = new Thread(job);
                    thread.Start();

                }
                else
                {
                    if (!isFolder)
                    {
                        AddToS3UploadQueue(fullPath);
                        job = new ThreadStart(s3u.UploadFileToS3);
                        Thread thread = new Thread(job);
                        thread.Start();
                        UpdateUploadCompletedS3UploadQueue(fullPath);
                    }
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR in ProcessEvent: " + ex.ToString());
                Trace.WriteLine("ERROR in ProcessEvent: " + ex.ToString());
            }
        }

        private bool CheckInS3UploadQueue(string fullPath)
        {
            lock (_changeLock)
            {
                if (_S3UploadQueue.ContainsKey(fullPath))
                {
                    S3UploadStatus st = _S3UploadQueue[fullPath];
                    if (st.IsProcessing) return false;
                    if (st.EventCompletedUTC.AddMilliseconds(1000) > DateTime.UtcNow) return false;
                }
                return true;
            }
        }

        private void AddToS3UploadQueue(string fullPath)
        {
            lock (_changeLock)
            {
                DateTime nowUTC = DateTime.Now.ToUniversalTime();
                S3UploadStatus st = new S3UploadStatus();
                st.EventStartedUTC = nowUTC;
                st.IsProcessing = true;

                if (_S3UploadQueue.ContainsKey(fullPath))
                {
                    _S3UploadQueue[fullPath] = st;
                }
                else
                {
                    _S3UploadQueue.Add(fullPath, st);
                }

                //cleanup
                Int32 cleanUpIntervalMinutes = Int32.Parse(ConfigurationManager.AppSettings["UploadQueueCleanUpMinutes"]);

                foreach (String path in _S3UploadQueue.Keys.ToList())
                {
                    S3UploadStatus ust = _S3UploadQueue[path];
                    if (ust.EventCompletedUTC != null && ust.EventCompletedUTC != DateTime.MinValue && ust.EventCompletedUTC.AddMinutes(cleanUpIntervalMinutes) < nowUTC)
                    {
                        _S3UploadQueue.Remove(path);
                    }
                }
            }
        }

        private void UpdateUploadCompletedS3UploadQueue(string fullPath)
        {
            lock (_changeLock)
            {
                if (_S3UploadQueue.ContainsKey(fullPath))
                {
                    S3UploadStatus st = _S3UploadQueue[fullPath];
                    st.IsProcessing = false;
                    st.EventCompletedUTC = DateTime.UtcNow;
                    _S3UploadQueue[fullPath] = st;
                }
            }
        }

    }
}
