﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Security.Permissions;

namespace Anritsu.ConnectS3UploadFW
{
  

    public class Program
    {
        

        public static void Main(string[] args)
        {
            if (ConfigurationManager.AppSettings["IsConsoleMode"] == "true")
            {
                ConnectS3UploadWatcher connectWatcher = new ConnectS3UploadWatcher();
                connectWatcher.StartWatch();
                Console.Read();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new ConnectS3UploadService() 
                };
                ServiceBase.Run(ServicesToRun);
            }
            
        }

        

        

    }
}
