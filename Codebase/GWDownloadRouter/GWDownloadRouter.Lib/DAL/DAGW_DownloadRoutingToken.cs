﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.GWDownloadRouter.Lib.DAL
{
    internal static class DAGW_DownloadRoutingToken
    {
        public static DataRow SelectByTokenID(Guid routeTokenID)
        {
            DBGWDownloadRouting db = new DBGWDownloadRouting();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGWDownloadRouting.SQL_SP.GW_DownloadRoutingToken_SelectByTokenID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenID);
                return SQLUtility.FillInDataRow(cmd);

            }
        }

        public static DataRow Insert(String downloadType, String fileName, String requestedUserEmail, String requestedUserIP, Int32 encodeFlag)
        {
            DBGWDownloadRouting db = new DBGWDownloadRouting();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGWDownloadRouting.SQL_SP.GW_DownloadRoutingToken_Insert);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@DownloadType", downloadType.Trim().ToUpperInvariant());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@FileName", fileName.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserEmail", requestedUserEmail.Trim());
                SQLUtility.SqlParam_AddAsString(ref cmd, "@RequestedUserIP", requestedUserIP.Trim());
                cmd.Parameters.AddWithValue("@EncodeFlag", encodeFlag);
                return SQLUtility.FillInDataRow(cmd);

            }
        }

        public static Int32 UpdateTokenExp(Guid routeTokenID, DateTime newTokenExpUTC)
        {
            DBGWDownloadRouting db = new DBGWDownloadRouting();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGWDownloadRouting.SQL_SP.GW_DownloadRoutingToken_UpdateTokenExp);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenID);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@TokenExp", newTokenExpUTC);
                return cmd.ExecuteNonQuery();

            }
        }

        public static Int32 UpdateForceExpire(Guid routeTokenID)
        {
            DBGWDownloadRouting db = new DBGWDownloadRouting();
            using (SqlConnection conn = db.GetOpenedConn())
            {
                SqlCommand cmd = db.MakeCmdSP(conn, null, DBGWDownloadRouting.SQL_SP.GW_DownloadRoutingToken_UpdateForceExpire);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@RouteTokenID", routeTokenID);
                return cmd.ExecuteNonQuery();

            }
        }
    }
}