﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.GWDownloadRouter.Lib.DAL
{
    internal abstract class DBBase
    {
        internal virtual String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("DB.SpSchema"); }
        }

        internal virtual String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("DB.ConnStr"); }
        }

        internal virtual Int32 CommandTimeout
        {
            get
            {
                int timeout = 0;
                int.TryParse(ConfigUtility.AppSettingGetValue("DB.CmdTimeout"), out timeout);
                return timeout;
            }
        }

        internal virtual SqlConnection GetOpenedConn()
        {
            return SQLUtility.ConnOpen(ConnStr);
        }

        internal virtual SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
        {
            return SQLUtility.MakeSPCmd(conn, tran, SP_Schema, spName, CommandTimeout);
        }

        internal virtual SqlCommand MakeCmdInline(SqlConnection conn, SqlTransaction tran, String sql)
        {
            return SQLUtility.MakeInlineCmd(conn, tran, sql, CommandTimeout);
        }
    }
}