﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.GWDownloadRouter.Lib.DAL
{
    internal class DBGWDownloadRouting : DBBase
    {
        internal override String SP_Schema
        {
            get { return ConfigUtility.AppSettingGetValue("GWDownloadRouter.DB.SpSchema.GWDLRouting"); }
        }

        internal override String ConnStr
        {
            get { return ConfigUtility.ConnStrGetValue("GWDownloadRouter.DB.ConnStr.GWDLRouting"); }
        }

        internal override Int32 CommandTimeout
        {
            get
            {
                int timeout = 0;
                int.TryParse(ConfigUtility.AppSettingGetValue("GWDownloadRouter.DB.CmdTimeout.GWDLRouting"), out timeout);
                return timeout;
            }
        }

        public class SQL_Inline
        {


            //public static String JapanDownload_SelectObjectByModel
            //{
            //    get
            //    {
            //        return "SELECT * FROM [W2_Object] (NOLOCK) WHERE ModelName = @ModelName";
            //    }
            //}


        }

        public class SQL_SP
        {
            public const String GW_DownloadRoutingToken_SelectByTokenID = "[uSP_GW_DownloadRoutingToken_SelectByTokenID]";
            public const String GW_DownloadRoutingToken_Insert = "[uSP_GW_DownloadRoutingToken_Insert]";
            public const String GW_DownloadRoutingToken_UpdateTokenExp = "[uSP_GW_DownloadRoutingToken_UpdateTokenExp]";
            public const String GW_DownloadRoutingToken_UpdateForceExpire = "[uSP_GW_DownloadRoutingToken_UpdateForceExpire]";
        }
    }
}