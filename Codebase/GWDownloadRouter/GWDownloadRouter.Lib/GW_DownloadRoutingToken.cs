﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anritsu.GWDownloadRouter.Lib
{
    public class GW_DownloadRoutingToken
    {
        public Guid RouteTokenID { get; set; }
        public String DownloadType { get; set; }
        public String FileName { get; set; }
        public String RequestedUserEmail { get; set; }
        public String RequestedUserIP { get; set; }
        public DateTime TokenExp { get; set; }
        public Boolean IsForcedExpired { get; set; }
        public Int32 EncodeFlag { get; set; } 
        public DateTime CreatedOnUTC { get; set; } 
    }
}