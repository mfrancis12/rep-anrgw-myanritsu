﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.GWDownloadRouter.Web.App_Lib
{
    public static class ConnectDLRouteWSClient
    {
        public static ConnectDLRouteWSRef.ConnectDLRouteWS GetService()
        {
            ConnectDLRouteWSRef.ConnectDLRouteWS ws = new ConnectDLRouteWSRef.ConnectDLRouteWS();
            ws.Url = ConfigUtility.AppSettingGetValue("GWDownloadRouter.ConnectDLRouteWS.EndpointURL");
            return ws;
        }

        public static ConnectDLRouteWSRef.GW_DownloadRoutingToken GetDownloadInfo(Guid tokenID, Boolean logAccess)
        {
            ConnectDLRouteWSRef.ConnectDLRouteWS srv = GetService();
            return srv.GetDownloadInfo(tokenID.ToString().ToUpperInvariant(), logAccess);
        }

        //public static Guid AddDownloadInfo(DLRTWS.DownloadTypeCode downloadType, String fileName, String userEmail, string userIP, Int32 encodeFlag)
        //{
        //    DLRTWS.DLRouteService srv = GetService();
        //    return srv.AddDownloadInfo(downloadType, fileName, userEmail, userIP, encodeFlag);
        //}

        //public static DLRTWS.GW_DownloadRoutingToken ExtendTokenExp(Guid tokenID, DateTime newTokenExpUTC)
        //{
        //    DLRTWS.DLRouteService srv = GetService();
        //    return srv.ExtendExp(tokenID.ToString().ToUpperInvariant(), newTokenExpUTC);
        //}

        //public static String GetDownloadURL(Guid tokenID, String filePath)
        //{
        //    if (tokenID.IsNullOrEmptyGuid() || filePath.IsNullOrEmptyString()) return String.Empty;
        //    return String.Format("{0}/{1}/{2}"
        //        , ConfigUtility.AppSettingGetValue("GWDownloadRouteInfo.SDK.DLHost")
        //        , tokenID.ToString()
        //        , filePath.Replace(@"\", "/")).ToLowerInvariant();
        //}
    }
}