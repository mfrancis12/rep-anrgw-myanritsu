﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.GWDownloadRouter.Web.App_Lib
{
    internal static class DLHelper
    {
        public static String GetDownloadBaseFolderPath(String downloadType)
        {
            if (downloadType.IsNullOrEmptyString()) return String.Empty;
            String pathKey = String.Format("GWDLRouter.DownloadType_{0}", downloadType.ToUpperInvariant());
            return ConfigUtility.AppSettingGetValue(pathKey);
        }

       
    }
}