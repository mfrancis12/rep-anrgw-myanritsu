﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Anritsu.GWDownloadRouter.Web.App_Lib
{
    public static class MimeHelper
    {
        private static Dictionary<string, string> _MimeCollection = null;
        public static Dictionary<string, string> MimeCollection
        {
            get
            {
                if (_MimeCollection == null)
                {
                    _MimeCollection = new Dictionary<string, string>();
                    XmlDocument mimeXml = new XmlDocument();
                    mimeXml.Load(HttpContext.Current.Server.MapPath("~/App_Data/mime.xml"));
                    if (mimeXml == null)
                        throw new ArgumentException("MIME file is missing.");
                    XmlElement root = mimeXml.DocumentElement;
                    XmlNodeList nodes = root.SelectNodes("/MIMETYPES/MIME"); // You can filter elements
                    foreach (XmlNode node in nodes)
                    {
                        string fileExt = node.Attributes["fileExt"].Value.ToLower().Trim();
                        string contentType = node["ContentType"].InnerText.Trim();
                        if (!_MimeCollection.ContainsKey(fileExt))
                            _MimeCollection.Add(fileExt, contentType);
                    }
                }
                return _MimeCollection;
            }
        }

        public static string GetContentType(string ext)
        {
            _MimeCollection = null;
            string contentType = string.Empty;
            bool foundit = MimeCollection.TryGetValue(ext.ToLower().Trim(), out contentType);
            return contentType;
        }
    }
}