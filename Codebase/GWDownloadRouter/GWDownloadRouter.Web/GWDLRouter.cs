﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Globalization;
using System.Security.Cryptography;
using System.Net;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.GWDownloadRouter.Web
{
    public class GWDLRouter : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (context == null || context.Request == null) return;
            switch (context.Request.HttpMethod.ToUpperInvariant())
            {
                case "GET":
                case "HEAD":
                    break;
                default:
                    context.Response.StatusCode = 501;
                    return;
            }

            if (context.Request.Url.Segments == null || context.Request.Url.Segments.Length < 2) throw new HttpException(404, "Page not found.");
            
            #region " validate request "
            UriBuilder baseUri = new UriBuilder(context.Request.Url.Scheme, context.Request.Url.Host, context.Request.Url.Port);
            Uri rawURI = new Uri(baseUri.Uri, context.Request.RawUrl);
            String tokenSeg = context.Request.Url.Segments[1];
            Guid tokenID = ConvertUtility.ConvertToGuid(tokenSeg.Replace("/", String.Empty), Guid.Empty);

            ConnectDLRouteWSRef.GW_DownloadRoutingToken dlInfo = App_Lib.ConnectDLRouteWSClient.GetDownloadInfo(tokenID, true);

            if (dlInfo == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.End();
                return;
            } 

            DirectoryInfo baseFolder = new DirectoryInfo(App_Lib.DLHelper.GetDownloadBaseFolderPath(dlInfo.DownloadType));
            if (!baseFolder.Exists)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.End();
                return;
            }

            //String requestedFilePath = rawURI.LocalPath.Replace("/" + tokenSeg, "").Replace("/", @"\");
            //if (!requestedFilePath.Equals(dlInfo.FileName, StringComparison.InvariantCultureIgnoreCase))
            //{
            //    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            //    context.Response.End();
            //    return;
            //}

            String requestedFileNameForUser = rawURI.LocalPath.Replace("/" + tokenSeg, "").Replace("/", @"\");
            if (!requestedFileNameForUser.Equals(dlInfo.FileNameForUser, StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.End();
                return;
            }

            #endregion
            string filePath = Path.Combine(baseFolder.FullName, dlInfo.FileName);
            Boolean isTestMode = ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("GWDownloadRouter.IsTestMode"), true);
            if (isTestMode)
            {
                if (Path.GetExtension(dlInfo.FileName).Contains(".pdf"))
                    filePath = Path.Combine(baseFolder.FullName, "testpdf.pdf");
                else
                    filePath = Path.Combine(baseFolder.FullName, "testzip.zip");
            }
            //if (fileInfo.Length > 1048576)
            //{
            //    Double newTotalMinutes = ((fileInfo.Length / 1048576.00) + 1.00) * 5.00;
            //    DateTime newExp = DateTime.UtcNow.AddMinutes(newTotalMinutes);
            //    dlRouteInfo = App_Lib.DLHelper.ExtendTokenExp(dlRouteInfo.RouteTokenID, newExp);
            //}
            if (dlInfo.EncodeFlag == 1)
            {
                DownloadEncryptedFile(context, filePath, dlInfo.FileNameForUser, tokenID);
            }
            else
            {
                DownloadRegularFile(context, filePath, dlInfo.FileNameForUser);
            }
            
        }

        private void DownloadEncryptedFile_Bak(HttpContext context, String filePath, Guid tokenID)
        {
            //this whole thing needs to change later when we are ready to replace the COM blowfish with something not COM.
            //Current COM component has issues and cannot be decrypted using other blowfish libraries.
            String srcEncryptedFilePath = filePath + ".bf";
            FileInfo fileInfo = new FileInfo(srcEncryptedFilePath);
            if (fileInfo == null || !fileInfo.Exists 
                || String.IsNullOrEmpty(fileInfo.Extension)
                || tokenID.IsNullOrEmptyGuid())
                throw new HttpException(404, "Page not found.");

            String fileNameForUser = fileInfo.Name.Replace(".bf", "");
            String tempEncryptedFilePath = context.Server.MapPath(String.Format("~/ectmp/{0}.bft", tokenID.ToString().ToLowerInvariant()));
            fileInfo.CopyTo(tempEncryptedFilePath, true);
            String tempDecryptedFilePath = DecryptBlowfishEncryptedFile(tempEncryptedFilePath);
            File.Delete(tempEncryptedFilePath);

            //String renamedFile = tempDecryptedFilePath.Replace(".bft", "").Replace(".bf", "");
            //File.Move(tempDecryptedFilePath, renamedFile);
            FileInfo decryptedFileInfo = new FileInfo(tempDecryptedFilePath);
            String ext = Path.GetExtension(filePath).ToLowerInvariant();



            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AddHeader("Content-Length", decryptedFileInfo.Length.ToString());
            context.Response.ContentType = App_Lib.MimeHelper.GetContentType(ext);
            if (fileInfo.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                context.Response.AddHeader("Content-Disposition", "inline; filename=" + fileNameForUser);
            else
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileNameForUser);
            //context.Response.AddHeader("X-Robots-Tag", "noarchive");//Tell search engine not to show cache link
            //context.Response.Buffer = false;
            //Byte[] fileBytes = DecryptBlowfishEncryptedFile(filePath);
            //context.Response.BinaryWrite(fileBytes);
            context.Response.TransmitFile(decryptedFileInfo.FullName);
            context.Response.Flush();
            context.Response.End();
        }

        private String DecryptBlowfishEncryptedFile(String encryptedFilePath)
        {
            String encKey = "Lgt3tJQTnLNai42Q8KtE";
            
            #region " COM REF "
            COMBLOWFISHLib.Crypt cr = new COMBLOWFISHLib.Crypt();
            cr.SetKey(encKey);            
            cr.FileDeCrypt(encryptedFilePath);
            return encryptedFilePath + ".bf";
            //using (FileStream originalStream = File.OpenRead(encryptedFilePath + ".bf"))
            //{
            //    decryptedBytes = new byte[originalStream.Length];
            //    originalStream.Read(encryptedBytes, 0, encryptedBytes.Length);
            //    originalStream.Close();
            //}
            //return decryptedBytes;
            //COMBLOWFISHLib.ICrypt icryp = COMBLOWFISHLib.CryptClass;
            //icryp.SetKey(encKey);
            //cryp.FileDeCrypt(encryptedFilePath);
            #endregion

            #region " Chilkat "
            //Chilkat.Crypt crypt = new Chilkat.Crypt();
            //crypt.UnlockComponent("whatever");
            //crypt.SetAlgorithmBlowfish();
            //crypt.KeyLength = 256;
            //crypt.SetSecretKeyViaPassPhrase(encKey);
            //FileInfo encFileInfo = new FileInfo(encryptedFilePath);
            //byte[] encBytes;
            //using (BinaryReader biReader = new BinaryReader(File.OpenRead(encryptedFilePath)))
            //{
            //    encBytes = biReader.ReadBytes((int)encFileInfo.Length);
            //}
            //return crypt.Decrypt(encBytes);
            #endregion
            
        }

        private void DownloadEncryptedFile(HttpContext context, String filePath, String fileNameForUserIn, Guid tokenID)
        {
            //this whole thing needs to change later when we are ready to replace the COM blowfish with something not COM.
            //Current COM component has issues and cannot be decrypted using other blowfish libraries.
            String srcEncryptedFilePath = filePath + ".bf";
            FileInfo fileInfo = new FileInfo(srcEncryptedFilePath);
            if (fileInfo == null || !fileInfo.Exists
                || String.IsNullOrEmpty(fileInfo.Extension)
                || tokenID.IsNullOrEmptyGuid())
                throw new HttpException(404, "Page not found.");

            //String fileNameForUser = fileInfo.Name.Replace(".bf", "");
            String fileNameForUser = Path.GetFileName(fileNameForUserIn);
            String tempEncryptedFilePath = context.Server.MapPath(String.Format("~/ectmp/{0}.bft", tokenID.ToString().ToLowerInvariant()));
            fileInfo.CopyTo(tempEncryptedFilePath, true);
            String tempDecryptedFilePath = DecryptBlowfishEncryptedFile(tempEncryptedFilePath);
            File.Delete(tempEncryptedFilePath);

            //String renamedFile = tempDecryptedFilePath.Replace(".bft", "").Replace(".bf", "");
            //File.Move(tempDecryptedFilePath, renamedFile);
            FileInfo decryptedFileInfo = new FileInfo(tempDecryptedFilePath);
            String ext = Path.GetExtension(filePath).ToLowerInvariant();
            String contentType = App_Lib.MimeHelper.GetContentType(ext);
            Boolean useInlineContentDisposition = fileInfo.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase);
            FileDownloadUtility.TransmitResumableFile(context, decryptedFileInfo, contentType, fileNameForUser, useInlineContentDisposition);
        }

        private void DownloadRegularFile(HttpContext context, String filePath, String fileNameForUserIn)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            if (fileInfo == null || !fileInfo.Exists || String.IsNullOrEmpty(fileInfo.Extension))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.End();
                return;
            }
            String fileNameForUser = Path.GetFileName(fileNameForUserIn);
            String contentType = App_Lib.MimeHelper.GetContentType(fileInfo.Extension.ToLower());
            Boolean useInlineContentDisposition = fileInfo.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase);
            FileDownloadUtility.TransmitResumableFile(context, fileInfo, contentType, fileNameForUser, useInlineContentDisposition);
        }

       

        #endregion
    }
}