@ECHO OFF

IF "%1"=="" GOTO UseGuide
IF "%2"=="" GOTO UseGuide
MKDIR outputfiles\%2

SET expdays=%1
SET certname=outputfiles\%2\%2
SET cfgfile=openssl.cnf
REM make .pem
openssl req -x509 -nodes -days %expdays% -newkey rsa:1024 -keyout %certname%.pem -out %certname%.pem -config %cfgfile%

REM export .pem as PKCS#12 file .pfx
openssl pkcs12 -export -out %certname%.pfx -in %certname%.pem

REM make .cer from .pem
openssl x509 -outform der -in %certname%.pem -out %certname%.cer
GOTO END

:UseGuide
ECHO gencert expire_days file_name_without_extention
GOTO END
:END