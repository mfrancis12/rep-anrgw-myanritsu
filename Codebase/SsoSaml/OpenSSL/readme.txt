open command prompt and change directory to this folder.
run gencert [[expiredays]] [[filename-withoutextention]].
For example: c:\openssl>gencert 365 lms

Read the instructions carefully and fill out the info.  The common name doesn't matter but you should use FQDN.  Remember the password you use.

Once you generate successfully, the output will go to folder under outputfiles.
Login to login.anritsu.com/siteadmin/home 
Browse to x509 Certificates section
Upload the files accordingly.  Remember to update password for PFX and Expiration date as well.


IDP will use the stored .cer file.
Send the .pfx and password to the Service Provider.  This must use this to sign the SAML messages.