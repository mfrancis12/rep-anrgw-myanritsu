﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Net.Security;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
namespace SpSample.LoginAtIdp.Web
{
    public class Global : System.Web.HttpApplication
    {
        public const string IdPCertKey = "IdPCertKey";
        public const string SPCertKey = "SPCertKey";
        //private const string SPKeyFile = "SPKey.pfx";
        //private const string SPKeyPassword = "password";

        void Application_Start(object sender, EventArgs e)
        {
            // In a test environment, trust all certificates.
            ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteServerCertificate;

            // Load the IdP cert file.
            LoadCertificate(IdPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, WebConfigurationManager.AppSettings["Cert.Idp"]), null);

            //// Load the SP cert file.
            String spCertKeyFile = System.Configuration.ConfigurationManager.AppSettings["Cert.SPKey"];
            String spCertKeyPwd = System.Configuration.ConfigurationManager.AppSettings["Cert.SPKey.Pwd"];
            LoadCertificate(SPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, spCertKeyFile), spCertKeyPwd);

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }
        private static bool ValidateRemoteServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // NOTE: This is a test application with self-signed certificates, so all certificates are trusted.
            return true;
        }

        private static X509Certificate2 LoadCertificate(string cacheKey, string fileName, string password)
        {
            X509Certificate2 cert = new X509Certificate2(fileName, password, X509KeyStorageFlags.MachineKeySet);
            HttpContext.Current.Application[cacheKey] = cert;
            return cert;
        }

        public static X509Certificate2 GetSpCertificate()
        {
            X509Certificate2 x509Certificate = HttpContext.Current.Application[Global.SPCertKey] as X509Certificate2;
            if (x509Certificate == null)
            {
                ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteServerCertificate;
                String spCertKeyFile = System.Configuration.ConfigurationManager.AppSettings["Cert.SPKey"];
                String spCertKeyPwd = System.Configuration.ConfigurationManager.AppSettings["Cert.SPKey.Pwd"];
                x509Certificate = LoadCertificate(SPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, spCertKeyFile), spCertKeyPwd);
            }
            return x509Certificate;
        }

        public static X509Certificate2 GetIdpCertificate()
        {
            X509Certificate2 x509Certificate = HttpContext.Current.Application[Global.IdPCertKey] as X509Certificate2;
            if (x509Certificate == null)
            {
                ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteServerCertificate;
                x509Certificate = LoadCertificate(IdPCertKey, Path.Combine(HttpRuntime.AppDomainAppPath, WebConfigurationManager.AppSettings["Cert.Idp"]), null);
            }
            return x509Certificate;
        }
    }
}
