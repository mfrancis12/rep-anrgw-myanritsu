﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Xml;
using System.Web.Security;
using Atp.Saml;
using Atp.Saml2;
using Atp.Saml2.Binding;

namespace SpSample.LoginAtIdp.Web
{
    public partial class HiddenACS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String wreply = Request.QueryString["wreply"];
            // Process the SAML response returned by the identity provider in response
            // to the authentication request sent by the service provider.

            // Receive the SAML response.
            Atp.Saml2.Response samlResponse;
            string relayState;

            ReceiveResponse(out samlResponse, out relayState);

            if (samlResponse == null) return;


            // Check whether the SAML response indicates success or an error and process accordingly.
            if (samlResponse.IsSuccess())
            {
                ProcessSuccessResponse(samlResponse, relayState, wreply);
            }
            else
            {
                ProcessErrorResponse(samlResponse);
            }
        }

        /// <summary>
        /// Receives the SAML response from the identity provider.
        /// </summary>
        /// <param name="samlResponse"></param>
        /// <param name="relayState"></param>
        private void ReceiveResponse(out Atp.Saml2.Response samlResponse, out string relayState)
        {
            // Determine the identity provider to service provider binding type.
            // We use a query string parameter rather than having separate endpoints per binding.
            string bindingType = WebConfigurationManager.AppSettings["SamlBinding.IdpToSp"];
            switch (bindingType)
            {
                case SamlBindingUri.HttpPost:
                    samlResponse = Atp.Saml2.Response.Create(Request);
                    relayState = samlResponse.RelayState;
                    break;

                //case SamlBindingUri.HttpArtifact:
                //    Saml2ArtifactType0004 httpArtifact = Saml2ArtifactType0004.CreateFromHttpArtifactHttpForm(Request);

                //    // Create an artifact resolve request.
                //    ArtifactResolve artifactResolve = new ArtifactResolve();
                //    artifactResolve.Issuer = new Issuer(GetAbsoluteUrl(this, "~/"));
                //    artifactResolve.Artifact = new Artifact(httpArtifact.ToString());

                //    // Send the artifact resolve request and receive the artifact response.
                //    string spArtifactResponderUrl = WebConfigurationManager.AppSettings["ArtifactIdProviderUrl"];

                //    ArtifactResponse artifactResponse = ArtifactResponse.SendSamlMessageReceiveAftifactResponse(spArtifactResponderUrl, artifactResolve);

                //    // Extract the authentication request from the artifact response.
                //    samlResponse = new Response(artifactResponse.Message);
                //    relayState = httpArtifact.RelayState;
                //    break;

                default:
                    Trace.Write("ServiceProvider", "Invalid identity provider to service provider binding");
                    samlResponse = null;
                    relayState = null;
                    return;
            }

            // Verify the response's signature.
            X509Certificate2 x509Certificate = Global.GetIdpCertificate();

            if (!samlResponse.Validate(x509Certificate))
            {
                throw new System.ApplicationException("The SAML response signature failed to verify.");
            }
        }

        /// <summary>
        /// Processes a successful SAML response.
        /// </summary>
        private void ProcessSuccessResponse(Atp.Saml2.Response samlResponse, string relayState, string wreply)
        {
            // Extract the asserted identity from the SAML response.
            Assertion samlAssertion = (Assertion)samlResponse.Assertions[0];

            // Get the subject name identifier.
            string userName = samlAssertion.Subject.NameId.NameIdentifier;

            // Get the originally requested resource URL from the relay state.
            string resourceUrl = String.IsNullOrEmpty(relayState) ? String.Empty : SamlSettings.CacheProvider.Remove(relayState) as string;

            if (String.IsNullOrEmpty(resourceUrl))
            {
                if(!String.IsNullOrEmpty(wreply)) resourceUrl = wreply;
                if (String.IsNullOrEmpty(resourceUrl))
                {
                    Trace.Write("ServiceProvider", "Nothing in cache");
                    resourceUrl = "/";
                }
            }

            List<KeyValuePair<string, string>> userProperties = new List<KeyValuePair<string, string>>();
            
            if (samlAssertion != null && samlAssertion.AttributeStatements != null)
            {
                foreach (AttributeStatement attstmt in samlAssertion.AttributeStatements)
                {                
                    foreach (Atp.Saml2.Attribute att in attstmt.Attributes)
                    {
                        String values = String.Empty;
                        foreach(Atp.Saml2.AttributeValue av in att.Values)
                        {
                            if(av.Data != null)
                            {
                                values += av.Data.ToString() + ",";
                            }
                        }

                        userProperties.Add(new KeyValuePair<string, string>(att.FriendlyName, values));
                
                    }
                }

                Session["ssUserProperties"] = userProperties;
            }

            // Create a login context for the asserted identity.
            FormsAuthentication.SetAuthCookie(userName, false);

            // Redirect to the originally requested resource URL.
            Response.Redirect(resourceUrl, false);
        }

        private void ProcessErrorResponse(Atp.Saml2.Response samlResponse)
        {
            string errorMessage = null;

            if ((samlResponse.Status.StatusMessage != null))
            {
                errorMessage = samlResponse.Status.StatusMessage.Message;
            }

            string redirectUrl = string.Format("~/error.aspx?error={0}", HttpUtility.UrlEncode(errorMessage));

            Response.Redirect(redirectUrl, false);
        }

        public static string GetAbsoluteUrl(Page page, string relativeUrl)
        {
            return new Uri(page.Request.Url, page.ResolveUrl(relativeUrl)).ToString();
        }
    }
}