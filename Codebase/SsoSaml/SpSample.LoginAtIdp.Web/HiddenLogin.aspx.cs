﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Configuration;
using Atp.Saml;
using Atp.Saml.Binding;
using Atp.Saml2;
using Atp.Saml2.Binding;
using System.Security.Cryptography.X509Certificates;
using Anritsu.AnrCommon.CoreLib;
namespace SpSample.LoginAtIdp.Web
{
    public partial class HiddenLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            String spToIdpBinding = WebConfigurationManager.AppSettings["SamlBinding.SpToIdp"];
            // Create and cache the relay state so we remember which SP resource the user wishes 
            // to access after SSO.
            string spResourceUrl = WebUtility.GetAbsoluteUrl(FormsAuthentication.GetRedirectUrl("", false));
            string relayState = Guid.NewGuid().ToString();
            SamlSettings.CacheProvider.Insert(relayState, spResourceUrl, new TimeSpan(1, 0, 0));

            // Create the authentication request.
            AuthnRequest authnRequest = BuildAuthenticationRequest(spResourceUrl);

            // Send the authentication request to the identity provider over the selected binding.
            string idpUrl = string.Format("{0}?sptkn={1}&wreply={2}", 
                WebConfigurationManager.AppSettings["SingleSignonIdProviderUrl"],
                WebConfigurationManager.AppSettings["SpTokenID"],
                HttpUtility.UrlEncode(spResourceUrl)
                );
            
            switch (spToIdpBinding)
            {
                case SamlBindingUri.HttpRedirect:
                    //X509Certificate2 x509Certificate = (X509Certificate2)Application[Global.SPCertKey];
                    //authnRequest.Redirect(Response, idpUrl, relayState, x509Certificate.PrivateKey);
                    throw new NotImplementedException();
                    break;
                case SamlBindingUri.HttpPost:
                    authnRequest.SendHttpPost(Response, idpUrl, relayState);
                    Response.End();
                    break;

                case SamlBindingUri.HttpArtifact:
                    //string identificationUrl = WebUtility.GetAbsoluteUrl("~/");
                    //Saml2ArtifactType0004 httpArtifact = new Saml2ArtifactType0004(SamlArtifact.GetSourceId(identificationUrl), SamlArtifact.GetHandle());
                    //SamlSettings.CacheProvider.Insert(httpArtifact.ToString(), authnRequest.GetXml(), new TimeSpan(1, 0, 0));
                    //httpArtifact.Redirect(Response, idpUrl, relayState);
                    throw new NotImplementedException();
            }
        }

        private AuthnRequest BuildAuthenticationRequest(String spResourceUrl)
        {
            // Create some URLs to identify the service provider to the identity provider.
            // As we're using the same endpoint for the different bindings, add a query string parameter
            // to identify the binding.
            string issuerUrl = WebUtility.GetAbsoluteUrl("~/");//VirtualPathUtility.ToAbsolute("~/");
            String acsBaseUrl = WebUtility.GetAbsoluteUrl("~/HiddenACS.aspx"); //GetAbsoluteUrl(this, "~/HiddenACS.aspx");//VirtualPathUtility.ToAbsolute("~/HiddenACS.aspx");
            string assertionConsumerServiceUrl = string.Format("{0}?&wreply={1}", 
                acsBaseUrl, 
                HttpUtility.UrlEncode(spResourceUrl)
                );

            // Create the authentication request.
            AuthnRequest authnRequest = new AuthnRequest();
            authnRequest.Destination = WebConfigurationManager.AppSettings["SingleSignonIdProviderUrl"];
            authnRequest.Issuer = new Issuer(issuerUrl);
            authnRequest.ForceAuthn = false;
            authnRequest.NameIdPolicy = new NameIdPolicy(null, null, true);
            authnRequest.ProtocolBinding = SamlBindingUri.HttpPost;
            authnRequest.AssertionConsumerServiceUrl = assertionConsumerServiceUrl;

            // Don't sign if using HTTP redirect as the generated query string is too long for most browsers.
            //if (spToIdPBindingList.SelectedValue != SamlBindingUri.HttpRedirect)
            {
                // Sign the authentication request.
                X509Certificate2 x509Certificate = Global.GetSpCertificate();
                authnRequest.Sign(x509Certificate);
            }
            return authnRequest;

        }
    }
}