﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Security;
using Atp.Saml2;
using Anritsu.AnrCommon.CoreLib;

namespace SpSample.LoginAtIdp.Web
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["idplgo"] == "logout")
            {
                X509Certificate2 x509IdpCert = Global.GetIdpCertificate();
                LogoutResponse logoutResponse = LogoutResponse.Create(Request, x509IdpCert.PublicKey.Key);
                // do something

                Response.Redirect("/default.aspx");
            }
            else
            {
                // Create a logout request.
                LogoutRequest logoutRequest = new LogoutRequest();
                logoutRequest.Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/"));
                logoutRequest.NameId = new NameId(Context.User.Identity.Name);

                // Send the logout request to the IdP over HTTP redirect.
                string logoutUrl = string.Format("{0}?sptkn={1}&lang={3}&wreply={2}",
                    WebConfigurationManager.AppSettings["LogoutIdProviderUrl"],
                    WebConfigurationManager.AppSettings["SpTokenID"],
                    HttpUtility.UrlEncode(WebUtility.GetAbsoluteUrl(FormsAuthentication.GetRedirectUrl("", false))),Request.QueryString["lang"].IsNullOrEmptyString()
                    );

                System.Web.Security.FormsAuthentication.SignOut();
                Session.Abandon();

                X509Certificate2 x509Certificate = Global.GetSpCertificate();
                logoutRequest.Redirect(Response, logoutUrl, null, x509Certificate.PrivateKey);
            }
        }
    }
}