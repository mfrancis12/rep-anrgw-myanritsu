﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Security;
namespace SpSample.LoginAtIdp.Web
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    sb.Append("<li>Login status: logged in</li>");
                    List<KeyValuePair<string, string>> userProperties = Session["ssUserProperties"] as List<KeyValuePair<string, string>>;
                    if (userProperties != null)
                    {                        
                        foreach (KeyValuePair<string, string> kv in userProperties)
                        {
                            sb.AppendFormat("<li>{0}: {1}</li>", kv.Key, kv.Value);
                        }
                    }
                    sb.Append("</ul>");
                    ltrUserInfo.Text = sb.ToString();
                }
            }
        }
    }
}