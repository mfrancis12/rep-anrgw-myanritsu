﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    internal static class DABase
    {
        public class GlobalWebSsoDB
        {
            private static String Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Idp.GlobalWebSsoDB.Schema"); }
            }

            private static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Idp.GlobalWebSsoDB.ConnStr"); }
            }
            
            private static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Idp.GlobalWebSsoDB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

            public static SqlConnection GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ConnStr);
            }
           

            public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
            }

            public class SP
            {
                public const String LoginAttempts_CheckLocked = "[uSP_GW_SSO_Saml_LoginAttempts_CheckLocked]";
                public const String LoginAttempts_Insert = "[uSP_GW_SSO_Saml_LoginAttempts_Insert]";

                public const String LoginHistory_Select = "[uSP_GW_SSO_LoginHistory_Get]";
                public const String LoginHistory_Insert = "[uSP_GW_SSO_LoginHistory_Insert]";

                public const String UserActivity_UpdateOnline = "[uSP_GW_SSO_Saml_UserActivity_UpdateOnline]";
                public const String UserActivity_UpdateLogin = "[uSP_GW_SSO_Saml_UserActivity_UpdateLogin]";
                public const String UserActivity_UpdateLogout = "[uSP_GW_SSO_Saml_UserActivity_UpdateLogout]";

                public const String User_SelectByGWUserId = "[uSP_GW_SSO_Saml_User_SelectByGWUserId]";
                public const String User_SelectByEmailOrUserName = "[uSP_GW_SSO_Saml_User_SelectByEmailOrUserName]";
                public const String User_SelectBySecretKey = "[uSP_GW_SSO_Saml_User_SelectBySecretKey]";
                public const String User_UpdatePassword = "[uSP_GW_SSO_Saml_User_UpdatePassword]";

                public const String ServiceProvider_SelectAll = "[uSP_GW_SSO_Saml_ServiceProvider_SelectAll]";
                public const String ServiceProvider_SelectBySpTokenID = "[uSP_GW_SSO_Saml_ServiceProvider_SelectBySpTokenID]";
                public const String ServiceProvider_SelectByCertID = "[uSP_GW_SSO_ServiceProvider_SelectByCertID]";
                public const String ServiceProvider_Update = "[uSP_GW_SSO_Saml_ServiceProvider_Update]";
                public const String SamlAttributeSet_Attribute_SelectBySpTokenID = "[uSP_GW_SSO_SamlAttributeSet_Attribute_SelectBySpTokenID]";
                public const String SamlAttributeSet_SelectAll = "[uSP_GW_SSO_SamlAttributeSet_SelectAll]";

                public const String SamlLog_Insert = "[uSP_GW_SSO_SamlLog_Insert]";

                public const String SamlCertStore_SelectByCertID = "[uSP_GW_SSO_SamlCertStore_SelectByCertID]";
                public const String SamlCertStore_SelectAll = "[uSP_GW_SSO_SamlCertStore_SelectAll]";
                public const String SamlCertStore_Insert = "[uSP_GW_SSO_SamlCertStore_Insert]";
                public const String SamlCertStore_Update = "[uSP_GW_SSO_SamlCertStore_Update]";

                public const String UserRole_SelectByUserID = "[uSP_GW_SSO_UserRoles_Get]";
                public const String UserPermissionToken_SelectByRoleID = "[uSP_GW_SSO_UserPermissionToken_Get]";

                public const String ISOCountry_SelectByCountryCode = "[uSP_AR_ISO_Country_Select_ByAlpha2Code]";

                public const String SPAudienceRestriction_SelectByServiceProviderID = "[uSP_GW_SSO_SamlAudienceRestriction_SelectByServiceProviderID]";
                public const String SamlSSOUserPasswordMigrate = "[uSP_GW_SSO_Saml_User_UpdatePassword_Migrate]";

                public const String InActiveUser_Check = "[uSP_GW_SSO_Saml_InActiveUser_Check]";
                public const String ResourceStrings_SelectByClass = "[uSP_GW_SSO_Saml_ResourceStrings_SelectByClass]";
                
                #region Verification Project MA-1447

                public const String GetValidationCode = "uSP_GW_SSO_Verification_Get_ValidationCode";
                public const String VerifyValidationCode = "uSP_GW_SSO_Verification_VerifyValidationCode";
                public const String VerifyActiveStatus = "uSP_GW_SSO_Verification_VerifyActiveStatus";

                #endregion
                public const String ContactUSLog = "[uSP_GW_SSO_ContactUS_Insert]";

            }
            #region Newly added code for recover and Reset password

            #region " Forgot Password "
            public static class ResetPassword
            {
                public const string ResetPasswordTokenInfo = "[uSP_GW_SSO_ForgotPassword_CheckToken]";
                public const string ResetPasswordTokenIsUsedUpdate = "[uSP_GW_SSO_ForgotPassword_UpdateUsedToken]";
                public const string ResetPasswordToken = "[uSP_GW_SSO_ForgotPassword_InsertToken]";
                public const string SSOUserGet = "[uSP_GW_SSO_User_Get]";
                public const string CultureGroups_GetGroupId = "uSP_AR_Resources_GetCultureGroupID";
                public const string CultureGroups_GetByCultureGroupId = "uSP_AR_AppResources_CultureGroupMaster_GetCultureGroupInfo";
            }
            #endregion

            #region " Forgot Password "
            public static class RedirectUrls
            {
                public const string RedirectUrl_SelectAll = "[uSP_GW_SSO_RedirectUrls_SelectAll]";
            }
            #endregion

            #region " New Registration"

            public static class NewUserRegistration
            {
                public const string GetSSOUserType = "uSP_GW_SSO_UserTypes_Get";
                public const string InsertNotActivatedSSOUser = "uSP_GW_SSO_User_NotActivated_Insert";
                public const string UpdateUserPassword = "uSP_GW_SSO_User_Password_Update";
                public const string UserInfo_Insert = "uSP_MyAnritsu_UserInfo_Insert";
                public const string GetSsoUserStatus = "uSP_GW_SSO_UserStatus_Get";
                public const string ActivateUserTokenInfo = "uSP_GW_SSO_User_Activated_Check_Token";
                public const string GetUserRoleByRoleTitle = "uSP_GW_SSO_UserRole_SelectByRoleTitle";
                public const string InsertUserUserRoleReference = "uSP_GW_SSO_User_UserRole_Insert";
                public const string InsertActiveSSOUser = "uSP_GW_SSO_User_Active_Insert";
                public const String Country_SelectByCultureGroupId = "[uSP_ContactUs_Countries_Get]";
                public const String State_SelectByCountryAlpha2Code = "[uSP_ContactUs_States_Get]";
                public const String GetCountryName_For_CultureGroupId = "[uSP_ContactUs_CountryName_ForCultureGroupCode_Get]";
                public const string GetSSOUserLoginURL = "uSP_GW_SSO_User_Get_SPLoginURL";
                public const string GetInActiveUserToken = "uSP_GW_SSO_User_InActive_Select_Token";
                public const string InActiveUserSelectByEmailAndUserName = "uSP_GW_SSO_InActiveUser_SelectByEmailAndUserName";
                public const string ExistingActiveInActiveUser_CheckByEmailOrUserName = "uSP_GW_SSO_Saml_ExistingUser_SelectByEmailOrUserName";
                public const string InActiveUserSelectByEmailOrUserName = "uSP_GW_SSO_InActiveUser_SelectByEmailOrUserName";
                #region Verification Project MA-1447
                public const string VerifyLoginNameExists = "uSP_GW_SSO_Verification_NewRegistration_VerifyLoginNameExists";
                public const string VerifyIsNewUser = "uSP_GW_SSO_Verification_NewRegistration_VerifyIsNewUser";
                public const string UserRegistrationStatus = "uSP_GW_SSO_Verification_NewRegistration_UserRegistration_Status";
                public const string GetNewUserToken = "uSP_GW_SSO_Verification_NewRegistration_Get_NewUser_Token";
                public const string NewRegistrationUserInsert = "uSP_GW_SSO_Verification_NewRegistration_User_Insert";
                #endregion

            }

            public static class EditMyAnritsuAccount
            {
                public const string UserInfo_Get = "uSP_MyAnritsu_UserInfo_Get";
                public const string GW_SSO_UserUpdate = "uSP_GW_SSO_User_Update_ByGWUserId";
                public const string GW_SSO_Connect_UserUpdate = "uSP_Sec_UserMembership_SSO_Update";
                public const string UserInfo_Update = "uSP_MyAnritsu_UserInfo_Update";

            }

            public static class ChangeUserEmailAddress
            {
                public const string ChangeEmailRequestInsert = "uSP_GW_SSO_User_EmailChangeRequest_Insert";
                public const string ChangeEmailRequestGet = "uSP_GW_SSO_Get_EmailValidation_RequestToken";
                public const string ChangeEmailLogInsert = "uSP_GW_SSO_User_EmailChangeLog_Insert";
                public const string CheckEmailAddressExists = "uSP_GW_SSO_CheckEmailAddressExist";
            }

            #endregion

            public static void CloseSqlConnection(SqlConnection conn)
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }
            public static object SetDBStrValue(string val)
            {
                if (val == null || val.Trim() == "")
                {
                    return DBNull.Value;
                }

                return val;
            }

            #endregion
        }

        public class GeoIPDB
        {
            private static String Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Idp.GeoIPDB.Schema"); }
            }

            private static String GeoIPConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Idp.DB_GeoIP.ConStr"); }
            }

            private static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Idp.GeoIPDB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

            public static SqlConnection GetOpenedConn()
            {
                return SQLUtility.ConnOpen(GeoIPConnStr);
            }

            public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
            }

            #region "Geo IP"

            public static class GeoIP
            {
                public const string IPCountryLocation_GetByIP = "[uSP_IPCountryLocation_GetByIP]";
                public const string IPCountryRegion_GetByCountryCode = "uSP_IPCountryRegion_GetByCountryCode";
            }
            #endregion
        }
        public class ERPDB
        {
            private static String Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Idp.ERP.Schema"); }
            }

            private static String ERPConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Idp.ERP.ConStr"); }
            }

            private static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Idp.ERP.CmdTimeout"), out timeout);
                    return timeout;
                }
            }

            public static SqlConnection GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ERPConnStr);
            }

            public static object SetDBStrValue(string val)
            {
                if (val == null || val.Trim() == "")
                {
                    return DBNull.Value;
                }

                return val;
            }

            public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, Schema, spName, CommandTimeout);
            }
            public class SP
            {
                public const String ERPUser_GetPassword = "[uSP_GetPasswordByEmailID]";
            }
        }


        public class ConnectDB
        {
            private static String ConnectConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
            }
            public static SqlConnection Connect_GetOpenedConn()
            {
                return SQLUtility.ConnOpen(ConnectConnStr);
            }
            public static String SP_Schema
            {
                get { return ConfigUtility.AppSettingGetValue("Connect.DB.SPSchema"); }
            }

            public static String ConnStr
            {
                get { return ConfigUtility.ConnStrGetValue("Connect.DB.ConnStr"); }
            }

            public static Int32 CommandTimeout
            {
                get
                {
                    int timeout;
                    int.TryParse(ConfigUtility.AppSettingGetValue("Connect.DB.CmdTimeout"), out timeout);
                    return timeout;
                }
            }
            public static SqlCommand MakeCmdSP(SqlConnection conn, SqlTransaction tran, String spName)
            {
                return SQLUtility.MakeSPCmd(conn, tran, ConnectDB.SP_Schema, spName, ConnectDB.CommandTimeout);
            }
            public class StoredProcs
            {

                public const string UpdateUserEmail = "uSP_Sec_UserMembership_SSO_Update";
                public const string ConnectTeamOwner = "uSp_Sso_IsConnectTeamowner";
            }
        }
    }
}
