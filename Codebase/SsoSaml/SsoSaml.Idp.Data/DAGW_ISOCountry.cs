﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_ISOCountry
    {
        public static DataRow Select_ByAlpha2Code(String alpha2CountryCode)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                return Select_ByAlpha2Code(alpha2CountryCode, conn, null);
            }
        }

        public static DataRow Select_ByAlpha2Code(String alpha2CountryCode, SqlConnection conn, SqlTransaction tran)
        {
            if (string.IsNullOrEmpty(alpha2CountryCode) || alpha2CountryCode.Length != 2) return null;
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.ISOCountry_SelectByCountryCode);
            cmd.Parameters.AddWithValue("@Alpha2Code", alpha2CountryCode);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                return Country_SelectByCultureGroupId(cultureGroupId, conn);
            }
        }

        public static DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId, SqlConnection conn)
        {
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.Country_SelectByCultureGroupId);
            cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataTable State_SelectByCountry(string countryValue)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.State_SelectByCountryAlpha2Code);
                cmd.Parameters.AddWithValue("@CountryValue", countryValue);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static string GetCountryNameForCultureGroupId(int cultureGroupId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetCountryName_For_CultureGroupId);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                string countryName = (cmd.ExecuteScalar() ?? "").ToString();
                return countryName;
            }
        }
    }
}
