﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_LoginAttempts
    {
        public static DataRow CheckLocked(String emailAddress)
        {
            if (emailAddress.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.LoginAttempts_CheckLocked);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress.Trim());
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        internal static void Insert(String emailAddress, String ipAddress, Boolean isLoginSuccess
            , SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.LoginAttempts_Insert);
            cmd.Parameters.AddWithValue("@EmailAddress", emailAddress.ConvertNullToEmptyString().Trim());
            cmd.Parameters.AddWithValue("@IPAddress", emailAddress.ConvertNullToEmptyString().Trim());
            SQLUtility.SqlParam_AddAsBoolean(ref cmd, "@LoginSuccess", isLoginSuccess);
            cmd.ExecuteNonQuery();
        }

        public static void LogFailedLogin(String emailAddress, String ipAddress)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                Insert(emailAddress, ipAddress, false, conn, null);
            }
        }

        public static void LogSuccessLogin(Int32 gwUserId, String emailAddress, String ipAddress)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlTransaction tran = conn.BeginTransaction();
                try
                {
                    DAGW_SSO_LoginHistory.Insert(gwUserId, ipAddress, conn, tran);
                    Insert(emailAddress, ipAddress, true, conn, tran);
                    tran.Commit();
                }
                catch { tran.Rollback(); }
            }
        }
    }
}
