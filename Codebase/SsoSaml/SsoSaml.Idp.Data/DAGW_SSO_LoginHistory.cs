﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_LoginHistory
    {
        internal static void Insert(Int32 gwUserId, String ipAddress, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.LoginHistory_Insert);
            cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
            cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
            cmd.ExecuteNonQuery();
        }

        public static void Insert(Int32 gwUserId, String ipAddress)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                Insert(gwUserId, ipAddress, conn, null);
            }
        }

        public static DataTable Select(Int32 gwUserId, Int32 count)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.LoginHistory_Select);
                cmd.Parameters.AddWithValue("@UserId", gwUserId);
                cmd.Parameters.AddWithValue("@Count", count);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
