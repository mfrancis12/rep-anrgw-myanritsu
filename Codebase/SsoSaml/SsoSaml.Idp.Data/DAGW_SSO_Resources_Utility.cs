﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_Resources_Utility
    {
        public static DataTable ResourceStrings_SelectByClass(string className, string language)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.ResourceStrings_SelectByClass);
                cmd.Parameters.AddWithValue("@ClassName", className);
                cmd.Parameters.AddWithValue("@Language", language);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

    }
}
