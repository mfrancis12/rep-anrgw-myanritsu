﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_SamlAttributeSet
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlAttributeSet_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
