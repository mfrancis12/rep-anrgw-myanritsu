﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_SamlAttributeSet_Attribute
    {
        public static DataTable SelectBySpTokenID(Guid spTokenId, SqlConnection conn, SqlTransaction tran)
        {
            if (spTokenId.IsNullOrEmptyGuid()) return null;
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.SamlAttributeSet_Attribute_SelectBySpTokenID);
            SQLUtility.SqlParam_AddAsGuid(ref cmd, "@SpTokenID", spTokenId);
            return SQLUtility.FillInDataTable(cmd);
        }

        public static DataTable SelectBySpTokenID(Guid spTokenId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                return SelectBySpTokenID(spTokenId, conn, null);
            }
        }
    }
}
