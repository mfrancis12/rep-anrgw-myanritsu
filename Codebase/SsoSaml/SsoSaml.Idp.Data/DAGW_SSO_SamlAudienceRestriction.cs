﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_SamlAudienceRestriction
    {
        public static DataTable SelectByServiceProviderID(int serviceProviderId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                return SelectByServiceProviderID(serviceProviderId, conn, null);
            }
        }
        public static DataTable SelectByServiceProviderID(int serviceProviderId, SqlConnection conn, SqlTransaction tran)
        {
            if (serviceProviderId < 0) return null;
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.SPAudienceRestriction_SelectByServiceProviderID);
            SQLUtility.SqlParam_AddAsInt(ref cmd, "@ServiceProviderID", serviceProviderId);
            return SQLUtility.FillInDataTable(cmd);
        }
    }
}
