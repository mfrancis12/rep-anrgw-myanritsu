﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_SamlCertStore
    {
        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlCertStore_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataRow Select(Int32 certId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                return Select(certId, conn, null);
            }
        }

        public static DataRow Select(Int32 certId, SqlConnection conn, SqlTransaction tran)
        {
            SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, tran, DABase.GlobalWebSsoDB.SP.SamlCertStore_SelectByCertID);
            cmd.Parameters.AddWithValue("@CertID", certId);
            return SQLUtility.FillInDataRow(cmd);
        }

        public static DataRow Insert(String internalName, Byte[] x509Cer, Byte[] x509Pfx, String x509PfxPwd, DateTime x509PfxExpOnUtc, String internalNote)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlCertStore_Insert);
                cmd.Parameters.AddWithValue("@InternalName", internalName);
                SQLUtility.SqlParam_AddAsVarBinary(ref cmd, "@x509Cer", x509Cer);
                SQLUtility.SqlParam_AddAsVarBinary(ref cmd, "@x509Pfx", x509Pfx);
                cmd.Parameters.AddWithValue("@x509PfxPwd", x509PfxPwd);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@x509PfxExpOnUTC", x509PfxExpOnUtc);
                cmd.Parameters.AddWithValue("@InternalNote", internalNote);
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r == null) return null;
                Int32 certId = ConvertUtility.ConvertToInt32( r["CertID"], 0 );
                return Select(certId, conn, null);
            }
        }

        public static void Update(Int32 certId, String internalName, Byte[] x509Cer, Byte[] x509Pfx, String x509PfxPwd, DateTime x509PfxExpOnUtc, String internalNote)
        {
            if (certId < 1) return;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlCertStore_Update);
                cmd.Parameters.AddWithValue("@CertID", certId);
                cmd.Parameters.AddWithValue("@InternalName", internalName);
                SQLUtility.SqlParam_AddAsVarBinary(ref cmd, "@x509Cer", x509Cer);
                SQLUtility.SqlParam_AddAsVarBinary(ref cmd, "@x509Pfx", x509Pfx);
                cmd.Parameters.AddWithValue("@x509PfxPwd", x509PfxPwd);
                SQLUtility.SqlParam_AddAsDateTime(ref cmd, "@x509PfxExpOnUTC", x509PfxExpOnUtc);
                cmd.Parameters.AddWithValue("@InternalNote", internalNote);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
