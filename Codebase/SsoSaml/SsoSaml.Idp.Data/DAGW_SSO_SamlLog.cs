﻿using System;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_SamlLog
    {
        public static void Insert(String spToken, String logType, String parameters, String samlXml, String error)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlLog_Insert);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SpToken", spToken);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@LogType", logType);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@Params", parameters);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@SamlXml", samlXml);
                SQLUtility.SqlParam_AddAsString(ref cmd, "@ErrorMsg", error);
                cmd.ExecuteNonQuery();
            }
        }

    }
}
