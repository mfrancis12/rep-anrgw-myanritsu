﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_ServiceProvider
    {
        public static DataRow SelectBySpTokenID(Guid spTokenId, out DataTable spAttributes, out DataTable spAudiences)
        {
            spAttributes = null;
            spAudiences = null;
            if (spTokenId.IsNullOrEmptyGuid()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.ServiceProvider_SelectBySpTokenID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "@SpTokenID", spTokenId);
                DataRow r = SQLUtility.FillInDataRow(cmd);
                if (r != null)
                {
                    string spid=Convert.ToString(r["ServiceProviderID"]);
                    int serviceProviderId;
                    Int32.TryParse(spid, out serviceProviderId);
                    spAttributes = DAGW_SSO_SamlAttributeSet_Attribute.SelectBySpTokenID(spTokenId, conn, null);
                    if(serviceProviderId>0)
                        spAudiences = DAGW_SSO_SamlAudienceRestriction.SelectByServiceProviderID(serviceProviderId, conn, null);
                }
                return r;
            }
        }

        public static DataTable SelectAll()
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.ServiceProvider_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }

        }

        public static DataTable SelectByCertID(Int32 certId)
        {
            if (certId < 1) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.ServiceProvider_SelectByCertID);
                cmd.Parameters.AddWithValue("@CertID", certId);
                return SQLUtility.FillInDataTable(cmd);
            }

        }
    }
}
