﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_UrlRedirect
    {
        public static DataTable Select_RedirectUrls()
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.RedirectUrls.RedirectUrl_SelectAll);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
    }
}
