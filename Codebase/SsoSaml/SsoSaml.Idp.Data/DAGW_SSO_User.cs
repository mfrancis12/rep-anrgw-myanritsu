﻿using System;
using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_User
    {
        public static DataRow SelectByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_SelectByEmailOrUserName);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());
                DataTable tb = SQLUtility.FillInDataTable(cmd);
                if (tb == null || tb.Rows.Count != 1) return null;
                return tb.Rows[0];
            }
        }

        public static DataRow SelectByGWUserId(Int32 gwUserId)
        {
            if (gwUserId < 1) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_SelectByGWUserId);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow SelectBySecretKey(Guid secretKey)
        {
            if (secretKey.IsNullOrEmptyGuid()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_SelectBySecretKey);
                cmd.Parameters.AddWithValue("@SecretKey", secretKey);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static Int32 UpdatePassword(Int32 gwUserId, String newEncryptedPwd, String ipAddress)
        {
            if (gwUserId < 1 || newEncryptedPwd.IsNullOrEmptyString()) return 0;
            if (ipAddress.IsNullOrEmptyString()) ipAddress = WebUtility.GetUserIP();
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_UpdatePassword);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@EncryptedPassword", newEncryptedPwd.Trim());
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress.Trim());
                return cmd.ExecuteNonQuery();
            }
        }

        public static Int32 UpdatePassword(Int32 gwUserId, String newEncryptedPwd, bool passwordMigrate, String ipAddress)
        {
            if (gwUserId < 1 || newEncryptedPwd.IsNullOrEmptyString()) return 0;
            if (ipAddress.IsNullOrEmptyString()) ipAddress = WebUtility.GetUserIP();
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_UpdatePassword);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@EncryptedPassword", newEncryptedPwd.Trim());
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress.Trim());
                cmd.Parameters.AddWithValue("@Password_Migrate", passwordMigrate);
                return cmd.ExecuteNonQuery();
            }
        }

        public static DataTable UserRole_SelectByGWUserId(Int32 gwUserId)
        {
            if (gwUserId < 1) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.UserRole_SelectByUserID);
                cmd.Parameters.AddWithValue("@UserId", gwUserId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataTable UserPermissionTokens_SelectByRoleId(Guid roleId)
        {
            if (roleId.IsNullOrEmptyGuid()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.UserPermissionToken_SelectByRoleID);
                SQLUtility.SqlParam_AddAsGuid(ref cmd, "RoleId", roleId);
                return SQLUtility.FillInDataTable(cmd);
            }
        }
        public static Int32 UpdatePassword(Int32 gwUserId, String newEncryptedPwd, String ipAddress, bool isUpdatePwd)
        {
            if (gwUserId < 1 || newEncryptedPwd.IsNullOrEmptyString()) return 0;
            if (ipAddress.IsNullOrEmptyString()) ipAddress = WebUtility.GetUserIP();
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.User_UpdatePassword);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@EncryptedPassword", newEncryptedPwd.Trim());
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress.Trim());
                cmd.Parameters.AddWithValue("@IsUpdatePwd", isUpdatePwd);
                return cmd.ExecuteNonQuery();
            }
        }
        public static void UpdateResetPasswordTokenIsUsed(Guid token)
        {
            if (token == Guid.Empty) return;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ResetPassword.ResetPasswordTokenIsUsedUpdate);
                cmd.Parameters.AddWithValue("@Token", token.ToString().ToUpper());
                cmd.ExecuteNonQuery();
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
            }
        }

        public static DataRow InsertResetPasswordToken(int gwUserId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ResetPassword.ResetPasswordToken);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow GetResetPasswordTokenInfo(Guid token)
        {
            if (token == Guid.Empty) return null;

            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ResetPassword.ResetPasswordTokenInfo);
                cmd.Parameters.AddWithValue("@Token", DABase.GlobalWebSsoDB.SetDBStrValue(token.ToString().ToUpper()));
                return SQLUtility.FillInDataRow(cmd);
            }
        }


        // for New User-Registration Dec-2013

        public static DataTable GetSSOUserType()
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetSSOUserType);
                return SQLUtility.FillInDataTable(cmd);
            }
        }

        public static DataSet InsertNotActivatedSSOUser(GWSsoUserType newUser)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.NewRegistrationUserInsert);
                cmd.Parameters.AddWithValue("@EmailAddress", newUser.EmailAddress);
                cmd.Parameters.AddWithValue("@LoginName", newUser.LoginName);
                cmd.Parameters.AddWithValue("@FirstName", newUser.FirstName);
                cmd.Parameters.AddWithValue("@LastName", newUser.LastName);
                cmd.Parameters.AddWithValue("@EncryptedPassword", newUser.EncryptedTempPassword);
                cmd.Parameters.AddWithValue("@UserStatusId", newUser.UserStatus.UserStatusId);
                cmd.Parameters.AddWithValue("@PrimaryCultureCode", newUser.PrimaryCultureCode);
                cmd.Parameters.AddWithValue("@JobTitle", newUser.JobTitle);
                cmd.Parameters.AddWithValue("@CompanyName", newUser.CompanyName);
                cmd.Parameters.AddWithValue("@StreetAddress1", newUser.StreetAddress1);
                cmd.Parameters.AddWithValue("@StreetAddress2", newUser.StreetAddress2);
                cmd.Parameters.AddWithValue("@City", newUser.City);
                cmd.Parameters.AddWithValue("@State", newUser.State);
                cmd.Parameters.AddWithValue("@ZipCode", newUser.PostalCode);
                cmd.Parameters.AddWithValue("@CountryCode", newUser.CountryCode);
                cmd.Parameters.AddWithValue("@PhoneNumber", newUser.PhoneNumber);
                cmd.Parameters.AddWithValue("@FaxNumber", newUser.FaxNumber);
                cmd.Parameters.AddWithValue("@RegisteredFromURL", newUser.RegisteredFromURL);
                cmd.Parameters.AddWithValue("@UserTypeID", newUser.UserType.UserTypeId);
                cmd.Parameters.AddWithValue("@SecretKey", newUser.SecToken);
                cmd.Parameters.AddWithValue("@AcceptEmail", newUser.AcceptsEmail);
                cmd.Parameters.AddWithValue("@firstnameinRoman", newUser.FirstNameInRoman);
                cmd.Parameters.AddWithValue("@lastNameinRoamn", newUser.LastNameInRoman);
                cmd.Parameters.AddWithValue("@companyinRoman", newUser.CompanyInRoman);
                cmd.Parameters.AddWithValue("@returnURL", newUser.ReturnURL);
                cmd.Parameters.AddWithValue("@sptoken", newUser.SpToken);
                cmd.Parameters.AddWithValue("@browser", newUser.Browser);
                cmd.Parameters.AddWithValue("@useragent", newUser.UserAgent);
                cmd.Parameters.AddWithValue("@clientIP", newUser.ClientIP);
                cmd.Parameters.AddWithValue("@IsIndividualEmailAddress", newUser.IsIndividualEmailAddress);
                cmd.Parameters.AddWithValue("@IsNewUser", newUser.IsNewUser);
                return SQLUtility.FillInDataSet(cmd, "usernotactive");
            }
        }

        public static DataRow UpdateUserPassword(int userId, string encryptedpassword, string spToken, bool isTemp)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.UpdateUserPassword);
                cmd.Parameters.AddWithValue("@userid", userId);
                cmd.Parameters.AddWithValue("@encryptedpassword", encryptedpassword);
                cmd.Parameters.AddWithValue("@sptoken", spToken);
                cmd.Parameters.AddWithValue("@istemp", isTemp);
                //cmd.ExecuteNonQuery();
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void InsertUserInfo(int userId, bool sendNewsMail)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.UserInfo_Insert);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@SendNewsMail", (sendNewsMail ? "1" : "0"));
                cmd.ExecuteNonQuery();
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
            }
        }

        public static DataRow GetUserStatus(int statusid)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetSsoUserStatus);
                cmd.Parameters.AddWithValue("@userstatusId", statusid);
                return SQLUtility.FillInDataRow(cmd);
            }
        }
       
        public static DataRow GetActiveUserTokenInfo(Guid token)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.ActivateUserTokenInfo);
                cmd.Parameters.AddWithValue("@token", token);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow GetUserRoleByRoleTitle(string title)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetUserRoleByRoleTitle);
                cmd.Parameters.AddWithValue("@userRoleTitle", title);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static void InsertUserUserRoleReference(int gwUserId, Guid roleId)
        {

            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.InsertUserUserRoleReference);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@GW_UserRoleId", roleId);
                cmd.ExecuteNonQuery();
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
            }
        }

        public static DataSet InsertActiveSSOUser(int gwTempUserId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.InsertActiveSSOUser);
                cmd.Parameters.AddWithValue("@UserId", gwTempUserId);

                return SQLUtility.FillInDataSet(cmd);

            }
        }

        public static DataSet SelectExistingUserByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.InActiveUserSelectByEmailOrUserName);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());

                return SQLUtility.FillInDataSet(cmd);

            }
        }

        public static DataTable GetUserInfo(int userId)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.EditMyAnritsuAccount.UserInfo_Get);
                cmd.Parameters.AddWithValue("@UserId", userId);
                DataTable tb = SQLUtility.FillInDataTable(cmd);

                return tb;
            }
        }

        public static void UpdateGWUser(GWSsoUserType userToUpdate, bool isTempPwd)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.EditMyAnritsuAccount.GW_SSO_UserUpdate);
                cmd.Parameters.AddWithValue("@GWUserId", userToUpdate.UserId);
                cmd.Parameters.AddWithValue("@EmailAddress", userToUpdate.EmailAddress);
                cmd.Parameters.AddWithValue("@LoginName", userToUpdate.LoginName);
                cmd.Parameters.AddWithValue("@FirstName", userToUpdate.FirstName);
                cmd.Parameters.AddWithValue("@MiddleName", userToUpdate.MiddleName);
                cmd.Parameters.AddWithValue("@LastName", userToUpdate.LastName);
                cmd.Parameters.AddWithValue("@EncryptedPassword", userToUpdate.EncryptedTempPassword);
                cmd.Parameters.AddWithValue("@UserStatusId", userToUpdate.UserStatus.UserStatusId);
                cmd.Parameters.AddWithValue("@PrimaryCultureCode", userToUpdate.PrimaryCultureCode.Trim());
                cmd.Parameters.AddWithValue("@JobTitle", userToUpdate.JobTitle);
                cmd.Parameters.AddWithValue("@CompanyName", userToUpdate.CompanyName);
                cmd.Parameters.AddWithValue("@StreetAddress1", userToUpdate.StreetAddress1);
                cmd.Parameters.AddWithValue("@StreetAddress2", userToUpdate.StreetAddress2);
                cmd.Parameters.AddWithValue("@City", userToUpdate.City);
                cmd.Parameters.AddWithValue("@State", userToUpdate.State);
                cmd.Parameters.AddWithValue("@ZipCode", userToUpdate.PostalCode);
                cmd.Parameters.AddWithValue("@CountryCode", userToUpdate.CountryCode);
                cmd.Parameters.AddWithValue("@PhoneNumber", userToUpdate.PhoneNumber);
                cmd.Parameters.AddWithValue("@FaxNumber", userToUpdate.FaxNumber);
                cmd.Parameters.AddWithValue("@RegisteredFromURL", userToUpdate.RegisteredFromURL);
                cmd.Parameters.AddWithValue("@FirstNameInRoman", userToUpdate.FirstNameInRoman);
                cmd.Parameters.AddWithValue("@LastNameInRoman ", userToUpdate.LastNameInRoman);
                cmd.Parameters.AddWithValue("@CompanyInRoman ", userToUpdate.CompanyInRoman);
                cmd.Parameters.AddWithValue("@IsIndividualEmailAddress", userToUpdate.IsIndividualEmailAddress);
                cmd.Parameters.AddWithValue("@UserTypeID", userToUpdate.UserType.UserTypeId);
                cmd.Parameters.AddWithValue("@IsTempPassword", isTempPwd);

                cmd.ExecuteNonQuery();
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);

            }
        }

        public static void UpdateConnectUserInfo(GWSsoUserType userToUpdate)
        {
            using (SqlConnection conn = DABase.ConnectDB.Connect_GetOpenedConn())
            {
                SqlCommand cmd = DABase.ConnectDB.MakeCmdSP(conn, null, DABase.ConnectDB.StoredProcs.UpdateUserEmail);
                cmd.Parameters.AddWithValue("@GWUserId", userToUpdate.UserId);
                cmd.Parameters.AddWithValue("@FirstName", userToUpdate.FirstName);
                cmd.Parameters.AddWithValue("@MiddleName", userToUpdate.MiddleName);
                cmd.Parameters.AddWithValue("@LastName", userToUpdate.LastName);
                cmd.Parameters.AddWithValue("@JobTitle", userToUpdate.JobTitle);
                cmd.Parameters.AddWithValue("@StreetAddress1", userToUpdate.StreetAddress1);
                cmd.Parameters.AddWithValue("@StreetAddress2", userToUpdate.StreetAddress2);
                cmd.Parameters.AddWithValue("@City", userToUpdate.City);
                cmd.Parameters.AddWithValue("@State", userToUpdate.State);
                cmd.Parameters.AddWithValue("@ZipCode", userToUpdate.PostalCode);
                cmd.Parameters.AddWithValue("@CountryCode", userToUpdate.CountryCode);
                cmd.Parameters.AddWithValue("@PhoneNumber", userToUpdate.PhoneNumber);
                cmd.Parameters.AddWithValue("@FaxNumber", userToUpdate.FaxNumber);
                cmd.Parameters.AddWithValue("@Userid", userToUpdate.LoginName);
                cmd.Parameters.AddWithValue("@IsEmailChange", 0);
                cmd.Parameters.AddWithValue("@CompanyName", userToUpdate.CompanyName);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userToUpdate.EmailAddress);
                cmd.ExecuteNonQuery();
            }
        }
        public static int UpdateConnectUserEmailAddress(int gwUserId, string userOldEmail, string userNewEmail)
        {
            using (var conn = DABase.ConnectDB.Connect_GetOpenedConn())
            {
                var cmd = DABase.ConnectDB.MakeCmdSP(conn, null, DABase.ConnectDB.StoredProcs.UpdateUserEmail);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userOldEmail);
                cmd.Parameters.AddWithValue("@UserNewEmailAddress", userNewEmail);
                cmd.Parameters.AddWithValue("@IsEmailChange", 1);
                return cmd.ExecuteNonQuery();
            }
        }
        public static DataTable ConnectUser_IsTeamOwner(GWSsoUserType userToUpdate)
        {
            using (SqlConnection conn = DABase.ConnectDB.Connect_GetOpenedConn())
            {
                SqlCommand cmd = DABase.ConnectDB.MakeCmdSP(conn, null, DABase.ConnectDB.StoredProcs.ConnectTeamOwner);
                cmd.Parameters.AddWithValue("@GwUserId", userToUpdate.UserId);
                return SQLUtility.FillInDataTable(cmd);                
            }
        }

        public static void UpdateUserInfo(int userId, bool sendNewsMail)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.EditMyAnritsuAccount.UserInfo_Update);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@SendNewsMail", (sendNewsMail ? "1" : "0"));
                cmd.ExecuteNonQuery();
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
            }
        }

        public static DataRow GetUserERPPasswordByEmailId(string emailId)
        {
            if (emailId.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.ERPDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.ERPDB.MakeCmdSP(conn, null, DABase.ERPDB.SP.ERPUser_GetPassword);
                cmd.Parameters.AddWithValue("@EmailID", DABase.ERPDB.SetDBStrValue(emailId));
                return SQLUtility.FillInDataRow(cmd);
            }
        }
        public static void UpdatePasswordMigrateFlag(Int32 gwUserId, bool passwordMigrate)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlSSOUserPasswordMigrate);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@Password_Migrate", passwordMigrate);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataRow Get_SpLoginURL_By_SpTokenId(Guid tokenid)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetSSOUserLoginURL);
                cmd.Parameters.AddWithValue("@spTokenId", tokenid);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataSet Get_InActive_UserToken(Guid tokenid)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetInActiveUserToken);
                cmd.Parameters.AddWithValue("@token", tokenid);
                return SQLUtility.FillInDataSet(cmd);
            }
        }

      
        public static DataRow CheckInActiveUser(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.InActiveUser_Check);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());
                DataTable tb = SQLUtility.FillInDataTable(cmd);
                if (tb == null || tb.Rows.Count != 1) return null;
                return tb.Rows[0];
            }
        }
       
        public static bool InActiveUserSelectByEmailAndUserName(String emailAddress, String userName)
        {
            if (emailAddress.IsNullOrEmptyString()) return false;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.InActiveUserSelectByEmailAndUserName);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress.Trim());
                cmd.Parameters.AddWithValue("@LoginName", userName.Trim());
                cmd.Parameters.Add("@IsExist", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                bool isexixt = Convert.ToBoolean(cmd.Parameters["@IsExist"].Value);
                return isexixt;

            }
        }
       
        public static DataSet SelectExistingActiveInActiveUserByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.ExistingActiveInActiveUser_CheckByEmailOrUserName);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());

                return SQLUtility.FillInDataSet(cmd);

            }
        }

        public static DataRow InsertUserEmailChangeRequest(int gwUserId, String userOldEmailAddress, String userNewEmailAddress, int cultureGroupId)
        {
            using (var conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                var cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ChangeUserEmailAddress.ChangeEmailRequestInsert);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userOldEmailAddress);
                cmd.Parameters.AddWithValue("@UserNewEmailAddress", userNewEmailAddress);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                var tbRequests = SQLUtility.FillInDataTable(cmd);
                if (tbRequests == null || tbRequests.Rows.Count != 1) return null;
                return tbRequests.Rows[0];
            }
        }
        public static DataRow GetUserEmailChangeRequest(Guid token)
        {
            using (var conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                var cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ChangeUserEmailAddress.ChangeEmailRequestGet);
                cmd.Parameters.AddWithValue("@token", token);
                var tbRequests = SQLUtility.FillInDataTable(cmd);
                if (tbRequests == null || tbRequests.Rows.Count < 1) return null;
                return tbRequests.Rows[0];
            }
        }

        public static void InsertUserEmailChangeLog(int gwUserId, String userOldEmailAddress, String userNewEmailAddress, Guid token, int cultureGroupId)
        {
            using (var conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                var cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ChangeUserEmailAddress.ChangeEmailLogInsert);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@UserOldEmailAddress", userOldEmailAddress);
                cmd.Parameters.AddWithValue("@UserNewEmailAddress", userNewEmailAddress);
                cmd.Parameters.AddWithValue("@token", token);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                cmd.ExecuteNonQuery();
            }
        }

        public static bool CheckEmailAddressExist(string emailAddress)
        {
            using (var conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                var cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ChangeUserEmailAddress.CheckEmailAddressExists);
                cmd.Parameters.AddWithValue("@UserEmailAddress", emailAddress);
                var tbRequests = SQLUtility.FillInDataTable(cmd);
                if (tbRequests == null || tbRequests.Rows.Count < 1) return false;
                return Convert.ToBoolean(tbRequests.Rows[0]["EmailExists"]);
            }
        }

        #region Verification Project MA-1447
     
        public static bool VerifyValidationCode(string emailaddress, string validationcode)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.VerifyValidationCode);

                cmd.Parameters.AddWithValue("@EmailAddress", emailaddress);
                cmd.Parameters.AddWithValue("@ValidationCode", validationcode);
                cmd.Parameters.Add("@IsExist", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                bool isexixt = Convert.ToBoolean(cmd.Parameters["@IsExist"].Value);
                return isexixt;

            }
        }
        public static DataRow VerifyActiveStatus(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.VerifyActiveStatus);
                cmd.Parameters.AddWithValue("@EmailAddressOrUserName", emailAddressOrUserName.Trim());
                DataTable tb = SQLUtility.FillInDataTable(cmd);
                if (tb == null || tb.Rows.Count != 1) return null;
                return tb.Rows[0];
            }
        }
        public static bool VerifyIsNewUser(String emailAddress)
        {
            if (emailAddress.IsNullOrEmptyString()) return false;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.VerifyIsNewUser);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress.Trim());
                cmd.Parameters.Add("@IsExist", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                bool isexixt = Convert.ToBoolean(cmd.Parameters["@IsExist"].Value);
                return isexixt;

            }
        }
        public static DataSet GetNewUserToken(string EmailAddress)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.GetNewUserToken);
                cmd.Parameters.AddWithValue("@email", EmailAddress);
                return SQLUtility.FillInDataSet(cmd);
            }
        }
        public static DataSet CheckUserRegistrationStatus(Guid tokenid)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.UserRegistrationStatus);
                cmd.Parameters.AddWithValue("@token", tokenid);
                return SQLUtility.FillInDataSet(cmd);
            }
        }
       
        public static bool VerifyLoginNameExists(String emailAddress, String userName)
        {
            if (emailAddress.IsNullOrEmptyString()) return false;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.NewUserRegistration.VerifyLoginNameExists);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress.Trim());
                cmd.Parameters.AddWithValue("@LoginName", userName.Trim());
                cmd.Parameters.Add("@IsAvilable", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                bool isexixt = Convert.ToBoolean(cmd.Parameters["@IsAvilable"].Value);
                return isexixt;

            }
        }

        public static DataSet GetValidationCode(String emailAddressOrUserName, int randomnumber, string requestedBy,string wereplyurl)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.GetValidationCode);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddressOrUserName.Trim());
                cmd.Parameters.AddWithValue("@RandomNumber", randomnumber);
                cmd.Parameters.AddWithValue("@WereplyUrl", wereplyurl);
                cmd.Parameters.AddWithValue("@CreatedBy", requestedBy);

                return SQLUtility.FillInDataSet(cmd);

            }
        }
        #endregion
    }
}
