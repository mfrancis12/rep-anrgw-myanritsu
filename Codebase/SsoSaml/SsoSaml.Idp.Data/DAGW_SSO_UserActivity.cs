﻿using System;
using System.Data.SqlClient;


namespace Anritsu.SsoSaml.Idp.Data
{
    public static class DAGW_SSO_UserActivity
    {
        public static void UpdateOnline(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.UserActivity_UpdateOnline);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@ServiceProviderID", serviceProviderId);
                cmd.Parameters.AddWithValue("@UserIP", userIp);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateLogin(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.UserActivity_UpdateLogin);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@ServiceProviderID", serviceProviderId);
                cmd.Parameters.AddWithValue("@UserIP", userIp);
                cmd.ExecuteNonQuery();
            }
        }
        public static void UpdateLogout(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.UserActivity_UpdateLogout);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@ServiceProviderID", serviceProviderId);
                cmd.Parameters.AddWithValue("@UserIP", userIp);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdatePasswordMigrateFlag(Int32 gwUserId, bool passwordMigrate)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.SamlSSOUserPasswordMigrate);
                cmd.Parameters.AddWithValue("@GWUserId", gwUserId);
                cmd.Parameters.AddWithValue("@Password_Migrate", passwordMigrate);
                cmd.ExecuteNonQuery();
            }
        }

        #region ContacUS Log
        public static bool InsertContactUSLog(string FirstName, string LastName, string EmailAddress, string URL, string Comments, int CultureGroupID)
        {
            bool result = false;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.SP.ContactUSLog);
                cmd.Parameters.AddWithValue("@CultureGroupId", CultureGroupID);
                cmd.Parameters.AddWithValue("@Url", URL.Trim());
                cmd.Parameters.AddWithValue("@Comments", Comments.Trim());
                cmd.Parameters.AddWithValue("@EmailId", EmailAddress.Trim());
                cmd.Parameters.AddWithValue("@FirstName", FirstName.Trim());
                cmd.Parameters.AddWithValue("@LastName", LastName.Trim());
                cmd.ExecuteNonQuery();
               
            }
            return result;
        }
        #endregion

    }
}
