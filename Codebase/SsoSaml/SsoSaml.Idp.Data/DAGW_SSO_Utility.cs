﻿using System.Data;
using System.Data.SqlClient;
using Anritsu.AnrCommon.CoreLib; 

namespace Anritsu.SsoSaml.Idp.Data
{
    public class DAGW_SSO_Utility
    {
        public static int GetCultureGroupIdForCultureCode(string cultureCode)
        {
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ResetPassword.CultureGroups_GetGroupId);
                cmd.Parameters.AddWithValue("@CultureCode", cultureCode);
                int cultureGroupId;
                try
                {
                    cultureGroupId = int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch
                {
                    cultureGroupId = 1;
                }
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
                return cultureGroupId;
            }
        }


        public static DataRow GetByCultureGroupId(int cultureGroupId)
        {
            DataRow row;
            using (SqlConnection conn = DABase.GlobalWebSsoDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GlobalWebSsoDB.ResetPassword.CultureGroups_GetByCultureGroupId);
                cmd.Parameters.AddWithValue("@CultureGroupId", cultureGroupId);
                row = SQLUtility.FillInDataRow(cmd);
                DABase.GlobalWebSsoDB.CloseSqlConnection(conn);
            }               
            return row;
        }

        public static DataRow GetCountryByIP(string ipAddress)
        {
            if (ipAddress.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GeoIPDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GeoIPDB.GeoIP.IPCountryLocation_GetByIP);
                cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                return SQLUtility.FillInDataRow(cmd);
            }
        }

        public static DataRow GetRegionByCountryCode(string countryCode)
        {
            if (countryCode.IsNullOrEmptyString()) return null;
            using (SqlConnection conn = DABase.GeoIPDB.GetOpenedConn())
            {
                SqlCommand cmd = DABase.GlobalWebSsoDB.MakeCmdSP(conn, null, DABase.GeoIPDB.GeoIP.IPCountryRegion_GetByCountryCode);
                cmd.Parameters.AddWithValue("@CountryCode", countryCode);
                return SQLUtility.FillInDataRow(cmd); 
            }
        }
    }
}
