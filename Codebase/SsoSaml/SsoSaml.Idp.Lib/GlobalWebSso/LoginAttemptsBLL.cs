﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.SsoSaml.Idp.Lib.GlobalWebSso
{
    internal static class LoginAttemptsBLL
    {
        public static Boolean CheckLocked(String emailAddress)
        {
            DataRow r = Data.DAGW_SSO_LoginAttempts.CheckLocked(emailAddress);
            if (r == null) return true;
            return ConvertUtility.ConvertToBoolean(r["IsLocked"], true);
        }
    }
}
