﻿using System.Data;

namespace Anritsu.SsoSaml.Idp.Lib.GlobalWebSso
{
    public static class ResourceStringsBLL
    {
        public static DataTable ResourceStrings_SelectByClass(string className, string language)
        {
            return Data.DAGW_SSO_Resources_Utility.ResourceStrings_SelectByClass(className, language);
        }
    }
}
