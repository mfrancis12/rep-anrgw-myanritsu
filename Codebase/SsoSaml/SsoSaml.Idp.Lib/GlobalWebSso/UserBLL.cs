﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.SsoSaml.Idp.Lib.GlobalWebSso
{
    internal static class UserBLL
    {
        public static DataRow SelectByEmailOrUserName(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            return Data.DAGW_SSO_User.SelectByEmailOrUserName(emailAddressOrUserName);
        }

        public static DataRow SelectByGWUserId(Int32 gwUserId)
        {
            if (gwUserId < 1) return null;
            return Data.DAGW_SSO_User.SelectByGWUserId(gwUserId);
        }

        public static DataRow SelectBySecretKey(Guid secretKey)
        {
            if (secretKey.IsNullOrEmptyGuid()) return null;
            return Data.DAGW_SSO_User.SelectBySecretKey(secretKey);
        }

        internal static String EncryptGlobalWebUserPassword(Int32 gwUserId, String plainPassword)
        {
            if (gwUserId < 1 || plainPassword.IsNullOrEmptyString()) return String.Empty;
            var enc =
                new AnrCommon.CoreLib.CryptoUtilities.EncryptionSalt();
            enc.SetEncryptionKeyByIntId(gwUserId);
            return enc.EncryptData(plainPassword);
        }

        public static DataRow SelectUserERPPasswordByEmailId(string email)
        {
            return Data.DAGW_SSO_User.GetUserERPPasswordByEmailId(email);
        }


        public static DataRow CheckInActiveUser(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            return Data.DAGW_SSO_User.CheckInActiveUser(emailAddressOrUserName);
        }
        #region Verification Project MA-1447

        public static DataRow VerifyActiveStatus(String emailAddressOrUserName)
        {
            if (emailAddressOrUserName.IsNullOrEmptyString()) return null;
            return Data.DAGW_SSO_User.VerifyActiveStatus(emailAddressOrUserName);
        }
        #endregion
    }
}
