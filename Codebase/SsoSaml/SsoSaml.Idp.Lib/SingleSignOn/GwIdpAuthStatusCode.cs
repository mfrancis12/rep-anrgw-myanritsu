﻿namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public enum GwIdpAuthStatusCode
    {
        Authorized = 0,
        InvalidUserOrPassword = 1,
        MaxFailedLoginAttempts = 2,
        AccountLocked = 8888,
        SystemError = 9999,
        InActiveUser = 3
    }
}
