﻿using System.Data;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public class GwIdpSamlAttributeSetBLL
    {
        public static DataTable SelectAll()
        {
            return Data.DAGW_SSO_SamlAttributeSet.SelectAll();
        }
    }
}
