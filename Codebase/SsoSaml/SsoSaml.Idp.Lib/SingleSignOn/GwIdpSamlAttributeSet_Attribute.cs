﻿using System;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    [Serializable]
    public class GwIdpSamlAttributeSet_Attribute
    {
        public Int32 SamlAttributeSetRefID { get; set; }
        public Int32 SamlAttributeSetID { get; set; }
        public Int32 SamlAttributeID { get; set; }
        public String AttributeName { get; set; }
        public String AttributeNameUrn { get { return String.Format("urn:oid:anrsso:{0}", AttributeName.ToLowerInvariant()); } }
        public String AttributeNameFormat { get; set; }
        public String AttributeNameFormatUrn { get { return String.Format("urn:oasis:names:tc:SAML:2.0:attrname-format:{0}", AttributeNameFormat.ToLowerInvariant()); } }
        public String AttributeFriendlyName { get; set; }
        public String ColumnName { get; set; }
        public String SrcTable { get; set; }
        public Int32 AttributeOrder { get; set; }
        public DateTime CreatedOnUTC { get; set; }
    }
}

