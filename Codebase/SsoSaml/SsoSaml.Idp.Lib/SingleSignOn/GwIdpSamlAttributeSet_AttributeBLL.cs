﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    internal class GwIdpSamlAttributeSet_AttributeBLL
    {
        internal static GwIdpSamlAttributeSet_Attribute InitFromData(DataRow r)
        {
            if (r == null) return null;
            var obj = new GwIdpSamlAttributeSet_Attribute
            {
                SamlAttributeSetRefID = ConvertUtility.ConvertToInt32(r["SamlAttributeSetRefID"], 0),
                SamlAttributeSetID = ConvertUtility.ConvertToInt32(r["SamlAttributeSetID"], 0),
                SamlAttributeID = ConvertUtility.ConvertToInt32(r["SamlAttributeID"], 0),
                AttributeName = ConvertUtility.ConvertNullToEmptyString(r["AttributeName"]),
                AttributeNameFormat = ConvertUtility.ConvertNullToEmptyString(r["AttributeNameFormat"]),
                AttributeFriendlyName = ConvertUtility.ConvertNullToEmptyString(r["AttributeFriendlyName"]),
                ColumnName = ConvertUtility.ConvertNullToEmptyString(r["ColumnName"]),
                SrcTable = ConvertUtility.ConvertNullToEmptyString(r["SrcTable"]),
                AttributeOrder = ConvertUtility.ConvertToInt32(r["AttributeOrder"], 0),
                CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["CreatedOnUTC"], DateTime.MinValue)
            };

            return obj;
        }

        internal static List<GwIdpSamlAttributeSet_Attribute> SelectBySpTokenID(Guid spTokenId)
        {
            if (spTokenId.IsNullOrEmptyGuid()) return null;
            DataTable tb = Data.DAGW_SSO_SamlAttributeSet_Attribute.SelectBySpTokenID(spTokenId);
            if (tb == null) return null;
            return (from DataRow r in tb.Rows select InitFromData(r)).ToList();
        }
    }
}
