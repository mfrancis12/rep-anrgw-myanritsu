﻿using System;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    [Serializable]
    public class GwIdpSamlAttribute_SrcTable
    {
        public const String GW_SSO_USER = "gw_sso_user";
        public const String GW_SSO_ROLEONLY = "gw_sso_role";
        public const String GW_SSO_ROLE_AND_PERMISSIONS = "gw_sso_roleperm";
        public const String GW_SSO_LOGINHISTORY = "gw_sso_lgnhist";
        public const String IDP_QueryString = "QueryString";
    }
}
