﻿using System;
using System.Xml;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public class GwIdpSamlLogBLL
    {
        public static void Log_LoginSamlRequest(String spToken, String parameters, XmlElement samlXml, String error)
        {
            String xmlStr = String.Empty;
            if (samlXml != null && samlXml.OwnerDocument != null) xmlStr = samlXml.OwnerDocument.OuterXml;
            Data.DAGW_SSO_SamlLog.Insert(spToken, "SamlLoginRequest", parameters, xmlStr, error);
        }
        public static void Log_LoginSamlResponse(String spToken, String parameters, XmlElement samlXml, String error)
        {
            String xmlStr = String.Empty;
            if (samlXml != null && samlXml.OwnerDocument != null) xmlStr = samlXml.OwnerDocument.OuterXml;
            Data.DAGW_SSO_SamlLog.Insert(spToken, "SamlLoginResponse", parameters, xmlStr, error);
        }
        public static void Log_LogoutSamlRequest(String spToken, String parameters, XmlElement samlXml, String error)
        {
            String xmlStr = String.Empty;
            if (samlXml != null && samlXml.OwnerDocument != null) xmlStr = samlXml.OwnerDocument.OuterXml;
            Data.DAGW_SSO_SamlLog.Insert(spToken, "SamlLogoutRequest", parameters, xmlStr, error);
        }
        public static void Log_LogoutSamlResponse(String spToken, String parameters, XmlElement samlXml, String error)
        {
            String xmlStr = String.Empty;
            if (samlXml != null && samlXml.OwnerDocument != null) xmlStr = samlXml.OwnerDocument.OuterXml;
            Data.DAGW_SSO_SamlLog.Insert(spToken, "SamlLogoutResponse", parameters, xmlStr, error);
        }
    }
}
