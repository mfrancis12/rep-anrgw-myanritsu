﻿using System;
using System.Collections.Generic;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    [Serializable]
    public class GwIdpServiceProvider
    {
        public Int32 ServiceProviderID { get; set; }
        public Guid SpTokenID { get; set; }
        public String SpResKeyPrefix { get; set; }
        public String ServiceProviderName { get; set; }
        public String SpHostName { get; set; }
        public String SpArtifactUrl { get; set; }
        public String SpLogoutUrl { get; set; }
        public String SpAcsUrl { get; set; }
        public String SpDefaultUrl { get; set; }
        public Int32 CertID { get; set; }
        public Boolean SpNameIdPolicyAllowCreate { get; set; }
        public Int32 SamlAttributeSet { get; set; }
        public String SamlBindingSpToIdp { get; set; }
        public String SamlBindingIdpToSp { get; set; }
        public DateTime CreatedOnUTC { get; set; }
        public List<GwIdpSamlAttributeSet_Attribute> SamlAttributes { get; set; }
        public List<String> Audiences { get; set; }
        public Boolean ShowContactUs { get; set; }
        public Boolean ShowSignup { get; set; }
        public Boolean SPInitiatedLogin { get; set; }
        public Boolean RequiredSignedAssertions { get; set; }
        public Boolean RequiredQualifiedNameUrn { get; set; }
    }
}
