﻿using System;
using System.Collections.Generic;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public class GwIdpServiceProviderBLL
    {
        internal static GwIdpServiceProvider InitFromData(DataRow r, DataTable attributes, DataTable spAudiences)
        {
            if (r == null) return null;
            var obj = new GwIdpServiceProvider
            {
                ServiceProviderID = ConvertUtility.ConvertToInt32(r["ServiceProviderID"], 0),
                SpTokenID = ConvertUtility.ConvertToGuid(r["SpTokenID"], Guid.Empty),
                SpResKeyPrefix = ConvertUtility.ConvertNullToEmptyString(r["SpResKeyPrefix"]),
                ServiceProviderName = ConvertUtility.ConvertNullToEmptyString(r["ServiceProviderName"]),
                SpHostName = ConvertUtility.ConvertNullToEmptyString(r["SpHostName"]),
                SpArtifactUrl = ConvertUtility.ConvertNullToEmptyString(r["SpArtifactUrl"]),
                SpLogoutUrl = ConvertUtility.ConvertNullToEmptyString(r["SpLogoutUrl"]),
                SpAcsUrl = ConvertUtility.ConvertNullToEmptyString(r["SpAcsUrl"]),
                SpDefaultUrl = ConvertUtility.ConvertNullToEmptyString(r["SpDefaultUrl"]),
                CertID = ConvertUtility.ConvertToInt32(r["CertID"], 0),
                SpNameIdPolicyAllowCreate = ConvertUtility.ConvertToBoolean(r["SpNameIdPolicyAllowCreate"], true),
                SamlAttributeSet = ConvertUtility.ConvertToInt32(r["SamlAttributeSet"], 0),
                SamlBindingSpToIdp =
                    ConvertUtility.ConvertToString(r["SamlBindingSpToIdp"],
                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"),
                SamlBindingIdpToSp =
                    ConvertUtility.ConvertToString(r["SamlBindingIdpToSp"],
                        "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"),
                CreatedOnUTC = ConvertUtility.ConvertToDateTime(r["SpResKeyPrefix"], DateTime.MinValue),
                ShowContactUs = ConvertUtility.ConvertToBoolean(r["ShowContactUs"], true),
                ShowSignup = ConvertUtility.ConvertToBoolean(r["ShowSignup"], true),
                SPInitiatedLogin = ConvertUtility.ConvertToBoolean(r["SPInitiatedLogin"], true),
                RequiredSignedAssertions = ConvertUtility.ConvertToBoolean(r["RequiredSignedAssertions"], false),
                RequiredQualifiedNameUrn = ConvertUtility.ConvertToBoolean(r["RequiredQualifiedNameUrn"], true)
            };
            if (attributes != null)
            {
                obj.SamlAttributes = new List<GwIdpSamlAttributeSet_Attribute>();
                foreach (DataRow aRow in attributes.Rows)
                    obj.SamlAttributes.Add(GwIdpSamlAttributeSet_AttributeBLL.InitFromData(aRow));
            }
            if (spAudiences != null)
            {
                obj.Audiences = new List<String>();
                foreach (DataRow aRow in spAudiences.Rows)
                    obj.Audiences.Add(Convert.ToString(aRow["AudienceURI"]));
            }
            return obj;
        }

        public static GwIdpServiceProvider SelectBySpTokenID(Guid spTokenId)
        {
            if (spTokenId.IsNullOrEmptyGuid()) return null;
            DataTable spAttributes;
            DataTable spAudiences;
            DataRow spRow = Data.DAGW_SSO_ServiceProvider.SelectBySpTokenID(spTokenId, out spAttributes, out spAudiences);
            return InitFromData(spRow, spAttributes,spAudiences);
        }

        public static DataTable SelectAll()
        {
            return Data.DAGW_SSO_ServiceProvider.SelectAll();
        }

        public static DataTable SelectByCertID(Int32 certId)
        {
            return Data.DAGW_SSO_ServiceProvider.SelectByCertID(certId);
        }
    }
}
