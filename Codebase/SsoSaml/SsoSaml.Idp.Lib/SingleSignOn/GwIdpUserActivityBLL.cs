﻿using System;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public static class GwIdpUserActivityBLL
    {
        public static void UpdateOnline(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            Data.DAGW_SSO_UserActivity.UpdateOnline(gwUserId, serviceProviderId, userIp);
        }

        public static void UpdateLogin(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            Data.DAGW_SSO_UserActivity.UpdateLogin(gwUserId, serviceProviderId, userIp);
        }

        public static void UpdateLogout(Int32 gwUserId, Int32 serviceProviderId, String userIp)
        {
            Data.DAGW_SSO_UserActivity.UpdateLogout(gwUserId, serviceProviderId, userIp);
        }
        public static bool InsertContactUSLog(string FirstName, string LastName, string EmailAddress, string URL, string Comments, int CultureGroupID)
        {
            return Data.DAGW_SSO_UserActivity.InsertContactUSLog(FirstName, LastName, EmailAddress, URL, Comments, CultureGroupID);
        }
    }
}
