﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.AnrCommon.EmailSDK;
using EM = Anritsu.AnrCommon.EmailSDK;
using System.Collections.Generic;
using System.Text;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using System.Text.RegularExpressions;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public static class GwIdpUserBLL
    {
        public static GwIdpAuthStatusCode AuthenticateUser(GwIdpServiceProvider sp,
            String emailAddressOrUserName,
            String password,
            String clientIp,
            out DataRow userData,
            out String internalMessage,
            bool isFromJpDownloads,
            out bool changePassword)
        {
            userData = null;
            changePassword = false;
            internalMessage = String.Empty;
            try
            {

                #region " check arguments "
                if (emailAddressOrUserName.IsNullOrEmptyString() || password.IsNullOrEmptyString() || sp == null || sp.ServiceProviderID < 1)
                {
                    return GwIdpAuthStatusCode.InvalidUserOrPassword;
                }

                if (clientIp == null) clientIp = WebUtility.GetUserIP();
                emailAddressOrUserName = emailAddressOrUserName.Trim();
                password = password.Trim();
                #endregion

                userData = GlobalWebSso.UserBLL.SelectByEmailOrUserName(emailAddressOrUserName);

                #region " user not found "
                if (userData == null)
                {
                  
                  
                        return GwIdpAuthStatusCode.InvalidUserOrPassword;
                  
                }
                

                #endregion

                String emailAddress = ConvertUtility.ConvertNullToEmptyString(userData["EmailAddress"]);
                #region " check account locked "

                if (ConvertUtility.ConvertToInt32(userData["UserStatusId"], 0) != 1)
                {
                    Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                    return GwIdpAuthStatusCode.AccountLocked;
                }
                #endregion

                #region "Check if Max Number of Failed Login Attempts has been met within the Cut off window"

                if (GlobalWebSso.LoginAttemptsBLL.CheckLocked(emailAddress))
                {
                    // use email address only because Divya designed the table b4 UserName feature
                    Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                    return GwIdpAuthStatusCode.MaxFailedLoginAttempts;
                }
                #endregion

                Int32 gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserId"], 0);
                #region " check password "

                if (gwUserId < 1) return GwIdpAuthStatusCode.InvalidUserOrPassword;
                String encryptedPassword = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, password);
                bool isJpUserMigrated = ConvertUtility.ConvertToBoolean(userData["Password_Migrate"], false);
                String userErpPassword;
                if (encryptedPassword != userData["EncryptedPassword"].ToString())
                {
                    if (isFromJpDownloads && !isJpUserMigrated)
                    {
                        userErpPassword = ExtractUserErpPassword(emailAddress);
                        if (null != userErpPassword && ConvertUtility.ConvertNullToEmptyString(userErpPassword).Equals(password, StringComparison.Ordinal))
                        {
                            //user found in ERP (JP Download DB).
                            return ForceUserToChangePassword(out changePassword, gwUserId, emailAddress, clientIp, sp.ServiceProviderID);
                        }
                    }
                    Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                    return GwIdpAuthStatusCode.InvalidUserOrPassword;
                }
                //user entered correct password
                if (isFromJpDownloads && !isJpUserMigrated)
                {
                    userErpPassword = ExtractUserErpPassword(emailAddress);
                    if (null != userErpPassword)
                    {
                        //string erpEncryptedPassword = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserID, userErpPassword);
                        ////if user GW and ERP DB passwords are same
                        //if (ConvertUtility.ConvertNullToEmptyString(encryptedPassword).Equals(erpEncryptedPassword, StringComparison.Ordinal))
                        //return ForceUserToChangePassword(out changePassword, gwUserID, emailAddress, clientIP, sp.ServiceProviderID);

                        //Update the flag (Password_Migrate)
                        Data.DAGW_SSO_User.UpdatePasswordMigrateFlag(gwUserId, true);
                    }
                }

                #endregion

                #region " inactive user search "
                DataRow activestatusdr = GlobalWebSso.UserBLL.VerifyActiveStatus(emailAddressOrUserName);
                if (activestatusdr == null)
                {
                    return GwIdpAuthStatusCode.InActiveUser;
                }
                #endregion
                Data.DAGW_SSO_LoginAttempts.LogSuccessLogin(gwUserId, emailAddress, clientIp);

                   //good user
                    GwIdpUserActivityBLL.UpdateLogin(gwUserId, sp.ServiceProviderID, clientIp);
                    return GwIdpAuthStatusCode.Authorized;
                
            }
            catch (Exception ex)
            {
                //unexpected error.
                internalMessage = "ERROR: " + ex.Message;
                return GwIdpAuthStatusCode.SystemError;
            }
        }

        public static string ExtractUserErpPassword(string email)
        {
            DataRow dr = GlobalWebSso.UserBLL.SelectUserERPPasswordByEmailId(email);
            if (null != dr && dr["Password"] != null)
                return ConvertUtility.ConvertNullToEmptyString(dr["Password"]);
            return null;
        }

        private static bool IsJapanDownloadUser(String email)
        {
            DataRow dr = GlobalWebSso.UserBLL.SelectUserERPPasswordByEmailId(email);
            if (null != dr)
                return true;
            return false;
        }
        private static GwIdpAuthStatusCode ForceUserToChangePassword(out bool chnagePassword, int gwUserId, string emailAddress, string clientIp, int serviceProviderId)
        {
            Data.DAGW_SSO_LoginAttempts.LogSuccessLogin(gwUserId, emailAddress, clientIp);
            GwIdpUserActivityBLL.UpdateLogin(gwUserId, serviceProviderId, clientIp);
            chnagePassword = true;
            return GwIdpAuthStatusCode.Authorized;
        }
        public static DataRow SelectBySecretKeyForAuthenticatedUser(Guid secretKey, Int32 spId, String clientIp)
        {
            #region " check arguments "
            if (secretKey.IsNullOrEmptyGuid()) return null;
            #endregion

            return GlobalWebSso.UserBLL.SelectBySecretKey(secretKey);
        }

        public static DataRow SelectBySecretKey(Guid secretKey)
        {
            #region " check arguments "
            if (secretKey.IsNullOrEmptyGuid()) return null;
            #endregion
            return GlobalWebSso.UserBLL.SelectBySecretKey(secretKey);
        }

        public static Boolean UpdateTempPassword(Int32 gwUserId, String newPlainPwd, String clientIp)
        {
            if (gwUserId < 1 || newPlainPwd.IsNullOrEmptyString()) return false;
            if (clientIp.IsNullOrEmptyString()) clientIp = WebUtility.GetUserIP();
            String newEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, newPlainPwd);
            return Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, clientIp) > 0;
        }

        public static Boolean UpdateTempPassword(Int32 gwUserId, String newPlainPwd, bool passwordMigrate, String clientIp)
        {
            if (gwUserId < 1 || newPlainPwd.IsNullOrEmptyString()) return false;
            if (clientIp.IsNullOrEmptyString()) clientIp = WebUtility.GetUserIP();
            String newEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, newPlainPwd);
            return Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, passwordMigrate, clientIp) > 0;
        }

        public static bool CheckCurrentPassword(Int32 gwUserId,string currentPlainPwd)
        {
            bool result = false;
            try
            {
                if (gwUserId < 1 || currentPlainPwd.IsNullOrEmptyString())
                {
                    result = false;
                    return result;
                }
                string clientIp = WebUtility.GetUserIP();
                String currentEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, currentPlainPwd);
                DataRow userData = GlobalWebSso.UserBLL.SelectByGWUserId(gwUserId);
                if (userData == null) return false;
                String emailAddress = ConvertUtility.ConvertNullToEmptyString(userData["EmailAddress"]);
                #region " check current password "
                if (currentEncryptedPwd != userData["EncryptedPassword"].ToString())
                {
                    Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                    result = false;
                    return result;
                }
                result = true;
                return result;
            }
            catch
            {
                result = false;
            }
            return result;
            #endregion

        }
        public static GwIdpAuthStatusCode UpdatePassword(Int32 gwUserId, String currentPlainPwd, String newPlainPwd, String clientIp)
        {
            if (gwUserId < 1 || newPlainPwd.IsNullOrEmptyString() || currentPlainPwd.IsNullOrEmptyString())
            {
                return GwIdpAuthStatusCode.InvalidUserOrPassword;
            }

            if (clientIp.IsNullOrEmptyString()) clientIp = WebUtility.GetUserIP();
            String currentEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, currentPlainPwd);
            String newEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, newPlainPwd);

            DataRow userData = GlobalWebSso.UserBLL.SelectByGWUserId(gwUserId);
            if (userData == null) return GwIdpAuthStatusCode.InvalidUserOrPassword;

            String emailAddress = ConvertUtility.ConvertNullToEmptyString(userData["EmailAddress"]);

            #region " check account locked "
            if (ConvertUtility.ConvertToInt32(userData["UserStatusId"], 0) != 1)
            {
                Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                return GwIdpAuthStatusCode.AccountLocked;
            }
            #endregion

            #region "Check if Max Number of Failed Login Attempts has been met within the Cut off window"
            if (GlobalWebSso.LoginAttemptsBLL.CheckLocked(emailAddress))
            {
                // use email address only because Divya designed the table b4 UserName feature
                Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                return GwIdpAuthStatusCode.MaxFailedLoginAttempts;
            }
            #endregion

            #region " check current password "
            if (currentEncryptedPwd != userData["EncryptedPassword"].ToString())
            {
                Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                return GwIdpAuthStatusCode.InvalidUserOrPassword;
            }
            #endregion

            if (IsJapanDownloadUser(emailAddress))
                Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, true, clientIp);
            else
                Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, clientIp);

            return GwIdpAuthStatusCode.Authorized;
        }
        public static DataRow GetUserIdByEmailOrUserName(string emailAddressOrUserName)
        {
            return GlobalWebSso.UserBLL.SelectByEmailOrUserName(emailAddressOrUserName);
        }
        public static GwIdpAuthStatusCode UpdatePassword(Int32 gwUserId, String newPlainPwd, String currentEncryptedPwd, String clientIp, bool isUpdatePwd)
        {
            String newEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, newPlainPwd);

            #region " check current password "
            if (currentEncryptedPwd == newEncryptedPwd)
            {
                return GwIdpAuthStatusCode.InvalidUserOrPassword;
            }
            #endregion

            Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, clientIp, isUpdatePwd);
            return GwIdpAuthStatusCode.Authorized;
        }
        public static GwIdpAuthStatusCode UpdatePassword(Int32 gwUserId, String newPlainPwd, String currentEncryptedPwd, String clientIp, bool isUpdatePwd, string emailAddress)
        {
            String newEncryptedPwd = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(gwUserId, newPlainPwd);

            #region " check current password "
            if (currentEncryptedPwd == newEncryptedPwd)
            {
                return GwIdpAuthStatusCode.InvalidUserOrPassword;
            }
            #endregion

            #region " check max failed login attempts "
            if (GlobalWebSso.LoginAttemptsBLL.CheckLocked(emailAddress))
            {
                Data.DAGW_SSO_LoginAttempts.LogFailedLogin(emailAddress, clientIp);
                return GwIdpAuthStatusCode.MaxFailedLoginAttempts;
            }
            #endregion

            Data.DAGW_SSO_User.UpdatePassword(gwUserId, newEncryptedPwd, clientIp, isUpdatePwd);
            return GwIdpAuthStatusCode.Authorized;
        }
        public static void UpdateResetPasswordTokenIsUsed(Guid token)
        {
            Data.DAGW_SSO_User.UpdateResetPasswordTokenIsUsed(token);
        }

        public static DataRow GetUserByGWUserId(Int32 gwUserId)
        {
            return GlobalWebSso.UserBLL.SelectByGWUserId(gwUserId);
        }

        public static Guid CreateResetPasswordToken(int gwUserId)
        {
            DataRow r = Data.DAGW_SSO_User.InsertResetPasswordToken(gwUserId);
            if (r == null) return Guid.Empty;
            return new Guid(r["Token"].ToString().ToUpper());
        }

        public static DataRow GetResetPasswordTokenInfo(Guid forgotPasswordToken)
        {
            return Data.DAGW_SSO_User.GetResetPasswordTokenInfo(forgotPasswordToken);
        }


        // -----------------For New User Registration ---------------------------------

        public static bool ISLoginNameOrEmailExisting(string emailAddressOrUserName)
        {
            DataRow dr = GlobalWebSso.UserBLL.SelectByEmailOrUserName(emailAddressOrUserName);
            if (dr == null) { return false; }
            return true;
        }

        public static DataTable GetSsoUerType()
        {
            return Data.DAGW_SSO_User.GetSSOUserType();
        }

        public static DataSet InsertNotActivatedSSOUser(GWSsoUserType newUser)
        {
            return Data.DAGW_SSO_User.InsertNotActivatedSSOUser(newUser);
        }

        public static DataRow UpdateUserPassword(int userId, string password, string spToken, bool isTemp)
        {
            string encryptedpassword = GlobalWebSso.UserBLL.EncryptGlobalWebUserPassword(userId, password);
            return Data.DAGW_SSO_User.UpdateUserPassword(userId, encryptedpassword, spToken, isTemp);
        }

        public static DataRow GetActiveUserTokenInfo(Guid token)
        {
            return Data.DAGW_SSO_User.GetActiveUserTokenInfo(token);
        }
       
        public static DataRow GetUserStatus(int statusid)
        {
            return Data.DAGW_SSO_User.GetUserStatus(statusid);
        }

        public static void InsertUserInfo(int userId, bool sendNewsMail)
        {
            Data.DAGW_SSO_User.InsertUserInfo(userId, sendNewsMail);
        }

        public static DataRow GetUserRoleByRoleTitle(string title)
        {
            return Data.DAGW_SSO_User.GetUserRoleByRoleTitle(title);
        }

        public static void InsertUserUserRoleReference(int gwUserId, Guid roleId)
        {
            Data.DAGW_SSO_User.InsertUserUserRoleReference(gwUserId, roleId);
        }

        public static DataSet InsertActiveSSOUser(int gwTempUserId)
        {
            return Data.DAGW_SSO_User.InsertActiveSSOUser(gwTempUserId);
        }

        public static bool ISExistingLoginNameOrEmail(string emailAddressOrUserName)
        {
            DataSet ds = Data.DAGW_SSO_User.SelectExistingUserByEmailOrUserName(emailAddressOrUserName);
            if (ds == null || ds.Tables.Count == 0) { return false; }
            return true;
        }

        public static bool IsLoginNameExistingToEdit(string loginName, string originalLoginName)
        {
            bool isExistingUser = false;
            DataRow dr = GlobalWebSso.UserBLL.SelectByEmailOrUserName(loginName.Trim());
            if (dr != null && loginName.ToLower() != originalLoginName.ToLower())
            {
                isExistingUser = true;
            }
            return isExistingUser;
        }

        public static DataTable GetUserInfo(int userId)
        {
            return Data.DAGW_SSO_User.GetUserInfo(userId);
        }

        public static void UpdateUserDetails(UpdateUserCallRequestType updateUserRequest, UpdateUserCallRequestType oldUser)
        {

            var userToUpdate = new GWSsoUserType
            {
                UserId = updateUserRequest.GWSsoUser.UserId,
                EmailAddress = updateUserRequest.GWSsoUser.EmailAddress.ConvertNullToEmptyString(),
                LoginName = updateUserRequest.GWSsoUser.LoginName.ConvertNullToEmptyString(),
                FirstName = updateUserRequest.GWSsoUser.FirstName.ConvertNullToEmptyString(),
                LastName = updateUserRequest.GWSsoUser.LastName.ConvertNullToEmptyString()
            };

            userToUpdate.LoginName = updateUserRequest.GWSsoUser.LoginName.ConvertNullToEmptyString();

            userToUpdate.FirstNameInRoman = updateUserRequest.GWSsoUser.FirstNameInRoman.ConvertNullToEmptyString();
            userToUpdate.LastNameInRoman = updateUserRequest.GWSsoUser.LastNameInRoman.ConvertNullToEmptyString();
            userToUpdate.CompanyInRoman = updateUserRequest.GWSsoUser.CompanyInRoman.ConvertNullToEmptyString();


            userToUpdate.MiddleName = updateUserRequest.GWSsoUser.MiddleName.ConvertNullToEmptyString();
            userToUpdate.EncryptedTempPassword = updateUserRequest.GWSsoUser.EncryptedTempPassword.ConvertNullToEmptyString();
            userToUpdate.JobTitle = updateUserRequest.GWSsoUser.JobTitle.ConvertNullToEmptyString();
            userToUpdate.CompanyName = updateUserRequest.GWSsoUser.CompanyName.ConvertNullToEmptyString();
            userToUpdate.StreetAddress1 = updateUserRequest.GWSsoUser.StreetAddress1.ConvertNullToEmptyString();
            userToUpdate.StreetAddress2 = updateUserRequest.GWSsoUser.StreetAddress2.ConvertNullToEmptyString();
            userToUpdate.City = updateUserRequest.GWSsoUser.City.ConvertNullToEmptyString();
            userToUpdate.State = updateUserRequest.GWSsoUser.State.ConvertNullToEmptyString();
            userToUpdate.PostalCode = updateUserRequest.GWSsoUser.PostalCode.ConvertNullToEmptyString();
            userToUpdate.CountryCode = updateUserRequest.GWSsoUser.CountryCode.ConvertNullToEmptyString();
            userToUpdate.PhoneNumber = updateUserRequest.GWSsoUser.PhoneNumber.ConvertNullToEmptyString().Trim().FormatPhoneNumber();
            userToUpdate.FaxNumber = updateUserRequest.GWSsoUser.FaxNumber.ConvertNullToEmptyString().Trim();
            userToUpdate.RegisteredFromURL = updateUserRequest.GWSsoUser.RegisteredFromURL.ConvertNullToEmptyString().Trim();
            userToUpdate.PrimaryCultureCode = updateUserRequest.GWSsoUser.PrimaryCultureCode.ConvertNullToEmptyString().Trim();
            userToUpdate.IsIndividualEmailAddress = updateUserRequest.GWSsoUser.IsIndividualEmailAddress;
            var gwUserStatus = new GWSsoUserStatusType();
            DataRow drUserStatus = GetUserStatus(updateUserRequest.GWSsoUser.UserStatus.UserStatusId);
            gwUserStatus.UserStatusId = int.Parse(drUserStatus["UserStatusId"].ToString());
            gwUserStatus.UserStatusTitle = drUserStatus["UserStatusTitle"].ToString();
            userToUpdate.UserStatus = gwUserStatus;

            var gwUserType = new GWSsoUserTypeType();
            DataTable dtUsertype = Data.DAGW_SSO_User.GetSSOUserType();
            DataRow[] drusertype = dtUsertype.Select("UserTypeId=" + updateUserRequest.GWSsoUser.UserType.UserTypeId + "");
            gwUserType.UserTypeId = int.Parse(drusertype[0]["UserTypeId"].ToString());
            gwUserType.UserType = drusertype[0]["UserType"].ToString();
            gwUserType.Description = drusertype[0]["Description"].ToString();
            gwUserType.IsUserProfile = bool.Parse(drusertype[0]["IsUserProfile"].ToString());
            userToUpdate.UserType = gwUserType;

            
            Data.DAGW_SSO_User.UpdateGWUser(userToUpdate, updateUserRequest.GWSsoUser.IsTempPassword);
            Data.DAGW_SSO_User.UpdateConnectUserInfo(userToUpdate);
            var dt = Data.DAGW_SSO_User.ConnectUser_IsTeamOwner(userToUpdate);
            if (dt!=null&&dt.Rows.Count > 0)
            {
                UserProfileUpdateNotifyAdmin(ConvertUtility.ConvertToGuid(dt.Rows[0]["AccountID"].ToString(), Guid.Empty), ConvertUtility.ConvertToGuid(dt.Rows[0]["OwnerMembershipID"].ToString(), Guid.Empty), oldUser, updateUserRequest, dt.Rows[0]["AccountName"].ToString());                    
            }
        }

        public static void UpdateUserEmailAddress(String UserOldEmailAddress, String UserNewEmailAddress, int GwUserId)
        {

            Data.DAGW_SSO_User.UpdateConnectUserEmailAddress(GwUserId, UserOldEmailAddress, UserNewEmailAddress);
        } 

        public static void UserProfileUpdateNotifyAdmin(Guid Accountid, Guid membershipId, UpdateUserCallRequestType oldUser, UpdateUserCallRequestType newUser,string accountName)
        {

            #region " send email(s) to admin for notification"
            // Lib.Account.Acc_AccountMaster acc = Lib.BLL.BLLAcc_AccountMaster.SelectByAccountID(accountID);
            var isAccountCreateHistory = !oldUser.GWSsoUser.CompanyName.Equals(newUser.GWSsoUser.CompanyName);

            List<EM.EmailWebRef.ReplaceKeyValueType> kvListForAdmin = GetKeyValuesForAdmin(Accountid, isAccountCreateHistory, false, membershipId, oldUser, newUser, accountName);
            Anritsu.AnrCommon.EmailSDK.EmailWebRef.SendTemplatedDistributedEmailCallRequest email = new Anritsu.AnrCommon.EmailSDK.EmailWebRef.SendTemplatedDistributedEmailCallRequest();
            email.EmailTemplateKey = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdate.NotifyAdmin");
            email.DistributionListKey = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUpdate.DistributionKey");
            email.CultureGroupId = 1;
            email.SubjectReplacementKeyValues = kvListForAdmin.ToArray();
            email.BodyReplacementKeyValues = kvListForAdmin.ToArray();
            Anritsu.AnrCommon.EmailSDK.EmailWebRef.BaseResponseType resp = EmailServiceManager.SendDistributedTemplatedEmail(email);
            string errorMsg;
            if (resp != null) errorMsg = resp.ResponseMessage;

            #endregion
        }


        private static List<ReplaceKeyValueType> GetKeyValuesForAdmin(Guid accountID,
           Boolean IsAccountCreateHistory, Boolean IsProfileAddrCreateHistory, Guid membershipId, UpdateUserCallRequestType oldUser, UpdateUserCallRequestType newUser,String AccountName)
        {

          
            List<EM.EmailWebRef.ReplaceKeyValueType> kvList = new List<EM.EmailWebRef.ReplaceKeyValueType>();
            EM.EmailWebRef.ReplaceKeyValueType kv;

            #region " user and account information "
            if (oldUser != null)
            {
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[MembershipID]]";
                kv.ReplacementValue = Convert.ToString(membershipId);
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-Email]]";
                kv.ReplacementValue = oldUser.GWSsoUser.EmailAddress;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-FirstName]]";
                if (!String.IsNullOrEmpty(oldUser.GWSsoUser.FirstName))
                    kv.ReplacementValue = oldUser.GWSsoUser.FirstName;
                else
                    kv.ReplacementValue = oldUser.GWSsoUser.FirstName;
                kvList.Add(kv);

                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[SubmittedUser-LastName]]";
                if (!String.IsNullOrEmpty(oldUser.GWSsoUser.LastName))
                    kv.ReplacementValue = oldUser.GWSsoUser.LastName;
                else
                    kv.ReplacementValue = oldUser.GWSsoUser.LastName;
                kvList.Add(kv);
                if (!oldUser.GWSsoUser.CompanyName.Equals(newUser.GWSsoUser.CompanyName))
                {
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-CompanyName]]";
                    kv.ReplacementValue = (oldUser.GWSsoUser.CompanyName != newUser.GWSsoUser.CompanyName) ? string.Format("'{0}' changed to '{1}' ", oldUser.GWSsoUser.CompanyName, newUser.GWSsoUser.CompanyName) : newUser.GWSsoUser.CompanyName;
                }
                else
                {
                    kv = new EM.EmailWebRef.ReplaceKeyValueType();
                    kv.TemplateKey = "[[Account-CompanyName]]";
                    kv.ReplacementValue = newUser.GWSsoUser.CompanyName;
                    kvList.Add(kv);
                    
                }
                kv = new EM.EmailWebRef.ReplaceKeyValueType();
                kv.TemplateKey = "[[Account-AccountName]]";
                kv.ReplacementValue = AccountName;
                kvList.Add(kv);


            }
            #endregion

            #region "Url information"
            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[AccountID]]";
            kv.ReplacementValue = Convert.ToString(accountID);
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[IsAccChanged]]";
            kv.ReplacementValue = IsAccountCreateHistory.ToString();
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[IsAddChanged]]";
            kv.ReplacementValue = IsProfileAddrCreateHistory.ToString();
            kvList.Add(kv);

            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[BaseUrl]]";
            kv.ReplacementValue = ConfigUtility.AppSettingGetValue("Connect.Web.BaseUrl");
            kvList.Add(kv);

            String adminNotificationUrlFormat = ConfigUtility.AppSettingGetValue("Connect.Web.OrgUserUpdate.AdminGroupNotification") +
            "accid=" + "[[AccountID]]" + "&accchngstatus=" + "[[IsAccChanged]]" + "&addchngstatus=" + "[[IsAddChanged]]" + "&memid=" + "[[MembershipID]]";


            //StringBuilder notifyUrl = new StringBuilder();
            //notifyUrl.Append("<ul>");
            String notifyUrl = adminNotificationUrlFormat.Replace("[[AccountID]]", accountID.ToString()).
                 Replace("[[IsAccChanged]]", IsAccountCreateHistory.ToString()).Replace("[[IsAddChanged]]", IsProfileAddrCreateHistory.ToString()).Replace("[[MembershipID]]", membershipId.ToString());
            String notificationLnk = String.Empty;
            //notifyUrl.AppendFormat("<a href='{0}'>User Update History</a>",
            //     adminNotificationUrlFormat.Replace("[[AccountID]]", accountID.ToString()).
            //     Replace("[[IsAccChanged]]", IsAccountCreateHistory.ToString()).Replace("[[IsAddChanged]]", IsProfileAddrCreateHistory.ToString()));
            //notifyUrl.Append("</ul>");
            kv = new EM.EmailWebRef.ReplaceKeyValueType();
            kv.TemplateKey = "[[AdminGroupNotificationUrl]]";
            kv.ReplacementValue = notifyUrl;
            kvList.Add(kv);
            #endregion

            return kvList;
        }
        public static void UpdateUserInfo(int userId, bool sendNewsMail)
        {
            Data.DAGW_SSO_User.UpdateUserInfo(userId, sendNewsMail);
        }


        public static DataRow Get_SpLoginURL_By_SpToken(Guid tokenid)
        {
            return Data.DAGW_SSO_User.Get_SpLoginURL_By_SpTokenId(tokenid);
        }


        public static DataSet Get_InActive_UserToken(Guid tokenid)
        {
            return Data.DAGW_SSO_User.Get_InActive_UserToken(tokenid);
        }

        public static DataSet GetInActiveUserData(string emailAddressOrUserName)
        {
            return Data.DAGW_SSO_User.SelectExistingUserByEmailOrUserName(emailAddressOrUserName);
        }

        public static bool InActiveUserSelectByEmailAndUserName(String emailAddress, String userName)
        {
            return Data.DAGW_SSO_User.InActiveUserSelectByEmailAndUserName(emailAddress, userName);
        }
        public static bool ISExistingActiveInActiveLoginNameOrEmail(string emailAddressOrUserName)
        {
            DataSet ds = Data.DAGW_SSO_User.SelectExistingActiveInActiveUserByEmailOrUserName(emailAddressOrUserName);
            if (ds == null || ds.Tables.Count == 0) { return false; }
            return true;
        }
        public static DataRow InsertUserEmailChangeRequest(int gwUserId, String userOldEmailAddress, String userNewEmailAddress, int cultureGroupId)
        {
            return Data.DAGW_SSO_User.InsertUserEmailChangeRequest(gwUserId, userOldEmailAddress, userNewEmailAddress, cultureGroupId);
        }

        public static DataRow GetUserEmailChangeRequest(Guid token)
        {
            return Data.DAGW_SSO_User.GetUserEmailChangeRequest(token);
        }

        public static void InsertUserEmailChangeLog(int gwUserId, String userOldEmailAddress, String userNewEmailAddress, Guid token, int cultureGroupId)
        {
            Data.DAGW_SSO_User.InsertUserEmailChangeLog(gwUserId, userOldEmailAddress, userNewEmailAddress, token, cultureGroupId);
        }

        public static bool CheckEmailAddressExist(string emailAddress)
        {
            return Data.DAGW_SSO_User.CheckEmailAddressExist(emailAddress);
        }
        #region Verification Project MA-1447
        public static bool VerifyValidationCode(string email, string validationcode)
        {
            return Data.DAGW_SSO_User.VerifyValidationCode(email, validationcode);
        }

        public static bool VerifyIsNewUser(String emailAddress)
        {
            return Data.DAGW_SSO_User.VerifyIsNewUser(emailAddress);
        }
               
        public static DataSet CheckUserRegistrationStatus(Guid tokenid)
        {
            return Data.DAGW_SSO_User.CheckUserRegistrationStatus(tokenid);
        }
        public static DataSet GetNewUserToken(String emailAddress)
        {
            return Data.DAGW_SSO_User.GetNewUserToken(emailAddress);
        }
        public static bool VerifyLoginNameExists(String emailAddress, String userName)
        {
            return Data.DAGW_SSO_User.VerifyLoginNameExists(emailAddress, userName);
        }
        public static DataSet GetValidationCode(string emailAddressOrUserName, int randomnumber, string requestedBy,string wereplyurl)
        {
            return Data.DAGW_SSO_User.GetValidationCode(emailAddressOrUserName, randomnumber, requestedBy, wereplyurl);

        }
        public static string MaskEmail(string s)
        {
            string _PATTERN = @"(?<=[\w]{1})[\w-\._\+%\\]*(?=[\w]{1}@)|(?<=@[\w]{1})[\w-_\+%]*(?=\.)";
            if (!s.Contains("@"))
                return new String('*', s.Length);
            if (s.Split('@')[0].Length < 4)
                return @"*@*.*";
            return Regex.Replace(s, _PATTERN, m => new string('*', m.Length));

        }
        #endregion
    }
}
