﻿using System;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public static class GwIdpUserLoginHistoryBLL
    {
        public static DataTable Select(Int32 gwUserId, Int32 count)
        {
            return Data.DAGW_SSO_LoginHistory.Select(gwUserId, count);
        }
    }
}
