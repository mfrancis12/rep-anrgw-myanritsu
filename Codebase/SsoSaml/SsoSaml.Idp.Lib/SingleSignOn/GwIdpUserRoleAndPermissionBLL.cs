﻿using System;
using System.Data;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public static class GwIdpUserRoleAndPermissionBLL
    {
        public static DataTable UserRole_Select(Int32 gwUserId)
        {
            if (gwUserId < 1) return null;
            return Data.DAGW_SSO_User.UserRole_SelectByGWUserId(gwUserId);
        }

        public static DataTable UserPermissionToken_Select(Guid roleId)
        {
            if (roleId.IsNullOrEmptyGuid()) return null;
            return Data.DAGW_SSO_User.UserPermissionTokens_SelectByRoleId(roleId);
        }
    }
}
