﻿using System;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Lib.SingleSignOn
{
    public class SamlCertStoreBLL
    {
        public static DataTable SelectAll()
        {
            return Data.DAGW_SSO_SamlCertStore.SelectAll();

        }
        public static DataRow Select(Int32 certId)
        {
            if (certId < 1) return null;
            return Data.DAGW_SSO_SamlCertStore.Select(certId);
        }

        public static DataRow Insert(String internalName, Byte[] x509Cer, Byte[] x509Pfx, String x509PfxPwd, DateTime x509PfxExpOnUtc, String internalNote)
        {
            return Data.DAGW_SSO_SamlCertStore.Insert(internalName, x509Cer, x509Pfx, x509PfxPwd, x509PfxExpOnUtc, internalNote);
        }

        public static void Update(Int32 certId, String internalName, Byte[] x509Cer, Byte[] x509Pfx, String x509PfxPwd, DateTime x509PfxExpOnUtc, String internalNote)
        {
            if (certId < 1) return;
            Data.DAGW_SSO_SamlCertStore.Update(certId, internalName, x509Cer, x509Pfx, x509PfxPwd, x509PfxExpOnUtc, internalNote);
        }
    }
}
