﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Lib.UrlRedirects
{
    public class UrlRedirect
    {
        public int UrlRedirectId { get; set; }
        public string OriginalUrl { get; set; }
        public string TargetUrl { get; set; }
        public bool AppendQueryStrings { get; set; }
        public RedirectType RedirectType { get; set; }
        public bool IsActive { get; set; }

        public UrlRedirect(DataRow rowUrlRedirect)
        {

            UrlRedirectId = Convert.ToInt32(rowUrlRedirect["UrlRedirectId"]);
            AppendQueryStrings = ConvertUtility.ConvertToBoolean(rowUrlRedirect["AppendQueryStrings"], true);
            TargetUrl = Convert.ToString(rowUrlRedirect["TargetUrl"]);
            TargetUrl = AppendQueryString(TargetUrl, AppendQueryStrings);
            OriginalUrl = Convert.ToString(rowUrlRedirect["OriginalUrl"]);
            RedirectType = (RedirectType)Enum.Parse(typeof(RedirectType), Convert.ToString(rowUrlRedirect["RedirectType"]));
            IsActive = Convert.ToBoolean(rowUrlRedirect["IsActive"]);
        }
        public static void RedirectUrl(HttpContext context, DataTable dtRedirecUrls)
        {
            string relativeUrl = context.Request.RawUrl;
            //get the Redirect Url
            UrlRedirect redirectUrl = UrlRedirectBLL.GetReidrectUrl(relativeUrl, dtRedirecUrls);
            if (null != redirectUrl)
            {
                switch (redirectUrl.RedirectType)
                {
                    case RedirectType.PERMANENT_REDIRECT:
                        context.Response.RedirectPermanent(redirectUrl.TargetUrl, true);
                        break;
                    case RedirectType.TMPORARY_REDIRECT:
                        context.Response.Redirect(redirectUrl.TargetUrl, true);
                        break;
                    default:
                        context.Response.Redirect(redirectUrl.TargetUrl, true);
                        break;
                }
            }
        }
        private string AppendQueryString(string targetUrl, bool appendQueryStrings)
        {
            HttpContext context = HttpContext.Current;
            if (appendQueryStrings
                             && context.Request.QueryString.Count > 0)
            {
                if (targetUrl.Contains("?"))
                    targetUrl += "&";
                else
                    targetUrl += "?";
                #region Regular query strings

                targetUrl = context.Request.QueryString.AllKeys.Where(key => !String.IsNullOrEmpty(key)).Aggregate(targetUrl, (current, key) => current + string.Format("{0}={1}&", key, context.Request.QueryString[key]));

                #endregion

                if (targetUrl.EndsWith("&"))
                    targetUrl = targetUrl.Remove(targetUrl.Length - 1);
            }
            return targetUrl;
        }
    }
    public enum RedirectType
    {
        PERMANENT_REDIRECT = 301,
        TMPORARY_REDIRECT = 302
    }
}