﻿using System.Data;

namespace Anritsu.SsoSaml.Idp.Lib.UrlRedirects
{
   public class UrlRedirectBLL
    {
       public static DataTable GetAllRedirectUrls()
       {
           return Data.DAGW_SSO_UrlRedirect.Select_RedirectUrls();
       }
       public static UrlRedirect GetReidrectUrl(string originalurl, DataTable dtRedirectUrls)
       {
           if (dtRedirectUrls == null || dtRedirectUrls.Rows.Count == 0)
               return null;
           DataRow[] drRedirectUrls=dtRedirectUrls.Select("OriginalUrl='"+originalurl+"'");
           if(drRedirectUrls.Length>0)
               return new UrlRedirect(drRedirectUrls[0]);
           return null;
       }
    }
}
