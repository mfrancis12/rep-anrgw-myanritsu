﻿using System.Data;
using DA = Anritsu.SsoSaml.Idp.Data;

namespace Anritsu.SsoSaml.Idp.Lib.Utility
{
    public static class CultureUtilityBLL
    {
        public static int GetCultureGroupIdForCultureCode(string cultureCode)
        {
            return DA.DAGW_SSO_Utility.GetCultureGroupIdForCultureCode(cultureCode);
        }
        public static DataRow GetByCultureGroupId(int cultureGroupId)
        {
            return DA.DAGW_SSO_Utility.GetByCultureGroupId(cultureGroupId);
        }
    }
}
