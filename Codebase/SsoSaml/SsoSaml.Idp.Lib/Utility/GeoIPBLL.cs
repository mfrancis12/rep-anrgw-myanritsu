﻿using System.Data;
using DA = Anritsu.SsoSaml.Idp.Data;

namespace Anritsu.SsoSaml.Idp.Lib.Utility
{
    public static class GeoIPBLL
    {
        public static DataRow GetCountryByIP(string ipAddress)
        {
            return DA.DAGW_SSO_Utility.GetCountryByIP(ipAddress);
        }
        public static string GetRegionByCountryCode(string countryCode)
        {
            DataRow dr = DA.DAGW_SSO_Utility.GetRegionByCountryCode(countryCode);
            return (null != dr) ? dr["Region"].ToString() : string.Empty;
           
        }
    }
}
