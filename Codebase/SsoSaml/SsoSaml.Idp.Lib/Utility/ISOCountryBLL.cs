﻿using System;
using System.Data;
using Anritsu.SsoSaml.Idp.Data;

namespace Anritsu.SsoSaml.Idp.Lib.GlobalWebSso
{
    public static class ISOCountryBLL
    {
        public static DataTable Country_SelectByCultureGroupId(Int32 cultureGroupId)
        {
            if (cultureGroupId <= 0) return null;
            return DAGW_ISOCountry.Country_SelectByCultureGroupId(cultureGroupId);
        }
        public static DataTable State_SelectByCountry(string countryValue)
        {
            if (string.IsNullOrEmpty(countryValue)) return null;
            return DAGW_ISOCountry.State_SelectByCountry(countryValue);
        }
        public static string GetCountryNameForCultureGroupId(int cultureGroupId)
        {
            return DAGW_ISOCountry.GetCountryNameForCultureGroupId(cultureGroupId);
        }
    }
}
