﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="UpdatePwd.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.UpdatePwd" %>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    <%--<script src='//edge1.anritsu.com/jsutils/custom-tooltip-v1.js' type='text/javascript'></script>
    <script src='//edge1.anritsu.com/jsutils/util-functions-v1.js' type='text/javascript'></script>
    <script type="text/javascript">
        var pwdRules = <%=pwdRules%>

            function ClearDisplayMsg() {
            return validateOnSubmit();
        }
		var pwdId = "#<%= txtNewPwd.ClientID %>";
        $(document).ready(function () {
            var pwdId = "#<%= txtNewPwd.ClientID %>";
            $("#<%= txtNewPwd.ClientID %>").customToolTip({
                selector: '#<%= txtNewPwd.ClientID %>',
                htmlContent: '<b>' + pwdRules["PasswordStrength.Text"] + ' : </b><span class="strength">'+ pwdRules["PasswordWeak.Text"] + '</span>'
						+ '<div class="strength-scale"><span class="weak"></span></div>'
						+ '<div class="pwd-guidelines">'
						+ '<ul>'
						+ '<li class="fail">' + pwdRules["PasswordLength.ErrorMessage"] + '</li>'
						+ '<li class="fail">' + pwdRules["PasswordMustContainOneAlphabet.ErrorMessage"] + '</li>'
						+ '<li class="fail">' + pwdRules["PasswordMustContainOneNumeric.ErrorMessage"] + '</li>'
						+ '<li class="fail">' + pwdRules["PasswordInvalidCharacters.ErrorMessage"] + '</li>'
						+ '</ul></div>'
						
            });
            $('.what').click(function () {
                $(".guidelines").css("width", "450");
                $(".guidelines").animate({ height: "toggle" }, 800, function () {
                });
            });
        });
     </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class='changePwd'>
    <div class='updatePwd width-60 inline-block'>
        <div class="dialogue-body">
                 <div class="content-detail">
		            <div class="form-container">
			            <div class="bd">
                            <div class="group input-text required form-element field">
                                 <label for="txtUserEmail"><asp:Localize ID="lcalUserEmail" runat="server" Text='<%$Resources:~/account/updatepwd,lcalUserEmail.Text %>'></asp:Localize>:</label>
                                 <asp:TextBox ID="txtUserEmail" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                            </div>

                            <div id="divOldPwd" runat="server" class="group input-password required  form-element field">
                                <label for="txtOldPwd">
                                    <asp:Localize ID="lcalOldPwd" runat="server" Text='<%$Resources:~/account/updatepwd,lcalOldPwd.Text %>'></asp:Localize>:</label>
                                <p class="error-message">
                                    <asp:Localize ID="Localize1" runat="server" Text='<%$Resources:~/account/updatepwd,rfvOldPwd.ErrorMessage%>'></asp:Localize></p>
                                <asp:TextBox ID="txtOldPwd" onBlur="validate('old-password',this.value)" runat="server" TextMode="Password"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="rfvOldPwd" runat="server" ControlToValidate="txtOldPwd"
                                    ValidationGroup="vgChangePwd" Visible="false" ErrorMessage="<%$Resources:~/account/updatepwd,rfvOldPwd.ErrorMessage%>"
                                    SetFocusOnError="true">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <%--<span class="errormsg" id="old-password"></span>--%>
                            <div class="group input-password required form-element field">
                                    <label for="txtNewPwd"><asp:Localize ID="lcalNewPwd" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwd.Text %>'></asp:Localize>:</label>
                                    <p class="error-message"><asp:Localize ID="Localize4" runat="server" Text='<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage %>'></asp:Localize></p>
									<p class="custom-error-message"><asp:Localize ID="Localize5" runat="server" Text='<%$Resources:~/account/updatepwd,revNewPwd.ErrorMessage %>'></asp:Localize></p>
                                    <asp:TextBox ID="txtNewPwd" onBlur="validate('new-password',this.value)" runat="server" TextMode="Password" CssClass="newPassword"></asp:TextBox>
		                     </div>
                           <%-- <span class="errormsg" id="new-password"></span>--%>
                            <div class="group input-password required form-element field">
                                <label for="txtNewPwdConfirm"><asp:Localize ID="lcalNewPwdConfirm" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwdConfirm.Text %>'></asp:Localize>:</label>
                                <p class="error-message"><asp:Localize ID="Localize2" runat="server" Text='<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>'></asp:Localize></p>
								<p class="custom-error-message"><asp:Localize ID="Localize3" runat="server" Text='<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>'></asp:Localize> </p>
                                <asp:TextBox ID="txtNewPwdConfirm" onBlur="validate('repeat-password',this.value)" runat="server" TextMode="Password" CssClass="confirmPassword"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Visible="false" ID="rfvNewPwdConfirm" runat="server" ControlToValidate="txtNewPwdConfirm"
                                    ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>"
                                    SetFocusOnError="true">*</asp:RequiredFieldValidator><asp:CompareValidator Visible="false" ID="cvPwdMatch"
                                        ControlToValidate="txtNewPwdConfirm" ControlToCompare="txtNewPwd" ErrorMessage="<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>"
                                        runat="server" ValidationGroup="vgChangePwd" SetFocusOnError="true">*</asp:CompareValidator>--%>
                            </div>
                           <%-- <span class="errormsg" id="repeat-password"></span>--%>
                            <div class="settingrow">
                                <asp:Label ID="lblMsg" Visible="false" runat="server" CssClass="msg server-error-message"></asp:Label>
                            </div>
                            <div class="form-element group overflow margin-top-15">
                                <asp:Button ID="bttUpdatePwd" runat="server" Text='<%$Resources:~/account/updatepwd,bttUpdatePwd.Text %>'
                                    ValidationGroup="vgChangePwd" OnClick="bttUpdatePwd_Click"  CssClass="button form-submit" />
                                <asp:Button ID="bttUpdatePwdCancel" runat="server" Text='<%$Resources:~/account/updatepwd,bttUpdatePwdCancel.Text %>'
                                    CausesValidation="false" OnClick="bttUpdatePwdCancel_Click" CssClass="button" />
                            </div>
                            
                        </div>
                    </div>
                </div>
        </div>
        <%--<div class="form-element"><span class="left" id="pGuidelinesText">Password Guidelines</span><span class="what"></span></div>--%>    
    </div>
    <div class="right-links" style="margin-top:20px">
        <div class="form-element overflow guidelines-wrapper"><span class="left" id="pGuidelinesText">Password Guidelines</span><span class='what'></span></div>
                    <div class="guidelines">
                        <asp:Localize ID="lcalPwdGuide" runat="server" Text='<%$Resources:~/account/updatepwd,lcalPwdGuide.Text %>'></asp:Localize>
                    </div>
    </div>
     <script type="text/javascript">
         pwdRules = <%=pwdRules%>
             
         $(window).load(function() {
             pwdId = <%=string.Format("\"#{0}\"", txtNewPwd.ClientID.ToString())%> 
             });
         //alert(pwdId);
         //console.log(pwdId);
         function ClearDisplayMsg() {
             return validateOnSubmit();
         }
        </script>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
