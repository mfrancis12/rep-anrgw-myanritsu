﻿using System;
using System.Web;
using System.Data;
using System.Text;
using System.Web.Security;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.AnrCommon.CoreLib;
using System.Collections;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class UpdatePwd : IdpBasePage
    {
        public String pwdRules;

        private String ReturnUrl
        {
            get
            {
                var returnUrl = Request[KeyDef.ParmKeys.ReturnURL].IsNullOrEmptyString()
                    ? Request[KeyDef.ParmKeys.ReplyUrl]
                    : Request[KeyDef.ParmKeys.ReturnURL];

                return GetRedirectUrl(returnUrl, ServiceProviderInfo.SpDefaultUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pwdRules = GetPasswordGuideLines();
            if (IsPostBack) return;
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null)
            {
                RdirectToLoginPage();
                return;
            }
            InitControlValues(userData);
        }

        private void InitControlValues(DataRow userData)
        {
            if (userData == null) return;
            txtUserEmail.Text = userData["EmailAddress"].ToString();
            var isTempPwd = ConvertUtility.ConvertToBoolean(userData["IsTempPassword"], false);
            //rfvOldPwd.Enabled = !isTempPwd;
            divOldPwd.Visible = !isTempPwd;
            bttUpdatePwdCancel.Visible = !isTempPwd;
        }

        private void RdirectToLoginPage()
        {
            FormsAuthentication.SignOut();
            //String addOnQueryStrings = String.Format("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            var addOnQs = new StringBuilder();
            //addOnQS.AppendFormat("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            addOnQs.AppendFormat("{0}={1}", KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(ReturnUrl));
            FormsAuthentication.RedirectToLoginPage(addOnQs.ToString());
        }

        protected void bttUpdatePwd_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            Page.Validate("vgChangePwd");
            if (!Page.IsValid) return;

            if (txtOldPwd.Text == txtNewPwd.Text)
            {

                lblMsg.Text = GetResourceString("PasswordGuideLines", "SameNewOldPassword.ErrorMessage");
                lblMsg.Visible = true;
                return;
            }

            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null) RdirectToLoginPage();

            var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
            var isTempPwd = ConvertUtility.ConvertToBoolean(userData["IsTempPassword"], false);
            var newPwd = txtNewPwd.Text.Trim();
            var userIp = WebUtility.GetUserIP();
            var returnUrl = ConfigUtility.AppSettingGetValue("Idp.Web.MyAccountUrl").Replace("[[LL-CC]]", GetCurrentCulture());// ReturnUrl;
            if (isTempPwd)
            {
                Lib.SingleSignOn.GwIdpUserBLL.UpdateTempPassword(gwUserId, newPwd, userIp);
                userData["IsTempPassword"] = 0;
                UIHelper.LoggedInUser_Set(userData);
                Response.Redirect(returnUrl, true);
            }
            else
            {
                var oldPwd = txtOldPwd.Text.Trim();
                lblMsg.Visible = true;
                var pwdChangeStatus =
                    Lib.SingleSignOn.GwIdpUserBLL.UpdatePassword(gwUserId, oldPwd, newPwd, userIp);

                #region " process status "

                switch (pwdChangeStatus)
                {
                    case Lib.SingleSignOn.GwIdpAuthStatusCode.Authorized:
                        Response.Redirect(returnUrl, true);
                        break;
                    case Lib.SingleSignOn.GwIdpAuthStatusCode.AccountLocked:
                        lblMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_AccountLocked");
                        break;
                    case Lib.SingleSignOn.GwIdpAuthStatusCode.InvalidUserOrPassword:
                        lblMsg.Text = GetResourceString("PasswordGuideLines", "InvalidOldPassword.ErrorMessage");
                        break;
                    case Lib.SingleSignOn.GwIdpAuthStatusCode.MaxFailedLoginAttempts:
                        lblMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_MaxFailedLoginAttempts");
                        break;
                    case Lib.SingleSignOn.GwIdpAuthStatusCode.SystemError:
                        lblMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_SystemError");
                        break;
                    default:
                        lblMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_SystemError");
                        break;
                }

                #endregion
            }
        }

        protected void bttUpdatePwdCancel_Click(object sender, EventArgs e)
        {
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null) RdirectToLoginPage();
            Response.Redirect(ConfigUtility.AppSettingGetValue("Idp.Web.MyAccountUrl").Replace("[[LL-CC]]", GetCurrentCulture()));
        }

        /*
                private bool IsValidAnritsuPassword(string password, out string messages)
                {
                    messages = "";
                    ArrayList al = new ArrayList();

                    if (password.IndexOf("'") >= 0)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordApostrophe").ToString());
                    }

                    if (password.IndexOf(" ") >= 0)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordBlankCharacters").ToString());
                    }

                    if (password.Trim().Length < 8)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordLength").ToString());
                    }

                    string specialChars = "!@#$%^*-";
                    bool hasDigit = false;
                    bool hasLetter = false;
                    bool hasBadSpecialChar = false;
                    for (int i = 0; i <= password.Length - 1; i++)
                    {
                        Char c = (Char)password[i];

                        if (Char.IsLetterOrDigit(c))
                        {
                            if (!hasDigit)
                                hasDigit = Char.IsDigit(c);

                            if (!hasLetter)
                                hasLetter = Char.IsLetter(c);
                        }
                        else
                        {
                            if (!hasBadSpecialChar)
                                hasBadSpecialChar = (specialChars.IndexOf(c) == -1);
                        }

                    }

                    if (!hasDigit)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordDigit").ToString());
                    }
                    if (!hasLetter)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordAlpha").ToString());
                    }

                    if (hasBadSpecialChar)
                    {
                        al.Add(this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordSpecialCharacters").ToString());
                    }

                    if (al.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder(100);

                        string errText = this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationError").ToString();
                        if (al.Count > 1)
                            errText = this.GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationErrors").ToString();

                        sb.Append("<table>");
                        sb.Append(@"<tr><td colspan=""2""><font color=""red"" face=""verdana"" size=""1.8""><b>" + errText + ":</b></font></td><tr>");

                        foreach (string str in al)
                        {
                            sb.Append(String.Format(@"<tr><td valign=""top""><li style=""COLOR: red""></li></li></td><td><font color=""red"" face=""verdana"" size=""1.8"">{0}</font></td><tr>", str));
                        }
                        sb.Append("</table>");

                        messages = sb.ToString();
                        return false;
                    }
                    return true;
                }
        */
        public string GetCurrentCulture()
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
        }
        private string GetPasswordGuideLines()
        {
            var browserLang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            var lang = (browserLang == null ? "en-US" : browserLang);
            var dt = ResourceStringsBLL.ResourceStrings_SelectByClass("PasswordGuideLines", lang);
            var sb = new StringBuilder("{");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append(String.Format(@"""{0}"":""{1}""",
                    ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ResourceKey"])
                    , ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ContentStr"])));
            }
            return sb.Append("}").ToString();
        }
    }
}