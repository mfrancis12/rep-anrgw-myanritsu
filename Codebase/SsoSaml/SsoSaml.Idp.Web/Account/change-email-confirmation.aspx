﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="change-email-confirmation.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.ChangeEmailConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
     
    input.ui-button {
        background: url("//images-dev.cdn-anritsu.com/apps/connect/img/ui-bg_highlight-soft_35_0faf46_1x100.png") repeat-x scroll 50% 50% #0faf46;
        border: 1px solid #456d8b;
        border-radius: 7px;
        color: #fff;
        font-weight: 700;
        padding: 0.4em 1em;
    }
         
         </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div>
        <anrui:GlobalWebBoxedPanel ID="gwpnlChangeEmailConfirm" runat="server" ShowHeader="true" HeaderText="<% $Resources: ~/account/change-email-confirmation, gwpnlChangeEmailConfirm.HeaderText %>"
            Width="700px">
           <div >
                <asp:Localize ID="lcalConfrimMsg" runat="server" Text='<% $Resources: ~/account/change-email-confirmation, lcalConfrimMsg.Text %>' ></asp:Localize><br />
                <br /><br />
                <asp:Button ID="btnLinkToEditAccountConfirm" runat="server" Text='<%$Resources:~/account/change-email-confirmation,btnLinkToEditAccount.Text %>'  OnClick="btnLinkToEditAccount_Click"/>
                <br />  
            </div>
        </anrui:GlobalWebBoxedPanel>

        <anrui:GlobalWebBoxedPanel ID="gwpnlLnkExpiredMsg" runat="server" ShowHeader="true" Visible="False" HeaderText="<% $Resources: ~/account/change-email-confirmation, gwpnlLnkExpiredMsg.HeaderText %>"
            Width="700px">
             <div style="text-align:center;color:red;">
                <asp:Localize ID="lcalLnkExpMsg" runat="server"  Text='<% $Resources: ~/account/change-email-confirmation, lcalLnkExpMsg.Text %>'></asp:Localize>
                <br />  <br />
                 <asp:Button ID="btnLinkToEditAccount" runat="server" Text='<%$Resources:~/account/change-email-confirmation,btnLinkToEditAccount.Text %>'  OnClick="btnLinkToEditAccount_Click"/>
            </div>
            </anrui:GlobalWebBoxedPanel>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
