﻿using System.Threading;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System;
using System.Collections.Generic;
using System.Data;
using SDK = Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.CoreLib;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using System.Web;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class ChangeEmailConfirm : IdpBasePage
    {
        private Guid EmailValidationToken
        {
            get
            {
                if (Session["EmailToken"] == null)
                {
                    throw new HttpException(404, "Page not found.");
                }

                var tkn = Session["EmailToken"].ToString(); //WebUtility.ReadRequest(KeyDef.ParmKeys.CommonParams.RTK, true);
                if (string.IsNullOrEmpty(tkn) || !tkn.IsGuid() || Request.Browser.Crawler)
                    throw new HttpException(404, "Page not found.");

                var tknGuid = new Guid(tkn);
                if (tknGuid == Guid.Empty)
                    throw new HttpException(404, "Page not found.");
              
                return tknGuid;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //get the email request using token information
                var drRequest = BLL.SingleSignOn.GwIdpUserBLL.GetUserEmailChangeRequest(EmailValidationToken);
                if (!ValidateEmailValidationToken(drRequest))
                {
                    gwpnlLnkExpiredMsg.Visible = true;
                    gwpnlChangeEmailConfirm.Visible = false;
                    return;
                }
                //insert record into Change email log table and invalidate the token
                BLL.SingleSignOn.GwIdpUserBLL.InsertUserEmailChangeLog(Convert.ToInt32(drRequest["GWUserId"]), Convert.ToString(drRequest["UserOldEmailAddress"]), Convert.ToString(drRequest["UserNewEmailAddress"]), Guid.Parse(Convert.ToString(drRequest["EmailToken"])), Convert.ToInt32(drRequest["CultureGroupID"]));
                BLL.SingleSignOn.GwIdpUserBLL.UpdateUserEmailAddress(Convert.ToString(drRequest["UserOldEmailAddress"]),
                    Convert.ToString(drRequest["UserNewEmailAddress"]), Convert.ToInt32(drRequest["GWUserId"]));
                //send email to admin
                // SendAdminEmail(Convert.ToString(drRequest["UserOldEmailAddress"]),Convert.ToString(drRequest["UserNewEmailAddress"]), Convert.ToString(drRequest["ISOCountryName"]));
                var userData = BLL.SingleSignOn.GwIdpUserBLL.GetUserByGWUserId(Convert.ToInt32(drRequest["GWUserId"]));

                SendEmailChangeComplete(Convert.ToString(drRequest["UserNewEmailAddress"]),
                                Convert.ToString(userData["FirstName"]),
                                Convert.ToString(userData["LastName"]),
                                Convert.ToInt32(Convert.ToInt32(drRequest["CultureGroupID"])));
                SendEmailChangeCompleteAdminEmail(Convert.ToString(drRequest["UserOldEmailAddress"]),
                    Convert.ToString(drRequest["UserNewEmailAddress"]));

            }
        }
        private static void SendEmailChangeComplete(string email, string firstname, string lastname, int cultureGroupId)
        {
            #region "Send email using EmailAPI "

            var req = new SendTemplatedEmailCallRequest
            {
                CultureGroupId = cultureGroupId,
                EmailTemplateKey = ConfigUtility.AppSettingGetValue("EmailChangeRequestComplete"),
                ToEmailAddresses = new[] { email.Trim() }
            };



            #region " body replacement values "
            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]"},
                new ReplaceKeyValueType {ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]"}
            };

            req.BodyReplacementKeyValues = bodyR.ToArray();
            #endregion

            // Send an email with Email service
            SDK.EmailServiceManager.SendTemplatedEmail(req);

            #endregion
        }

        private static void SendEmailChangeCompleteAdminEmail(string oldUserEmail, string newUserEmail)
        {
            #region " send email(s) to web master/Account Admins"

            var email =
               new SendTemplatedDistributedEmailCallRequest
               {
                   EmailTemplateKey = ConfigUtility.AppSettingGetValue("EmailChangeRequestCompleteAdmin"),
                   DistributionListKey = ConfigUtility.AppSettingGetValue("EmailScheduler.ChangeEmailRequestComplete.DistributionKey"),
                   CultureGroupId = 1
               };

            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = oldUserEmail, TemplateKey = "[[OLDUSEREMAIL]]"},
                new ReplaceKeyValueType {ReplacementValue = newUserEmail, TemplateKey = "[[NEWUSEREMAIL]]"}
            };

            email.BodyReplacementKeyValues = bodyR.ToArray();
            SDK.EmailServiceManager.SendDistributedTemplatedEmail(email);

            #endregion
        }
        protected void btnLinkToEditAccount_Click(object sender, EventArgs e)
        {
            string gwCultureCode = Thread.CurrentThread.CurrentCulture.Name;
            Response.Redirect(ConfigUtility.AppSettingGetValue("Idp.Web.EditAccountUrl").Replace("[[LL-CC]]", gwCultureCode));
        }

        private bool ValidateEmailValidationToken(DataRow drRequest)
        {
            if (drRequest == null)
                throw new HttpException(404, "Page not found.");
            //token expired or used
            return !Convert.ToBoolean(drRequest["Expired"]) && !ConvertUtility.ConvertToBoolean(drRequest["IsUsed"], false);
        }

        private void SendAdminEmail(string oldUserEmail, string newUserEmail, string userCountry)
        {
            #region " send email(s) to web master/Account Admins"

            var email =
               new SendTemplatedDistributedEmailCallRequest
               {
                   EmailTemplateKey = KeyDef.EmailTemplates.ChangeEmailRequestAdminEmail,
                   DistributionListKey = ConfigUtility.AppSettingGetValue("Idp.Web.ChangeEmailRequest.DistributionKey"),
                   CultureGroupId = 1
               };
            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = oldUserEmail, TemplateKey = "[[OLDUSEREMAIL]]"},
                new ReplaceKeyValueType {ReplacementValue = newUserEmail, TemplateKey = "[[NEWUSEREMAIL]]"},
                new ReplaceKeyValueType {ReplacementValue = userCountry, TemplateKey = "[[USERCOUNTRY]]"},
            };

            email.BodyReplacementKeyValues = bodyR.ToArray();
            SDK.EmailServiceManager.SendDistributedTemplatedEmail(email);

            //return response.StatusCode;

            #endregion
        }
    }
}