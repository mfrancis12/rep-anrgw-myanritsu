﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="changeEmailAddress.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.ChangeEmailAddress" %>
<%@ Register TagPrefix="sbk" Namespace="Subkismet.Captcha" Assembly="Subkismet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<style type="text/css">

    .tbx-300{
        border: 1px solid #ccc;
        font-size: 11px;
        padding: 5px;
        width: 300px;
        
    }
    .field {
        display: inline-block;
        padding-left: 0;
        width: 135px;
    }
    .form-element {
        font-weight: bold;
        overflow: hidden;
        padding: 5px 10px;
    }
    .errormsg {
        color: #ff0000;
        display: block;
        padding: 0 10px;
    }
    .form-element.field {
        padding: 10px 10px 0;
        width: 150px;
    }
    input.ui-button {
        background: url("//static.cdn-anritsu.com/apps/connect/img/ui-bg_highlight-soft_35_0faf46_1x100.png") repeat-x scroll 50% 50% #0faf46;
        border: 1px solid #456d8b;
        border-radius: 7px;
        color: #fff;
        font-weight: 700;
        padding: 0.4em 1em;
        margin-left:260px;
    }
    </style>--%>
    <%--<script type="text/javascript">

        $(document).ready(function () {
            $("#cphContentTop_txtUserNewEmail").focus();
            
        });

        var changeEmailRules=<%=GetChangeEmailRules()%>
            function validateOnSubmit() {
                var newEmail = validateNewEmail('newEmailAddress', $('[id$=txtUserNewEmail]').val());
                var repeatEmail = validateRepeatEmail('repeatEmailAddress', $('[id$=txtUserConfirmEmail]').val());
                if (!newEmail || !repeatEmail) {

                    return false;
                }
                else {
                    return true;
                }
            }

        function validate(controlToValidate, currentVal) {
            switch (controlToValidate) {
                case 'newEmailAddress':
                    validateNewEmail(controlToValidate, currentVal)
                    break;
                case 'repeatEmailAddress':
                    validateRepeatEmail(controlToValidate, currentVal)
                    break;
            }
        }
        var repeatEmailId = "";
        function validateRepeatEmail(controlId, val) {
            if (val.length == 0) {
                setMessage(changeEmailRules["lcalConfirmEmailAddress.ErrorMessage"], controlId);
                return false;
            }
            else if (val === newEmailId) {
                setMessage("", controlId);
                return true
            }
            else {
                setMessage(changeEmailRules["lcalEmailAddressMissMatch.ErrorMessage"], controlId);
                return false;
            }

        }
        var newEmailId = "";
        function validateNewEmail(controlId, val) {
            if (val.length == 0) {
                setMessage(changeEmailRules["lcalNewEmailAddress.ErrorMessage"], controlId);
                return false;
            }
            else {
                validateEmail(controlId, val);
                return true;
            }

        }
        function validateEmail(controlId, val) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

            if (!emailReg.test(val)) {
                setMessage(changeEmailRules["lcalInvalidEmailAddress.ErrorMessage"], controlId);
                return false;
            } else {
                setMessage('', controlId);
                newEmailId = val;
                return true;
            }
        }
        function setMessage(message, ctrlId) {
            $('#' + ctrlId).html(message);
            $('#' + ctrlId).css('display', 'block');
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="">
        <div id="gwpnlChangeEmailForm2" runat="server" >
            <div class="dialogue-body">
                <div class="content-detail">
						<div class="form-container">
							<div class="bd width-60">
                                <div class="group input-text required form-element field">
                                     <label for="email-old"><asp:Localize ID="lcalUserOldEmail" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalUserOldEmail.Text %>'></asp:Localize>:</label>
                                     <asp:TextBox ID="txtUserOldEmail" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="group input-password required">
                             <div>
                                    <asp:Label ID="lblpwdmessage" CssClass="server-error-message" Visible="false" runat="server"></asp:Label>
                                </div>
                                      <label for="Password"><asp:Localize ID="lcalPwd" runat="server" Text="<%$Resources:~/signin,PasswordLabel.Text %>"></asp:Localize>:*</label>
                             <%--<p class="error-message"><asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="<%$Resources:~/signin,PasswordRequired.ErrorMessage %>" ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></p>--%>
                             <p class="error-message"><asp:Localize ID="Localize7" runat="server" Text="<%$Resources:~/signin,PasswordRequired.ErrorMessage %>"></asp:Localize></p>
                             <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                         </div>
                                <div class="form-element field group input-text required">
                                     <label for="email-new"><asp:Localize ID="lcalUserNewEmail" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalUserNewEmail.Text %>'></asp:Localize>:</label>
                                     <p class="error-message"><asp:Localize ID="Localize3" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalNewEmailAddress.ErrorMessage %>'></asp:Localize></p>
							         <p class="custom-error-message"><asp:Localize ID="Localize2" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalInvalidEmailAddress.ErrorMessage %>'></asp:Localize></p>
                                     <asp:TextBox ID="txtUserNewEmail" runat="server" CssClass="email email-new"></asp:TextBox>
                                </div>
                                <span class="errormsg" id="newEmailAddress"></span>
                                <div>
                                    <asp:Label ID="lblMsg" CssClass="server-error-message" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="form-element field group input-text required">
                                     <label for="email-confirm"><asp:Localize ID="lcalUserNewConfirmEmail" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalUserNewConfirmEmail.Text %>'></asp:Localize>:</label>
                                     <p class="error-message"><asp:Localize ID="Localize4" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalConfirmEmailAddress.ErrorMessage%>'></asp:Localize></p>
							         <p class="custom-error-message"><asp:Localize ID="Localize5" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalEmailAddressMissMatch.ErrorMessage%>'></asp:Localize></p>
                                     <asp:TextBox ID="txtUserConfirmEmail" runat="server" CssClass="email-confirm"></asp:TextBox>
                                </div>
                                <span class="errormsg" id="repeatEmailAddress"></span>
                                <%--<div class="form-element" style="padding:5px;">
                                    <sbk:CaptchaControl ID="captcha" LayoutStyle="CssBased" runat="server" CssClass="captcha" Style="clear: both !Important;"
                                                        ErrorMessage="<%$Resources:Common, lcalCaptchaInvalidCode.ErrorMessage %>" ForeColor="Red"
                                                        Display="Dynamic" CaptchaLength="4" ControlToValidate="txtUserNewEmail" EnableClientScript="false"
                                                        ValidationGroup="vgChangePwd" />
                                </div>--%>

                                <div class="form-element group overflow margin-top-15">
                                    <asp:Button ID="bttUpdateEmail" runat="server" Text='<% $Resources: ~/account/changeemailaddress, bttUpdateEmail.Text %>'
                                        ValidationGroup="vgChangePwd" OnClick="bttUpdateEmail_Click" CssClass=" form-submit"/>
                                    <asp:Button ID="bttUpdateEmailCancel" runat="server" Text='<% $Resources: ~/account/changeemailaddress, bttUpdateEmailCancel.Text %>' OnClick="btnLinkToEditAccount_Click"
                                        CausesValidation="false" CssClass="button"/>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
<%--        <anrui:GlobalWebBoxedPanel ID="gwpnlConfirmMsg" runat="server" ShowHeader="true" Visible="False" HeaderText='<% $Resources: ~/account/changeemailaddress, gwpnlConfirmMsg.HeaderText %>'
            Width="700px" >
             <asp:Localize ID="lcalConfirmMsg" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalConfirmMsg.Text %>'></asp:Localize>
             <asp:Button ID="btnLinkToEditAccount" runat="server" Text='<%$Resources:~/account/change-email-confirmation,btnLinkToEditAccount.Text %>'  OnClick="btnLinkToEditAccount_Click" CssClass="button"/>
                    <br />
            </anrui:GlobalWebBoxedPanel>--%>
        <asp:Panel ID="gwpnlConfirmMsg" runat="server" Visible="false" CssClass="width-60">
                <h3 class="section-sub-header"><asp:Localize ID="Localize1" runat="server" Text='<% $Resources: ~/account/changeemailaddress, gwpnlConfirmMsg.HeaderText %>'></asp:Localize></h3>
                 <asp:Localize ID="lcalConfirmMsg" runat="server" Text='<% $Resources: ~/account/changeemailaddress, lcalConfirmMsg.Text %>'></asp:Localize>
             <asp:Button ID="btnLinkToEditAccount" runat="server" Text='<%$Resources:~/account/change-email-confirmation,btnLinkToEditAccount.Text %>'  OnClick="btnLinkToEditAccount_Click" CssClass="button"/>
                    <br />
        </asp:Panel>
    </div>

    
                                     <asp:Panel runat="server" style="width:50%" Visible="false" ID="panelVerification">
                         <div class="form-container">
                     <%--<div class="heading">
                          <h2><asp:Localize ID="localverificationheader" runat="server"   Text='<%$Resources:~/account/verification,lcalverificationheader.Label %>'></asp:Localize></h2>
                     </div>--%>
                             <div>
                                 <h4><asp:Localize ID="Localize6" runat="server"   Text='<%$Resources:~/account/verification,lcalDisclaimer.Text %>'></asp:Localize></h4>
                             </div>
                              <h3>  <span id="spanmessgae" runat="server" style="color:#2F977A">
                        <asp:Localize ID="lcalErrorMsg"  runat="server" visible="false"></asp:Localize>     
                         </span></h3>
                  
                             <div id="divverification" runat="server">
                     <div class="bd">
                       
                          <div class="group input-text">
                             <asp:TextBox ID="txtVerification" placeholder='<%$Resources:~/account/verification,lcalverificationvalidationtextboxplaceholder.Text %>'   MaxLength="6"  runat="server"></asp:TextBox>
                         </div>
                         <div class='settingrow' style='padding-top: 10px;'>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="LoginUserValidationGroup"/>
                        
                    </div>
                         <div class="group">
                          <div class='btnrow'>
                             <div class='btncollft'>
                                  <div class="settingrow" style="padding-top: 10px!important;">
                                 <asp:Button ID="btnVerify" runat="server" 
                                     Text='<%$Resources:~/account/verification,lcalverificationverifybutton.Text %>'
                                       CssClass="button form-submit margin-right-15" OnClick="btnVerify_Click" />
                                   
                             </div>
                                
                             </div>
                     </div>
                    <div class='clearfloat'></div>
                </div>
                          <asp:LinkButton ID="btnresend" runat="server" Text='<%$Resources:~/account/verification,lcalverificationresend.Text %>'   CssClass="inline-block margin-top-15" OnClick="btnresend_Click"> </asp:LinkButton>&nbsp; &nbsp;
                          <span  style="color:#289b7d;font-size:14px;" id="resendspanmessage" runat="server"><asp:Literal ID="lcalResendMessage" runat="server"></asp:Literal></span>
                         
                         <br/>    
                         <asp:HyperLink ID="lnkhelp" runat="server" Text='<%$Resources:~/signin,hlHelp.Text %>' Target="_blank" NavigateUrl="https://login.anritsu.com/contact-us" CssClass="inline-block "></asp:HyperLink>
                        
                 </div>
                                 </div>
                
             </div>    
                 </asp:Panel> 
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
