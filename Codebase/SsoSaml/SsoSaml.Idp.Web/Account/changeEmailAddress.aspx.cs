﻿using System.Text;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using Anritsu.SsoSaml.Idp.Lib.SingleSignOn;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using SDK = Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.CoreLib;
using System.Threading;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using System.Web.Security;
using System.Web;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class ChangeEmailAddress : IdpBasePage
    {
        private DataRow _userData;
        System.Collections.Specialized.NameValueCollection nameValues = null;

        private String ReturnUrl
        {
            get
            {
                var returnUrl = Request[KeyDef.ParmKeys.ReturnURL].IsNullOrEmptyString()
                    ? Request[KeyDef.ParmKeys.ReplyUrl]
                    : Request[KeyDef.ParmKeys.ReturnURL];

                return GetRedirectUrl(returnUrl, ServiceProviderInfo.SpDefaultUrl);
            }
        }
        private int CultureGropuId 
        {
            get { return GetCurrentCultureGroupId(); }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
            _userData = UIHelper.LoggedInUser_Get(false);
            InitControlValues(_userData);

            bttUpdateEmailCancel.CssClass = "button";
            bttUpdateEmail.CssClass = "button form-submit";
        }
        private void CreateHistoryForBrowsers(string pagescenrio, bool addquerystring = false)
        {
            string jsobj = string.Empty;
            pagescenrio = pagescenrio + "_" + Guid.NewGuid().ToString();
            if (addquerystring && nameValues != null)
                jsobj = "var stateObj = { page:'" + pagescenrio + "' };history.pushState(stateObj, '" + pagescenrio + "', 'changeEmailAddress?" + nameValues.ToString() + "'); ";
            else
                jsobj = "var stateObj = { page:'" + pagescenrio + "' };history.pushState(stateObj, '" + pagescenrio + "', 'changeEmailAddress'); ";
            System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "verificationscript", jsobj, true);
        }
        protected void btnVerify_Click(object sender, EventArgs e)
        {

            if (Session["Verifycount"] == null)
            {
                Session["Verifycount"] = 0;
            }
            lcalErrorMsg.Visible = false;
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[0-9]{6,6}$");
            if (string.IsNullOrEmpty(txtVerification.Text.Trim()) == true || regex.IsMatch(txtVerification.Text.Trim()) == false)
            {
                lcalErrorMsg.Visible = true;
                spanmessgae.Style.Add("color", "red");
                lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationvalidation.Msg").ToString();
                return;
            }



            if (int.Parse(Session["Verifycount"].ToString()) > 5)
            {
                lcalErrorMsg.Visible = true;
                spanmessgae.Style.Add("color", "red");
                lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationmaximumattempts.Msg").ToString();
                btnVerify.Enabled = false;
                CreateHistoryForBrowsers("verifymaxattempt");
              
                return;
            }

            if (BLL.SingleSignOn.GwIdpUserBLL.VerifyValidationCode(txtUserOldEmail.Text.ToString(), txtVerification.Text.Trim()))
            {
                gwpnlConfirmMsg.Visible = true;
                gwpnlChangeEmailForm2.Visible = false;
                panelVerification.Visible = false;
               
                Response.Redirect("/account/change-email-confirmation.aspx");
            }
            else
            {
                Session["Verifycount"] = int.Parse(Session["Verifycount"].ToString()) + 1;
                lcalErrorMsg.Visible = true;
                lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationinvalid.Msg").ToString();
                spanmessgae.Style.Add("color", "red");
                CreateHistoryForBrowsers("verifyfailed");

            }
        }
        protected void btnresend_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtUserOldEmail.Text) == false)
                {
                    var email = SendActivationCodeEmail(txtUserOldEmail.Text, txtUserOldEmail.Text);
                    lcalResendMessage.Text = GetGlobalResourceObject("~/account/verification", "lcalresendclick.Msg").ToString().Replace("[[USEREMAILADDRESS]]", txtUserOldEmail.Text);
                    lcalResendMessage.Visible = true;

                }
              

            }
            catch (Exception ex)
            {
               // ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx");
            }
        }
        protected void bttUpdateEmail_Click(object sender, EventArgs e)
        {
            if (!IsValid) return;
            //Check the current password
            if (!ValidatePassword()) return;
            //Check the new email address exist in the Anritsu User Store
            if (!ValidateNewEmailId()) return;
            SendActivationCodeEmail(txtUserOldEmail.Text, txtUserOldEmail.Text);
            //make an entry in to the database and send the email validation link to new email address and send confirmation link to old email address.
            var drChangeEmailRequest =
              GwIdpUserBLL.InsertUserEmailChangeRequest(Convert.ToInt32(_userData["GwUserId"]),
                  txtUserOldEmail.Text.Trim(), txtUserNewEmail.Text.Trim(), CultureGropuId);
            Session["EmailToken"] = Guid.Parse(drChangeEmailRequest["EmailToken"].ToString());
            gwpnlChangeEmailForm2.Visible = false;
            panelVerification.Visible = true;
           

           
        }
        private void UpdateEmail()
        {
            //make an entry in to the database and send the email validation link to new email address and send confirmation link to old email address.
            var drChangeEmailRequest =
                GwIdpUserBLL.InsertUserEmailChangeRequest(Convert.ToInt32(_userData["GwUserId"]),
                    txtUserOldEmail.Text.Trim(), txtUserNewEmail.Text.Trim(), CultureGropuId);



            //send emails to user new email address
            var validationEmailStatusCode = SendEmailValidationLink(txtUserNewEmail.Text.Trim(),
                Convert.ToString(_userData["FirstName"]),
                Convert.ToString(_userData["LastName"]), Guid.Parse(drChangeEmailRequest["EmailToken"].ToString()));
            var confirmationEmailStatusCode = SendChangeEmailConfirmation(txtUserOldEmail.Text.Trim(), Convert.ToString(_userData["FirstName"]),
                Convert.ToString(_userData["LastName"]));
            if (validationEmailStatusCode != 0 || confirmationEmailStatusCode != 0) return;
            gwpnlConfirmMsg.Visible = true;
            gwpnlChangeEmailForm2.Visible = false;
        }

        protected void btnLinkToEditAccount_Click(object sender, EventArgs e)
        {
            string gwCultureCode = Thread.CurrentThread.CurrentCulture.Name;
            Response.Redirect(ConfigUtility.AppSettingGetValue("Idp.Web.MyAccountUrl").Replace("[[LL-CC]]", gwCultureCode));
        }

        private void InitControlValues(DataRow userData)
        {
            if (userData == null) return;
            txtUserOldEmail.Text = userData["EmailAddress"].ToString();
        }

        private bool ValidatePassword()
        {
          
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null) RedirectToLoginPage();
            var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
            if (GwIdpUserBLL.CheckCurrentPassword(gwUserId, Password.Text.Trim())) return true;
            lblpwdmessage.Text = GetResourceString("IdpAuthStatus", "AuthStatus_InvalidUserOrPassword");
            lblpwdmessage.Visible = true;
            return false;

          
        }

        private void RedirectToLoginPage()
        {
            FormsAuthentication.SignOut();
            //String addOnQueryStrings = String.Format("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            var addOnQs = new StringBuilder();
            //addOnQS.AppendFormat("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            addOnQs.AppendFormat("{0}={1}", KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(ReturnUrl));
            FormsAuthentication.RedirectToLoginPage(addOnQs.ToString());
        }

        private bool ValidateNewEmailId()
        {
            //check the new email address 
            if (!GwIdpUserBLL.CheckEmailAddressExist(txtUserNewEmail.Text.Trim())) return true;
            lblMsg.Text = Convert.ToString(GetGlobalResourceObject("~/account/changeemailAddress", "lcalEmailExists.ErrorMessage"));
            lblMsg.Visible = true;
            return false;
        }

        private int SendEmailValidationLink(string email, string firstname, string lastname, Guid token)
        {
            #region "Send email using EmailAPI "

            var req = new SendTemplatedEmailCallRequest
            {
                CultureGroupId = GetCurrentCultureGroupId(),
                EmailTemplateKey = KeyDef.EmailTemplates.ChangeEmailVerificationUserEmail,
                ToEmailAddresses = new[] {email.Trim()}
            };

            #region " subject replacement values "

            var subjR = new List<ReplaceKeyValueType>();
            req.SubjectReplacementKeyValues = subjR.ToArray();
            #endregion

            #region " body replacement values "
            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]"},
                new ReplaceKeyValueType {ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]"}
            };

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[VERIFICATIONLINK]]",
                ReplacementValue = ConfigurationManager.AppSettings["ChangeEmailValidationLinkUrl"]
                    .Replace("[[validationToken]]", token.ToString().ToUpper())
                    .Replace("[[LL-CC]]", UIHelper.GetCulture())
            };
            bodyR.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTUSLINK]]",
                ReplacementValue = ConfigurationManager.AppSettings["Idp.Web.ContactUsUrl"]
            };
            bodyR.Add(keyValue);

            req.BodyReplacementKeyValues = bodyR.ToArray();
            #endregion

            // Send an email with Email service
            var res = SDK.EmailServiceManager.SendTemplatedEmail(req);
            return res.StatusCode;

            #endregion
        }

        private int SendChangeEmailConfirmation(string email, string firstname, string lastname)
        {
            #region "Send email using EmailAPI "

            var req = new SendTemplatedEmailCallRequest
            {
                CultureGroupId = CultureGropuId,
                EmailTemplateKey = KeyDef.EmailTemplates.ChangeEmailConfirmationEmail,
                ToEmailAddresses = new[] { email.Trim() }
            };

            #region " subject replacement values "

            var subjR = new List<ReplaceKeyValueType>();
            req.SubjectReplacementKeyValues = subjR.ToArray();
            #endregion

            #region " body replacement values "
            var bodyR = new List<ReplaceKeyValueType>
            {
                new ReplaceKeyValueType {ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]"},
                new ReplaceKeyValueType {ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]"}
            };

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTUSLINK]]",
                ReplacementValue = ConfigurationManager.AppSettings["Idp.Web.ContactUsUrl"]
            };
            bodyR.Add(keyValue);

            req.BodyReplacementKeyValues = bodyR.ToArray();
            #endregion

            // Send an email with Email service
            var res = SDK.EmailServiceManager.SendTemplatedEmail(req);
            return res.StatusCode;

            #endregion
        }

        public String GetChangeEmailRules()
        {
            var rules = new StringBuilder("{");
            rules.Append(String.Format(@"""lcalNewEmailAddress.ErrorMessage"":""{0}"",""lcalConfirmEmailAddress.ErrorMessage"":""{1}"",""lcalEmailAddressMissMatch.ErrorMessage"":""{2}"",""lcalInvalidEmailAddress.ErrorMessage"":""{3}""",
                GetGlobalResourceObject("~/account/changeemailaddress", "lcalNewEmailAddress.ErrorMessage"),
            GetGlobalResourceObject("~/account/changeemailaddress", "lcalConfirmEmailAddress.ErrorMessage"),
            GetGlobalResourceObject("~/account/changeemailaddress", "lcalEmailAddressMissMatch.ErrorMessage"),
            GetGlobalResourceObject("~/account/changeemailaddress", "lcalInvalidEmailAddress.ErrorMessage")));
            return rules.Append("}").ToString();
        }

    }
}