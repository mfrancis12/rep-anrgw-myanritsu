﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="changepwd.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.ChangePwd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

	<%--<style type="text/css">
         #crumbcontainer
        {
            display: none;
        }
        .updatePwd {
            margin: 10px 10px 10px 70px;
            width:600px;
        }
    </style>--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="updatePwd  width-60 left inline-block">
                    <div class="dialogue-header">
                        <h3> <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:~/account/changepwd,pnlUpdatePwd.HeaderText %>" /></h3>
					</div>
					<div class="dialogue-body">
					  <div class="content-detail">
						<div class="form-container">
							<div class="bd">
							  <div class="group input-text settingrow">
								<label for="accountLogin"> <span class="settinglabel-10"><asp:Localize ID="Localize1" runat="server" Text='<%$Resources:~/account/updatepwd,lcalUserEmail.Text %>'></asp:Localize>:</span></label>
								<%--<input type="text" id="accountLogin" class="email" disabled=true value="anirudhn@techaspect.com">--%>
                                <asp:TextBox ID="txtUserEmail" runat="server" ReadOnly="true" Enabled="false" CssClass="email"></asp:TextBox>
							  </div>

                                <%--<div class="settingrow">
                                    <span class="settinglabel-10">
                                        <asp:Localize ID="Localize1" runat="server" Text='<%$Resources:~/account/updatepwd,lcalUserEmail.Text %>'></asp:Localize>:</span>
                                    <asp:TextBox ID="TextBox1" runat="server" SkinID="tbx-300" ReadOnly="true" Enabled="false"></asp:TextBox>
                                </div>--%>


							  <div class="group input-password required settingrow">
								<label for="oldPassword"> <span class="settinglabel-10"><asp:Localize ID="lcalNewPwd" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwd.Text %>'></asp:Localize>:</span></label>
								<p class="error-message"><asp:Localize ID="Localize2" runat="server" Text='<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage %>'></asp:Localize></p>
							    <p class="custom-error-message"><asp:Localize ID="Localize3" runat="server" Text='<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage %>'></asp:Localize></p>
								<%--<input type="password" id="oldPassword" class="">--%>
                                <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" CssClass="newPassword"></asp:TextBox>
                                  <%--<asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtNewPwd"
                                        ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage%>"
                                        SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revNewPwd" runat="server" ControlToValidate="txtNewPwd"
                                        ValidationExpression="^.*(?=.{8,})(?=.*[a-zA-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\$%\^\*]{0,}).*$"
                                        ErrorMessage="<%$Resources:~/account/updatepwd,revNewPwd.ErrorMessage%>" ValidationGroup="vgChangePwd"
                                        SetFocusOnError="true">*</asp:RegularExpressionValidator>--%>
							  </div>
							  <%--<div class="settingrow">
                                    <span class="settinglabel-10">
                                        <asp:Localize ID="lcalNewPwd" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwd.Text %>'></asp:Localize>:</span>
                                    <asp:TextBox ID="txtNewPwd" runat="server" SkinID="tbx-300" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtNewPwd"
                                        ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage%>"
                                        SetFocusOnError="true">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revNewPwd" runat="server" ControlToValidate="txtNewPwd"
                                        ValidationExpression="^.*(?=.{8,})(?=.*[a-zA-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\$%\^\*]{0,}).*$"
                                        ErrorMessage="<%$Resources:~/account/updatepwd,revNewPwd.ErrorMessage%>" ValidationGroup="vgChangePwd"
                                        SetFocusOnError="true">*</asp:RegularExpressionValidator>
              
                                </div>--%>
								<div class="group input-password required settingrow">
									<label for="cphContentTop_txtNewPwd"><span class="settinglabel-10">
                                        <asp:Localize ID="lcalNewPwdConfirm" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwdConfirm.Text %>'></asp:Localize>:</span></label>
									<p class="error-message"><asp:Localize ID="Localize4" runat="server" Text='<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage %>'></asp:Localize></p>
									<p class="custom-error-message"><asp:Localize ID="Localize5" runat="server" Text='<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage %>'></asp:Localize> </p>
									<%--<input type="password" id="cphContentTop_txtNewPwd" class="newPassword" onblur="validate('new-password',this.value)">--%>
                                    <asp:TextBox ID="txtNewPwdConfirm" runat="server" TextMode="Password" CssClass="confirmPassword"></asp:TextBox>
                                   <%-- <asp:RequiredFieldValidator ID="rfvNewPwdConfirm" runat="server" ControlToValidate="txtNewPwdConfirm"
                                        ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>"
                                        SetFocusOnError="true">*</asp:RequiredFieldValidator><asp:CompareValidator ID="cvPwdMatch"
                                            ControlToValidate="txtNewPwdConfirm" ControlToCompare="txtNewPwd" ErrorMessage="<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>"
                                            runat="server" ValidationGroup="vgChangePwd" SetFocusOnError="true">*</asp:CompareValidator>--%>
								</div>

                               <%-- <div class="settingrow">
                                    <%--<span class="settinglabel-10">
                                        <asp:Localize ID="lcalNewPwdConfirm" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwdConfirm.Text %>'></asp:Localize>:</span>
                                    <asp:RequiredFieldValidator ID="rfvNewPwdConfirm" runat="server" ControlToValidate="txtNewPwdConfirm"
                                        ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>"
                                        SetFocusOnError="true">*</asp:RequiredFieldValidator><asp:CompareValidator ID="cvPwdMatch"
                                            ControlToValidate="txtNewPwdConfirm" ControlToCompare="txtNewPwd" ErrorMessage="<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>"
                                            runat="server" ValidationGroup="vgChangePwd" SetFocusOnError="true">*</asp:CompareValidator>
                                </div>--%>
							  
							  <div class="group overflow settingrow margin-top-15">
                                  <span class="settinglabel-10">&nbsp;</span>
                                    <asp:Button ID="bttUpdatePwd" runat="server" SkinID="btngreenbg" Text='<%$Resources:~/account/updatepwd,bttUpdatePwd.Text %>'
                                        ValidationGroup="vgChangePwd" OnClick="bttUpdatePwd_Click"  CssClass="button form-submit left" />
                  
                                  <%--<asp:Button SkinID="btngreenbg" ID="bttUpdatePwdCancel" runat="server" Text='<%$Resources:~/account/updatepwd,bttUpdatePwdCancel.Text %>'
                                        CausesValidation="false" OnClick="bttUpdatePwdCancel_Click" CssClass="button right" />--%>
								<%--<button type="submit" class="button form-submit left">submit</button>
								<button type="button" class="button right">Cancel</button>--%>
							  </div>
							</div>
						</div>
					  </div>
					</div>
                    <%--<div class="form-element overflow"><span class="left" id="pGuidelinesText">Password Guidelines</span><span class="what"></span></div>
                    <div class="guidelines">
                            <asp:Localize ID="lcalPwdGuide" runat="server" Text='<%$Resources:~/account/updatepwd,lcalPwdGuide.Text %>'></asp:Localize>
                    </div>--%>
                    

          </div>
    <div class="right-links">
        <div class="form-element overflow guidelines-wrapper"><span id="pGuidelinesText" class="left"></span><span class='what'></span></div>
                    <div class="guidelines">
                        <asp:Localize ID="lcalPwdGuide" runat="server" Text='<%$Resources:~/account/updatepwd,lcalPwdGuide.Text %>'></asp:Localize>
                    </div>
    </div>
    <div class='updatePwd width-60'>
        <%--<anrui:GlobalWebBoxedPanel ID="pnlUpdatePwd" runat="server" ShowHeader="true" HeaderText="<%$Resources:~/account/changepwd,pnlUpdatePwd.HeaderText %>"
            Width="700px">--%>
            <%--<div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalUserEmail" runat="server" Text='<%$Resources:~/account/updatepwd,lcalUserEmail.Text %>'></asp:Localize>:</span>
                <asp:TextBox ID="txtUserEmail" runat="server" SkinID="tbx-300" ReadOnly="true" Enabled="false"></asp:TextBox>
            </div>--%>
            <%--<div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalNewPwd" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwd.Text %>'></asp:Localize>:</span>
                <asp:TextBox ID="txtNewPwd" runat="server" SkinID="tbx-300" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtNewPwd"
                    ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,rfvNewPwd.ErrorMessage%>"
                    SetFocusOnError="true">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revNewPwd" runat="server" ControlToValidate="txtNewPwd"
                    ValidationExpression="^.*(?=.{8,})(?=.*[a-zA-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\$%\^\*]{0,}).*$"
                    ErrorMessage="<%$Resources:~/account/updatepwd,revNewPwd.ErrorMessage%>" ValidationGroup="vgChangePwd"
                    SetFocusOnError="true">*</asp:RegularExpressionValidator>
              
            </div>--%>
           <%-- <div class="settingrow">
                <span class="settinglabel-10">
                    <asp:Localize ID="lcalNewPwdConfirm" runat="server" Text='<%$Resources:~/account/updatepwd,lcalNewPwdConfirm.Text %>'></asp:Localize>:</span>
                <asp:TextBox ID="txtNewPwdConfirm" runat="server" SkinID="tbx-300" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNewPwdConfirm" runat="server" ControlToValidate="txtNewPwdConfirm"
                    ValidationGroup="vgChangePwd" ErrorMessage="<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>"
                    SetFocusOnError="true">*</asp:RequiredFieldValidator><asp:CompareValidator ID="cvPwdMatch"
                        ControlToValidate="txtNewPwdConfirm" ControlToCompare="txtNewPwd" ErrorMessage="<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>"
                        runat="server" ValidationGroup="vgChangePwd" SetFocusOnError="true">*</asp:CompareValidator>
            </div>--%>
           <%-- <div class="settingrow">
                <span class="settinglabel-10">&nbsp;</span>
                <asp:Button ID="bttUpdatePwd" runat="server" SkinID="btngreenbg" Text='<%$Resources:~/account/updatepwd,bttUpdatePwd.Text %>'
                    ValidationGroup="vgChangePwd" OnClick="bttUpdatePwd_Click" OnClientClick="ClearDisplayMsg()" />
                  
              <asp:Button SkinID="btngreenbg" ID="bttUpdatePwdCancel" runat="server" Text='<%$Resources:~/account/updatepwd,bttUpdatePwdCancel.Text %>'
                    CausesValidation="false" OnClick="bttUpdatePwdCancel_Click" />

            </div>--%>
            <div class="settingrow">
                <asp:ValidationSummary ID="vsumChangePwd" runat="server" ValidationGroup="vgChangePwd"
                    ShowMessageBox="false" />
                <asp:Label ID="lblMsg" runat="server" CssClass="msg"></asp:Label>
               <%-- <p>
                    <asp:Localize ID="lcalPwdGuide" runat="server" Text='<%$Resources:~/account/updatepwd,lcalPwdGuide.Text %>'></asp:Localize>
                </p>--%>
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
