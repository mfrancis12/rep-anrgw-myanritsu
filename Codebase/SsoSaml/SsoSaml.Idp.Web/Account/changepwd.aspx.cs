﻿using System;
using System.Web;
using System.Data;
using System.Text;
using System.Web.Security;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.AnrCommon.CoreLib;
using System.Collections;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class ChangePwd : IdpBasePage
    {
        private String ReturnUrl
        {
            get
            {
                var returnUrl = Request[KeyDef.ParmKeys.ReturnURL].IsNullOrEmptyString()
                    ? Request[KeyDef.ParmKeys.ReplyUrl]
                    : Request[KeyDef.ParmKeys.ReturnURL];

                return GetRedirectUrl(returnUrl, ServiceProviderInfo.SpDefaultUrl);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Form.Attributes["autocomplete"] = "off";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null)
            {
                RdirectToLoginPage();
                return;
            }
            InitControlValues(userData);
        }

        private void InitControlValues(DataRow userData)
        {
            if (userData == null) return;
            txtUserEmail.Text = userData["EmailAddress"].ToString();
        }

        private void RdirectToLoginPage()
        {
            FormsAuthentication.SignOut();
            var addOnQs = new StringBuilder();
            addOnQs.AppendFormat("{0}={1}", KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(ReturnUrl));
            FormsAuthentication.RedirectToLoginPage(addOnQs.ToString());
        }

        protected void bttUpdatePwd_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            Page.Validate("vgChangePwd");
            if (!Page.IsValid) return;
            //check for valid anritsu password
            string erroMessage;
            var isValidAnritsuPassword = IsValidAnritsuPassword(txtNewPwd.Text, out erroMessage);
            if (!isValidAnritsuPassword)
            {
                lblMsg.Text = erroMessage;
                return;
            }
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null) RdirectToLoginPage();

            var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
            var newPwd = txtNewPwd.Text.Trim();
            var userIp = WebUtility.GetUserIP();
            var returnUrl = ReturnUrl;
            Lib.SingleSignOn.GwIdpUserBLL.UpdateTempPassword(gwUserId, newPwd, true, userIp);
            Response.Redirect(returnUrl, true);
        }

        private bool IsValidAnritsuPassword(string password, out string messages)
        {
            messages = "";
            var al = new ArrayList();

            if (password.IndexOf("'", StringComparison.Ordinal) >= 0)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordApostrophe").ToString());
            }

            if (password.IndexOf(" ", StringComparison.Ordinal) >= 0)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordBlankCharacters").ToString());
            }

            if (password.Trim().Length < 8)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordLength").ToString());
            }

            const string specialChars = "!@#$%^*-";
            var hasDigit = false;
            var hasLetter = false;
            var hasBadSpecialChar = false;
            for (var i = 0; i <= password.Length - 1; i++)
            {
                var c = password[i];

                if (Char.IsLetterOrDigit(c))
                {
                    if (!hasDigit)
                        hasDigit = Char.IsDigit(c);

                    if (!hasLetter)
                        hasLetter = Char.IsLetter(c);
                }
                else
                {
                    if (!hasBadSpecialChar)
                        hasBadSpecialChar = (specialChars.IndexOf(c) == -1);
                }

            }

            if (!hasDigit)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordDigit").ToString());
            }
            if (!hasLetter)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordAlpha").ToString());
            }

            if (hasBadSpecialChar)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordSpecialCharacters").ToString());
            }

            if (al.Count <= 0) return true;
            var sb = new StringBuilder(100);

            var errText = GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationError").ToString();
            if (al.Count > 1)
                errText = GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationErrors").ToString();

            sb.Append("<table>");
            sb.Append(@"<tr><td colspan=""2""><font color=""red"" face=""verdana"" size=""1.8""><b>" + errText +
                      ":</b></font></td><tr>");

            foreach (string str in al)
            {
                sb.Append(
                    String.Format(
                        @"<tr><td valign=""top""><li style=""COLOR: red""></li></li></td><td><font color=""red"" face=""verdana"" size=""1.8"">{0}</font></td><tr>",
                        str));
            }
            sb.Append("</table>");

            messages = sb.ToString();
            return false;
        }
    }
}