﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="edit-my-account.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.edit_my_account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <asp:ScriptManager ID="myScriptmanager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ContactWebMasterPanel" runat="server" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <%--<div>                
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="Welcome" runat="server"  Text="Welcome"></asp:Label>
                        </td>
                        <td align="right">
                            <img src="/images/li.gif" alt="" /><asp:Localize runat="server" ID="lcalChangepwd" ></asp:Localize>
                            Text="<%$Resources:~/account/new-user-registration,lcalChangePwd.Text%> "
                            </a>
                        </td>
                    </tr>
                </table>
            </div>--%>

            <div id="EditAccountBox" class="boxitem content-detail-70" runat="server">
                <!-- ListItem -->
                <div class="boxitemtitle">
                    <div class="boxitemtitleleft">
                    </div>
                    <div class="boxitemtitleright">
                    </div>
                    <div class="boxitemtitletext">
                        <%--<asp:Localize runat="server" ID="lclBoxTitle" Text="<%$Resources:~/account/new-user-registration,lcalEditBoxTitle.Text%> "></asp:Localize>--%>
                    </div>
                </div>
                <div class="boxitembody">
                    <div class="content-form">
                        <div class="content-detail" style="padding: 0px">
                            <%--<h1><asp:Label ID="Welcome" runat="server"  Text="Welcome"></asp:Label></h1>--%>
                            <div class="form-container">

                                <%--<h2><asp:Localize runat="server" ID="lclBoxTitle" Text="<%$Resources:~/account/new-user-registration,lcalEditBoxTitle.Text%> "></asp:Localize></h2>--%>
                                <h3 class="section-sub-header">
                                    <asp:Localize runat="server" ID="lclBoxTitle" Text="<%$Resources:~/account/new-user-registration,lcalEditInfo.Text%>"></asp:Localize>
                                </h3>
                                <%--<p><strong>*Required</strong></p>
                    <div style="line-height:0px;padding-bottom:20px" class="hd">
                      <h3>Your information</h3>
                    </div>--%>

                                <asp:PlaceHolder ID="phRegfrm" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                        <div class="content-aside right-links">
                            <p>
                                <asp:Localize runat="server" ID="lcalChangepwd"></asp:Localize>
                                <i class="icon icon-arrow-blue"></i>
                            </p>
                            <p>
                                <asp:HyperLink ID="hypChangeEmail" runat="server"></asp:HyperLink>
                                <i class="icon icon-arrow-blue"></i>
                            </p>
                        </div>
                    </div>
                    
                    <div id="div_mailsubc" runat="server">
                        <div>
                            <asp:Localize runat="server" ID="lcalNotifymail" Text="<%$Resources:~/account/new-user-registration, lcalNotifymail.Text%> "></asp:Localize>
                        </div>
                        <p class="radio-container">
                            <asp:CheckBox ID="ReceiveMails" SkinID="RadioBlock" Text="<%$Resources:~/account/new-user-registration, rdBtnReceivemail.Text %>"
                                Checked="true" runat="server" />
                        </p>
                    </div>
                   <%-- <div class="settingrow company-profile-radio-container" style="padding:5px 0">
                        <p class="radio-container">
                            <asp:RadioButton ID="rdoIndividualAnritsuID" SkinID="RadioBlock" runat="server" Checked="true" GroupName="EmailTypeGroup"
                                Text="<%$Resources:~/account/new-user-registration,rdoIndividualAnritsuID.Text%>" />
                        </p>
                        <p class="radio-container">
                            <asp:RadioButton ID="rdoGroupAnritsuID" SkinID="RadioBlock" runat="server" GroupName="EmailTypeGroup"
                                Text="<%$Resources:~/account/new-user-registration,rdoGroupAnritsuID.Text%>" />
                        </p>
                    </div>--%>
                    <div>
                        <asp:Localize runat="server" ID="lclEamilComm" Text="<%$Resources:~/account/new-user-registration,lcalUnsubscribeEmailCommunications%>" Visible="false"></asp:Localize>

                    </div>
                    <div>
                        <asp:Label ID="lblMessage" runat="server" CssClass="server-error-message"></asp:Label>
                    </div>
                    
                    <br />
                    <div class="divcenter">
                        <asp:Button ID="Save" runat="server" Text="<%$Resources:Common,Save %>"
                            UseSubmitBehavior="true" SkinID="btngreenbg" OnClick="Save_Click" ValidationGroup="1" CssClass="form-submit button" />
                        <asp:Button ID="bttCancel" SkinID="btngreenbg" Text="<%$Resources:Common, Cancel%>"
                            runat="server" CausesValidation="false"
                            OnClick="bttCancel_Click" CssClass="button"></asp:Button>
                    </div>
                </div>
                <%--<div class="boxitemfooter">
                    <div>
                        <img src="/images/wdwftrl.gif" alt="" />
                    </div>
                    <div class="movetoright">
                        <img src="/images/wdwftrr.gif" alt="" />
                    </div>
                </div>--%>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Save" EventName="Click" />
        </Triggers>

    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="ContactWebMasterPanel">
        <ProgressTemplate>
            <div class="updateProgress">
                <img src="/images/indicator.gif" alt="" /><asp:Literal ID="ltrProcess" runat="server"
                    Text="<%$Resources:Common,Processing%>"></asp:Literal>
            </div>
            <div class="updateProgressbackground">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
