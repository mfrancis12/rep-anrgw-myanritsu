﻿using AjaxControlToolkit;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Web.App_Controls;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.Security;
using System.Text;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using Anritsu.AnrCommon.CoreLib;
using System.Web.UI.HtmlControls;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class edit_my_account : IdpBasePage
    {
        #region Private Properties/Fields
        private const string Classkey = "~/account/new-user-registration";
        private DataTable _countriesList;
        private int _cultureGroupId;
        private string _strState = string.Empty;
        private DataTable _receiveMailsInfo;

        void postalCodeRegExHandler()
        {
            bool bTe;
            var strTe = CountryName.SelectedValue;
            switch (strTe)
            {
                case "DE":
                case "FR":
                case "US":
                    bTe = true;
                    break;
                default:
                    bTe = false;
                    break;
            }
            //  PostalCodeRegExpValidator.Enabled = bTe;
        }

        private TextBox FirstName
        {
            get;
            set;
        }

        private TextBox LastName
        {
            get;
            set;
        }

        private TextBox Company
        {
            get;
            set;
        }

        private TextBox JobTitle
        {
            get;
            set;
        }

        private TextBox Phone
        {
            get;
            set;
        }

        private TextBox FaxNumber
        {
            get;
            set;
        }

        private TextBox Address1
        {
            get;
            set;
        }

        private TextBox Address2
        {
            get;
            set;
        }

        private TextBox City
        {
            get;
            set;
        }

        private StateControl State
        {
            get;
            set;
        }

        private DropDownList CountryName
        {
            get;
            set;
        }

        private TextBox PostalCode
        {
            get;
            set;
        }

        private TextBox Email
        {
            get;
            set;
        }

        private TextBox LoginName
        {
            get;
            set;
        }

        private RegularExpressionValidator PostalCodeRegExpValidator
        {
            get;
            set;
        }

        private Label lblMark
        {
            get;
            set;
        }

        private TextBox FirstNameInRoman
        {
            get;
            set;
        }
        private TextBox LastNameInRoman
        {
            get;
            set;
        }
        private TextBox CompanyInRoman
        {
            get;
            set;
        }
        private RadioButton rdoIndividualAnritsuID { get; set; }
        private RadioButton rdoGroupAnritsuID { get; set; }
        #endregion

        #region Page Events

        private void Page_Init(object sender, EventArgs e)
        {
            // Initilize Registration form controls dynamically
            InitRegistrationFormControls();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
          

            lcalChangepwd.Text = GetGlobalResourceObject(Classkey, "lcalChangePwd.Text").ToString().Replace("[[UPDATEPWDLINK]]", "UpdatePwd?" + KeyDef.ParmKeys.ReturnURL + "=" + Request.Url.AbsoluteUri + "&" + KeyDef.ParmKeys.SsoIDP.SiteLocale + "=" + Request.QueryString[KeyDef.ParmKeys.SsoIDP.SiteLocale]);
            LoadControlsRecursively(phRegfrm);
            _cultureGroupId = GetCurrentCultureGroupId();
            State.ValidationGroup = "1";

            if (IsPostBack)
            {
                if (CountryName.SelectedValue.Equals("US"))
                {
                    //  PostalCodeRegExpValidator.Enabled = State._IsUnitedStates = lblMark.Visible = true;
                    State._IsUnitedStates = true;
                    State._IsJapan = false;
                }
                else if (CountryName.SelectedValue.Equals("JP"))
                {
                    //  PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                    State._IsJapan = true;
                    State._IsUnitedStates = false;
                }
                else
                {
                    // PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                    State._IsJapan = State._IsUnitedStates = false;

                }
            }
            else
            {
                var li = new ListItem(GetGlobalResourceObject("Common", "Select").ToString(), "Select");

                _countriesList = ISOCountryBLL.Country_SelectByCultureGroupId(_cultureGroupId);
                CountryName.DataSource = _countriesList;
                CountryName.DataTextField = "CountryName";
                CountryName.DataValueField = "Alpha2";
                CountryName.DataBind();
                CountryName.Items.Insert(0, li);

                #region populating User details

                if (Page.IsPostBack) return;
                // Getting the logged in user details 

                PopulateUserData();
                CountryName_SelectedIndexChanged(this, e);
               
            }

        }
                #endregion

        public void PopulateUserData()
        {
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null)
            {
                 RedirectToLoginPage();
            }
            else
            {
            
                //if (GetCurrentCulture() == "ja-JP")
                //    Welcome.Text = userData["LastName"] + " " + userData["FirstName"];
                //else
                //    Welcome.Text = userData["FirstName"] + " " + userData["LastName"];


                // Populating User details in the controls which are to be Editable 
                // (Based on XML file order)
                var doc = XElement.Load(Server.MapPath("~/App_Data/RegFiledOrder.xml"));

                var browserLang = GetCurrentCulture();

                var fields = (from item in doc.Descendants("Region")
                              where item.Attribute("Name").Value == (browserLang ?? "default")
                              select item).ToList();
                if (fields.Count == 0)
                {
                    fields = (from item in doc.Descendants("Region")
                              where item.Attribute("Name").Value == "default"
                              select item).ToList();
                }

                var sortedFields = (from i in fields.Descendants("field")
                                    where bool.Parse(i.Attribute("isEditable").Value)
                                    orderby int.Parse(i.Attribute("fieldorder").Value)
                                    select i).ToList();

                foreach (var item in sortedFields)
                {
                    switch (item.Attribute("uniquename").Value)
                    {
                        case RegFromFieldIds.FirstName:
                            FirstName.Text = userData["FirstName"].ToString();
                            break;
                        case RegFromFieldIds.LastName:
                            LastName.Text = userData["LastName"].ToString();
                            break;
                        case RegFromFieldIds.FirstName_In_Roman:
                            FirstNameInRoman.Text = userData["FirstNameInRoman"].ToString();
                            break;
                        case RegFromFieldIds.LastName_In_Roman:
                            LastNameInRoman.Text = userData["LastNameInRoman"].ToString();
                            break;
                        case RegFromFieldIds.Company:
                            Company.Text = userData["CompanyName"].ToString();
                            break;
                        case RegFromFieldIds.Company_In_Roman:
                            CompanyInRoman.Text = userData["CompanyInRoman"].ToString();
                            break;
                        case RegFromFieldIds.JobTitle:
                            JobTitle.Text = userData["JobTitle"].ToString();
                            break;
                        case RegFromFieldIds.Phone:
                            Phone.Text = userData["PhoneNumber"].ToString();
                            break;
                        case RegFromFieldIds.FaxNumber:
                            FaxNumber.Text = userData["FaxNumber"].ToString();
                            break;
                        case RegFromFieldIds.Address1:
                            Address1.Text = userData["StreetAddress1"].ToString();
                            break;
                        case RegFromFieldIds.Address2:
                            Address2.Text = userData["StreetAddress2"].ToString();
                            break;
                        case RegFromFieldIds.City:
                            City.Text = userData["City"].ToString();
                            break;
                        case RegFromFieldIds.State:
                            _strState = userData["State"].ToString();
                            break;
                        case RegFromFieldIds.PostalCode:
                            PostalCode.Text = userData["ZipCode"].ToString();
                            break;
                        case RegFromFieldIds.CountryName:
                            CountryName.SelectedValue = userData["CountryCode"].ToString();
                            break;
                        case RegFromFieldIds.Email:
                            Email.Text = userData["EmailAddress"].ToString();
                            break;
                    }
                }
                LoginName.Text = userData["LoginName"].ToString();

                _receiveMailsInfo = BLL.SingleSignOn.GwIdpUserBLL.GetUserInfo(int.Parse(userData["GWUserId"].ToString()));
                if (_receiveMailsInfo.Rows.Count > 0)
                    ReceiveMails.Checked = (_receiveMailsInfo.Rows[0]["SendNewsMail"].ToString().Equals("True"));

                Boolean IsIndividualEmailAddress = ConvertUtility.ConvertToBoolean(userData["IsIndividualEmailAddress"],true);
                if (IsIndividualEmailAddress)
                   rdoIndividualAnritsuID.Checked= true;
                else
                    rdoGroupAnritsuID.Checked = true;

                

                div_mailsubc.Visible = !ReceiveMails.Checked;
                lclEamilComm.Visible = ReceiveMails.Checked;
            }
        }


        #region PagePreRender
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (CountryName.SelectedItem.Value.Equals("US"))
            {
                State.refreshControl(this, e, "US");
                State.setState(true, false, _strState);
            }
            else if (CountryName.SelectedItem.Value.Equals("JP"))
            {
                State.refreshControl(this, e, "JP");
                State.setState(false, true, _strState);
            }
            else
                State.setState(false, false, _strState);

            postalCodeRegExHandler();
        }
        #endregion

        #endregion

        #region Local Methods

        private void InitRegistrationFormControls()
        {
            var doc = XElement.Load(Server.MapPath("~/App_Data/RegFiledOrder.xml"));

            var browserLang = GetCurrentCulture();

            var fields = (from item in doc.Descendants("Region")
                          where item.Attribute("Name").Value == (browserLang ?? "default")
                          select item).ToList();
            if (fields.Count == 0)
            {
                fields = (from item in doc.Descendants("Region")
                          where item.Attribute("Name").Value == "default"
                          select item).ToList();
            }
            var sortedFields = (from i in fields.Descendants("field")
                                where bool.Parse(i.Attribute("isEditable").Value)
                                orderby int.Parse(i.Attribute("fieldorder").Value)
                                select i).ToList();


            var table = new Table { CssClass = "edittable" };
            var it = 0;
            var fieldsDiv = new HtmlGenericControl("div");
            fieldsDiv.Attributes.Add("class", "bd");
             var fieldsDivMyinfo = new HtmlGenericControl("div");
            fieldsDivMyinfo.Attributes.Add("class", "bd");
            TableRow tr = null;
            foreach (var field in sortedFields)
            {

                switch (field.Attribute("uniquename").Value)
                {
                    case RegFromFieldIds.FirstName:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FirstName, GetGlobalResourceObject(Classkey, "ltrlFirstName").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvFirstName.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.LastName:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.LastName, GetGlobalResourceObject(Classkey, "ltrlLastName").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvLastName.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.FirstName_In_Roman:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FirstName_In_Roman, GetGlobalResourceObject(Classkey, "ltrlFirstNameinRoman").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvFirstNameinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.LastName_In_Roman:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.LastName_In_Roman, GetGlobalResourceObject(Classkey, "ltrlLastNameinRoman").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvLastNameinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Company:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Company, GetGlobalResourceObject(Classkey, "ltrlCompany.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvCompany.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Company_In_Roman:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Company_In_Roman, GetGlobalResourceObject(Classkey, "ltrlCompanyinRoman.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvCompanyinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.JobTitle:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.JobTitle, GetGlobalResourceObject(Classkey, "ltrlJobtitle.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvJobTitle.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Phone:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Phone, GetGlobalResourceObject(Classkey, "ltrlPhone.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvPhone.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.FaxNumber:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FaxNumber,
                            GetGlobalResourceObject(Classkey, "ltrlFax.Text").ToString(), false, "group input-text",
                            "Text", 100, null, false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Address1:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Address1, GetGlobalResourceObject(Classkey, "ltrlAddress1.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvAddress.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Address2:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Address2, GetGlobalResourceObject(Classkey, "ltrlAddress2.Text").ToString(), false, "group input-text", "Text", 100, null, false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.City:
                        fieldsDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.City, GetGlobalResourceObject(Classkey, "ltrlCity.Text").ToString(), true, "group input-text required", "Text", 50, GetGlobalResourceObject(Classkey, "rfvCity.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.State:
                        statediv = new HtmlGenericControl("div");
                        statediv.Attributes.Add("class", "group input-select country-select required");
                        statediv.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlState.Text").ToString()));
                        statediv.Controls.Add(CreateLabel("lblMark", false, "*"));
                        statediv.Controls.Add(CreateStateControl(RegFromFieldIds.State, "1"));
                        fieldsDiv.Controls.Add(statediv);
                        break;
                    case RegFromFieldIds.PostalCode:
                        var divPc = CreateTextBoxDiv(RegFromFieldIds.PostalCode, GetGlobalResourceObject(Classkey, "ltrlPostalCode.Text").ToString(), true, "group input-text required", "Text", 10, GetGlobalResourceObject(Classkey, "rfvPostalCode.ErrorMessage").ToString(), true, "\\d{5}(-\\d{4})?", GetGlobalResourceObject(Classkey, "revPostalCode.ErrorMessage").ToString(),"zipcode");
                        fieldsDiv.Controls.Add(divPc);
                        break;
                    case RegFromFieldIds.CountryName:
                        var divCn = new HtmlGenericControl("div");
                        divCn.Attributes.Add("class", "group input-select country-select required");
                        divCn.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlCountry.Text").ToString()));
                        CreateRequiredTag(GetGlobalResourceObject(Classkey, "rfvCountry.ErrorMessage").ToString(), divCn, "error-message");
                        divCn.Controls.Add(CreateDropDownList(RegFromFieldIds.CountryName, "ddl-212", true,
                            new EventHandler(CountryName_SelectedIndexChanged)));
                        
                        fieldsDiv.Controls.Add(divCn);
                        break;
                    case RegFromFieldIds.Email:
                        var divEm = CreateTextBoxDiv(RegFromFieldIds.Email, GetGlobalResourceObject(Classkey, "ltrlEmail.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvEmailId.ErrorMessage").ToString(), true, "\\s*\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\s*", GetGlobalResourceObject(Classkey, "revEmailId.ErrorMessage").ToString());
                        fieldsDivMyinfo.Controls.Add(divEm);
                        break;
                    case RegFromFieldIds.ConfirmEmail:
                        var divConEm = CreateTextBoxDiv(RegFromFieldIds.ConfirmEmail, GetGlobalResourceObject(Classkey, "ltrlConfirmEmail.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvEmailId.ErrorMessage").ToString(), false, string.Empty, string.Empty);
                        fieldsDivMyinfo.Controls.Add(divConEm);
                        break;
                }


            }
            // Add Login name in the emailAddress row 
            var divLogin = new HtmlGenericControl("div");
            divLogin.Attributes.Add("class", "group input-text");
            divLogin.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlLoginname.Text").ToString()));
            CreateRequiredTag(GetGlobalResourceObject(Classkey, "revLoginName.ErrorMessage").ToString(), divLogin, "custom-error-message");
            var txtbox = CreateTextBox(RegFromFieldIds.LoginName, 8);
            txtbox.Attributes.Add("data-regex", "^[a-zA-Z0-9][a-zA-Z0-9-_.]{4,6}[a-zA-Z0-9]$");
            divLogin.Controls.Add(txtbox);
            var divradio = new HtmlGenericControl("div");
            divradio.Attributes.Add("class", "settingrow company-profile-radio-container");
            divradio.Attributes.Add("style", "padding: 5px 0");
            divradio.Controls.Add(CreateRadioButton("rdoIndividualAnritsuID", "RadioBlock", true, "EmailTypeGroup", GetGlobalResourceObject(Classkey, "rdoIndividualAnritsuID.Text").ToString()));
            divradio.Controls.Add(CreateRadioButton("rdoGroupAnritsuID", "RadioBlock", false, "EmailTypeGroup", GetGlobalResourceObject(Classkey, "rdoGroupAnritsuID.Text").ToString()));
            divLogin.Controls.Add(divradio);

            //fieldsDiv.Controls.Add(divLogin);
            fieldsDivMyinfo.Controls.Add(divLogin);
            fieldsDivMyinfo.Controls.Add(divradio);
            // Add Login Msg in a seperate row at the bottom  te
            tr = new TableRow();

            //var tchyplink = new TableCell();
            //tchyplink.Controls.Add(CreateImage("img1", "ChangeEmailAddress"));
            //tchyplink.Controls.Add(CreateHyperLink("changeEmail", GetGlobalResourceObject(Classkey, "hypChangeEmail.Text").ToString(), Color.Blue, "./changeEmailAddress" + Request.Url.Query));
            hypChangeEmail.Text = GetGlobalResourceObject(Classkey, "hypChangeEmail.Text").ToString();
            hypChangeEmail.NavigateUrl = "./changeEmailAddress" + Request.Url.Query;
            //tchyplink.ColumnSpan = 2;
            var divLoginErr = new HtmlGenericControl("div");
            divLoginErr.Attributes.Add("class", "group input-text");
            divLoginErr.Controls.Add(CreateLabel("lblLoginErrMsg", Color.Gray, GetGlobalResourceObject(Classkey, "lblLoginErrMsg.Text").ToString()));
            fieldsDiv.Controls.Add(divLoginErr);
            phRegfrm.Controls.Add(fieldsDivMyinfo);
            var h3 = new HtmlGenericControl("h3");
            h3.Attributes.Add("class", "section-sub-header");
            h3.InnerText = GetGlobalResourceObject(Classkey, "lcalEditCompInfo.Text").ToString();
            phRegfrm.Controls.Add(h3);
            phRegfrm.Controls.Add(fieldsDiv);
            
        }


        private HtmlGenericControl CreateTextBoxDiv(string id, string label, bool isRequired, string cssClass, string controlType, int maxLen, string errorMsg, bool IsregexReq, string regex, string regexMsg, string textBoxCss = "")
        {
            var div = new HtmlGenericControl("div");
            div.Attributes.Add("class", cssClass);
            switch (controlType)
            {
                case "Text": Label lbl = new Label();
                    lbl.Attributes.Add("for", id);
                    lbl.Text = label;
                    div.Controls.Add(lbl);
                    if (isRequired)
                    {
                        CreateRequiredTag(errorMsg, div, "error-message");
                        if (IsregexReq)
                        {
                            CreateRequiredTag(regexMsg, div, "custom-error-message");
                        }
                    }
                    TextBox tbox = CreateTextBox(id, maxLen);
                    if (IsregexReq)
                    {
                        tbox.Attributes.Add("data-regex", regex);
                    }
                    if (!String.IsNullOrEmpty(textBoxCss))
                        tbox.Attributes.Add("class", textBoxCss);
                    div.Controls.Add(tbox);

                    break;
            }


            return div;
        }

        private static void CreateRequiredTag(string errorMsg, HtmlGenericControl div, string cssClass)
        {
            var pValidation = new HtmlGenericControl("p");
            pValidation.Attributes.Add("class", cssClass);
            pValidation.InnerText = errorMsg;
            div.Controls.Add(pValidation);
        }
        private ValidatorCalloutExtender CreateValidatorCalloutExtender(string id, string targetControlId)
        {
            var vce = new ValidatorCalloutExtender { ID = id, TargetControlID = targetControlId };
            //vce.CssClass = "customCalloutStyle";
            return vce;
        }

        private RequiredFieldValidator CreateRequiredFieldValidator(string id, string controlToValidate, ValidatorDisplay display, bool setFocusOnError, string validationGroup, string errorMessage)
        {
            var rfv = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationGroup = validationGroup,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString()
            };
            return rfv;
        }

        private RequiredFieldValidator CreateRequiredFieldValidator(string id, string controlToValidate, ValidatorDisplay display, bool setFocusOnError, string validationGroup, string errorMessage, string initialValue)
        {
            var rfv = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationGroup = validationGroup,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                InitialValue = initialValue
            };
            return rfv;
        }

        private RegularExpressionValidator CreateRegularExpressionValidator(string id, bool enabled, string errorMessage, ValidatorDisplay display, bool setFocusOnError, string validationExpression, string controlToValidate, string validationGroup)
        {
            var rev = new RegularExpressionValidator
            {
                ID = id,
                Enabled = enabled,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationExpression = validationExpression,
                ControlToValidate = controlToValidate,
                ValidationGroup = validationGroup
            };
            return rev;
        }

        private CompareValidator CreateCompareValidator(string id, string controlToValidate, string controlToCompare, string errorMessage, ValidatorDisplay display, bool setOnFocusError, string validationGroup)
        {
            var cv = new CompareValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                ControlToCompare = controlToCompare,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                Display = display,
                SetFocusOnError = setOnFocusError,
                ValidationGroup = validationGroup
            };
            return cv;
        }

        private TextBox CreateTextBox(string id, int maxLength)
        {
            var txtBox = new TextBox { ID = id, MaxLength = maxLength };
            if (id == "Email") txtBox.Enabled = false;
            return txtBox;
        }

        private HtmlGenericControl CreateRadioButton(string id, string skinid, bool check, string GroupName, string Text)
        {
            var radiop = new HtmlGenericControl("p");
            radiop.Attributes.Add("class", "radio-container");
            var rbtn = new RadioButton { ID = id, SkinID = skinid, Checked = check, GroupName = GroupName, Text = Text };
            radiop.Controls.Add(rbtn);
            return radiop;
        }

        private Label CreateLabel(string id, Color foreColor, string text)
        {
            var lbl = new Label { ID = id, Text = text, ForeColor = foreColor };
            //lbl.Font.Size = fontUnit;
            return lbl;
        }
        private HyperLink CreateHyperLink(string id, string text, Color forecolor, string link)
        {
            var hypLink = new HyperLink { ID = id, Text = text, NavigateUrl = link, ForeColor = forecolor };
            return hypLink;
        }
        private System.Web.UI.WebControls.Image CreateImage(string id, string text)
        {
            var img = new System.Web.UI.WebControls.Image { ID = id, AlternateText = text, ImageUrl = "../images/li.gif" };
            return img;

        }

        private Label CreateLabel(string id, bool visible, string text)
        {
            var lbl = new Label { ID = id, Text = text, Visible = visible };
            return lbl;
        }

        private StateControl CreateStateControl(string id, string validationGroup)
        {
            var ucState = (StateControl)LoadControl("/App_Controls/UcStateControl.ascx");
            ucState.ValidationGroup = validationGroup;
            ucState.ID = id;
            return ucState;
        }

        private DropDownList CreateDropDownList(string id, string skinId, bool autoPostBack, EventHandler eventHandler)
        {
            var ddl = new DropDownList { ID = id, SkinID = skinId, AutoPostBack = autoPostBack };
            ddl.SelectedIndexChanged += eventHandler;
            return ddl;
        }

        private Literal CreateLiteral(string text)
        {
            var lt = new Literal { Text = text };
            return lt;
        }

        /*
                private Control FindControlRecursive(Control control, string id)
                {
                    if (control == null) return null;
                    //try to find the control at the current level
                    var ctrl = control.FindControl(id);

                    if (ctrl != null) return ctrl;
                    //search the children
                    foreach (Control child in control.Controls)
                    {
                        ctrl = FindControlRecursive(child, id);

                        if (ctrl != null)
                        {
                            break;
                        }
                    }
                    return ctrl;
                }
        */

        private void LoadControlsRecursively(Control rootControl)
        {
            foreach (Control child in rootControl.Controls)
            {
                switch (child.ID)
                {
                    case RegFromFieldIds.FirstName:
                        if (child is TextBox)
                            FirstName = (TextBox)child;
                        break;
                    case RegFromFieldIds.LastName:
                        if (child is TextBox)
                            LastName = (TextBox)child;
                        break;
                    case RegFromFieldIds.FirstName_In_Roman:
                        if (child is TextBox)
                            FirstNameInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.LastName_In_Roman:
                        if (child is TextBox)
                            LastNameInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.Company:
                        if (child is TextBox)
                            Company = (TextBox)child;
                        break;
                    case RegFromFieldIds.Company_In_Roman:
                        if (child is TextBox)
                            CompanyInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.JobTitle:
                        if (child is TextBox)
                            JobTitle = (TextBox)child;
                        break;
                    case RegFromFieldIds.Phone:
                        if (child is TextBox)
                            Phone = (TextBox)child;
                        break;
                    case RegFromFieldIds.FaxNumber:
                        if (child is TextBox)
                            FaxNumber = (TextBox)child;
                        break;
                    case RegFromFieldIds.Address1:
                        if (child is TextBox)
                            Address1 = (TextBox)child;
                        break;
                    case RegFromFieldIds.Address2:
                        if (child is TextBox)
                            Address2 = (TextBox)child;
                        break;
                    case RegFromFieldIds.City:
                        if (child is TextBox)
                            City = (TextBox)child;
                        break;
                    case RegFromFieldIds.State:
                        if (child is StateControl)
                            State = (StateControl)child;
                        break;
                    case RegFromFieldIds.PostalCode:
                        if (child is TextBox)
                            PostalCode = (TextBox)child;
                        break;
                    case RegFromFieldIds.CountryName:
                        if (child is DropDownList)
                            CountryName = (DropDownList)child;
                        break;
                    case RegFromFieldIds.Email:
                        if (child is TextBox)
                            Email = (TextBox)child;
                        break;
                    case RegFromFieldIds.LoginName:
                        if (child is TextBox)
                            LoginName = (TextBox)child;
                        break;
                    case "rdoIndividualAnritsuID":
                        if (child is RadioButton)
                            rdoIndividualAnritsuID = (RadioButton)child;
                        break;
                    case "rdoGroupAnritsuID":
                        if (child is RadioButton)
                            rdoGroupAnritsuID = (RadioButton)child;
                        break;
                    case "PostalCodeRegExpValidator":
                        if (child is RegularExpressionValidator)
                            PostalCodeRegExpValidator = (RegularExpressionValidator)child;
                        break;
                    case "lblMark":
                        if (child is Label)
                            lblMark = (Label)child;
                        break;
                }
                LoadControlsRecursively(child);
            }
        }


        /*
                private void SetCountryBasedOnCultureGroupID()
                {
                    string countryName = string.Empty;

                    countryName = ISOCountryBLL.GetCountryNameForCultureGroupId(GetCurrentCultureGroupId());

                    DataRow[] r = _countriesList.Select(string.Format("CountryName = '{0}'", countryName.Replace("'", "''")));
                    if (_cultureGroupId == 3) //EMEA
                    {
                        CountryName.SelectedValue = "Select";
                    }
                    else
                    {
                        CountryName.SelectedValue = r.Length == 0 ? "US" : r[0]["Alpha2"].ToString();
                    }
                    if (CountryName.SelectedItem.Value.Equals("US"))
                        lblMark.Visible = true;
                    else
                        lblMark.Visible = State.IsStateRequired;
                }
        */
      
        #endregion

        #region PostBack Events

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsValid) return;
                var userData = UIHelper.LoggedInUser_Get(true);
                if (userData == null || !Context.User.Identity.IsAuthenticated)
                {
                   
                    // Response.Redirect("http://www.anritsu.com");
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
             
                var updateUserRequest = new UpdateUserCallRequestType();
                var OldUserRequest = new UpdateUserCallRequestType();
                var userObj = new GWSsoUserType();
                var olduserObj = new GWSsoUserType();

                if (BLL.SingleSignOn.GwIdpUserBLL.IsLoginNameExistingToEdit(LoginName.Text, userData["LoginName"].ToString()))
                {
                    lblMessage.Text = GetGlobalResourceObject(Classkey, "ErrMsgLoginNameAlreadyExists").ToString();
                }
                else
                {
                    // Mapping the user Details Based on the Controls which are editable(based on XML file)
                    var doc = XElement.Load(Server.MapPath("~/App_Data/RegFiledOrder.xml"));

                    var browserLang = GetCurrentCulture();

                    var fields = (from item in doc.Descendants("Region")
                                  where item.Attribute("Name").Value == (browserLang ?? "default")
                                  select item).ToList();
                    if (fields.Count == 0)
                    {
                        fields = (from item in doc.Descendants("Region")
                                  where item.Attribute("Name").Value == "default"
                                  select item).ToList();
                    }
                    var sortedFields = (from i in fields.Descendants("field")
                                        where bool.Parse(i.Attribute("isEditable").Value)
                                        orderby int.Parse(i.Attribute("fieldorder").Value)
                                        select i).ToList();
                    foreach (var item in sortedFields)
                    {
                        switch (item.Attribute("uniquename").Value)
                        {
                            case RegFromFieldIds.FirstName:
                                userObj.FirstName = FirstName.Text.Trim();
                                break;
                            case RegFromFieldIds.LastName:
                                userObj.LastName = LastName.Text.Trim();
                                break;
                            case RegFromFieldIds.FirstName_In_Roman:
                                userObj.FirstNameInRoman = FirstNameInRoman.Text.Trim();
                                break;
                            case RegFromFieldIds.LastName_In_Roman:
                                userObj.LastNameInRoman = LastNameInRoman.Text.Trim();
                                break;
                            case RegFromFieldIds.Company:
                                userObj.CompanyName = Company.Text.Trim();
                                break;
                            case RegFromFieldIds.Company_In_Roman:
                                userObj.CompanyInRoman = CompanyInRoman.Text.Trim();
                                break;
                            case RegFromFieldIds.JobTitle:
                                userObj.JobTitle = JobTitle.Text.Trim();
                                break;
                            case RegFromFieldIds.Phone:
                                userObj.PhoneNumber = Phone.Text.Trim();
                                break;
                            case RegFromFieldIds.FaxNumber:
                                userObj.FaxNumber = FaxNumber.Text.Trim();
                                break;
                            case RegFromFieldIds.Address1:
                                userObj.StreetAddress1 = Address1.Text;
                                break;
                            case RegFromFieldIds.Address2:
                                userObj.StreetAddress2 = Address2.Text;
                                break;
                            case RegFromFieldIds.City:
                                userObj.City = City.Text;
                                break;
                            case RegFromFieldIds.State:
                                userObj.State = State.SelectedStateValue;
                                break;
                            case RegFromFieldIds.PostalCode:
                                userObj.PostalCode = PostalCode.Text;
                                break;
                            case RegFromFieldIds.CountryName:
                                userObj.CountryCode = CountryName.SelectedValue;
                                break;
                            case RegFromFieldIds.Email:
                                userObj.EmailAddress = Email.Text;
                                break;
                        }
                    }
                    
                    foreach (var item in sortedFields)
                    {
                        switch (item.Attribute("uniquename").Value)
                        {
                            case RegFromFieldIds.FirstName:
                                olduserObj.FirstName = userData["FirstName"].ToString();
                                break;
                            case RegFromFieldIds.LastName:
                                olduserObj.LastName = userData["LastName"].ToString();
                                break;
                            case RegFromFieldIds.FirstName_In_Roman:
                                olduserObj.FirstNameInRoman = userData["FirstNameInRoman"].ToString();
                                break;
                            case RegFromFieldIds.LastName_In_Roman:
                                olduserObj.LastNameInRoman = userData["LastNameInRoman"].ToString();
                                break;
                            case RegFromFieldIds.Company:
                                olduserObj.CompanyName = userData["CompanyName"].ToString();
                                break;
                            case RegFromFieldIds.Company_In_Roman:
                                olduserObj.CompanyInRoman = userData["CompanyInRoman"].ToString();
                                break;
                            case RegFromFieldIds.JobTitle:
                                olduserObj.JobTitle = userData["JobTitle"].ToString();
                                break;
                            case RegFromFieldIds.Phone:
                                olduserObj.PhoneNumber = userData["PhoneNumber"].ToString();
                                break;
                            case RegFromFieldIds.FaxNumber:
                                olduserObj.FaxNumber = userData["FaxNumber"].ToString();
                                break;
                            case RegFromFieldIds.Address1:
                                olduserObj.StreetAddress1 = userData["StreetAddress1"].ToString();
                                break;
                            case RegFromFieldIds.Address2:
                                olduserObj.StreetAddress2 = userData["StreetAddress2"].ToString();
                                break;
                            case RegFromFieldIds.City:
                                olduserObj.City = userData["City"].ToString();
                                break;
                            case RegFromFieldIds.State:
                                olduserObj.State = userData["State"].ToString();
                                break;
                            case RegFromFieldIds.PostalCode:
                                olduserObj.PostalCode = userData["ZipCode"].ToString();
                                break;
                            case RegFromFieldIds.CountryName:
                                olduserObj.CountryCode = userData["CountryCode"].ToString();
                                break;
                            case RegFromFieldIds.Email:
                                olduserObj.EmailAddress = userData["EmailAddress"].ToString();
                                break;
                        }
                    }
                    userObj.LoginName = LoginName.Text;
                    userObj.UserId = int.Parse(userData["GWUserId"].ToString());
                    userObj.PrimaryCultureCode = UIHelper.GetCulture();
                  //  userObj.EncryptedTempPassword = userData["EncryptedPassword"].ToString();
                    Boolean IsIndividualEmailAddress = true;
                    if (rdoIndividualAnritsuID.Checked)
                        IsIndividualEmailAddress = true;
                    else
                        IsIndividualEmailAddress = false;

                    userObj.IsIndividualEmailAddress = IsIndividualEmailAddress;

                    var usrType = new GWSsoUserTypeType
                    {
                        UserTypeId = int.Parse(userData["UserTypeID"].ToString()),
                        UserType = userData["UserTypeName"].ToString(),
                        IsUserProfile = bool.Parse(userData["UserTypeIsUserProfile"].ToString())
                    };
                    userObj.UserType = usrType;

                    var usrStatus = new GWSsoUserStatusType
                    {
                        UserStatusId = int.Parse(userData["UserStatusId"].ToString())
                    };
                    userObj.UserStatus = usrStatus;
                    updateUserRequest.GWSsoUser = userObj;
                    
                    OldUserRequest.GWSsoUser = olduserObj;
                  

                    OldUserRequest.GWSsoUser.IsIndividualEmailAddress = IsIndividualEmailAddress;
                    updateUserRequest.GWSsoUser.IsIndividualEmailAddress = IsIndividualEmailAddress;

                    // Update user details in SSO and My anritsu DB
                    BLL.SingleSignOn.GwIdpUserBLL.UpdateUserDetails(updateUserRequest, OldUserRequest);

                    // Update userInfo details
                    BLL.SingleSignOn.GwIdpUserBLL.UpdateUserInfo(userObj.UserId, ReceiveMails.Checked);

                    // Getting the Updated User data by userId
                   
                    userData = BLL.SingleSignOn.GwIdpUserBLL.GetUserByGWUserId(userObj.UserId);

                    // Setting the logged in session after update
                    UIHelper.LoggedInUser_Set(userData);


                    var returnUrl = Request[KeyDef.ParmKeys.ReturnURL].IsNullOrEmptyString()
                        ? Request[KeyDef.ParmKeys.ReplyUrl]
                        : Request[KeyDef.ParmKeys.ReturnURL];
                    lblMessage.Text = returnUrl.IsNullOrEmptyString()
                        ? GetGlobalResourceObject(Classkey, "lcalEditThankyouMsg.Text").ToString()
                        : GetGlobalResourceObject(Classkey, "lcalEditThankyouMsgWithReturnURL.Text")
                            .ToString()
                            .Replace("[[RETURNURL]]", ReturnURL);

                    PopulateUserData();
                    CountryName_SelectedIndexChanged(this, e);
                }
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ddlchange", "$('#" + State.ClientID + "').select();$(\"select[id $= 'cmbState']\").select();", true);
                
            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx",true);
            }
        }
        HtmlGenericControl statediv;
        protected void CountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCountry = CountryName.SelectedValue.ToString();
            if (selectedCountry.Equals("US"))
            {
                State.refreshControl(this, e, selectedCountry);
                statediv.Attributes.Add("class", "group input-select country-select");
            }
            else if (selectedCountry.Equals("JP"))
            {
                State.refreshControl(this, e, selectedCountry);
                statediv.Attributes.Add("class", "group input-select country-select");
            }
            else
            {
                statediv.Attributes.Add("class", "group input-text country-select");
            }
            //PostalCodeRegExpValidator.ValidationGroup = "1";

            switch (selectedCountry)
            {
                case "US":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{5}(-\d{4})?");
                        // PostalCodeRegExpValidator.ValidationExpression = @"\d{5}(-\d{4})?";
                        break;
                    }
                case "JP":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{3}(-(\d{4}|\d{2}))?");
                        //PostalCodeRegExpValidator.ValidationExpression = @"\d{3}(-(\d{4}|\d{2}))?";
                        break;
                    }
                case "DE":
                    {
                        PostalCode.Attributes.Add("data-regex", @"(D-)?\d{5}");
                        //PostalCodeRegExpValidator.ValidationExpression = @"(D-)?\d{5}";
                        break;
                    }
                case "FR":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{5}");
                        // PostalCodeRegExpValidator.ValidationExpression = @"\d{5}";
                        break;
                    }
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ddlchange", "$('#" + CountryName.ClientID + "').select();$(\"select[id $= 'cmbState']\").select();", true);

        }

        #endregion

        private void RedirectToLoginPage()
        {
            FormsAuthentication.SignOut();
            //String addOnQueryStrings = String.Format("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            var addOnQs = new StringBuilder();
            //addOnQS.AppendFormat("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            addOnQs.AppendFormat("{0}={1}&{2}={3}", KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(ReturnURL),KeyDef.ParmKeys.SiteLocale
                    , HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.SiteLocale].ConvertNullToEmptyString()));
            FormsAuthentication.RedirectToLoginPage(addOnQs.ToString());
        }

        protected void bttCancel_Click(object sender, EventArgs e)
        {
            //Response.Redirect(ReturnURL);
            Response.Redirect(ConfigUtility.AppSettingGetValue("Idp.Web.MyAccountUrl").Replace("[[LL-CC]]", GetCurrentCulture()));
        }

        public string GetCurrentCulture()
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
        }
    }
}