﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" CodeBehind="myaccount.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.myaccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTopWithBanner" runat="server">
    <asp:ScriptManager ID="myScriptmanager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ContactWebMasterPanel" runat="server" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
<div class="full-container">
  <div class="myaccount-hero-banner"><span><% =GetGlobalResourceObject("~/account/myaccount", "PageTitle") %></span></div>
</div>

<div class="container page-height" style="padding: 0 20px">
  <div class="rightside">
    <div class="settingrow">
      <div class="container marginTop">
        <div class="myanritsu-menu">
          <ul>
            <span id="cphContentTop_HomeSupportResourcesCtrl1_dlAddOnLinks" style="display:inline-block;width:100%;">
              <span>
                <li>
                  <a  id="hypEditAcc" runat="server" href="#">
                    <div>
                      <div class="myanritsu-icon edit-my-account"></div>
                      <p><% =GetGlobalResourceObject("~/account/edit-my-account", "PageTitle") %></p>
                    </div>
                  </a>
                </li>
              </span>
              <span>
                <li>
                  <a id="hypChangepwd" runat="server" href="#">
                    <div>
                      <div class="myanritsu-icon change-my-password"></div>
                      <p><% =GetGlobalResourceObject("~/account/updatepwd", "PageTitle") %></p>
                    </div>
                  </a>
                </li>
              </span>
              <span>
                <li>
                  <a id="hypChangeEmail" runat="server" href="#">
                    <div>
                      <div class="myanritsu-icon change-my-email"></div>
                      <p><% =GetGlobalResourceObject("~/account/changeemailAddress", "PageTitle") %></p>
                    </div>
                  </a>
                </li>
              </span>
            </span>
          </ul>
        </div>
      </div>
    </div>
    <div class="settingrow">
    </div>
  </div>
</div>
    </ContentTemplate>
         </asp:UpdatePanel>
    </asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
