﻿using AjaxControlToolkit;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Web.App_Controls;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.Security;
using System.Text;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using Anritsu.AnrCommon.CoreLib;
using System.Web.UI.HtmlControls;
using Anritsu.SsoSaml.Idp.Web.App_MasterPages;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class myaccount : IdpBasePage
    {
        private const string Classkey = "~/account/new-user-registration";
        private void RedirectToLoginPage()
        {
            FormsAuthentication.SignOut();
            //String addOnQueryStrings = String.Format("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            var addOnQs = new StringBuilder();
            //addOnQS.AppendFormat("{0}={1}", KeyDef.ParmKeys.SsoIDP.SpTokenID, ServiceProviderInfo.SpTokenID);
            addOnQs.AppendFormat("{0}={1}&{2}={3}", KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(ReturnURL), KeyDef.ParmKeys.SiteLocale
                    , HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.SiteLocale].ConvertNullToEmptyString()));
            FormsAuthentication.RedirectToLoginPage(addOnQs.ToString());
        }

     
        protected void Page_Load(object sender, EventArgs e)
        {
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null)
            {
                RedirectToLoginPage();
            }
            var master = (MasterPageSiteDefault)Master;
            master.GreenPageTitleCtrl.Visible = false;


            hypEditAcc.HRef= "./edit-my-account" + Request.Url.Query;
            hypChangeEmail.HRef = "./changeEmailAddress" + Request.Url.Query;
            hypChangepwd.HRef = "./UpdatePwd?" + KeyDef.ParmKeys.ReturnURL + "=" + Request.Url.AbsoluteUri + "&" + KeyDef.ParmKeys.SsoIDP.SiteLocale + "=" + Request.QueryString[KeyDef.ParmKeys.SsoIDP.SiteLocale];
        }
    }
}