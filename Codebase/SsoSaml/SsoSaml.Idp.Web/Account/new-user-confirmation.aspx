﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="new-user-confirmation.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.new_user_confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <asp:Panel runat="server" ID="pnlUserConfirmation">
        <div class="boxitem">
            <div class="boxitemtitle">
                <div class="boxitemtitleleft"></div>
                <div class="boxitemtitleright"></div>
               <%-- <div class="boxitemtitletext">
                    <asp:Localize runat="server" ID="lcalthankyouHeader" Text="<%$Resources:~/account/new-user-confirmation,lcalthankyouHeader.Text%>"></asp:Localize>
                </div>--%>
            </div>
            <div class="boxitembody" style="text-align:center;font-size:24px;font-family:Segoe UI,Meiryo,Hiragino Sans,Hiragino Kaku Gothic ProN,Helvetica Neue,Helvetica,Arial,sans-serif";>
                <asp:Localize runat="server" ID="lcalMsg"></asp:Localize>
                <br />  
                <asp:Button ID="btnSendNewLink" runat="server" SkinID="btngreenbg" OnClick="btnSendNewLink_Click" Text="<%$Resources:~/account/new-user-confirmation,btnSendNewLink.Text%>" Visible="false" />
                &nbsp;&nbsp;  
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
