﻿using System;
using System.Web;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using CoreUtils = Anritsu.AnrCommon.CoreLib;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class new_user_confirmation : IdpBasePage
    {
        public Guid RegisterUserToken
        {
            get
            {
                var tkn = WebUtility.ReadRequest(KeyDef.ParmKeys.CommonParams.RTK, true);
                if (string.IsNullOrEmpty(tkn) || !MethodExt.IsGuid(tkn) || Request.Browser.Crawler)
                    throw new HttpException(404, "Page not found.");

                var tknGuid = new Guid(tkn);
                if (tknGuid == Guid.Empty)
                    throw new HttpException(404, "Page not found.");
                return tknGuid;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ValidateToken();
            }
        }

        public Guid ServiceProviderToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID], Guid.Empty);
            }
        }

        private void ValidateToken()
        {
            var r = BLL.SingleSignOn.GwIdpUserBLL.GetActiveUserTokenInfo(RegisterUserToken);
            if (r == null)
            {
                NotValidActivationLink();
            }
            else if (Convert.ToBoolean(r["IsUsed"]))
            {
                InvalidToken();
            }
            else if (Convert.ToBoolean(r["IsExpired"]))
            {
                ExpiredToken(r["UserEmail"].ToString());
            }
            else
            {
                InsertRegisteredUserData(int.Parse(r["GWTempUserId"].ToString()));
            }
        }

        private void InvalidToken()
        {
            var contactUrl = "../contact-us" + Request.Url.Query;
            lcalMsg.Text =
                GetGlobalResourceObject("~/account/new-user-confirmation", "lcalOldBadLink.ErrorMessage")
                    .ToString()
                    .Replace("[[CONTACTUSLINK]]", contactUrl);
        }

        private void ExpiredToken(string userEmail)
        {
            var contactUrl = "../contact-us" + Request.Url.Query;
            btnSendNewLink.Visible = true;
            lcalMsg.Text =
                GetGlobalResourceObject("~/account/new-user-confirmation", "lcalExpiredLnk.Msg")
                    .ToString()
                    .Replace("[[USEREMAIL]]", userEmail)
                    .Replace("[[CONTACTUSLINK]]", contactUrl);
        }

        private void InsertRegisteredUserData(int gwTempUserId)
        {

            // Inserting Active user details in SSo-User table
            var regUserdet = BLL.SingleSignOn.GwIdpUserBLL.InsertActiveSSOUser(gwTempUserId);

            var defaultToken =
                ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID"), Guid.Empty);
            var spToken = ServiceProviderToken != Guid.Empty ? ServiceProviderToken : defaultToken;


            var plainPwd = GetPlainPwd(gwTempUserId, regUserdet.Tables[1].Rows[0]["Password"].ToString());

            // Inserting encrypted Password of registered user ,password is encrypted in BLL layer
            var userSpLoginUrl =
                BLL.SingleSignOn.GwIdpUserBLL.UpdateUserPassword(
                    int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()), plainPwd, spToken.ToString(), false);

            // Add MyAnritsu Role to User
            var dataroles = BLL.SingleSignOn.GwIdpUserBLL.GetUserRoleByRoleTitle("MyAnritsu");
            BLL.SingleSignOn.GwIdpUserBLL.InsertUserUserRoleReference(
                int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()),
                Guid.Parse(dataroles["UserRoleId"].ToString()));

            // Inserting userinformation in Info table
            BLL.SingleSignOn.GwIdpUserBLL.InsertUserInfo(
                int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()),
                Convert.ToBoolean(regUserdet.Tables[1].Rows[0]["AcceptEmail"]));

            pnlUserConfirmation.Visible = true;

            var signInUrl = userSpLoginUrl["SpLoginUrl"].ToString();
            var returnUrl = ReplyURL;
            if (!String.IsNullOrEmpty(returnUrl))
            {
                signInUrl +=
                    String.Format(
                        "?ReturnURL={0}&" + KeyDef.ParmKeys.CommonParams.IsNew + "=True&lang=" + UIHelper.GetCulture(),
                        HttpUtility.UrlEncode(returnUrl.Trim()));
            }
            else
            {
                signInUrl +=
                    String.Format("?" + KeyDef.ParmKeys.CommonParams.IsNew + "=True&lang=" + UIHelper.GetCulture(),
                        HttpUtility.UrlEncode(returnUrl.Trim()));
            }

            Response.Redirect(signInUrl, false);
        }

        private string GetPlainPwd(int gwTempUserId, string encryptedPwd)
        {
            if (gwTempUserId < 1 || encryptedPwd.IsNullOrEmptyString()) return String.Empty;
            var enc =
                new AnrCommon.CoreLib.CryptoUtilities.EncryptionSalt();
            enc.SetEncryptionKeyByIntId(gwTempUserId);
            return enc.DecryptData(encryptedPwd);
        }

        protected void btnSendNewLink_Click(object sender, EventArgs e)
        {
            var ds = BLL.SingleSignOn.GwIdpUserBLL.Get_InActive_UserToken(RegisterUserToken);
            if (ds.Tables.Count == 0) return;
            var statuscode = SendActivationLinkEmail(ds.Tables[1].Rows[0]["EmailAddress"].ToString()
                , ds.Tables[1].Rows[0]["FirstName"].ToString()
                , ds.Tables[1].Rows[0]["LastName"].ToString(), Guid.Parse(ds.Tables[0].Rows[0]["Token"].ToString()));
            if (statuscode == 0)
            {
                var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                if (nameValues.Count > 0)
                    Response.Redirect("new-user-registration-thankyou.aspx?EmailId=" +
                                      ds.Tables[1].Rows[0]["EmailAddress"] + "&" + nameValues);
                else
                    Response.Redirect("new-user-registration-thankyou.aspx?EmailId=" +
                                      ds.Tables[1].Rows[0]["EmailAddress"]);
            }
            else
            {
                Response.Redirect("../error.aspx");
            }
        }

        /// <summary>
        /// This method is to handle the false scenario where user is trying to use 
        /// old activation link when having new activation link already generated
        /// Reference ticket number GWS-4856
        /// </summary>        
        private void NotValidActivationLink()
        {
            var contactUrl = "../contact-us" + Request.Url.Query;
            btnSendNewLink.Visible = false;
            lcalMsg.Text =
                GetGlobalResourceObject("~/account/new-user-confirmation", "lcalInvalidLnk.Msg")
                    .ToString().Replace("[[CONTACTUSLINK]]", contactUrl);
        }
    }
}