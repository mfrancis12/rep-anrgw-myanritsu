﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="new-user-registration-thankyou.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.new_user_registration_thankyou" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <div class="boxitem">
        <div class="boxitemtitle dialogue-header">
            <div class="boxitemtitleleft"></div>
            <div class="boxitemtitleright"></div>
            <div class="boxitemtitletext">
                <asp:Localize runat="server" ID="lcalthankyouHeader" Text="<%$Resources:~/account/new-user-registration-thankyou,lcalthankyouHeader.Text%>"></asp:Localize>
            </div>
        </div>
        <div class="boxitembody dialogue-body">
            <asp:Localize runat="server" ID="lcalThanksMsg" Text="<%$Resources:~/account/new-user-registration-thankyou,lcalThanksMsg.Text%>"></asp:Localize>
            &nbsp;&nbsp;  
        </div>
        <%--<div class="boxitemfooter">
            <div>
                <img src="/images/wdwftrl.gif" alt="" />
            </div>
            <div class="movetoright">
                <img src="/images/wdwftrr.gif" alt="" />
            </div>
        </div>--%>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
