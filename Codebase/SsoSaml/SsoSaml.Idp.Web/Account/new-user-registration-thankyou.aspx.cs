﻿using System;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class new_user_registration_thankyou : IdpBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            var email = Request.QueryString[KeyDef.ParmKeys.CommonParams.EmailId];
            var reSend = Request.QueryString[KeyDef.ParmKeys.CommonParams.Resend];
            var contactUrl = "../contact-us" + Request.Url.Query;
            if (string.IsNullOrEmpty(email)) return;
            if (!string.IsNullOrEmpty(reSend))
            {
                var ds = BLL.SingleSignOn.GwIdpUserBLL.GetInActiveUserData(email);
                if (ds.Tables.Count != 0)
                {
                    var statuscode = SendActivationLinkEmail(ds.Tables[1].Rows[0]["EmailAddress"].ToString()
                        , ds.Tables[1].Rows[0]["FirstName"].ToString(), ds.Tables[1].Rows[0]["LastName"].ToString()
                        , Guid.Parse(ds.Tables[0].Rows[0]["Token"].ToString()));
                    if (statuscode == 0)
                    {
                        DisplayThanksMsg(email, contactUrl);
                    }
                    else
                    {
                        Response.Redirect("../error.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../error.aspx");
                }
            }
            else
            {
                DisplayThanksMsg(email, contactUrl);
            }
        }

        protected void DisplayThanksMsg(string email, string contactUrl)
        {
            lcalThanksMsg.Text =
                GetGlobalResourceObject("~/account/new-user-registration-thankyou", "lcalThanksMsg.Text")
                    .ToString()
                    .Replace("[[USEREMAIL]]", email)
                    .Replace("[[CONTACTUSLINK]]", contactUrl);
        }
    }
}