﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="new-user-registration.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.new_user_registration" %>

<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        
        $(function () {
            
            function ShowTooltip(event, imageid, divid) {
                $("#" + divid).css("display", 'none');

                //get the Tooltip div object.
                var transparent = document.getElementById(divid);
                var obj = document.getElementById(imageid);
                ImgPos = FindPosition(obj);

                $("#" + divid).css("left", ImgPos[0] - 370 + "px");
                if (divid == "popupimgLgnNm") {
                    $("#" + divid).css("top", ImgPos[1] - 100);
                }
                else {
                    $("#" + divid).css("top", ImgPos[1] - 220);
                }

                $("#" + divid).slideDown("slow");
            }
            function HideTooltip(dividh) {
                $("#" + dividh).slideUp("slow");
            }

            function FindPosition(oElement) {
                if (typeof (oElement.offsetParent) != "undefined") {
                    for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
                        var position = document.defaultView.getComputedStyle(oElement, null).getPropertyValue("position");
                        if (position == 'relative')
                            break;
                        posX += oElement.offsetLeft;
                        posY += oElement.offsetTop;
                    }
                    return [posX, posY];
                }
                else {
                    return [oElement.x, oElement.y];
                }
            }
            function ValidateDisclaimer(source, args) {
                var cbxDisclaimer = document.getElementById('<%= cbxDisclaimer.ClientID %>');
                if (cbxDisclaimer.checked) { args.IsValid = true;
                }
                else { args.IsValid = false;
                }
                return false;
            }

            function ValidationCheck() {
                if (Page_ClientValidate("1")) {
                    if (Page_ClientValidate("2")) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            $('.modal-popup').magnificPopup({
                modal: true
            });
            $(document).on('click', '.popup-modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        });
    </script>
     <style type="text/css">
        .captcha .captchaErr{display:normal!important;width:100%!important;padding-top:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">


    <%--<div id="popupimgpwd" class="transparent">
        <div class="pwdguide" style="text-align: left;">
            <asp:Localize runat="server" ID="lcalPwdGuide" Text="<%$Resources:~/account/new-user-registration,lcalPwdGuide.Text%> "></asp:Localize>
        </div>
    </div>

    <div id="popupimgLgnNm" class="infoText">
        <div class="pwdguide" style="text-align: left;">
            <asp:Localize runat="server" ID="Localize1" Text="<%$Resources:~/account/new-user-registration,lblCreateLoginMsg.Text%> "></asp:Localize>
            <asp:Localize runat="server" ID="Localize2" Text="<%$Resources:~/account/new-user-registration,lblLoginErrMsg.Text%> "></asp:Localize>
        </div>
    </div>--%>

    <asp:ScriptManager ID="myScriptmanager" runat="server" EnablePartialRendering="False">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ContactWebMasterPanel" runat="server" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <div class="pagetitle">
                                <div class="pagetitleleft">
                                    <h1>
                                       <asp:Localize runat="server" ID="pagetitle" Text=""></asp:Localize>
                                    </h1>
                                </div>
                            </div>
            <div id="step1"   class ="boxitem" runat="server">
                <!-- ListItem -->
                <div class="boxitemtitle">
                    <div class="boxitemtitleleft">
                    </div>
                    <div class="boxitemtitleright">
                    </div>

                    
                    <div class="boxitemtitletext">
                        
                    </div>
                </div>
                <div class="boxitembody content-detail-70">
                    <%--<div>
                        <asp:Localize runat="server" ID="lclInformation" Text="<%$Resources:~/account/new-user-registration,lclInformation.Text%> "></asp:Localize>

                    </div>--%>
                    <div class="content-form">
                        <div class="content-detail">
                            
                            <div class="margin-top-15">
                                <asp:Label ID="lblMessage" runat="server" CssClass="server-error-message"></asp:Label>
                            </div>
                            <h3 class="section-sub-header">
                                <asp:Localize runat="server" ID="Localize1" Text="<%$Resources:~/account/new-user-registration,lcalEditInfo.Text%>"></asp:Localize>
                            </h3>
                            <div class="form-container">

                              
                                <input type="text" style="display: none" />
                                <input type="password" style="display: none" />
                                <asp:PlaceHolder ID="phRegfrmMyInfo" runat="server"></asp:PlaceHolder>
                                <div id="mainSection" runat="server" class="section">
                                    <div class="fields">

                                        <div id="loginName_div" runat="server" class="group input-text">

                                            <%-- <div class="settingrow company-profile-radio-container" style="padding: 5px 0">
                                                <p class="radio-container">
                                                    <asp:RadioButton ID="rdoIndividualAnritsuID" SkinID="RadioBlock" runat="server" Checked="true" GroupName="EmailTypeGroup"
                                                        Text="<%$Resources:~/account/new-user-registration,rdoIndividualAnritsuID.Text%>" />
                                                </p>
                                                <p class="radio-container">
                                                    <asp:RadioButton ID="rdoGroupAnritsuID" SkinID="RadioBlock" runat="server" GroupName="EmailTypeGroup"
                                                        Text="<%$Resources:~/account/new-user-registration,rdoGroupAnritsuID.Text%>" />
                                                    <p>
                                            </div>--%>
                                        </div>


                                    </div>

                                    <div id="guidelines_login" class="guidelines" runat="server">
                                    </div>
                                </div>
                                <h3 class="section-sub-header">
                                    <asp:Localize runat="server" ID="lclBoxTitle" Text="<%$Resources:~/account/new-user-registration,lcalEditCompInfo.Text%>"></asp:Localize></h3>
                                <asp:PlaceHolder ID="phRegfrm" runat="server"></asp:PlaceHolder>
                                
                              
                                <br />
                                  <div id="submitsection" >
                                <%--<h3 class="section-sub-header">
                                    <asp:Localize runat="server" ID="lCalTou" Text="<%$Resources:~/account/new-user-registration, lcaltermsofuse%> "></asp:Localize>
                                </h3>--%>
                                <div id='container' class="group">
                                    <div class="margin-top-15">
                                        <asp:Label ID="lblTocValidator" runat="server" CssClass="server-error-message" Text="<%$Resources:~/account/new-user-registration, cvDisclaimer.ErrMsg%>" Visible="false"></asp:Label>
                                    </div>
                                    <asp:CheckBox runat="server" ID="cbxDisclaimer" AutoPostBack="false" />
                                    &nbsp<asp:Localize runat="server" ID="lcalDisclaimer" Text="<%$Resources:~/account/new-user-registration, lcalDisclaimer.Text%> "></asp:Localize>
                                </div>
                                <div class="group margin-top-15">
                                    <asp:Localize runat="server" ID="lcalNotifymail" Text="<%$Resources:~/account/new-user-registration, lcalNotifymail.Text%> "></asp:Localize>
                                </div>

                                <div class="group margin-top-15">
                                    <asp:CheckBox ID="ReceiveMails" class="rdBtnReceivemail" Checked="true" runat="server" />
                                    &nbsp<asp:Localize runat="server" ID="lcalRcvMail" Text=" <%$Resources:~/account/new-user-registration, rdBtnReceivemail.Text %>"></asp:Localize>
                                </div>

                                <sbk:CaptchaControl ID="captchactrl" LayoutStyle="CssBased" ValidationGroup="1" runat="server" CssClass="captcha captchaErr"  EnableClientScript="false"
                                    ErrorMessage="<%$Resources:Common, lcalCaptchaInvalidCode.ErrorMessage %>" Display="Dynamic" CaptchaLength="4" ForeColor="#f5a623" />

                                <div class="margin-top-15 server-error-message">
                                    <asp:CustomValidator ID="cv_cbxDisclaimer" runat="server" ErrorMessage="<%$Resources:~/account/new-user-registration, cvDisclaimer.ErrMsg%> "
                                        ClientValidationFunction="ValidateDisclaimer" CssClass="terms_ErrorMsg server-error-message" ValidationGroup="2"></asp:CustomValidator>
                                </div>

                                <div class="divcenter">
                                    <asp:Button ID="Save" runat="server" Text="<%$Resources:~/account/new-user-registration,btnNext.Text %>"
                                        UseSubmitBehavior="true" SkinID="btngreenbg" OnClientClick="return ValidationCheck();" OnClick="Save_Click" ValidationGroup="1" CssClass="form-submit button" />
                                    <asp:Button ID="Cancel" runat="server" Text="<%$Resources:Common, Cancel%>" SkinID="btngreenbg" OnClick="Cancel_Click" CssClass="button" />
                                </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="modal-desc" class="popup white-popup-block mfp-hide">
                    <%--<input type="button" value="X" class="x popup-modal-dismiss" style="cursor: pointer" />--%>
                    <div class="content">
                        <div class="boxitemtitle">
                            <div class="boxitemtitleleft">
                            </div>
                            <div class="boxitemtitleright">
                            </div>
                            <div class="boxitemtitletext">
                                <asp:Localize runat="server" ID="Localize17" Text="<%$Resources:~/account/new-user-registration,lcalDisclaimerTitle.Text%>"></asp:Localize>
                            </div>
                        </div>
                        <div class="boxitembody">
                            <div class="boxitemlisttext" style="height: 375px; overflow: auto">
                                <asp:Localize runat="server" ID="lclTermsOfUseContent" Text="<%$Resources:~/account/new-user-registration,lcalTermsOfUseContent.Text%> "></asp:Localize>
                            </div>
                        </div>
                        <%--<div class="boxitemfooter">
                <div>
                    <img src="/images/wdwftrl.gif" alt="" />
                </div>
                <div class="movetoright">
                    <img src="/images/wdwftrr.gif" alt="" />
                </div>
            </div>--%>
                        <div style="width: 100%">
                            <%--<img height="5px" src="/images/whiteSpacer.gif" alt="" />--%>
                        </div>
                    </div>
                    <a class="popup-modal-dismiss anchor-blue" href="#">Dismiss</a>
                </div>
                <%--<div class="boxitemfooter">
                    <div>
                        <img src="/images/wdwftrl.gif" alt="" />
                    </div>
                    <div class="movetoright">
                        <img src="/images/wdwftrr.gif" alt="" />
                    </div>
                </div>--%>
            </div>

       

           
                                     <asp:Panel runat="server" style="width:50%" Visible="false" ID="step2">
                         <div class="form-container">
                     <%--<div class="heading">
                          <h2><asp:Localize ID="localverificationheader" runat="server"   Text='<%$Resources:~/account/verification,lcalverificationheader.Label %>'></asp:Localize></h2>
                     </div>--%>
                             <div>
                                 <h4><asp:Localize ID="Localize2" runat="server"   Text='<%$Resources:~/account/verification,lcalDisclaimer.Text %>'></asp:Localize></h4>
                             </div>
                              <h3>  <span id="spanmessgae" runat="server" style="color:#2F977A">
                        <asp:Localize ID="lcalErrorMsg"  runat="server" visible="false"></asp:Localize>     
                         </span></h3>
                  
                             <div id="divverification" runat="server">
                     <div class="bd">
                       
                          <div class="group input-text">
                             <asp:TextBox ID="txtVerification" placeholder='<%$Resources:~/account/verification,lcalverificationvalidationtextboxplaceholder.Text %>'   MaxLength="6"  runat="server"></asp:TextBox>
                         </div>
                         <div class='settingrow' style='padding-top: 10px;'>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="LoginUserValidationGroup"/>
                        
                    </div>
                         <div class="group">
                          <div class='btnrow'>
                             <div class='btncollft'>
                                  <div class="settingrow" style="padding-top: 10px!important;">
                                 <asp:Button ID="btnVerify" runat="server" 
                                     Text='<%$Resources:~/account/verification,lcalverificationverifybutton.Text %>'
                                       CssClass="button form-submit margin-right-15" OnClick="btnVerify_Click" />
                                   
                             </div>
                                
                             </div>
                     </div>
                    <div class='clearfloat'></div>
                </div>
                          <asp:LinkButton ID="btnresend" runat="server" Text='<%$Resources:~/account/verification,lcalverificationresend.Text %>'   CssClass="inline-block margin-top-15" OnClick="btnresend_Click"> </asp:LinkButton>&nbsp; &nbsp;
                          <span  style="color:#289b7d;font-size:14px;" id="resendspanmessage" runat="server"><asp:Literal ID="lcalResendMessage" runat="server"></asp:Literal></span>
                         
                         <br/>    
                         <asp:HyperLink ID="lnkhelp" runat="server" Text='<%$Resources:~/signin,hlHelp.Text %>' Target="_blank" NavigateUrl="https://login.anritsu.com/contact-us" CssClass="inline-block "></asp:HyperLink>
                        
                 </div>
                                 </div>
                <div id="divLoginCtrl">
                
                </div>
             </div>    
                 </asp:Panel> 
                                  

            <div id="step3"  visible="false"  runat="server">
             <asp:PlaceHolder ID="passwordfrm" runat="server"></asp:PlaceHolder>
            <div class="section" id="passwordsection" style="width:70%"  runat="server">
                   
                                        <div class="fields">
                                            <p>
                                                <asp:Localize runat="server" ID="lclPwdInf" Text="<%$Resources:~/account/new-user-registration,Createpwdinfo.Text%>"></asp:Localize></p>
                                            <div id="fieldsData" runat="server" class="group input-password required"></div>
                                            <div id="fieldsData1" runat="server" class="group input-password required"></span></div>

                                        </div>

                                        <div id="guidData" runat="server" class="guidelines">
                                        </div>
               
                                    </div>
                   <div class="group">
                          <div class='btnrow'>
                             <div class='btncollft'>
                                  <div class="settingrow" style="padding-top: 10px!important;">
                                 <asp:Button ID="Button1" runat="server" 
                                     Text='<%$Resources:~/account/new-user-registration,passwordbutton.Text %>'
                                       CssClass="button form-submit margin-right-15" OnClick="btnCreateAccount_Click" />
                                      
                                     <%-- <asp:HyperLink ID="HyperLink1" runat="server" Text='<%$Resources:~/signin,hlForgotPwd.Text %>' Target="_blank" NavigateUrl="https://www.anritsu.com/My-Anritsu/Forgot-Password.aspx"></asp:HyperLink>--%>
                             </div>
                                
                             </div>
                     </div>
                    <div class='clearfloat'></div>
                </div>
           </div>
                                
        </ContentTemplate>
            
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Save" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="ContactWebMasterPanel">
        <ProgressTemplate>
            <div class="updateProgress">
                <img src="/images/indicator.gif" alt="" /><asp:Literal ID="ltrProcess" runat="server"
                    Text="<%$Resources:Common,Processing%>"></asp:Literal>
            </div>
            <div class="updateProgressbackground">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type="text/javascript">
        pwdRules = <%=pwdRules%>;
        $(window).load(function() {
            pwdId = <%=string.Format("\"#{0}\"", Password.ClientID.ToString())%>;
        });
        //alert(pwdId);
        //console.log(pwdId);
        function ClearDisplayMsg() {
            return validateOnSubmit();
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
