﻿/*
* Author   :   Kishore Kumar M
* Date of Creation :   19-Apr-2013
* Purpose  : New user Registration-SSO
* Date of modification:
* -----------------------------
* Date     :   Member          |
* -----------------------------
* -----------------------------
*  *  */

using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using AjaxControlToolkit;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Web.App_Controls;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using SDK = Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.CoreLib;
using System.Configuration;
using System.Text;
using System.Threading;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class new_user_registration : IdpBasePage
    {
        #region Private Properties/Fields
        public String pwdRules;
        private const string Classkey = "~/account/new-user-registration";
        private DataTable _countriesList;
        private int _cultureGroupId;
        private Guid _registerUsertoken;
        System.Collections.Specialized.NameValueCollection nameValues = null;
        public Guid RegisterUserToken
        {
            get
            {
                var tkn = WebUtility.ReadRequest(KeyDef.ParmKeys.CommonParams.RTK, true);

                var tknGuid = Guid.Empty;
                if (string.IsNullOrEmpty(tkn) == false)
                {
                    tknGuid= new Guid(tkn);
                }
                
                return tknGuid;
            }
        }
        private readonly string _contactUrl = "../contact-us" + HttpContext.Current.Request.Url.Query;

        private TextBox FirstName { get; set; }
        private TextBox LastName { get; set; }
        private TextBox Company { get; set; }
        private TextBox JobTitle { get; set; }
        private TextBox Phone { get; set; }
        private TextBox FaxNumber { get; set; }
        private TextBox Address1 { get; set; }
        private TextBox Address2 { get; set; }
        private TextBox City { get; set; }
        private StateControl State { get; set; }
        private DropDownList CountryName { get; set; }
        private TextBox PostalCode { get; set; }
        private TextBox Email { get; set; }
        private TextBox ConfirmEmail { get; set; }
        public TextBox Password { get; set; }
        private TextBox ConfirmPassword { get; set; }
        private TextBox LoginName { get; set; }
        private Label lblMark { get; set; }
        private RegularExpressionValidator PostalCodeRegExpValidator { get; set; }

        private TextBox FirstNameInRoman { get; set; }
        private TextBox LastNameInRoman { get; set; }
        private TextBox CompanyInRoman { get; set; }

        private RadioButton rdoIndividualAnritsuID { get; set; }
        private RadioButton rdoGroupAnritsuID { get; set; }
        #endregion

        #region Page Events

        private void Page_Init(object sender, EventArgs e)
        {
            //Initilize Registration form controls dynamically
            InitRegistrationFormControls();
            Form.Attributes["autocomplete"] = "off";

          
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                
                    pwdRules = GetPasswordGuideLines();
                    LoadControlsRecursively(phRegfrm);
                    LoadControlsRecursively(phRegfrmMyInfo);
                    LoadControlsRecursively(passwordfrm);
                   
                    _cultureGroupId = GetCurrentCultureGroupId();
                    State.ValidationGroup = "1";
                nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                if (IsPostBack)
                {
                    if (CountryName.SelectedValue.Equals("US"))
                    {
                        // PostalCodeRegExpValidator.Enabled = State._IsUnitedStates = lblMark.Visible = true;
                        State._IsJapan = false;
                    }
                    else if (CountryName.SelectedValue.Equals("JP"))
                    {
                        // PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                        State._IsJapan = true;
                        State._IsUnitedStates = false;
                    }
                    else
                    {
                        //  PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                        State._IsJapan = State._IsUnitedStates = false;
                    }
                
                }
                else
                {
                    
                    pagetitle.Text = GetGlobalResourceObject("~/account/new-user-registration", "CreateAnritsuID.Title").ToString(); ;
                    var li = new ListItem(GetGlobalResourceObject("Common", "Select").ToString(), "Select");

                    _countriesList = ISOCountryBLL.Country_SelectByCultureGroupId(_cultureGroupId);
                    CountryName.DataSource = _countriesList.DefaultView;
                    CountryName.DataTextField = "CountryName";
                    CountryName.DataValueField = "Alpha2";
                    CountryName.DataBind();
                    CountryName.Items.Insert(0, li);

                    SetCountryBasedOnCultureGroupId();
                    CountryName_SelectedIndexChanged(this, e);
                    if (RegisterUserToken != Guid.Empty)
                    {
                        VerificationProcess(RegisterUserToken);
                    }
                }
              
                var mp = Master as App_MasterPages.MasterPageSiteDefault;
                mp.GreenPageTitleCtrl.Visible = false;
                var contactUrl = "/contact-us" + Request.Url.Query;
                if (Thread.CurrentThread.CurrentCulture.Name.Equals("ja-JP"))
                {
                    lnkhelp.NavigateUrl = GetResourceString("~/signin", "faqlink");
                }
                else
                {
                    lnkhelp.NavigateUrl = contactUrl;
                }
                



            }
            catch (Exception ex)
            {

            }
        }

        #endregion
        HtmlGenericControl statediv;//=new HtmlGenericControl("div");
        #region PostBack Events

        protected void Save_Click(object sender, EventArgs e)
        {
            // Collect the user information and strore in Gw User object.
            //System.Threading.Thread.Sleep(3000);
            var txtFname = (TextBox)FindControlRecursive(phRegfrm, "FirstName");
            RegisterNewUser();
            
        }

        protected void CountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCountry = CountryName.SelectedValue;
            if (selectedCountry.Equals("US"))
            {
                State.refreshControl(this, e, selectedCountry);
                statediv.Attributes.Add("class", "group input-select country-select");
            }
            else if (selectedCountry.Equals("JP"))
            {
                State.refreshControl(this, e, selectedCountry);
                statediv.Attributes.Add("class", "group input-select country-select");
            }
            else
            {
                statediv.Attributes.Add("class", "group input-text country-select");
            }
            // PostalCodeRegExpValidator.ValidationGroup = "1";

            switch (selectedCountry)
            {

                case "US":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{5}(-\d{4})?");
                        // PostalCodeRegExpValidator.ValidationExpression = @"\d{5}(-\d{4})?";
                        break;
                    }
                case "JP":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{3}(-(\d{4}|\d{2}))?");
                        //PostalCodeRegExpValidator.ValidationExpression = @"\d{3}(-(\d{4}|\d{2}))?";
                        break;
                    }
                case "DE":
                    {
                        PostalCode.Attributes.Add("data-regex", @"(D-)?\d{5}");
                        //PostalCodeRegExpValidator.ValidationExpression = @"(D-)?\d{5}";
                        break;
                    }
                case "FR":
                    {
                        PostalCode.Attributes.Add("data-regex", @"\d{5}");
                        // PostalCodeRegExpValidator.ValidationExpression = @"\d{5}";
                        break;
                    }
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ddlchange", "$('#" + CountryName.ClientID + "').select();$(\"select[id $= 'cmbState']\").select();", true);

        }

        #endregion

        #region Local Methods

        private void VerificationProcess(Guid token)
        {
            CreateHistoryForBrowsers("verficationprocess");
            var ds = BLL.SingleSignOn.GwIdpUserBLL.CheckUserRegistrationStatus(token);
                if (ds.Tables.Count > 0)
                {
                    var status = ds.Tables[0].Rows[0]["STATUS"].ToString();
                    var email = ds.Tables[0].Rows[0]["EMAIL"].ToString();
                    Session["userid"] = int.Parse(ds.Tables[0].Rows[0]["USERID"].ToString());
                    Email.Text = email;
                    
                    if (status == "VERIFICATION" && string.IsNullOrEmpty(email) == false)
                    {

                        pagetitle.Text = GetGlobalResourceObject("~/account/new-user-registration", "EmailVerification.Title").ToString(); ;
                        step2.Visible = true;
                        step1.Visible = false;
                        string maskedemail = Lib.SingleSignOn.GwIdpUserBLL.MaskEmail(email);
                        lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationinitial.Msg").ToString().Replace("[[USEREMAILADDRESS]]", maskedemail.ToString());
                        lcalErrorMsg.Visible = true;
                        CreateHistoryForBrowsers("VERIFICATION",true);
                    }
                    else if (status == "PASSWORD" && string.IsNullOrEmpty(email) == false)
                    {
                        pagetitle.Text = GetGlobalResourceObject("~/account/new-user-registration", "SetPassword.Title").ToString(); ;
                        step2.Visible = false;
                        step1.Visible = false;
                        step3.Visible = true;
                        CreateHistoryForBrowsers("PASSWORD");
                    }
                    else if (status == "APPROVED" && string.IsNullOrEmpty(email) == false)
                    {
                        Response.Redirect("~/signin", false);
                    }
                    else
                    {
                        Response.Redirect("~/account/new-user-registration", false);
                    }
                }
           

        }
        private void InitRegistrationFormControls()
        {
            var doc = XElement.Load(Server.MapPath("~/App_Data/RegFiledOrder.xml"));

            var browserLang = GetCurrentCulture();
            var fields = (from item in doc.Descendants("Region")
                          where item.Attribute("Name").Value == (browserLang ?? "default")
                          select item).ToList();
            if (fields.Count == 0)
            {
                fields = (from item in doc.Descendants("Region")
                          where item.Attribute("Name").Value == "default"
                          select item).ToList();
            }
            var sortedFields = (from i in fields.Descendants("field")
                                orderby int.Parse(i.Attribute("fieldorder").Value)
                                select i).ToList();
            var table = new Table();
            var it = 0;
            var fieldsCompanyDiv = new HtmlGenericControl("div");
            fieldsCompanyDiv.Attributes.Add("class", "bd");
            fieldsCompanyDiv.ID = "CompanyDiv";

            var fieldsDivMyinfo = new HtmlGenericControl("div");
            fieldsDivMyinfo.Attributes.Add("class", "bd");
            fieldsDivMyinfo.ID = "MyInfoDiv";
            TableRow tr = null;
            TextBox txtFocus = null;
            string txtboxName = null;
            foreach (var field in sortedFields)
            {
                switch (field.Attribute("uniquename").Value)
                {
                    case RegFromFieldIds.FirstName:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FirstName, GetGlobalResourceObject(Classkey, "ltrlFirstName").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvFirstName.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.LastName:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.LastName, GetGlobalResourceObject(Classkey, "ltrlLastName").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvLastName.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.FirstName_In_Roman:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FirstName_In_Roman, GetGlobalResourceObject(Classkey, "ltrlFirstNameinRoman").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvFirstNameinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.LastName_In_Roman:
                        fieldsDivMyinfo.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.LastName_In_Roman, GetGlobalResourceObject(Classkey, "ltrlLastNameinRoman").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvLastNameinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Company:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Company, GetGlobalResourceObject(Classkey, "ltrlCompany.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvCompany.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Company_In_Roman:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Company_In_Roman, GetGlobalResourceObject(Classkey, "ltrlCompanyinRoman.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvCompanyinRoman.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.JobTitle:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.JobTitle, GetGlobalResourceObject(Classkey, "ltrlJobtitle.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvJobTitle.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Phone:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Phone, GetGlobalResourceObject(Classkey, "ltrlPhone.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvPhone.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.FaxNumber:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.FaxNumber,
                            GetGlobalResourceObject(Classkey, "ltrlFax.Text").ToString(), false, "group input-text",
                            "Text", 100, null, false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Address1:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Address1, GetGlobalResourceObject(Classkey, "ltrlAddress1.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvAddress.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.Address2:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.Address2, GetGlobalResourceObject(Classkey, "ltrlAddress2.Text").ToString(), false, "group input-text", "Text", 100, null, false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.City:
                        fieldsCompanyDiv.Controls.Add(CreateTextBoxDiv(RegFromFieldIds.City, GetGlobalResourceObject(Classkey, "ltrlCity.Text").ToString(), true, "group input-text required", "Text", 50, GetGlobalResourceObject(Classkey, "rfvCity.ErrorMessage").ToString(), false, string.Empty, string.Empty));
                        break;
                    case RegFromFieldIds.State:
                        statediv = new HtmlGenericControl("div");
                        statediv.Attributes.Add("class", "group input-select country-select required");
                        statediv.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlState.Text").ToString()));
                        statediv.Controls.Add(CreateLabel("lblMark", false, "*"));
                        statediv.Controls.Add(CreateStateControl(RegFromFieldIds.State, "1"));
                        fieldsCompanyDiv.Controls.Add(statediv);
                        break;
                    case RegFromFieldIds.PostalCode:
                        var divPc = CreateTextBoxDiv(RegFromFieldIds.PostalCode, GetGlobalResourceObject(Classkey, "ltrlPostalCode.Text").ToString(), true, "group input-text required", "Text", 10, GetGlobalResourceObject(Classkey, "rfvPostalCode.ErrorMessage").ToString(), true, "@\\d{5}(-\\d{4})?", GetGlobalResourceObject(Classkey, "revPostalCode.ErrorMessage").ToString(), "zipcode");
                        fieldsCompanyDiv.Controls.Add(divPc);
                        break;
                    case RegFromFieldIds.CountryName:
                        var divCn = new HtmlGenericControl("div");
                        divCn.Attributes.Add("class", "group input-select country-select required");
                        divCn.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlCountry.Text").ToString()));
                        CreateRequiredTag(GetGlobalResourceObject(Classkey, "rfvCountry.ErrorMessage").ToString(), divCn, "error-message");
                        divCn.Controls.Add(CreateDropDownList(RegFromFieldIds.CountryName, "ddl-212", true,
                            new EventHandler(CountryName_SelectedIndexChanged)));
                        fieldsCompanyDiv.Controls.Add(divCn);
                        break;
                    case RegFromFieldIds.Email:
                        var divEm = CreateTextBoxDiv(RegFromFieldIds.Email, GetGlobalResourceObject(Classkey, "ltrlEmail.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvEmailId.ErrorMessage").ToString(), true, "\\s*\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\s*", GetGlobalResourceObject(Classkey, "revEmailId.ErrorMessage").ToString(), "email email-new");
                        fieldsDivMyinfo.Controls.Add(divEm);
                        break;
                    case RegFromFieldIds.ConfirmEmail:
                        var divConEm = CreateTextBoxDiv(RegFromFieldIds.ConfirmEmail, GetGlobalResourceObject(Classkey, "ltrlConfirmEmail.Text").ToString(), true, "group input-text required", "Text", 100, GetGlobalResourceObject(Classkey, "rfvEmailId.ErrorMessage").ToString(), false, string.Empty, string.Empty, "email-confirm");
                        fieldsDivMyinfo.Controls.Add(divConEm);
                        break;
                }

            }


            phRegfrm.Controls.Add(fieldsCompanyDiv);
            //var h3 = new HtmlGenericControl("h");
            //h3.Attributes.Add("class", "section-sub-header");
            //h3.InnerText = "Password Information";

            //phH3.Controls.Add(h3);

           
            //txtBoxConfirmPass.AutoCompleteType = AutoCompleteType.Disabled;    


            loginName_div.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlLoginname.Text").ToString()));
            CreateRequiredTag(GetGlobalResourceObject(Classkey, "revLoginName.ErrorMessage").ToString(), loginName_div, "custom-error-message");
            var logintxtbox = CreateTextBox(RegFromFieldIds.LoginName, 8);
            logintxtbox.Attributes.Add("data-regex", "^[a-zA-Z0-9][a-zA-Z0-9-_.]{4,6}[a-zA-Z0-9]$");
            loginName_div.Controls.Add(logintxtbox);
            var divradio = new HtmlGenericControl("div");
            divradio.Attributes.Add("class", "settingrow company-profile-radio-container");
            divradio.Attributes.Add("style", "padding: 5px 0");
            divradio.Controls.Add(CreateRadioButton("rdoIndividualAnritsuID", "RadioBlock", true, "EmailTypeGroup", GetGlobalResourceObject(Classkey, "rdoIndividualAnritsuID.Text").ToString()));
            divradio.Controls.Add(CreateRadioButton("rdoGroupAnritsuID", "RadioBlock", false, "EmailTypeGroup", GetGlobalResourceObject(Classkey, "rdoGroupAnritsuID.Text").ToString()));

            fieldsDivMyinfo.Controls.Add(loginName_div);
            fieldsDivMyinfo.Controls.Add(divradio);
            phRegfrmMyInfo.Controls.Add(fieldsDivMyinfo);

            guidelines_login.Controls.Add(
                CreateLiteral(GetGlobalResourceObject(Classkey, "lblLoginErrMsg.Text").ToString()));


           addPasswordSection();
            if (txtboxName != null)
            {
                txtFocus = (TextBox)phRegfrm.FindControl(txtboxName);
            }
            if (txtFocus != null)
            {
                SetFocus(txtFocus);
            }
        }

        private void addPasswordSection()
        {
            fieldsData.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlPassword.Text").ToString()));
            CreateRequiredTag(GetGlobalResourceObject(Classkey, "rfvPassword.ErrorMessage").ToString(), fieldsData, "error-message");
            CreateRequiredTag(GetGlobalResourceObject("~/account/updatepwd", "revNewPwd.ErrorMessage").ToString(), fieldsData, "custom-error-message");
            var txtBoxpass = new TextBox { CssClass = "newPassword", MaxLength = 100, ID = RegFromFieldIds.Password };
            txtBoxpass.Attributes.Add("Name", RegFromFieldIds.Password);
            //CreateTextBox(RegFromFieldIds.Password, 100);
            txtBoxpass.Attributes.Add("data-regex", "^.*(?=.{8,})(?=.*[a-zA-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\\$%\\^\\*]{0,}).*$");
            txtBoxpass.TextMode = TextBoxMode.Password;
            //txtBoxpass.AutoCompleteType = AutoCompleteType.Disabled;
            fieldsData.Controls.Add(txtBoxpass);

            guidData.Controls.Add(CreateLiteral(GetGlobalResourceObject(Classkey, "lcalPwdGuide.Text").ToString()));

            fieldsData1.Controls.Add(
                CreateLiteral(GetGlobalResourceObject(Classkey, "ltrlConfirmPassword.Text").ToString()));
            CreateRequiredTag(GetGlobalResourceObject(Classkey, "rfvConfirmPassword.ErrorMessage").ToString(), fieldsData1, "error-message");
            CreateRequiredTag(GetGlobalResourceObject(Classkey, "cvConfirmPassword.ErrorMessage").ToString(), fieldsData1, "custom-error-message");
            var txtBoxConfirmPass = new TextBox { CssClass = "confirmPassword", MaxLength = 100 };
            txtBoxpass.Attributes.Add("Name", RegFromFieldIds.ConfirmPassword);
            txtBoxConfirmPass.TextMode = TextBoxMode.Password;
            fieldsData1.Controls.Add(txtBoxConfirmPass);
            passwordfrm.Controls.Add(passwordsection);
          

        }
        private string GetPasswordGuideLines()
        {
            var browserLang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            var lang = (browserLang == null ? "en-US" : browserLang);
            var dt = ResourceStringsBLL.ResourceStrings_SelectByClass("PasswordGuideLines", lang);
            var sb = new StringBuilder("{");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append(String.Format(@"""{0}"":""{1}""",
                    ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ResourceKey"])
                    , ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ContentStr"])));
            }
            return sb.Append("}").ToString();
        }

        private HtmlGenericControl CreateTextBoxDiv(string id, string label, bool isRequired, string cssClass, string controlType, int maxLen, string errorMsg, bool IsregexReq, string regex, string regexMsg, string textBoxCss = "")
        {
            var div = new HtmlGenericControl("div");
            div.Attributes.Add("class", cssClass);
            switch (controlType)
            {
                case "Text": Label lbl = new Label();
                    lbl.Attributes.Add("for", id);
                    lbl.Text = label;
                    div.Controls.Add(lbl);
                    if (isRequired)
                    {
                        CreateRequiredTag(errorMsg, div, "error-message");
                        if (IsregexReq)
                        {
                            CreateRequiredTag(regexMsg, div, "custom-error-message");
                        }
                    }
                    TextBox tbox = CreateTextBox(id, maxLen);
                    if (IsregexReq)
                    {
                        tbox.Attributes.Add("data-regex", regex);
                    }
                    if (!String.IsNullOrEmpty(textBoxCss))
                        tbox.Attributes.Add("class", textBoxCss);
                    div.Controls.Add(tbox);

                    break;
            }


            return div;
        }

        private static void CreateRequiredTag(string errorMsg, HtmlGenericControl div, string cssClass)
        {
            var pValidation = new HtmlGenericControl("p");
            pValidation.Attributes.Add("class", cssClass);
            pValidation.InnerText = errorMsg;
            div.Controls.Add(pValidation);
        }

        private ValidatorCalloutExtender CreateValidatorCalloutExtender(string id, string targetControlId)
        {
            var vce = new ValidatorCalloutExtender { ID = id, TargetControlID = targetControlId };
            return vce;
        }

        private RequiredFieldValidator CreateRequiredFieldValidator(string id, string controlToValidate,
            ValidatorDisplay display, bool setFocusOnError, string validationGroup, string errorMessage)
        {
            var rfv = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationGroup = validationGroup,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString()
            };
            return rfv;
        }

        private RequiredFieldValidator CreateRequiredFieldValidator(string id, string controlToValidate,
            ValidatorDisplay display, bool setFocusOnError, string validationGroup, string errorMessage,
            string initialValue)
        {
            var rfv = new RequiredFieldValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationGroup = validationGroup,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                InitialValue = initialValue
            };
            return rfv;
        }

        private RegularExpressionValidator CreateRegularExpressionValidator(string id, bool enabled, string errorMessage,
            ValidatorDisplay display, bool setFocusOnError, string validationExpression, string controlToValidate,
            string validationGroup)
        {
            var rev = new RegularExpressionValidator
            {
                ID = id,
                Enabled = enabled,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                Display = display,
                SetFocusOnError = setFocusOnError,
                ValidationExpression = validationExpression,
                ControlToValidate = controlToValidate,
                ValidationGroup = validationGroup
            };
            return rev;
        }

        private CompareValidator CreateCompareValidator(string id, string controlToValidate, string controlToCompare,
            string errorMessage, ValidatorDisplay display, bool setOnFocusError, string validationGroup)
        {
            var cv = new CompareValidator
            {
                ID = id,
                ControlToValidate = controlToValidate,
                ControlToCompare = controlToCompare,
                ErrorMessage = GetGlobalResourceObject(Classkey, errorMessage).ToString(),
                Display = display,
                SetFocusOnError = setOnFocusError,
                ValidationGroup = validationGroup
            };
            return cv;
        }

        private TextBox CreateTextBox(string id, int maxLength, string cssClass = "")
        {
            var txtBox = new TextBox { ID = id, MaxLength = maxLength, CssClass = cssClass };
            return txtBox;
        }

        private HtmlGenericControl CreateRadioButton(string id, string skinid, bool check, string GroupName, string Text)
        {
            var radiop = new HtmlGenericControl("p");
            radiop.Attributes.Add("class", "radio-container");
            var rbtn = new RadioButton { ID = id, SkinID = skinid, Checked = check, GroupName = GroupName, Text = Text };
            radiop.Controls.Add(rbtn);
            return radiop;
        }
        /*
                private Label CreateLabel(string id, Color foreColor, string text, FontUnit fontUnit)
                {
                    Label lbl = new Label();
                    lbl.ID = id;
                    lbl.Text = text;
                    lbl.ForeColor = foreColor;
                    //lbl.Font.Size = fontUnit;
                    return lbl;
                }
        */

        private Label CreateLabel(string id, bool visible, string text)
        {
            var lbl = new Label { ID = id, Text = text, Visible = visible };
            return lbl;
        }

        private StateControl CreateStateControl(string id, string validationGroup)
        {
            var ucState = (StateControl)LoadControl("/App_Controls/UcStateControl.ascx");
            ucState.ValidationGroup = validationGroup;
            ucState.ID = id;
            return ucState;
        }

        private DropDownList CreateDropDownList(string id, string skinId, bool autoPostBack, EventHandler eventHandler)
        {
            var ddl = new DropDownList { ID = id, SkinID = skinId, AutoPostBack = autoPostBack };
            ddl.SelectedIndexChanged += eventHandler;
            return ddl;
        }

        private Literal CreateLiteral(string text)
        {
            var lt = new Literal { Text = text };
            return lt;
        }

        private Control FindControlRecursive(Control control, string id)
        {
            if (control == null) return null;
            //try to find the control at the current level
            var ctrl = control.FindControl(id);

            if (ctrl != null) return ctrl;
            //search the children
            foreach (Control child in control.Controls)
            {
                ctrl = FindControlRecursive(child, id);

                if (ctrl != null)
                {
                    break;
                }
            }
            return ctrl;
        }

        private void LoadControlsRecursively(Control rootControl)
        {
            foreach (Control child in rootControl.Controls)
            {
                switch (child.ID)
                {
                    case RegFromFieldIds.FirstName:
                        if (child is TextBox)
                            FirstName = (TextBox)child;
                        break;
                    case RegFromFieldIds.LastName:
                        if (child is TextBox)
                            LastName = (TextBox)child;
                        break;
                    case RegFromFieldIds.FirstName_In_Roman:
                        if (child is TextBox)
                            FirstNameInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.LastName_In_Roman:
                        if (child is TextBox)
                            LastNameInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.Company:
                        if (child is TextBox)
                            Company = (TextBox)child;
                        break;
                    case RegFromFieldIds.Company_In_Roman:
                        if (child is TextBox)
                            CompanyInRoman = (TextBox)child;
                        break;
                    case RegFromFieldIds.JobTitle:
                        if (child is TextBox)
                            JobTitle = (TextBox)child;
                        break;
                    case RegFromFieldIds.Phone:
                        if (child is TextBox)
                            Phone = (TextBox)child;
                        break;
                    case RegFromFieldIds.FaxNumber:
                        if (child is TextBox)
                            FaxNumber = (TextBox)child;
                        break;
                    case RegFromFieldIds.Address1:
                        if (child is TextBox)
                            Address1 = (TextBox)child;
                        break;
                    case RegFromFieldIds.Address2:
                        if (child is TextBox)
                            Address2 = (TextBox)child;
                        break;
                    case RegFromFieldIds.City:
                        if (child is TextBox)
                            City = (TextBox)child;
                        break;
                    case RegFromFieldIds.State:
                        if (child is StateControl)
                            State = (StateControl)child;
                        break;
                    case RegFromFieldIds.PostalCode:
                        if (child is TextBox)
                            PostalCode = (TextBox)child;
                        break;
                    case RegFromFieldIds.CountryName:
                        if (child is DropDownList)
                            CountryName = (DropDownList)child;
                        break;
                    case RegFromFieldIds.Email:
                        if (child is TextBox)
                            Email = (TextBox)child;
                        break;
                    case RegFromFieldIds.ConfirmEmail:
                        if (child is TextBox)
                            ConfirmEmail = (TextBox)child;
                        break;
                    case RegFromFieldIds.Password:
                        if (child is TextBox)
                            Password = (TextBox)child;
                        break;
                    case RegFromFieldIds.ConfirmPassword:
                        if (child is TextBox)
                            ConfirmPassword = (TextBox)child;
                        break;
                    case RegFromFieldIds.LoginName:
                        if (child is TextBox)
                            LoginName = (TextBox)child;
                        break;
                    case "rdoIndividualAnritsuID":
                        if (child is RadioButton)
                            rdoIndividualAnritsuID = (RadioButton)child;
                        break;
                    case "rdoGroupAnritsuID":
                        if (child is RadioButton)
                            rdoGroupAnritsuID = (RadioButton)child;
                        break;
                    case "PostalCodeRegExpValidator":
                        if (child is RegularExpressionValidator)
                            PostalCodeRegExpValidator = (RegularExpressionValidator)child;
                        break;
                    case "lblMark":
                        if (child is Label)
                            lblMark = (Label)child;
                        break;
                }
                LoadControlsRecursively(child);
            }
        }


        private void SetCountryBasedOnCultureGroupId()
        {
            var countryName = ISOCountryBLL.GetCountryNameForCultureGroupId(GetCurrentCultureGroupId());

            var r = _countriesList.Select(string.Format("CountryName = '{0}'", countryName.Replace("'", "''")));
            if (_cultureGroupId == 3) //EMEA
            {
                CountryName.SelectedValue = "Select";
            }
            else
            {
                CountryName.SelectedValue = r.Length == 0 ? "US" : r[0]["Alpha2"].ToString();
            }
            lblMark.Visible = CountryName.SelectedItem.Value.Equals("US") || State.IsStateRequired;
        }

        public void RegisterNewUser()
        {
            if (!IsValid) return;

            #region Checking Existing UserName & Login
            bool isNewUser = true;
            if ( BLL.SingleSignOn.GwIdpUserBLL.CheckEmailAddressExist(Email.Text.Trim()))
            {
                lblMessage.Text = GetGlobalResourceObject(Classkey, "ErrMsgUserIdAlreadyExists").ToString();
            }
            else if (string.IsNullOrEmpty(LoginName.Text.Trim()) ==false && !BLL.SingleSignOn.GwIdpUserBLL.VerifyLoginNameExists(Email.Text.Trim(),
                 LoginName.Text.Trim()))
            {
                lblMessage.Text = GetGlobalResourceObject(Classkey, "ErrMsgLoginNameAlreadyExists").ToString();
            }
                          // This is to check if there is any non-activated account with same Email AND LoginName
            //if )
            //{
            //    isNewUser = false;
            //    //var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            //    //lblMessage.Text =
            //    //    GetGlobalResourceObject(Classkey, "lcalAccountAvailable.ErrMsg")
            //    //        .ToString()
            //    //        .Replace("[[ACTIVATIONLINK]]",
            //    //            ConfigurationManager.AppSettings["ANRITSUID_RESEND_EMAIL_URL"] + "&" +
            //    //            nameValues.ToString())
            //    //        .Replace("[[EMAILID]]", Email.Text.Trim())
            //    //        .Replace("[[RESEND]]", "True");
            //}
            //else if (isNewUser && BLL.SingleSignOn.GwIdpUserBLL.ISExistingActiveInActiveLoginNameOrEmail(LoginName.Text.Trim()))
            //{
            //    lblMessage.Text = GetGlobalResourceObject(Classkey, "ErrMsgLoginNameAlreadyExists").ToString();
            //}
            // This is to check if there is any account (active or inactive) with same email
           

                // This is to check if there is any account (active or inactive) with same login name
             


            #endregion

            else
            {
                try
                {
                    if (cbxDisclaimer.Checked)
                    {
                        if(BLL.SingleSignOn.GwIdpUserBLL.VerifyIsNewUser(Email.Text.Trim()))
                        {
                            isNewUser = false;
                        }
                        if (CreateUserAccount(isNewUser))
                        {
                            var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                            string qstring = null;
                            string returnUrl = HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.ReplyUrl];
                            string sptoken= HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID];
                            string culture = UIHelper.GetCulture();
                            if (!string.IsNullOrEmpty(returnUrl) && !string.IsNullOrEmpty(sptoken) && !string.IsNullOrEmpty(culture))
                            {
                                qstring = KeyDef.ParmKeys.SsoIDP.SpTokenID.ToString() + "=" + sptoken + "&" + KeyDef.ParmKeys.ReplyUrl.ToString() + "=" + returnUrl + "&" + "lang=" + culture + "&ReturnURL =/signin";
                            }
                            else
                            {
                                qstring = "ReturnURL=/signin";
                            }
                            CreateHistoryForBrowsers("Verification");
                            string activatedEmail = SendActivationCodeEmail(Email.Text, "System", qstring);
                            Session["rtk"] = _registerUsertoken;
                            VerificationProcess(_registerUsertoken);
                            
                        }
                        else
                        {
                            lblMessage.Text =
                                GetGlobalResourceObject(Classkey, "lcalProcessingErr.ErrMsg")
                                    .ToString()
                                    .Replace("[[CONTACTUSLINK]]", _contactUrl);
                        }
                    }
                    else
                    {
                        lblTocValidator.Visible = true;
                        lblMessage.Text = GetGlobalResourceObject("Common", "lcalRequiredField").ToString();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                    Response.Redirect("../error.aspx");
                }
            }
        }

        public bool CreateUserAccount(bool isNewUser=true)
        {
            var accountCreated = false;

            try
            {
                #region New User Registration

                var dt = BLL.SingleSignOn.GwIdpUserBLL.GetSsoUerType();
                var userTypes = (from r in dt.AsEnumerable()
                                 select new SsoUserType
                                 {
                                     UserTypeId = r.Field<int>("UserTypeId"),
                                     UserType = r.Field<string>("UserType"),
                                     IsUserProfile = r.Field<bool>("IsUserProfile"),
                                     Description = r.Field<string>("Description")
                                 }).ToList();

                var userType = userTypes.Find(
                    delegate(SsoUserType user)
                    {
                        return
                            (String.Equals(user.UserType, KeyDef.ApplicationVariables.SSOUserType,
                                StringComparison.CurrentCultureIgnoreCase));
                    });
                var userTypeId = userType.UserTypeId;

                // Setting the initial status is 1 for the new user
                var userstatusrow = BLL.SingleSignOn.GwIdpUserBLL.GetUserStatus(1);
                var userStatusId = int.Parse(userstatusrow[0].ToString());

                var newUser = new GWSsoUserType();
                var newuserStatus = new GWSsoUserStatusType();
                var newuserType = new GWSsoUserTypeType();

              //  Session["UserEmail"] = Email.Text.Trim();
                newUser.EmailAddress = Email.Text.Trim();
                newUser.LoginName = LoginName.Text.Trim();
                newUser.FirstName = FirstName.Text.Trim();
                newUser.LastName = LastName.Text.Trim();

                if (phRegfrm.FindControl(RegFromFieldIds.FirstName_In_Roman) != null)
                    newUser.FirstNameInRoman = FirstNameInRoman.Text.Trim();
                if (phRegfrm.FindControl(RegFromFieldIds.LastName_In_Roman) != null)
                    newUser.LastNameInRoman = LastNameInRoman.Text.Trim();
                if (phRegfrm.FindControl(RegFromFieldIds.Company_In_Roman) != null)
                    newUser.CompanyInRoman = CompanyInRoman.Text.Trim();

                newUser.JobTitle = JobTitle.Text;
                newUser.CompanyName = Company.Text;
                newUser.PhoneNumber = Phone.Text;
                newUser.FaxNumber = FaxNumber.Text;
                newUser.StreetAddress1 = Address1.Text;
                newUser.StreetAddress2 = Address2.Text;
                newUser.City = City.Text;
                newUser.State = State.ToString();
                newUser.PostalCode = PostalCode.Text;
                newUser.CountryCode = CountryName.SelectedItem.Value;
                newUser.EmailAddress = Email.Text;
                newUser.EncryptedTempPassword = Password.Text.Trim();

                var isUsCustomer = !string.IsNullOrEmpty(CountryName.SelectedItem.Value) &&
                                   CountryName.SelectedItem.Value.ToUpper().Contains("US");

                newUser.PrimaryCultureCode = GetCurrentCulture();
                if (isUsCustomer && string.IsNullOrEmpty(newUser.PrimaryCultureCode))
                    newUser.PrimaryCultureCode = "en-us";
                newUser.AcceptsEmail = ReceiveMails.Checked;
                newUser.RegisteredFromURL = Request.Url.ToString();

                newuserStatus.UserStatusId = userStatusId;
                newuserType.UserTypeId = userTypeId;
                newUser.UserStatus = newuserStatus;
                newUser.UserType = newuserType;
                newUser.SecToken = Guid.NewGuid().ToString();
                newUser.SpToken = Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID];
                newUser.ReturnURL = ReplyURL;

                newUser.Browser = Request.Browser != null ? Request.Browser.Type : "";
                newUser.ClientIP = WebUtility.GetUserIP();
                newUser.UserAgent = HttpContext.Current.Request != null ? HttpContext.Current.Request.UserAgent : "";
                Boolean IsIndividualEmailAddress = true;
                if (rdoIndividualAnritsuID.Checked)
                    IsIndividualEmailAddress = true;
                else
                    IsIndividualEmailAddress = false;

                newUser.IsIndividualEmailAddress = IsIndividualEmailAddress;
                newUser.IsNewUser = isNewUser;

                var userdet = BLL.SingleSignOn.GwIdpUserBLL.InsertNotActivatedSSOUser(newUser);
                _registerUsertoken = Guid.Parse(userdet.Tables[1].Rows[0]["Token"].ToString());
                var userId = int.Parse(userdet.Tables[0].Rows[0]["GWTempUserId"].ToString());
                //BLL.SingleSignOn.GwIdpUserBLL.UpdateUserPassword(userId, Password.Text.Trim(), null, true);
                accountCreated = true;


               

                //Don't send existing ActivationLinkEmail , we have changed to verification email MA-1447
                //var statuscode = SendActivationLinkEmail(Email.Text, FirstName.Text, LastName.Text,
                //    _registerUsertoken);
                //accountCreated = statuscode == 0;


                #endregion
            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx");
            }

            return accountCreated;
        }

        /// <summary>
        /// Preserves new user information.
        /// </summary>
        public object NewUserAccountInformation
        {
            set { Session[KeyDef.SessionKeys.NewUserAccountInformation] = value; }
            get { return Session[KeyDef.SessionKeys.NewUserAccountInformation]; }
        }

        public string GetCurrentCulture()
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
        }

        /*
                private bool IsValidLoginNameFormat(string stringToValidate)
                {
                    if (string.IsNullOrEmpty(stringToValidate))
                    {
                        return true;
                    }
                    Regex regex = new Regex("^[a-zA-Z0-9][a-zA-Z0-9-_.]{4,6}[a-zA-Z0-9]$");
                    return regex.IsMatch(stringToValidate);
                }
        */

        /*
                private bool IsValidEmailFormat(string stringToValidate)
                {
                    if (string.IsNullOrEmpty(stringToValidate))
                    {
                        return false;
                    }
                    Regex regex = new Regex("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
                    return regex.IsMatch(stringToValidate);
                }
        */

        private void postalCodeRegExHandler()
        {
            bool bTe;
            var strTe = CountryName.SelectedValue;
            switch (strTe)
            {
                //case "JP":
                case "DE":
                case "FR":
                case "US":
                    bTe = true;
                    break;
                default:
                    bTe = false;
                    break;
            }
            //PostalCodeRegExpValidator.Enabled = bTe;
        }

        #endregion

        #region PagePreRender

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (CountryName.SelectedItem.Value.Equals("US"))
            {
                State.setState(true, false, "");
            }
            else if (CountryName.SelectedItem.Value.Equals("JP"))
            {
                State.setState(false, true, "");
            }
            else
            {
                State.setState(false, false, "");
            }
            postalCodeRegExHandler();
        }

        #endregion

        protected void Cancel_Click(object sender, EventArgs e)
        {
            var redirectUrl = ReturnURL;
            if (string.IsNullOrEmpty(redirectUrl))
            {
                redirectUrl = ConfigUtility.AppSettingGetValue("USREF_SSOLOGIN_URL");
            }
            Response.Redirect(redirectUrl);
        }


        #region Verification
        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
                
                // passwordsection.Visible = true;

                if (Session["Verifycount"] == null)
                {
                    Session["Verifycount"] = 0;
                }
                lcalErrorMsg.Visible = false;
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[0-9]{6,6}$");
                if( string.IsNullOrEmpty(txtVerification.Text.Trim())==true || regex.IsMatch(txtVerification.Text.Trim()) ==false)
                {
                    lcalErrorMsg.Visible = true;
                    spanmessgae.Style.Add("color", "red");
                    lcalErrorMsg.Text= GetGlobalResourceObject("~/account/verification", "lcalverificationvalidation.Msg").ToString();
                    return;
                }


               
                if (int.Parse(Session["Verifycount"].ToString()) > 5)
                {
                    lcalErrorMsg.Visible = true;
                    spanmessgae.Style.Add("color", "red");
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationmaximumattempts.Msg").ToString();
                    btnVerify.Enabled = false;
                    CreateHistoryForBrowsers("verifymaxattempt");
                    // System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "goBackJS", "var stateObj = { page:'verifymaxattempt' };history.pushState(stateObj, 'verificationmaxattempt', 'signin'); ", true);
                    return;
                }
                
                if (BLL.SingleSignOn.GwIdpUserBLL.VerifyValidationCode(Email.Text.ToString(), txtVerification.Text.Trim()))
                {
                    //   ValidateToken();
                    divverification.Visible = false;
                    //divcontinue.Visible = true;
                    lcalErrorMsg.Visible = true;
                    spanmessgae.Style.Add("color", "#2F977A");
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationsuccess.Msg").ToString();
                    CreateHistoryForBrowsers("verifysuccess",true);
                    //System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "goBackJS", "var stateObj = { page:'verifysuccess' };history.pushState(stateObj, 'verificationsuccess', 'signin'); ", true);
                    step1.Visible = false;
                    step2.Visible = false;
                    step3.Visible = true;
                    Guid tknGuid = Guid.Empty;
                    if (Session["rtk"] == null)
                    {
                        DataSet ds = BLL.SingleSignOn.GwIdpUserBLL.GetNewUserToken(Email.Text);
                        if (ds.Tables.Count > 0)
                        {
                            var token = ds.Tables[0].Rows[0]["Token"].ToString();
                            tknGuid = new Guid(token);
                            
                        }
                    }
                    else
                    {
                        tknGuid = new Guid(Session["rtk"].ToString());

                    }
                    VerificationProcess(tknGuid);


                }
                else
                {
                    Session["Verifycount"] = int.Parse(Session["Verifycount"].ToString()) + 1;
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationinvalid.Msg").ToString();
                    spanmessgae.Style.Add("color", "red");
                    CreateHistoryForBrowsers("verifyfailed");

                }

            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx");
            }
        }

        protected void btnresend_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Email.Text)== false)
                {
                    var email= SendActivationCodeEmail(Email.Text, Email.Text);
                    lcalResendMessage.Text = GetGlobalResourceObject("~/account/verification", "lcalresendclick.Msg").ToString().Replace("[[USEREMAILADDRESS]]", Email.Text);
                    lcalResendMessage.Visible = true;

                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx");
            }
        }
        private void InsertRegisteredUserData(int gwTempUserId)
        {

            // Inserting Active user details in SSo-User table
            var regUserdet = BLL.SingleSignOn.GwIdpUserBLL.InsertActiveSSOUser(gwTempUserId);

            var defaultToken =
                ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID"), Guid.Empty);
            var spToken = ServiceProviderToken != Guid.Empty ? ServiceProviderToken : defaultToken;


            var plainPwd = GetPlainPwd(gwTempUserId, regUserdet.Tables[1].Rows[0]["Password"].ToString());

            // Inserting encrypted Password of registered user ,password is encrypted in BLL layer
            var userSpLoginUrl =
                BLL.SingleSignOn.GwIdpUserBLL.UpdateUserPassword(
                    int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()), plainPwd, spToken.ToString(), false);

            // Add MyAnritsu Role to User
            var dataroles = BLL.SingleSignOn.GwIdpUserBLL.GetUserRoleByRoleTitle("MyAnritsu");
            BLL.SingleSignOn.GwIdpUserBLL.InsertUserUserRoleReference(
                int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()),
                Guid.Parse(dataroles["UserRoleId"].ToString()));

            // Inserting userinformation in Info table
            BLL.SingleSignOn.GwIdpUserBLL.InsertUserInfo(
                int.Parse(regUserdet.Tables[0].Rows[0]["GWUserId"].ToString()),
                Convert.ToBoolean(regUserdet.Tables[1].Rows[0]["AcceptEmail"]));

            //pnlUserConfirmation.Visible = true;

            var signInUrl = userSpLoginUrl["SpLoginUrl"].ToString();
            var returnUrl = ReplyURL;
            if (!String.IsNullOrEmpty(returnUrl))
            {
                signInUrl +=
                    String.Format(
                        "?ReturnURL={0}&" + KeyDef.ParmKeys.CommonParams.IsNew + "=True&lang=" + UIHelper.GetCulture(),
                        HttpUtility.UrlEncode(returnUrl.Trim()));
            }
            else
            {
                signInUrl +=
                    String.Format("?" + KeyDef.ParmKeys.CommonParams.IsNew + "=True&lang=" + UIHelper.GetCulture(),
                        HttpUtility.UrlEncode(returnUrl.Trim()));
            }

            Response.Redirect(signInUrl, false);
        }

        private string GetPlainPwd(int gwTempUserId, string encryptedPwd)
        {
            if (gwTempUserId < 1 || encryptedPwd.IsNullOrEmptyString()) return String.Empty;
            var enc =
                new AnrCommon.CoreLib.CryptoUtilities.EncryptionSalt();
            enc.SetEncryptionKeyByIntId(gwTempUserId);
            return enc.DecryptData(encryptedPwd);
        }
       
        protected void btnCreateAccount_Click(object sender, EventArgs e)
        {
            try
            {
                Guid tknGuid = Guid.Empty;
                int userid = 0;
                if (Session["rtk"] == null)
                {
                    DataSet ds = BLL.SingleSignOn.GwIdpUserBLL.GetNewUserToken(Email.Text);
                    if (ds.Tables.Count > 0)
                    {
                        var token = ds.Tables[0].Rows[0]["Token"].ToString();
                        Session["userid"] = int.Parse(ds.Tables[0].Rows[0]["GWTempUserId"].ToString());
                        tknGuid = new Guid(token);
                        Session["rtk"] = tknGuid;



                    }
                }
                
                tknGuid = new Guid(Session["rtk"].ToString());
                VerificationProcess(tknGuid);
                userid=int.Parse(Session["userid"].ToString());
                BLL.SingleSignOn.GwIdpUserBLL.UpdateUserPassword(userid, Password.Text.Trim(), null, true);
                InsertRegisteredUserData(userid);
            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                Response.Redirect("../error.aspx");
            }

        }

        private void CreateHistoryForBrowsers(string pagescenrio, bool addquerystring = false)
        {
            string jsobj = string.Empty;
            pagescenrio = pagescenrio + "_" + Guid.NewGuid().ToString();
            if (addquerystring && nameValues != null)
                jsobj = "var stateObj = { page:'" + pagescenrio + "' };history.pushState(stateObj, '" + pagescenrio + "', 'new-user-registration?" + nameValues.ToString() + "'); ";
            else
                jsobj = "var stateObj = { page:'" + pagescenrio + "' };history.pushState(stateObj, '" + pagescenrio + "', 'new-user-registration'); ";
            System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "verificationscript", jsobj, true);
        }
        #endregion
    }
}