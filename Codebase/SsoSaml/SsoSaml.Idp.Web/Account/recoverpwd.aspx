﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="recoverpwd.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.recoverpwd" %>

<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<%@ Register TagPrefix="AjaxControlToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%--<script src='//edge1.anritsu.com/jsutils/custom-tooltip-v1.js' type='text/javascript'></script>
    <script src='//edge1.anritsu.com/jsutils/util-functions-v1.js' type='text/javascript'></script>
    <script type="text/javascript" language="javascript">
            function pageLoad(sender, args){
                if (args.get_isPartialLoad()){
                    var message_text = pwdRules["PasswordGuideLines.Text"];
                    $("#pGuidelinesText").html(message_text);

            }
        }           
        var pwdRules = <%=pwdRules%>
         function ClearDisplayMsg() {
             return validateOnSubmitReset();
             }
        var pwdId = "#<%= txtNewPasswordText.ClientID %>";
        
         $(document).ready(function () {
           
             $("#<%= txtNewPasswordText.ClientID %>").customToolTip({
                 selector: '#<%= txtNewPasswordText.ClientID %>',
                 htmlContent: '<b>' + pwdRules["PasswordStrength.Text"] + ' :</b><span class="strength">' + pwdRules["PasswordWeak.Text"] + '</span>'
                         + '<div class="strength-scale"><span class="weak"></span></div>'
                         + '<div class="pwd-guidelines">'
                         + '<ul>'
                         + '<li class="fail">' + pwdRules["PasswordLength.ErrorMessage"] + '</li>'
                         + '<li class="fail">' + pwdRules["PasswordMustContainOneAlphabet.ErrorMessage"] + '</li>'
                         + '<li class="fail">' + pwdRules["PasswordMustContainOneNumeric.ErrorMessage"] + '</li>'
                         + '<li class="fail">' + pwdRules["PasswordInvalidCharacters.ErrorMessage"] + '</li>'
                         + '</ul></div>'

             });
         });

    </script>
    --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <asp:ScriptManager ID="smRecoverPwd" runat="server" />
    <asp:UpdatePanel ID="upnlResetPwd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="recoverPwd" class="">
                <asp:Label ID="lblOldBadLink" runat="server" ForeColor="Red"></asp:Label>
                <div id="pnlRecoverPwd" runat="server">
                    <%--<anrui:GlobalWebBoxedPanel ID="pnlRecoverPwd" DefaultButton="bttSave" Visible="true" runat="server" ShowHeader="true" HeaderText="<%$Resources:~/account/recoverpwd,pnlRecoverPwd.HeaderText %>"
                    Width="700px">--%>
                    <div class="dialogue-body">
                        <div class="content-detail">
                            <div class="form-container">
                                <div class="bd width-60">
                                    <div class="group input-password required form-element field">
                                        <label>
                                            <asp:Localize ID="Localize3" runat="server" Text="<%$Resources:~/account/recoverpwd,lcalNewPwd.Text %>"></asp:Localize>:</label>
                                        <p class="error-message"> <asp:Localize ID="Localize5" runat="server" Text="<%$Resources:~/account/recoverpwd,rfvNewPwd.ErrorMessage %>"></asp:Localize></p>
                                        <p class="custom-error-message"><asp:Localize ID="Localize7" runat="server" Text='<%$Resources:~/account/updatepwd,revNewPwd.ErrorMessage %>'></asp:Localize></p>
                                        <asp:TextBox ID="txtNewPwd"   onBlur="validate('new-password',this.value)" runat="server" TextMode="Password" AutoCompleteType="None" CssClass="newPassword"></asp:TextBox>
                                    </div>
                                    <%-- <span class="errormsg" id="new-password"></span>--%>
                                    <div class="group input-password required form-element field">
                                        <label>
                                            <asp:Localize ID="Localize4" runat="server" Text="<%$Resources:~/account/recoverpwd,lcalNewPwdConfirm.Text %>"></asp:Localize>:</label>
                                        <p class="error-message">
                                            <asp:Localize ID="Localize2" runat="server" Text='<%$Resources:~/account/updatepwd,txtNewPwdConfirm.ErrorMessage%>'></asp:Localize></p>
                                        <p class="custom-error-message">
                                            <asp:Localize ID="Localize1" runat="server" Text='<%$Resources:~/account/updatepwd,cvPwdMatch.ErrorMessage%>'></asp:Localize>
                                        </p>
                                        <asp:TextBox ID="txtReType" onBlur="validate('repeat-password', this.value)" runat="server" TextMode="Password" AutoCompleteType="None" CssClass="confirmPassword"></asp:TextBox>
                                    </div>

                                   <div class="settingrow">
                                <asp:Label ID="lblMessage" Text="" runat="server" CssClass="msg server-error-message"></asp:Label>
                            </div>
                                    <div class="group overflow form-element margin-top-15">
                                        <asp:Button ID="bttSave" Text="<%$Resources:~/account/recoverpwd, bttSave.Text%>"
                                            runat="server" ValidationGroup="vgRecoverPwd"
                                            OnClick="bttSave_Click" OnClientClick="return ClearDisplayMsg()" CssClass="button form-submit"></asp:Button>
                                        <asp:Button ID="bttCancel" Text="<%$Resources:Common, Cancel%>"
                                            runat="server" CausesValidation="false"
                                            OnClick="bttCancel_Click" CssClass="button"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%-- <div class="form-element"><span id="pGuidelinesText" class="left"></span><span class='what' onclick="showHelp()"></span></div>
                <div class="guidelines">
                    <asp:Localize ID="lcalPwdGuide" runat="server" Text='<%$Resources:~/account/updatepwd,lcalPwdGuide.Text %>'></asp:Localize>
                </div>--%>
                <%--</anrui:GlobalWebBoxedPanel>--%>
                <asp:Panel ID="pnlContinue" runat="server" Visible="false">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$Resources:~/account/recoverpwd,lcalThankMsgPassword.Text %>"></asp:Localize>
                    <br />
                    <br />
                    <asp:Button ID="btnContinue" SkinID="btngreenbg" runat="server" Text="<%$Resources:~/account/recoverpwd,btnContinue.Text %>" OnClick="btnContinue_Click" CausesValidation="false" ValidationGroup="vgContinue" CssClass="button" />
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="upResetPwd" runat="server" AssociatedUpdatePanelID="upnlResetPwd">
        <ProgressTemplate>
            <div class="updateProgress">
                <img src="/images/indicator.gif" alt="" /><asp:Literal ID="ltrProcess" runat="server"
                    Text="<%$Resources:Common, Processing.Text%>"></asp:Literal>
            </div>
            <div class="updateProgressbackground">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
     <script type="text/javascript">
         pwdRules = <%=pwdRules%>
             
         $(window).load(function() {
             pwdId = <%=string.Format("\"#{0}\"", txtNewPwd.ClientID.ToString())%> 
             });
         //alert(pwdId);
         //console.log(pwdId);
         function ClearDisplayMsg() {
             return validateOnSubmit();
         }
        </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>

