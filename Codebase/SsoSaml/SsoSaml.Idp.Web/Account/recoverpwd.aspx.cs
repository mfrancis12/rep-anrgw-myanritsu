﻿using System;
using System.Web;
using System.Text.RegularExpressions;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using CoreUtils = Anritsu.AnrCommon.CoreLib;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Lib.SingleSignOn;
using Anritsu.AnrCommon.CoreLib;
using System.Collections;
using System.Text;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class recoverpwd : IdpBasePage
    {
        public String pwdRules;

        public Guid ResetPwdToken
        {
            get
            {
                var tkn = WebUtility.ReadRequest(KeyDef.ParmKeys.CommonParams.FTK, true);
                if (string.IsNullOrEmpty(tkn) || !MethodExt.IsGuid(tkn) || Request.Browser.Crawler)
                    throw new HttpException(404, "Page not found.");

                var tknGuid = new Guid(tkn);
                if (tknGuid == Guid.Empty)
                    throw new HttpException(404, "Page not found.");
                return tknGuid;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Form.Attributes["autocomplete"] = "off";
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            pwdRules = GetPasswordGuideLines();
            if (IsPostBack) return;
            ValidateToken();
            pnlContinue.Visible = false;
        }

        private void ValidateToken()
        {
            var r = GwIdpUserBLL.GetResetPasswordTokenInfo(ResetPwdToken);
            if (r == null)
                throw new HttpException(404, "Page not found.");
            if (Convert.ToBoolean(r["IsAuthorized"]))
            {
                pnlRecoverPwd.Visible = true;
            }
            else
                InvalidResetPwdLink();
        }

        private void InvalidResetPwdLink()
        {
            lblOldBadLink.Text =
                GetGlobalResourceObject("~/account/recoverpwd", "lcalOldBadLink.ErrorMessage").ToString();
            pnlRecoverPwd.Visible = false;
        }

        protected void bttCancel_Click(object sender, EventArgs e)
        {
            RedirectToReturnPage();
        }

        private bool ValidInputs()
        {
            #region " validation "

            if (string.IsNullOrEmpty(txtNewPwd.Text))
            {
                 lblMessage.Text =
                GetGlobalResourceObject("~/account/recoverpwd", "lcalBlankPassword.ErrorMessage").ToString();
                return false;
            }
            if (string.IsNullOrEmpty(txtReType.Text) || (txtNewPwd.Text != txtReType.Text))
            {
                lblMessage.Text =
                GetGlobalResourceObject("~/account/recoverpwd", "lcalPassNotMatch.ErrorMessage").ToString();
                return false;
            }
            var regPwd = new Regex(@"^.*(?=.{8,})(?=.*[a-zA-Z]{1,})(?=.*[0-9]{1,})(?=.*[!@#\$%\^\*]{0,}).*$");
            if (regPwd.IsMatch(txtNewPwd.Text.Trim())) return true;
            lblMessage.Text =
            GetGlobalResourceObject("~/account/recoverpwd", "lcalNoFollowGuidelines.ErrorMessage").ToString();
            return false;

            #endregion
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            if (!IsValid || !ValidInputs())
            {
                return;
            }
            var newPlainPassword = txtNewPwd.Text;
            string erroMessage;
            //check for valid anritsu password
            var isValidAnritsuPassword = IsValidAnritsuPassword(newPlainPassword, out erroMessage);
            if (!isValidAnritsuPassword)
            {
                lblMessage.Text = erroMessage;
                return;
            }
            try
            {
                var token = ResetPwdToken;
                var r = GwIdpUserBLL.GetResetPasswordTokenInfo(token);
                if (r == null) throw new HttpException(404, "Page not found.");
                var isAuthorizedToken = Convert.ToBoolean(r["IsAuthorized"]);
                var userId = Convert.ToInt32(r["GWUserId"]);
                var userIp = WebUtility.GetUserIP();

                if (!isAuthorizedToken)
                {
                    InvalidResetPwdLink();
                    return;
                }
                var usr = GwIdpUserBLL.GetUserByGWUserId(userId);
                if (usr == null)
                {
                    InvalidResetPwdLink();
                    return;
                }

                var currentEncryptrdpwd = usr["EncryptedPassword"].ToString();
                var emailAddress = usr["EmailAddress"].ToString();

                //GwIdpAuthStatusCode statusCode = Lib.SingleSignOn.GwIdpUserBLL.UpdatePassword(userID, newPlainPassword, currentEncryptrdpwd, userIP, false);
                var statusCode = GwIdpUserBLL.UpdatePassword(userId, newPlainPassword, currentEncryptrdpwd, userIp,
                    false, emailAddress);

                switch (statusCode)
                {
                    case GwIdpAuthStatusCode.MaxFailedLoginAttempts:
                        lblMessage.Text = GetGlobalResourceObject("IdpAuthStatus", "AuthStatus_MaxFailedLoginAttempts").ToString();
                        return;
                    case GwIdpAuthStatusCode.InvalidUserOrPassword:
                        lblMessage.Text = GetGlobalResourceObject("Common", "InvalidNewPassword.ErrorMessage").ToString();
                        return;
                    case GwIdpAuthStatusCode.Authorized:
                        GwIdpUserBLL.UpdateResetPasswordTokenIsUsed(token);
                        pnlRecoverPwd.Visible = false;
                        pnlContinue.Visible = true;
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                // lblMessage.Text = ex.Message;
                pnlContinue.Visible = false;
            }
        }

        public bool IsValidAnritsuPassword(string password, out string messages)
        {
            messages = "";
            var al = new ArrayList();

            if (password.IndexOf("'", StringComparison.Ordinal) >= 0)
            {
                al.Add(
                    GetGlobalResourceObject("PasswordGuideLines", "PasswordInvalidCharacters.ErrorMessage").ToString());
            }

            if (password.IndexOf(" ", StringComparison.Ordinal) >= 0)
            {
                al.Add(GetGlobalResourceObject("~/account/recoverpwd", "PasswordBlankCharacters").ToString());
            }

            if (password.Trim().Length < 8)
            {
                al.Add(GetGlobalResourceObject("PasswordGuideLines", "PasswordLength.ErrorMessage").ToString());
            }

            const string specialChars = "!@#$%^*-";
            var hasDigit = false;
            var hasLetter = false;
            var hasBadSpecialChar = false;
            for (var i = 0; i <= password.Length - 1; i++)
            {
                var c = password[i];

                if (Char.IsLetterOrDigit(c))
                {
                    if (!hasDigit)
                        hasDigit = Char.IsDigit(c);

                    if (!hasLetter)
                        hasLetter = Char.IsLetter(c);
                }
                else
                {
                    if (!hasBadSpecialChar)
                        hasBadSpecialChar = (specialChars.IndexOf(c) == -1);
                }

            }

            if (!hasDigit)
            {
                al.Add(
                    GetGlobalResourceObject("PasswordGuideLines", "PasswordMustContainOneNumeric.ErrorMessage")
                        .ToString());
            }
            if (!hasLetter)
            {
                al.Add(
                    GetGlobalResourceObject("PasswordGuideLines", "PasswordMustContainOneAlphabet.ErrorMessage")
                        .ToString());
            }

            if (hasBadSpecialChar)
            {
                al.Add(
                    GetGlobalResourceObject("PasswordGuideLines", "PasswordInvalidCharacters.ErrorMessage").ToString());
            }

            if (al.Count <= 0) return true;
            var sb = new StringBuilder(100);

            var errText = GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationError").ToString();
            if (al.Count > 1)
                errText = GetGlobalResourceObject("~/account/recoverpwd", "PasswordValidationErrors").ToString();

            sb.Append("<table>");
            sb.Append(@"<tr><td colspan=""2""><font color=""red"" face=""verdana"" size=""1.8""><b>" + errText +
                      ":</b></font></td><tr>");

            foreach (string str in al)
            {
                sb.Append(
                    String.Format(
                        @"<tr><td valign=""top""><li style=""COLOR: red""></li></li></td><td><font color=""red"" face=""verdana"" size=""1.8"">{0}</font></td><tr>",
                        str));
            }
            sb.Append("</table>");

            messages = sb.ToString();
            return false;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            RedirectToReturnPage();
        }

        protected void RedirectToReturnPage()
        {
            var replyUrl = Request[KeyDef.ParmKeys.ReplyUrl];
            Response.Redirect(GetRedirectUrl(replyUrl));
        }

        private string GetPasswordGuideLines()
        {
            var browserLang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            var lang = (browserLang == null ? "en-US" : browserLang);
            var dt = ResourceStringsBLL.ResourceStrings_SelectByClass("PasswordGuideLines", lang);
            var sb = new StringBuilder("{");
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append(String.Format(@"""{0}"":""{1}""",
                    ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ResourceKey"])
                    , ConvertUtility.ConvertNullToEmptyString(dt.Rows[i]["ContentStr"])));
            }
            return sb.Append("}").ToString();
        }
    }
}