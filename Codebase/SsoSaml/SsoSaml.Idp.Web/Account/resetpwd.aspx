﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="resetpwd.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.Account.resetpwd" %>

<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<%@ Register TagPrefix="AjaxControlToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <style type="text/css">
        .fp_tb
        {
            width: 100%;
        }

            .fp_tb .lbl
            {
                font-weight: bolder;
                text-align: right;
            }

        .fp_cptr
        {
            text-align: right;
        }

        .fp_desc
        {
            padding: 10px 5px 10px 5px;
        }

        #crumbcontainer
        {
            display: none;
        }

        #resetPwdBox
        {
            margin: 10px 10px 10px 70px;
            width: 600px;
            color:#666666;
            font-size:12px !important;   
        }
         center { margin:10px 0px 0px 0px;}
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphContentTop" runat="server">
    <asp:ScriptManager ID="smResetPwd" runat="server" />
    <asp:UpdatePanel ID="upnlResetPwd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="resetPwdBox" class="">
                <div id="pnlResetPwd" runat="server" >
                <div class="dialogue-header">
                    <h3>
                        <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:~/account/resetpwd,pnlResetPwd.HeaderText %>" /></h3>
                </div>
                <%-- <anrui:GlobalWebBoxedPanel ID="pnlResetPwd" runat="server" ShowHeader="true" HeaderText="<%$Resources:~/account/resetpwd,pnlResetPwd.HeaderText %>"
                    DefaultButton="bttSubmit" Height="280">--%>
                <div class="dialogue-body">
                    <p class="fp_desc">
                        <asp:Localize ID="lcalDesc" runat="server" Text="<%$Resources:~/account/resetpwd, lcalResetPwdGuide.Text %>"></asp:Localize>
                    </p>
                    <div class="content-detail">
                        <div class="form-container">
                            <div class="bd width-60">
                                <div class="group input-text required">
                                    <label>
                                        <asp:Localize ID="lcalContactMail" runat="server" Text="<%$Resources:~/account/resetpwd, lcalUserEmailOrLoginName.Text %>"></asp:Localize>:*
                                    </label>
                                     <p class="server-error-message">
                        <asp:Localize ID="lblMessage" runat="server"></asp:Localize>
                    </p>
                                    <p class="error-message">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:~/account/resetpwd,rfvEmailOrLoginName.ErrorMessage%>" />
                                    </p>
                                        <asp:TextBox ID="txtEmailAddress" runat="server" AutoCompleteType="None"></asp:TextBox>
                                </div>
                                <sbk:CaptchaControl ID="captchactrl" LayoutStyle="CssBased" runat="server" CssClass="captcha" EnableClientScript="false"
                                ErrorMessage="<%$Resources:Common, lcalCaptchaInvalidCode.ErrorMessage %>" CaptchaLength="4" ForeColor="Red"
                                ControlToValidate="txtEmailAddress" ValidationGroup="vgResetPwd" Display="Dynamic" />
                                <div class="settingrow group overflow margin-top-15">
                                    <asp:Button ID="bttSubmit" Text="<%$Resources:~/account/resetpwd, bttSubmit.Text%>" runat="server"
                                        ValidationGroup="vgResetPwd" OnClick="bttSubmit_Click" CssClass="form-submit button"></asp:Button>
                                    <asp:Button ID="bttLogin" Text="<%$Resources:~/account/resetpwd,bttLogin.Text %>" runat="server"
                                        CausesValidation="false" OnClick="bttLogin_Click" CssClass="button"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- <table class="fp_tb" cellpadding="3" cellspacing="3" border="0" align="center">
                                <tr>
                                    <td class="lbl">
                                        <asp:Localize ID="lcalContactMail" runat="server" Text="<%$Resources:~/account/resetpwd, lcalUserEmailOrLoginName.Text %>"></asp:Localize>:*
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmailAddress" runat="server" SkinID="tbx-20" Width="250" AutoCompleteType="None"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="EmailValidator" ControlToValidate="txtEmailAddress"
                                            ErrorMessage="<%$Resources:~/account/resetpwd,rfvEmailOrLoginName.ErrorMessage%>"
                                            runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgResetPwd" />
                                        <AjaxControlToolkit:ValidatorCalloutExtender ID="EmailValidatorCalloutExtender" runat="server"
                                            TargetControlID="EmailValidator" />
                                    </td>
                                </tr>
                            </table>--%>
                   
                    <%-- <sbk:CaptchaControl ID="captchactrl" LayoutStyle="CssBased" runat="server" CssClass="captcha" EnableClientScript="false"
                                ErrorMessage="<%$Resources:Common, lcalCaptchaInvalidCode.ErrorMessage %>" CaptchaLength="4" ForeColor="Red"
                                ControlToValidate="txtEmailAddress" ValidationGroup="vgResetPwd" Display="Dynamic" />
                            <br />--%>
                    <div class="clearfloat">
                    </div>

                </div>
                    </div>
                <%--</anrui:GlobalWebBoxedPanel>--%>
                
            </div>
            <asp:Panel ID="pnlContinue" runat="server" Visible="false">
                    <asp:Localize ID="Localize7" runat="server"></asp:Localize><br />
                    <br />
                </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="upResetPwd" runat="server" AssociatedUpdatePanelID="upnlResetPwd">
        <ProgressTemplate>
            <div class="updateProgress">
                <img src="/images/indicator.gif" alt="" /><asp:Literal ID="ltrProcess" runat="server"
                    Text="<%$Resources:Common, Processing.Text%>"></asp:Literal>
            </div>
            <div class="updateProgressbackground">
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
