﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using SDK = Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;

namespace Anritsu.SsoSaml.Idp.Web.Account
{
    public partial class resetpwd : IdpBasePage
    {
        public string EmailAddress
        {
            get
            {
                var email = WebUtility.ReadRequest<string>(KeyDef.ParmKeys.CommonParams.EmailId, true);
                return string.IsNullOrEmpty(email) ? string.Empty : email.HTMLDecode();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Form.Attributes["autocomplete"] = "off";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            txtEmailAddress.Text = EmailAddress;
            pnlContinue.Visible = false;
        }

        private bool IsValidInputs()
        {
            if (string.IsNullOrEmpty(txtEmailAddress.Text))
            {
                lblMessage.Text =
                    GetGlobalResourceObject("~/account/resetpwd", "rfvEmailOrLoginName.ErrorMessage").ToString();
                return false;
            }
            if (txtEmailAddress.Text.IsValidLoginNameOrEmailFormat()) return true;
            lblMessage.Text =
                GetGlobalResourceObject("~/account/resetpwd", "lcalInvalidLoginNameorEmail.ErrorMessage").ToString();
            return false;
        }

        protected void bttSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid || !IsValidInputs()) return;

            try
            {
                if (!txtEmailAddress.Text.Trim().IsValidEmailFormat())
                    throw new ArgumentException(
                        GetGlobalResourceObject("~/account/resetpwd", "lcalInvalidLoginNameorEmail.ErrorMessage")
                            .ToString());
                var rowGwUser = BLL.SingleSignOn.GwIdpUserBLL.GetUserIdByEmailOrUserName(txtEmailAddress.Text.Trim());
                if (rowGwUser == null)
                {
                    //invalidate the captcha
                    throw new ArgumentException(
                        GetGlobalResourceObject("~/account/resetpwd", "lcalInvalidLoginNameorEmail.ErrorMessage")
                            .ToString());
                }

                var newToken =
                    BLL.SingleSignOn.GwIdpUserBLL.CreateResetPasswordToken(int.Parse(rowGwUser["GWUserId"].ToString()));
                if (newToken == Guid.Empty)
                    throw new ArgumentException("ERROR");
                //get the CurrentCulture GroupID
                var cultureGroupId = GetCurrentCultureGroupId(GetCulture());

                #region "Send email using EmailAPI "

                var req = new SDK.EmailWebRef.SendTemplatedEmailCallRequest
                {
                    CultureGroupId = cultureGroupId,
                    EmailTemplateKey = KeyDef.EmailTemplates.USER_FORGOT_PWD,
                    ToEmailAddresses = new[] {rowGwUser["EmailAddress"].ToString()}
                };
                SDK.EmailWebRef.ReplaceKeyValueType keyValue = null;

                #region " subject replacement values "

                var subjR = new List<SDK.EmailWebRef.ReplaceKeyValueType>();

                keyValue = new SDK.EmailWebRef.ReplaceKeyValueType
                {
                    TemplateKey = "[[LASTNAME]]",
                    ReplacementValue = rowGwUser["LastName"].ToString()
                };
                subjR.Add(keyValue);

                keyValue = new SDK.EmailWebRef.ReplaceKeyValueType
                {
                    TemplateKey = "[[FIRSTNAME]]",
                    ReplacementValue = rowGwUser["FirstName"].ToString()
                };
                subjR.Add(keyValue);

                keyValue = new SDK.EmailWebRef.ReplaceKeyValueType
                {
                    TemplateKey = "[[MIDDLENAME]]",
                    ReplacementValue = rowGwUser["MiddleName"].ToString()
                };
                subjR.Add(keyValue);

                req.SubjectReplacementKeyValues = subjR.ToArray();

                #endregion

                #region " body replacement values "

                var bodyR = new List<SDK.EmailWebRef.ReplaceKeyValueType>();

                var returnUrl = ReturnUrl;

                keyValue = new SDK.EmailWebRef.ReplaceKeyValueType
                {
                    TemplateKey = "[[RESETPWDURL]]",
                    ReplacementValue = ConfigurationManager.AppSettings["USREF_SSORESETPWD_URL"]
                        .Replace("[[FORGOTPWDTOKEN]]", newToken.ToString().ToUpper())
                        .Replace("[[RETURNURL]]", returnUrl)
                        .Replace("[[LL-CC]]", GetCulture())
                };
                bodyR.Add(keyValue);

                string[] supportedNames =
                {
                    "GWUserId", "JobTitle", "EmailAddress", "FirstName", "MiddleName", "LastName", "JobTitle",
                    "CompanyName"
                    , "StreetAddress1", "StreetAddress2", "City", "State", "ZipCode", "CountryCode", "PhoneNumber",
                    "FaxNumber", "RegisteredFromURL"
                };
                if (rowGwUser != null)
                {
                    foreach (
                        DataColumn col in
                            rowGwUser.Table.Columns.Cast<DataColumn>()
                                .Where(col => supportedNames.Contains(col.ColumnName)))
                    {
                        keyValue = new SDK.EmailWebRef.ReplaceKeyValueType
                        {
                            TemplateKey = string.Format("[[{0}]]", col.ColumnName.ToUpper())
                        };
                        var val = rowGwUser[col.ColumnName];
                        keyValue.ReplacementValue = (val == null ? string.Empty : val.ToString());
                        bodyR.Add(keyValue);
                    }
                }
                //
                req.BodyReplacementKeyValues = bodyR.ToArray();

                #endregion

                //send an email with Email service
                var res = SDK.EmailServiceManager.SendTemplatedEmail(req);
                if (res.StatusCode == 0)
                {
                    pnlResetPwd.Visible = false;
                    pnlContinue.Visible = true;
                    //set the text
                    Localize7.Text = GetGlobalResourceObject("~/account/resetpwd", "lcalThankyouContent.Text").ToString().Replace("[[CONTACTUSLINK]]", ConfigurationManager.AppSettings["Idp.Web.ContactUsUrl"]);
                }
                else
                {
                    pnlResetPwd.Visible = true;
                    pnlContinue.Visible = false;
                }

                #endregion
            }
            catch (ArgumentException ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void bttLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect(ReturnUrl);
        }

        private string GetCulture()
        {
            return Request.QueryString[KeyDef.ParmKeys.SsoIDP.SiteLocale]
                   ?? System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
        }

        private string GetDefaultReturnUrl()
        {
            var defaultToken =
                ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID"), Guid.Empty);
            var spToken = ServiceProviderToken != Guid.Empty ? ServiceProviderToken : defaultToken;
            var dr = BLL.SingleSignOn.GwIdpUserBLL.Get_SpLoginURL_By_SpToken(spToken);
            return dr["SpLoginUrl"].ToString();
        }

        private String ReturnUrl
        {
            get
            {
                var returnUrl = Request[KeyDef.ParmKeys.ReplyUrl].IsNullOrEmptyString()
                    ? Request[KeyDef.ParmKeys.ReturnURL]
                    : Request[KeyDef.ParmKeys.ReplyUrl];

                return GetRedirectUrl(returnUrl, GetDefaultReturnUrl());
            }
        }
    }
}