﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Admin_SpMenu.ascx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.App_Controls.Admin_SpMenu" %>
<anrui:GlobalWebBoxedPanel ID="gwpnlResAdmin" runat="server" ShowHeader="false" IsSideBarLinkList="true">
    <ul>
        <li><a href="/siteadmin/home">Admin Home</a></li>
        <li><a href="/siteadmin/spadmin/home">Service Providers</a></li>
        <li><a href="/siteadmin/spadmin/x509list">x509 Certificates</a></li>
        <li><a href="/siteadmin/spadmin/x509detail?certid=0">Add New x509 Certificate</a></li>
    </ul>
</anrui:GlobalWebBoxedPanel>