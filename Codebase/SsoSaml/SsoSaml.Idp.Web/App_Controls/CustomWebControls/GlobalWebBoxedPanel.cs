﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.App_Controls.CustomWebControls
{
    public class GlobalWebBoxedPanel : Panel
    {
        public string CssOuterWrapperClass
        {
            get
            {
                if (ViewState[AddPrefixName("CssOuterWrapperClass")] == null)
                    ViewState[AddPrefixName("CssOuterWrapperClass")] = "panelwrapper boxedpanel";
                return ViewState[AddPrefixName("CssOuterWrapperClass")] as string;
            }
            set { ViewState[AddPrefixName("CssOuterWrapperClass")] = value; }
        }

        public string HeaderText
        {
            get { return ViewState[AddPrefixName("HeaderText")] as String; }
            set { ViewState[AddPrefixName("HeaderText")] = value; }
        }

        public bool ShowHeader
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("ShowHeader")], true); }
            set { ViewState[AddPrefixName("ShowHeader")] = value; }
        }

        public bool IsSideBarLinkList
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("IsSideBarLinkList")], false); }
            set { ViewState[AddPrefixName("IsSideBarLinkList")] = value; }
        }

        public bool NoBorder
        {
            get { return ConvertUtility.ConvertToBoolean(ViewState[AddPrefixName("_NoBorder")], false); }
            set { ViewState[AddPrefixName("_NoBorder")] = value; }
        }

        private string AddPrefixName(string input)
        {
            return string.Format("{0}{1}", ID, input);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null) return;

            writer.Write("<div id='{0}' class='{1}'>", ClientID, CssOuterWrapperClass); //div1            
            writer.Write("<a id='{0}_moduleanchor' class='moduleanchor'></a>", ClientID);
            if (ShowHeader) writer.Write("<h2 class='art-PostHeader moduletitle'>{0}</h2>", HeaderText);
            writer.Write("<div class='modulecontentwrap'>"); //div2
            if (!NoBorder) writer.Write("<div class=' modulecontent' id='boxcontent_{0}'>", ClientID); //div3
            if (IsSideBarLinkList) writer.Write("<div class='gwsbrmenu no-bullets'>"); //div4
            RenderContents(writer);
            if (IsSideBarLinkList) writer.Write("</div>"); //div4
            if (!NoBorder) writer.Write("</div>"); //div3
            writer.Write("</div>"); //div2
            writer.Write("</div>"); //div1
        }
    }
}