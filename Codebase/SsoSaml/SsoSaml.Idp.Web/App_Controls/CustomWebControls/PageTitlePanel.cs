﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.SsoSaml.Idp.Web.App_Controls.CustomWebControls
{
    public class PageTitlePanel : WebControl, INamingContainer
    {
        private const String ResourcekeyPagetitle = "PageTitle";
        private const String ResourcekeyBrowsertitle = "BrowserPageTitle";

        private String _greenPageTitleText;
        private String _browserTitleText;

        public Boolean AppendDefaultAppBrowserTitle { get; set; }
        public String PageTitleType { get; set; }

        public String GreenPageTitleText
        {
            get { return _greenPageTitleText; }
            set { _greenPageTitleText = value; }
        }

        public String BrowserTitleText
        {
            get { return _browserTitleText; }
            set { _browserTitleText = value; }
        }

        public PageTitlePanel()
        {
            AppendDefaultAppBrowserTitle = true;
            PageTitleType = "GreenPageTitle";
            if (HttpContext.Current == null)
            {
                return;
            }
            if (String.IsNullOrEmpty(GreenPageTitleText))
                GreenPageTitleText = GetResourceStringForPage(ResourcekeyPagetitle);
            if (String.IsNullOrEmpty(BrowserTitleText))
                BrowserTitleText = GetResourceStringForPage(ResourcekeyBrowsertitle);

            EnsureChildControls();

        }

        private void Initialize()
        {
            if (HttpContext.Current == null)
            {
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HttpContext.Current == null)
            {
                return;
            }
            Initialize();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (HttpContext.Current == null)
            {
                writer.Write("[" + this.ID + "]");
                return;
            }

            string titleText;
            switch (PageTitleType.ToLowerInvariant())
            {
                case "greenpagetitle":
                    titleText = _greenPageTitleText;
                    break;
                case "browsertitle":
                    titleText = _browserTitleText;
                    if (titleText == ResourcekeyBrowsertitle) titleText = _greenPageTitleText;
                    if (AppendDefaultAppBrowserTitle)
                        titleText += " - " + HttpContext.GetGlobalResourceObject("Common", "DefaultAppBrowserTitle");
                    break;
                default:
                    titleText = "-";
                    break;
            }
            writer.Write(titleText);
        }

        private String GetResourceStringForPage(String resourceKey)
        {
            if (String.IsNullOrEmpty(resourceKey)) return String.Empty;
            var pageUrl = App_Lib.UI.UIHelper.GetPageUrl();
            object res = HttpContext.GetGlobalResourceObject(pageUrl, resourceKey) as String;
            return res == null ? resourceKey : res.ToString();
        }
    }
}