﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.SsoSaml.Idp.Web.App_Controls
{
    public partial class UCDynamicCdn : System.Web.UI.UserControl
    {

        public string RelativePath
        {
            get
            {
                return Convert.ToString(Request.QueryString["rel"]);
            }
        }

        public string CdnPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["StaticCdn"]);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.Write(string.Format("{0}{1}", CdnPath, RelativePath));
            Response.End();
        }
    }
}