﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcBreadCrumb.ascx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.App_Controls.UcBreadCrumb" %>
<div id="crumbcontainer">
    <div class="breadcrumbs">
    <asp:DataList ID="dlBC" runat="server" RepeatDirection="Horizontal" ItemStyle-CssClass="unselectedcrumb" SelectedItemStyle-CssClass="selectedcrumb" Visible="false">
        <ItemTemplate>
            <asp:HyperLink ID="hlBC" runat="server" CssClass="unselectedcrumb" EnableTheming="false" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "PageUrl") %>' Text='<%# DataBinder.Eval(Container.DataItem, "PageTitle") %>'></asp:HyperLink>
        </ItemTemplate>
        <SelectedItemTemplate>
            <span class='selectedcrumb'><%# DataBinder.Eval(Container.DataItem, "PageTitle") %></span>
        </SelectedItemTemplate>
        <SeparatorTemplate>&nbsp;>&nbsp;</SeparatorTemplate>
    </asp:DataList>
    </div>
</div>