﻿using System;
using Anritsu.AnrCommon.CoreLib;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System.Web;
using ApiSdk;
using System.Threading;

namespace Anritsu.SsoSaml.Idp.Web.App_Controls
{
    public partial class UcFooter : System.Web.UI.UserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            ltrFooter.Text = GetFooterHtml(Thread.CurrentThread.CurrentCulture.ToString());
        }

        protected string GetFooterHtml(string currentLanguage)
        {
            var apiUri =
                new Uri(ConfigurationManager.AppSettings["Sitecore_FooterAPI_Url"].Replace("[[CULTURE_CODE]]",
                    currentLanguage));
            var apiParams = new Dictionary<string, string>();

            var rsClass = new ResponseClass();
            try
            {
                //log request strat time
                //get Logger Connection string 
                var logConnStr = ConfigUtility.ConnStrGetValue("CoreLib.ErrorLogUtility");
                Object logger = new { AppDescrption = "SsoSaml", RequestMethod = "GetFooterHtml", LoggerConnection = logConnStr };
                //AppLogUtility.InitRequestLog("SsoSaml", Request.Url.Host, Request.RawUrl, apiUri.ToString(), GetParams(apiParams), "GetFooterHtml",null,"Request Initiated");
                var response = rsClass.CallMethod<String>(apiUri, apiParams,logger:logger);
                //log request response time/end time
                //AppLogUtility.EndRequestLog("SsoSaml", Request.Url.Host, Request.RawUrl, apiUri.ToString(), GetParams(apiParams), "GetFooterHtml", null, "Request End");
                return response.StatusCode == HttpStatusCode.OK
                    ? Convert.ToString(response.Content) : string.Empty;
            }
            catch (Exception ex)
            {
                ErrorLogUtility.LogError("SsoSaml", Request.Url.Host, ex, HttpContext.Current);
                //AppLogUtility.EndRequestLog("SsoSaml", Request.Url.Host, Request.RawUrl, apiUri.ToString(), GetParams(apiParams), "GetFooterHtml", null, "Request End due to Exception :"+ex.Message+":"+ex.StackTrace);
                return string.Empty;
            }
        }

        private string GetParams(Dictionary<string, string> param)
        {
           var requestParams=new  StringBuilder();
            if (param.Count > 0)
            {
                if (!requestParams.ToString().Contains("?"))
                    requestParams.Append("?");

                foreach (var key in param)
                {
                    requestParams.Append(key.Key);
                    requestParams.Append("=");
                    requestParams.Append(key.Value);
                    requestParams.Append("&");
                }
            }
            else return null;
            return requestParams.ToString();
        }
    }
}