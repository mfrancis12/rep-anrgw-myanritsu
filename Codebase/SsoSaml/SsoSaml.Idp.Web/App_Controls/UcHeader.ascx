﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcHeader.ascx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.App_Controls.UcHeader" %>
<div class="header-area header4-search">
     <header id="global-header">
      <h1 class="header__logo">
      <a href="<%=AnritsuLogoLink %>">
    </a>
     </h1>

 <ul class="util-links">
<li class="util__logout">
   <asp:HyperLink ID="hlSignOut" runat="server" Visible="false" NavigateUrl="/signout"
                Text="<%$Resources:Common,SignOut %>"></asp:HyperLink>

</li>
</ul>
  
</header>
 </div>
<%--<div class="header text-center">

    <asp:HyperLink ID="hlHome1" runat="server" class="logo desktop"><img src="//dl.cdn-anritsu.com/appfiles/img/icons/logo.png" alt="Anritsu" /><h1>anritsu</h1></asp:HyperLink>
    <a href="/" class="mobile-logo mobile">
        <img src="//dl.cdn-anritsu.com/appfiles/img/icons/logo-mobile.png" alt="anritsu">
    </a>
    <div class="sso-main-container" style="margin: -20px 0 0 50px;display: inline-block" >
    <a   id="lnkMyAnritsu" runat="server" >
       
         <h2 style="color:#2F977A; font-style:normal"><asp:Localize ID="lcalSignInLegend" runat="server" Text="<%$Resources:~/signin,lcalSignInLegend.Text %>"></asp:Localize></h2>
    </a>
        </div>
    <div class="nav">
        <div class="desktop">
            <span class="margin-right-15">
                <asp:Literal ID="ltrLoginName" runat="server" Visible="false"></asp:Literal></span>
            <asp:HyperLink ID="hlSignOut" runat="server" Visible="false" NavigateUrl="/signout"
                Text="<%$Resources:Common,SignOut %>"></asp:HyperLink>
        </div>
    </div>
</div>--%>

<%--<div class="header1right">
            <div class="cell" style='text-align: right;'>
                <div class="myanritsu">
                    <asp:Literal ID="ltrLoginName" runat="server" Visible="false"></asp:Literal>
                    <asp:HyperLink ID="hlSignOut" runat="server" Visible="false" NavigateUrl="~/signout" Text="<%$Resources:Common,SignOut %>"></asp:HyperLink>
                </div>
                <div class="clearfloat"></div>
            </div>
            <div class="title">
                <div class="myanritsutitle">
                        <span class="sitesloganspan"></span>
                </div>
                <div class="selectcountrytitle">
                        &nbsp;
                </div>
            </div>
            <div class="clearfloat"></div>
        </div>--%>
<%-- </div>--%>

<%--<div class="header small">
			<a href="/" class="logo desktop"> 
				<img src="../App_Themes/AnrGreen/static/img/icons/logo-normal.png" alt="anritsu">
				<h1>anritsu</h1>
			</a>
			<a href="/" class="mobile-logo mobile">
				<img src="../App_Themes/AnrGreen/static/img/icons/logo-mobile.png" alt="anritsu">
			</a>
        </div>--%>