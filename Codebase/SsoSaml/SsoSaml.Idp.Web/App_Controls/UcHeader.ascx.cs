﻿using System;
using System.Threading;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System.Web;
namespace Anritsu.SsoSaml.Idp.Web.App_Controls
{
    public partial class UcHeader : System.Web.UI.UserControl
    {
        string homeUrl = ConfigUtility.AppSettingGetValue("Idp.Web.HomeUrl");

        protected string AnritsuLogoLink
        {
            get
            {
                string gwCultureCode = Thread.CurrentThread.CurrentCulture.Name;
                var marketLocale = App_Lib.UI.UIHelper.InitMarketLocale();
                var homeUrl = ConfigUtility.AppSettingGetValue("Idp.Web.HomeUrl");
                var homesigninUrl = ConfigUtility.AppSettingGetValue("Idp.Web.HomeSigninUrl");

                homesigninUrl = marketLocale.IsNullOrEmptyString()
                    ? homesigninUrl.Replace("/[[LL-CC]]", "/" + UIHelper.GetLocaleDefaultMapping(gwCultureCode))
                    : homesigninUrl.Replace("[[LL-CC]]", marketLocale);
                homeUrl = marketLocale.IsNullOrEmptyString()
                    ? homeUrl.Replace("/[[LL-CC]]", "/" + UIHelper.GetLocaleDefaultMapping(gwCultureCode))
                    : homeUrl.Replace("[[LL-CC]]", marketLocale);

            
                if (Page.User.Identity.IsAuthenticated)
                {
                    homeUrl = homesigninUrl;

                }
               
                return (string.Format("{0}", homeUrl));
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (IsPostBack) return;

            if (!Page.User.Identity.IsAuthenticated) return;
            hlSignOut.Visible = true;
            //string gwCultureCode = Thread.CurrentThread.CurrentCulture.Name;
            //var marketLocale = App_Lib.UI.UIHelper.InitMarketLocale();
            //var homeUrl = ConfigUtility.AppSettingGetValue("Idp.Web.HomeUrl");
            //var homesigninUrl= ConfigUtility.AppSettingGetValue("Idp.Web.HomeSigninUrl");

            //homesigninUrl = marketLocale.IsNullOrEmptyString()
            //    ? homesigninUrl.Replace("/[[LL-CC]]", "/" + UIHelper.GetLocaleDefaultMapping(gwCultureCode))
            //    : homesigninUrl.Replace("[[LL-CC]]", marketLocale);
            //homeUrl = marketLocale.IsNullOrEmptyString()
            //    ? homeUrl.Replace("/[[LL-CC]]", "/"+ UIHelper.GetLocaleDefaultMapping(gwCultureCode))
            //    : homeUrl.Replace("[[LL-CC]]", marketLocale);
            //if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("edit-my-account"))
            //{
            //    hlHome1.NavigateUrl = homesigninUrl;

            //}
            //else
            //{
            //    hlHome1.NavigateUrl = homeUrl;
            //}

            //hlHome1.CssClass = "logo desktop";
            //if (!Page.User.Identity.IsAuthenticated) return;
            //hlSignOut.Visible = true;

            //lnkMyAnritsu.HRef =ConfigUtility.AppSettingGetValue("MyAnritsuUrl")+gwCultureCode;
            //var userData = App_Lib.UI.UIHelper.LoggedInUser_Get(false);
            //if (userData == null) return;
            //var loginName = userData["LoginName"].ToString();
            //if (loginName.IsNullOrEmptyString()) loginName = userData["EmailAddress"].ToString().ToLowerInvariant();
            //ltrLoginName.Text = String.Format("<span class='settinglabelplain'>My Anritsu ID:</span> {0}", String.IsNullOrEmpty(userData["LoginName"].ToString()) ? userData["EmailAddress"].ToString() : userData["LoginName"].ToString());
            //ltrLoginName.Visible = hlSignOut.Visible = true;

            hlSignOut.NavigateUrl = string.Format("{0}?{1}={2}&{3}={4}", "/signout",
                KeyDef.ParmKeys.SiteLocale,
                HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.SiteLocale].ConvertNullToEmptyString()), KeyDef.ParmKeys.ReturnURL, HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.ReturnURL].ConvertNullToEmptyString()));
        }
    }
}