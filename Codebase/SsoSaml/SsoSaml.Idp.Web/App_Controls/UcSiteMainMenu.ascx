﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcSiteMainMenu.ascx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.App_Controls.UcSiteMainMenu" %>
<div id="sitemenuwrap">
    <asp:Menu ID="smnu" runat="server" ClientIDMode="Static" 
        Orientation="Horizontal" DataSourceID="smd" EnableTheming="false" 
        StaticEnableDefaultPopOutImage="false" 
        onmenuitemdatabound="smnu_MenuItemDataBound">
    <StaticMenuItemStyle CssClass="staticitem" HorizontalPadding="15px" VerticalPadding="8px" />
    <DynamicMenuStyle BorderStyle="Solid" BorderWidth="2px" BorderColor="#086F6F"/>
    <DynamicMenuItemStyle CssClass="dynamicitem" BackColor="#E8FFED" HorizontalPadding="10px" VerticalPadding="5px" ForeColor="#086F6F"/>
    </asp:Menu>
<asp:SiteMapDataSource ID="smd" runat="server" StartingNodeUrl="~/" ShowStartingNode="false" EnableViewState="false" />
</div>