﻿/*
 * Author   :   Kishore Kumar M
 * Date of Creation :   15-Nov-2013
 * Purpose  : State user control
 * Date of modification:
 * ------------------------------------------------------------------------------
 * Date     :   Member          | Description                                    |
 * ------------------------------------------------------------------------------
 ***/
using System;
using System.Data;
using System.Web.UI.WebControls;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;


namespace Anritsu.SsoSaml.Idp.Web.App_Controls
{
    public partial class StateControl : System.Web.UI.UserControl
    {
        private DataTable _states;
        public string ValidationGroup { get; set; }
        public bool _IsUnitedStates { get; set; }
        public bool _IsJapan { get; set; }
        public int _Width { get; set; }
        public bool IsStateRequired { get; set; }

        public string SelectedStateText
        {
            get
            {
                if (_IsUnitedStates || _IsJapan)
                {
                    if (cmbState.SelectedItem == null) return string.Empty;
                    return cmbState.SelectedItem.Text;
                }
                return txtState.Text.Trim();
            }
        }

        public string SelectedStateValue
        {
            get
            {
                if (_IsUnitedStates || _IsJapan)
                {
                    if (cmbState.SelectedItem == null) return string.Empty;
                    return cmbState.SelectedValue;
                }
                return txtState.Text.Trim();
            }
        }

        public StateControl()
        {
            var cultureGroupId = IdpBasePage.GetCurrentCultureGroupId();
            _IsUnitedStates = (cultureGroupId == 1);
            _IsJapan = (cultureGroupId == 2);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (_IsUnitedStates)
                {
                    PopulateCountries("US");
                    cmbState.Visible = true ;//= rfvStateBox.Enabled = vceStateBox.Enabled;
                    txtState.Style.Add("display", "none");
                   // rfvStateBox.ControlToValidate = "cmbState";
                }
                else if (_IsJapan)
                {
                    PopulateCountries("JP");
                    cmbState.Visible = true;
                   // vceStateBox.Enabled = rfvStateBox.Enabled = IsStateRequired;
                    txtState.Style.Add("display", "none");
                   // rfvStateBox.ControlToValidate = "cmbState";
                }
                else
                {
                    txtState.Style.Add("display", "block");
                    cmbState.Visible = false;
                  //  vceStateBox.Enabled = rfvStateBox.Enabled = IsStateRequired;
                  //  rfvStateBox.ControlToValidate = "txtState";
                }
            }
           // rfvStateBox.ValidationGroup = ValidationGroup;
            if (_Width > 0)
            {
                txtState.Width = _Width;
                cmbState.Width = _Width;
            }
            if (_IsUnitedStates)
            {
                if (cmbState.Items.Count.Equals(0))
                    PopulateCountries("US");
                cmbState.Visible = true;
                txtState.Style.Add("display", "none");
               // rfvStateBox.ControlToValidate = "cmbState";
            }
            else if (_IsJapan)
            {
                if (cmbState.Items.Count.Equals(0))
                    PopulateCountries("JP");
                cmbState.Visible = true;
                //vceStateBox.Enabled = rfvStateBox.Enabled = IsStateRequired;
                txtState.Style.Add("display", "none");
               // rfvStateBox.ControlToValidate = "cmbState";
            }
            else
            {
                cmbState.Visible = false;
               // rfvStateBox.Enabled = vceStateBox.Enabled = IsStateRequired;
                txtState.Style.Add("display", "block");
             //   rfvStateBox.ControlToValidate = "txtState";
            }
        }

        private void PopulateCountries(string countryCode)
        {
            _states = ISOCountryBLL.State_SelectByCountry(countryCode);
            cmbState.DataSource = _states;
            cmbState.DataTextField = "StateName";
            cmbState.DataValueField = "ISOCode";
            cmbState.DataBind();
            
            var select = new ListItem("---" + GetGlobalResourceObject("Common", "Select") + "---", string.Empty);
            cmbState.Items.Insert(0, select);
        }

        public override string ToString()
        {
            string retString;
            if (_IsUnitedStates || _IsJapan)
            {
                retString = cmbState.SelectedValue;
            }
            else
            {
                retString = txtState.Text;
            }
            return retString;
        }

        public void clearText()
        {
            txtState.Text = string.Empty;
        }

        //Set the state if country is US or JP
        public void setUSState(string stateName)
        {
            cmbState.SelectedIndex = cmbState.Items.IndexOf(cmbState.Items.FindByText(stateName));
        }

        public void setOtherState(string stateName)
        {
            txtState.Text = stateName;
        }

        public void Clear()
        {
            cmbState.Visible = false;
            txtState.Style.Add("display", "block");
            txtState.Text = string.Empty;
        }

        //Remove this later
        public void refreshControl(object sender, EventArgs e)
        {
            Page_Load(sender, e);
            PopulateCountries("US");
        }

        public void refreshControl(object sender, EventArgs e, string country)
        {
            if (!string.IsNullOrEmpty(country))
            {
                if (country.Equals("US"))
                {
                    _IsUnitedStates = true;
                    _IsJapan = false;
                }
                else if (country.Equals("JP"))
                {
                    _IsJapan = true;
                    _IsUnitedStates = false;
                }
            }
            Page_Load(sender, e);
            PopulateCountries(country);
        }

        [Obsolete()]
        public void setState(bool isUnitedStates, string stateName)
        {
            setState(isUnitedStates, false, stateName);
        }

        public void setState(bool isUnitedStates, bool isJapan, string stateName)
        {
            if (isUnitedStates || isJapan)
            {
                if (string.IsNullOrEmpty(stateName))
                {
                    cmbState.SelectedIndex = 0;
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                }
                else if (!string.IsNullOrEmpty(stateName))
                {
                    if (stateName.Length == 2)
                    {
                        if (isUnitedStates)
                        {
                            PopulateCountries("US");
                            cmbState.SelectedValue = stateName;
                        }
                        else if (isJapan)
                        {
                            PopulateCountries("JP");
                            cmbState.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        if (isUnitedStates)
                        {
                            PopulateCountries("US");
                            cmbState.SelectedIndex = 0;
                        }
                        else if (isJapan)
                        {
                            PopulateCountries("JP");
                            cmbState.SelectedValue = stateName;
                        }
                    }
                    //cmbState.SelectedValue = stateName;
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                }
                else
                {
                    for (int i = 0; i < cmbState.Items.Count; i++)
                    {
                        if (cmbState.Items[i].Text.ToLower().Equals(stateName.ToLower()))
                        {
                            cmbState.SelectedIndex = i;
                            txtState.Style.Add("display", "none");
                            cmbState.Visible = true;
                            return;
                        }
                        if (cmbState.Items[i].Value.ToLower().Equals(stateName.ToLower()))
                        {
                            cmbState.SelectedIndex = i;
                            txtState.Style.Add("display", "none");
                            cmbState.Visible = true;
                            return;
                        }
                    }
                    txtState.Style.Add("display", "none");
                    cmbState.Visible = true;
                }
            }
            else
            {
                txtState.Style.Add("display", "block");
                cmbState.Visible = false;
                txtState.Text = stateName;
            }
        }
    }
}