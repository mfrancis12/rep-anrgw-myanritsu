﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcRegistrationfrm.ascx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.App_Controls.UserRegistration.UcRegistrationfrm" %>
<%@ Register TagPrefix="AjaxControlToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="anritsu" Src="~/App_Controls/UcStateControl.ascx"
    TagName="StateControl" %>
<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
   <div id="NewUserBox" class="boxitem" runat="server">
                <!-- ListItem -->
                <div class="boxitemtitle">
                    <div class="boxitemtitleleft">
                    </div>
                    <div class="boxitemtitleright">
                    </div>
                    <div class="boxitemtitletext">
                        Create an Anritsu Account
                    </div>
                </div>
                <div class="boxitembody">
                    <div>
                        The fields indicated with an asterisk(*) are required to complete this transaction,
                        other fields are optional. if you do not want to provide us with the required information,
                        please use the "Back" button on your browser to return to the previous page, or
                        close the window or browser session that is displaying this page.
                    </div>
                    <br />
                    <table>
                        <tr>
                            <td>First Name :*
                            </td>
                            <td>
                                <asp:TextBox ID="FirstName" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="FirstNameValidator" runat="server" ErrorMessage="<%$Resources:ErrorMessage,ErrMsgBlankFirstName%>"
                                    ControlToValidate="FirstName" Display="None" SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="FirstNameValidatorCalloutExtender"
                                    runat="server" TargetControlID="FirstNameValidator" />
                            </td>
                            <td>Last Name :*
                            </td>
                            <td>
                                <asp:TextBox ID="LastName" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="LastNameValidator" runat="server" ErrorMessage="<%$Resources:ErrorMessage,ErrMsgBlankLastName%>"
                                    ControlToValidate="LastName" Display="None" SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="LastNameValidatorCalloutExtender"
                                    runat="server" TargetControlID="LastNameValidator" />
                            </td>
                        </tr>
                        <tr>
                            <td>Job Title :*
                            </td>
                            <td>
                                <asp:TextBox ID="JobTitle" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="JobTitleValidator" runat="server" ControlToValidate="JobTitle"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgJobTitleReq%>" Display="None" SetFocusOnError="true"
                                    ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="JobTitleValidatorCalloutExtender"
                                    runat="server" TargetControlID="JobTitleValidator" />
                            </td>
                            <td>Company :*
                            </td>
                            <td>
                                <asp:TextBox ID="Company" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="CompanyValidator" runat="server" ControlToValidate="Company"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterCompanyValid%>" Display="None"
                                    SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="CompanyValidatorCalloutExtender"
                                    runat="server" TargetControlID="CompanyValidator" />
                            </td>
                        </tr>
                        <tr>
                            <td>Phone :*
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="PhoneNumber" runat="server" MaxLength="20" SkinID="default"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PhoneNumberValidator" runat="server" ControlToValidate="PhoneNumber"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgPhoneReq%>" Display="None" SetFocusOnError="true"
                                    ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="PhoneNumberValidatorCalloutExtender"
                                    runat="server" TargetControlID="PhoneNumberValidator" />
                            </td>
                            <td>Fax Number :
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="FaxNumber" runat="server" MaxLength="50" SkinID="default"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Country :*
                            </td>
                            <td>
                                <asp:DropDownList ID="CountryName" runat="server" SkinID="default" AutoPostBack="True"
                                    OnSelectedIndexChanged="CountryName_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="CountryNameValidator" runat="server" ControlToValidate="CountryName"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterCountryRequired%>" Display="None"
                                    SetFocusOnError="true" InitialValue="Select" ValidationGroup="1"></asp:RequiredFieldValidator>
                            </td>
                            <AjaxControlToolkit:ValidatorCalloutExtender ID="CountryNameValidatorCalloutExtender"
                                runat="server" TargetControlID="CountryNameValidator" />
                            <td>Address1 :*
                            </td>
                            <td>
                                <asp:TextBox ID="Address1" runat="server" MaxLength="1000" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="Address1Validator" runat="server" ControlToValidate="Address1"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgBlankAddress%>" Display="None"
                                    SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="Address1ValidatorCalloutExtender"
                                    runat="server" TargetControlID="Address1Validator" />
                            </td>
                        </tr>
                        <tr>
                            <td>Address2 :
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="Address2" runat="server" MaxLength="1000" SkinID="default"></asp:TextBox>
                            </td>
                            <td>City :*
                            </td>
                            <td>
                                <asp:TextBox ID="City" runat="server" MaxLength="50" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="CityValidator" runat="server" ControlToValidate="City"
                                    ErrorMessage="<%$Resources:ErrorMessage,JobAppCityReq%>" Display="None" SetFocusOnError="true"
                                    ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="CityValidatorCalloutExtender" runat="server"
                                    TargetControlID="CityValidator" />
                            </td>
                        </tr>
                        <tr>
                            <td>State :<asp:Label ID="lblMark" runat="server" Text="*" Visible="false"></asp:Label>
                            </td>
                            <td colspan="2">
                                <anritsu:StateControl ID="State" runat="server" />
                                <td>Postal Code :*
                                </td>
                                <td>
                                    <asp:TextBox ID="PostalCode" runat="server" MaxLength="10" SkinID="default"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="PostalCodeValidator" runat="server" ControlToValidate="PostalCode"
                                        ErrorMessage="<%$Resources:ErrorMessage,ErrMsgBlankPostalCode%>" Display="None"
                                        SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                    <AjaxControlToolkit:ValidatorCalloutExtender ID="PostalCodeValidatorCalloutExtender"
                                        runat="server" TargetControlID="PostalCodeValidator" />
                                    <asp:RegularExpressionValidator ID="PostalCodeRegExpValidator" Enabled="false" runat="server"
                                        ErrorMessage="<%$Resources:ErrorMessage,ErrMsgNumbersPostal%>" Display="None"
                                        SetFocusOnError="true" ValidationExpression="\d{5}(-\d{4})?" ControlToValidate="PostalCode"
                                        ValidationGroup="1" />
                                    <AjaxControlToolkit:ValidatorCalloutExtender ID="PostalCodeValidatorCalloutExtender1"
                                        runat="server" TargetControlID="PostalCodeRegExpValidator" />
                                </td>
                        </tr>
                        <tr>
                            <td>Email :*
                            </td>
                            <td>
                                <asp:TextBox ID="EmailId" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="EmailId"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterEmailRequired%>" Display="None"
                                    SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="EmailValidatorCalloutExtender" runat="server"
                                    TargetControlID="EmailRequired" />
                                <asp:RegularExpressionValidator ID="EmailRegExpValidator" runat="server" ControlToValidate="EmailId"
                                    ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*" Display="None"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgInvalidEmail%>" SetFocusOnError="true"
                                    ValidationGroup="1"></asp:RegularExpressionValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="EmailValidatorCalloutExtender1"
                                    runat="server" TargetControlID="EmailRegExpValidator" />
                            </td>
                            <td>Confirm Email :*
                            </td>
                            <td>
                                <asp:TextBox ID="ConfirmEmail" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="ConfirmEmailRequiredFieldValidator" runat="server"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterEmailRequired%>" Display="None"
                                    SetFocusOnError="true" ControlToValidate="ConfirmEmail" ValidationGroup="1" />
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ConfirmEmailValidatorCalloutExtender"
                                    runat="server" TargetControlID="ConfirmEmailRequiredFieldValidator" />
                                <asp:CompareValidator ID="EmailCompareValidator" ControlToValidate="ConfirmEmail"
                                    ControlToCompare="EmailId" runat="server" ErrorMessage="<%$Resources:ErrorMessage,ErrMsgEmailNotMatch%>"
                                    Display="None" SetFocusOnError="true" ValidationGroup="1"></asp:CompareValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ConfirmEmailValidatorCalloutExtender1"
                                    runat="server" TargetControlID="EmailCompareValidator" />
                            </td>
                        </tr>
                         <tr>
                            <td>Password :*
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="EmailId"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterEmailRequired%>" Display="None"
                                    SetFocusOnError="true" ValidationGroup="1"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                    TargetControlID="EmailRequired" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailId"
                                    ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*" Display="None"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgInvalidEmail%>" SetFocusOnError="true"
                                    ValidationGroup="1"></asp:RegularExpressionValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2"
                                    runat="server" TargetControlID="EmailRegExpValidator" />--%>
                            </td>
                            <td>Confirm Password :*
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" MaxLength="100" SkinID="default"></asp:TextBox>
                            </td>
                            <td>
                              <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                    ErrorMessage="<%$Resources:ErrorMessage,NewsLetterEmailRequired%>" Display="None"
                                    SetFocusOnError="true" ControlToValidate="ConfirmEmail" ValidationGroup="1" />
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3"
                                    runat="server" TargetControlID="ConfirmEmailRequiredFieldValidator" />
                                <asp:CompareValidator ID="CompareValidator1" ControlToValidate="ConfirmEmail"
                                    ControlToCompare="EmailId" runat="server" ErrorMessage="<%$Resources:ErrorMessage,ErrMsgEmailNotMatch%>"
                                    Display="None" SetFocusOnError="true" ValidationGroup="1"></asp:CompareValidator>
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4"
                                    runat="server" TargetControlID="EmailCompareValidator" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td>Login Name :
                            </td>
                            <td>
                                <asp:TextBox ID="LoginName" runat="server" MaxLength="8" SkinID="default"></asp:TextBox>
                            </td>
                            <td colspan="4" width="55%">
                                <asp:RegularExpressionValidator ID="LoginNameRegExpValidator" runat="server"
                                    ErrorMessage="<%$Resources:ErrorMessage,ErrMsgInvalidLoginName%>" Display="None"
                                    SetFocusOnError="true" ValidationExpression="^[a-zA-Z0-9][a-zA-Z0-9-_.]{4,6}[a-zA-Z0-9]$" ControlToValidate="LoginName"
                                    ValidationGroup="1" />
                                <AjaxControlToolkit:ValidatorCalloutExtender ID="LoginNameValidatorCalloutExtender"
                                    runat="server" TargetControlID="LoginNameRegExpValidator" />
                                <asp:Label ID="lblCreateLoginMsg" runat="server" ForeColor="Black" Text="<%$Resources:MyAnritsu,CreateLoginNameMsg%>" Font-Size="11px"></asp:Label><br />
                                <asp:Label ID="lblLoginErrMsg" runat="server" ForeColor="Gray" Text="<%$Resources:MyAnritsu,LoginNameExpMsg%>" Font-Size="11px"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div>
                        <asp:Localize runat="server" ID="Localize16" Text=" Periodically Anritsu sends information to subscribing customers via e-mail "></asp:Localize>
                    </div>
                    <div>
                        <asp:CheckBox ID="ReceiveMails" class="RadioButtonList1" Text=" I wish to receive these messages "
                            Checked="true" runat="server" />
                    </div>
                    <br />
                    <sbk:CaptchaControl ID="captchactrl" LayoutStyle="CssBased" ValidationGroup="1" runat="server" CssClass="captcha" EnableClientScript="false"
                        ErrorMessage="<%$Resources:MyAnritsu, InvalidCode %>" Display="Dynamic" CaptchaLength="4" />
                    <div>
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                    <div class="divcenter">
                        <asp:Button ID="Save" runat="server" Text="<%$Resources:CommonFormStrings,Submit%>"
                            UseSubmitBehavior="false" SkinID="btngreenbg" OnClick="Save_Click" ValidationGroup="1" />
                    </div>
                </div>
                <div class="boxitemfooter">
                    <div>
                        <img src="/images/wdwftrl.gif" alt="" />
                    </div>
                    <div class="movetoright">
                        <img src="/images/wdwftrr.gif" alt="" />
                    </div>
                </div>
            </div>