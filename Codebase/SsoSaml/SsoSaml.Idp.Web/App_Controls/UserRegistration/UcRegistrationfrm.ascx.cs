﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Anritsu.SsoSaml.Idp.Lib.GlobalWebSso;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;

namespace Anritsu.SsoSaml.Idp.Web.App_Controls.UserRegistration
{
    public partial class UcRegistrationfrm : System.Web.UI.UserControl
    {
        private DataTable _countriesList;
        private int _cultureGroupId;

        #region Public Properties

        public StateControl ucStateControl
        {
            get { return State; }
            set { State = value; }
        }

        public DropDownList ucCountryName
        {
            get { return CountryName; }
            set { CountryName = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            PostalCode.MaxLength = 10;
            PhoneNumber.MaxLength = 25;
            _cultureGroupId = IdpBasePage.GetCurrentCultureGroupId();
            State.ValidationGroup = "1";
            if (IsPostBack)
            {
                if (CountryName.SelectedValue.Equals("US"))
                {
                    PostalCodeRegExpValidator.Enabled = State._IsUnitedStates = lblMark.Visible = true;
                    State._IsJapan = false;
                }
                else if (CountryName.SelectedValue.Equals("JP"))
                {
                    PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                    State._IsJapan = true;
                    State._IsUnitedStates = false;
                }
                else
                {
                    PostalCodeRegExpValidator.Enabled = lblMark.Visible = State.IsStateRequired;
                    State._IsJapan = State._IsUnitedStates = false;
                }
            }
            else
            {
                var li = new ListItem(GetGlobalResourceObject("CommonFormStrings", "Select").ToString(), "Select");

                _countriesList = ISOCountryBLL.Country_SelectByCultureGroupId(_cultureGroupId);
                CountryName.DataSource = _countriesList;
                CountryName.DataTextField = "CountryName";
                CountryName.DataValueField = "Alpha2";
                CountryName.DataBind();
                CountryName.Items.Insert(0, li);
                //EmailId.Text = (Request.QueryString[Constants.QueryStringVariables.EmailId] ?? "").HTMLDecode();
                SetCountryBasedOnCultureGroupID();
                CountryName_SelectedIndexChanged(this, e);
            }
        }

        protected void CountryName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Save_Click(object sender, EventArgs e)
        {

        }

        #region Local Methods


        private void SetCountryBasedOnCultureGroupID()
        {
            var countryName = ISOCountryBLL.GetCountryNameForCultureGroupId(IdpBasePage.GetCurrentCultureGroupId());

            var r = _countriesList.Select(string.Format("CountryName = '{0}'", countryName.Replace("'", "''")));
            if (_cultureGroupId == 3) //EMEA
            {
                CountryName.SelectedValue = "Select";
            }
            else
            {
                CountryName.SelectedValue = r.Length == 0 ? "US" : r[0]["Alpha2"].ToString();
            }
            lblMark.Visible = CountryName.SelectedItem.Value.Equals("US") || State.IsStateRequired;
        }

        #endregion
    }
}