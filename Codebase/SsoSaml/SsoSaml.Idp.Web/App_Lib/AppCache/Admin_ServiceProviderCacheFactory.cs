﻿using System;
using System.Web;
using System.Web.Caching;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class Admin_ServiceProviderCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                {
                    _cacheFileDependencyPath =
                        HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/Admin_SpCache.config");
                }
                return _cacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpAdminSp"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            var cacheKey = GetCacheKey(addOnCacheKey);
            var context = HttpContext.Current;
            DataTable tb;
            if (context == null || context.Cache == null)
            {
                tb = GetDataFromDB(addOnCacheKey);
            }
            else
            {
                tb = context.Cache.Get(cacheKey) as DataTable;
                if (tb != null) return tb;
                tb = GetDataFromDB(addOnCacheKey);
                if (tb == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(7);
                context.Cache.Insert(cacheKey, tb,
                    new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
            }
            return tb;
        }

        private DataTable GetDataFromDB(String addOnCacheKey)
        {
            DataTable tb = null;
            switch (addOnCacheKey)
            {
                case "allServiceProviders":
                    tb = Lib.SingleSignOn.GwIdpServiceProviderBLL.SelectAll();
                    break;
                case "allSamlAttributeSet":
                    tb = Lib.SingleSignOn.GwIdpSamlAttributeSetBLL.SelectAll();
                    break;
            }
            return tb;
        }
    }
}