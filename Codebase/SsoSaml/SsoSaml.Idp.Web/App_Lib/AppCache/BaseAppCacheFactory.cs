﻿using System;
using System.Globalization;
using System.Web;
using System.IO;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public abstract class BaseAppCacheFactory
    {
        public abstract string CacheFileDependencyPath { get; }
        public abstract string CacheKeyPrefix { get; }

        public virtual void TouchCacheFile()
        {
            using (var sw = new StreamWriter(CacheFileDependencyPath))
            {
                sw.WriteLine(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
                sw.Close();
            }
        }

        public virtual String GetCacheKey(String addOnCacheKey)
        {
            return String.IsNullOrEmpty(addOnCacheKey)
                ? CacheKeyPrefix
                : String.Format("{0}_{1}", CacheKeyPrefix, addOnCacheKey.Trim());
        }

        public virtual void RemoveItemFromCache(String addOnCacheKey)
        {
            var cacheKey = GetCacheKey(addOnCacheKey);
            if (string.IsNullOrEmpty(cacheKey)) return;
            var context = HttpContext.Current;
            if (context == null || context.Cache == null) return;
            context.Cache.Remove(cacheKey);
        }

        public virtual void RemoveAll()
        {
            TouchCacheFile();
        }

        public abstract Object GetData(String addOnCacheKey);
    }
}