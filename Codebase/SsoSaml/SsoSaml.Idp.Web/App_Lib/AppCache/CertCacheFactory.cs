﻿using System;
using System.Web;
using System.Web.Caching;
using System.Security.Cryptography.X509Certificates;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class CertCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                    _cacheFileDependencyPath =
                        HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/IdpCertCache.config");
                return _cacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpCert"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            throw new NotImplementedException();
        }

        public X509Certificate2 GetCertificatePFX(Int32 certId)
        {
            if (certId < 1) return null;

            var cacheKey = GetCacheKey(String.Format("PFX_{0}", certId));
            var context = HttpContext.Current;
            X509Certificate2 cert = null;
            if (context == null || context.Cache == null)
            {
                cert = Providers.SamlIdentityProvider.SamlUtility.LoadCertificateFromCertID_Pfx(certId);
            }
            else
            {
                cert = context.Cache.Get(cacheKey) as X509Certificate2;
                if (cert != null) return cert;
                cert = Providers.SamlIdentityProvider.SamlUtility.LoadCertificateFromCertID_Pfx(certId);
                if (cert == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                context.Cache.Insert(cacheKey, cert,
                    new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
            }
            return cert;
        }

        public X509Certificate2 GetCertificateCER(Int32 certId)
        {
            if (certId < 1) return null;

            var cacheKey = GetCacheKey(String.Format("CER_{0}", certId));
            var context = HttpContext.Current;
            X509Certificate2 cert;
            if (context == null || context.Cache == null)
            {
                cert = Providers.SamlIdentityProvider.SamlUtility.LoadCertificateFromCertID_Cer(certId);
            }
            else
            {
                cert = context.Cache.Get(cacheKey) as X509Certificate2;
                if (cert != null) return cert;
                cert = Providers.SamlIdentityProvider.SamlUtility.LoadCertificateFromCertID_Cer(certId);
                if (cert == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                context.Cache.Insert(cacheKey, cert,
                    new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
            }
            return cert;
        }
    }
}