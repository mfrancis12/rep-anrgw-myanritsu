﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Data;
using System.Runtime.Caching;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class RedirectUrlProviderCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                    _cacheFileDependencyPath =
                        HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/RedirectUrlsCache.config");
                return _cacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpRedirect"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            DataTable dtRedirectUrls;
            ObjectCache cache = MemoryCache.Default;
            if (cache == null)
            {
                dtRedirectUrls = Lib.UrlRedirects.UrlRedirectBLL.GetAllRedirectUrls();
            }
            else
            {
                dtRedirectUrls = cache.Get(CacheKeyPrefix) as DataTable;
                if (dtRedirectUrls != null) return dtRedirectUrls;
                dtRedirectUrls = Lib.UrlRedirects.UrlRedirectBLL.GetAllRedirectUrls();
                if (dtRedirectUrls == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                //MemoryCache

                var policy = new CacheItemPolicy();
                var filePaths = new List<string> {CacheFileDependencyPath};

                policy.ChangeMonitors.Add(new HostFileChangeMonitor(filePaths));
                policy.AbsoluteExpiration = absExpDate;
                policy.SlidingExpiration = Cache.NoSlidingExpiration;
                cache.Set(CacheKeyPrefix, dtRedirectUrls, policy);
            }
            return dtRedirectUrls;
        }
    }
}