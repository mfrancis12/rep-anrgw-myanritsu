﻿using System;
using System.Web;
using System.Web.Caching;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class ResourceLocaleCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                    _cacheFileDependencyPath =
                        HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/ResLocaleCache.config");
                return _cacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpResLocale"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            var cacheKey = GetCacheKey(addOnCacheKey);
            var context = HttpContext.Current;
            DataTable resLocale;
            if (context == null || context.Cache == null)
            {
                resLocale = AnrCommon.AnrGWResourceProvider.AnrGWResourceLocale.SelectAll();
            }
            else
            {
                resLocale = context.Cache.Get(cacheKey) as DataTable;
                if (resLocale != null) return resLocale;
                resLocale = AnrCommon.AnrGWResourceProvider.AnrGWResourceLocale.SelectAll();
                if (resLocale == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                context.Cache.Insert(cacheKey, resLocale,
                    new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
            }
            return resLocale;
        }
    }
}