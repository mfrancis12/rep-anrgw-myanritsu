﻿using System;
using System.Web;
using System.Web.Caching;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class ServiceProviderCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                    _cacheFileDependencyPath =
                        HttpContext.Current.Server.MapPath("~/App_Data/AppCacheDependencies/SpCache.config");
                return _cacheFileDependencyPath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpSp"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            var spTokenId = ConvertUtility.ConvertToGuid(addOnCacheKey, Guid.Empty);
            if (spTokenId.IsNullOrEmptyGuid()) return null;

            var cacheKey = GetCacheKey(addOnCacheKey);
            var context = HttpContext.Current;
            Lib.SingleSignOn.GwIdpServiceProvider sp;
            if (context == null || context.Cache == null)
            {
                sp = Lib.SingleSignOn.GwIdpServiceProviderBLL.SelectBySpTokenID(spTokenId);
            }
            else
            {
                sp = context.Cache.Get(cacheKey) as Lib.SingleSignOn.GwIdpServiceProvider;
                if (sp != null) return sp;
                sp = Lib.SingleSignOn.GwIdpServiceProviderBLL.SelectBySpTokenID(spTokenId);
                if (sp == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                context.Cache.Insert(cacheKey, sp,
                    new CacheDependency(CacheFileDependencyPath), absExpDate, Cache.NoSlidingExpiration);
            }
            return sp;
        }
    }
}