﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Runtime.Caching;
using System.Xml.Linq;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache
{
    public class WhiteListUrlProviderCacheFactory : BaseAppCacheFactory
    {
        private string _cacheFileDependencyPath = String.Empty;
        private string _sourceFilePath = String.Empty;

        public override string CacheFileDependencyPath
        {
            get
            {
                if (String.IsNullOrEmpty(_cacheFileDependencyPath))
                    _cacheFileDependencyPath = HttpContext.Current.Server.MapPath("~/App_Data/WhitelistUrl.config");
                return _cacheFileDependencyPath;
            }
        }

        public string SourceFilePath
        {
            get
            {
                _sourceFilePath = HttpContext.Current.Server.MapPath("~/App_Data/WhitelistUrl.config");
                return _sourceFilePath;
            }
        }

        public override string CacheKeyPrefix
        {
            get { return "idpWhiteList"; }
        }

        public override object GetData(string addOnCacheKey)
        {
            List<string> hostlist;
            ObjectCache cache = MemoryCache.Default;
            if (cache == null)
            {
                hostlist = GetWhiteListUrl();
            }
            else
            {
                hostlist = cache.Get(CacheKeyPrefix) as List<String>;
                if (hostlist != null) return hostlist;
                hostlist = GetWhiteListUrl();
                if (hostlist == null) return null;
                var absExpDate = DateTime.UtcNow.AddDays(30);
                //MemoryCache

                var policy = new CacheItemPolicy();
                var filePaths = new List<string> {CacheFileDependencyPath};

                policy.ChangeMonitors.Add(new HostFileChangeMonitor(filePaths));
                policy.AbsoluteExpiration = absExpDate;
                policy.SlidingExpiration = Cache.NoSlidingExpiration;
                cache.Set(CacheKeyPrefix, hostlist, policy);
            }
            return hostlist;
        }

        private List<String> GetWhiteListUrl()
        {
            var elements = XElement.Load(SourceFilePath);
            var hostlist = (from c in elements.Elements("add")
                where (string) c.Attribute("Isactive") == "true"
                select c.Attribute("url").Value).ToList<string>();
            return hostlist.Count == 0 ? null : hostlist;
        }
    }
}