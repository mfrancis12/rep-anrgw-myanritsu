﻿using System;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.GoogleAPIs
{
    public class GoogleTranslateSupportedLangCode
    {
        public const String Afrikaans = "af";
        public const String Albanian = "sq";
        public const String Arabic = "ar";
        public const String Azerbaijani = "az";
        public const String Basque = "eu";
        public const String Bengali = "bn";
        public const String Belarusian = "be";
        public const String Bulgarian = "bg";
        public const String Catalan = "ca";
        public const String Chinese_Simplified = "zh-CN";
        public const String Chinese_Traditional = "zh-TW";
        public const String Croatian = "hr";
        public const String Czech = "cs";
        public const String Danish = "da";
        public const String Dutch = "nl";
        public const String English = "en";
        public const String Esperanto = "eo";
        public const String Estonian = "et";
        public const String Filipino = "tl";
        public const String Finnish = "fi";
        public const String French = "fr";
        public const String Galician = "gl";
        public const String Georgian = "ka";
        public const String German = "de";
        public const String Greek = "el";
        public const String Gujarati = "gu";
        public const String Haitian_Creole = "ht";
        public const String Hebrew = "iw";
        public const String Hindi = "hi";
        public const String Hungarian = "hu";
        public const String Icelandic = "is";
        public const String Indonesian = "id";
        public const String Irish = "ga";
        public const String Italian = "it";
        public const String Japanese = "ja";
        public const String Kannada = "kn";
        public const String Korean = "ko";
        public const String Latin = "la";
        public const String Latvian = "lv";
        public const String Lithuanian = "lt";
        public const String Macedonian = "mk";
        public const String Malay = "ms";
        public const String Maltese = "mt";
        public const String Norwegian = "no";
        public const String Persian = "fa";
        public const String Polish = "pl";
        public const String Portuguese = "pt";
        public const String Romanian = "ro";
        public const String Russian = "ru";
        public const String Serbian = "sr";
        public const String Slovak = "sk";
        public const String Slovenian = "sl";
        public const String Spanish = "es";
        public const String Swahili = "sw";
        public const String Swedish = "sv";
        public const String Tamil = "ta";
        public const String Telugu = "te";
        public const String Thai = "th";
        public const String Turkish = "tr";
        public const String Ukrainian = "uk";
        public const String Urdu = "ur";
        public const String Vietnamese = "vi";
        public const String Welsh = "cy";
        public const String Yiddish = "yi";
    }
}