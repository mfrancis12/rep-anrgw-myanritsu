﻿using System;
using System.Web;
using System.Net;
using System.Text;

using System.Json;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.GoogleAPIs
{
    public class GoogleTranslateUtil
    {
        public static String TranslateFromEnglish(String targetLang, String q)
        {
            return Translate(GoogleTranslateSupportedLangCode.English, targetLang, q);
        }

        public static String Translate(String sourceLang, String targetLang, String q)
        {
            if (q.IsNullOrEmptyString()) return String.Empty;
            //else if (q.Length > 5000) return q; //Google Translate API currently only support 5000 chars

            using (var webClient = new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add("X-HTTP-Method-Override", "POST");
                dynamic result =
                    JsonValue.Parse(
                        webClient.DownloadString(BuildGoogleTranslateAPIRequestUrl(sourceLang, targetLang, q)));

                var translatedText = String.Empty;
                if (result != null && result.data != null && result.data.translations.Count > 0)
                {
                    translatedText = result.data.translations[0].translatedText;
                }
                return HttpUtility.HtmlEncode(translatedText);
            }
        }

        private static String BuildGoogleTranslateAPIRequestUrl(String sourceLang, String targetLang,
            String textToTranslate)
        {
            var url = new StringBuilder(ConfigUtility.AppSettingGetValue("GoogleTranslateAPI.EndpointUrl"));
            url.Append("?");
            url.AppendFormat("key={0}", ConfigUtility.AppSettingGetValue("GoogleTranslateAPI.APIKey"));
            url.AppendFormat("&source={0}", sourceLang.Trim());
            url.AppendFormat("&target={0}", targetLang.Trim());
            url.Append("&format=html");
            var cleanedStr = HttpUtility.HtmlDecode(textToTranslate.ConvertNullToEmptyString().Trim());
            url.AppendFormat("&q={0}", HttpUtility.UrlEncode(cleanedStr));
            return url.ToString();
        }
    }
}