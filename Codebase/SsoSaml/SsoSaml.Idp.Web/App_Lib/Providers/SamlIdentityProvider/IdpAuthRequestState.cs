﻿using System;
using System.Xml;
using Atp.Saml2;
namespace Anritsu.SsoSaml.Idp.Web.App_Lib.Providers.SamlIdentityProvider
{
    [Serializable]
    public class IdpAuthRequestState
    {
        public Lib.SingleSignOn.GwIdpServiceProvider ServiceProviderInfo { get; set; }
        public String AuthnRequestXmlDoc { get; set; }
        public String RelayState { get; set; }
        public String AssertionConsumerServiceURL { get; set; }

        public IdpAuthRequestState() { }

        public IdpAuthRequestState(Lib.SingleSignOn.GwIdpServiceProvider sp, AuthnRequest authnRequest, String relayState) 
        {
            RelayState = relayState;
            ServiceProviderInfo = sp;
            if (sp != null) AssertionConsumerServiceURL = sp.SpAcsUrl;
            if (authnRequest == null) return;
            var xmlAuthn = authnRequest.GetXml();
            if (xmlAuthn != null && xmlAuthn.OwnerDocument != null)
            {
                AuthnRequestXmlDoc = authnRequest.GetXml().OwnerDocument.OuterXml;
            }
            AssertionConsumerServiceURL = authnRequest.AssertionConsumerServiceUrl;
        }

        public IdpAuthRequestState(Lib.SingleSignOn.GwIdpServiceProvider sp, String relayState)
        {
            ServiceProviderInfo = sp;
            RelayState = relayState;
            if (sp != null) AssertionConsumerServiceURL = sp.SpAcsUrl;
        }
    }
}