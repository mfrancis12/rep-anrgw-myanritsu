﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Data;
using Atp.Saml;
using Atp.Saml.Binding;
using Atp.Saml2;
using Atp.Saml2.Binding;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.Providers.SamlIdentityProvider
{
    internal class SamlUtility
    {
        internal static X509Certificate2 LoadCertificateFromCertID_Cer(Int32 certId)
        {
            var row = Lib.SingleSignOn.SamlCertStoreBLL.Select(certId);
            if (row == null) return null;
            var certData = (Byte[]) row["x509Cer"];
            return new X509Certificate2(certData);
        }

        internal static X509Certificate2 LoadCertificateFromCertID_Pfx(Int32 certId)
        {
            var row = Lib.SingleSignOn.SamlCertStoreBLL.Select(certId);
            if (row == null) return null;
            var certData = (Byte[]) row["x509Pfx"];
            var pfxPwd = row["x509PfxPwd"].ToString();
            return new X509Certificate2(certData, pfxPwd, X509KeyStorageFlags.MachineKeySet);
        }

        internal static AuthnRequest CreateAuthnRequest_ForHttpPostBinding(HttpRequest req)
        {
            if (req == null) return null;
            var authnRequest = AuthnRequest.CreateFromHttpPost(req);
            return authnRequest;
        }

        internal static Lib.SingleSignOn.GwIdpServiceProvider GetDefaultServiceProvider()
        {
            var spCache = new AppCache.ServiceProviderCacheFactory();
            return
                spCache.GetData(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID")) as
                    Lib.SingleSignOn.GwIdpServiceProvider;
        }

        internal static Lib.SingleSignOn.GwIdpServiceProvider GetServiceProvider(Guid spTokenId)
        {
            if (spTokenId.IsNullOrEmptyGuid()) return null;
            var spCache = new AppCache.ServiceProviderCacheFactory();
            return spCache.GetData(spTokenId.ToString()) as Lib.SingleSignOn.GwIdpServiceProvider;
        }

        /// <summary>
        /// Process SAML request
        /// </summary>
        /// <param name="spInfo">Service Provider Information</param>
        /// <param name="authnRequest"></param>
        /// <param name="relayState"></param>
        public static void ProcessAuthnRequest(Lib.SingleSignOn.GwIdpServiceProvider spInfo,
            out AuthnRequest authnRequest,
            out string relayState)
        {
            authnRequest = null;
            relayState = null;

            var context = HttpContext.Current;

            if (spInfo == null || spInfo.ServiceProviderID < 1 || context == null) return;
            var certCache = new AppCache.CertCacheFactory();
            switch (spInfo.SamlBindingSpToIdp)
            {
                    #region " init authnRequest "

                case SamlBindingUri.HttpRedirect:
                    var x509Certificate = certCache.GetCertificateCER(spInfo.CertID);
                    authnRequest = AuthnRequest.Create(context.Request.RawUrl, x509Certificate.PublicKey.Key);
                    relayState = authnRequest.RelayState;
                    return;

                case SamlBindingUri.HttpPost:
                    authnRequest = AuthnRequest.CreateFromHttpPost(context.Request);
                    relayState = authnRequest.RelayState;
                    break;

                    //case SamlBindingUri.HttpArtifact:                    
                    //    //Saml2ArtifactType0004 httpArtifact = Saml2ArtifactType0004.CreateFromHttpArtifactQueryString(context.Request);
                    //    //// Create an artifact resolve request.
                    //    //ArtifactResolve artifactResolve = new ArtifactResolve();
                    //    //artifactResolve.Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/"));
                    //    //artifactResolve.Artifact = new Artifact(httpArtifact.ToString());
                    //    //ArtifactResponse artifactResponse = ArtifactResponse.SendSamlMessageReceiveAftifactResponse(spInfo.SpArtifactUrl, artifactResolve);

                    //    //// Extract the authentication request from the received artifact response.
                    //    //authnRequest = new AuthnRequest(artifactResponse.Message);
                    //    //relayState = httpArtifact.RelayState;
                    //    break;

                default:
                    authnRequest = null;
                    relayState = null;
                    throw new NotImplementedException();

                    #endregion
            }

            if (authnRequest.IsSigned())
            {
                // Get the loaded certificate.
                var x509Certificate = certCache.GetCertificateCER(spInfo.CertID);
                // And validate the authentication request with the certificate.
                if (!authnRequest.Validate(x509Certificate))
                {
                    throw new ApplicationException("The authentication request signature failed to verify.");
                }
            }
        }

        public static Response CreateSaml2Response(
            Lib.SingleSignOn.GwIdpServiceProvider spInfo,
            DataRow userData,
            AuthnRequest authnRequest,
            bool rememberme)
        {
            var context = HttpContext.Current;
            if (context == null) return null;

            var samlResponse = new Response();
            var issuerUrl = WebUtility.GetAbsoluteUrl("~/");
            samlResponse.Issuer = new Issuer(issuerUrl);

            var assertionConsumerServiceUrl = spInfo.SpAcsUrl;
            if (authnRequest != null)
            {
                if (authnRequest.NameIdPolicy != null)
                    ;

                if (!authnRequest.AssertionConsumerServiceUrl.IsNullOrEmptyString())
                    assertionConsumerServiceUrl = authnRequest.AssertionConsumerServiceUrl;
            }

            //if (context.User.Identity.IsAuthenticated) //&& nameIdPolicy_AllowCreate)//|| !nameIdPolicy_AllowCreate) 
            if (userData == null)
            {
                samlResponse.Status = new Status(SamlPrimaryStatusCode.Responder, SamlSecondaryStatusCode.AuthnFailed,
                    "The user is not authenticated at the identity provider");
            }
            else
            {
                samlResponse.Status = new Status(SamlPrimaryStatusCode.Success, null);

                var samlAssertion = new Assertion
                {
                    Issuer = samlResponse.Issuer,
                    Conditions = new Conditions(new TimeSpan(0, 10, 0))
                };
                var audienceRestriction = new AudienceRestriction();
                if (spInfo.Audiences.Count == 0)
                    audienceRestriction.Audiences.Add(new Audience(assertionConsumerServiceUrl));
                else
                    SetAudienceList(ref audienceRestriction, spInfo.Audiences);
                samlAssertion.Conditions.ConditionsList.Add(audienceRestriction);
                var subject = new Subject(new NameId(HttpContext.Current.User.Identity.Name));
                var subjectConfirmation = new SubjectConfirmation(SamlSubjectConfirmationMethod.Bearer);
                var subjectConfirmationData = new SubjectConfirmationData();
                //if (authnRequest != null)
                //{
                //    subjectConfirmationData.InResponseTo = authnRequest.Id;
                //}
                subjectConfirmationData.Recipient = assertionConsumerServiceUrl;
                //"NotBefore" is not required. fix for salesforce sso
                //subjectConfirmationData.NotBefore = samlAssertion.Conditions.NotBefore;
                subjectConfirmationData.NotOnOrAfter = samlAssertion.Conditions.NotOnOrAfter;

                subjectConfirmation.SubjectConfirmationData = subjectConfirmationData;
                subject.SubjectConfirmations.Add(subjectConfirmation);
                samlAssertion.Subject = subject;

                var authnStatement = new AuthnStatement
                {
                    AuthnContext =
                        new AuthnContext
                        {
                            AuthnContextClassRef = new AuthnContextClassRef(SamlAuthenticateContext.Password)
                        }
                };
                samlAssertion.Statements.Add(authnStatement);
                //custom attributes, user properties
                var attStmt = GenerateSamlAttributes(spInfo, userData);
                //add remember me attribute.
                attStmt.Attributes.Add(new Atp.Saml2.Attribute("urn:oid:anrsso:rememberme",
                    "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", "rememberme", rememberme.ToString()));
                samlAssertion.Statements.Add(attStmt);
                //If SAML Assertion requires Signing.
                if (spInfo.RequiredSignedAssertions)
                {
                    var certCache = new AppCache.CertCacheFactory();
                    var x509Certificate = certCache.GetCertificatePFX(
                        ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("Idp.Web.Cert.IdpCertID"), 100));
                    samlAssertion.Sign(x509Certificate);
                }
                samlResponse.Assertions.Add(samlAssertion);
            }

            return samlResponse;
        }

        private static void SetAudienceList(ref AudienceRestriction audienceRestriction, List<String> spAudiences)
        {
            foreach (var s in spAudiences)
            {
                audienceRestriction.Audiences.Add(new Audience(s));
            }
        }

        public static void SendSaml2Response(Response samlResponse,
            IdpAuthRequestState ssoState, String wreply, String mkt)
        {
            Trace.Write("SendSaml2Response:Starts");
            if (samlResponse == null
                || ssoState == null
                || ssoState.ServiceProviderInfo == null
                || ssoState.ServiceProviderInfo.SpTokenID.IsNullOrEmptyGuid())
            {
                throw new HttpException(404, "Page not found.");
            }

            var certCache = new AppCache.CertCacheFactory();

            Trace.Write("SendSaml2Response:Sign the SAML response.");

            var x509Certificate = certCache.GetCertificatePFX(
                ConvertUtility.ConvertToInt32(ConfigUtility.AppSettingGetValue("Idp.Web.Cert.IdpCertID"), 100));
            samlResponse.Sign(x509Certificate);

            var spAcs = ssoState.AssertionConsumerServiceURL;
            var acsurl = new Uri(spAcs);
            var qswreply = HttpUtility.ParseQueryString(acsurl.Query).Get(UI.KeyDef.ParmKeys.ReplyUrl);
            var qsmkt = HttpUtility.ParseQueryString(acsurl.Query).Get(UI.KeyDef.ParmKeys.SsoIDP.MarketLocale);

            var addOnQs = new StringBuilder();
            if (!wreply.IsNullOrEmptyString() && qswreply.IsNullOrEmptyString())
                addOnQs.AppendFormat("&{0}={1}", UI.KeyDef.ParmKeys.ReplyUrl, wreply);
            if (!mkt.IsNullOrEmptyString() && qsmkt.IsNullOrEmptyString())
                addOnQs.AppendFormat("&{0}={1}", UI.KeyDef.ParmKeys.SsoIDP.MarketLocale, mkt.Trim());
            if (addOnQs.Length > 0)
            {
                var qs = addOnQs.ToString();
                if (spAcs.Contains("?"))
                {
                    if (spAcs.EndsWith("&")) spAcs += qs.Substring(1);
                    else spAcs += qs;
                }
                else
                {
                    spAcs += "?" + qs.Substring(1);
                }
            }

            var idpProtocolBinding = SamlBindingUri.UriToBinding(ssoState.ServiceProviderInfo.SamlBindingIdpToSp);
            switch (idpProtocolBinding)
            {
                case SamlBinding.HttpPost:
                    Trace.Write("SendSaml2Response:SendPostBindingForm");
                    samlResponse.SendPostBindingForm(HttpContext.Current.Response.OutputStream, spAcs,
                        ssoState.RelayState);
                    break;

                case SamlBinding.HttpArtifact:
                    var identificationUrl = WebUtility.GetAbsoluteUrl("~/");
                    var httpArtifact = new Saml2ArtifactType0004(SamlArtifact.GetSourceId(identificationUrl)
                        , SamlArtifact.GetHandle());

                    // Cache the authentication request for subsequent sending using the artifact resolution protocol. Sliding expiration time is 1 hour.
                    SamlSettings.CacheProvider.Insert(httpArtifact.ToString(), samlResponse.GetXml()
                        , new TimeSpan(1, 0, 0));

                    Trace.Write("SendSaml2Response:SendPostForm using artifact");
                    httpArtifact.SendPostForm(HttpContext.Current.Response.OutputStream, spAcs,
                        ssoState.RelayState);
                    break;

                default:
                    Trace.Write("IdentityProvider", "Invalid identity provider binding");
                    throw new NotImplementedException();
            }
        }

        public static AttributeStatement GenerateSamlAttributes(Lib.SingleSignOn.GwIdpServiceProvider spInfo,
            DataRow userData)
        {
            if (spInfo == null || userData == null) return null;
            var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
            var attStmt = new AttributeStatement();
            var attributes = spInfo.SamlAttributes;
            if (attributes == null) return attStmt;
            attributes =
                attributes.OrderBy(src => src.SrcTable)
                    .ThenBy(o => o.AttributeOrder)
                    .ThenBy(n => n.AttributeName)
                    .ToList();
            foreach (var att in attributes)
            {
                var attributename = (spInfo.RequiredQualifiedNameUrn) ? att.AttributeNameUrn : att.AttributeName;
                switch (att.SrcTable)
                {
                    case Lib.SingleSignOn.GwIdpSamlAttribute_SrcTable.GW_SSO_USER:

                        attStmt.Attributes.Add(new Atp.Saml2.Attribute(attributename,
                            att.AttributeNameFormatUrn,
                            att.AttributeFriendlyName,
                            GetValueByAttributeNameFromUser(userData, att.ColumnName)));
                        break;
                    case Lib.SingleSignOn.GwIdpSamlAttribute_SrcTable.GW_SSO_ROLEONLY:
                        GenerateAttributesForGWRole(att, ref attStmt, gwUserId, attributename);
                        break;
                    case Lib.SingleSignOn.GwIdpSamlAttribute_SrcTable.GW_SSO_ROLE_AND_PERMISSIONS:
                        GenerateAttributesForGWRoleAndPermissions(att, ref attStmt, gwUserId, attributename);
                        break;
                    case Lib.SingleSignOn.GwIdpSamlAttribute_SrcTable.GW_SSO_LOGINHISTORY:
                        GenerateAttributesForLoginHistory(att, ref attStmt, gwUserId, attributename);
                        break;
                    case Lib.SingleSignOn.GwIdpSamlAttribute_SrcTable.IDP_QueryString:
                        GenerateAttributesFromQueryString(att, ref attStmt);
                        break;
                }
            }
            return attStmt;
        }

        private static void GenerateAttributesFromQueryString(Lib.SingleSignOn.GwIdpSamlAttributeSet_Attribute att,
            ref AttributeStatement attStmt)
        {
            var qsvalue = HttpContext.Current.Request.QueryString[att.ColumnName];
            if (!string.IsNullOrEmpty(qsvalue))
                attStmt.Attributes.Add(new Atp.Saml2.Attribute(att.ColumnName, SamlAttributeNameFormat.Unspecified, null,
                    qsvalue));
        }


        private static String GetValueByAttributeNameFromUser(DataRow userData, String columnName)
        {
            if (userData == null || columnName.IsNullOrEmptyString()) return String.Empty;
            var multicols = columnName.Split('~');
            if (multicols.Length > 1)
            {
                return GetConcatinatedColumnValuesByAttributeNameFromUser(userData, multicols);
            }
            return !userData.Table.Columns.Contains(columnName)
                ? String.Empty
                : ConvertUtility.ConvertNullToEmptyString(userData[columnName]).Trim();
        }

        private static String GetConcatinatedColumnValuesByAttributeNameFromUser(DataRow userData, String[] columnNames)
        {
            var attributeValue = string.Empty;
            if (userData == null || columnNames.Length == 0) return String.Empty;
            return columnNames.Aggregate(attributeValue,
                (current, colname) =>
                    current + (ConvertUtility.ConvertNullToEmptyString(userData[colname]).Trim() + " "));
        }

        private static void GenerateAttributesForGWRole(Lib.SingleSignOn.GwIdpSamlAttributeSet_Attribute att,
            ref AttributeStatement attStmt, Int32 gwUserId, string attributeName)
        {
            if (attStmt == null || att == null) return;

            var tb = Lib.SingleSignOn.GwIdpUserRoleAndPermissionBLL.UserRole_Select(gwUserId);
            if (tb == null) return;
            foreach (DataRow r in tb.Rows)
            {
                var roleId = r["UserRoleId"].ToString().ToUpperInvariant();
                //include all or specified role(s) in the format of role_251AD8C9-0D76-403E-9F1F-932E4C9FD5C4
                if (att.ColumnName != "all" && att.ColumnName != String.Format("role_{0}", roleId)) continue;
                var at = new Atp.Saml2.Attribute(attributeName,
                    att.AttributeNameFormatUrn,
                    att.AttributeFriendlyName);
                at.Values.Add(new AttributeValue(String.Format("GWROLE~{0}", roleId)));
                at.Values.Add(
                    new AttributeValue(String.Format("GWROLETITLE~{0}",
                        r["UserRoleTitle"].ToString().MakeAlphaNumericOnly("_"))));
                attStmt.Attributes.Add(at);
            }
        }

        private static void GenerateAttributesForGWRoleAndPermissions(
            Lib.SingleSignOn.GwIdpSamlAttributeSet_Attribute att,
            ref AttributeStatement attStmt, Int32 gwUserId, string attributeName)
        {
            if (attStmt == null || att == null) return;

            var tbRoles = Lib.SingleSignOn.GwIdpUserRoleAndPermissionBLL.UserRole_Select(gwUserId);
            if (tbRoles == null) return;
            foreach (DataRow roleR in tbRoles.Rows)
            {
                var userRoleId = ConvertUtility.ConvertToGuid(roleR["UserRoleId"], Guid.Empty);
                var roleId = roleR["UserRoleId"].ToString().ToUpperInvariant();
                var roleTitle = roleR["UserRoleTitle"].ToString().MakeAlphaNumericOnly("_");

                //include all or specified role(s) in the format of role_251AD8C9-0D76-403E-9F1F-932E4C9FD5C4
                if (att.ColumnName != "all" && att.ColumnName != String.Format("role_{0}", roleId)) continue;
                var tbPer = Lib.SingleSignOn.GwIdpUserRoleAndPermissionBLL.UserPermissionToken_Select(userRoleId);
                //do not add if role does not have permission
                if (tbPer == null) continue;
                var at = new Atp.Saml2.Attribute(attributeName,
                    att.AttributeNameFormatUrn,
                    att.AttributeFriendlyName);
                at.Values.Add(new AttributeValue(String.Format("GWROLE~{0}", roleId)));
                at.Values.Add(new AttributeValue(String.Format("GWROLETITLE~{0}", roleTitle)));
                foreach (DataRow perR in tbPer.Rows)
                {
                    at.Values.Add(new AttributeValue(String.Format("GWPERTOKEN~{0}|PERTITLE~{1}"
                        , perR["PermissionToken"].ToString().ToUpperInvariant()
                        , perR["TokenTitle"].ToString().MakeAlphaNumericOnly("_")
                        )));
                }
                attStmt.Attributes.Add(at);
            }
        }

        private static void GenerateAttributesForLoginHistory(Lib.SingleSignOn.GwIdpSamlAttributeSet_Attribute att,
            ref AttributeStatement attStmt, Int32 gwUserId, string attributename)
        {
            if (attStmt == null || att == null) return;
            var count = ConvertUtility.ConvertToInt32(att.ColumnName, 3);
            var tb = Lib.SingleSignOn.GwIdpUserLoginHistoryBLL.Select(gwUserId, count);
            if (tb == null) return;
            foreach (DataRow h in tb.Rows)
            {
                var dt = ConvertUtility.ConvertToDateTime(h["LoginUTCDate"], DateTime.MinValue);

                var at = new Atp.Saml2.Attribute(attributename,
                    att.AttributeNameFormatUrn,
                    att.AttributeFriendlyName);
                at.Values.Add(new AttributeValue(String.Format("LoggedInDate~{0}", dt.ToString("U"))));
                at.Values.Add(new AttributeValue(String.Format("IP~{0}", h["IPAddress"])));
                attStmt.Attributes.Add(at);
            }
        }
    }
}