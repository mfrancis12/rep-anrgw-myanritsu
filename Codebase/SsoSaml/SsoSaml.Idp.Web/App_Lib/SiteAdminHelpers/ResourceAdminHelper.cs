﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.GoogleAPIs;
using AnrGWRes = Anritsu.AnrCommon.AnrGWResourceProvider;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers
{
    internal static class ResourceAdminHelper
    {
        public static void ResAdmin_SearchOptionsSet(List<SearchByOption> searchOptions)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return;
            HttpContext.Current.Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchOptions] = searchOptions;
        }

        public static List<SearchByOption> ResAdmin_SearchOptionsGet(Boolean reset)
        {
            if (HttpContext.Current == null || HttpContext.Current.Session == null) return null;
            var opt =
                HttpContext.Current.Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchOptions] as
                    List<SearchByOption>;
            if (!reset) return opt;
            opt = new List<SearchByOption>();
            ResAdmin_SearchOptionsSet(opt);
            return opt;
        }

        public static DataTable SelectFiltered(List<SearchByOption> searchOptions)
        {
            var classKey = String.Empty;
            var resKey = String.Empty;
            var locale = String.Empty;
            var contentKeyword = String.Empty;

            if (searchOptions == null)
                return AnrGWRes.AnrGWResourceAdmin.SelectFiltered(classKey, resKey, locale, contentKeyword);
            foreach (var sbo in searchOptions)
            {
                switch (sbo.SearchByField)
                {
                    case "ClassKey":
                        classKey = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "ResKey":
                        resKey = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "Locale":
                        locale = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                    case "ContentKeyword":
                        contentKeyword = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                        break;
                }
            }
            return AnrGWRes.AnrGWResourceAdmin.SelectFiltered(classKey, resKey, locale, contentKeyword);
        }

        public static void AutoTranslate(DataRow defaultEnglishContent)
        {
            if (defaultEnglishContent == null) return;
            var tb = AnrGWRes.AnrGWResourceLocale.SelectAll();
            foreach (DataRow r in tb.Rows)
            {
                var lang = r["Lang"].ToString();
                var targetLocale = new CultureInfo(r["Locale"].ToString());
                if (targetLocale.Name == "en") continue;

                var defaultEngClassKey = ConvertUtility.ConvertNullToEmptyString(defaultEnglishContent["ClassKey"]);
                var defaultEngResKey = ConvertUtility.ConvertNullToEmptyString(defaultEnglishContent["ResourceKey"]);
                var defaultContent = ConvertUtility.ConvertNullToEmptyString(defaultEnglishContent["ContentStr"]);
                var c = AnrGWRes.AnrGWResourceAdmin.Select(defaultEngClassKey, defaultEngResKey, targetLocale.Name);
                if (c == null)
                {
                    if (lang == "en") continue;
                    var translatedText = GoogleTranslateUtil.TranslateFromEnglish(lang, defaultContent);
                    AnrGWRes.AnrGWResourceAdmin.Insert(defaultEngClassKey, defaultEngResKey, targetLocale.Name,
                        translatedText, false);
                }
                else
                {
                    var contentId = ConvertUtility.ConvertToInt32(c["ContentID"], 0);
                    var allowAutoTranslate = ConvertUtility.ConvertToBoolean(c["AllowAutoTranslate"], false);
                    if (lang == "en")
                    {
                        if (allowAutoTranslate)
                        {
                            AnrGWRes.AnrGWResourceAdmin.Delete(contentId);
                        }
                    }
                    else
                    {
                        if (!allowAutoTranslate) continue;
                        var translatedText = GoogleTranslateUtil.TranslateFromEnglish(lang, defaultContent);
                        AnrGWRes.AnrGWResourceAdmin.Update(contentId, translatedText, allowAutoTranslate, false);
                    }
                }
            }
        }
    }
}