﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers
{
    [Serializable]
    public class SearchByOption
    {
        public String SearchByField { get; set; }
        public object SearchObject { get; set; }

        public SearchByOption() { }
        public SearchByOption(String searchByField, object searchObject)
        {
            SearchByField = searchByField;
            SearchObject = searchObject;
        }

        public static object GetObjectByField(String searchByField, List<SearchByOption> lst)
        {
            var o = from sbo in lst
                    where sbo.SearchByField.Equals(searchByField, StringComparison.InvariantCultureIgnoreCase)
                    select sbo;
            return !o.Any() ? null : o.FirstOrDefault();
        }
    }
}