﻿using System;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib
{
    [Serializable]
    public class SsoUserType
    {
        private int _userTypeIdField;
        private string _userTypeField;
        private bool _isUserProfileField;
        private string _descriptionField;

        /// <remarks/>
        public int UserTypeId
        {
            get { return _userTypeIdField; }
            set { _userTypeIdField = value; }
        }

        /// <remarks/>
        public string UserType
        {
            get { return _userTypeField; }
            set { _userTypeField = value; }
        }

        /// <remarks/>
        public bool IsUserProfile
        {
            get { return _isUserProfileField; }
            set { _isUserProfileField = value; }
        }

        /// <remarks/>
        public string Description
        {
            get { return _descriptionField; }
            set { _descriptionField = value; }
        }
    }
}