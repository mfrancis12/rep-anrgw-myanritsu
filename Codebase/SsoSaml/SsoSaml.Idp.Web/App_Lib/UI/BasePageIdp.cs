﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using Anritsu.SsoSaml.Idp.Web.App_Lib.Providers.SamlIdentityProvider;
using System.Web.Security;
using Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache;
using BLL = Anritsu.SsoSaml.Idp.Lib;
using SDK = Anritsu.AnrCommon.EmailSDK;
using System.Configuration;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.UI
{
    public class IdpBasePage : System.Web.UI.Page
    {
        public String SpResKeyPrefix
        {
            get
            {
                var ssoState = Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] as IdpAuthRequestState;
                if (ssoState != null && ssoState.ServiceProviderInfo != null)
                    return ssoState.ServiceProviderInfo.SpResKeyPrefix;

                //if ssoState is null, try get it from Service Provider token or Default SP
                var sp = ServiceProviderInfo;
                return sp == null
                    ? ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpResKeyPrefix")
                    : sp.SpResKeyPrefix;
            }
        }

        public Lib.SingleSignOn.GwIdpServiceProvider ServiceProviderInfo
        {
            get
            {
                var ssoState = Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] as IdpAuthRequestState;
                if (ssoState != null && ssoState.ServiceProviderInfo != null) return ssoState.ServiceProviderInfo;

                var defaultSpTokenId =
                    ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID"),
                        Guid.Empty);
                var spTokenId = ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID],
                    defaultSpTokenId);
                var spCache = new ServiceProviderCacheFactory();
                return spCache.GetData(spTokenId.ToString()) as Lib.SingleSignOn.GwIdpServiceProvider;
            }
        }

        public String ReturnURL
        {
            get
            {
                var returnUrl = Request[KeyDef.ParmKeys.ReturnURL].IsNullOrEmptyString()
                    ? Request[KeyDef.ParmKeys.ReplyUrl]
                    : Request[KeyDef.ParmKeys.ReturnURL];

                return GetRedirectUrl(returnUrl);
            }
        }

        public Guid ServiceProviderToken
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID], Guid.Empty);
            }
        }

        public Boolean IsSamlRequest
        {
            get { return !Request[KeyDef.ParmKeys.SsoIDP.SAMLRequest].IsNullOrEmptyString(); }
        }

        public Lib.SingleSignOn.GwIdpServiceProvider GetServiceProviderInfo(Boolean returnDefaultIfEmpty)
        {
            var spCache = new ServiceProviderCacheFactory();
            var sp = spCache.GetData(ServiceProviderToken.ToString()) as Lib.SingleSignOn.GwIdpServiceProvider;
            if (sp != null || !returnDefaultIfEmpty) return sp;
            var defaultSpTokenId =
                ConvertUtility.ConvertToGuid(ConfigUtility.AppSettingGetValue("Idp.Web.DefaultSP.SpTokenID"), Guid.Empty);
            sp = spCache.GetData(defaultSpTokenId.ToString()) as Lib.SingleSignOn.GwIdpServiceProvider;
            return sp;
        }

        protected override void InitializeCulture()
        {
            UIHelper.InitLocale();
            base.InitializeCulture();
            UIHelper.InitMarketLocale();
        }

        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            base.OnInit(e);
        }

        public String GetResourceStringForRelyingParty(String resKey)
        {
            return GetResourceString("IdpSignInSPCustom", resKey);
        }

        public String GetResourceString(String classKey, String resourceKey)
        {
            if (classKey.IsNullOrEmptyString() || resourceKey.IsNullOrEmptyString()) return String.Empty;
            return ConvertUtility.ConvertNullToEmptyString(this.GetGlobalResourceObject(classKey, resourceKey));
        }

        public DataRow RefreshUserInfo(Boolean signOutIfNull)
        {
            Trace.Write("RefreshUserInfo:Starts");
            var userDataRefresh = UIHelper.LoggedInUser_Get(false);
            var ipAddress = WebUtility.GetUserIP();
            if (!User.Identity.IsAuthenticated || userDataRefresh != null) return userDataRefresh;
            Trace.Write("RefreshUserInfo:userDataRefresh is null.  Refresh from database.");
            userDataRefresh = Lib.SingleSignOn.GwIdpUserBLL.SelectBySecretKeyForAuthenticatedUser(
                ConvertUtility.ConvertToGuid(User.Identity.Name, Guid.Empty),
                GetServiceProviderInfo(true).ServiceProviderID,
                ipAddress);

            if (userDataRefresh == null && signOutIfNull)
            {
                FormsAuthentication.SignOut();
            }
            UIHelper.LoggedInUser_Set(userDataRefresh);
            return userDataRefresh;
        }

        public void DoFallBackAction()
        {
            FormsAuthentication.SignOut();
            //Session.Abandon();
            UIHelper.LoggedInUser_Set(null); //reset user data
            Response.Redirect(FormsAuthentication.LoginUrl);
            Response.End();
        }

        public void OverwritePageTitleGreen(String pageTitleText)
        {
            if (pageTitleText.IsNullOrEmptyString()) return;
            var mp = Master as App_MasterPages.MasterPage1Col;
            //mp.GreenPageTitleCtrl.GreenPageTitleText = pageTitleText;
        }

        public void OverwritePageTitleBrowser(String browserTitleText)
        {
            if (browserTitleText.IsNullOrEmptyString()) return;
            var mp = Master as App_MasterPages.MasterPage1Col;
            mp.BrowserTitleCtrl.BrowserTitleText = browserTitleText;
        }

        public static bool IsWhiteListHost(string hostName)
        {
            var isWhiteListUrl = false;
            try
            {
                var provider = new WhiteListUrlProviderCacheFactory();

                var hostlist = provider.GetData("idpWhiteList") as List<String>;
                if (hostlist != null && hostlist.Count > 0)
                {
                    isWhiteListUrl = hostlist.Contains(hostName);
                }
            }
            catch (Exception)
            {
            }
            return isWhiteListUrl;
        }

        public static int GetCurrentCultureGroupId()
        {
            var lang = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString(); //GetCurrentLanguage();
            var groupId = BLL.Utility.CultureUtilityBLL.GetCultureGroupIdForCultureCode(lang);
            return groupId;
        }

        public static int GetCurrentCultureGroupId(string lang)
        {
            if (string.IsNullOrEmpty(lang))
                lang = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString(); //GetCurrentLanguage();
            var groupId = BLL.Utility.CultureUtilityBLL.GetCultureGroupIdForCultureCode(lang);
            return groupId;
        }
        private static int GenerateRandomNumber()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
           return  rand.Next(100000, 999999);
        }
        public static string SendActivationCodeEmail(string emailorlogin,string requestedBy, string wereplyurl=null)
        {


            var ds = BLL.SingleSignOn.GwIdpUserBLL.GetValidationCode(emailorlogin, GenerateRandomNumber(), requestedBy, wereplyurl);
            var validationcode = "";
            var firstname = "";
            var lastname = "";
            var emailaddress = "";
            int sendEmailcount = 0;
            if (ds.Tables.Count != 0)
            {
                validationcode = ds.Tables[0].Rows[0]["ValidationCode"].ToString();
                firstname= ds.Tables[0].Rows[0]["FirstName"].ToString();
                lastname= ds.Tables[0].Rows[0]["LastName"].ToString();
                emailaddress= ds.Tables[0].Rows[0]["EmailAddress"].ToString();
                sendEmailcount = int.Parse( ds.Tables[0].Rows[0]["SendEmailCount"].ToString());
            }
            if (string.IsNullOrEmpty(emailaddress) == false && sendEmailcount <= 5)
            {
                #region "Send email using EmailAPI "

                var req = new SendTemplatedEmailCallRequest
                {
                    CultureGroupId = GetCurrentCultureGroupId(),
                    EmailTemplateKey = KeyDef.EmailTemplates.ANRITSUID_ACTIVATION_USER_EMAIL,
                    ToEmailAddresses = new[] { emailaddress.Trim() }
                };

                #region " subject replacement values "

                var subjR = new List<ReplaceKeyValueType>();
                req.SubjectReplacementKeyValues = subjR.ToArray();

                #endregion

                #region " body replacement values "

                var bodyR = new List<ReplaceKeyValueType>();
                var returnUrl = HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.ReplyUrl];

                bodyR.Add(new ReplaceKeyValueType() { ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]" });
                bodyR.Add(new ReplaceKeyValueType() { ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]" });
                bodyR.Add(new ReplaceKeyValueType() { ReplacementValue = validationcode.Trim(), TemplateKey = "[[VALIDATIONCODE]]" });
                var keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[ANRITSUID_ACTIVATION_USER_URL]]",
                    ReplacementValue = ConfigurationManager.AppSettings["ANRITSUID_ACTIVATION_USER_URL"]
                        .Replace("[[VALIDATIONCODE]]", validationcode.ToString().ToUpper())
                        .Replace("[[SPTOKEN]]", HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID])
                        .Replace("[[RETURNURL]]", returnUrl)
                        .Replace("[[LL-CC]]", UIHelper.GetCulture())
                };
                bodyR.Add(keyValue);

                keyValue = new ReplaceKeyValueType
                {
                    TemplateKey = "[[CONTACTUSLINK]]",
                    ReplacementValue = ConfigurationManager.AppSettings["Idp.Web.ContactUsUrl"]
                };
                bodyR.Add(keyValue);
                req.BodyReplacementKeyValues = bodyR.ToArray();

                #endregion

                // Send an email with Email service
                var res = SDK.EmailServiceManager.SendTemplatedEmail(req);
            }
            return emailaddress;

            #endregion
        }
        public static int SendActivationLinkEmail(string email, string firstname, string lastname, Guid usertoken)
        {
            #region "Send email using EmailAPI "

            var req = new SendTemplatedEmailCallRequest
            {
                CultureGroupId = GetCurrentCultureGroupId(),
                EmailTemplateKey = KeyDef.EmailTemplates.ANRITSUID_ACTIVATION_USER_EMAIL,
                ToEmailAddresses = new[] {email.Trim()}
            };

            #region " subject replacement values "

            var subjR = new List<ReplaceKeyValueType>();
            req.SubjectReplacementKeyValues = subjR.ToArray();

            #endregion

            #region " body replacement values "

            var bodyR = new List<ReplaceKeyValueType>();
            var returnUrl = HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.ReplyUrl];

            bodyR.Add(new ReplaceKeyValueType() {ReplacementValue = firstname.Trim(), TemplateKey = "[[FIRSTNAME]]"});
            bodyR.Add(new ReplaceKeyValueType() {ReplacementValue = lastname.Trim(), TemplateKey = "[[LASTNAME]]"});

            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[ANRITSUID_ACTIVATION_USER_URL]]",
                ReplacementValue = ConfigurationManager.AppSettings["ANRITSUID_ACTIVATION_USER_URL"]
                    .Replace("[[USERREGISTRATIONTOKEN]]", usertoken.ToString().ToUpper())
                    .Replace("[[SPTOKEN]]", HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID])
                    .Replace("[[RETURNURL]]", returnUrl)
                    .Replace("[[LL-CC]]", UIHelper.GetCulture())
            };
            bodyR.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[CONTACTUSLINK]]",
                ReplacementValue = ConfigurationManager.AppSettings["Idp.Web.ContactUsUrl"]
            };
            bodyR.Add(keyValue);
            req.BodyReplacementKeyValues = bodyR.ToArray();

            #endregion

            // Send an email with Email service
            var res = SDK.EmailServiceManager.SendTemplatedEmail(req);
            return res.StatusCode;

            #endregion
        }


        public String ReplyURL
        {
            get
            {
                var replyUrl = Request[KeyDef.ParmKeys.ReplyUrl];
                return GetRedirectUrl(replyUrl);
            }
        }

        public String DefaultReturnUrl()
        {
            var defKey=!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PUB"].ToString())?ConfigurationManager.AppSettings["PUB"].ToString():"en-US";
            var marketLocale = UIHelper.InitMarketLocale();
            var returnUrl = ConfigUtility.AppSettingGetValue("Idp.Web.HomeUrl");
            returnUrl = marketLocale.IsNullOrEmptyString()
                ? returnUrl.Replace("/[[LL-CC]]", defKey)
                : returnUrl.Replace("[[LL-CC]]", marketLocale);
            return returnUrl;
        }

        public String GetRedirectUrl(string url)
        {
            return GetRedirectUrl(url, DefaultReturnUrl());
        }

        public String GetRedirectUrl(string url, string defaultUrl)
        {
            if (url.IsNullOrEmptyString()) return defaultUrl;
            url = HttpUtility.UrlDecode(UIHelper.SplitString(url, ','));
            return !IsWhiteListUrl(url) ? defaultUrl : url;
        }

        public bool IsWhiteListUrl(string url)
        {
            var isWhiteListUrl = true;
            if ((url.StartsWith("/") || url.StartsWith("~/")) && !url.StartsWith("//")) return true;

            url = url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("//") ? url : "http://" + url;
            if (!IsWhiteListHost(new Uri(url).Host))
                isWhiteListUrl = false;

            return isWhiteListUrl;
        }
    }
}