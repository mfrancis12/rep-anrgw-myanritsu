﻿namespace Anritsu.SsoSaml.Idp.Web.App_Lib.UI
{
    public class BasePageSiteAdminIdp : System.Web.UI.Page
    {
        protected override void InitializeCulture()
        {
            UIHelper.InitLocale();
            base.InitializeCulture();
            UIHelper.InitMarketLocale();
        }
    }
}