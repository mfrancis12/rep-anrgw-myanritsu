﻿using System;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
namespace Anritsu.SsoSaml.Idp.Web.App_Lib.UI
{
    public class BasePageSsoIdp : System.Web.UI.Page
    {
        public Guid ServiceProviderTokenID
        {
            get
            {
                return ConvertUtility.ConvertToGuid( Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID], Guid.Empty );
            }
        }

        public Boolean IsSamlRequest
        {
            get
            {
                return !Request[KeyDef.ParmKeys.SsoIDP.SAMLRequest].IsNullOrEmptyString();
            }
        }

        public void DoFallBackAction()
        {
            FormsAuthentication.SignOut();
            //Session.Abandon();
            UIHelper.LoggedInUser_Set(null);//reset user data
            Response.Redirect(FormsAuthentication.LoginUrl);
            Response.End();
        }

        protected override void InitializeCulture()
        {
            UIHelper.InitLocale();
            base.InitializeCulture();
            UIHelper.InitMarketLocale();
        }
    }
}