﻿using System;

namespace Anritsu.SsoSaml.Idp.Web.App_Lib.UI
{
    internal static class KeyDef
    {
        public class ParmKeys
        {
            public const String SiteLocale = "lang";
            public const String ReturnURL = "ReturnURL";
            public const String ReplyUrl = "wreply";
            public const String LogoutStatus = "idplgo";

            public class SsoIDP
            {
                //public const String EndpointBinding = "binding"; //Endpoint binding.
                public const String SpTokenID = "sptkn"; //Service Provider / Relying Party unique identifier
                public const String MarketLocale = "mkt"; //region code to overide for customizations
                public const String SiteLocale = "lang"; // content locale
                public const String SAMLRequest = "SAMLRequest";
            }

            public class SiteAdmin
            {
                public class ResourceAdmin
                {
                    public const String Search_Auto = "srhmd";
                    public const String Search_PreClassKey = "srhclass";
                    public const String Search_PreResKey = "srhreskey";
                    public const String Search_ContentID = "contentid";
                }

                public class SpAdmin
                {
                    public const String x509_CertID = "certid";
                }
            }

            public class CommonParams
            {
                public const String EmailId = "EmailId";
                public const String FTK = "ftk";

                //----------------------for registrationKEY--------------------------
                public const String RTK = "rtk";
                public const String IsNew = "isNew";
                public const String Resend = "rsnd";
            }
        }

        public class SessionKeys
        {
            //public const String SiteLocale = "AnrLocale";
            public const String SiteMarket = "AnrMkt";
            public const String LoggedInUser = "idpLoggedInUser";
            public const String CultureGroupId = "CultureGroupId";

            public class LoginAtIDP
            {
                public const String AuthRequestState = "idpAuthReq";
            }

            public class SiteAdmin
            {
                public class ResourceAdmin
                {
                    public const string Search_SearchOptions = "ssResAdminSearchOptions";
                    public const string Search_SearchResults = "ssResAdminSearchResults";
                }
            }

            //New User Regsistriaton
            public const String NewUserAccountInformation = "NewUserAccountInformation";
        }

        public class CacheKeys
        {

        }

        public class UrlList
        {
            public const String SignIn = "~/signin";

            public class SiteAdmin
            {
                public class ResourceAdmin
                {
                    public const String SearchResults = "~/siteadmin/resourceadmin/resadminsearchresults";
                    public const String SearchDetails = "~/siteadmin/resourceadmin/resadmindetail";
                }
            }
        }

        public static class EmailTemplates
        {
            public const string CONTACT_GLOBALWEB_REQUEST = "CONTACT-GLOBALWEB-REQUEST";
            public const string USER_FORGOT_PWD = "USER-FORGOT-PWD";
            public const string ANRITSUID_ACTIVATION_USER_EMAIL = "ANRITSUID-ACTIVATION-USER-EMAIL";
            public const string ChangeEmailVerificationUserEmail = "CHANGE-EMAIL-VERIFICATION-NEW-EMAIL";
            public const string ChangeEmailConfirmationEmail = "CHANGE-EMAIL-REQUEST-OLD-EMAIL";
            public const string ChangeEmailRequestAdminEmail = "CHANGE-EMAIL-REQUEST-ADMIN-EMAIL";
        }

        // -------------------for new Registration  ----------------------
        public static class ApplicationVariables
        {
            public const string SSOUserType = "visitor";
        }
    }

    public static class RegFromFieldIds
    {
        public const string FirstName = "FirstName";
        public const string FirstName_In_Roman = "FirstNameInRoman";
        public const string LastName = "LastName";
        public const string LastName_In_Roman = "LastNameInRoman";
        public const string Company = "Company";
        public const string Company_In_Roman = "CompanyInRoman";
        public const string JobTitle = "JobTitle";
        public const string Phone = "Phone";
        public const string FaxNumber = "FaxNumber";
        public const string Address1 = "Address1";
        public const string Address2 = "Address2";
        public const string City = "City";
        public const string State = "State";
        public const string PostalCode = "PostalCode";
        public const string CountryName = "CountryName";
        public const string Email = "Email";
        public const string ConfirmEmail = "ConfirmEmail";
        public const string Password = "Password";
        public const string ConfirmPassword = "ConfirmPassword";
        public const string LoginName = "LoginName";
    }
}