﻿using System;
using System.Threading;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;
using Anritsu.AnrCommon.CoreLib;


namespace Anritsu.SsoSaml.Idp.Web.App_Lib.UI
{
    internal static class UIHelper
    {
        public static string GetPageUrl()
        {
            if (HttpContext.Current == null) return null;
            var relUrl = "~" + HttpContext.Current.Request.FilePath;
            return relUrl;
        }

        public static DataRow LoggedInUser_Get(bool refresh)
        {
            var context = HttpContext.Current;
            if (context == null) return null;

            var tbUser = context.Session[KeyDef.SessionKeys.LoggedInUser] as DataTable;

            if (tbUser != null && tbUser.Rows.Count == 1 && !refresh) return tbUser.Rows[0];
            var secretKey = ConvertUtility.ConvertToGuid(context.User.Identity.Name, Guid.Empty);
            if (secretKey.IsNullOrEmptyGuid()) return null;

            var userInfoFromDb = Lib.SingleSignOn.GwIdpUserBLL.SelectBySecretKey(secretKey);
            if (userInfoFromDb == null) return null;
            tbUser = userInfoFromDb.Table;
            context.Session[KeyDef.SessionKeys.LoggedInUser] = tbUser;

            return tbUser.Rows[0];
        }

        public static void LoggedInUser_Set(DataRow userData)
        {
            var context = HttpContext.Current;
            if (context == null || context.Session == null) return;

            if (!context.User.Identity.IsAuthenticated)
            {
                context.Session[KeyDef.SessionKeys.LoggedInUser] = null; //not logged in yet so reset
                return;
            }
            if (userData != null)
            {
                context.Session[KeyDef.SessionKeys.LoggedInUser] = userData.Table;
            }
        }

        public static void InitLocale()
        {
            var context = HttpContext.Current;
            if (context == null) return;
            var langInQsToOverwrite = context.Request.QueryString[KeyDef.ParmKeys.SsoIDP.SiteLocale];
            if (langInQsToOverwrite.IsNullOrEmptyString()) return;
            var ciInQs = ProcessCulture(langInQsToOverwrite);

            if (ciInQs == null) return;
            Thread.CurrentThread.CurrentUICulture = ciInQs;
            Thread.CurrentThread.CurrentCulture = ciInQs;
        }

        public static String InitMarketLocale()
        {
            var context = HttpContext.Current;
            if (context == null) return String.Empty;
            var mktInQsToOverwrite = context.Request.QueryString[KeyDef.ParmKeys.SsoIDP.MarketLocale];
            if (!mktInQsToOverwrite.IsNullOrEmptyString())
            {
                var ciMktInQs = ProcessCulture(mktInQsToOverwrite);
                if (ciMktInQs != null)
                {
                    if (context.Session != null) context.Session[KeyDef.SessionKeys.SiteMarket] = ciMktInQs.Name;
                    return ciMktInQs.Name;
                }
            }

            var mkt = String.Empty;
            if (context.Session != null)
                mkt = ConvertUtility.ConvertNullToEmptyString(context.Session[KeyDef.SessionKeys.SiteMarket]);
            return mkt;
        }

        public static string GetCulture()
        {
            return HttpContext.Current.Request.QueryString[KeyDef.ParmKeys.SsoIDP.SiteLocale]
                   ?? Thread.CurrentThread.CurrentUICulture.ToString();
        }

        public static CultureInfo ProcessCulture(string cultureInfo)
        {
            const string defaultCulture = "en-US";

            CultureInfo cInfo = null;
            try
            {
                cInfo = new CultureInfo(SplitString(cultureInfo, ','));
            }
            catch
            {
                //set it to default culture
                cInfo = new CultureInfo(defaultCulture);
            }

            return cInfo;
        }

        public static string SplitString(string value, char delimiter)
        {
            return value.Contains(delimiter) ? value.Split(delimiter)[0] : value;
        }

        public static string GetLocaleDefaultMapping(string locale)
        {
            if (string.IsNullOrEmpty(locale)) return null;
            if (locale.Contains('-')) return locale;
            switch (locale.ToLower())
            {
                case "ja" :
                    return "ja-JP";
                case "en":
                    return "en-Us";
                case "zh":
                    return "zh-CN";
                default:
                    return "en-Us";
            }
        }
    }
}