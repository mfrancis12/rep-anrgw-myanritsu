﻿using System;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Lib.Utility;

namespace Anritsu.SsoSaml.Idp.Web.App_MasterPages
{
    public partial class MasterPage1Col : System.Web.UI.MasterPage
    {
        //public App_Controls.CustomWebControls.PageTitlePanel GreenPageTitleCtrl { get { return pt; } }
        public App_Controls.CustomWebControls.PageTitlePanel BrowserTitleCtrl { get { return ptBrowserTitle; } }
        /// <summary>
        /// Check country code belongs to EMEA region
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        private bool IsEMEARegion(string countryCode)
        {
            bool Emearegion = false;
            
            if (GeoIPBLL.GetRegionByCountryCode(countryCode) == "EMEA")
            {
                Emearegion = true;
            }

            return Emearegion;
        }

        /// <summary>
        /// Get country code from Current culture
        /// </summary>
        /// <returns></returns>
        private string countryCodeFromBrowser()
        {
            //Get the current culture from browser
            string browserLang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
            string lang = (browserLang == null ? "en-US" : browserLang);
            string browserLangCountryCode = string.Empty;
           
            if (!string.IsNullOrEmpty(lang))
            {
                if (lang.IndexOf('-') > 0)
                {
                    browserLangCountryCode = lang.Substring(3, 2).ToUpper();
                }
                else
                {
                    browserLangCountryCode = lang.ToUpper();
                }
            }
            return browserLangCountryCode;
        }
        /// <summary>
        /// Get country code from IP address
        /// </summary>
        /// <returns></returns>
        private string countryCodeFromIPAddress()
        {
            string ipAddressCountryCode = string.Empty;
            string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"].ToString();
            System.Data.DataRow dr = GeoIPBLL.GetCountryByIP(ipAddress);
            ipAddressCountryCode = (null != dr) ? dr["ISOCountryCode"].ToString() : string.Empty;
            return ipAddressCountryCode;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 // Check with browser lang
                if (IsEMEARegion(countryCodeFromBrowser()))
                {
                    cookieconsent.Visible = true;
                }//Check with Ip address
                else if(IsEMEARegion(countryCodeFromIPAddress()))
                {
                  cookieconsent.Visible = true;
                }
              
            }
            catch(Exception ex)
            {
                cookieconsent.Visible = false;
            }

        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigUtility.AppSettingGetValue("AWSBucketName")); }
        }
    }
}