﻿using System;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.App_MasterPages
{
    public partial class MasterPageSiteDefault : System.Web.UI.MasterPage
    {
        public App_Controls.CustomWebControls.PageTitlePanel GreenPageTitleCtrl { get { return pt; } }
        public App_Controls.CustomWebControls.PageTitlePanel BrowserTitleCtrl { get { return ptBrowserTitle; } }
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string CDNPath
        {
            get { return string.Format("//{0}", ConfigUtility.AppSettingGetValue("AWSBucketName")); }
        }
    }
}