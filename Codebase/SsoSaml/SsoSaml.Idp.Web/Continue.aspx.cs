﻿using System;
using System.Web;
using System.Data;
using System.Xml;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.Providers.SamlIdentityProvider;
using Atp.Saml2;

namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class Continue : IdpBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            Response.Cache.SetNoTransforms();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                var userData = RefreshUserInfo(true);
                DoSamlResponse(userData);
            }
            else
            {
                Trace.Write("DoSamlResponse:User is not authenticated.  Redirect to signin.");
                RedirectToSignIn();
            }
        }

        private void DoSamlResponse(DataRow userData)
        {
            var rememberme = false;
            Trace.Write("DoSamlResponse:Starts");
            if (userData == null)
            {
                Trace.Write("DoSamlResponse:userData is null.  Redirect to signin");
                RedirectToSignIn();
                return;
            }

            Lib.SingleSignOn.GwIdpServiceProvider sp = null;
            var logParms = String.Empty;
            XmlElement logSamlNode = null;
            var logError = String.Empty;
            try
            {
                var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
                var userIp = WebUtility.GetUserIP();
                AuthnRequest authReq = null;
                var ssoState = Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] as IdpAuthRequestState;

                if (ssoState == null)
                {
                    Trace.Write("DoSamlResponse:ssoState is null.  redirecting to returnURL " + ReturnURL);
                    Response.Redirect(ReturnURL, true);
                    Response.End();
                }


                if (ssoState.AuthnRequestXmlDoc.IsNullOrEmptyString())
                    Trace.Write("DoSamlResponse:AuthnRequestXmlDoc is null or empty.");
                else
                {
                    Trace.Write("DoSamlResponse:load AuthnRequestXmlDoc from ssoState xml.");
                    var xDocAuth = new XmlDocument();
                    xDocAuth.LoadXml(ssoState.AuthnRequestXmlDoc);
                    authReq = new AuthnRequest(xDocAuth.DocumentElement);
                }
                sp = ssoState.ServiceProviderInfo;
                if (sp == null)
                {
                    Trace.Write("DoSamlResponse:sp is null in ssoState.  Get it from cache and use default if null.");
                    sp = GetServiceProviderInfo(true);
                }
                Trace.Write("DoSamlResponse:CreateSaml2Response");
                if (null != Session["Rememberme"])
                    rememberme = Convert.ToBoolean(Session["Rememberme"]);
                var samlResponse = SamlUtility.CreateSaml2Response(sp, userData, authReq, rememberme);
                Trace.Write("DoSamlResponse:Update user is online status to db.");
                Lib.SingleSignOn.GwIdpUserActivityBLL.UpdateOnline(gwUserId, sp.ServiceProviderID, userIp);

                if (samlResponse != null) logSamlNode = samlResponse.GetXml();
                Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] = null; //reset session

                //if (ssoState == null) ssoState = new IdpAuthRequestState(sp, String.Empty);
                Trace.Write("DoSamlResponse:Log_LoginSamlResponse");
                Lib.SingleSignOn.GwIdpSamlLogBLL.Log_LoginSamlResponse(sp.SpTokenID.ToString(), logParms, logSamlNode,
                    logError);

                if (ReturnURL.StartsWith("/") || ReturnURL.StartsWith("~/"))
                {
                    Trace.Write("DoSamlResponse:Do local redirect to " + ReturnURL);
                    Response.Redirect(ReturnURL);
                }
                else
                {
                    Trace.Write("DoSamlResponse:SendSaml2Response");
                    var wreply =
                        HttpUtility.UrlEncode(
                            Request.QueryString[KeyDef.ParmKeys.ReplyUrl].ConvertNullToEmptyString().Trim());
                    var mkt = HttpUtility.UrlEncode(UIHelper.InitMarketLocale().ConvertNullToEmptyString().Trim());
                    SamlUtility.SendSaml2Response(samlResponse, ssoState, wreply, mkt);
                }
                Response.End(); //important
            }
            catch (ArgumentException aex)
            {
                Trace.Write("DoSamlResponse:Argument Exp: " + aex.Message);
                logError = aex.Message;
                Lib.SingleSignOn.GwIdpSamlLogBLL.Log_LoginSamlResponse(sp.SpTokenID.ToString(), logParms, logSamlNode,
                    logError);
                DoFallBackAction();
            }
        }

        private void RedirectToSignIn()
        {
            Trace.Write("RedirectToSignIn");
            GetServiceProviderInfo(true);
            var url = String.Format("{0}?{1}", FormsAuthentication.LoginUrl, Request.QueryString);
            Response.Redirect(url, true);
        }
    }
}