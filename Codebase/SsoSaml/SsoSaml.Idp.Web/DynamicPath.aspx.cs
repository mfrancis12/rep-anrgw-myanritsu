﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class DynamicPath : System.Web.UI.Page
    {
        public string RelativePath
        {
            get
            {
                return Convert.ToString(Request.QueryString["rel"]);
            }
        }

        public string CdnPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["StaticCdn"]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.Write(string.Format("{0}{1}", CdnPath, RelativePath));
            Response.End();
        }
    }
}