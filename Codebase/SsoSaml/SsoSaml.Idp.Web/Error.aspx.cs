﻿using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using System;

namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }
        protected override void InitializeCulture()
        {
            UIHelper.InitLocale();
            base.InitializeCulture();
            UIHelper.InitMarketLocale();
        }
    }
}