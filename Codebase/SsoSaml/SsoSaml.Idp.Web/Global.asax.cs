﻿using System;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Net.Security;
using System.Web.Routing;
using System.IO;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Lib.UrlRedirects;
using Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache;
using System.Data;
using System.Text.RegularExpressions;

namespace Anritsu.SsoSaml.Idp.Web
{
    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            if (ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("Idp.Web.Cert.IsSelfSigned"), false))
            {
                // Set server certificate callback.
                ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteServerCertificate;
            }
        }

        /// <summary>
        /// Verifies the remote Secure Sockets Layer (SSL) certificate used for authentication.
        /// </summary>
        /// <param name="sender">An object that contains state information for this validation.</param>
        /// <param name="certificate">The certificate used to authenticate the remote party.</param>
        /// <param name="chain">The chain of certificate authorities associated with the remote certificate.</param>
        /// <param name="sslPolicyErrors">One or more errors associated with the remote certificate.</param>
        /// <returns>A System.Boolean value that determines whether the specified certificate is accepted for authentication.</returns>
        private static bool ValidateRemoteServerCertificate(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            // NOTE: set it to true for test application with self-signed certificates, so all certificates are trusted.
            return ConvertUtility.ConvertToBoolean(ConfigUtility.AppSettingGetValue("Idp.Web.Cert.IsSelfSigned"), false);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Path.Equals("/default.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = "/signin" + Request.Url.Query;
                Response.End();
            }
            else if (Request.Path.EndsWith(".aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.RedirectLocation = Request.RawUrl.Replace(".aspx", string.Empty).ToLowerInvariant();
                Response.End();
            }

            // redirect urls
            InvokeUrlRedirect();
        }

        private void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        private void Application_Error(object sender, EventArgs e)
        {
            ErrorLogUtility.LogError("SsoSaml", Request.Url.Host,
                Server.GetLastError(), HttpContext.Current);
            //Server.ClearError();
        }

        private void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        private void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            var appRootPath = HttpContext.Current.Server.MapPath("~/");
            var rootInfo = new DirectoryInfo(appRootPath);
            var fiAspx = rootInfo.GetFiles("*.aspx", SearchOption.AllDirectories);
            foreach (var fi in fiAspx)
            {
                var webPath = fi.FullName.Replace(appRootPath, string.Empty).Replace("\\", "/");
                var routeUrl = webPath.Replace(".aspx", string.Empty);
                var physicalFile = "~/" + webPath;
                routes.MapPageRoute(routeUrl, routeUrl, physicalFile, false);
            }
        }

        private void InvokeUrlRedirect()
        {
            var originalUrl = Context.Request.Url.ToString();
            if (originalUrl.IsNullOrEmptyString()) return;
            var filepath = originalUrl.Substring(originalUrl.LastIndexOf("/", StringComparison.Ordinal) + 1);
            var redirectPrefix =
                ConvertUtility.ConvertToString(ConfigUtility.AppSettingGetValue("Idp.Web.RedirectPrefix"), "redir_");
            var match = Regex.Match(filepath, redirectPrefix);
            if (!match.Success) return;
            var redirectUrlsCache = new RedirectUrlProviderCacheFactory();
            var dtRedirecUrls = redirectUrlsCache.GetData("idpRedirect") as DataTable;
            UrlRedirect.RedirectUrl(HttpContext.Current, dtRedirecUrls);
        }
    }
}
