﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPage1Col.Master" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SignIn"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>--%>

<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
    <style type="text/css">
    .sso-main-container .contentsarea.container a.button.heading-button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 center;
        border: 2px solid #2b7690;
        color: #2b7690;
        font-size: 14px;
        font-weight: 700;
        margin-right: 10px;
        padding: 4px 12px;
    }
   .sso-main-container .contentsarea.container a.button.heading-button:hover {
        border: 2px solid #44bde9;
        color: #44bde9;
    }
</style>
    <input style="display:none" />
	<input type="password" style="display:none" />
    <div class="settingrow">
    <div class='signin_container'>
        <div class="row login-form">
             
             <div class="colrgt login-form-detail" >
                  
                 <asp:Panel ID="pllogin" runat="server">
                       <div class="heading">
                            <div id="divNewUser" runat="server" >
                             <asp:Localize ID="lcalAboutApp" runat="server" Text=""></asp:Localize> 
                         </div>
                              <h2><asp:Localize ID="lcalSignInLegend" runat="server" Text="<%$Resources:~/signin,lcalSignInLegend.Text %>"></asp:Localize></h2>
                     </div>
                      <div class="form-container">
                          
                   
                     <div class="bd">
                           <h3><span id="spanverifywelcomemessage" runat="server"   style="color:#2F977A">
                          <asp:Localize ID="verificationwelcomemsg" runat="server" Visible="false" Text=""></asp:Localize>
                        </span></h3>
                              <div class="group input-text required">
                             <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" Text="<%$Resources:~/signin,UserNameLabel.Text %>"></asp:Label>
                             <%--<p class="error-message"><asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="<%$Resources:~/signin,UserNameRequired.ErrorMessage %>" Display="Dynamic" ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></p>--%>
                             <p class="error-message"><asp:Localize ID="Localize1" runat="server" Text="<%$Resources:~/signin,UserNameRequired.ErrorMessage %>"></asp:Localize></p>
                             <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                         </div>
                         <div class="group input-password required">
                             <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" Text="<%$Resources:~/signin,PasswordLabel.Text %>"></asp:Label>
                             <%--<p class="error-message"><asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="<%$Resources:~/signin,PasswordRequired.ErrorMessage %>" ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></p>--%>
                             <p class="error-message"><asp:Localize ID="Localize2" runat="server" Text="<%$Resources:~/signin,PasswordRequired.ErrorMessage %>"></asp:Localize></p>
                             <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                         </div>
                         <div class="group input-checkbox borderTop">
                             <p class="no-margin">
                                 <label for="">
                                     <asp:CheckBox ID="RememberMe" runat="server"/>
                                     <span><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:~/signin,RememberMeLabel.Text%>" /></span>
                                 </label>
                             </p>
                         </div>
                         
                    <div class='settingrow' style='padding-top: 10px;'>
                        <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" ValidationGroup="LoginUserValidationGroup"/>
                        <span class="msg server-error-message"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></span>
                    </div>
                        
                         <div class="group">
                          <div class='btnrow'>
                             <div class='btncollft'>
                                  <div class="settingrow" style="padding-top: 10px!important;">
                                 <asp:Button ID="LoginButton" runat="server" 
                                     Text="<%$Resources:~/signin,LoginButton.Text %>" OnClick="LoginButton_Click"
                                       CssClass="button form-submit margin-right-15" />
                                       <asp:HyperLink ID="hlForgotPwd" runat="server" Text='<%$Resources:~/signin,hlForgotPwd.Text %>' Target="_blank" NavigateUrl="https://www.anritsu.com/My-Anritsu/Forgot-Password.aspx"></asp:HyperLink>
                                     
                             </div>
                                
                             </div>
                     </div>
                              <div class='clearfloat'></div>
                        </div>
                        </br>
                            <asp:HyperLink ID="SignUpLink" runat="server" CssClass="button heading-button" Text='<%$Resources:~/signin,hlSignUp.Text %>' Target="_blank" NavigateUrl="/account/new-user-registration"></asp:HyperLink>
                            <asp:HyperLink ID="hlHelp" runat="server" Text='<%$Resources:~/signin,hlHelp.Text %>' Target="_blank" NavigateUrl="https://www.anritsu.com/Contact-Us/Web-Master.aspx" CssClass="inline-block "></asp:HyperLink>
                        
                 </div>
                <div id="divLoginCtrl">
                
                </div>
             </div>    
            </asp:Panel>
                
                 <asp:Panel runat="server" Visible="false" ID="plverification">
                         <div class="form-container">
                     <div class="heading">
                          <h2><asp:Localize ID="localverificationheader" runat="server" Text='<%$Resources:~/account/verification,lcalverificationheader.Label %>'></asp:Localize></h2>
                     </div>
                             <div>
                                 <h4><asp:Localize ID="Localize3" runat="server"   Text='<%$Resources:~/account/verification,lcalDisclaimer.Text %>'></asp:Localize></h4>
                             </div>
                              <h3>  <span id="spanmessgae" runat="server" style="color:#2F977A">
                        <asp:Localize ID="lcalErrorMsg"  runat="server" visible="false"></asp:Localize>     
                         </span></h3>
                   
                             <div id="divverification" runat="server">
                     <div class="bd">
                       
                             <div class="group input-text">
                              <p class="error-message"><asp:Localize ID="validationmsg" runat="server" Text='<%$Resources:~/account/verification,lcalverificationvalidation.Msg %>'></asp:Localize></p>
                              
                             <asp:TextBox ID="txtVerification" placeholder='<%$Resources:~/account/verification,lcalverificationvalidationtextboxplaceholder.Text %>'   MaxLength="6"  runat="server"></asp:TextBox>
                         </div>
                        
                         
                         
                    <div class='settingrow' style='padding-top: 10px;'>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="LoginUserValidationGroup"/>
                        
                    </div>
                        
                         <div class="group">
                          <div class='btnrow'>
                             <div class='btncollft'>
                                  <div class="settingrow" style="padding-top: 10px!important;">
                                 <asp:Button ID="btnVerify" runat="server" 
                                     Text='<%$Resources:~/account/verification,lcalverificationverifybutton.Text %>'
                                       CssClass="button form-submit margin-right-15" OnClick="btnVerify_Click" />
                                      
                                     <%-- <asp:HyperLink ID="HyperLink1" runat="server" Text='<%$Resources:~/signin,hlForgotPwd.Text %>' Target="_blank" NavigateUrl="https://www.anritsu.com/My-Anritsu/Forgot-Password.aspx"></asp:HyperLink>--%>
                             </div>
                                
                             </div>
                     </div>
                    <div class='clearfloat'></div>
                </div>                  
                          <asp:LinkButton ID="btnresend" runat="server" Text='<%$Resources:~/account/verification,lcalverificationresend.Text %>'   CssClass="inline-block margin-top-15" OnClick="btnresend_Click"> </asp:LinkButton>&nbsp; &nbsp;
                          <span  style="color:#289b7d;font-size:14px;" id="resendspanmessage" runat="server"><asp:Literal ID="lcalResendMessage" runat="server"></asp:Literal></span>
                            <br/>    
                          <br/>  
                         <asp:HyperLink ID="lnkhelp" runat="server" Text='<%$Resources:~/signin,hlHelp.Text %>' Target="_blank" NavigateUrl="https://login.anritsu.com/contact-us" CssClass="inline-block "></asp:HyperLink>
                        
                 </div>
                                 </div>
                <div id="divLoginCtrl">
                
                </div>
             </div>    
                 </asp:Panel> 
        </div>

    

             <div class="collft login-info">
              <h1><asp:Localize ID="Localize4" runat="server"   Text='<%$Resources:~/signin,leftheader.Text %>'></asp:Localize></h1>
                <%--<h2>Create an account to access the resources that matter most to you and your organization.</h2>
                <ul>
                  <li> <img src="http://placehold.it/80x50" alt="img-thumb"/><span>Lorem ipsum dolor sit amet to adipiscing elit euismod bibendum laoreet.</span></li>
                  <li> <img src="http://placehold.it/80x50" alt="img-thumb"/><span>Lorem ipsum dolor sit amet to adipiscing elit euismod bibendum laoreet.</span></li>
                  <li> <img src="http://placehold.it/80x50" alt="img-thumb"/><span>Lorem ipsum dolor sit amet to adipiscing elit euismod bibendum laoreet.</span></li>
                  <li> <img src="http://placehold.it/80x50" alt="img-thumb"/><span>Lorem ipsum dolor sit amet to adipiscing elit euismod bibendum laoreet.</span></li>
                </ul>--%>
                 <asp:Localize ID="lcal" runat="server" Text=""></asp:Localize>

              <%--<img src="//images-dev.cdn-anritsu.com/images/about-anritsu/AboutAnritsu.jpg" alt="Anritsu Sign In" width="200px" height="200px" />--%>
                <%--<div class="aboutapp">
                    <asp:Localize ID="lcalAboutApp" runat="server" Text=""></asp:Localize>
                </div>--%>
                <%--<p>
                    <asp:HyperLink ID="hlSignUp" runat="server" CssClass="button" Text='<%$Resources:~/signin,hlSignUp.Text %>' Target="_blank" NavigateUrl="/account/new-user-registration"></asp:HyperLink>
                </p>--%>
                <%--<p> Anritsu does not share our registered user information with any outside parties.</p>
                <p class="link"><a href="#">Privacy Statement</a></p>--%>
                    <%--<asp:Localize ID="lcalPvtStmt"  runat="server"></asp:Localize>--%>
             </div>  
            
        </div>
            
    </div>

   

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
    
</asp:Content>
