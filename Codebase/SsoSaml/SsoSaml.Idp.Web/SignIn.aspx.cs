﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Xml;
using System.Data;
using System.Web.Security;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.Providers.SamlIdentityProvider;
using Atp.Saml2;
using System.Configuration;
using BLL = Anritsu.SsoSaml.Idp.Lib;


namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class SignIn : IdpBasePage
    {
        System.Collections.Specialized.NameValueCollection nameValues = null;
        protected override void OnInit(EventArgs e)
        {
            if (!IsPostBack)
            {
                InitSamlRequest();
                var sp = GetServiceProviderInfo(true);
                if (User.Identity.IsAuthenticated)
                {
                    if (sp.SPInitiatedLogin && Request.QueryString[KeyDef.ParmKeys.ReplyUrl].IsNullOrEmptyString())
                    {
                        FormsAuthentication.SignOut();
                        UIHelper.LoggedInUser_Set(null);
                    }
                    else
                    {
                        RedirectToContinue(sp);
                    }
                }
                //Form.DefaultFocus = UserName.ClientID;
                Form.DefaultButton = LoginButton.UniqueID;
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (!IsPostBack)
            {
         
                InitControls();
               
            }
            //fix for bug# 5445 Custom SP texts fall back to default after invalid password failure
            InitSpCustomControls();
            var mp = Master as App_MasterPages.MasterPage1Col;
            //mp.GreenPageTitleCtrl.Visible = false;
           
           nameValues= HttpUtility.ParseQueryString(Request.QueryString.ToString());
           


        }

        private void InitSpCustomControls()
        {
            var sp = GetServiceProviderInfo(true);
            var mktLocale = UIHelper.InitMarketLocale();
            var resourceKeyAboutApp = String.Format("{0}_AboutApp", sp.SpResKeyPrefix);
            var resourceKeyPageTitle = String.Format("{0}_PageTitle", sp.SpResKeyPrefix);
            var resourceKeyBrowserPageTitle = String.Format("{0}_BrowserPageTitle", sp.SpResKeyPrefix);
            var spPageTitleText = GetResourceStringForRelyingParty(resourceKeyPageTitle);
            var spBrowserPageTitleText = GetResourceStringForRelyingParty(resourceKeyBrowserPageTitle);

            OverwritePageTitleGreen(spPageTitleText);
            if (!spBrowserPageTitleText.IsNullOrEmptyString() && spBrowserPageTitleText != resourceKeyBrowserPageTitle)
                OverwritePageTitleBrowser(spBrowserPageTitleText);
            //add About my anritsu link changes MA-1346
            var resValue = ConvertUtility.ConvertToString(GetResourceString("~/signin", "lblsigninfo.Text"), string.Empty);
            lcal.Text = resValue.Replace("[[GLOBALWEBDOMAIN]]",
                ConfigurationManager.AppSettings["Idp.Web.GlobalWebDomain"]);
            var isNew = Request.QueryString[KeyDef.ParmKeys.CommonParams.IsNew];
            divNewUser.Visible = Convert.ToBoolean(isNew);
            if (isNew.IsNullOrEmptyString()) return;
            if (Convert.ToBoolean(isNew))
            {
                lcalAboutApp.Text = GetResourceString("~/signin", "NewAccountWelcomeMsg.Text");
            }

        }

        private void InitControls()
        {
            var sp = GetServiceProviderInfo(true);

            var signUpUrl = ConfigUtility.AppSettingGetValue("Idp.Web.SignUpUrl");

            //var mktLocale = UIHelper.InitMarketLocale();
            //contactUrl = mktLocale.IsNullOrEmptyString()
            //    ? contactUrl.Replace("[[LL-CC]]/", String.Empty)
            //    : contactUrl.Replace("[[LL-CC]]", mktLocale);

            var returnUrl = Request.Url.AbsolutePath;
            if (!string.IsNullOrEmpty(Request.Url.Query))
                signUpUrl += Request.Url.Query + "&ReturnURL=" + returnUrl;
            else
                signUpUrl += "?ReturnURL=" + returnUrl;

            hlForgotPwd.NavigateUrl = "/account/resetpwd" + Request.Url.Query;
            //show/hide contact us link 
            hlHelp.Visible = hlHelp.Visible = sp.ShowContactUs;
            var contactUrl = "/contact-us" + Request.Url.Query;
            if (Thread.CurrentThread.CurrentCulture.Name.Equals("ja-JP"))
            {
                hlHelp.NavigateUrl = GetResourceString("~/signin", "faqlink");
                lnkhelp.NavigateUrl = GetResourceString("~/signin", "faqlink");
            }
            else
            {
                hlHelp.NavigateUrl = contactUrl;
                lnkhelp.NavigateUrl = contactUrl;
            }

              
            //show/hide sign up link
            //lcalSignUpLabel.Visible = hlSignUp.Visible = sp.ShowSignup;
            var marketLocale = UIHelper.InitMarketLocale();
            var culture = marketLocale.IsNullOrEmptyString()
                ? "pub"
                : marketLocale;
            //lcalPvtStmt.Text = GetResourceString("~/signin", "lclsigninPvtStmt.Text").Replace("[[LL-CC]]", culture);
            SignUpLink.NavigateUrl = signUpUrl;
            // var processingText = GetGlobalResourceObject("common", "processing").ToString();
            //WebUtility.GenerateProcessingScript(LoginButton, "onclick", processingText, new List<String>() {});
        }

        private void InitSamlRequest()
        {
            var spInfo = GetServiceProviderInfo(true);
            if (IsSamlRequest)
            {
                Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] = null;
            }
            else
            {
                var ssoState = new IdpAuthRequestState(spInfo, String.Empty);
                Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] = ssoState;
                return;
            }

            var logParms = String.Empty;
            logParms = Request.Params.ToString();
            XmlElement logReqNode = null;
            var logError = String.Empty;

            try
            {
                AuthnRequest authnRequest;
                string relayState;
                SamlUtility.ProcessAuthnRequest(spInfo, out authnRequest, out relayState);
                if (authnRequest == null) throw new ArgumentException("Invalid AuthnRequest in the Request.");

                logReqNode = authnRequest.GetXml();

                var ssoState = new IdpAuthRequestState(spInfo, authnRequest, relayState);
                Session[KeyDef.SessionKeys.LoginAtIDP.AuthRequestState] = ssoState;
            }
            catch (ArgumentException aex)
            {
                logError = aex.Message;
                DoFallBackAction();
            }
            finally
            {
                Lib.SingleSignOn.GwIdpSamlLogBLL.Log_LoginSamlRequest(spInfo.SpTokenID.ToString(), logParms, logReqNode,
                    logError);
            }
        }

        private void DoLogin()
        {
            var ipAddress = WebUtility.GetUserIP();
            var userName = UserName.Text.Trim();
            var pwd = Password.Text.Trim();

            var sp = GetServiceProviderInfo(true);
            DataRow userData;
            String internalMsg;
            bool changePassword;

            var authStatus = Lib.SingleSignOn.GwIdpUserBLL.AuthenticateUser(sp,
                userName, pwd, ipAddress, out userData, out internalMsg, IsFromJpDownloadSite(), out changePassword);

            switch (authStatus)
            {
                case Lib.SingleSignOn.GwIdpAuthStatusCode.Authorized:
                    if (userData == null)
                        throw new HttpException(500, "User data not found for " + userName); //something wrong.
                    var userKey = userData["SecretKey"].ToString();
                    var isTempPassword = ConvertUtility.ConvertToBoolean(userData["IsTempPassword"], false);
                    UIHelper.LoggedInUser_Set(userData);
                    var cookieUserName = userKey;
                    var rememberMe = RememberMe.Checked;

                    #region " issue forms auth cookie for IDP "

                    //seperate time out for persistent and non persistent cookies
                    var timeout = rememberMe ? 1440 : FormsAuthentication.Timeout.Minutes;
                    var ticket = new FormsAuthenticationTicket(cookieUserName, rememberMe, timeout);
                    var encrypted = FormsAuthentication.Encrypt(ticket);
                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted)
                    {
                        Expires = DateTime.Now.AddMinutes(timeout),
                        Domain = FormsAuthentication.CookieDomain,
                        Secure = FormsAuthentication.RequireSSL,
                        HttpOnly = true
                    };
                    Response.Cookies.Add(authCookie);
                    //FormsAuthentication.SetAuthCookie(cookieUserName, rememberMe);
                    //store rememberMe in session variable
                    Session["Rememberme"] = rememberMe;

                    #endregion

                    if (isTempPassword)
                    {
                        var continueUrl = BuildContinueUrl(sp);
                        Response.Redirect(String.Format("/account/updatepwd?{0}={1}",
                            KeyDef.ParmKeys.ReturnURL,
                            HttpUtility.UrlEncode(continueUrl)),
                            true);
                    }
                    else if (changePassword)
                    {
                        var continueUrl = BuildContinueUrl(sp);
                        Response.Redirect(String.Format("/account/changepwd?{0}={1}",
                            KeyDef.ParmKeys.ReturnURL,
                            HttpUtility.UrlEncode(continueUrl)),
                            true);
                    }
                    else
                    {
                        RedirectToContinue(sp);
                    }
                    break;
                case Lib.SingleSignOn.GwIdpAuthStatusCode.AccountLocked:
                    ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_AccountLocked");
                    UserName.Focus();
                    break;
                case Lib.SingleSignOn.GwIdpAuthStatusCode.InvalidUserOrPassword:
                    ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_InvalidUserOrPassword");
                    UserName.Focus();
                    break;
                case Lib.SingleSignOn.GwIdpAuthStatusCode.MaxFailedLoginAttempts:
                    ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_MaxFailedLoginAttempts");
                    UserName.Focus();
                    break;
                case Lib.SingleSignOn.GwIdpAuthStatusCode.SystemError:
                    ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_SystemError");
                    UserName.Focus();
                    break;
                case Lib.SingleSignOn.GwIdpAuthStatusCode.InActiveUser:


                   string activatedEmail =SendActivationCodeEmail(userName,"System");
                    if (string.IsNullOrEmpty(activatedEmail) == false)
                    {
                        Session["UserEmail"] = activatedEmail;
                        lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationinitial.Msg").ToString().Replace("[[USEREMAILADDRESS]]", Lib.SingleSignOn.GwIdpUserBLL.MaskEmail(Session["UserEmail"].ToString()));
                        lcalErrorMsg.Visible = true;
                        pllogin.Visible = false;
                        plverification.Visible = true;
                        UserName.Focus();
                        CreateHistoryForBrowsers("Verification");
                      
                    }
                    else
                    {
                        pllogin.Visible = true;
                        plverification.Visible = false;
                        lcalErrorMsg.Visible = false;
                        ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_SystemError");
                        UserName.Focus();
                        break;
                    }
                    break;
                default:
                    ltrMsg.Text = GetResourceString("IdpAuthStatus", "AuthStatus_SystemError");
                    UserName.Focus();
                    break;
            }
        }

        private bool IsFromJpDownloadSite()
        {
            var jpDownloadSiteDomain = ConfigUtility.AppSettingGetValue("JPDownloadSiteDomain");
            return (ReturnURL.IndexOf(jpDownloadSiteDomain, StringComparison.Ordinal) > 0);
        }
       
        protected void LoginButton_Click(object sender, EventArgs e)
        {
           
            Page.Validate("LoginUserValidationGroup");
            if (!Page.IsValid) return;
             DoLogin();
        }

        private void RedirectToContinue(Lib.SingleSignOn.GwIdpServiceProvider sp)
        {
            var url = BuildContinueUrl(sp);
            Response.Redirect(url, true);
        }

        private String BuildContinueUrl(Lib.SingleSignOn.GwIdpServiceProvider sp)
        {
            const string url = "~/continue?";
            var qs = "" + Request.QueryString;
            if (qs.IsNullOrEmptyString())
            {
                qs = String.Format("{0}={1}&{2}={3}"
                    , KeyDef.ParmKeys.SsoIDP.SpTokenID, sp.SpTokenID
                    , KeyDef.ParmKeys.ReplyUrl, HttpUtility.UrlEncode(sp.SpDefaultUrl));
            }
            return url + qs;
        }
        /// <summary>
        /// This method will create history for each verification process to avoid back button issues
        /// </summary>
        /// <param name="pagescenrio"></param>
        /// <returns></returns>
        private void CreateHistoryForBrowsers(string pagescenrio,bool addquerystring=false)
        {

            string jsobj = string.Empty;
            pagescenrio= pagescenrio+"_"+ Guid.NewGuid().ToString();
            if(addquerystring)
                jsobj= "var stateObj = { page:'" + pagescenrio +"' };history.pushState(stateObj, '"+ pagescenrio + "', 'signin?"+nameValues.ToString()+"'); ";
            else
                jsobj = "var stateObj = { page:'" + pagescenrio + "' };history.pushState(stateObj, '" + pagescenrio + "', 'signin'); ";
            System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "verificationscript", jsobj, true);
           
        }
       

       
        /// <summary>
        /// Verify Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVerify_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (Session["Verifycount"] == null)
                {
                    Session["Verifycount"] = 0;
                }
                lcalErrorMsg.Visible = false;

                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[0-9]{6,6}$");
                if (string.IsNullOrEmpty(txtVerification.Text.Trim()) == true || regex.IsMatch(txtVerification.Text.Trim()) == false)
                {
                    lcalErrorMsg.Visible = true;
                    spanmessgae.Style.Add("color", "red");
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationvalidation.Msg").ToString();
                    return;
                }
                if (int.Parse(Session["Verifycount"].ToString()) >= 5)
                {
                    lcalErrorMsg.Visible = true;
                    spanmessgae.Style.Add("color", "red");
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationmaximumattempts.Msg").ToString();
                    btnVerify.Enabled = false;
                    CreateHistoryForBrowsers("verifymaxattempt");
                    return;
                }


               

                if (BLL.SingleSignOn.GwIdpUserBLL.VerifyValidationCode(UserName.Text.Trim(), txtVerification.Text.Trim()))
                {
                
                    divverification.Visible = false;
                    lcalErrorMsg.Visible = true;
                   
                    spanmessgae.Style.Add("color", "#2F977A");
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationsuccess.Msg").ToString();
                    verificationwelcomemsg.Visible = true;
                    verificationwelcomemsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationsuccess.Msg").ToString();
                    CreateHistoryForBrowsers("verifysuccess",true);
                    var userName = UserName.Text.Trim();
                    UserName.Enabled = false;
                    UserName.ReadOnly = true;
                    pllogin.Visible = true;
                    plverification.Visible = false;
                   


                }
                else
                {
                    Session["Verifycount"] = int.Parse(Session["Verifycount"].ToString()) + 1;
                    lcalErrorMsg.Visible = true;
                    lcalErrorMsg.Text = GetGlobalResourceObject("~/account/verification", "lcalverificationinvalid.Msg").ToString();
                    spanmessgae.Style.Add("color", "red");
                    CreateHistoryForBrowsers("verifyfailed");

                }

            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Resend Email Verfication code Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnresend_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserEmail"] != null)
                {
                    Session["UserEmail"] = SendActivationCodeEmail(Session["UserEmail"].ToString(), Session["UserEmail"].ToString());
                    lcalResendMessage.Text = GetGlobalResourceObject("~/account/verification", "lcalresendclick.Msg").ToString().Replace("[[USEREMAILADDRESS]]", Session["UserEmail"].ToString());
                    lcalResendMessage.Visible = true;
                    
                }
                else
                {

                }

            }
            catch (Exception ex)
            {

            }
        }

    }
}