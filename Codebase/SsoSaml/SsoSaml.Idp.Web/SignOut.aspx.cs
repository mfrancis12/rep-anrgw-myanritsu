﻿using System;
using System.Web;
using System.Web.Security;
using System.Data;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache;
using Atp.Saml2;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class SignOut : IdpBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            Response.Cache.SetNoTransforms();

            var spInfo = GetServiceProviderInfo(true);
            var userData = UIHelper.LoggedInUser_Get(false);
            UpdateUserActivity(userData, spInfo);
            //remove My Anritsu Auth Cookie
            var myAnritsuAuthCookie =
               HttpContext.Current.Request.Cookies[ConfigUtility.AppSettingGetValue("Idp.Web.MyAnritsuAuthCookie")];
            if (myAnritsuAuthCookie != null)
            {
                var clonedCookie = new HttpCookie(ConfigUtility.AppSettingGetValue("Idp.Web.MyAnritsuAuthCookie"))
                {
                    Expires = DateTime.Now.AddDays(-1d),
                    Domain = "anritsu.com"
                };
                HttpContext.Current.Response.Cookies.Add(clonedCookie);
            }
            FormsAuthentication.SignOut();
            Session.Abandon();

            if (spInfo == null || !IsSamlRequest)
            {
                var culture = HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.SiteLocale].ConvertNullToEmptyString());
                var retUrl = HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.ReturnURL].ConvertNullToEmptyString());
                if(string.IsNullOrEmpty(culture))
                Response.Redirect(ReturnURL, true);
                else
                {
                    var url = new Uri(ReturnURL);
                    if (string.IsNullOrEmpty(retUrl))
                    {
                        var regionalUri = string.Format("//{0}/{1}/", url.Host, culture);
                        Response.Redirect(regionalUri, true);
                    }
                    else
                    {
                        Response.Redirect(ReturnURL, true);
                    }
                }
            }
            else
            {
                var certCache = new CertCacheFactory();
                var x509Certificate = certCache.GetCertificateCER(spInfo.CertID);

                var logoutRequest = LogoutRequest.Create(Request, x509Certificate.PublicKey.Key);
                LogLogoutRequest(spInfo.SpTokenID.ToString(), logoutRequest);

                #region " You can send a logout request to any other service providers here. "

                #endregion

                var logoutResponse = new LogoutResponse {Issuer = new Issuer(WebUtility.GetAbsoluteUrl("~/"))};
                var logoutUrl = String.Format("{0}?{1}=logout&{2}={3}&{4}={5}"
                    , spInfo.SpLogoutUrl
                    , KeyDef.ParmKeys.LogoutStatus
                    , KeyDef.ParmKeys.ReplyUrl,
                    HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.ReplyUrl].ConvertNullToEmptyString())
                    ,KeyDef.ParmKeys.SiteLocale
                    , HttpUtility.UrlEncode(Request[KeyDef.ParmKeys.SiteLocale].ConvertNullToEmptyString())
                    );
                LogLogoutResponse(spInfo.SpTokenID.ToString(), logoutResponse, logoutUrl);
                logoutResponse.Redirect(Response, logoutUrl, logoutRequest.RelayState, x509Certificate.PrivateKey);
                Response.End();
            }

            base.OnInit(e);
        }

        private void UpdateUserActivity(DataRow userData, Lib.SingleSignOn.GwIdpServiceProvider spInfo)
        {
            if (userData == null || spInfo == null) return;
            var gwUserId = ConvertUtility.ConvertToInt32(userData["GWUserID"], 0);
            var userIp = WebUtility.GetUserIP();
            Lib.SingleSignOn.GwIdpUserActivityBLL.UpdateLogout(gwUserId, spInfo.ServiceProviderID, userIp);
        }

        private void LogLogoutRequest(String spTokenId, LogoutRequest req)
        {
            if (req == null) return;
            var logParms = String.Empty;
            if (HttpContext.Current.Request.Params != null) logParms = HttpContext.Current.Request.Params.ToString();
            var logSamlNode = req.GetXml();
            var logError = String.Empty;

            Lib.SingleSignOn.GwIdpSamlLogBLL.Log_LogoutSamlRequest(spTokenId, logParms, logSamlNode, logError);
        }

        private void LogLogoutResponse(String spTokenId, LogoutResponse resp, string logoutUrl)
        {
            if (resp == null) return;
            var logParms = logoutUrl;
            var logSamlNode = resp.GetXml();
            var logError = String.Empty;

            Lib.SingleSignOn.GwIdpSamlLogBLL.Log_LogoutSamlResponse(spTokenId, logParms, logSamlNode, logError);
        }
    }
}