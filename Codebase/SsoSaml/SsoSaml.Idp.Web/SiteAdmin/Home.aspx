﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPage1Col.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div style='margin-left: 200px;'>
<anrui:GlobalWebBoxedPanel ID="gwpnlAdminMenu" runat="server" ShowHeader="true" HeaderText="Single Sign On Administration">
<center>
<div class='siteadminmenu'>
    <ul class="no-bullets">
        <li><a class='lnkSiteList' href='/siteadmin/resourceadmin/resadminsearch'>Resource Strings</a></li>
        <li><a class='lnkGeoZone' href='/siteadmin/spadmin/home'>Service Providers</a></li>
        <li><a class='lnkGeoZone' href='/siteadmin/spadmin/x509list'>x.509 Certificates</a></li>
    </ul>
</div>
</center>
<div class='clearfloat'></div>
</anrui:GlobalWebBoxedPanel>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
