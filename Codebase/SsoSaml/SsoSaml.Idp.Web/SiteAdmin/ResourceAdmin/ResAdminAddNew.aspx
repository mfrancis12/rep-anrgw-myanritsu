﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="ResAdminAddNew.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin.ResAdminAddNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<anrui:GlobalWebBoxedPanel ID="gwpnlResAdmin" runat="server" ShowHeader="false" IsSideBarLinkList="true">
    <div class="width-30 left">
         <ul class="no-bullets">
            <li><a href="/siteadmin/home">Admin Home</a></li>
            <li><a href="ResAdminSearch">Search Resources</a></li>
            <li><a href="ResAdminAddNew">Add New Resource</a></li>
        </ul>
    </div>
   
    <div class="width-60 left">
        <div class="settingrow input-text group">
            <span class="settinglabelrgt-15"><asp:Localize ID="lcalClassKey" runat="server" Text="Resource Class"></asp:Localize></span>
            <asp:TextBox ID="txtClassKey" runat="server" ValidationGroup="vgAddNewRes"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvClassKey" runat="server" ControlToValidate="txtClassKey" ValidationGroup="vgAddNewRes" ErrorMessage='required!'>required!</asp:RequiredFieldValidator>
        </div>
        <div class="settingrow group input-text">
            <span class="settinglabelrgt-15"><asp:Localize ID="lcalResKey" runat="server" Text="Resource Key"></asp:Localize></span>
            <asp:TextBox ID="txtResKey" runat="server" ValidationGroup="vgAddNewRes"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvResKey" runat="server" ControlToValidate="txtResKey" ValidationGroup="vgAddNewRes">required!</asp:RequiredFieldValidator>
        </div>
        <div class="settingrow group">
            <span class="settinglabelrgt-15">&nbsp;</span>
            <asp:Button ID="bttAddNewContent" runat="server" Text='Add New Resource' onclick="bttAddNewContent_Click"  ValidationGroup="vgAddNewRes" CssClass="button"/>
        </div>
    </div>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
