﻿using System;
using System.Collections.Generic;
using System.Linq;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers;
using AnrGWRes = Anritsu.AnrCommon.AnrGWResourceProvider;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin
{
    public partial class ResAdminAddNew : BasePageSiteAdminIdp
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitClassKey();
            }
        }

        private void InitClassKey()
        {
            var lst = ResourceAdminHelper.ResAdmin_SearchOptionsGet(false);
            if (lst == null) return;
            var classKey = from s in lst
                where s.SearchByField.Equals("ClassKey")
                select s;
            if (classKey.Any())
            {
                txtClassKey.Text = ConvertUtility.ConvertNullToEmptyString(classKey.First().SearchObject);
            }
        }

        protected void bttAddNewContent_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            var classKey = txtClassKey.Text.Trim();
            var resKey = txtResKey.Text.Trim();
            var contentId = AnrGWRes.AnrGWResourceAdmin.Insert(classKey, resKey, "en", "-", false);
            if (contentId > 0)
            {
                WebUtility.HttpRedirect(null,
                    String.Format("{0}?{1}={2}", KeyDef.UrlList.SiteAdmin.ResourceAdmin.SearchDetails
                        , KeyDef.ParmKeys.SiteAdmin.ResourceAdmin.Search_ContentID, contentId));
            }
        }
    }
}