﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="ResAdminDetail.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin.ResAdminDetail"  validateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="/App_ClientControls/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        theme: "advanced",
        mode: "textareas",
        convert_urls: false,
        forced_root_block: "",
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<anrui:GlobalWebBoxedPanel ID="gwpnlResAdmin" runat="server" ShowHeader="false" IsSideBarLinkList="true">
     <div class="width-30 left">
    <ul class="no-bullets">
    <li><a href="/siteadmin/home">Admin Home</a></li>
        <li><a href="ResAdminSearch.aspx">Search Resources</a></li>
        <li><a href="ResAdminAddNew.aspx">Add New Resource</a></li>
    </ul>
         </div>
    <div class="width-70 left">
            <div class="settingrow">
    <span class="settinglabelrgt-15"><asp:Localize ID="lcalContentClassKey" runat="server" Text='Resource Class:'></asp:Localize></span>
    <asp:Literal ID="ltrContentClassKey" runat="server"></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15"><asp:Localize ID="lcalContentResKey" runat="server" Text='Resource Key:'></asp:Localize></span>
    <asp:Literal ID="ltrContentResKey" runat="server"></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15"><asp:Localize ID="lcalContentLocale" runat="server" Text='Locale:'></asp:Localize></span>
    <asp:Literal ID="ltrContentLocale" runat="server"></asp:Literal>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15"><asp:Localize ID="lcalContentAllowAutoTranslate" runat="server" Text='Allow Auto Translate:'></asp:Localize></span>
    <asp:CheckBox ID="cbxContentAllowAutoTranslate" runat="server" Text=" " />
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15"><asp:Localize ID="lcalContentStr" runat="server" Text='Resource String-'></asp:Localize></span>
</div>
<div class="settingrow">
<textarea id="txtContent" runat="server" rows="30" cols="20" style="width: 98%; height: 500px;"></textarea>
</div>
<div class="settingrow" id="divAutoTranslate" runat="server">
<span class="settinglabelrgt-15"><asp:Localize ID="lcalAutoTranslate" runat="server" Text='Auto Translate'></asp:Localize></span>
<asp:CheckBox ID="cbxAutoTranslate" runat="server" Text=" " Checked="true"/>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">&nbsp;</span>
    <asp:Button ID="bttSave" runat="server" 
        Text='Save' 
        onclick="bttSave_Click" />
    <asp:Button ID="bttDelete" runat="server" Text='Delete' onclick="bttDelete_Click" />
</div>
        </div>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
