﻿using System;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers;
using AnrGWRes = Anritsu.AnrCommon.AnrGWResourceProvider;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin
{
    public partial class ResAdminDetail : App_Lib.UI.BasePageSiteAdminIdp
    {
        private Int32 ContentId
        {
            get
            {
                return
                    ConvertUtility.ConvertToInt32(
                        Request.QueryString[KeyDef.ParmKeys.SiteAdmin.ResourceAdmin.Search_ContentID], 0);
            }
        }

        private String ReturnUrl
        {
            get { return Request.QueryString[KeyDef.ParmKeys.ReturnURL]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (ContentId < 1) throw new HttpException(404, "Page not found.");
            InitContent();
        }

        protected void bttSave_Click(object sender, EventArgs e)
        {
            var row = AnrGWRes.AnrGWResourceAdmin.Select(ContentId);
            if (row == null) throw new HttpException(404, "Page not found.");
            var newContent = HttpUtility.HtmlEncode(txtContent.Value.Trim());
            var locale = ConvertUtility.ConvertNullToEmptyString(row["Locale"]);
            AnrGWRes.AnrGWResourceAdmin.Update(ContentId, newContent, cbxContentAllowAutoTranslate.Checked, false);

            var isDefaultLocale = locale == "en";
            if (cbxAutoTranslate.Checked && isDefaultLocale)
            {
                ResourceAdminHelper.AutoTranslate(row);
            }
            DoRedirect();
        }

        protected void bttDelete_Click(object sender, EventArgs e)
        {
            var row = AnrGWRes.AnrGWResourceAdmin.Select(ContentId);
            if (row == null) throw new HttpException(404, "Page not found.");
            AnrGWRes.AnrGWResourceAdmin.Delete(ContentId);

            DoRedirect();
        }

        private void InitContent()
        {
            if (ContentId < 1) return; //new
            var row = AnrGWRes.AnrGWResourceAdmin.Select(ContentId);
            if (row == null) throw new HttpException(404, "Page not found.");
            var locale = ConvertUtility.ConvertNullToEmptyString(row["Locale"]);
            var classKey = ConvertUtility.ConvertNullToEmptyString(row["ClassKey"]);
            var resKey = ConvertUtility.ConvertNullToEmptyString(row["ResourceKey"]);
            ltrContentClassKey.Text = classKey;
            ltrContentResKey.Text = resKey;
            ltrContentLocale.Text = ConvertUtility.ConvertNullToEmptyString(row["LocaleDisplayText"]);
            txtContent.Value = HttpUtility.HtmlDecode(ConvertUtility.ConvertNullToEmptyString(row["ContentStr"]));
            cbxContentAllowAutoTranslate.Checked = ConvertUtility.ConvertToBoolean(row["AllowAutoTranslate"], true);

            var isDefaultLocale = (locale == "en");
            divAutoTranslate.Visible = isDefaultLocale;
            bttDelete.Visible = !isDefaultLocale;

            //DataTable tb = Lib.Content.Res_ContentStoreBLL.SelectByClassKeyResourceKey(classKey, resKey);
            //dlOtherLocales.DataSource = tb;
            //dlOtherLocales.DataBind();

            //DataTable tbSupportedLocales = AnrGWRes.AnrGWResourceLocale.SelectAll();
            //var filtered = (from l in tbSupportedLocales.AsEnumerable()
            //                join n in tb.AsEnumerable() on l["Locale"].ToString() equals n["Locale"].ToString()
            //             into f
            //                where f.Count() == 0
            //                select l);

            //DataTable filteredNewLocales = null;
            //if (filtered != null && filtered.Count() > 0) filteredNewLocales = filtered.CopyToDataTable();
            //if (filteredNewLocales == null || filteredNewLocales.Rows.Count < 1)
            //{
            //    cmbAddNewLang.Visible = false;
            //}
            //else
            //{
            //    DataView dv = filteredNewLocales.DefaultView;
            //    dv.Sort = "DisplayText ASC";
            //    cmbAddNewLang.DataSource = dv;
            //    cmbAddNewLang.DataBind();
            //}

        }

        private void DoRedirect()
        {
            var nextUrl = ReturnUrl.ConvertNullToEmptyString().Trim();
            if (nextUrl.IsNullOrEmptyString())
            {
                Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchResults] = null;
                nextUrl = KeyDef.UrlList.SiteAdmin.ResourceAdmin.SearchResults;
            }
            if (nextUrl != "0") WebUtility.HttpRedirect(null, nextUrl);
        }
    }
}