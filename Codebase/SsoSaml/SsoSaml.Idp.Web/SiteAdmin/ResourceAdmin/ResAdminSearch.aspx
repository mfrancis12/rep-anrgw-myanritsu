﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="ResAdminSearch.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin.ResAdminSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<anrui:GlobalWebBoxedPanel ID="gwpnlResAdmin" runat="server" ShowHeader="false" IsSideBarLinkList="true">
    <div class="width-30 left">
    <ul class="no-bullets">
    <li><a href="/siteadmin/home">Admin Home</a></li>
        <li><a href="ResAdminSearch.aspx">Search Resources</a></li>
        <li><a href="ResAdminAddNew.aspx">Add New Resource</a></li>
    </ul>
        </div>
    <div class="width-60 left">
        <div class="settingrow margin-top-15">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByClassKey" runat="server" Text="Search By Resource Class" TextAlign="Left" />
        </span>
            <div class="group input-text">
        <asp:TextBox ID="txtSearchByClassKey" runat="server" SkinID="tbx-300"></asp:TextBox>
                </div>
    </div>
    <div class="settingrow margin-top-15">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByResKey" runat="server" Text="Search By Resource Key" TextAlign="Left" />
        </span>
        <div class="group input-text">
        <asp:TextBox ID="txtSearchByResKey" runat="server" SkinID="tbx-300"></asp:TextBox>
            </div>
    </div>
    <div class="settingrow margin-top-15">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByLocale" runat="server" Text="Search By Locale" TextAlign="Left" />
        </span>
        <div class="group input-text">
        <asp:DropDownList ID="cmbSearchByLocale" runat="server" DataTextField="DisplayText" DataValueField="Locale">
        </asp:DropDownList>
            </div>
    </div>
    <div class="settingrow margin-top-15">
        <span class="settinglabelrgt-15" style="width: 20em!Important">
            <asp:CheckBox ID="cbxSearchByContentKeyword" runat="server" Text="Search By Keyword" TextAlign="Left" />
        </span>
        <div class="group input-text">
        <asp:TextBox ID="txtSearchByContentKeyword" runat="server" SkinID="tbx-300"></asp:TextBox>
            </div>
    </div>
    <div class="settingrow margin-top-15">
        <span class="settinglabelrgt-15" style="width: 20em!Important">&nbsp;</span>
        <asp:Button ID="bttSearchContent" runat="server" Text="Search"
            OnClick="bttSearchContent_Click" CssClass="button"/>
        <asp:Button ID="bttClearCache" runat="server" Text="Clear Resource String Cache"
            OnClick="bttClearCache_Click" CssClass="button" />
    </div>

    </div>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
