﻿using System;
using System.Collections.Generic;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin
{
    public partial class ResAdminSearch : BasePageSiteAdminIdp
    {
        private Boolean AutoSearch
        {
            get
            {
                return
                    ConvertUtility.ConvertToBoolean(
                        Request.QueryString[KeyDef.ParmKeys.SiteAdmin.ResourceAdmin.Search_Auto], false);
            }
        }

        private String PreSetClassKey
        {
            get
            {
                return
                    ConvertUtility.ConvertNullToEmptyString(
                        Request.QueryString[KeyDef.ParmKeys.SiteAdmin.ResourceAdmin.Search_PreClassKey]);
            }
        }

        private String PreSetResKey
        {
            get
            {
                return
                    ConvertUtility.ConvertNullToEmptyString(
                        Request.QueryString[KeyDef.ParmKeys.SiteAdmin.ResourceAdmin.Search_PreResKey]);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            BindContentLocale();
            InitSearchOptions();
        }

        protected void bttSearchContent_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        protected void bttClearCache_Click(object sender, EventArgs e)
        {
            var resCache = new App_Lib.AppCache.ResourceLocaleCacheFactory();
            resCache.TouchCacheFile();

            AnrCommon.AnrGWResourceProvider.AnrGWResourceAdmin.ClearResCache();
        }

        private void BindContentLocale()
        {
            var resLocaleCache = new App_Lib.AppCache.ResourceLocaleCacheFactory();
            cmbSearchByLocale.DataSource = resLocaleCache.GetData("supportedlocales") as DataTable;
            cmbSearchByLocale.DataBind();
        }

        private void InitSearchOptions()
        {
            var searchOptions = ResourceAdminHelper.ResAdmin_SearchOptionsGet(false);
            if (searchOptions != null)
            {
                foreach (var sbo in searchOptions)
                {
                    switch (sbo.SearchByField)
                    {
                        case "ClassKey":
                            cbxSearchByClassKey.Checked = true;
                            txtSearchByClassKey.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ResKey":
                            cbxSearchByResKey.Checked = true;
                            txtSearchByResKey.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "Locale":
                            cbxSearchByLocale.Checked = true;
                            cmbSearchByLocale.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                        case "ContentKeyword":
                            cbxSearchByContentKeyword.Checked = true;
                            txtSearchByContentKeyword.Text = ConvertUtility.ConvertNullToEmptyString(sbo.SearchObject);
                            break;
                    }
                }
            }

            if (!AutoSearch) return;
            var hasAtLeastOnePreset = false;
            if (!PreSetClassKey.IsNullOrEmptyString())
            {
                cbxSearchByClassKey.Checked = true;
                txtSearchByClassKey.Text = PreSetClassKey;
                hasAtLeastOnePreset = true;
            }

            if (!PreSetResKey.IsNullOrEmptyString())
            {
                cbxSearchByResKey.Checked = true;
                txtSearchByResKey.Text = PreSetResKey;
                hasAtLeastOnePreset = true;
            }

            if (hasAtLeastOnePreset) DoSearch();
        }

        private void DoSearch()
        {
            var existingSearchOptions = ResourceAdminHelper.ResAdmin_SearchOptionsGet(false);
            var searchOptions = ResourceAdminHelper.ResAdmin_SearchOptionsGet(true);
            if (cbxSearchByClassKey.Checked)
                searchOptions.Add(new SearchByOption("ClassKey", txtSearchByClassKey.Text.Trim()));
            if (cbxSearchByResKey.Checked)
                searchOptions.Add(new SearchByOption("ResKey", txtSearchByResKey.Text.Trim()));
            if (cbxSearchByLocale.Checked)
                searchOptions.Add(new SearchByOption("Locale", cmbSearchByLocale.SelectedValue));
            if (cbxSearchByContentKeyword.Checked)
                searchOptions.Add(new SearchByOption("ContentKeyword", txtSearchByContentKeyword.Text.Trim()));
            if (existingSearchOptions != searchOptions)
            {
                Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchResults] = null;
            }
            ResourceAdminHelper.ResAdmin_SearchOptionsSet(searchOptions);
            WebUtility.HttpRedirect(null, KeyDef.UrlList.SiteAdmin.ResourceAdmin.SearchResults);
        }
    }
}