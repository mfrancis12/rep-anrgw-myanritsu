﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="ResAdminSearchResults.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin.ResAdminSearchResults" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="//images-dev.cdn-anritsu.com/controls/componentart/Windows7/theme.css" type="text/css" rel="stylesheet" />
<link href="//images-dev.cdn-anritsu.com/controls/componentart/Windows7/icons.css" type="text/css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<anrui:GlobalWebBoxedPanel ID="gwpnlResAdmin" runat="server" ShowHeader="false" IsSideBarLinkList="true">
    <div class="width-30 left">
        <ul class="no-bullets">
        <li><a href="/siteadmin/home">Admin Home</a></li>
            <li><a href="ResAdminSearch.aspx">Search Resources</a></li>
            <li><a href="ResAdminAddNew.aspx">Add New Resource</a></li>
        </ul>
    </div>
    <div class="width-70 left">
    <div class="settingrow">
<ComponentArt:DataGrid id="cadgContent" AutoTheming="true" ClientIDMode="AutoID"
        PagerStyle="Numbered" ShowSearchBox="true" SearchText="Search within"
        EnableViewState="false"
        RunningMode="Callback" GroupBy="ClassKey,ResourceKey"
        ShowFooter="true" ShowHeader="true"
        PageSize="150" AllowPaging="true"
        Width="97%"
        runat="server">
        <Levels>
          <ComponentArt:GridLevel DataKeyField="ContentID">
              <Columns>
              <ComponentArt:GridColumn DataCellClientTemplateId="ctContentDetails" Width="50" HeadingText=" " />
                <ComponentArt:GridColumn DataField="ClassKey" HeadingText="Class Key" Align="Left"/>
                <ComponentArt:GridColumn DataField="ResourceKey" HeadingText="Resource Key" Align="Left"/>
                <ComponentArt:GridColumn DataField="Locale" HeadingText="Locale" Align="Center"/>
                <ComponentArt:GridColumn DataField="AllowAutoTranslate" HeadingText="Auto Translate" Align="Center"/>
                <ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="Created Date" FormatString="MMM dd, yyyy" Width="80"  Align="Right"/>                
                <ComponentArt:GridColumn DataField="ContentID" Visible="false"/>
              </Columns>
          </ComponentArt:GridLevel>
          </Levels>
          <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ctContentDetails">
                <a href='resadmindetail?contentid=## DataItem.GetMember("ContentID").Value ##' title=''>details</a>
            </ComponentArt:ClientTemplate>
          </ClientTemplates>
        </ComponentArt:DataGrid>
    </div>
    
    </div>
</anrui:GlobalWebBoxedPanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
