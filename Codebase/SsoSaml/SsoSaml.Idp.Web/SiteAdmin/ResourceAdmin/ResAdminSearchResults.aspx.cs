﻿using System;
using System.Data;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.SiteAdminHelpers;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.ResourceAdmin
{
    public partial class ResAdminSearchResults : BasePageSiteAdminIdp
    {
        private DataTable SearchResults
        {
            get
            {
                var tb = Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchResults] as DataTable;
                if (tb != null && tb.Rows.Count >= 1) return tb;
                tb = ResourceAdminHelper.SelectFiltered(ResourceAdminHelper.ResAdmin_SearchOptionsGet(false));
                if (tb != null) Session[KeyDef.SessionKeys.SiteAdmin.ResourceAdmin.Search_SearchResults] = tb;
                return tb;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            cadgContent.NeedRebind += cadgContent_OnNeedRebind;
            cadgContent.NeedDataSource += cadgContent_OnNeedDataSource;
            cadgContent.PageIndexChanged += cadgContent_OnPageChanged;
            cadgContent.SortCommand += cadgContent_OnSort;
            cadgContent.FilterCommand += cadgContent_OnFilter;
            cadgContent.GroupCommand += cadgContent_OnGroup;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            cadgContent_GetGridDataSource();
            cadgContent.DataBind();
        }

        private void cadgContent_GetGridDataSource()
        {
            cadgContent.DataSource = SearchResults;
        }

        public void cadgContent_OnNeedRebind(object sender, EventArgs oArgs)
        {
            cadgContent.DataBind();
        }

        public void cadgContent_OnNeedDataSource(object sender, EventArgs oArgs)
        {
            cadgContent_GetGridDataSource();
        }

        public void cadgContent_OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cadgContent.CurrentPageIndex = oArgs.NewIndex;
        }

        public void cadgContent_OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cadgContent.Filter = oArgs.FilterExpression;
        }

        public void cadgContent_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cadgContent.Sort = oArgs.SortExpression;
        }

        public void cadgContent_OnGroup(object sender, ComponentArt.Web.UI.GridGroupCommandEventArgs oArgs)
        {
            cadgContent.GroupBy = oArgs.GroupExpression;
        }
    }
}