﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin.Home" %>
<%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register src="~/App_Controls/Admin_SpMenu.ascx" tagname="Admin_SpMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="//images-dev.cdn-anritsu.com/controls/componentart/windows7/theme.css" type="text/css" rel="stylesheet" />
<link href="//images-dev.cdn-anritsu.com/controls/componentart/windows7/icons.css" type="text/css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<uc1:Admin_SpMenu ID="Admin_SpMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="settingrow">
<ComponentArt:DataGrid id="cadgServiceProviders" AutoTheming="true" ClientIDMode="AutoID"
        PagerStyle="Numbered" ShowSearchBox="true" SearchText="Search within"
        EnableViewState="false"
        RunningMode="Callback"
        ShowFooter="false" ShowHeader="true"
        PageSize="150" AllowPaging="false"
        Width="97%"
        runat="server">
        <Levels>
          <ComponentArt:GridLevel DataKeyField="ServiceProviderID">
              <Columns>
              <ComponentArt:GridColumn DataCellClientTemplateId="ctSpDetails" Width="50" HeadingText=" " />
                <ComponentArt:GridColumn DataField="ServiceProviderName" HeadingText="Service Provider Name" Align="Left" Width="200"/>
                <ComponentArt:GridColumn DataField="SpResKeyPrefix" HeadingText="ResKeyPrefix" Align="Left" Width="80"/>
                <ComponentArt:GridColumn DataField="SpHostName" HeadingText="Host Name" Align="Left" Width="170"/>
                <ComponentArt:GridColumn DataField="SamlAttributeSetDesc" HeadingText="SamlAttributeSet" Align="Left" Width="150"/>
                <ComponentArt:GridColumn DataField="CreatedOnUTC" HeadingText="Created Date" FormatString="MMM dd, yyyy" Width="80"  Align="Right"/>                
                <ComponentArt:GridColumn DataField="ServiceProviderID" Visible="false"/>
                <ComponentArt:GridColumn DataField="SpTokenID" Visible="false"/>
              </Columns>
          </ComponentArt:GridLevel>
          </Levels>
          <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ctSpDetails">
                <a href='spdetail?sptkn=## DataItem.GetMember("SpTokenID").Value ##' title=''>details</a>
            </ComponentArt:ClientTemplate>
          </ClientTemplates>
        </ComponentArt:DataGrid>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
