﻿using System;
using System.Data;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin
{
    public partial class Home : App_Lib.UI.BasePageSiteAdminIdp
    {
        protected override void OnInit(EventArgs e)
        {
            cadgServiceProviders.NeedRebind += cadgServiceProviders_OnNeedRebind;
            cadgServiceProviders.NeedDataSource += cadgServiceProviders_OnNeedDataSource;
            cadgServiceProviders.PageIndexChanged += cadgServiceProviders_OnPageChanged;
            cadgServiceProviders.SortCommand += cadgServiceProviders_OnSort;
            cadgServiceProviders.FilterCommand += cadgServiceProviders_OnFilter;
            cadgServiceProviders.GroupCommand += cadgServiceProviders_OnGroup;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            cadgServiceProviders_GetGridDataSource();
            cadgServiceProviders.DataBind();
        }

        private void cadgServiceProviders_GetGridDataSource()
        {
            var spCache = new App_Lib.AppCache.Admin_ServiceProviderCacheFactory();
            cadgServiceProviders.DataSource = spCache.GetData("allServiceProviders") as DataTable;
        }

        public void cadgServiceProviders_OnNeedRebind(object sender, EventArgs oArgs)
        {
            cadgServiceProviders.DataBind();
        }

        public void cadgServiceProviders_OnNeedDataSource(object sender, EventArgs oArgs)
        {
            cadgServiceProviders_GetGridDataSource();
        }

        public void cadgServiceProviders_OnPageChanged(object sender,
            ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            cadgServiceProviders.CurrentPageIndex = oArgs.NewIndex;
        }

        public void cadgServiceProviders_OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            cadgServiceProviders.Filter = oArgs.FilterExpression;
        }

        public void cadgServiceProviders_OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            cadgServiceProviders.Sort = oArgs.SortExpression;
        }

        public void cadgServiceProviders_OnGroup(object sender, ComponentArt.Web.UI.GridGroupCommandEventArgs oArgs)
        {
            cadgServiceProviders.GroupBy = oArgs.GroupExpression;
        }
    }
}