﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="SpDetail.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin.SpDetail" %>
<%@ Register src="~/App_Controls/Admin_SpMenu.ascx" tagname="Admin_SpMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<uc1:Admin_SpMenu ID="Admin_SpMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="settingrow">
    <span class='msg'>Read only.  Changes must be made at database level.  Improper changes could break Single Sign On with the Service Provider.</span>
</div>

<div class="settingrow">
    <span class="settinglabelrgt-15">TokenID</span>
    <asp:TextBox ID="txtSpTokenID" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Resource Key Prefix</span>
    <asp:TextBox ID="txtResKeyPrefix" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Service Provider Name</span>
    <asp:TextBox ID="txtSpName" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Host Name</span>
    <asp:TextBox ID="txtSpHostName" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Saml Attribute Set</span>
    <asp:DropDownList ID="cmbSamlAttributeSet" runat="server" DataTextField="SamlAttributeSetDesc" DataValueField="SamlAttributeSetID" ValidationGroup="vgSp" Enabled="false"></asp:DropDownList>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Sign Out Url</span>
    <asp:TextBox ID="txtUrlSignOut" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Assertion Consumer Service (ACS) Url</span>
    <asp:TextBox ID="txtUrlAcs" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">Artifact Url</span>
    <asp:TextBox ID="txtUrlArtifact" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">SP to IDP SamlBindingUri</span>
    <asp:TextBox ID="txtSamlBindingUriSpToIdp" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>
<div class="settingrow">
    <span class="settinglabelrgt-15">IDP to SP SamlBindingUri</span>
    <asp:TextBox ID="txtSamlBindingUriIdpToSp" runat="server" SkinID="tbx-400" ReadOnly="true"></asp:TextBox>
</div>

<div class="settingrow">
    <span class="settinglabelrgt-15">x509 Certificate</span>
    <asp:HyperLink ID="hlCert" runat="server" NavigateUrl="x509detail?certid={0}"></asp:HyperLink>
</div>
<div class="settingrow">
<span class="settinglabelrgt-15">&nbsp;</span>
<asp:Button ID="bttClearSPCache" runat="server" Text="Clear Service Provider Cache" onclick="bttClearSPCache_Click" />
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
