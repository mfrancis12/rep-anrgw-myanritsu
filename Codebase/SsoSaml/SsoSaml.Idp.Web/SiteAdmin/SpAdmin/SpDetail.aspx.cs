﻿using System;
using System.Web;
using System.Data;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Web.App_Lib.AppCache;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin
{
    public partial class SpDetail : App_Lib.UI.BasePageSiteAdminIdp
    {
        private Guid SpTokenId
        {
            get
            {
                return ConvertUtility.ConvertToGuid(Request.QueryString[KeyDef.ParmKeys.SsoIDP.SpTokenID], Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            InitSamlAttributeSet();
            InitServiceProvider();
        }

        private void InitSamlAttributeSet()
        {
            var admSpCache = new Admin_ServiceProviderCacheFactory();
            cmbSamlAttributeSet.DataSource = admSpCache.GetData("allSamlAttributeSet") as DataTable;
            cmbSamlAttributeSet.DataBind();
        }

        private void InitServiceProvider()
        {
            if (SpTokenId.IsNullOrEmptyGuid()) throw new HttpException(404, "Page not found.");

            var spCache = new ServiceProviderCacheFactory();
            var sp = spCache.GetData(SpTokenId.ToString()) as Lib.SingleSignOn.GwIdpServiceProvider;
            if (sp == null) throw new HttpException(404, "Page not found.");
            txtSpTokenID.Text = sp.SpTokenID.ToString().ToUpperInvariant();
            txtResKeyPrefix.Text = sp.SpResKeyPrefix.ToUpperInvariant();
            txtSpName.Text = sp.ServiceProviderName;
            txtSpHostName.Text = sp.SpHostName;
            txtUrlSignOut.Text = sp.SpLogoutUrl;
            txtUrlAcs.Text = sp.SpAcsUrl;
            txtUrlArtifact.Text = sp.SpArtifactUrl;
            cmbSamlAttributeSet.SelectedValue = sp.SamlAttributeSet.ToString();
            hlCert.NavigateUrl = String.Format("x509detail?{0}={1}", KeyDef.ParmKeys.SiteAdmin.SpAdmin.x509_CertID,
                sp.CertID);
            hlCert.Text = sp.CertID.ToString();
            txtSamlBindingUriIdpToSp.Text = sp.SamlBindingIdpToSp;
            txtSamlBindingUriSpToIdp.Text = sp.SamlBindingSpToIdp;
        }

        protected void bttClearSPCache_Click(object sender, EventArgs e)
        {
            var spCache = new ServiceProviderCacheFactory();
            spCache.TouchCacheFile();
        }
    }
}