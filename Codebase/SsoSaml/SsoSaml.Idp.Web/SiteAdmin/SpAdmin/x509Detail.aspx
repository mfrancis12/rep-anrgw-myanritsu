﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="x509Detail.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin.x509Detail" %>
<%@ Register src="~/App_Controls/Admin_SpMenu.ascx" tagname="Admin_SpMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<uc1:Admin_SpMenu ID="Admin_SpMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<div class="settingrow group input-text">
    <span class="settinglabelrgt-15">CertID:</span>
    <asp:Literal ID="ltrCertID" runat="server"></asp:Literal>
</div>
<div class="settingrow group input-text">
    <span class="settinglabelrgt-15">Internal Name:</span>
    <asp:TextBox ID="txtCertInternalName" runat="server" SkinID="tbx-400"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvCertInternalName" runat="server" ControlToValidate="txtCertInternalName">required!</asp:RequiredFieldValidator>
</div>
<div class="settingrow group input-textarea">
    <span class="settinglabelrgt-15">Internal Notes:</span>
    <asp:TextBox ID="txtCertInternalNotes" runat="server" SkinID="tbx-400" TextMode="MultiLine" Rows="10"></asp:TextBox>
</div>
<div class="settingrow group input-text">
    <span class="settinglabelrgt-15">x.509 Public Key (.cer):</span>
    <asp:FileUpload ID="fupCertPublicKey" runat="server" />
    <asp:LinkButton ID="lbttCertPublicKey" runat="server" Text="download" CausesValidation="false"
        onclick="lbttCertPublicKey_Click"></asp:LinkButton>
</div>
<div class="settingrow group input-text">
    <span class="settinglabelrgt-15">x.509 Private Key (.pfx):</span>
    <asp:FileUpload ID="fupCertPrivateKey" runat="server" />
    <asp:LinkButton ID="lbttCertPrivateKey" runat="server" Text="download"  CausesValidation="false"
        onclick="lbttCertPrivateKey_Click"></asp:LinkButton>
</div>
<div class="settingrow group input-password">
    <span class="settinglabelrgt-15">x.509 Private Key Password:</span>
    <asp:TextBox ID="txtCertPrivateKeyPwd" runat="server" SkinID="tbx-200"></asp:TextBox>
</div>
<div class="settingrow group input-text">
    <span class="settinglabelrgt-15">x.509 Private Key Exp:</span>
    <asp:TextBox ID="txtCertPfxExp" runat="server" SkinID="tbx-100"></asp:TextBox>
</div>
<div class="settingrow group">
    <span class="settinglabelrgt-15">&nbsp;</span>
    <asp:Button ID="bttCertSave" runat="server" Text="Save" onclick="bttCertSave_Click" CssClass="button" />
    <asp:Button ID="bttClearCache" runat="server" Text="Clear Certificate Cache" onclick="bttClearCache_Click" CssClass="button" />
</div>
<div class="settingrow">
    <p class="msg"><asp:Literal ID="ltrMsg" runat="server"></asp:Literal></p>
</div>
<div class="settingrow">
    <fieldset>
        <legend>Service Providers</legend>
        <asp:GridView ID="gvSP" runat="server" AutoGenerateColumns="false">
            <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="SpTokenID" DataNavigateUrlFormatString="spdetail?sptkn={0}" DataTextField="SpTokenID" HeaderText="SpTokenID" />
            <asp:BoundField DataField="ServiceProviderName" HeaderText="Service Provider Name" />
            <asp:BoundField DataField="SpHostName" HeaderText="Host Name" />
            <asp:BoundField DataField="SpResKeyPrefix" HeaderText="Resource Key Prefix" />
            </Columns>
        </asp:GridView>
    </fieldset>
</div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
