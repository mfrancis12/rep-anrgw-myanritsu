﻿using System;
using System.Web;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using Anritsu.AnrCommon.CoreLib;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin
{
    public partial class x509Detail : App_Lib.UI.BasePageSiteAdminIdp
    {
        private Int32 CertId
        {
            get
            {
                return
                    ConvertUtility.ConvertToInt32(
                        Request.QueryString[App_Lib.UI.KeyDef.ParmKeys.SiteAdmin.SpAdmin.x509_CertID], 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitCertInfo();
            }
        }

        private void InitCertInfo()
        {
            if (CertId < 1)
            {
//new
                lbttCertPrivateKey.Visible = lbttCertPublicKey.Visible = false;
                return;
            }

            var r = Lib.SingleSignOn.SamlCertStoreBLL.Select(CertId);
            if (r == null) throw new HttpException(404, "Page not found.");
            ltrCertID.Text = r["CertID"].ToString();
            txtCertInternalName.Text = r["InternalName"].ToString();
            txtCertInternalNotes.Text = r["InternalNote"].ToString();
            txtCertPrivateKeyPwd.Text = r["x509PfxPwd"].ToString();
            txtCertPfxExp.Text = r["x509PfxExpOnUTC"].ToString();

            gvSP.DataSource = Lib.SingleSignOn.GwIdpServiceProviderBLL.SelectByCertID(CertId);
            gvSP.DataBind();
        }

        protected void bttCertSave_Click(object sender, EventArgs e)
        {
            ltrMsg.Text = String.Empty;
            if (!Page.IsValid) return;
            SaveCert();
        }

        private void SaveCert()
        {
            try
            {
                Byte[] cerFile = null;
                Byte[] pfxFile = null;
                var pfxPwd = txtCertPrivateKeyPwd.Text.Trim();
                var pfxExpDate = ConvertUtility.ConvertToDateTime(txtCertPfxExp.Text.Trim(), DateTime.MinValue);

                if (fupCertPublicKey.HasFile)
                {
                    var hpfCert = fupCertPublicKey.PostedFile;
                    cerFile = new Byte[hpfCert.ContentLength];
                    hpfCert.InputStream.Read(cerFile, 0, hpfCert.ContentLength);
                    //if (cerFile == null) throw new ArgumentException("Invalid Public Key file.");
                    new X509Certificate2(cerFile);
                    //if (cert == null) throw new ArgumentException("Invalid Public Key file (.cer).");
                }

                if (fupCertPrivateKey.HasFile)
                {
                    var hpfPfx = fupCertPrivateKey.PostedFile;
                    pfxFile = new Byte[hpfPfx.ContentLength];
                    hpfPfx.InputStream.Read(pfxFile, 0, hpfPfx.ContentLength);
                    //if (pfxFile == null) throw new ArgumentException("Invalid Private Key file.");
                    new X509Certificate2(pfxFile, pfxPwd, X509KeyStorageFlags.MachineKeySet);
                    //if (pfx == null) throw new ArgumentException("Invalid Private Key file (.pfx).");
                }

                if (CertId < 1) //new
                {
                    var r = Lib.SingleSignOn.SamlCertStoreBLL.Insert(txtCertInternalName.Text.Trim(), cerFile, pfxFile,
                        pfxPwd, pfxExpDate, txtCertInternalNotes.Text.Trim());
                    if (r == null) throw new ArgumentException("Unable to add new cert.");
                    var certId = ConvertUtility.ConvertToInt32(r["CertID"], 0);
                    if (certId < 1) throw new ArgumentException("Unable to add new cert.");
                    Response.Redirect(String.Format("x509detail?{0}={1}",
                        App_Lib.UI.KeyDef.ParmKeys.SiteAdmin.SpAdmin.x509_CertID, certId));
                }
                else
                {
                    Lib.SingleSignOn.SamlCertStoreBLL.Update(CertId, txtCertInternalName.Text.Trim(), cerFile, pfxFile,
                        pfxPwd, pfxExpDate, txtCertInternalNotes.Text.Trim());
                    Response.Redirect(String.Format("x509detail?{0}={1}",
                        App_Lib.UI.KeyDef.ParmKeys.SiteAdmin.SpAdmin.x509_CertID, CertId));
                }
            }
            catch (ArgumentException aEx)
            {
                ltrMsg.Text = "Error: " + aEx.Message;
            }
        }

        protected void lbttCertPrivateKey_Click(object sender, EventArgs e)
        {
            var r = Lib.SingleSignOn.SamlCertStoreBLL.Select(CertId);
            if (r == null) return;
            var fileName = String.Format("x509_{0}.pfx", r["CertID"]);
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
            Response.BinaryWrite((Byte[]) r["x509Pfx"]);
        }

        protected void lbttCertPublicKey_Click(object sender, EventArgs e)
        {
            var r = Lib.SingleSignOn.SamlCertStoreBLL.Select(CertId);
            if (r == null) return;
            var fileName = String.Format("x509_{0}.cer", r["CertID"]);
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
            Response.BinaryWrite((Byte[]) r["x509Cer"]);
        }

        protected void bttClearCache_Click(object sender, EventArgs e)
        {
            var certCache = new App_Lib.AppCache.CertCacheFactory();
            certCache.TouchCacheFile();
            ltrMsg.Text = "Certificate cache cleared.";
        }
    }
}