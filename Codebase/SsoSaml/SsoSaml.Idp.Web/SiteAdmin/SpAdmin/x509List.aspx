﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="x509List.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin.x509List" %>
<%@ Register src="~/App_Controls/Admin_SpMenu.ascx" tagname="Admin_SpMenu" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
<uc1:Admin_SpMenu ID="Admin_SpMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
<asp:GridView ID="gvCerts" runat="server" AutoGenerateColumns="false">
    <Columns>
        <asp:HyperLinkField DataNavigateUrlFields="CertID" DataNavigateUrlFormatString="x509detail?certid={0}" DataTextField="CertID" HeaderText="CertID" />
        <asp:BoundField DataField="InternalName" HeaderText="Internal Name" />
        <asp:BoundField DataField="x509PfxExpOnUTC" HeaderText="x509PfxExp (UTC)" DataFormatString="{0:MMM-dd-yyyy}" />
        <asp:BoundField DataField="ModifiedOnUTC" HeaderText="ModifiedOnUTC" DataFormatString="{0:MMM-dd-yyyy}" />
    </Columns>
</asp:GridView>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
