﻿using System;

namespace Anritsu.SsoSaml.Idp.Web.SiteAdmin.SpAdmin
{
    public partial class x509List : App_Lib.UI.BasePageSiteAdminIdp
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindCerts();
            }
        }

        private void BindCerts()
        {
            gvCerts.DataSource = Lib.SingleSignOn.SamlCertStoreBLL.SelectAll();
            gvCerts.DataBind();
        }
    }
}