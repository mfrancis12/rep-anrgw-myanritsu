﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_MasterPages/MasterPageSiteDefault.Master" AutoEventWireup="true" CodeBehind="contact-us.aspx.cs" Inherits="Anritsu.SsoSaml.Idp.Web.contact_us" %>

<%@ Register TagPrefix="sbk" Assembly="Subkismet" Namespace="Subkismet.Captcha" %>
<%@ Register TagPrefix="AjaxControlToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<style type="text/css">
        .settinglabelleft
        {
            text-align: left;
            clear: left;
            float: left;
            font-weight: bold;
            margin-right: 10px;
            width: 15em;
        }

        #contactusBox
        {
            margin: 10px 10px 10px 70px;
            width: 630px;
        }

        .tbx-600
        {
            border: 1px solid #CCC;
            font-size: 11px;
            padding: 5px 0px 5px 5px;
            width: 600px;
        }
        #crumbcontainer
        {
            display: none;
        }
        #contactusBox 
        {
             font-size:12px !important; 
        }
        center { margin:10px 0px 0px 0px;}
        .thxcontent { margin-top:90px;color:black;}
    </style>
    <script type="text/javascript">
        function validatelimit(obj, maxchar) {
            if (this.id) obj = this;
            var remaningChar = maxchar - obj.value.length;
            if (remaningChar <= 0) {
                obj.value = obj.value.substring(maxchar, 0);
                return false;
            }
            else { return true; }
        }
        $("document").ready(function () {
            if (window.location.href.indexOf('ja-JP') > 0) {
                $("input[name$='txtLastName']").parent("div").insertBefore($("input[name$='txtFirstName']").parent("div"));
                $("#lbFirstName").text("");
            } else {
                $("#lbLastName").text("");
            }
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTopList" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft1" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContentTop" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphContentCenterLeft" runat="server">
    <asp:ScriptManager ID="smContactus" runat="server" />
    <div id="contactusBox" class="">
        <anrui:GlobalWebBoxedPanel ID="pnlContactUs" runat="server" Width="400"
            DefaultButton="btnSubmit" Height="280">
            <div class="dialogue-header">
                <h3> <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:~/contact-us,pnlContactUs.HeaderText %>" /></h3>
            </div>
            <div class="dialogue-body">
                <div class="content-detail">
						<div class="form-container">
							<div class="bd width-60">
                                <div class="group input-text required">
                                    <label>
                                        <asp:Localize ID="lcalFirstName" runat="server" Text="<%$Resources:~/contact-us, lcalFirstName.Text %>"></asp:Localize>:*
                                    </label>
                                    <p class="error-message"><asp:Localize ID="Localize1" runat="server" Text="<%$Resources:~/contact-us,rfvFirstName.ErrorMessage%>"></asp:Localize></p>
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" Enabled="False"
                                        ErrorMessage="<%$Resources:~/contact-us,rfvFirstName.ErrorMessage%>"
                                        runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs" />
                                    <AjaxControlToolkit:ValidatorCalloutExtender ID="vceFirstName" runat="server" Enabled="False" 
                                        TargetControlID="rfvFirstName" />
                                </div>
                                <div class="group input-text required">
                                     <label>
                                        <asp:Localize ID="lcalLastName" runat="server" Text="<%$Resources:~/contact-us, lcalLastName.Text %>"></asp:Localize>:*
                                    </label>
                                    <p class="error-message"><asp:Localize ID="Localize3" runat="server" Text="<%$Resources:~/contact-us,rfvLastName.ErrorMessage%>"></asp:Localize></p>
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </div> 
                                <div class="group input-text required">
                                     <label>
                                        <asp:Localize ID="lcalEmail" runat="server" Text="<%$Resources:~/contact-us, lcalEmailAddress.Text %>"></asp:Localize>:*
                                    </label>
                                    <p class="error-message"><asp:Localize ID="Localize4" runat="server" Text="<%$Resources:~/contact-us,rfvEmailAddress.ErrorMessage%>"></asp:Localize></p>
                                    <p class="custom-error-message"><asp:Localize ID="Localize2" runat="server" Text="<%$Resources:~/contact-us,rfvEmailAddress.ErrorMessage%>"></asp:Localize></p>
                                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="email"></asp:TextBox>
                                </div> 
                                <div class="group input-text">
                                     <label>
                                        <asp:Localize ID="lcalUrl" runat="server" Text="<%$Resources:~/contact-us, lcalUrl.Text %>"></asp:Localize>:
                                    </label>
                                   
                                    <asp:TextBox ID="txtUrl" runat="server" SkinID="tbx-600" ReadOnly="true"></asp:TextBox>
                                </div> 
                                <div class="group input-textarea required">
                                     <label>
                                        <asp:Localize ID="lcalQuestions" runat="server" Text="<%$Resources:~/contact-us, lcalQuestions.Text %>"></asp:Localize>:* 
                                    </label>
                                   <p class="error-message"><asp:Localize ID="Localize5" runat="server" Text="<%$Resources:~/contact-us,rfvtxtComments.ErrorMessage%>"></asp:Localize></p>
                                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" MaxLength="2000" onkeyup="validatelimit(this,2000)"></asp:TextBox>
                                </div> 
                                <div class="group margin-top-15">
                                    <span class="settinglabelrgt-15" style="width: 50em!Important">&nbsp;</span>
                                    <asp:Button ID="btnSubmit" runat="server" Text="<%$Resources:Common, bttSubmit.Text%>" ValidationGroup="vgContactUs" OnClick="btnSubmit_Click" CssClass="form-submit button" />
                                </div>
                                </div>

                            </div>
                    </div>
            </div>
             <%--<div class="settingrow">
                <span class="settinglabelleft" style="width: 20em!Important">
                    <asp:Localize ID="lcalFirstName" runat="server" Text="<%$Resources:~/contact-us, lcalFirstName.Text %>"></asp:Localize>:<label id="lbFirstName">*</label>
                </span>
                <br />
                <asp:TextBox ID="txtFirstName" runat="server" SkinID="tbx-300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" Enabled="False"
                    ErrorMessage="<%$Resources:~/contact-us,rfvFirstName.ErrorMessage%>"
                    runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs" />
                <AjaxControlToolkit:ValidatorCalloutExtender ID="vceFirstName" runat="server" Enabled="False" 
                    TargetControlID="rfvFirstName" />
            </div>--%>
            <%--<div class="settingrow">
                <span class="settinglabelleft" style="width: 20em!Important">
                    <asp:Localize ID="lcalLastName" runat="server" Text="<%$Resources:~/contact-us, lcalLastName.Text %>"></asp:Localize>:<label id="lbLastName">*</label>
                </span>
                <br />
                <asp:TextBox ID="txtLastName" runat="server" SkinID="tbx-300"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfvLastName" ControlToValidate="txtLastName" Enabled="False"
                    ErrorMessage="<%$Resources:~/contact-us,rfvLastName.ErrorMessage%>"
                    runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs" />
                <AjaxControlToolkit:ValidatorCalloutExtender ID="vceLastName" runat="server" Enabled="False"
                    TargetControlID="rfvLastName" />
            </div>--%>
            <%--<div class="settingrow">
                <span class="settinglabelleft" style="width: 20em!Important">
                    <asp:Localize ID="lcalEmail" runat="server" Text="<%$Resources:~/contact-us, lcalEmailAddress.Text %>"></asp:Localize>:*                  
                    
                </span>
                <br />
                <asp:TextBox ID="txtEmailAddress" runat="server" SkinID="tbx-300"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmailId" ControlToValidate="txtEmailAddress"
                    ErrorMessage="<%$Resources:~/contact-us,rfvEmailAddress.ErrorMessage%>"
                    runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs" />
                <AjaxControlToolkit:ValidatorCalloutExtender ID="EmailValidatorCalloutExtender" runat="server"
                    TargetControlID="rfvEmailId" />
                <asp:RegularExpressionValidator ID="reEmailRegExp" runat="server" ErrorMessage="<%$Resources:~/contact-us,InvalidEmail.ErrorMessage%>"
                    ControlToValidate="txtEmailAddress" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs"
                    ValidationExpression="\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*\s*"></asp:RegularExpressionValidator>
                <AjaxControlToolkit:ValidatorCalloutExtender ID="emailVCE2" runat="server" TargetControlID="reEmailRegExp" />
            </div>--%>
           <%-- <div class="settingrow">
                <span class="settinglabelleft" style="width: 50em!Important">
                     <asp:Localize ID="lcalUrl" runat="server" Text="<%$Resources:~/contact-us, lcalUrl.Text %>"></asp:Localize>:
                </span>
                <br />
                <asp:TextBox ID="txtUrl" runat="server" SkinID="tbx-600" ReadOnly="true"></asp:TextBox>
            </div>--%>
            <%--<div class="settingrow">
                <span class="settinglabelleft" style="width: 50em!Important">
                  <asp:Localize ID="lcalQuestions" runat="server" Text="<%$Resources:~/contact-us, lcalQuestions.Text %>"></asp:Localize>:*                     
                </span>
                <br />
                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Height="90px" SkinID="tbx-600" MaxLength="2000" onkeyup="validatelimit(this,2000)"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtComments" ControlToValidate="txtComments"
                    ErrorMessage="<%$Resources:~/contact-us,rfvtxtComments.ErrorMessage%>"
                    runat="server" SetFocusOnError="true" Display="None" ValidationGroup="vgContactUs" />
                <AjaxControlToolkit:ValidatorCalloutExtender ID="vceComments" runat="server"
                    TargetControlID="rfvtxtComments" />
            </div>--%>
            <%--<center>
                <sbk:CaptchaControl ID="captchactrl" LayoutStyle="CssBased" runat="server" CssClass="captcha" EnableClientScript="false"
                    ErrorMessage="<%$Resources:Common, lcalCaptchaInvalidCode.ErrorMessage %>" CaptchaLength="4" ForeColor="Red"
                    ValidationGroup="vgContactUs" Display="Dynamic"  />
                <div class="clearfloat">
                </div>
                <div class="settingrow">
                    <span class="settinglabelrgt-15" style="width: 50em!Important">&nbsp;</span>
                    <asp:Button ID="btnSubmit" SkinID="btngreenbg" runat="server" Text="<%$Resources:Common, bttSubmit.Text%>" ValidationGroup="vgContactUs" OnClick="btnSubmit_Click" />
                </div>
            </center>--%>
        </anrui:GlobalWebBoxedPanel>
        <asp:Panel ID="pnlThanks" runat="server" Visible="false" CssClass="thxcontent">
            <asp:Localize ID="Localize7" runat="server" Text="<%$Resources:~/contact-us, lcalThankyouContent.Text%>"></asp:Localize><br />
             <br />
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphContentCenterRight" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cphContentBottom" runat="server">
</asp:Content>
