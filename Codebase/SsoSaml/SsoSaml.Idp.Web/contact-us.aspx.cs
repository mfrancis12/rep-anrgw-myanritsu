﻿using System;
using System.Collections.Generic;
using Anritsu.AnrCommon.EmailSDK;
using Anritsu.AnrCommon.EmailSDK.EmailWebRef;
using Anritsu.AnrCommon.CoreLib;
using Anritsu.SsoSaml.Idp.Web.App_Lib.UI;
using Anritsu.SsoSaml.Idp.Lib.Utility;
using System.Data;
using Anritsu.SsoSaml.Idp.Lib.SingleSignOn;

namespace Anritsu.SsoSaml.Idp.Web
{
    public partial class contact_us : Anritsu.SsoSaml.Idp.Web.App_Lib.UI.IdpBasePage
    {
        #region Private Fields

        private int _cultureGroupID;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            if (!Request.Url.AbsoluteUri.Contains("ja-JP"))
            {
              //  rfvFirstName.Enabled = vceFirstName.Enabled = true;
            }
            else
            {
               // rfvLastName.Enabled = vceLastName.Enabled = true;
            }
            _cultureGroupID = 1;
            txtUrl.Text = (null == Request.UrlReferrer) ? string.Empty : Request.UrlReferrer.ToString();
            if (!Page.User.Identity.IsAuthenticated) return;
            var userData = UIHelper.LoggedInUser_Get(false);
            if (userData == null) return;
            var firstName = Convert.ToString(userData["FirstName"]);
            var lastName = Convert.ToString(userData["LastName"]);
            var emailAddress = Convert.ToString(userData["EmailAddress"]).ToLowerInvariant();
            if (!firstName.IsNullOrEmptyString()) txtFirstName.Text = firstName;
            if (!lastName.IsNullOrEmptyString()) txtLastName.Text = lastName;
            if (!emailAddress.IsNullOrEmptyString()) txtEmailAddress.Text = emailAddress;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid || !IsPageValid()) return;
            if (SendMail() != 0) return;
            //  pnlContactUs.Visible = false;
            //Add Log
            try
            {
                _cultureGroupID = GetCurrentCultureGroupId();
                GwIdpUserActivityBLL.InsertContactUSLog(txtFirstName.Text.Trim(), txtLastName.Text.Trim(), txtEmailAddress.Text.Trim(), txtUrl.Text.Trim(), txtComments.Text.Trim(), _cultureGroupID);
            }
            catch
            {
               pnlThanks.Visible = true;
            }
            pnlThanks.Visible = true;
        }
        protected int SendMail()
        {
            var templatedReq = new SendTemplatedDistributedEmailCallRequest
            {
                EmailTemplateKey = KeyDef.EmailTemplates.CONTACT_GLOBALWEB_REQUEST,
                DistributionListKey = "Contact_GloablWeb_DistributionList_US",
                CultureGroupId = _cultureGroupID
            };

            #region subject replacement values

            var subjectKeys = new List<ReplaceKeyValueType>();
            var keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAIL]]",
                ReplacementValue = txtEmailAddress.Text.Trim().HTMLEncode()
            };
            subjectKeys.Add(keyValue);

            #endregion

            #region body replacement values

            var bodyKeys = new List<ReplaceKeyValueType>();

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[FIRSTNAME]]",
                ReplacementValue = txtFirstName.Text.Trim().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[LASTNAME]]",
                ReplacementValue = txtLastName.Text.Trim().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[EMAIL]]",
                ReplacementValue = txtEmailAddress.Text.Trim().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[COMMENTS]]",
                ReplacementValue = txtComments.Text.Trim().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[REPORTEDLINK]]",
                ReplacementValue = txtUrl.Text.Trim().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType {TemplateKey = "[[COUNTRY]]"};
            //get the country from Geoip
            string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"].ToString();
            DataRow dr = GeoIPBLL.GetCountryByIP(ipAddress);
            keyValue.ReplacementValue = (null != dr) ? dr["ISOCountryCode"].ToString() : string.Empty;
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType {TemplateKey = "[[IPADDRESS]]", ReplacementValue = ipAddress};
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType {TemplateKey = "[[BROWSER]]", ReplacementValue = Request.Browser.Browser};
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType {TemplateKey = "[[VERSION]]", ReplacementValue = Request.Browser.Version};
            bodyKeys.Add(keyValue);

            keyValue = new ReplaceKeyValueType
            {
                TemplateKey = "[[UTCDATE]]",
                ReplacementValue = DateTime.UtcNow.ToString().HTMLEncode()
            };
            bodyKeys.Add(keyValue);

            #endregion
            templatedReq.SubjectReplacementKeyValues = subjectKeys.ToArray();
            templatedReq.BodyReplacementKeyValues = bodyKeys.ToArray();
            var emailCallResponse = EmailServiceManager.SendDistributedTemplatedEmail(templatedReq);
            if (emailCallResponse.StatusCode != 0)
            {
                throw new ArgumentException(emailCallResponse.ResponseMessage);
            }
            return emailCallResponse.StatusCode;
        }

        private bool IsPageValid()
        {
            return !String.IsNullOrEmpty(txtEmailAddress.Text.Trim()) && !String.IsNullOrEmpty(txtComments.Text.Trim());
        }
    }
}