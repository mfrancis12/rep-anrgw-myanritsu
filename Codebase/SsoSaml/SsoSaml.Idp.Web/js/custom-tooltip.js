(function ( $ ) {
	$.fn.customToolTip = function(toolTipParams)
	{
		var toolTipOptions = $.extend(
			{
				'selector':'#tooltip',
				'htmlContent':'This is default tooltip content'
			},toolTipParams);
			
		if(toolTipOptions.selector)
			{
				$('body').on('click', toolTipOptions.selector, function(event) {
						if($(".customtooltip").length < 1)
						{
							$('<div class="customtooltip"></div>').insertAfter(toolTipOptions.selector);
							if(toolTipOptions.htmlContent)
								$(".customtooltip").html(toolTipOptions.htmlContent);
						}
						var el = $(this);
						var position = $(el).position();
						$('.customtooltip').css('display','none');
						$(this).siblings('.customtooltip').css( {position:"absolute", top:(position.top+5), left: (position.left+getControlWidth(toolTipOptions.selector)),display:'block'});
						event.stopPropagation();
				});
				
				$('body').on('focus',toolTipOptions.selector,function(event) {
						if($(".customtooltip").length < 1)
						{
							$('<div class="customtooltip"></div>').insertAfter(toolTipOptions.selector);
							if(toolTipOptions.htmlContent)
								$(".customtooltip").html(toolTipOptions.htmlContent);
						}
						var el = $(this);
						var position = $(el).position();
						$('.customtooltip').css('display','none');
						$(this).siblings('.customtooltip').css( {position:"absolute", top:(position.top+5), left: (position.left+getControlWidth(toolTipOptions.selector)),display:'block'});
						event.stopPropagation();
				});
				
				$(toolTipOptions.selector).blur( function(event) {
						$(this).siblings('.customtooltip').css('display','none');
						//$(".customtooltip").remove();
				});
				
				getControlWidth = function(selector)
				{
					return $(selector).width() + 25;
				}
				
				$("html").click( function(event) {
					$(".customtooltip").css("display","none");
					 //$(".customtooltip").remove();
				});	
			}
	}
}(jQuery));