$(document).ready(function(){
		var ruleSet = [];
		var successCount = 0;
        var message_text = pwdRules["PasswordGuideLines.Text"];
        $("#pGuidelinesText").html(message_text);
        $('body').on('keyup',pwdId,function () {
            var pwdvalue = $(pwdId).val();
		    ruleSet = $('.pwd-guidelines ul > li');
		    var rule = validateRules(pwdvalue);
		    getStrength(pwdvalue);
		})
		$('body').on('click', pwdId, function () {
		    var pwdvalue = $(pwdId).val();
		    if (pwdvalue.length > 0) {
		        ruleSet = $('.pwd-guidelines ul > li');
		        var rule = validateRules(pwdvalue);
		        getStrength(pwdvalue);
		    }
		});
		
		function getStrength(val){		    
		    var strength = 'weak';
             if($(ruleSet[0]).hasClass('success') 
			 && $(ruleSet[1]).hasClass('success') 
			 && $(ruleSet[2]).hasClass('success')) 
				{
					strength = 'good';
				}
			if(strength == 'good' && $(ruleSet[3]).hasClass('success'))
				{
					strength = 'strong';
				}

			switch(strength)
			{
				case 'weak':
					$(".strength-scale span").removeClass();
					$(".strength-scale span").addClass("weak");
					$(".strength").html(pwdRules["PasswordWeak.Text"]);
					//alert("" + pwdRules["PasswordWeak.Text"]);
				break;
				case 'good':
					$(".strength-scale span").removeClass();
					$(".strength-scale span").addClass("good");
					//alert("" + pwdRules["PasswordWeak.Text"]);
					$(".strength").html(pwdRules["PasswordGood.Text"]);

				break;
				case 'strong':
					$(".strength-scale span").removeClass();
					$(".strength-scale span").addClass("strong");
					$(".strength").html(pwdRules["PasswordStrong.Text"]);
				break;
				default:							
					$(".strength-scale span").addClass("weak");
					$(".strength").html(pwdRules["PasswordWeak.Text"]);
				break;
			}
		}
		function validateRules(value)
		{	
			if(value.length > 7)
			{
				$(ruleSet[0]).removeClass('fail');
				$(ruleSet[0]).addClass('success');
			}
			else
			{
				$(ruleSet[0]).removeClass('success');
				$(ruleSet[0]).addClass('fail');
			}
			
			if(checkForAlphabet(value))
			{
				$(ruleSet[1]).removeClass('fail');
				$(ruleSet[1]).addClass('success');
			}
			else
			{
				$(ruleSet[1]).removeClass('success');
				$(ruleSet[1]).addClass('fail');
			}
				
			if(checkForNumeric(value))
			{
				$(ruleSet[2]).removeClass('fail');
				$(ruleSet[2]).addClass('success');
			}
			else
			{
				$(ruleSet[2]).removeClass('success');
				$(ruleSet[2]).addClass('fail');
			}
			
			if(checkForSpecialChar(value))
			{
				$(ruleSet[3]).removeClass('fail');
				$(ruleSet[3]).addClass('success');
			}
			else
			{
				$(ruleSet[3]).removeClass('success');
				$(ruleSet[3]).addClass('fail');
			}
			
		}
		
});

/*var message_text = pwdRules["PasswordGuideLines.Text"];
$("#pGuidelinesText").html(message_text); */
    function showHelp(){
        var message_text = pwdRules["PasswordGuideLines.Text"];
            $("#pGuidelinesText").html(message_text);
            $(".guidelines").css("width", "450");
            $(".guidelines").animate({ height: "toggle" }, 800, function () {
        });
    }
    function help() {
        $(pwdId).keyup(function () {
            var pwdvalue = $(pwdId).val();
            ruleSet = $('.pwd-guidelines ul > li');
            var rule = validateRules(pwdvalue);
            getStrength(pwdvalue);
        });
    }
    function validate(controlToValidate,currentVal){
	    switch(controlToValidate)
	    {
		    case 'old-password':
			    validateOldPassword(controlToValidate,currentVal);
		    break;
		    case 'new-password':
			    validateNewPassword(controlToValidate,currentVal)
		    break;
		    case 'repeat-password':
			    validateRepeatPassword(controlToValidate,currentVal)
		    break;
	    }
    }

var oldPassword = "";
    function validateOldPassword(controlId,val){
	    if(val.length == 0)
	    {
		    setMessage(pwdRules["CurrentPasswordRequired.ErrorMessage"], controlId);
		    return false;
	    }
	    else
	    {
		    setMessage('',controlId);
		    oldPassword = val;
		    return true;
	    }
	
    }

var newPassword = "";
    function validateNewPassword(controlId,val){
        newPassword = val;
        if (val.length == 0) {
            setMessage(pwdRules["NewPasswordRequired.ErrorMessage"], controlId);
		    return false;
	    }else if(val.length < 8){
	        setMessage(pwdRules["PasswordLength.ErrorMessage"], controlId);
		    return false;
	    }else if(!checkForAlphabet(val)){	
	        setMessage(pwdRules["PasswordMustContainOneAlphabet.ErrorMessage"], controlId);
		    return false;
	    }else if(!checkForNumeric(val)){	
	        setMessage(pwdRules["PasswordMustContainOneNumeric.ErrorMessage"], controlId);
		    return false;
	    }else if(!checkForOptionalSpecialChar(val)){	
	        setMessage(pwdRules["PasswordInvalidCharacters.ErrorMessage"], controlId);
		    return false;
	    }else if(compareOldNewPassword(val)){
		    setMessage(pwdRules["SameNewOldPassword.ErrorMessage"], controlId);
		    return false;
	    }else{
		    setMessage('',controlId);
		    newPassword = val;
		    return true;
	    }
    }

    function validateRepeatPassword(controlId,val){
        if (!comparePasswords(val))
	    {
		    //setMessage('New password and confirm password does not match',controlId);
		    setMessage(pwdRules["PasswordDoesNotMatch.ErrorMessage"],controlId);
		    return false;
	    }
	    else
	    {
		    setMessage('',controlId);
		    return true;
	    }
	
    }

    function comparePasswords(repeatPassword){
        if(newPassword == repeatPassword)
		    return true;
	    else
		    return false;
    }

    function compareOldNewPassword(newPasswordText){
        if(newPasswordText == oldPassword)
		    return true;
	    else
		    return false;
    }

    function setMessage(message,ctrlId){
	    $('#'+ctrlId).html(message);
	    $('#'+ctrlId).css('display','block');
    }

    function checkForAlphabet(val){
	    var pattern = /[a-zA-Z]/
	    var result = val.search(pattern);
	    if(result != -1)
		    return true;
	    else
		    return false;
    }

    function checkForNumeric(val)
    {
	    var pattern = /[0-9]/
	    var result = val.search(pattern);
	    if(result != -1)
		    return true;
	    else
		    return false;
    }

    function checkForSpecialChar(val)
    {
	    var pattern = /[!@#$%^*]/
	    var result = val.search(pattern);
	    if(result != -1)
		    return true;
	    else
		    return false;
    }

    function checkForOptionalSpecialChar(val)
    {
	    var pattern = /[^a-zA-Z0-9!@#$%^*]/
	    var result = val.search(pattern);
	    if(result != -1)
		    return false;
	    else
		    return true;
    }

    function validateOnSubmit()
    {	
	    var oldpwd = validateOldPassword('old-password',$('[id$=txtOldPwd]').val());
	    var newpwd = validateNewPassword('new-password',$('[id$=txtNewPwd]').val());
	    var repeatpwd = validateRepeatPassword('repeat-password',$('[id$=txtNewPwdConfirm]').val());
	
	    if(!oldpwd || !newpwd || !repeatpwd)
		    return false;
	    else
		    return true;
	
    }
    function validateOnSubmitReset() {
        var newpwd = validateNewPassword('new-password', $('[id$=txtNewPasswordText]').val());
        var repeatpwd = validateRepeatPassword('repeat-password', $('[id$=txtReType]').val());

        if (!newpwd || !repeatpwd)
            return false;
        else
            return true;

    }





			