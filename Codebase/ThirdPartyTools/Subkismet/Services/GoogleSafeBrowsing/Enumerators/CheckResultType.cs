﻿namespace Subkismet.Services.GoogleSafeBrowsing
{
    /// <summary>
    /// Enumerator for Google Safe Browsing check results.
    /// </summary>
    public enum CheckResultType
    {
        /// <summary>
        /// Safe link.
        /// </summary>
        Safe = 0,
        /// <summary>
        /// Phishing link.
        /// </summary>
        Phishing = 1,
        /// <summary>
        /// Malware link.
        /// </summary>
        Malware = 2
    }
}