namespace Subkismet.Services.GoogleSafeBrowsing
{
    /// <summary>
    /// Enumerator for Key type.
    /// </summary>
    public enum KeyType
    {
        /// <summary>
        /// Addition.
        /// </summary>
        Addition = 0,
        /// <summary>
        /// Deletion.
        /// </summary>
        Deletion = 1
    }
}