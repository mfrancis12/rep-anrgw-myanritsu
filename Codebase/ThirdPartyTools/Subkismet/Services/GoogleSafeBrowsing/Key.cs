using System;
using System.Collections.Generic;
using System.Text;
using Subkismet.Properties;

namespace Subkismet.Services.GoogleSafeBrowsing
{
    /// <summary>
    /// Represents a key in a blacklist.
    /// </summary>
    public class Key
    {
        /// <summary>
        /// Public constructor.
        /// </summary>
        public Key()
        {
        }

        /// <summary>
        /// Public constructor with parameters.
        /// </summary>
        /// <param name="type">The KeyType type of the key.</param>
        /// <param name="value">The value.</param>
        public Key(KeyType type, string value)
        {
            if (value == null)
                throw new ArgumentNullException("value", Resources.ArgumentNull_String);
            this.type = type;
            this.value = value;
        }

        private KeyType type;

        /// <summary>
        /// The type of the key (phishing or malware).
        /// </summary>
        public KeyType Type
        {
            get { return type; }
            set { type = value; }
        }

        private string value;

        /// <summary>
        /// The value of the key.
        /// </summary>
        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Compares the current Key object with the incoming object.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>A Boolean value that determines if two objects are equal or not.</returns>
        public override bool Equals(object obj)
        {
            Key key = obj as Key;

            if ((key.Type == this.type) && (key.Value == this.value))
                return true;
            return false;
        }

        /// <summary>
        /// Gets the hash code for the Key object.
        /// </summary>
        /// <returns>Integer value of the hash code.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
