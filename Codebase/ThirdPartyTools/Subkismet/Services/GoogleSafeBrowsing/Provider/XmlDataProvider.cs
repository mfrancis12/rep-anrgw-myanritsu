using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.Collections.Specialized;

namespace Subkismet.Services.GoogleSafeBrowsing.Provider
{
    /// <summary>
    /// XML implementation for DataProvider.
    /// </summary>
    public class XmlDataProvider : DataProvider
    {
        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="name">The name of the provider.</param>
        /// <param name="config">A NameValueCollection of configuration elements.</param>
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            string phishingPath = config["phishingFilePath"];
            this.phishingFilePath = phishingPath;

            string malwarePath = config["malwareFilePath"];
            this.malwareFilePath = malwarePath;
        }

        #region Properties

        /// <summary>
        /// The path of the phishing XML file.
        /// </summary>
        private string phishingFilePath;

        public string PhishingFilePath
        {
            get { return phishingFilePath; }
        }

        /// <summary>
        /// The path of the malware XML file.
        /// </summary>
        private string malwareFilePath;

        public string MalwareFilePath
        {
            get { return malwareFilePath; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a single key to a black list.
        /// </summary>
        /// <param name="key">The string value of the key to be added.</param>
        /// <param name="listType">The black list type.</param>
        public override void AddKey(string key, BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlDocument document = new XmlDocument();
            document.Load(listPath);

            XPathNavigator navigator = document.CreateNavigator();

            navigator.MoveToChild("list", string.Empty);
            navigator.MoveToChild("keys", string.Empty);

            using (XmlWriter writer = navigator.PrependChild())
            {
                writer.WriteStartElement("key");
                writer.WriteValue(key);
                writer.WriteEndElement();
                writer.Close();
            }

            document.Save(listPath);
        }

        /// <summary>
        /// Removes a single key from a black list.
        /// </summary>
        /// <param name="key">The string value of the key to remove.</param>
        /// <param name="listType">The black list type.</param>
        public override void RemoveKey(string key, BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlDocument document = new XmlDocument();
            document.Load(listPath);

            XPathNavigator navigator = document.CreateNavigator();

            XPathNavigator nodeToDelete = navigator.SelectSingleNode
                (string.Format("list/keys/key[. ='{0}']", key));

            if (nodeToDelete != null)
            {
                nodeToDelete.DeleteSelf();
                document.Save(listPath);
            }
        }

        /// <summary>
        /// Adds a list of keys to a black list.
        /// </summary>
        /// <param name="keys">A list of Ke objects to be added.</param>
        /// <param name="listType">The black list type.</param>
        public override void AddKeys(List<Key> keys, BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlDocument document = new XmlDocument();
            document.Load(listPath);

            XPathNavigator navigator = document.CreateNavigator();

            navigator.MoveToChild("list", string.Empty);
            navigator.MoveToChild("keys", string.Empty);

            using (XmlWriter writer = navigator.PrependChild())
            {
                foreach (Key key in keys)
                {
                    writer.WriteStartElement("key");
                    writer.WriteValue(key.Value);
                    writer.WriteEndElement();
                }
                writer.Close();
            }

            document.Save(listPath);
        }

        /// <summary>
        /// Returns a lis of all keys for a black list.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        /// <returns>List of string values for keys.</returns>
        public override List<string> GetKeys(BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlReader reader = XmlReader.Create(new MemoryStream(File.ReadAllBytes(listPath)));

            XPathDocument document = new XPathDocument(reader);
            XPathNavigator navigator = document.CreateNavigator();

            List<string> keys = new List<string>();

            foreach (XPathItem item in navigator.Select("/list/keys/key"))
            {
                keys.Add(item.Value);
            }

            return keys;
        }

        /// <summary>
        /// Returns the latest version for a black list.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        /// <returns>String value of the version.</returns>
        public override string GetLatestVersion(BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlReader reader = XmlReader.Create(new MemoryStream(File.ReadAllBytes(listPath)));

            XPathDocument document = new XPathDocument(reader);
            XPathNavigator navigator = document.CreateNavigator();

            navigator.MoveToChild("list", string.Empty);
            navigator.MoveToAttribute("version", string.Empty);

            return navigator.Value;
        }

        /// <summary>
        /// Updates the version of a black list.
        /// </summary>
        /// <param name="updateVersion">String value of the version.</param>
        /// <param name="listType">The black list type.</param>
        public override void UpdateVersion(string updateVersion, BlackListType listType)
        {
            string listPath = GetListTypePath(listType);

            XmlDocument document = new XmlDocument();
            document.Load(listPath);

            XPathNavigator navigator = document.CreateNavigator();

            navigator.MoveToChild("list", string.Empty);
            navigator.MoveToAttribute("version", string.Empty);

            navigator.SetValue(updateVersion);


            document.Save(listPath);
        }

        /// <summary>
        /// Clears the content of a black list.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        public override void Clear(BlackListType listType)
        {
            File.Delete(GetListTypePath(listType));

            CreateNewFile(listType, this.GetListTypePath(listType));
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Returns the path of a black list XML file.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        /// <returns>The path.</returns>
        private string GetListTypePath(BlackListType listType)
        {
            if (listType == BlackListType.Malware)
            {
                CreateNewFile(BlackListType.Malware, this.MalwareFilePath);
                return this.MalwareFilePath;
            }
            else
            {
                CreateNewFile(BlackListType.Phishing, this.PhishingFilePath);
                return this.PhishingFilePath;
            }
        }

        /// <summary>
        /// Creates a new black list file at the specified path.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        /// <param name="path">The file path.</param>
        private void CreateNewFile(BlackListType listType, string path)
        {
            if (!File.Exists(path))
            {
                File.WriteAllText(path, GetInitialXml(listType));
            }
        }

        /// <summary>
        /// Returns the initial XML value of a black list.
        /// </summary>
        /// <param name="listType">The black list type.</param>
        /// <returns>String value of the XML.</returns>
        private string GetInitialXml(BlackListType listType)
        {
            StringBuilder sb = new StringBuilder();

            StringWriterWithEncoding stringWriter = new StringWriterWithEncoding(sb, Encoding.UTF8);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings);

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("list");

            if (listType == BlackListType.Malware)
                xmlWriter.WriteAttributeString("type", "Malware");
            else
                xmlWriter.WriteAttributeString("type", "Phishing");

            xmlWriter.WriteAttributeString("version", "1.-1");

            xmlWriter.WriteStartElement("keys");
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();

            xmlWriter.Flush();
            xmlWriter.Close();
            stringWriter.Flush();

            return stringWriter.ToString();
        }

        #endregion
    }

    #region Helper classes

    /// <summary>
    /// Class to write XML files with custom encoding
    /// </summary>
    internal class StringWriterWithEncoding : StringWriter
    {
        private Encoding myEncoding;

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="sb">StrinbBuilder to write XML into it.</param>
        /// <param name="encoding">Encoding for the </param>
        public StringWriterWithEncoding(StringBuilder sb, Encoding encoding)
        {
            this.myEncoding = encoding;
        }

        /// <summary>
        /// ReadOnly value for the Encoding.
        /// </summary>
        public override Encoding Encoding
        { get { return this.myEncoding; } }
    }

    #endregion
}
