USE [sandbox_GlobalWeb]
--Email address label
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Email address'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='en'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'電子メールアドレス'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='ja'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'이메일'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='ko'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Email address'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='ru'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'电子邮件地址'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='zh'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'電子郵件帳戶'
where resourcekey='lcalUserEmailOrLoginName.Text' and classkey='~/account/resetpwd' and Locale='zh-TW'


--Email address required feild error
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Email address is Required'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='en'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'電子メールアドレスを入力してください。'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ja'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'이메일주소가 필요합니다'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ko'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Email address is Required'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ru'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'需要输入电子邮件地址。'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='zh'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'需要填入電子郵件帳戶。'
where resourcekey='rfvEmailOrLoginName.ErrorMessage' and classkey='~/account/resetpwd' and Locale='zh-TW'


--Email address not found 
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Invalid Email address'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='en'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'電子メールアドレスが正しく入力されていません。'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ja'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'유저가 발견되지 않습니다'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ko'
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'Пользователь не найден.'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='ru'
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'未找到用户。'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='zh'
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'未找到使用者。'
where resourcekey='lcalInvalidLoginNameorEmail.ErrorMessage' and classkey='~/account/resetpwd' and Locale='zh-TW'

--Reset pwd description
update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'&lt;strong&gt;You will be receiving an email with instructions after entering your email address below.&lt;/strong&gt;' 
where resourcekey='lcalResetPwdGuide.Text' and classkey='~/account/resetpwd' and Locale='en'


update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'&lt;strong&gt; Вам будет отправлено письмо с инструкциями на этот E-Mail &lt;/strong&gt;' 
where resourcekey='lcalResetPwdGuide.Text' and classkey='~/account/resetpwd' and Locale='ru'


update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'在下方输入您的电子邮件地址后，您将收到重置密码的说明邮件。' 
where resourcekey='lcalResetPwdGuide.Text' and classkey='~/account/resetpwd' and Locale='zh'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'請於下列輸入您的電子郵件帳戶，您将收到重置密碼的說明郵件。' 
where resourcekey='lcalResetPwdGuide.Text' and classkey='~/account/resetpwd' and Locale='zh-TW'

--Reset pwd Thank you page

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'<p>パスワード変更依頼を受け付けました。</p>
<p>お客さまの登録された電子メールアドレス宛に、パスワード変更手順のお知らせメールが送信されます。そのメールに書かれた手順に従ってパスワードの変更を実施してください。</p>
<p>もしメールが見つからない場合は、次のような原因が考えられます。</p>
<ul>
<li>ご入力いただいたメールアドレスの入力に誤りがある</li>
<li>ご利用中のメールBOXの容量超過によりメールが受信できない</li>
<li>ご利用中のセキュリティソフトの迷惑メール防止機能で、迷惑メールとして処理されている</li>
</ul>
<p>上記をご確認いただき、なおメールが届かない場合は、<a href="[[CONTACTUSLINK]]">当社までご連絡ください</a>。</p>' 
where resourcekey='lcalThankyouContent.Text' and classkey='~/account/resetpwd' and Locale='ja'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'已向您的邮箱地址送发了重置密码的说明邮件，请查看您的邮件并按照说明重置密码。' 
where resourcekey='lcalThankyouContent.Text' and classkey='~/account/resetpwd' and Locale='zh'

update [dbo].[GW_SSO_AnrGWResResourceData] set contentstr=N'已發送至您的電子郵件帳戶。請查看您的電子郵件。' 
where resourcekey='lcalThankyouContent.Text' and classkey='~/account/resetpwd' and Locale='zh-TW'



